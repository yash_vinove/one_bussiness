<?php session_start(); ?>
<?php include('../config.php'); ?>
<?php include('../sessionconfig.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	
 	<title>Style Configurator</title>

 	<!-- Bootstrap -->
 	<link href="bootstrap-3.3.1/dist/css/bootstrap.css" rel="stylesheet">

 	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
 	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">   
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
	<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script type="text/javascript">
    function redirectMe (sel) {
        var url = sel[sel.selectedIndex].value;
        window.location = url;
    }
    </script>
    <link rel='stylesheet' type='text/css' href='../style.php' />
</head>
  
<body class='body'>
	<?php include_once ('../headerthree.php');	?>
	
	<?php
		$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
	   	$rowid = isset($_GET['rowid']) ? strip_tags($_GET['rowid']) : '';
	 ?>
	 
	<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding:10px;">
		<!--search results header bar for desktop-->
		<div class='bodyheader'>
			<h3>Configure Style</h3>
		</div>
			<?php 
			if(isset($_POST['submit'])) {
				//get post values
				$h1size = isset($_POST['h1size']) ? strip_tags($_POST['h1size']) : '';
				$h2size = isset($_POST['h2size']) ? strip_tags($_POST['h2size']) : '';
				$h3size = isset($_POST['h3size']) ? strip_tags($_POST['h3size']) : '';
				$h4size = isset($_POST['h4size']) ? strip_tags($_POST['h4size']) : '';
				$h5size = isset($_POST['h5size']) ? strip_tags($_POST['h5size']) : '';
				$paragraphsize = isset($_POST['paragraphsize']) ? strip_tags($_POST['paragraphsize']) : '';
				$primary = isset($_POST['primary']) ? strip_tags($_POST['primary']) : '';
				$secondary = isset($_POST['secondary']) ? strip_tags($_POST['secondary']) : '';
				$tertiary = isset($_POST['tertiary']) ? strip_tags($_POST['tertiary']) : '';
				$successpositive = isset($_POST['successpositive']) ? strip_tags($_POST['successpositive']) : '';
				$warningnegative = isset($_POST['warningnegative']) ? strip_tags($_POST['warningnegative']) : '';
				$backup1 = isset($_POST['backup1']) ? strip_tags($_POST['backup1']) : '';
				$backup2 = isset($_POST['backup2']) ? strip_tags($_POST['backup2']) : '';
				$backup3 = isset($_POST['backup3']) ? strip_tags($_POST['backup3']) : '';
				$backup4 = isset($_POST['backup4']) ? strip_tags($_POST['backup4']) : '';
				$backup5 = isset($_POST['backup5']) ? strip_tags($_POST['backup5']) : '';
				$backup6 = isset($_POST['backup6']) ? strip_tags($_POST['backup6']) : '';
				$backup7 = isset($_POST['backup7']) ? strip_tags($_POST['backup7']) : '';
				$backup8 = isset($_POST['backup8']) ? strip_tags($_POST['backup8']) : '';
				$backup9 = isset($_POST['backup9']) ? strip_tags($_POST['backup9']) : '';
				$backup10 = isset($_POST['backup10']) ? strip_tags($_POST['backup10']) : '';
				$backgroundcolour = isset($_POST['backgroundcolour']) ? strip_tags($_POST['backgroundcolour']) : '';
				$headingbackgroundcolour = isset($_POST['headingbackgroundcolour']) ? strip_tags($_POST['headingbackgroundcolour']) : '';
				$headingtextcolour = isset($_POST['headingtextcolour']) ? strip_tags($_POST['headingtextcolour']) : '';
				$containerbackgroundcolour = isset($_POST['containerbackgroundcolour']) ? strip_tags($_POST['containerbackgroundcolour']) : '';
				$containertextcolour = isset($_POST['containertextcolour']) ? strip_tags($_POST['containertextcolour']) : '';
				$primarybackgroundcolour = isset($_POST['primarybackgroundcolour']) ? strip_tags($_POST['primarybackgroundcolour']) : '';
				$primarytextcolour = isset($_POST['primarytextcolour']) ? strip_tags($_POST['primarytextcolour']) : '';
				$primarytextsize = isset($_POST['primarytextsize']) ? strip_tags($_POST['primarytextsize']) : '';
				$primarybordercolour = isset($_POST['primarybordercolour']) ? strip_tags($_POST['primarybordercolour']) : '';
				$primaryhovercolour = isset($_POST['primaryhovercolour']) ? strip_tags($_POST['primaryhovercolour']) : '';
				$secondarybackgroundcolour = isset($_POST['secondarybackgroundcolour']) ? strip_tags($_POST['secondarybackgroundcolour']) : '';
				$secondarytextcolour = isset($_POST['secondarytextcolour']) ? strip_tags($_POST['secondarytextcolour']) : '';
				$secondarytextsize = isset($_POST['secondarytextsize']) ? strip_tags($_POST['secondarytextsize']) : '';
				$secondarybordercolour = isset($_POST['secondarybordercolour']) ? strip_tags($_POST['secondarybordercolour']) : '';
				$secondaryhovercolour = isset($_POST['secondaryhovercolour']) ? strip_tags($_POST['secondaryhovercolour']) : '';
				$navigationbackgroundcolour = isset($_POST['navigationbackgroundcolour']) ? strip_tags($_POST['navigationbackgroundcolour']) : '';
				$navigationheadingsize = isset($_POST['navigationheadingsize']) ? strip_tags($_POST['navigationheadingsize']) : '';
				$navigationbusinessnamesize = isset($_POST['navigationbusinessnamesize']) ? strip_tags($_POST['navigationbusinessnamesize']) : '';
				$navigationheadingcolour = isset($_POST['navigationheadingcolour']) ? strip_tags($_POST['navigationheadingcolour']) : '';
				$navigationbusinessnamecolour = isset($_POST['navigationbusinessnamecolour']) ? strip_tags($_POST['navigationbusinessnamecolour']) : '';
				$topbackgroundcolour = isset($_POST['topbackgroundcolour']) ? strip_tags($_POST['topbackgroundcolour']) : '';
				$bottombackgroundcolour = isset($_POST['bottombackgroundcolour']) ? strip_tags($_POST['bottombackgroundcolour']) : '';
				$fontfamily = isset($_POST['fontfamily']) ? strip_tags($_POST['fontfamily']) : '';
				
				//validate fields
				$validate = "";
				if($fontfamily == "") {
					$validate .= "Font Family is a required field. "; 	
				}
				if($h1size == "") {
					$validate .= "Heading 1 Text Size is a required field. "; 	
				}
				if($h1size >=1 && $h1size <= 1000) {
				}
				else {
					$validate .= "Heading 1 Text Size must be a number between 1 and 1000. "; 	
				}
				if($h2size == "") {
					$validate .= "Heading 2 Text Size is a required field. "; 	
				}
				if($h2size >=1 && $h2size <= 1000) {
				}
				else {
					$validate .= "Heading 2 Text Size must be a number between 1 and 1000. "; 	
				}
				if($h3size == "") {
					$validate .= "Heading 3 Text Size is a required field. "; 	
				}
				if($h3size >=1 && $h3size <= 1000) {
				}
				else {
					$validate .= "Heading 3 Text Size must be a number between 1 and 1000. "; 	
				}
				if($h4size == "") {
					$validate .= "Heading 4 Text Size is a required field. "; 	
				}
				if($h4size >=1 && $h4size <= 1000) {
				}
				else {
					$validate .= "Heading 4 Text Size must be a number between 1 and 1000. "; 	
				}
				if($h5size == "") {
					$validate .= "Heading 5 Text Size is a required field. "; 	
				}
				if($h5size >=1 && $h5size <= 1000) {
				}
				else {
					$validate .= "Heading 5 Text Size must be a number between 1 and 1000. "; 	
				}
				if($paragraphsize == "") {
					$validate .= "Paragraph Size is a required field. "; 	
				}
				if($paragraphsize >=1 && $paragraphsize <= 1000) {
				}
				else {
					$validate .= "Paragraph Size must be a number between 1 and 1000. "; 	
				}
				
				if($primary == "") {
					$validate .= "Primary Colour is a required field. "; 	
				}
				if(strlen($primary) ==6 || strlen($primary) ==7) {
				}
				else {
					$validate .= "Primary Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				if($secondary == "") {
					$validate .= "Secondary Colour is a required field. "; 	
				}
				if(strlen($secondary) ==6 || strlen($secondary) ==7) {
				}
				else {
					$validate .= "Secondary Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				if($tertiary == "") {
					$validate .= "Tertiary Colour is a required field. "; 	
				}
				if(strlen($tertiary) ==6 || strlen($tertiary) ==7) {
				}
				else {
					$validate .= "Tertiary Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				if($successpositive == "") {
					$validate .= "Success Colour is a required field. "; 	
				}
				if(strlen($successpositive) ==6 || strlen($successpositive) ==7) {
				}
				else {
					$validate .= "Success Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				if($warningnegative == "") {
					$validate .= "Warning Colour is a required field. "; 	
				}
				if(strlen($warningnegative) ==6 || strlen($warningnegative) ==7) {
				}
				else {
					$validate .= "Warning Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				
				
				if($backgroundcolour == "") {
					$validate .= "Background Colour is a required field. "; 	
				}
				if(strlen($backgroundcolour) ==6 || strlen($backgroundcolour) ==7) {
				}
				else {
					$validate .= "Background Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				if($headingbackgroundcolour == "") {
					$validate .= "Heading Background Colour is a required field. "; 	
				}
				if(strlen($headingbackgroundcolour) ==6 || strlen($headingbackgroundcolour) ==7) {
				}
				else {
					$validate .= "Heading Background Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				if($headingtextcolour == "") {
					$validate .= "Heading Text Colour is a required field. "; 	
				}
				if(strlen($headingtextcolour) ==6 || strlen($headingtextcolour) ==7) {
				}
				else {
					$validate .= "Heading Text Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				if($containerbackgroundcolour == "") {
					$validate .= "Container Background Colour is a required field. "; 	
				}
				if(strlen($containerbackgroundcolour) ==6 || strlen($containerbackgroundcolour) ==7) {
				}
				else {
					$validate .= "Container Background Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				if($containertextcolour == "") {
					$validate .= "Container Text Colour is a required field. "; 	
				}
				if(strlen($containertextcolour) ==6 || strlen($containertextcolour) ==7) {
				}
				else {
					$validate .= "Container Text Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				
				if($primarybackgroundcolour == "") {
					$validate .= "Primary Background Colour is a required field. "; 	
				}
				if(strlen($primarybackgroundcolour) ==6 || strlen($primarybackgroundcolour) ==7) {
				}
				else {
					$validate .= "Primary Background Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				if($primarytextcolour == "") {
					$validate .= "Primary Text Colour is a required field. "; 	
				}
				if(strlen($primarytextcolour) ==6 || strlen($primarytextcolour) ==7) {
				}
				else {
					$validate .= "Primary Text Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				if($primarytextsize == "") {
					$validate .= "Primary Text Size is a required field. "; 	
				}
				if($primarytextsize >=1 && $primarytextsize <= 1000) {
				}
				else {
					$validate .= "Primary Text Size must be a number between 1 and 1000. "; 	
				}
				if($primarybordercolour == "") {
					$validate .= "Primary Border Colour is a required field. "; 	
				}
				if(strlen($primarybordercolour) ==6 || strlen($primarybordercolour) ==7) {
				}
				else {
					$validate .= "Primary Border Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				if($primaryhovercolour == "") {
					$validate .= "Primary Hover Colour is a required field. "; 	
				}
				if(strlen($primaryhovercolour) ==6 || strlen($primaryhovercolour) ==7) {
				}
				else {
					$validate .= "Primary Hover Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				if($secondarybackgroundcolour == "") {
					$validate .= "Secondary Background Colour is a required field. "; 	
				}
				if(strlen($secondarybackgroundcolour) ==6 || strlen($secondarybackgroundcolour) ==7) {
				}
				else {
					$validate .= "Secondary Background Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				if($secondarytextcolour == "") {
					$validate .= "Secondary Text Colour is a required field. "; 	
				}
				if(strlen($secondarytextcolour) ==6 || strlen($secondarytextcolour) ==7) {
				}
				else {
					$validate .= "Secondary Text Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				if($secondarytextsize == "") {
					$validate .= "Secondary Text Size is a required field. "; 	
				}
				if($secondarytextsize >=1 && $secondarytextsize <= 1000) {
				}
				else {
					$validate .= "Secondary Text Size must be a number between 1 and 1000. "; 	
				}
				if($secondarybordercolour == "") {
					$validate .= "Secondary Border Colour is a required field. "; 	
				}
				if(strlen($secondarybordercolour) ==6 || strlen($secondarybordercolour) ==7) {
				}
				else {
					$validate .= "Secondary Border Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				if($secondaryhovercolour == "") {
					$validate .= "Secondary Hover Colour is a required field. "; 	
				}
				if(strlen($secondaryhovercolour) ==6 || strlen($secondaryhovercolour) ==7) {
				}
				else {
					$validate .= "Secondary Hover Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				
				
				if($navigationbackgroundcolour == "") {
					$validate .= "Navigation Background Colour is a required field. "; 	
				}
				if(strlen($navigationbackgroundcolour) ==6 || strlen($navigationbackgroundcolour) ==7) {
				}
				else {
					$validate .= "Navigation Background Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				if($navigationheadingsize == "") {
					$validate .= "Navigation Heading Size is a required field. "; 	
				}
				if($navigationheadingsize >=1 && $navigationheadingsize <= 1000) {
				}
				else {
					$validate .= "Navigation Heading Size must be a number between 1 and 1000. "; 	
				}
				if($navigationbusinessnamesize == "") {
					$validate .= "Navigation Business Name Size is a required field. "; 	
				}
				if($navigationbusinessnamesize >=1 && $navigationbusinessnamesize <= 1000) {
				}
				else {
					$validate .= "Navigation Business Name must be a number between 1 and 1000. "; 	
				}				
				if($navigationheadingcolour == "") {
					$validate .= "Navigation Heading Colour is a required field. "; 	
				}
				if(strlen($navigationheadingcolour) ==6 || strlen($navigationheadingcolour) ==7) {
				}
				else {
					$validate .= "Navigation Heading Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				if($navigationbusinessnamecolour == "") {
					$validate .= "Navigation Business Name Colour is a required field. "; 	
				}
				if(strlen($navigationbusinessnamecolour) ==6 || strlen($navigationbusinessnamecolour) ==7) {
				}
				else {
					$validate .= "Navigation Business Name Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				
				
				if($topbackgroundcolour == "") {
					$validate .= "Top Background Colour is a required field. "; 	
				}
				if(strlen($topbackgroundcolour) ==6 || strlen($topbackgroundcolour) ==7) {
				}
				else {
					$validate .= "Top Background Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				if($bottombackgroundcolour == "") {
					$validate .= "Bottom Background Colour is a required field. "; 	
				}
				if(strlen($bottombackgroundcolour) ==6 || strlen($bottombackgroundcolour) ==7) {
				}
				else {
					$validate .= "Bottom Background Colour must be a hex code e.g. #fff999 or e.g. fff999. "; 	
				}
				
				
				if($validate == "") {
					//delete all existing data for this style
					$deleterecords = mysql_query("delete from styleitem where styleid = '$rowid'");
				
					//add new records into the table for this style
					$datecreated = date("Y-m-d");
					
					$addfontfamily = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('fontfontfamily','$rowid','$fontfamily','0','$datecreated')");
					
					$addh1size = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('texth1size','$rowid','$h1size','0','$datecreated')");
					$addh2size = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('texth2size','$rowid','$h2size','0','$datecreated')");
					$addh3size = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('texth3size','$rowid','$h3size','0','$datecreated')");
					$addh4size = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('texth4size','$rowid','$h4size','0','$datecreated')");
					$addh5size = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('texth5size','$rowid','$h5size','0','$datecreated')");
					$addparagraphsize = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('textparagraphsize','$rowid','$paragraphsize','0','$datecreated')");
					
					$primary = str_replace("#", "", $primary);
					$primary = "#".$primary;
					$addprimary = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('colourprimary','$rowid','$primary','0','$datecreated')");
					$secondary = str_replace("#", "", $secondary);
					$secondary = "#".$secondary;
					$addsecondary = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('coloursecondary','$rowid','$secondary','0','$datecreated')");
					$tertiary = str_replace("#", "", $tertiary);
					$tertiary = "#".$tertiary;
					$addtertiary = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('colourtertiary','$rowid','$tertiary','0','$datecreated')");
					$successpositive = str_replace("#", "", $successpositive);
					$successpositive = "#".$successpositive;
					$addsuccesspositive = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('coloursuccesspositive','$rowid','$successpositive','0','$datecreated')");
					$warningnegative = str_replace("#", "", $warningnegative);
					$warningnegative = "#".$warningnegative;
					$addwarningnegative = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('colourwarningnegative','$rowid','$warningnegative','0','$datecreated')");
					$backup1 = str_replace("#", "", $backup1);
					$backup1 = "#".$backup1;
					$addbackup1 = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('colourbackup1','$rowid','$backup1','0','$datecreated')");
					$backup2 = str_replace("#", "", $backup2);
					$backup2 = "#".$backup2;
					$addbackup2 = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('colourbackup2','$rowid','$backup2','0','$datecreated')");
					$backup3 = str_replace("#", "", $backup3);
					$backup3 = "#".$backup3;
					$addbackup3 = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('colourbackup3','$rowid','$backup3','0','$datecreated')");
					$backup4 = str_replace("#", "", $backup4);
					$backup4 = "#".$backup4;
					$addbackup1 = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('colourbackup4','$rowid','$backup4','0','$datecreated')");
					$backup5 = str_replace("#", "", $backup5);
					$backup5 = "#".$backup5;
					$addbackup5 = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('colourbackup5','$rowid','$backup5','0','$datecreated')");
					$backup6 = str_replace("#", "", $backup6);
					$backup6 = "#".$backup6;
					$addbackup6 = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('colourbackup6','$rowid','$backup6','0','$datecreated')");
					$backup7 = str_replace("#", "", $backup7);
					$backup7 = "#".$backup7;
					$addbackup7 = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('colourbackup7','$rowid','$backup7','0','$datecreated')");
					$backup8 = str_replace("#", "", $backup8);
					$backup8 = "#".$backup8;
					$addbackup8 = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('colourbackup8','$rowid','$backup8','0','$datecreated')");
					$backup9 = str_replace("#", "", $backup9);
					$backup9 = "#".$backup9;
					$addbackup9 = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('colourbackup9','$rowid','$backup9','0','$datecreated')");
					$backup10 = str_replace("#", "", $backup10);
					$backup10 = "#".$backup10;
					$addbackup10 = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('colourbackup10','$rowid','$backup10','0','$datecreated')");
					
					$backgroundcolour = str_replace("#", "", $backgroundcolour);
					$backgroundcolour = "#".$backgroundcolour;
					$addbackgroundcolour = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('backgroundbackgroundcolour','$rowid','$backgroundcolour','0','$datecreated')");
					$headingbackgroundcolour = str_replace("#", "", $headingbackgroundcolour);
					$headingbackgroundcolour = "#".$headingbackgroundcolour;
					$addheadingbackgroundcolour = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('backgroundheadingbackgroundcolour','$rowid','$headingbackgroundcolour','0','$datecreated')");
					$headingtextcolour = str_replace("#", "", $headingtextcolour);
					$headingtextcolour = "#".$headingtextcolour;
					$addheadingtextcolour = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('backgroundheadingtextcolour','$rowid','$headingtextcolour','0','$datecreated')");
					$containerbackgroundcolour = str_replace("#", "", $containerbackgroundcolour);
					$containerbackgroundcolour = "#".$containerbackgroundcolour;
					$addcontainerbackgroundcolour = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('backgroundcontainerbackgroundcolour','$rowid','$containerbackgroundcolour','0','$datecreated')");
					$containertextcolour = str_replace("#", "", $containertextcolour);
					$containertextcolour = "#".$containertextcolour;
					$addcontainertextcolour = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('backgroundcontainertextcolour','$rowid','$containertextcolour','0','$datecreated')");
					
					$primarybackgroundcolour = str_replace("#", "", $primarybackgroundcolour);
					$primarybackgroundcolour = "#".$primarybackgroundcolour;
					$addprimarybackgroundcolour = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('buttonprimarybackgroundcolour','$rowid','$primarybackgroundcolour','0','$datecreated')");
					$primarytextcolour = str_replace("#", "", $primarytextcolour);
					$primarytextcolour = "#".$primarytextcolour;
					$addprimarytextcolour = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('buttonprimarytextcolour','$rowid','$primarytextcolour','0','$datecreated')");
					$addprimarytextsize = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('buttonprimarytextsize','$rowid','$primarytextsize','0','$datecreated')");				
					$primarybordercolour = str_replace("#", "", $primarybordercolour);
					$primarybordercolour = "#".$primarybordercolour;
					$addprimarybordercolour = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('buttonprimarybordercolour','$rowid','$primarybordercolour','0','$datecreated')");
					$primaryhovercolour = str_replace("#", "", $primaryhovercolour);
					$primaryhovercolour = "#".$primaryhovercolour;
					$addprimaryhovercolour = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('buttonprimaryhovercolour','$rowid','$primaryhovercolour','0','$datecreated')");
										
					$secondarybackgroundcolour = str_replace("#", "", $secondarybackgroundcolour);
					$secondarybackgroundcolour = "#".$secondarybackgroundcolour;
					$addsecondarybackgroundcolour = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('buttonsecondarybackgroundcolour','$rowid','$secondarybackgroundcolour','0','$datecreated')");
					$secondarytextcolour = str_replace("#", "", $secondarytextcolour);
					$secondarytextcolour = "#".$secondarytextcolour;
					$addsecondarytextcolour = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('buttonsecondarytextcolour','$rowid','$secondarytextcolour','0','$datecreated')");
					$addsecondarytextsize = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('buttonsecondarytextsize','$rowid','$secondarytextsize','0','$datecreated')");				
					$secondarybordercolour = str_replace("#", "", $secondarybordercolour);
					$secondarybordercolour = "#".$secondarybordercolour;
					$addsecondarybordercolour = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('buttonsecondarybordercolour','$rowid','$secondarybordercolour','0','$datecreated')");
					$secondaryhovercolour = str_replace("#", "", $secondaryhovercolour);
					$secondaryhovercolour = "#".$secondaryhovercolour;
					$addsecondaryhovercolour = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('buttonsecondaryhovercolour','$rowid','$secondaryhovercolour','0','$datecreated')");
					
					$navigationbackgroundcolour = str_replace("#", "", $navigationbackgroundcolour);
					$navigationbackgroundcolour = "#".$navigationbackgroundcolour;
					$addnavigationbackgroundcolour = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('topnavigationnavigationbackgroundcolour','$rowid','$navigationbackgroundcolour','0','$datecreated')");
					$addnavigationheadingsize = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('topnavigationnavigationheadingsize','$rowid','$navigationheadingsize','0','$datecreated')");				
					$addnavigationbusinessnamesize = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('topnavigationnavigationbusinessnamesize','$rowid','$navigationbusinessnamesize','0','$datecreated')");				
					$navigationheadingcolour = str_replace("#", "", $navigationheadingcolour);
					$navigationheadingcolour = "#".$navigationheadingcolour;
					$addnavigationheadingcolour = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('topnavigationnavigationheadingcolour','$rowid','$navigationheadingcolour','0','$datecreated')");
					$navigationbusinessnamecolour = str_replace("#", "", $navigationbusinessnamecolour);
					$navigationbusinessnamecolour = "#".$navigationbusinessnamecolour;
					$addnavigationbusinessnamecolour = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('topnavigationnavigationbusinessnamecolour','$rowid','$navigationbusinessnamecolour','0','$datecreated')");
					
					$topbackgroundcolour = str_replace("#", "", $topbackgroundcolour);
					$topbackgroundcolour = "#".$topbackgroundcolour;
					$addtopbackgroundcolour = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('bottomnavigationtopbackgroundcolour','$rowid','$topbackgroundcolour','0','$datecreated')");
					$bottombackgroundcolour = str_replace("#", "", $bottombackgroundcolour);
					$bottombackgroundcolour = "#".$bottombackgroundcolour;
					$addbottombackgroundcolour = mysql_query("insert into styleitem (styleitemname, styleid, styledetail, disabled, datecreated) 
					values ('bottomnavigationbottombackgroundcolour','$rowid','$bottombackgroundcolour','0','$datecreated')");
										
					
					//redirect user back to UI style page
					$url = '../pagegrid.php?pagetype=style';
					echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">';   				

					
				}
				
				
			} 
			else {
				$getdata = mysql_query("select * from styleitem where styleid = '$rowid'");
				if(mysql_num_rows($getdata)==0) {
					$h1size = "24";
					$h2size = "18";	
					$h3size = "14";	
					$h4size = "12";	
					$h5size = "10";
					$paragraphsize = "14";
					$primary = "#ffffff";	
					$secondary = "#ffffff";
					$tertiary = "#ffffff";
					$successpositive = "#21f900";	
					$warningnegative = "#f90d00";	
					$backup1 = "#ffffff";	
					$backup2 = "#ffffff";	
					$backup3 = "#ffffff";	
					$backup4 = "#ffffff";	
					$backup5 = "#ffffff";	
					$backup6 = "#ffffff";	
					$backup7 = "#ffffff";	
					$backup8 = "#ffffff";	
					$backup9 = "#ffffff";	
					$backup10 = "#ffffff";	
					$backgroundcolour = "#D3D3D3";
					$headingbackgroundcolour = "#e9e9e9";
					$headingtextcolour = "#000000";	
					$containerbackgroundcolour = "#ffffff";
					$containertextcolour = "#000000";	
					$primarybackgroundcolour = "#3687f3";	
					$primarytextcolour = "#ffffff";	
					$primarytextsize = "16";
					$primarybordercolour = "#2e6da4";	
					$primaryhovercolour = "#286090";		
					$secondarybackgroundcolour = "#3687f3";	
					$secondarytextcolour = "#ffffff";	
					$secondarytextsize = "16";	
					$secondarybordercolour = "#2e6da4";
					$secondaryhovercolour = "#286090";	
					$navigationbackgroundcolour = "#e9e9e9";
					$navigationheadingsize = "14";
					$navigationbusinessnamesize = "24";
					$navigationheadingcolour = "#000000";
					$navigationbusinessnamecolour = "#3687f3";
					$topbackgroundcolour = "#3687f3";	
					$bottombackgroundcolour = "#2e6da4";
					$fontfamily = "Arial, Helvetica, sans-serif";			
				}
				else {
				
					while($row=mysql_fetch_array($getdata)){ 
						$styleitemname = $row['styleitemname'];
						$styledetail = $row['styledetail'];			
						
						if($styleitemname == 'texth1size'){
							$h1size = $styledetail;					
						}	
						if($styleitemname == 'texth2size'){
							$h2size = $styledetail;					
						}	
						if($styleitemname == 'texth3size'){
							$h3size = $styledetail;					
						}	
						if($styleitemname == 'texth4size'){
							$h4size = $styledetail;					
						}	
						if($styleitemname == 'texth5size'){
							$h5size = $styledetail;					
						}	
						if($styleitemname == 'textparagraphsize'){
							$paragraphsize = $styledetail;					
						}	
						
						if($styleitemname == 'colourprimary'){
							$primary = $styledetail;					
						}	
						if($styleitemname == 'coloursecondary'){
							$secondary = $styledetail;					
						}	
						if($styleitemname == 'colourtertiary'){
							$tertiary = $styledetail;					
						}	
						
						if($styleitemname == 'coloursuccesspositive'){
							$successpositive = $styledetail;					
						}	
						if($styleitemname == 'colourwarningnegative'){
							$warningnegative = $styledetail;					
						}	
						if($styleitemname == 'colourbackup1'){
							$backup1 = $styledetail;					
						}	
						if($styleitemname == 'colourbackup2'){
							$backup2 = $styledetail;					
						}	
						if($styleitemname == 'colourbackup3'){
							$backup3 = $styledetail;					
						}	
						if($styleitemname == 'colourbackup4'){
							$backup4 = $styledetail;					
						}	
						if($styleitemname == 'colourbackup5'){
							$backup5 = $styledetail;					
						}	
						if($styleitemname == 'colourbackup6'){
							$backup6 = $styledetail;					
						}	
						if($styleitemname == 'colourbackup7'){
							$backup7 = $styledetail;					
						}	
						if($styleitemname == 'colourbackup8'){
							$backup8 = $styledetail;					
						}	
						if($styleitemname == 'colourbackup9'){
							$backup9 = $styledetail;					
						}	
						if($styleitemname == 'colourbackup10'){
							$backup10 = $styledetail;					
						}	
						
	
						if($styleitemname == 'backgroundbackgroundcolour'){
							$backgroundcolour = $styledetail;					
						}	
						if($styleitemname == 'backgroundheadingbackgroundcolour'){
							$headingbackgroundcolour = $styledetail;					
						}	
						if($styleitemname == 'backgroundheadingtextcolour'){
							$headingtextcolour = $styledetail;					
						}	
						if($styleitemname == 'backgroundcontainerbackgroundcolour'){
							$containerbackgroundcolour = $styledetail;					
						}	
						if($styleitemname == 'backgroundcontainertextcolour'){
							$containertextcolour = $styledetail;					
						}	
						
						
						if($styleitemname == 'buttonprimarybackgroundcolour'){
							$primarybackgroundcolour = $styledetail;					
						}	
						if($styleitemname == 'buttonprimarytextcolour'){
							$primarytextcolour = $styledetail;					
						}	
						if($styleitemname == 'buttonprimarytextsize'){
							$primarytextsize = $styledetail;					
						}	
						if($styleitemname == 'buttonprimarybordercolour'){
							$primarybordercolour = $styledetail;					
						}	
						if($styleitemname == 'buttonprimaryhovercolour'){
							$primaryhovercolour = $styledetail;					
						}	
						if($styleitemname == 'buttonsecondarybackgroundcolour'){
							$secondarybackgroundcolour = $styledetail;					
						}	
						if($styleitemname == 'buttonsecondarytextcolour'){
							$secondarytextcolour = $styledetail;					
						}	
						if($styleitemname == 'buttonsecondarytextsize'){
							$secondarytextsize = $styledetail;					
						}	
						if($styleitemname == 'buttonsecondarybordercolour'){
							$secondarybordercolour = $styledetail;					
						}	
						if($styleitemname == 'buttonsecondaryhovercolour'){
							$secondaryhovercolour = $styledetail;					
						}	
						
						
						if($styleitemname == 'topnavigationnavigationbackgroundcolour'){
							$navigationbackgroundcolour = $styledetail;					
						}	
						if($styleitemname == 'topnavigationnavigationheadingsize'){
							$navigationheadingsize = $styledetail;					
						}	
						if($styleitemname == 'topnavigationnavigationbusinessnamesize'){
							$navigationbusinessnamesize = $styledetail;					
						}	
						if($styleitemname == 'topnavigationnavigationheadingcolour'){
							$navigationheadingcolour = $styledetail;					
						}	
						if($styleitemname == 'topnavigationnavigationbusinessnamecolour'){
							$navigationbusinessnamecolour = $styledetail;					
						}	
						
						if($styleitemname == 'bottomnavigationtopbackgroundcolour'){
							$topbackgroundcolour = $styledetail;					
						}	
						if($styleitemname == 'bottomnavigationbottombackgroundcolour'){
							$bottombackgroundcolour = $styledetail;					
						}	
						if($styleitemname == 'fontfontfamily'){
							$fontfamily = $styledetail;					
						}	
						
					}
				}
			
			}
			
			?>
			
			<div class='bodycontent'>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<?php 
	  				if(isset($validate) && $validate <> "") {
						echo "<p class='background-warning'>".$validate."</p>";
					} 
					?>
				<?php
				echo "<p>Below you will find a number of sections that can be changed. Please ensure you click 
				Save at the bottom of the page, in order to change the saved details. 
				On the right hand side you can see an example of the saved changes you have made.<br/><br/></p>";
				?>
		
			</div>
				
					<form class="form-horizontal" action='styleconfigurator.php?rowid=<?php echo $rowid?>' method="post">
						<div class="col-xs-12 col-md-7 col-sm-8 col-lg-6">
						
							<h2>Font</h2><br/>
							<div class="form-group">
								<label for="Font Family" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Font Family</label>
						    	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
					    		  	<?php
					    		  	echo "<select class='form-control' name='fontfamily'>";
						        	echo "<option value=''>Select Item</option>";
						        	if($fontfamily=="Arial, Helvetica, sans-serif") {
						        		echo "<option value='Arial, Helvetica, sans-serif' selected='true'>Arial, Helvetica, sans-serif</option>";
						        	} 
						        	else {
						        		echo "<option value='Arial, Helvetica, sans-serif'>Arial, Helvetica, sans-serif</option>";	
						        	}
					         		if($fontfamily=="Arial Black, Gadget, sans-serif") {
						        		echo "<option value='Arial Black, Gadget, sans-serif' selected='true'>Arial Black, Gadget, sans-serif</option>";
						        	} 
						        	else {
						        		echo "<option value='Arial Black, Gadget, sans-serif'>Arial Black, Gadget, sans-serif</option>";	
						        	}
					         		if($fontfamily=="Comic Sans MS, cursive, sans-serif") {
						        		echo "<option value='Comic Sans MS, cursive, sans-serif' selected='true'>Comic Sans MS, cursive, sans-serif</option>";
						        	} 
						        	else {
						        		echo "<option value='Comic Sans MS, cursive, sans-serif'>Comic Sans MS, cursive, sans-serif</option>";	
						        	}
					         		if($fontfamily=="Courier New, Courier, monospace") {
						        		echo "<option value='Courier New, Courier, monospace' selected='true'>Courier New, Courier, monospace</option>";
						        	} 
						        	else {
						        		echo "<option value='Courier New, Courier, monospace'>Courier New, Courier, monospace</option>";	
						        	}
					         		if($fontfamily=="Georgia, serif") {
						        		echo "<option value='Georgia, serif' selected='true'>Georgia, serif</option>";
						        	} 
						        	else {
						        		echo "<option value='Georgia, serif'>Georgia, serif</option>";	
						        	}
					         		if($fontfamily=="Impact, Charcoal, sans-serif") {
						        		echo "<option value='Impact, Charcoal, sans-serif' selected='true'>Impact, Charcoal, sans-serif</option>";
						        	} 
						        	else {
						        		echo "<option value='Impact, Charcoal, sans-serif'>Impact, Charcoal, sans-serif</option>";	
						        	}
					         		if($fontfamily=="Lucida Console, Monaco, monospace") {
						        		echo "<option value='Lucida Console, Monaco, monospace' selected='true'>Lucida Console, Monaco, monospace</option>";
						        	} 
						        	else {
						        		echo "<option value='Lucida Console, Monaco, monospace'>Lucida Console, Monaco, monospace</option>";	
						        	}
					         		if($fontfamily=="Lucida Sans Unicode, Lucida Grande, sans-serif") {
						        		echo "<option value='Lucida Sans Unicode, Lucida Grande, sans-serif' selected='true'>Lucida Sans Unicode, Lucida Grande, sans-serif</option>";
						        	} 
						        	else {
						        		echo "<option value='Lucida Sans Unicode, Lucida Grande, sans-serif'>Lucida Sans Unicode, Lucida Grande, sans-serif</option>";	
						        	}
					         		if($fontfamily=="Palatino Linotype, Book Antiqua, Palatino, serif") {
						        		echo "<option value='Palatino Linotype, Book Antiqua, Palatino, serif' selected='true'>Palatino Linotype, Book Antiqua, Palatino, serif</option>";
						        	} 
						        	else {
						        		echo "<option value='Palatino Linotype, Book Antiqua, Palatino, serif'>Palatino Linotype, Book Antiqua, Palatino, serif</option>";	
						        	}
					         		if($fontfamily=="Tahoma, Geneva, sans-serif") {
						        		echo "<option value='Tahoma, Geneva, sans-serif' selected='true'>Tahoma, Geneva, sans-serif</option>";
						        	} 
						        	else {
						        		echo "<option value='Tahoma, Geneva, sans-serif'>Tahoma, Geneva, sans-serif</option>";	
						        	}
					         		if($fontfamily=="Times New Roman, Times, serif") {
						        		echo "<option value='Times New Roman, Times, serif' selected='true'>Times New Roman, Times, serif</option>";
						        	} 
						        	else {
						        		echo "<option value='Times New Roman, Times, serif'>Times New Roman, Times, serif</option>";	
						        	}
					         		if($fontfamily=="Trebuchet MS, Helvetica, sans-serif") {
						        		echo "<option value='Trebuchet MS, Helvetica, sans-serif' selected='true'>Trebuchet MS, Helvetica, sans-serif</option>";
						        	} 
						        	else {
						        		echo "<option value='Trebuchet MS, Helvetica, sans-serif'>Trebuchet MS, Helvetica, sans-serif</option>";	
						        	}
					         		if($fontfamily=="Verdana, Geneva, sans-serif") {
						        		echo "<option value='Verdana, Geneva, sans-serif' selected='true'>Verdana, Geneva, sans-serif</option>";
						        	} 
						        	else {
						        		echo "<option value='Verdana, Geneva, sans-serif'>Verdana, Geneva, sans-serif</option>";	
						        	}
					         		echo "</select>";
						      		?>
						      		Select a font from the dropdown list. These are the fonts most browsers support. 
						    	</div>
						  	</div> 								
								
								
							<h2>Text Sizes</h2><br/>
							<div class="form-group">
								<label for="Text H1" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Heading 1 Text Size</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="h1size" value="<?php echo $h1size ?>">
							   	This should be your largest potential heading size in pixels (px).	
							  	</div>
							</div>
							<div class="form-group">
								<label for="Text H2" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Heading 2 Text Size</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="h2size" value="<?php echo $h2size ?>">
							   	Size in pixels (px).	
							  	</div>
							</div>
							<div class="form-group">
								<label for="Text H3" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Heading 3 Text Size</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="h3size" value="<?php echo $h3size ?>">
							   	Size in pixels (px).	
							  	</div>
							</div>
							<div class="form-group">
								<label for="Text H4" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Heading 4 Text Size</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="h4size" value="<?php echo $h4size ?>">
							   	Size in pixels (px).	
							  	</div>
							</div>
							<div class="form-group">
								<label for="Text H5" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Heading 5 Text Size</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="h5size" value="<?php echo $h5size ?>">
							   	This should be your smallest potential heading size in pixels (px).	
							  	</div>
							</div>
							<div class="form-group">
								<label for="Paragraph Size" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Paragraph Size</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="paragraphsize" value="<?php echo $paragraphsize ?>">
							   	This should be the default size of most of the text on the system in pixels (px).	
							  	</div>
							</div>
							
							<h2>Colours</h2><br/>
							<div class="form-group">
								<label for="Primary Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Primary Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="primary" value="<?php echo $primary ?>">
							   	This should be a hex code e.g. #80dftg.	
							  	</div>
							</div>
							<div class="form-group">
								<label for="Secondary Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Secondary Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="secondary" value="<?php echo $secondary ?>">
							   	This should be a hex code e.g. #80dftg.	
							  	</div>
							</div>
							<div class="form-group">
								<label for="Tertiary Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Tertiary Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="tertiary" value="<?php echo $tertiary ?>">
							   	This should be a hex code e.g. #80dftg.	
							  	</div>
							</div>
							<div class="form-group">
								<label for="Success Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Success Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="successpositive" value="<?php echo $successpositive ?>">
							   	This should be a hex code e.g. #80dftg. It is used on all positive success messages.	
							  	</div>
							</div>
							<div class="form-group">
								<label for="Warning Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Warning Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="warningnegative" value="<?php echo $warningnegative ?>">
							   	This should be a hex code e.g. #80dftg. It is used on all negative warning messages.
							  	</div>
							</div>
							<div class="form-group">
								<label for="Backup Colour 1" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Backup Colour 1</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="backup1" value="<?php echo $backup1 ?>">
							   	This should be a hex code e.g. #80dftg.	
							  	</div>
							</div>
							<div class="form-group">
								<label for="Backup Colour 2" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Backup Colour 2</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="backup2" value="<?php echo $backup2 ?>">
							   	This should be a hex code e.g. #80dftg.	
							  	</div>
							</div>
							<div class="form-group">
								<label for="Backup Colour 3" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Backup Colour 3</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="backup3" value="<?php echo $backup3 ?>">
							   	This should be a hex code e.g. #80dftg.	
							  	</div>
							</div>
							<div class="form-group">
								<label for="Backup Colour 4" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Backup Colour 4</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="backup4" value="<?php echo $backup4 ?>">
							   	This should be a hex code e.g. #80dftg.	
							  	</div>
							</div>
							<div class="form-group">
								<label for="Backup Colour 5" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Backup Colour 5</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="backup5" value="<?php echo $backup5 ?>">
							   	This should be a hex code e.g. #80dftg.	
							  	</div>
							</div>
							<div class="form-group">
								<label for="Backup Colour 6" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Backup Colour 6</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="backup6" value="<?php echo $backup6 ?>">
							   	This should be a hex code e.g. #80dftg.	
							  	</div>
							</div>
							<div class="form-group">
								<label for="Backup Colour 7" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Backup Colour 7</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="backup7" value="<?php echo $backup7 ?>">
							   	This should be a hex code e.g. #80dftg.	
							  	</div>
							</div>
							<div class="form-group">
								<label for="Backup Colour 8" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Backup Colour 8</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="backup8" value="<?php echo $backup8 ?>">
							   	This should be a hex code e.g. #80dftg.	
							  	</div>
							</div>
							<div class="form-group">
								<label for="Backup Colour 9" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Backup Colour 9</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="backup9" value="<?php echo $backup9 ?>">
							   	This should be a hex code e.g. #80dftg.	
							  	</div>
							</div>
							<div class="form-group">
								<label for="Backup Colour 10" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Backup Colour 10</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="backup10" value="<?php echo $backup10 ?>">
							   	This should be a hex code e.g. #80dftg.	
							  	</div>
							</div>
							
							<h2>Background</h2><br/>
							<div class="form-group">
								<label for="Background Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Background Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="backgroundcolour" value="<?php echo $backgroundcolour ?>">
							   	This should be a hex code e.g. #80dftg. This is for the main underlying background.
							  	</div>
							</div>
							<div class="form-group">
								<label for="Heading Background Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Heading Background Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="headingbackgroundcolour" value="<?php echo $headingbackgroundcolour ?>">
							   	This should be a hex code e.g. #80dftg. This is for the background underlying the main content panel heading. 
							  	</div>
							</div>
							<div class="form-group">
								<label for="Heading Text Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Heading Text Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="headingtextcolour" value="<?php echo $headingtextcolour ?>">
							   	This should be a hex code e.g. #80dftg. This is for the colour of the text on the main content panel heading. 
							  	</div>
							</div>
							<div class="form-group">
								<label for="Container Background Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Container Background Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="containerbackgroundcolour" value="<?php echo $containerbackgroundcolour ?>">
							   	This should be a hex code e.g. #80dftg. This is for the background underlying the main content panel. 
							  	</div>
							</div>
							<div class="form-group">
								<label for="Container Text Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Container Text Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="containertextcolour" value="<?php echo $containertextcolour ?>">
							   	This should be a hex code e.g. #80dftg. This is for the colour of the text on the main content panel.
							  	</div>
							</div>
											  	
											  	
							<h2>Buttons</h2><br/>
							<div class="form-group">
								<label for="Primary Background Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Primary Background Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="primarybackgroundcolour" value="<?php echo $primarybackgroundcolour ?>">
							   	This should be a hex code e.g. #80dftg. This is the background colour of the majority of the buttons.
							  	</div>
							</div>
							<div class="form-group">
								<label for="Primary Text Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Primary Text Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="primarytextcolour" value="<?php echo $primarytextcolour ?>">
							   	This should be a hex code e.g. #80dftg. This is the text colour on the majority of the buttons.
							  	</div>
							</div>
							<div class="form-group">
								<label for="Primary Text Size" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Primary Text Size</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="primarytextsize" value="<?php echo $primarytextsize ?>">
							   	Size of the text on the button in pixels (px).	
							  	</div>
							</div>
							<div class="form-group">
								<label for="Primary Border Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Primary Border Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="primarybordercolour" value="<?php echo $primarybordercolour ?>">
							   	This should be a hex code e.g. #80dftg. This is the colour of the border on most buttons.
							  	</div>
							</div>
							<div class="form-group">
								<label for="Primary Hover Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Primary Hover Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="primaryhovercolour" value="<?php echo $primaryhovercolour ?>">
							   	This should be a hex code e.g. #80dftg. This is the colour of the majority of the buttons when you hover over them.
							  	</div>
							</div>
							<div class="form-group">
								<label for="Secondary Background Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Secondary Background Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="secondarybackgroundcolour" value="<?php echo $secondarybackgroundcolour ?>">
							   	This should be a hex code e.g. #80dftg. This is the background colour of secondary buttons.
							  	</div>
							</div>
							<div class="form-group">
								<label for="Secondary Text Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Secondary Text Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="secondarytextcolour" value="<?php echo $secondarytextcolour ?>">
							   	This should be a hex code e.g. #80dftg. This is the text colour on the secondary buttons.
							  	</div>
							</div>
							<div class="form-group">
								<label for="Secondary Text Size" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Secondary Text Size</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="secondarytextsize" value="<?php echo $secondarytextsize ?>">
							   	Size of the text on the secondary button in pixels (px).	
							  	</div>
							</div>
							<div class="form-group">
								<label for="Secondary Border Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Secondary Border Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="secondarybordercolour" value="<?php echo $secondarybordercolour ?>">
							   	This should be a hex code e.g. #80dftg. This is the colour of the border on the secondary buttons.
							  	</div>
							</div>
							<div class="form-group">
								<label for="Secondary Hover Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Secondary Hover Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="secondaryhovercolour" value="<?php echo $secondaryhovercolour ?>">
							   	This should be a hex code e.g. #80dftg. This is the colour of the secondary buttons when you hover over them.
							  	</div>
							</div>
							
							<h2>Top Navigation</h2><br/>
							<div class="form-group">
								<label for="Navigation Background Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Navigation Background Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="navigationbackgroundcolour" value="<?php echo $navigationbackgroundcolour ?>">
							   	This should be a hex code e.g. #80dftg. This is the background colour of the top navigation banner.
							  	</div>
							</div>
							<div class="form-group">
								<label for="Navigation Heading Size" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Navigation Heading Size</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="navigationheadingsize" value="<?php echo $navigationheadingsize ?>">
							   	Size of the text on the top navigation heading in pixels (px).	
							  	</div>
							</div>
							<div class="form-group">
								<label for="Navigation Business Name Size" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Navigation Business Name Size</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="navigationbusinessnamesize" value="<?php echo $navigationbusinessnamesize ?>">
							   	Size of the text of the business name in the top navigation in pixels (px).	
							  	</div>
							</div>
							<div class="form-group">
								<label for="Navigation Heading Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Navigation Heading Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="navigationheadingcolour" value="<?php echo $navigationheadingcolour ?>">
							   	This should be a hex code e.g. #80dftg. This is the colour of the text in the top main navigation.
							  	</div>
							</div>
							<div class="form-group">
								<label for="Navigation Business Name Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Navigation Business Name Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="navigationbusinessnamecolour" value="<?php echo $navigationbusinessnamecolour ?>">
							   	This should be a hex code e.g. #80dftg. This is the colour of the business name in the top navigation.
							  	</div>
							</div>
							
							<h2>Bottom Navigation</h2><br/>
							<div class="form-group">
								<label for="Top Background Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Top Background Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="topbackgroundcolour" value="<?php echo $topbackgroundcolour ?>">
							   	This should be a hex code e.g. #80dftg. This is the main colour on the bottom navigation panel.
							  	</div>
							</div>
							<div class="form-group">
								<label for="Bottom Background Colour" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Bottom Background Colour</label>
							   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							   	<input type="text" class="form-control" name="bottombackgroundcolour" value="<?php echo $bottombackgroundcolour ?>">
							   	This should be a hex code e.g. #80dftg. This is the bottom colour on the bottom navigation panel.
							  	</div>
							</div>
							
							
						</div>
						<div class="form-group">
					    	<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					    		<div style="text-align:center;">
					      		<button type='submit' name='submit' value='Submit' class="button-primary">Save</button>																									
					    		</div>
					    	</div>
					  	</div>
					</form>	
					
					<br/><br/><br/>
				</div>
	  		</div>
		</div>
			
	</div>	
	<?php include_once ('../footer.php'); ?>
</body>
