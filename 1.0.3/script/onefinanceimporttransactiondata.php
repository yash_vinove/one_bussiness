<?php

$date = date("Y-m-d");
$twelvemonths = strtotime ('-1 year', strtotime($date));
$twelvemonths = date ('Y-m-d' , $twelvemonths);
	              	
$getconfiguration = mysql_query("select * from $database.onefinanceconfiguration where disabled = 0");
$getconfigurationrow = mysql_fetch_array($getconfiguration);
$oneproductsalesimportactivated = $getconfigurationrow['oneproductsalesimportactivated'];
$oneproductsalesimportfinancesubcategoryid = $getconfigurationrow['oneproductsalesimportfinancesubcategoryid'];
$oneproductsalesimportfinancialbudgetid = $getconfigurationrow['oneproductsalesimportfinancialbudgetid'];
$oneproductsalesimportfallbacksalestaxid = $getconfigurationrow['oneproductsalesimportfallbacksalestaxid'];
$oneproductsalesimportfallbackfinancialaccountid = $getconfigurationrow['oneproductsalesimportfallbackfinancialaccountid'];

if($oneproductsalesimportactivated == 1){
	//check existing sold items to ensure their status has not changed back
	$getsolditemsunsold = mysql_query("select productstockitem.productstockitemid from $database.financialtransaction 
	inner join $database.productstockitem on productstockitem.productstockitemid = financialtransaction.productstockitemid	
	where financialtransaction.productstockitemid <> 0 and 
	(productstockitem.customerid = 0 or productstockitem.selldate = '0000-00-00')");
	while($row22 = mysql_fetch_array($getsolditemsunsold)){
		$productstockitemid = $row22['productstockitemid'];
		echo $productstockitemid."<br/>";
		
		//delete financial transaction record as this item is no longer sold
		$deleterecord = mysql_query("delete from $database.financialtransaction where productstockitemid = '$productstockitemid'");
	}
	
	//check if onecustomerconfig table exists in the database
	$checktable = mysql_query("SELECT table_name
	FROM information_schema.tables
	WHERE table_schema = '$database'
	AND table_name = 'ecommercelisting'");
	$numrows = mysql_num_rows($checktable);

	//check existing sold items have not changed their details (only records sold within last 12 months)
	$checkdetails = "select financialtransactionid as 'FTfinancialtransactionid', productstockitem.productstockitemid as 'PSIproductstockitemid', 
	financialtransaction.businessunitid as 'FTbusinessunitid', financialtransaction.amount as 'FTamount', 
	financialtransaction.salestaxid as 'FTsalestaxid', financialtransaction.amountincludingtax as 'FTamountincludingtax', 
	financialtransaction.customerid as 'FTcustomerid',	financialtransaction.transactiondate as 'FTtransactiondate', 
	financialtransaction.financialbudgetperiodid as 'FTfinancialbudgetperiodid', financialtransaction.currencyid as 'FTcurrencyid', 
	financialtransaction.financialaccountid as 'FTfinancialaccountid', financialtransaction.productstockitemid as 'FTproductstockitemid',
	productstockitem.customerid as 'PSIcustomerid', productstockitem.businessunitid as 'PSIbusinessunitid', 
	productstockitem.selldate as 'PSIselldate', productstockitem.sellcurrencyid as 'PSIcurrencyid', productstockitem.selltaxrate as 'PSIselltaxrate',
	businessunit.countryid as 'PSIcountryid',";
	if($numrows >= 1){
		$checkdetails = $checkdetails." if(ecommercelisting.shippingprice is not null, ecommercelisting.shippingprice, 0) + productstockitem.sellprice as 
		'PSIamount', if(ecommercelisting.shippingprice is not null, ecommercelisting.shippingprice, 0)
		 + if(ecommercelisting.shippingtax is not null, ecommercelisting.shippingtax, 0) + productstockitem.sellpriceincludingtax 
		as 'PSIamountincludingtax'";	
	}
	else {
		$checkdetails = $checkdetails." productstockitem.sellprice as 'PSIamount', productstockitem.sellpriceincludingtax 
		as 'PSIamountincludingtax'";	
	}
	$checkdetails = $checkdetails."from $database.financialtransaction 
	inner join $database.productstockitem on productstockitem.productstockitemid = financialtransaction.productstockitemid
	inner join $database.businessunit on businessunit.businessunitid = productstockitem.businessunitid";
	if($numrows >= 1){
		$checkdetails = $checkdetails." left join $database.ecommercelisting on ecommercelisting.productstockitemid = 
		productstockitem.productstockitemid";	
	}
	$checkdetails = $checkdetails." where financialtransaction.productstockitemid <> 0 and productstockitem.selldate >= '$twelvemonths'";
	//echo $checkdetails;
	$checkdetails = mysql_query($checkdetails);
	while($row22 = mysql_fetch_array($checkdetails)){
		$PSIproductstockitemid = $row22['PSIproductstockitemid'];
		$PSIbusinessunitid = $row22['PSIbusinessunitid'];
		$PSIcustomerid = $row22['PSIcustomerid'];
		$PSItransactiondate = $row22['PSIselldate'];
		$PSIcurrencyid = $row22['PSIcurrencyid'];
		$PSIamount = $row22['PSIamount'];
		$PSIamountincludingtax = $row22['PSIamountincludingtax'];
		$getfinancialbudgetperiod = mysql_query("select * from $database.financialbudgetperiod 
		where financialbudgetid = '$oneproductsalesimportfinancialbudgetid' and startdate <= '$PSItransactiondate' 
		and enddate >= '$PSItransactiondate'");
		$getfinancialbudgetperiodrow = mysql_fetch_array($getfinancialbudgetperiod);
		$PSIfinancialbudgetperiodid = $getfinancialbudgetperiodrow['financialbudgetperiodid'];
		$getconfigbu = mysql_query("select * from $database.onefinanceconfigurationbusinessunit where businessunitid = '$PSIbusinessunitid'");
		if(mysql_num_rows($getconfigbu) >= 1){
			$row45 = mysql_fetch_array($getconfigbu);
			$PSIsalestaxid = $row45['salestaxid'];
			$PSIfinancialaccountid = $row45['financialaccountid'];
		}
		else {
			$PSIselltaxrate = $row22['PSIselltaxrate'];
			$PSIcountryid = $row22['PSIcountryid'];
			$gettaxrate = "select * from $database.salestax where taxratepercentage = '$PSIselltaxrate' and countryid = '$PSIcountryid'";
			$gettaxrate = mysql_query($gettaxrate);
			$gettaxraterow = mysql_fetch_array($gettaxrate);
			$PSIsalestaxid = $gettaxraterow['salestaxid'];
			if($PSIsalestaxid == 0){
				$PSIsalestaxid = 	$oneproductsalesimportfallbacksalestaxid;
			}
			$getfinancialaccount = "select * from $database.financialaccount where countryid = '$PSIcountryid'";
			$getfinancialaccount = mysql_query($getfinancialaccount);
			$getfinancialaccountrow = mysql_fetch_array($getfinancialaccount);
			$PSIfinancialaccountid = $getfinancialaccountrow['financialaccountid'];
			if($PSIfinancialaccountid == 0){
				$PSIfinancialaccountid = 	$oneproductsalesimportfallbackfinancialaccountid;
			}
		}
		$FTfinancialtransactionid = $row22['FTfinancialtransactionid'];
		$FTproductstockitemid = $row22['FTproductstockitemid'];
		$FTbusinessunitid = $row22['FTbusinessunitid'];
		$FTcustomerid = $row22['FTcustomerid'];
		$FTtransactiondate = $row22['FTtransactiondate'];
		$FTcurrencyid = $row22['FTcurrencyid'];
		$FTamount = $row22['FTamount'];
		$FTamountincludingtax = $row22['FTamountincludingtax'];
		$FTfinancialbudgetperiodid = $row22['FTfinancialbudgetperiodid'];
		$FTsalestaxid = $row22['FTsalestaxid'];
		$FTfinancialaccountid = $row22['FTfinancialaccountid'];
		echo "CHECK RECORD HAS NOT CHANGED:<br/>";		
		echo "recordid: ".$FTfinancialtransactionid." - ".$PSIproductstockitemid."<br/>";
		echo "businessunitid: ".$FTbusinessunitid." - ".$PSIbusinessunitid."<br/>";
		echo "customerid: ".$FTcustomerid." - ".$PSIcustomerid."<br/>";
		echo "transactiondate: ".$FTtransactiondate." - ".$PSItransactiondate."<br/>";
		echo "currencyid: ".$FTcurrencyid." - ".$PSIcurrencyid."<br/>";
		echo "amount: ".$FTamount." - ".$PSIamount."<br/>";
		echo "amountincludingtax: ".$FTamountincludingtax." - ".$PSIamountincludingtax."<br/>";
		echo "financialbudgetperiodid: ".$FTfinancialbudgetperiodid." - ".$PSIfinancialbudgetperiodid."<br/>";
		echo "salestaxid: ".$FTsalestaxid." - ".$PSIsalestaxid."<br/>";
		echo "financialaccountid: ".$FTfinancialaccountid." - ".$PSIfinancialaccountid."<br/>";
		echo "<br/>";
		
		if($FTbusinessunitid <> $PSIbusinessunitid || $FTcustomerid <> $PSIcustomerid || $FTtransactiondate <> $PSItransactiondate || 
		$FTcurrencyid <> $PSIcurrencyid || $FTamount <> $PSIamount || $FTamountincludingtax <> $PSIamountincludingtax || 
		$FTfinancialbudgetperiodid <> $PSIfinancialbudgetperiodid || $FTsalestaxid <> $PSIsalestaxid ||
		$FTfinancialaccountid <> $PSIfinancialaccountid){
			$updatefinancialtransaction = mysql_query("update $database.financialtransaction set 
			businessunitid = '$PSIbusinessunitid', customerid = '$PSIcustomerid', transactiondate = '$PSItransactiondate', 
			currencyid = '$PSIcurrencyid', amount = '$PSIamount', amountincludingtax = '$PSIamountincludingtax', 
			financialbudgetperiodid = '$PSIfinancialbudgetperiodid', salestaxid = '$PSIsalestaxid', financialaccountid = '$PSIfinancialaccountid' 
			where financialtransaction.productstockitemid = '$PSIproductstockitemid'");		
		}
	}
		
	//get newly sold items that have not been copied across before
	$getproductstockitem = "select  productstockitem.productstockitemid as 'PSIproductstockitemid', 
	productstockitem.customerid as 'PSIcustomerid', productstockitem.businessunitid as 'PSIbusinessunitid', 
	productstockitem.selldate as 'PSIselldate', productstockitem.sellcurrencyid as 'PSIcurrencyid', productstockitem.selltaxrate as 'PSIselltaxrate',
	businessunit.countryid as 'PSIcountryid',";
	if($numrows >= 1){
		$getproductstockitem = $getproductstockitem." if(ecommercelisting.shippingprice is not null, ecommercelisting.shippingprice, 0) + productstockitem.sellprice as 
		'PSIamount', if(ecommercelisting.shippingprice is not null, ecommercelisting.shippingprice, 0)
		 + if(ecommercelisting.shippingtax is not null, ecommercelisting.shippingtax, 0) + productstockitem.sellpriceincludingtax 
		as 'PSIamountincludingtax'";	
	}
	else {
		$getproductstockitem = $getproductstockitem." productstockitem.sellprice as 'PSIamount', productstockitem.sellpriceincludingtax 
		as 'PSIamountincludingtax'";	
	}
	$getproductstockitem = $getproductstockitem."from $database.productstockitem 
	left join $database.financialtransaction on financialtransaction.productstockitemid = productstockitem.productstockitemid
	inner join $database.businessunit on businessunit.businessunitid = productstockitem.businessunitid";
	if($numrows >= 1){
		$getproductstockitem = $getproductstockitem." left join $database.ecommercelisting on ecommercelisting.productstockitemid = 
		productstockitem.productstockitemid";	
	}
	$getproductstockitem = $getproductstockitem." where (productstockitem.customerid <> 0 or productstockitem.selldate <> '0000-00-00') 
	and financialtransaction.productstockitemid is null";
	$getproductstockitem = mysql_query($getproductstockitem);
	while($row90=mysql_fetch_array($getproductstockitem)){
		//get variables
		$PSIproductstockitemid = $row90['PSIproductstockitemid'];
		$PSIbusinessunitid = $row90['PSIbusinessunitid'];
		$PSIcustomerid = $row90['PSIcustomerid'];
		$PSItransactiondate = $row90['PSIselldate'];
		$PSIcurrencyid = $row90['PSIcurrencyid'];
		$PSIamount = $row90['PSIamount'];
		$PSIamountincludingtax = $row90['PSIamountincludingtax'];
		$getfinancialbudgetperiod = mysql_query("select * from $database.financialbudgetperiod 
		where financialbudgetid = '$oneproductsalesimportfinancialbudgetid' and startdate <= '$PSItransactiondate' 
		and enddate >= '$PSItransactiondate'");
		$getfinancialbudgetperiodrow = mysql_fetch_array($getfinancialbudgetperiod);
		$PSIfinancialbudgetperiodid = $getfinancialbudgetperiodrow['financialbudgetperiodid'];
		$getconfigbu = mysql_query("select * from $database.onefinanceconfigurationbusinessunit where businessunitid = '$PSIbusinessunitid'");
		if(mysql_num_rows($getconfigbu) >= 1){
			$row45 = mysql_fetch_array($getconfigbu);
			$PSIsalestaxid = $row45['salestaxid'];
			$PSIfinancialaccountid = $row45['financialaccountid'];
		}
		else {
			$PSIselltaxrate = $row90['PSIselltaxrate'];
			$PSIcountryid = $row90['PSIcountryid'];
			$gettaxrate = "select * from $database.salestax where taxratepercentage = '$PSIselltaxrate' and countryid = '$PSIcountryid'";
			$gettaxrate = mysql_query($gettaxrate);
			$gettaxraterow = mysql_fetch_array($gettaxrate);
			$PSIsalestaxid = $gettaxraterow['salestaxid'];
			if($PSIsalestaxid == 0){
				$PSIsalestaxid = 	$oneproductsalesimportfallbacksalestaxid;
			}
			$getfinancialaccount = "select * from $database.financialaccount where countryid = '$PSIcountryid'";
			$getfinancialaccount = mysql_query($getfinancialaccount);
			$getfinancialaccountrow = mysql_fetch_array($getfinancialaccount);
			$PSIfinancialaccountid = $getfinancialaccountrow['financialaccountid'];
			if($PSIfinancialaccountid == 0){
				$PSIfinancialaccountid = 	$oneproductsalesimportfallbackfinancialaccountid;
			}
		}
		echo "ADD NEW RECORD:<br/>";		
		echo "recordid: ".$PSIproductstockitemid."<br/>";
		echo "businessunitid: ".$PSIbusinessunitid."<br/>";
		echo "customerid: ".$PSIcustomerid."<br/>";
		echo "transactiondate: ".$PSItransactiondate."<br/>";
		echo "currencyid: ".$PSIcurrencyid."<br/>";
		echo "amount: ".$PSIamount."<br/>";
		echo "amountincludingtax: ".$PSIamountincludingtax."<br/>";
		echo "financialbudgetperiodid: ".$PSIfinancialbudgetperiodid."<br/>";
		echo "salestaxid: ".$PSIsalestaxid."<br/>";
		echo "financialaccountid: ".$PSIfinancialaccountid."<br/>";
		echo "<br/>";
		
		//create new record
		$financialtransactionname = "OneProductSales ".$PSIcustomerid." ".$PSIproductstockitemid;
		$createfintran = mysql_query("insert into $database.financialtransaction (financialtransactionname, financialbudgetperiodid, 
		businessunitid, financesubcategoryid, currencyid, amount,		salestaxid, amountincludingtax, customerid, supplierid, transactiondate, financialtransactionstatusid,		financialaccountid, recurfinancialtransactionid, disabled, datecreated, masteronly, productstockitemid) 
		values ('$financialtransactionname', '$PSIfinancialbudgetperiodid', '$PSIbusinessunitid', 
		'$oneproductsalesimportfinancesubcategoryid', '$PSIcurrencyid', '$PSIamount', '$PSIsalestaxid', '$PSIamountincludingtax', 
		'$PSIcustomerid', '0', '$PSItransactiondate', '5', '$PSIfinancialaccountid', '0', '0', 
		'$date', '0', '$PSIproductstockitemid')");
	}	
}

?>