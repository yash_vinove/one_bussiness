<?php session_start(); ?>
<?php include('../config.php'); ?>
<?php include('../sessionconfig.php'); ?>
<?php

require('../fpdf181/fpdf.php');

	class PDF extends FPDF
	{
		
	// Page header
	function Header()
	{
	    // Logo
	    $this->Image('../fpdf181/logo.png',80,10,50);
	    // Line break
	    $this->Ln(15);
	    // Arial bold 15
	    $this->SetFont('Arial','B',13);
	}
	
	
	// Page footer
	function Footer()
	{
	    // Position at 1.5 cm from bottom
	    $this->SetY(-15);
	    // Arial italic 8
	    $this->SetFont('Arial','I',8);
	    // Page number
	    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	}
	
	}

$rowid = isset($_GET['rowid']) ? strip_tags($_GET['rowid']) : '';

$querybuilder = mysql_query("select * from $masterdatabase.accountbillinginvoice 
left join tenant on tenant.tenantid = accountbillinginvoice.tenantid
left join accountbillingcurrency on accountbillingcurrency.accountbillingcurrencyid = accountbillinginvoice.accountbillingcurrencyid
where accountbillinginvoiceid = '$rowid'");
while($rowinvoice=mysql_fetch_array($querybuilder)){
	
	
	//get needed variables
	$accountbillinginvoicename = $rowinvoice['accountbillinginvoicename'];
	//echo $accountbillinginvoicename;
	$accountbillinginvoiceid = $rowinvoice['accountbillinginvoiceid'];
	$tenantname = $rowinvoice['tenantname'];
	$invoicedate = $rowinvoice['invoicedate'];
	$currencycode = $rowinvoice['currencycode'];
	$totalrate = $rowinvoice['totalrate'];
	$taxpercentage = $rowinvoice['taxpercentage'];
	$totalrateincludingtax = $rowinvoice['totalrateincludingtax'];
	$tenantfirstname = $rowinvoice['firstname'];
	$tenantlastname = $rowinvoice['lastname'];
	$tenantemailaddress = $rowinvoice['emailaddress'];
	$taxvalue = $totalrateincludingtax-$totalrate;
	$tenantperson = $tenantfirstname." ".$tenantlastname;
	
	$date = date("d-M-Y");
	$invoicedate = date("d-M-Y", strtotime($invoicedate));
	$month = date("M Y", strtotime($invoicedate .'-1 months'));
	
	// Instantiation of inherited class
	$pdf = new PDF();
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->SetFont('Arial','B',13);
	$pdf->Cell(75);
   	$pdf->Cell(40,10,'Invoice - '.$tenantname,0,0,'C');
   	$pdf->Ln(15);
	$pdf->SetFont('Arial','',10);
	$pdf->MultiCell(0,5,'Invoice Number: '.$accountbillinginvoiceid,0,1);
	$pdf->MultiCell(0,5,'Invoice Date: '.$invoicedate,0,1);
	$pdf->MultiCell(0,5,'Business: '.$tenantname,0,1);
	$pdf->MultiCell(0,5,'For the attention of: '.$tenantperson,0,1);
	$pdf->Ln(5);
	$pdf->SetFont('Arial','',10);
	$pdf->MultiCell(0,5,'This is an invoice for the use of your Tristom Labs Ltd technology. If you have any queries please contact Tristom Labs on the contact details at the bottom of the invoice. Payment is due within 30 days of the date on the invoice. Terms and conditions apply, and are available upon request.',0,1);
	$pdf->Ln(5);
	$pdf->SetFont('Arial','U',12);
	$pdf->Cell(0,10,'Invoice Breakdown',0,1);
	$pdf->SetFont('Arial','B',10);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$width = 130;
	$pdf->MultiCell($width, 6, 'Item', 1, 'L', FALSE);
	$pdf->SetXY($x + $width, $y);
	$pdf->Cell(40,6, 'Cost', 1, 0, "l");	
	$pdf->Ln(6);
	//iterate through the business records
	$querybuilder2 = mysql_query("select * from andrewnorth_onebusinessmaster.accountbillinginvoicebreakdown 
	where accountbillinginvoiceid = '$accountbillinginvoiceid'");
	while($rowinvoice2=mysql_fetch_array($querybuilder2)){
		//get needed variables
		$businessname = $rowinvoice2['businessname'];
		$additionalcostdescription = $rowinvoice2['additionalcostdescription'];
		$additionalamount = $rowinvoice2['additionalamount'];
		$pdf->SetFont('Arial','',10);
		$y = $pdf->GetY();
		$x = $pdf->GetX();
		$width = 130;
		$pdf->MultiCell($width, 6, $additionalcostdescription, 1, 'L', FALSE);
		$pdf->SetXY($x + $width, $y);
		$pdf->Cell(40,6, $currencycode.' '.number_format($additionalamount, 2), 1, 0, "l");	
		$pdf->Ln(6);
	}
	$pdf->SetFont('Arial','B',10);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$width = 130;
	$pdf->MultiCell($width, 6, 'Grand Total', 1, 'L', FALSE);
	$pdf->SetXY($x + $width, $y);
	$pdf->Cell(40,6, $currencycode.' '.number_format($totalrate, 2), 1, 0, "l");	
	$pdf->Ln(6);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$width = 130;
	$pdf->MultiCell($width, 6, 'Tax Due ('.$taxpercentage.'%)', 1, 'L', FALSE);
	$pdf->SetXY($x + $width, $y);
	$pdf->Cell(40,6, $currencycode.' '.number_format($taxvalue, 2), 1, 0, "l");	
	$pdf->Ln(6);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$width = 130;
	$pdf->MultiCell($width, 6, 'Grand Total Including Tax / Total Due', 1, 'L', FALSE);
	$pdf->SetXY($x + $width, $y);
	$pdf->Cell(40,6, $currencycode.' '.number_format($totalrateincludingtax, 2), 1, 0, "l");	
	$pdf->Ln(6);
	
	
	$pdf->Ln(6);
	$pdf->SetFont('Arial','U',12);
	$pdf->Cell(0,10,'Payment Details',0,1);
	$pdf->SetFont('Arial','',10);
	$pdf->MultiCell(0,5,'Make all cheques payable to Tristom Labs Ltd, write the invoice number on the back of the cheque, and send to address XXX, XXX, XXX, XXX, XXX.',0,1);
	$pdf->Ln(5);
	$pdf->MultiCell(0,5,'Or send payment via transfer to Tristom Labs Ltd, Account Number 12345678, Sort Code 454545',0,1);
	
	//uncomment this, if you want to see an example in the browser for testing. However, comment it out once you are finished otherwise the whilst loop will not run.
	//$pdf->Output();
	$filename="../../documents/".$database."/Tristom Labs Ltd Invoice - ".$accountbillinginvoiceid.".pdf";
	$pdf->Output($filename,'F');

	$updateinvoiceasgenerated = mysql_query("update $masterdatabase.accountbillinginvoice set accountbillinginvoicestatusid = '1'
	where accountbillinginvoiceid = '$rowid'");
	
}

//redirect back to the page
$url = '../pagegrid.php?pagetype=accountbillinginvoice&updated=2';
echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">';   				


?>