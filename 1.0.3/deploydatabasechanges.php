<?php

//INSTRUCTIONS FOR LOCAL
//Just run url: http://localhost:8888/onebusiness/1.0.3/deploydatabasechanges.php?deployto=local&appsetupapplication=[appid]


//INSTRUCTIONS FOR LIVE
//Go to live onebusinessmaster and export sql backup of tenant, accountbilling tables, and business
//Also do a full backup of live database, just in case something goes wrong
//Save the export somewhere safe
//Drop all live onebusinessmaster tables
//Export the local onebusinessmaster database
//Import local onebusinessmaster database into the live environment
//Delete the tenant, billing tables, and business tables from live
//Run the saved onebusinessmaster backup of tenant, billing tables, and business to reinstall them onto the live system
//Then run url: http://localhost:8888/onebusiness/1.0.3/deploydatabasechanges.php?deployto=live&appsetupapplication=[appid]

$deployto = isset($_GET['deployto']) ? $_GET['deployto'] : '';
$appsetupapplication = isset($_GET['appsetupapplication']) ? $_GET['appsetupapplication'] : '';

//connect to database - localhost
$user_name = "root";
$password = "root";
$server = "localhost";	
$database = "onebusinessmaster";   

$db_handle = @mysql_connect($server, $user_name, $password, TRUE);
$db_found = @mysql_select_db($database, $db_handle);

$tenantdatabaselocal = array();
$tenantdatabaselive = array();
$businessdatabaselocal = array();
$businessdatabaselive = array();

if($deployto == "local"){
	$gettenant = mysql_query("select * from $database.tenant");
	while ($rowtenant = mysql_fetch_array($gettenant)){
		$tenantid = $rowtenant['tenantid'];
		$tenantdatabase = "onebusinesstenant".$tenantid;	
		array_push($tenantdatabaselocal, $tenantdatabase);
		$db_handle = @mysql_connect($server, $user_name, $password, TRUE);
		$db_found = @mysql_select_db($tenantdatabase, $db_handle);
		$checktable = mysql_query("SELECT table_name
		FROM information_schema.tables
		WHERE table_schema = '$tenantdatabase'
		AND table_name = 'business'");
		if(mysql_num_rows($checktable) >= 1){
			$numrows = mysql_num_rows($checktable);
			$getbusinesses = mysql_query("select * from $tenantdatabase.business");
			while ($rowbusiness = mysql_fetch_array($getbusinesses)){
				$databasename = $rowbusiness['databasename'];
				
				//check if they have the application installed
				$checktable = mysql_query("SELECT table_name
				FROM information_schema.tables
				WHERE table_schema = '$databasename'
				AND table_name = 'appsetupapplicationbusiness'");
				$numrows = mysql_num_rows($checktable);
				if($numrows >= 1){
					$getapplication = mysql_query("select * from $databasename.appsetupapplicationbusiness
					where appsetupapplicationid = '$appsetupapplication'");
					if(mysql_num_rows($getapplication)){
						array_push($businessdatabaselocal, $databasename);	
					}
				}
				if($appsetupapplication == 2)	{
					array_push($businessdatabaselocal, $databasename);	
				}
			}
		}
	}
}

//echo "tenantdatabaselocal: ".json_encode($tenantdatabaselocal)."<br/><br/>";
//echo "businessdatabaselocal: ".json_encode($businessdatabaselocal)."<br/><br/>";

//connect to database - live
$user_name = "andynort_12345";
$password = "UO1X4,L9RvbB";
$server = "103.67.235.116";   
$database = "andynort_onebusinessmaster";  

$db_handle = @mysql_connect($server, $user_name, $password, TRUE);
$db_found = @mysql_select_db($database, $db_handle);

if($deployto == 'live'){
	$gettenant = mysql_query("select * from $database.tenant");
	echo "Note for Live Connection: If this does not work then add your local IP address to Remote MySQL list.<br/>";
	while ($rowtenant = mysql_fetch_array($gettenant)){
		$tenantid = $rowtenant['tenantid'];
		$tenantdatabase = "andynort_onebusinesstenant".$tenantid;	
		array_push($tenantdatabaselive, $tenantdatabase);
		$db_handle = @mysql_connect($server, $user_name, $password, TRUE);
		$db_found = @mysql_select_db($tenantdatabase, $db_handle);
		$checktable = mysql_query("SELECT table_name
		FROM information_schema.tables
		WHERE table_schema = '$tenantdatabase'
		AND table_name = 'business'");
		if(mysql_num_rows($checktable) >= 1){
			$numrows = mysql_num_rows($checktable);
			$getbusinesses = mysql_query("select * from $tenantdatabase.business");
			while ($rowbusiness = mysql_fetch_array($getbusinesses)){
				$databasename = $rowbusiness['databasename'];
				$databasename = 'andynort_'.$databasename;
				//echo $databasename;
				//check if they have the application installed
				$checktable = mysql_query("SELECT table_name
				FROM information_schema.tables
				WHERE table_schema = '$databasename'
				AND table_name = 'appsetupapplicationbusiness'");
				$numrows = mysql_num_rows($checktable);
				if($numrows >= 1){
					$getapplication = mysql_query("select * from $databasename.appsetupapplicationbusiness
					where appsetupapplicationid = '$appsetupapplication'");
					if(mysql_num_rows($getapplication)){
						array_push($businessdatabaselive, $databasename);	
					}
				}
				if($appsetupapplication == 2)	{
					array_push($businessdatabaselive, $databasename);	
				}	
			}
		}
	}
}

//echo "tenantdatabaselive: ".json_encode($tenantdatabaselive)."<br/><br/>";
//echo "businessdatabaselive: ".json_encode($businessdatabaselive)."<br/><br/>";

//GET FINAL DATABASE ARRAY
if($appsetupapplication == 1 && $deployto == "local"){
	$databasearray = $tenantdatabaselocal;
}
if($appsetupapplication == 1 && $deployto == "live"){
	$databasearray = $tenantdatabaselive;
}
if($appsetupapplication <> 1 && $deployto == "local"){
	$databasearray = $businessdatabaselocal;
}
if($appsetupapplication <> 1 && $deployto == "live"){
	$databasearray = $businessdatabaselive;
}
echo "finaldatabaselist: ".json_encode($databasearray)."<br/><br/>";
if($deployto == "local"){
	$masterdatabase = 'onebusinessmaster';
}
if($deployto == "live"){
	$masterdatabase = 'andynort_onebusinessmaster';
}


//GET ALL OF THE APPLICATION DETAILS
//connect to database - localhost
if($deployto == 'local'){
	$user_name = "root";
	$password = "root";
	$server = "localhost";	
	$database = "onebusinessmaster"; 
}
else {
	$user_name = "andynort_12345";
	$password = "UO1X4,L9RvbB";
	$server = "103.67.235.116";   
	$database = "andynort_onebusinessmaster"; 
}  
$db_handle = @mysql_connect($server, $user_name, $password, TRUE);
$db_found = @mysql_select_db($database, $db_handle);
$getapplication = mysql_query("select * from $database.appsetupapplication 
where appsetupapplicationid = '$appsetupapplication'");
$getapplicationrow = mysql_fetch_array($getapplication);
$databasetables = $getapplicationrow['databasetables'];
$businessgridpages = $getapplicationrow['businessgridpages'];
$businessnavigationsections = $getapplicationrow['businessnavigationsections'];
$businesspermissionroles = $getapplicationrow['businesspermissionroles'];
$businessdashboardsections = $getapplicationrow['businessdashboardsections'];
$businessreportsections = $getapplicationrow['businessreportsections'];
$businessviews = $getapplicationrow['businessviews'];
$businessscripts = $getapplicationrow['businessscripts'];
$businessemailcontents = $getapplicationrow['businessemailcontents'];
$businessrequestmanagerconfigurations = $getapplicationrow['businessrequestmanagerconfigurations'];


foreach($databasearray as $databasename) {
	echo "<b>Run Updates For: ".$databasename."</b><br/>";
	
	//insert databasetables
	$array = explode(',', $databasetables); 
	foreach($array as $value) 
	{
	    echo "database table inserted, if missing: ".$value."<br/>";
	    $query2 = mysql_query("create table ".$databasename.".".$value." like ".$masterdatabase.".".$value);
		$checktable = mysql_query("SELECT table_name
		FROM information_schema.tables
		WHERE table_schema = '$databasename'
		AND table_name = '$value'");
		$numrows = mysql_num_rows($checktable);
		if($numrows == 0){
			//automatically set the minimum id to 100,000 so that clients records don't override onebusiness records
		    $insertdummy = "insert into ".$databasename.".".$value." (".$value."id) values (1000000)";
		    //echo $insertdummy."<br/>";
		    $insertdummy = mysql_query($insertdummy);
		    $deletedummy = mysql_query("delete from ".$databasename.".".$value." where ".$value."id = 1000000");
	    } 
	}
	
	//insert grid pages and associated databasetables	
	$array = explode(',', $businessgridpages); 
	foreach($array as $value) 
	{
	   	$getstructurefunction = mysql_query("select * from $masterdatabase.structurefunction where structurefunctionid = '$value'");
	  	$getstructurefunctionrow = mysql_fetch_array($getstructurefunction);
	  	$tablename = $getstructurefunctionrow['tablename'];
		
	    
	   	//CHECK 1 - create database table
	   	$checktable = mysql_query("SELECT table_name
		FROM information_schema.tables
		WHERE table_schema = '$databasename'
		AND table_name = '$tablename'");
		$numrows = mysql_num_rows($checktable);
		if($numrows == 0){
			$query2 = mysql_query("create table ".$databasename.".".$tablename." like ".$masterdatabase.".".$tablename);
	   		//automatically set the minimum id to 100,000 so that clients records don't override onebusiness records
		   	$insertdummy = "insert into ".$databasename.".".$tablename." (".$tablename."id) values (1000000)";
		  	//echo $insertdummy."<br/>";
		  	$insertdummy = mysql_query($insertdummy);
		  	$deletedummy = mysql_query("delete from ".$databasename.".".$tablename." where ".$tablename."id = 1000000");
		  	echo "add table: ".$value."-".$tablename."<br/>";
	  	}
	  	else {
	  		echo "update table: ".$value."-".$tablename."<br/>";
			//check for missing columns and add them in
			//get list of columns
			$getcolumns1 = mysql_query("select * from $masterdatabase.structurefield where structurefunctionid = '$value'");
			while($row22 = mysql_fetch_array($getcolumns1)){
				$fieldname = $row22['structurefieldname'];
				$fieldtypename = $row22['structurefieldtypeid'];
				$item45 = $databasename."/".$tablename;
				$checktable = "select *
         		from INFORMATION_SCHEMA.INNODB_SYS_COLUMNS
         		inner join INFORMATION_SCHEMA.INNODB_SYS_TABLES on INNODB_SYS_COLUMNS.TABLE_ID = INNODB_SYS_TABLES.TABLE_ID
         		where INNODB_SYS_TABLES.name = '$item45' and INNODB_SYS_COLUMNS.name = '$fieldname'";	
         		//echo $checktable;  	
         		$checktable = mysql_query($checktable);
	  			if(mysql_num_rows($checktable) == 0){
	  				//insert the field into the database table 
					//add the text/value/tableconnect field to the table
					if($fieldtypename == 1){
						$insertfield = "ALTER TABLE ".$databasename.".".$tablename." ADD ".$fieldname." TEXT not null";	
						//echo $insertfield;
						$insertfield = mysql_query($insertfield);
					}
					
					//add the text/value/tableconnect field to the table
					if($fieldtypename == 2){
						$insertfield = "ALTER TABLE ".$databasename.".".$tablename." ADD ".$fieldname." DOUBLE not null";	
						//echo $insertfield;
						$insertfield = mysql_query($insertfield);
					}
					
					//add the dateselect field to the table
					if($fieldtypename == 3){
						$insertfield = "ALTER TABLE ".$databasename.".".$tablename." ADD ".$fieldname." DATE not null";
						//echo $insertfield;
						$insertfield = mysql_query($insertfield);
					}
					
					//add the status/uniquetableid/checkbox field to the table
					if($fieldtypename == 5 || $fieldtypename == 6 || $fieldtypename == 7 || $fieldtypename == 4){
						$insertfield = "ALTER TABLE ".$databasename.".".$tablename." ADD ".$fieldname." INT not null";
						//echo $insertfield;
						$insertfield = mysql_query($insertfield);
					}
					echo "added field to database: ".$fieldname."<br/>";	  			
	  			}
			}
	  	}
	    
	   	//CHECK 2 - create structurefunction table content
	   	$deletestructurefunction = mysql_query("delete from $databasename.structurefunction 
	   	where structurefunctionid = '$value'");
	   	$insertstructurefieldtypes = mysql_query("INSERT INTO $databasename.structurefunction (structurefunctionid, 
	   	structurefunctionname, tablename, 
		primarykeyfield, gridorderfield, gridorderdirection, datefilterfield, uploaderactive, runscriptadd, runscriptedit, 
		runscriptdisable, runscriptenable, runscriptextra1name, runscriptextra1page, runscriptextra2name, runscriptextra2page, 
		runscriptextra3name, runscriptextra3page, 
		runscriptextra4name, runscriptextra4page, runscriptextra5name, runscriptextra5page, addhelptext, edithelptext, 
		gridhelptext, mastercontrol, tenantcontrol, businesscontrol, disabled, datecreated, masteronly, allowdocupload, doctype, 
		runscriptdocupload, runscriptdocdelete)
		SELECT structurefunctionid, structurefunctionname, tablename, 
		primarykeyfield, gridorderfield, gridorderdirection, datefilterfield, uploaderactive, runscriptadd, runscriptedit, 
		runscriptdisable, runscriptenable, 
		runscriptextra1name, runscriptextra1page, runscriptextra2name, runscriptextra2page, runscriptextra3name, runscriptextra3page, 
		runscriptextra4name, runscriptextra4page, runscriptextra5name, runscriptextra5page,
		addhelptext, edithelptext, gridhelptext, mastercontrol, tenantcontrol, businesscontrol, disabled, datecreated, masteronly, 
		allowdocupload, doctype, runscriptdocupload, runscriptdocdelete
		FROM $masterdatabase.structurefunction
		where structurefunctionid = '$value'");
		
		//CHECK 3 - create structurefield table content
		//get list of existing structurefunctionpages in onebusinessmaster
		$getstructurefields = mysql_query("select * from $masterdatabase.structurefield where structurefunctionid = '$value'");
		while($row89 = mysql_fetch_array($getstructurefields)){
			//check page exists in business database
			$structurefieldid = $row89['structurefieldid'];
			$getstructurefield = mysql_query("select * from $databasename.structurefield where structurefieldid = '$structurefieldid'");
			if(mysql_num_rows($getstructurefield) >= 1){
				//delete record if does exist	
				$deletestructurefield = mysql_query("delete from $databasename.structurefield
				where structurefieldid = '$structurefieldid'");
				echo "update field: ".$structurefieldid."<br/>";
			}
			else {
				echo "add field: ".$structurefieldid."<br/>";
			}
			//then add record back in again
			$insertstructurefieldtypes = mysql_query("INSERT INTO $databasename.structurefield (structurefieldid, structurefunctionid, 
			structurefieldtypeid, structurefieldname, displayname, fieldposition, showingrid, gridposition, 
			requiredfield, textlimit, tableconnect, tablefieldconnect, lowvalue, highvalue, helptext, keywordsearchable, uniqueval, 
			noedit, mastercontrol, tenantcontrol, businesscontrol, disabled, datecreated, masteronly)
			SELECT structurefieldid, structurefunctionid, structurefieldtypeid, structurefieldname, displayname, fieldposition, 
			showingrid, gridposition, 
			requiredfield, textlimit, tableconnect, tablefieldconnect, lowvalue, highvalue, helptext, keywordsearchable, uniqueval, 
			noedit, mastercontrol, tenantcontrol, businesscontrol, disabled, datecreated, masteronly
			FROM $masterdatabase.structurefield
			where structurefieldid = '$structurefieldid'");								
		}
			
		//CHECK 4 - create structurefunctionpage table content
		$deletestructurefunctionpage = mysql_query("delete from $databasename.structurefunctionpage 
	   	where structurefunctionid = '$value'");
	   	$insertstructurefunctionpage = mysql_query("INSERT INTO $databasename.structurefunctionpage (structurefunctionpageid, 
		structurefunctionpagename, disabled, datecreated, navigationposition, structurefunctionsectionid, viewid, structurefunctionpagetypeid,
		structurefunctionid, documentationpagedescription, masteronly)
		SELECT structurefunctionpageid, structurefunctionpagename, disabled, datecreated, 
		navigationposition, structurefunctionsectionid, viewid, structurefunctionpagetypeid, structurefunctionid, documentationpagedescription, 
		masteronly
		FROM $masterdatabase.structurefunctionpage
		where structurefunctionid = '$value'");
		
	}
	
	
	//update dashboards
	$array = explode(',', $businessdashboardsections); 
	foreach($array as $value4) 
	{
		echo "update dashboard id: ".$value4."<br/>";
		//create chartsection table content
		$deletechartsection = mysql_query("delete from $databasename.chartsection 
	   	where chartsectionid = '$value4'");
	   $insertchartsection = mysql_query("INSERT INTO 
		$databasename.chartsection (chartsectionid, chartsectionname, disabled, datecreated, sortorder, masteronly)
		SELECT chartsectionid, chartsectionname, disabled, datecreated, sortorder, masteronly
		FROM $masterdatabase.chartsection
		where chartsectionid = '$value4'"); 
		
		//create chartsectionfilter table content
		$deletechartsectionfilter = mysql_query("delete from $databasename.chartsectionfilter 
	   	where chartsectionid = '$value4'");
		$insertchartsectionfilter = mysql_query("INSERT INTO 
		$databasename.chartsectionfilter (dashboardsectionfilterid, dashboardsectionfiltername, structurefieldid, chartsectionid, 
		disabled, datecreated, masteronly)
		SELECT dashboardsectionfilterid, dashboardsectionfiltername, structurefieldid, chartsectionid, 
		disabled, datecreated, masteronly
		FROM $masterdatabase.chartsectionfilter
		where chartsectionid = '$value4'"); 
		
		//create chart table content
		$deletechart = mysql_query("delete from $databasename.chart
	   	where chartsectionid = '$value4'");
		$insertchart = mysql_query("INSERT INTO 
		$databasename.chart (chartid, chartname, disabled, datecreated, chartsectionid, datasetid, charttypeid, chartdescription, masteronly)
		SELECT chartid, chartname, disabled, datecreated, chartsectionid, datasetid, charttypeid, chartdescription, masteronly
		FROM $masterdatabase.chart
		where chartsectionid = '$value4'");
		
		//create dashboardsectionfilter table content
		$deletedashboardsectionfilter = mysql_query("delete from $databasename.dashboardsectionfilter
	   	where chartsectionid = '$value4'");
		$insertdashboardsectionfilter = mysql_query("INSERT INTO 
		$databasename.dashboardsectionfilter (dashboardsectionfilterid, dashboardsectionfiltername, structurefieldid, chartsectionid, 
		disabled, datecreated, masteronly)
		SELECT dashboardsectionfilterid, dashboardsectionfiltername, structurefieldid, chartsectionid, 
		disabled, datecreated, masteronly
		FROM $masterdatabase.dashboardsectionfilter
		where chartsectionid = '$value4'");
		
		//create dataset table content
		$getdatasets = mysql_query("select * from $databasename.chart where chartsectionid = '$value4'");
		while($row46 = mysql_fetch_array($getdatasets)){
			$datasetid = $row46['datasetid'];
			$chartid = $row46['chartid'];
			echo "update chart id: ".$chartid."<br/>";
			$deletedataset = mysql_query("delete from $databasename.dataset
	   		where datasetid = '$datasetid'");
			$insertdataset = mysql_query("INSERT INTO 
			$databasename.dataset (datasetid, datasetname, disabled, datecreated, sortbyfield, datefilterfield, invert, masteronly)
			SELECT datasetid, datasetname, disabled, datecreated, sortbyfield, datefilterfield, invert, masteronly
			FROM $masterdatabase.dataset
			where datasetid = '$datasetid'"); 
			
			//create datasetcolumn table content
			$deletedatasetcolumn = mysql_query("delete from $databasename.datasetcolumn
	   		where datasetid = '$datasetid'");
			$insertdatasetcolumns = mysql_query("INSERT INTO 
			$databasename.datasetcolumn (datasetcolumnid, datasetcolumnname, disabled, datecreated, datasetid, 
			structurefieldid, datasetaggregateid, format, sortorder, connectedtablename, hidecolumn, masteronly, calculationlogic)
			SELECT datasetcolumnid, datasetcolumnname, disabled, datecreated, datasetid, 
			structurefieldid, datasetaggregateid, format, sortorder, connectedtablename, hidecolumn, masteronly, calculationlogic
			FROM $masterdatabase.datasetcolumn
			where datasetid = '$datasetid'"); 
			
			//create datasetfilter table content
			$getdatasetcolumns = mysql_query("select * from $databasename.datasetcolumn where datasetid = '$datasetid'");
			while($row47 = mysql_fetch_array($getdatasetcolumns)){
				$datasetcolumnid = $row47['datasetcolumnid'];
				$deletedatasetcolumn = mysql_query("delete from $databasename.datasetfilter
	   			where datasetcolumnid = '$datasetcolumnid'");
				$insertdataset = mysql_query("INSERT INTO 
				$databasename.datasetfilter (datasetfilterid, datasetfiltername, disabled, datecreated, datasetcolumnid, datasetfilterlogicid, masteronly)
				SELECT datasetfilterid, datasetfiltername, disabled, datecreated, datasetcolumnid, datasetfilterlogicid, masteronly
				FROM $masterdatabase.datasetfilter
				where datasetcolumnid = '$datasetcolumnid'"); 
			}			
		}
	}
	
	
	//reports
	$array = explode(',', $businessreportsections); 
	foreach($array as $value5) 
	{
		echo "update report section id: ".$value5."<br/>";
		//create reportsection table content
		$deletereportsection = mysql_query("delete from $databasename.reportsection 
	   	where reportsectionid = '$value5'");
	   $insertreportsection = mysql_query("INSERT INTO 
		$databasename.reportsection (reportsectionid, reportsectionname, disabled, datecreated, sortorder, masteronly, defaultstartdate, defaultenddate)
		SELECT reportsectionid, reportsectionname, disabled, datecreated, sortorder, masteronly, defaultstartdate, defaultenddate
		FROM $masterdatabase.reportsection
		where reportsectionid = '$value5'"); 
		
		//create report table content
		$deletereport = mysql_query("delete from $databasename.report
	   	where reportsectionid = '$value5'");
		$insertreport = mysql_query("INSERT INTO 
		$databasename.report (reportid, reportname, disabled, datecreated, reportsectionid, datasetid, reportdescription, masteronly)
		SELECT reportid, reportname, disabled, datecreated, reportsectionid, datasetid, reportdescription, masteronly
		FROM $masterdatabase.report
		where reportsectionid = '$value5'");
		
		//create dataset table content
		$getdatasets = mysql_query("select * from $databasename.report where reportsectionid = '$value5'");
		while($row46 = mysql_fetch_array($getdatasets)){
			$datasetid = $row46['datasetid'];
			$reportid = $row46['reportid'];
			echo "update report id: ".$reportid."<br/>";
			$deletedataset = mysql_query("delete from $databasename.dataset
	   		where datasetid = '$datasetid'");
			$insertdataset = mysql_query("INSERT INTO 
			$databasename.dataset (datasetid, datasetname, disabled, datecreated, sortbyfield, datefilterfield, invert, masteronly)
			SELECT datasetid, datasetname, disabled, datecreated, sortbyfield, datefilterfield, invert, masteronly
			FROM $masterdatabase.dataset
			where datasetid = '$datasetid'"); 
			
			//create datasetcolumn table content
			$deletedatasetcolumn = mysql_query("delete from $databasename.datasetcolumn
	   		where datasetid = '$datasetid'");
			$insertdatasetcolumns = mysql_query("INSERT INTO 
			$databasename.datasetcolumn (datasetcolumnid, datasetcolumnname, disabled, datecreated, datasetid, 
			structurefieldid, datasetaggregateid, format, sortorder, connectedtablename, hidecolumn, masteronly, calculationlogic)
			SELECT datasetcolumnid, datasetcolumnname, disabled, datecreated, datasetid, 
			structurefieldid, datasetaggregateid, format, sortorder, connectedtablename, hidecolumn, masteronly, calculationlogic
			FROM $masterdatabase.datasetcolumn
			where datasetid = '$datasetid'"); 
			
			//create datasetfilter table content
			$getdatasetcolumns = mysql_query("select * from $databasename.datasetcolumn where datasetid = '$datasetid'");
			while($row47 = mysql_fetch_array($getdatasetcolumns)){
				$datasetcolumnid = $row47['datasetcolumnid'];
				$deletedatasetcolumn = mysql_query("delete from $databasename.datasetfilter
	   			where datasetcolumnid = '$datasetcolumnid'");
				$insertdataset = mysql_query("INSERT INTO 
				$databasename.datasetfilter (datasetfilterid, datasetfiltername, disabled, datecreated, datasetcolumnid, datasetfilterlogicid, masteronly)
				SELECT datasetfilterid, datasetfiltername, disabled, datecreated, datasetcolumnid, datasetfilterlogicid, masteronly
				FROM $masterdatabase.datasetfilter
				where datasetcolumnid = '$datasetcolumnid'"); 
			}			
		}
	}


	//update views
	$array = explode(',', $businessviews); 
	foreach($array as $value6) 
	{
		//create view table content
		echo "update view id: ".$value6."<br/>";
		$deletedatasetcolumn = mysql_query("delete from $databasename.view
	   	where viewid = '$value6'");
		$insertview = mysql_query("INSERT INTO 
		$databasename.view (viewid, viewname, disabled, datecreated, masteronly)
		SELECT viewid, viewname, disabled, datecreated, masteronly
		FROM $masterdatabase.view
		where viewid = '$value6'"); 
	
		//create structurefunctionpage table content
		$deletestructurefunctionpage = mysql_query("delete from $databasename.structurefunctionpage
	   	where viewid = '$value6'");
		$insertview = mysql_query("INSERT INTO 
		$databasename.structurefunctionpage (structurefunctionpageid, structurefunctionpagename, disabled, datecreated,
		navigationposition, structurefunctionsectionid, viewid, structurefunctionpagetypeid, structurefunctionid, documentationpagedescription, 
		masteronly)
		SELECT structurefunctionpageid, structurefunctionpagename, disabled, datecreated,
		navigationposition, structurefunctionsectionid, viewid, structurefunctionpagetypeid, structurefunctionid, documentationpagedescription, 
		masteronly
		FROM $masterdatabase.structurefunctionpage
		where viewid = '$value6'"); 
		
		//create viewsection table content
		$deleteviewsection = mysql_query("delete from $databasename.viewsection
	   	where viewid = '$value6'");
		$insertviewsection = mysql_query("INSERT INTO 
		$databasename.viewsection (viewsectionid, viewsectionname, disabled, datecreated, viewid, position, viewtextid,
		viewdatasetid, viewmapid, viewcalendarid, viewspecialcodeurl, documentationhelp, masteronly)
		SELECT viewsectionid, viewsectionname, disabled, datecreated, viewid, position, viewtextid,
		viewdatasetid, viewmapid, viewcalendarid, viewspecialcodeurl, documentationhelp, masteronly
		FROM $masterdatabase.viewsection
		where viewid = '$value6'"); 
		
		//get added viewsections so you can add the rest of the components
		$getviewsections = mysql_query("select * from $databasename.viewsection where viewid = '$value6'");
		while($row89 = mysql_fetch_array($getviewsections)){
			$viewsectionid = $row89['viewsectionid'];
			$viewtextid = $row89['viewtextid'];
			$viewdatasetid = $row89['viewdatasetid'];
			$viewmapid = $row89['viewmapid'];
			$viewcalendarid = $row89['viewcalendarid'];
			
			echo "update view section id: ".$viewsectionid."<br/>";
			//create viewtext table content
			if($viewtextid >= 1){
				$deleteviewtext = mysql_query("delete from $databasename.viewtext
	   			where viewtextid = '$viewtextid'");
				$insertviewtext = mysql_query("INSERT INTO 
				$databasename.viewtext (viewtextid, viewtextname, disabled, datecreated, text, styleheadertag, masteronly)
				SELECT viewtextid, viewtextname, disabled, datecreated, text, styleheadertag, masteronly
				FROM $masterdatabase.viewtext
				where viewtextid = '$viewtextid'"); 	
			}	
			
			//create viewdataset table content
			if($viewdatasetid >= 1){
				$deleteviewdataset = mysql_query("delete from $databasename.viewdataset
	   			where viewdatasetid = '$viewdatasetid'");
				$insertviewdataset = mysql_query("INSERT INTO 
				$databasename.viewdataset (viewdatasetid, viewdatasetname, disabled, datecreated, datasetid, showheadings, 
				gridtextdescription, tablewidth, masteronly, allowedituniquefieldid, requestconfigurationid, pagelink, pagelinkname,
				structurefieldiddisable, structurefieldidenable, addnew)
				SELECT viewdatasetid, viewdatasetname, disabled, datecreated, datasetid, showheadings, 
				gridtextdescription, tablewidth, masteronly, allowedituniquefieldid, requestconfigurationid, pagelink, pagelinkname,
				structurefieldiddisable, structurefieldidenable, addnew
				FROM $masterdatabase.viewdataset
				where viewdatasetid = '$viewdatasetid'"); 	
				
				//create viewdataset dataset table content
				$getdatasets = mysql_query("select * from $databasename.viewdataset where viewdatasetid = '$viewdatasetid'");
				while($row46 = mysql_fetch_array($getdatasets)){
					$datasetid = $row46['datasetid'];
					echo "update dataset id: ".$datasetid."<br/>";
					$deletedataset = mysql_query("delete from $databasename.dataset
			   		where datasetid = '$datasetid'");
					$insertdataset = mysql_query("INSERT INTO 
					$databasename.dataset (datasetid, datasetname, disabled, datecreated, sortbyfield, datefilterfield, invert, masteronly)
					SELECT datasetid, datasetname, disabled, datecreated, sortbyfield, datefilterfield, invert, masteronly
					FROM $masterdatabase.dataset
					where datasetid = '$datasetid'"); 
					
					//create datasetcolumn table content
					$deletedatasetcolumns = mysql_query("delete from $databasename.datasetcolumn
			   		where datasetid = '$datasetid'");
					$insertdatasetcolumns = mysql_query("INSERT INTO 
					$databasename.datasetcolumn (datasetcolumnid, datasetcolumnname, disabled, datecreated, datasetid, 
					structurefieldid, datasetaggregateid, format, sortorder, connectedtablename, hidecolumn, masteronly, calculationlogic)
					SELECT datasetcolumnid, datasetcolumnname, disabled, datecreated, datasetid, 
					structurefieldid, datasetaggregateid, format, sortorder, connectedtablename, hidecolumn, masteronly, calculationlogic
					FROM $masterdatabase.datasetcolumn
					where datasetid = '$datasetid'"); 
					
					//create datasetfilter table content
					$getdatasetcolumns = mysql_query("select * from $databasename.datasetcolumn where datasetid = '$datasetid'");
					while($row47 = mysql_fetch_array($getdatasetcolumns)){
						$datasetcolumnid = $row47['datasetcolumnid'];
						$deletedatasetcolumn = mysql_query("delete from $databasename.datasetfilter
			   			where datasetcolumnid = '$datasetcolumnid'");
						$insertdataset = mysql_query("INSERT INTO 
						$databasename.datasetfilter (datasetfilterid, datasetfiltername, disabled, datecreated, datasetcolumnid, datasetfilterlogicid, masteronly)
						SELECT datasetfilterid, datasetfiltername, disabled, datecreated, datasetcolumnid, datasetfilterlogicid, masteronly
						FROM $masterdatabase.datasetfilter
						where datasetcolumnid = '$datasetcolumnid'"); 
					}			
				}
			}
			
			//create viewmap table content
			if($viewmapid >= 1){
				$deleteviewmap = mysql_query("delete from $databasename.viewmap
			   	where viewmapid = '$viewmapid'");
				$insertviewmap = mysql_query("INSERT INTO 
				$databasename.viewmap (viewmapid, viewmapname, disabled, datecreated, datasetid, viewheat, viewlocationpins, 
				allowsearch, datasetcolumnid, masteronly)
				SELECT viewmapid, viewmapname, disabled, datecreated, datasetid, viewheat, viewlocationpins, 
				allowsearch, datasetcolumnid, masteronly
				FROM $masterdatabase.viewmap
				where viewmapid = '$viewmapid'"); 
				
				//create viewmapfilter table content
				$deleteviewmapfilter = mysql_query("delete from $databasename.viewmapfilter
			   	where viewmapfilterid = '$viewmapid'");
				$insertviewmapfilter = mysql_query("INSERT INTO 
				$databasename.viewmapfilter (viewmapfilterid, viewmapfiltername, disabled, datecreated, viewmapid, 
				datasetcolumnid, datasetfilterlogicid, masteronly)
				SELECT viewmapfilterid, viewmapfiltername, disabled, datecreated, viewmapid, 
				datasetcolumnid, datasetfilterlogicid, masteronly
				FROM $masterdatabase.viewmapfilter
				where viewmapid = '$viewmapid'"); 
					
				//create viewmap dataset table content
				$getdatasets = mysql_query("select * from $databasename.viewmap where viewmapid = '$viewmapid'");
				while($row46 = mysql_fetch_array($getdatasets)){
					$datasetid = $row46['datasetid'];
					echo "update dataset id: ".$datasetid."<br/>";
					$deletedataset = mysql_query("delete from $databasename.dataset
			   		where datasetid = '$datasetid'");
					$insertdataset = mysql_query("INSERT INTO 
					$databasename.dataset (datasetid, datasetname, disabled, datecreated, sortbyfield, datefilterfield, invert, masteronly)
					SELECT datasetid, datasetname, disabled, datecreated, sortbyfield, datefilterfield, invert, masteronly
					FROM $masterdatabase.dataset
					where datasetid = '$datasetid'"); 
					
					//create datasetcolumn table content
					$deletedatasetcolumn = mysql_query("delete from $databasename.datasetcolumn
			   		where datasetcolumnid = '$datasetid'");
					$insertdatasetcolumns = mysql_query("INSERT INTO 
					$databasename.datasetcolumn (datasetcolumnid, datasetcolumnname, disabled, datecreated, datasetid, 
					structurefieldid, datasetaggregateid, format, sortorder, connectedtablename, hidecolumn, masteronly, calculationlogic)
					SELECT datasetcolumnid, datasetcolumnname, disabled, datecreated, datasetid, 
					structurefieldid, datasetaggregateid, format, sortorder, connectedtablename, hidecolumn, masteronly, calculationlogic
					FROM $masterdatabase.datasetcolumn
					where datasetid = '$datasetid'"); 
					
					//create datasetfilter table content
					$getdatasetcolumns = mysql_query("select * from $databasename.datasetcolumn where datasetid = '$datasetid'");
					while($row47 = mysql_fetch_array($getdatasetcolumns)){
						$datasetcolumnid = $row47['datasetcolumnid'];
						$deletedatasetcolumn = mysql_query("delete from $databasename.datasetfilter
			   			where datasetcolumnid = '$datasetcolumnid'");
						$insertdataset = mysql_query("INSERT INTO 
						$databasename.datasetfilter (datasetfilterid, datasetfiltername, disabled, datecreated, datasetcolumnid, datasetfilterlogicid, masteronly)
						SELECT datasetfilterid, datasetfiltername, disabled, datecreated, datasetcolumnid, datasetfilterlogicid, masteronly
						FROM $masterdatabase.datasetfilter
						where datasetcolumnid = '$datasetcolumnid'"); 
					}			
				}
			}
			
			//create viewcalendar table content
			if($viewcalendarid >= 1){
				$deleteviewcalendar = mysql_query("delete from $databasename.viewcalendar
			   	where viewcalendarid = '$viewcalendarid'");
				$insertviewcalendar = mysql_query("INSERT INTO 
				$databasename.viewcalendar (viewcalendarid, viewcalendarname, disabled, datecreated, datasetid, masteronly)
				SELECT viewcalendarid, viewcalendarname, disabled, datecreated, datasetid, masteronly
				FROM $masterdatabase.viewcalendar
				where viewcalendarid = '$viewcalendarid'"); 	
				
				//create viewcalendar dataset table content
				$getdatasets = mysql_query("select * from $databasename.viewcalendar where viewcalendarid = '$viewcalendarid'");
				while($row46 = mysql_fetch_array($getdatasets)){
					$datasetid = $row46['datasetid'];
					echo "update dataset id: ".$datasetid."<br/>";
					$deletedataset = mysql_query("delete from $databasename.dataset
			   		where datasetid = '$datasetid'");
					$insertdataset = mysql_query("INSERT INTO 
					$databasename.dataset (datasetid, datasetname, disabled, datecreated, sortbyfield, datefilterfield, invert, masteronly)
					SELECT datasetid, datasetname, disabled, datecreated, sortbyfield, datefilterfield, invert, masteronly
					FROM $masterdatabase.dataset
					where datasetid = '$datasetid'"); 
					
					//create datasetcolumn table content
					$deletedatasetcolumn = mysql_query("delete from $databasename.datasetcolumn
			   		where datasetcolumnid = '$datasetid'");
					$insertdatasetcolumns = mysql_query("INSERT INTO 
					$databasename.datasetcolumn (datasetcolumnid, datasetcolumnname, disabled, datecreated, datasetid, 
					structurefieldid, datasetaggregateid, format, sortorder, connectedtablename, hidecolumn, masteronly, calculationlogic)
					SELECT datasetcolumnid, datasetcolumnname, disabled, datecreated, datasetid, 
					structurefieldid, datasetaggregateid, format, sortorder, connectedtablename, hidecolumn, masteronly, calculationlogic
					FROM $masterdatabase.datasetcolumn
					where datasetid = '$datasetid'"); 
					
					//create datasetfilter table content
					$getdatasetcolumns = mysql_query("select * from $databasename.datasetcolumn where datasetid = '$datasetid'");
					while($row47 = mysql_fetch_array($getdatasetcolumns)){
						$datasetcolumnid = $row47['datasetcolumnid'];
						$deletedatasetcolumn = mysql_query("delete from $databasename.datasetfilter
			   			where datasetcolumnid = '$datasetcolumnid'");
						$insertdataset = mysql_query("INSERT INTO 
						$databasename.datasetfilter (datasetfilterid, datasetfiltername, disabled, datecreated, datasetcolumnid, datasetfilterlogicid, masteronly)
						SELECT datasetfilterid, datasetfiltername, disabled, datecreated, datasetcolumnid, datasetfilterlogicid, masteronly
						FROM $masterdatabase.datasetfilter
						where datasetcolumnid = '$datasetcolumnid'"); 
					}			
				}		
			}
			
				
		}
	}
	
	
	//insert businessemailcontents
	if($businessemailcontents <> ''){
		$array = explode(',', $businessemailcontents); //split string into array seperated by ', '
		foreach($array as $value) //loop over values
		{
		   	//create emailcontent table content
		   	echo "update emailcontent id: ".$value."<br/>";
		   	$deleteemailcontents = mysql_query("delete from $databasename.emailcontent
			where emailcontentid = '$value'");
		   	$insertemailcontents = mysql_query("INSERT INTO $databasename.emailcontent (emailcontentid, emailcontentname, disabled,
		   	datecreated, masteronly, subject, content, emaillayoutid)
			SELECT emailcontentid, emailcontentname, disabled, datecreated, masteronly, subject, content, emaillayoutid
			FROM $masterdatabase.emailcontent
			where emailcontentid = '$value'");
		}
		//get all added emailcontents 
		$getemailcontent = mysql_query("select emaillayoutid from $databasename.emailcontent group by emaillayoutid");
		while($row2=mysql_fetch_array($getemailcontent)){
			//create emaillayout table content
			$value99 = $row2['emaillayoutid'];
			echo "update emaillayout id: ".$value99."<br/>";
			$deleteemailcontents = mysql_query("delete from $databasename.emaillayout
			where emaillayoutid = '$value99'");
		   	$insertemaillayouts = mysql_query("INSERT INTO $databasename.emaillayout (emaillayoutid, emaillayoutname, disabled, 
		   	datecreated, masteronly, htmlcontent)
			SELECT emaillayoutid, emaillayoutname, disabled, datecreated, masteronly, htmlcontent
			FROM $masterdatabase.emaillayout
			where emaillayoutid = '$value99'");	
		}
	}	
		
		
	//insert requestconfiguration	
	$array = explode(',', $businessrequestmanagerconfigurations); //split string into array seperated by ', '
	foreach($array as $value) //loop over values
	{
	   	//create requestconfiguration table content
	   	echo "update requestionconfiguration id: ".$value."<br/>";
		$deleterequestconfiguration = mysql_query("delete from $databasename.requestconfiguration
		where requestconfigurationid = '$value'");
	   	$insertrequestconfiguration = mysql_query("INSERT INTO $databasename.requestconfiguration (requestconfigurationid,
	   	requestconfigurationname, disabled, datecreated,requesttable, requestidfield, requestdisplayfield, masteronly, 
	   	emailcontentrequestid, emailcontentdeclinedid, emailcontentapprovedid, permissiontable)
		SELECT requestconfigurationid,
	   	requestconfigurationname, disabled, datecreated,requesttable, requestidfield, requestdisplayfield, masteronly, 
	   	emailcontentrequestid, emailcontentdeclinedid, emailcontentapprovedid, permissiontable
		FROM $masterdatabase.requestconfiguration
		where requestconfigurationid = '$value'");
	}
	
	//businesspermissionroles
	$array = explode(',', $businesspermissionroles); //split string into array seperated by ', '
	foreach($array as $value3) //loop over values
	{
	    $getpermissionrole = mysql_query("select * from permissionrole
	    where permissionroleid = '$value3'");
	    $getpermissionrolerow = mysql_fetch_array($getpermissionrole);
	    $permissionrolename = $getpermissionrolerow['permissionrolename'];
	    
	    //create permissionrole table content
	   	echo "update permissionrole id: ".$value3."<br/>";
		$deletepermissionrole = mysql_query("delete from $databasename.permissionrole
		where permissionroleid = '$value3'");
		$insertpermissionrole = mysql_query("INSERT INTO 
		$databasename.permissionrole (permissionroleid, permissionrolename, disabled, datecreated, helpdocumentation, masteronly)
		SELECT permissionroleid, permissionrolename, disabled, datecreated, helpdocumentation, masteronly
		FROM $masterdatabase.permissionrole
		where permissionroleid = '$value3'"); 
		
		$date = date("Y-m-d");	
		
		//create permissionrolestructurefunctionpage table content
		echo "update permissionrolestructurefunctionpage id: ".$value3."<br/>";
		$deletepermissionrolestructurefunctionpage = mysql_query("delete from $databasename.permissionrolestructurefunctionpage
		where permissionroleid = '$value3'");
		$insertpermissionrolestructurefunctionpage = mysql_query("INSERT INTO 
		$databasename.permissionrolestructurefunctionpage (permissionrolestructurefunctionpageid, 
		permissionroleid, structurefunctionpageid, disabled, datecreated, masteronly)
		SELECT permissionrolestructurefunctionpageid, 
		permissionroleid, structurefunctionpageid, disabled, datecreated, masteronly
		FROM $masterdatabase.permissionrolestructurefunctionpage
		where permissionroleid = '$value3'"); 
		
		//create permissionrolestructurefunctionsection table content
		echo "update permissionrolestructurefunctionsection id: ".$value3."<br/>";
		$deletepermissionrolestructurefunctionsection = mysql_query("delete from $databasename.permissionrolestructurefunctionsection
		where permissionroleid = '$value3'");
		$insertpermissionrolestructurefunctionsection = mysql_query("INSERT INTO 
		$databasename.permissionrolestructurefunctionsection (permissionrolestructurefunctionsectionid, 
		permissionroleid, structurefunctionsectionid, disabled, datecreated, masteronly)
		SELECT permissionrolestructurefunctionsectionid, 
		permissionroleid, structurefunctionsectionid, disabled, datecreated, masteronly
		FROM $masterdatabase.permissionrolestructurefunctionsection
		where permissionroleid = '$value3'"); 
		
		//create permissionroledashboardsection table content
		echo "update permissionroledashboardsection id: ".$value3."<br/>";
		$deletepermissionroledashboardsection = mysql_query("delete from $databasename.permissionroledashboardsection
		where permissionroleid = '$value3'");
		$insertpermissionroledashboardsection = mysql_query("INSERT INTO 
		$databasename.permissionroledashboardsection (permissionroledashboardsectionid, 
		permissionroleid, chartsectionid, disabled, datecreated, masteronly)
		SELECT permissionroledashboardsectionid, 
		permissionroleid, chartsectionid, disabled, datecreated, masteronly
		FROM $masterdatabase.permissionroledashboardsection
		where permissionroleid = '$value3'"); 
		
		//create permissionrolereportsection table content
		echo "update permissionrolereportsection id: ".$value3."<br/>";
		$deletepermissionrolereportsection = mysql_query("delete from $databasename.permissionrolereportsection
		where permissionroleid = '$value3'");
		$insertpermissionrolereportsection = mysql_query("INSERT INTO 
		$databasename.permissionrolereportsection (permissionrolereportsectionid, 
		permissionroleid, reportsectionid, disabled, datecreated, masteronly)
		SELECT permissionrolereportsectionid, 
		permissionroleid, reportsectionid, disabled, datecreated, masteronly
		FROM $masterdatabase.permissionrolereportsection
		where permissionroleid = '$value3'"); 
	}
	
	
	//scripts
	$array = explode(',', $businessscripts); //split string into array seperated by ', '
	foreach($array as $value8) //loop over values
	{
	    //create scheduledjob table content
	   	echo "update scheduledjob id: ".$value8."<br/>";
		$deletescheduledjob = mysql_query("delete from $databasename.scheduledjob
		where scheduledjobscriptid = '$value8'");
		$insertscheduledjob = mysql_query("INSERT INTO 
		$databasename.scheduledjob (scheduledjobid, scheduledjobname, disabled, datecreated, scheduledjobscriptid, min,
		hour, daymonth, month, dayweek, lastrundate, masteronly)
		SELECT scheduledjobid, scheduledjobname, disabled, datecreated, scheduledjobscriptid, min,
		hour, daymonth, month, dayweek, lastrundate, masteronly
		FROM $masterdatabase.scheduledjob
		where scheduledjobscriptid = '$value8'");
		
		//create scheduledjobscript table content
		echo "update scheduledjobscript id: ".$value8."<br/>";
		$deletescheduledjobscript = mysql_query("delete from $databasename.scheduledjobscript
		where scheduledjobscriptid = '$value8'");
		$insertscheduledjobscript = mysql_query("INSERT INTO 
		$databasename.scheduledjobscript (scheduledjobscriptid, scheduledjobscriptname, disabled, datecreated, 
		description, scripturl, masteronly)
		SELECT scheduledjobscriptid, scheduledjobscriptname, disabled, datecreated, 
		description, scripturl, masteronly
		FROM $masterdatabase.scheduledjobscript
		where scheduledjobscriptid = '$value8'"); 
	}	

}

?>