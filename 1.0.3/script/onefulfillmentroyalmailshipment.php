<?php
// set_time_limit (500);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$date = date('Y-m-d');
$datetime = date('Y-m-d H:i:s');		

$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);

$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
//echo "<br/>Get batching stats: ";
//echo "<br/>differencemilliseconds: ".$differencemilliseconds;
//echo "<br/>sqlqueriestime: ".$sqlqueriestime;

const ROYAL_MAIL_URL = "https://api.royalmail.net/shipping/v3/";
const CLIENT_ID = "2c90c60a-3e41-4940-bef2-c6f163484b77";
const CLIENT_SECRET = "K7lJ7hL8mR1fC6qE2tJ7uI6sI2fK8fM5cJ2wP5eH1jD0dC3rW7";
const PASSWORD = "Harmony04";
const USERNAME = "admin@packitsafe.com";

// Get Token API
$segment = 'token';
$url = ROYAL_MAIL_URL.$segment;

echo "<br/>url : ".$url;
$header = array(
    "accept: application/json",
    "x-ibm-client-id: ".CLIENT_ID,
    "x-ibm-client-secret: ".CLIENT_SECRET,
    "x-rmg-security-password: ".PASSWORD,
    "x-rmg-security-username: ".USERNAME
);
echo "<br/>header : ".$header;

$result = curlRequest($url, $header);

echo "<br/><br/>result: ".$result;

$token = $result->token;

// Create Shipments
$segment = 'shipments';
$url = ROYAL_MAIL_URL.$segment;

$header = array(
  "accept: application/json",
  "content-type: application/json",
  "x-ibm-client-id: ".CLIENT_ID,
  "x-rmg-auth-token: ".$token
);

$postFields = 
{
  "Shipper": {
    "AddressId": "UNIQUEID123",
    "ShipperReference": "REF123456789",
    "ShipperDepartment": "123456789",
    "CompanyName": "Company & Co.",
    "ContactName": "Jane Smith",
    "AddressLine1": "Level 5",
    "AddressLine2": "Hashmoore House",
    "AddressLine3": "10 Sky Lane",
    "Town": "Leatherhead",
    "County": "Surrey",
    "CountryCode": "GB",
    "Postcode": "AA34 3AB",
    "PhoneNumber": "7723456789",
    "EmailAddress": "email@server.com",
    "VatNumber": "GB123 4567 89"
  },
  "Destination": {
    "AddressId": "UNIQUEID123",
    "CompanyName": "Company & Co.",
    "ContactName": "Jane Brown",
    "AddressLine1": "White Horse",
    "AddressLine2": "10 Round Road",
    "AddressLine3": "Mitre Peak",
    "Town": "Leatherhead",
    "County": "Surrey",
    "CountryCode": "GB",
    "Postcode": "AB3 5CD",
    "PhoneNumber": "7123456789",
    "EmailAddress": "email@example.com",
    "VatNumber": "GB123 4567 89"
  },
  "ShipmentInformation": {
    "ShipmentDate": "2019-01-16",
    "ServiceCode": "TPLN",
    "ServiceOptions": {
      "PostingLocation": "123456789",
      "ServiceLevel": "01",
      "ServiceFormat": "P",
      "Safeplace": "Front Porch",
      "SaturdayGuaranteed": false,
      "ConsequentialLoss": "Level2",
      "LocalCollect": false,
      "TrackingNotifications": "Email",
      "RecordedSignedFor": false
    },
    "TotalPackages": 1,
    "TotalWeight": 2.2,
    "WeightUnitOfMeasure": "KG",
    "Product": "NDX",
    "DescriptionOfGoods": "Clothing",
    "ReasonForExport": "Sale of goods",
    "Value": 39.98,
    "Currency": "GBP",
    "LabelFormat": "PDF",
    "SilentPrintProfile": "75b59db8-3cd3-4578-888e-54be016f07cc",
    "ShipmentAction": "Process",
    "Packages": [
      {
        "PackageOccurrence": 1,
        "PackagingId": "UNIQUEID123",
        "Weight": 2.2,
        "Length": 15,
        "Width": 15,
        "Height": 5
      }
    ],
    "Items": [
      {
        "ItemId": "UNIQUEID123",
        "Quantity": 2,
        "Description": "White Tee-shirt",
        "Value": 19.99,
        "Weight": 0.9,
        "PackageOccurrence": 1,
        "HsCode": "652534",
        "SkuCode": "SKU3455692",
        "CountryOfOrigin": "CN",
        "ImageUrl": "http://www.myimagestore.com/myimage.jpg"
      }
    ]
  }
}

echo "<br/><br/>";




// function for sending each curl request
function curlRequest($url, $header) 
{
  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_HTTPHEADER => $header,
  ));
  
  $response = curl_exec($curl);
  $err = curl_error($curl);
  
  curl_close($curl);
  
  if ($err) {
    echo "cURL Error #:" . $err;
    die();
  } else {
    return $response;
  }
  
}
?>