<?php session_start(); ?>
<?php include('../config.php'); ?>
<?php include('../sessionconfig.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	
 	<title>Style Configurator</title>

 	<!-- Bootstrap -->
 	<link href="bootstrap-3.3.1/dist/css/bootstrap.css" rel="stylesheet">

 	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
 	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">   
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
	<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script type="text/javascript">
    function redirectMe (sel) {
        var url = sel[sel.selectedIndex].value;
        window.location = url;
    }
    </script>
    <link rel='stylesheet' type='text/css' href='../style.php' />
</head>
  
<body class='body'>
	<?php include_once ('../headerthree.php');	?>
	
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:10px;">
		<!--search results header bar for desktop-->
		<div class='bodyheader'>
			<h3>Upload Invoice</h3>
		</div>
			<?php 
			$rowid = isset($_GET['rowid']) ? strip_tags($_GET['rowid']) : '';
			$submitdoc = isset($_POST['submitdoc']) ? $_POST['submitdoc'] : ''; 
			if($submitdoc) {
				$filename=$_FILES["file"]["name"];
				if ($filename<>"") {
     				include "accountbillinguploadinvoiceuploader.php";
     			}
			}
			?>				
			
			<div class='bodycontent'>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<?php 
	  				if(isset($validate) && $validate <> "") {
						echo "<p class='background-warning'>".$validate."</p>";
					} 
					?>
				
			</div>
			
			<form action="" method="post" enctype="multipart/form-data" name="form1" id="form1"> 
			  	<b>Add new document: </b><br/>PDF Format Only<br/>
			  	<input name="file" type="file" id="file" /> 
			  	<br/>
			  	<input class="btn btn-primary" type="submit" name="submitdoc" value="Save Document" /> 
			</form> 
			<br/>
			<script>
			$(document).ready(function(){
			    $('#myTable4').dataTable({
			 });
			});
			</script>
			
			
					<br/><br/><br/>
				</div>
	  		</div>
		</div>
			
	</div>	
	<?php include_once ('../footer.php'); ?>
</body>
