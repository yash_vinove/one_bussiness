<?php

$urlarray = array();

require_once "simple_html_dom.php";
$start_url = $topurl;       
define ('CLI', false);
$extension = array (
					 ".html", 
					 ".php",
					 "/",
				   ); 
$freq = "daily";
$priority = "1.0";
define ('VERSION', "1.0");                                            
define ('NL', CLI ? "\n" : "<br>");
$scanned = array();

$pf = fopen ($file, "w");
if (!$pf) {
	echo "Cannot create $file!" . NL;
	return;
}

$start_url = filter_var ($start_url, FILTER_SANITIZE_URL);

fwrite ($pf, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" .
			 "<?xml-stylesheet type=\"text/xsl\"?>\n" .
			 "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"\n" .
			 "        xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" .
			 "        xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9\n" .
			 "        http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">\n" .
			 "  <url>\n" .
			 "    <loc>" . htmlentities ($start_url) ."</loc>\n" .
			 "    <changefreq>$freq</changefreq>\n" .
			 "    <priority>$priority</priority>\n" .
			 "  </url>\n");

$url = filter_var ($start_url, FILTER_SANITIZE_URL);
$surl = str_replace("https://", "", $url);
$surl = str_replace("http://", "", $surl);
$surl = str_replace("www.", "", $surl);

function rel2abs($rel, $base) {
	if(strpos($rel,"//") === 0) {
	return "http:".$rel;
	}
	/* return if  already absolute URL */
	if  (parse_url($rel, PHP_URL_SCHEME) != '') return $rel;
	$first_char = substr ($rel, 0, 1);
	/* queries and  anchors */
	if ($first_char == '#'  || $first_char == '?') return $base.$rel;
	/* parse base URL  and convert to local variables:
	$scheme, $host,  $path */
	extract(parse_url($base));
	/* remove  non-directory element from path */
	$path = preg_replace('#/[^/]*$#',  '', $path);
	/* destroy path if  relative url points to root */
	if ($first_char ==  '/') $path = '';
	/* dirty absolute  URL */
	$abs =  "$host$path/$rel";
	/* replace '//' or  '/./' or '/foo/../' with '/' */
	$re =  array('#(/.?/)#', '#/(?!..)[^/]+/../#');
	for($n=1; $n>0;  $abs=preg_replace($re, '/', $abs, -1, $n)) {}
	/* absolute URL is  ready! */
	return  $scheme.'://'.$abs;
}
	
function checkurl($url){
	global $surl, $scanned;
	$agent = "Mozilla/5.0 (compatible; iProDev PHP XML Sitemap Generator/" . VERSION . ", http://iprodev.com)";
	$ch = curl_init();
	curl_setopt ($ch, CURLOPT_AUTOREFERER, true);
	curl_setopt ($ch, CURLOPT_URL, $url);
	curl_setopt ($ch, CURLOPT_USERAGENT, $agent);
	curl_setopt ($ch, CURLOPT_VERBOSE, 1);
	curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	curl_setopt ($ch, CURLOPT_HEADER, 0);
	curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
	$data= curl_exec($ch);
	curl_close($ch);
	$html = str_get_html ($data);
	$a1   = $html->find('a');

	foreach ($a1 as $val) {
		$next_url = $val->href or "";		
		$ignore = false;
		if ((substr ($next_url, 0, 7) != "http://")  && (substr ($next_url, 0, 8) != "https://") && (substr ($next_url, 0, 6) != "ftp://")   && (substr ($next_url, 0, 7) != "mailto:")){
			$next_url = @rel2abs ($next_url, $url);
		}		
		if (strpos($next_url, '/images/') !== false || 
		strpos($next_url, '&out=json') !== false || 
		strpos($next_url, '/wp-content/') !== false || 
		strpos($next_url, $surl) == false || 
		in_array ($next_url, $scanned) || 
		$next_url == "#" || 
		!filter_var ($next_url, FILTER_VALIDATE_URL)) 
		{
	 		$ignore = true;
		}
		else {
			if($ignore == false){
				array_push($scanned, $next_url);		
				//echo "<br/>".$next_url;
			}
		}	
	}
	//echo "<br/><br/>".json_encode($scanned);
}

//get all links on homepage of website
array_push($urlarray, $start_url);
checkurl($start_url);

//get all links on all pages found on the homepage
foreach($scanned as $url){
	checkurl($url);
}

//write all scanned to the sitemap
foreach ($scanned as $url) {
	$pr = number_format ( round ( $priority / count ( explode( "/", trim ( str_ireplace ( array ("http://", "https://"), "", $url ), "/" ) ) ) + 0.5, 3 ), 1 );
	fwrite ($pf, "  <url>\n" .
	"    <loc>" . htmlentities ($url) ."</loc>\n" .
	"    <changefreq>$freq</changefreq>\n" .
	"    <priority>$pr</priority>\n" .
	"  </url>\n");
}
fwrite ($pf, "</urlset>\n");
fclose ($pf);

?>