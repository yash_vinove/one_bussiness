<?php session_start();
include('config.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en">
  <head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	
    	<title>OneBusiness</title>

    	<!-- Bootstrap -->
    	<link href="bootstrap-3.3.1/dist/css/bootstrap.css" rel="stylesheet">

    	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    	<!--[if lt IE 9]>
    	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    	<![endif]-->
    
    	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    	<SCRIPT type="text/javascript">
    	function SwitchHiddenDiv(){
    		document.getElementById('div1').style.display="block";
    	}
    	</SCRIPT>
										
  </head>
  
  <body>
	<?php
	//declare variables	
	$t = isset($_GET['t']) ? $_GET['t'] : '';
	$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
	$businessname = isset($_POST['businessname']) ? strip_tags($_POST['businessname']) : '';
	$contactfirstname = isset($_POST['contactfirstname']) ? strip_tags($_POST['contactfirstname']) : '';
	$contactlastname = isset($_POST['contactlastname']) ? strip_tags($_POST['contactlastname']) : '';
	$contactemailaddress = isset($_POST['contactemailaddress']) ? strip_tags($_POST['contactemailaddress']) : '';
	$contactphone = isset($_POST['contactphone']) ? strip_tags($_POST['contactphone']) : '';
	$password = isset($_POST['password']) ? strip_tags($_POST['password']) : '';
	$currency = isset($_POST['currency']) ? strip_tags($_POST['currency']) : '';
	$country = isset($_POST['country']) ? strip_tags($_POST['country']) : ''; 	
 	
  	if($submit) {
 		//validate all fields
 		$validate = ""; 	
		if($businessname == "") {
			$validate = $validate."Business Name is a required field. "; 	
	 	}
	 	if($contactfirstname == "") {
			$validate = $validate."Contact First Name is a required field. "; 	
	 	}
	 	if($contactlastname == "") {
			$validate = $validate."Contact Last Name is a required field. ";	 	
	 	}
	 	if($contactemailaddress == "") {
			$validate = $validate."Contact Email Address is a required field. ";	 	
	 	}
	 	if($contactphone == "") {
			$validate = $validate."Contact Phone is a required field. ";	 	
	 	}
	 	if($currency == "") {
			$validate = $validate."Currency is a required field. ";	 	
	 	}
	 	if($country == "") {
			$validate = $validate."Country is a required field. ";	 	
	 	}
	 	if (strpos($contactemailaddress,'@') == false) {
    		$validate = $validate."Contact Email address must contain an @ symbol. ";
		}
		if(strlen($password)>25||strlen($password)<6) {
     		$validate = $validate."Your password must be between 6 and 25 characters. ";
   		}
   }
   
   
   
   	//run code if validation complete
   if($submit) {
   		if($validate == "") {
			$password = md5($password);   			
   			
			//get countryid
			$getcountryid = mysql_query("select * from $masterdatabase.accountbillingcountry 
			where accountbillingcountry.accountbillingcountryid = '$country'");
			$getcountryrow = mysql_fetch_array($getcountryid);
			$countryid = $getcountryrow['countryid'];
			
			//get currencyid   			
   			$getcurrencyid = mysql_query("select * from $masterdatabase.accountbillingcurrency 
			where accountbillingcurrency.accountbillingcurrencyid = '$currency'");
			$getcurrencyrow = mysql_fetch_array($getcurrencyid);
			$currencyid = $getcurrencyrow['currencyid'];
			
   			//add tenant record
   			$date = date("Y-m-d");
			$tenantadd = mysql_query("insert into $masterdatabase.tenant (tenantname, firstname, lastname, emailaddress,
			phonenumber, rootpassword, accountbillingcurrencyid, accountbillingcountryid, tenanttypeid, currencyid, countryid, disabled, datecreated) 
			values ('$businessname','$contactfirstname','$contactlastname','$contactemailaddress','$contactphone',
			'$password','$currency','$country','3','$currencyid','$countryid','0','$date')");   
			$tenantid = mysql_insert_id();		
			$recordid = $tenantid;	
   			$filedir = 1;
   			//populate tenant and businesses
   			$getversion = mysql_query("select max(versionid) as maxversionid from $masterdatabase.version where disabled = '0'");
   			$getversionrow = mysql_fetch_array($getversion);
   			$versionid = $getversionrow['maxversionid'];
   			$getversion2 = mysql_query("select * from $masterdatabase.version where versionid = '$versionid'");
   			$getversion2row = mysql_fetch_array($getversion2);
   			$versionfolder = $getversion2row['versionfoldername'];
   			$includepath = $versionfolder."/script/tenantadd.php";
   			include($includepath);
   					
     		//go to login page
  			$url = 'index.php?tenant='.$tenantid;
   			echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">';								
		}
		else {
			$validate = $validate."Password is incorrect.";
		}			
	}

 
	if($tenant >= 1){
		$checktenantactive = mysql_query("select * from $masterdatabase.tenant where tenantid = '$tenant' and disabled = '0'");
		if(mysql_num_rows($checktenantactive)>=1) {
		}
		else {
			echo "<p class='bg-danger'>This tenant is no longer active, please contact your vendor for more information.</p>";
			die;
		}
	}
	?>
		<div class="containerhome">
			
			<div class="row">
  				<div class="hidden-xs col-md-4 col-sm-4">
			  		<img src="images/loginpage.jpg" class="img-responsive" alt="">
  				</div>
  					
  				<div class="col-md-8 col-sm-8 col-lg-8 col-xs-12">
   				<div class="col-md-12 col-sm-12 col-xs-12">
				  		<h3 class="text-center">Register to get started</h3>
				  		<p>Welcome to OneBusiness. To setup a pilot account for 30 days please fill in the details below, and the system will
				  		automatically create a login for you. Once you click submit, you will be redirected to the login page - please enter
				  		the email address and password you registered with in order to access the system. </p>
				  		
				  		<p>If you wish to continue using the 
				  		platform after 30 days, please feel free to get in contact and we will fully activate your account. After 30 days we will 
				  		disable and delete your business from the system, in order to preserve your business privacy. </p>
				  		
				  		<p>If you wish to speak to someone about OneBusiness please see our contact list for Tristom Labs or one of our partners
				  		on <a href="http://www.tristomlabs.com">Tristom Labs Website</a>.</p>
				  		<div class="row">
				      	<div class="col-xs-1 col-sm-1 col-md-1">
				      	</div>
				      	
				      	<div class="col-xs-10 col-sm-10 col-md-10">
				      		<div style="text-align:center;">
					        		<?php 
									if($t == "" || $t == 1){
										if(isset($validate) && $validate <> "") {
											echo "<p class='bg-danger'>".$validate."</p>";
										} 
										?>
										<div> 
											<br/>
											<form class="form-horizontal" action='register.php?t=1' method="post">
											  	<div class="form-group">
											    	<label for="businessname" class="col-sm-3 control-label">Business Name *</label>
											    	<div class="col-sm-9">
											      	<input type="text" class="form-control" name="businessname" value="<?php echo $businessname ?>">
											    	</div>
											  	</div>
											  	<div class="form-group">
											    	<label for="contactfirstname" class="col-sm-3 control-label">Contact First Name *</label>
											    	<div class="col-sm-9">
											      	<input type="text" class="form-control" name="contactfirstname" value="<?php echo $contactfirstname ?>">
											    	</div>
											  	</div>
											  	<div class="form-group">
											    	<label for="contactlastname" class="col-sm-3 control-label">Contact Last Name *</label>
											    	<div class="col-sm-9">
											      	<input type="text" class="form-control" name="contactlastname" value="<?php echo $contactlastname ?>">
											    	</div>
											  	</div>
											  	<div class="form-group">
											    	<label for="contactemailaddress" class="col-sm-3 control-label">Contact Email Address *</label>
											    	<div class="col-sm-9">
											      	<input type="text" class="form-control" name="contactemailaddress" value="<?php echo $contactemailaddress ?>">
											    	</div>
											  	</div>
											  	<div class="form-group">
											    	<label for="contactphone" class="col-sm-3 control-label">Contact Phone *</label>
											    	<div class="col-sm-9">
											      	<input type="text" class="form-control" name="contactphone" value="<?php echo $contactphone ?>">
											    	</div>
											  	</div>
											  	<div class="form-group">
											    	<label for="password" class="col-sm-3 control-label">Password *</label>
											    	<div class="col-sm-9">
											      	<input type="text" class="form-control" name="password" value="<?php echo $password ?>">
											    	</div>
											  	</div>
											  	<div class="form-group">
											  		<label for="currency" class="col-sm-3 control-label">Currency *</label>
											    	<div class="col-sm-9">
											      	<?php
												      $querytype="SELECT accountbillingcurrencyid, accountbillingcurrencyname from $masterdatabase.accountbillingcurrency where disabled='0' order by accountbillingcurrencyname asc";
												      $resulttype = mysql_query ($querytype);
												      echo "<select class='form-control' name=currency>";
												      echo "<option value=''>Select Currency *</option>";
												          while($ntcurrency=mysql_fetch_array($resulttype)){
												              if ($currency==$ntcurrency['accountbillingcurrencyid']) {
												                echo "<option value=$ntcurrency[accountbillingcurrencyid] selected='true'>$ntcurrency[accountbillingcurrencyname]</option>";
												              }
												              else
												                echo "<option value=$ntcurrency[accountbillingcurrencyid] >$ntcurrency[accountbillingcurrencyname]</option>";
												              }
												      echo "</select>";
												   	?>
												  	</div>
												</div>
											  	<div class="form-group">
											  		<label for="country" class="col-sm-3 control-label">Country *</label>
											    	<div class="col-sm-9">
											      	<?php
												      $querytype="SELECT accountbillingcountryid, accountbillingcountryname from $masterdatabase.accountbillingcountry where disabled='0' order by accountbillingcountryname asc";
												      $resulttype = mysql_query ($querytype);
												      echo "<select class='form-control' name=country>";
												      echo "<option value=''>Select Country *</option>";
												          while($ntcountry=mysql_fetch_array($resulttype)){
												              if ($country==$ntcountry['accountbillingcountryid']) {
												                echo "<option value=$ntcountry[accountbillingcountryid] selected='true'>$ntcountry[accountbillingcountryname]</option>";
												              }
												              else
												                echo "<option value=$ntcountry[accountbillingcountryid] >$ntcountry[accountbillingcountryname]</option>";
												              }
												      echo "</select>";
												   	?>
												  	</div>
												</div>
											  	<div class="form-group">
											    	<div class="col-sm-offset-0 col-sm-12">
											      	<button type='submit' name='submit' value='Submit' class="btn btn-primary">Register</button>
											      	</div>
											  	</div>
											</form>									
										</div>	 	
																
									<?php
									}
									
									?>
									
								</div>
					      	</div>
					      	<div class="col-xs-1 col-sm-1 col-md-1">
					      	</div>
				    	</div>
					</div>
				</div>
			</div>
	  			
  		</div> 
  						    	

  </body>
</html>

