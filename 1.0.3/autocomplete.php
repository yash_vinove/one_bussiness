<?php session_start(); ?>
<?php

/*
 * Results array
 */
$results = array();

/*
 * Autocomplete formatter
 */
function autocomplete_format($results) {
    foreach ($results as $result) {
        echo $result[0] . '|' . $result[1] . "\n";
    }
}

include('config.php');
$tablename111 = isset($_GET['tablename']) ? strip_tags($_GET['tablename']) : '';
$fieldname111 = $tablename111.'name';
$fieldid111 = $tablename111.'id';
    	

/*
 * Search for term if it is given
 */
if (isset($_GET['q'])) {
    $q = strtolower($_GET['q']);
    //encrypt customername field
   // echo $fieldname111;
   if ($q) {
    	$getresults = "select * from ".$tablename111." where ".$fieldname111." like '%".$q."%'";
        $getresults = mysql_query($getresults);        
        while ($row55 = mysql_fetch_array($getresults)) {
            $key = $row55[$fieldname111]." (".$row55[$fieldid111].")";	
            $value = $row55[$fieldid111];	
            $results[] = array($key, $value);
        }
  	}
}

/*
 * Output format
 */
$output = 'autocomplete';
if (isset($_GET['output'])) {
    $output = strtolower($_GET['output']);
}

/*
 * Output results
 */
if ($output === 'json') {
    echo json_encode($results);
} else {
    echo autocomplete_format($results);
}
