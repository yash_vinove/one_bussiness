<?php 
   $lifetime = strtotime('+3000 minutes', 0);
   session_set_cookie_params($lifetime);
   ?>
<?php session_start(); ?>
<?php
   if(!isset($_COOKIE['database'])) {
       //echo "Cookie named 'database' is not set!";
   } else {
       //echo "Cookie 'database' is set!<br>";
       //echo "Value is: " . $_COOKIE['database'];
   }
   
   /*$lifetime = strtotime('+3000 minutes', 0);
   session_set_cookie_params($lifetime);*/
   ini_set('display_errors', 1);
   ini_set('display_startup_errors', 1);
   error_reporting(E_ALL);
   ?>
<?php //include('config.php'); ?>
<?php include('sessionconfig.php'); ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link href="./fontAwesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="screen" title="stylesheet" />
      <title>OneBusiness</title>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
      <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
      <link rel='stylesheet' type='text/css' href='css/sidebar.css' />
      <link rel='stylesheet' type='text/css' href='css/content.css' />
      <link rel='stylesheet' type='text/css' href='css/header.css' />
   </head>
   <body >
      <?php //include_once ('headerthree.php');	?>
      <html>
         <head>
         </head>
         <body class="content">
            <!----------------------------------------- sidebar ---------------------------------->
            <div class="sidebar">
               <nav class="main-menu">
                  <ul >
                     <li><i class="fa fa-long-arrow-left fa-2x arrow icon_click"></i></li>
                  </ul>
                  <ul class="navbar-nav flex-column w-100 mt-5">
                     <li class="nav-item active">
                        <img src="img/icons/my-apps.png" class="icon_click dropdown">
                        <a class="nav-link" href="my_app.php"  >My Apps</a>
                     </li>
                     <div class="dropdown-divider"></div>
                     <!-- Second Li Start-->
                     <li class="nav-item">
                        <img src="img/icons/dashboard-icon.png" class="icon_click dropdown">
                        <a class="nav-link" href="#" >
                        Dashboard
                        </a>
                     </li>
                     <!-- Second Li Close-->
                     <div class="dropdown-divider"></div>
                     <!-- <li class="nav-item">
                        <i class="fa fa-laptop fa-2x icon_click dropdown"></i>
                          <a class="nav-link" href="#" data-toggle="collapse" data-target="#navPages" aria-expanded="false" aria-controls="navPages" >
                             Reports   
                             <span class="ml-auto">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down">
                                   <polyline points="6 9 12 15 18 9"></polyline>
                                </svg>
                             </span>
                          </a>
                          <div id="navPages" class="collapse" data-parent="#sidebarNav">
                             <ul class="nav flex-column">
                                <li class="nav-item">
                                   <a class="nav-link" href="#" data-toggle="collapse" data-target="#navPagesSub" aria-expanded="false" aria-controls="navPagesSub" >
                                      Profile   
                                      <span class=" ml-auto">
                                         <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down">
                                            <polyline points="6 9 12 15 18 9"></polyline>
                                         </svg>
                                      </span>
                                   </a>
                                   <div class="collapse"  id="navPagesSub">
                                      <ul class="nav  flex-column navPagesSub">
                                         <li class="nav-item">
                                            <a class="nav-link" href="#" data-toggle="collapse" data-target="#navPagesSub2" aria-expanded="false" aria-controls="navPagesSub2" >
                                      Default Item   
                                      <span class=" ml-auto">
                                         <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down">
                                            <polyline points="6 9 12 15 18 9"></polyline>
                                         </svg>
                                      </span>
                                   </a>
                                           <div class="collapse"  id="navPagesSub2">
                                      <ul class="nav  flex-column">
                                         
                                         <li class="nav-item">
                                            <a class="nav-link" href="#">CMS</a>
                                         </li>
                                         <li class="nav-item">
                                            <a class="nav-link" href="#">Link</a>
                                         </li>
                                         <li class="nav-item">
                                            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                                         </li>
                                      </ul>
                                   </div>
                                         </li>
                                         <li class="nav-item">
                                            <a class="nav-link" href="#">CMS</a>
                                         </li>
                                         <li class="nav-item">
                                            <a class="nav-link" href="#">Link</a>
                                         </li>
                                         <li class="nav-item">
                                            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                                         </li>
                                      </ul>
                                   </div>
                                </li>
                                <li class="nav-item">
                                   <a class="nav-link" href="#">CMS</a>
                                </li>
                                <li class="nav-item">
                                   <a class="nav-link" href="#">Link</a>
                                </li>
                                <li class="nav-item">
                                   <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                                </li>
                             </ul>
                          </div>
                        </li>Second Li Close -->
                     <li class="nav-item ">
                        <img src="img/icons/reports-icon.png" class="icon_click dropdown">
                        <a class="nav-link" href="reports.php"  >
                        Reports</a>
                     </li>
                     <div class="dropdown-divider"></div>
                     <li class="nav-item ">
                        <img src="img/icons/my-details-icon.png" class="icon_click dropdown">
                        <a class="nav-link" href="#"  >
                        My Details 
                        </a>
                     </li>
                     <div class="dropdown-divider"></div>
                     <li class="nav-item ">
                        <img src="img/icons/users-icon.png" class="icon_click dropdown">
                        <a class="nav-link" href="#"  >
                        Users  
                        </a>
                        <!-- <div id="navDashboard4" class="collapse" data-parent="#sidebarNav">
                           <ul class="nav flex-column nav_1">
                              <li class="nav-item ">
                                 <a class="nav-link" href="#">Default</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link" href="#">CMS</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link" href="#">Link</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                              </li>
                           </ul>
                           </div> -->
                     </li>
                     <div class="dropdown-divider"></div>
                     <li class="nav-item ">
                        <img src="img/icons/site-builder-icon.png" class="icon_click dropdown">
                        <a class="nav-link" href="#"  >
                        Site Billing  
                        </a>
                     </li>
                     <div class="dropdown-divider"></div>
                     <li class="nav-item ">
                        <img src="img/icons/translation-icon.png" class="icon_click dropdown">
                        <a class="nav-link" href="#"  >Translation </a>
                     </li>
                     <div class="dropdown-divider"></div>
                     <li class="nav-item ">
                        <img src="img/icons/security-icon.png" class="icon_click dropdown">
                        <a class="nav-link" href="#"  >
                           Security
                           <!-- <span class="ml-auto">
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down">
                                 <polyline points="6 9 12 15 18 9"></polyline>
                              </svg>
                              </span> -->
                        </a>
                        <!-- <div id="navDashboard4" class="collapse" data-parent="#sidebarNav">
                           <ul class="nav flex-column nav_1">
                              <li class="nav-item ">
                                 <a class="nav-link" href="#">Default</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link" href="#">CMS</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link" href="#">Link</a>
                              </li>
                               <li class="nav-item">
                                 <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                              </li> 
                           </ul>
                           </div> -->
                     </li>
                     <div class="dropdown-divider"></div>
                     <li class="nav-item ">
                        <img src="img/icons/fave-icon-selected.png" class="icon_click dropdown">
                        <a class="nav-link" href="#" data-toggle="collapse" data-target="#navDashboard7" aria-expanded="false" aria-controls="navDashboard7" >
                           Favourites 
                           <span class="ml-auto">
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down">
                                 <polyline points="6 9 12 15 18 9"></polyline>
                              </svg>
                           </span>
                        </a>
                        <div id="navDashboard7" class="collapse" data-parent="#sidebarNav">
                           <ul class="nav flex-column nav_1">
                              <li class="nav-item ">
                                 <a class="nav-link" href="#">Stock</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link" href="#">Manage</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link" href="#">Finance</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link" href="#">Product</a>
                              </li>
                              <li class="link">
                                 <p>Manage Qick Link <img src="img/icons/add-icon.png" width="20px"></p>
                              </li>
                           </ul>
                        </div>
                     </li>
                  </ul>
                  <!-- <ul class="logout">
                     <li>
                        <a href="#">
                              <i class="fa fa-power-off fa-2x"></i>
                             <span class="nav-text">
                                 Logout
                             </span>
                         </a>
                     </li>  
                     </ul> -->
               </nav>
            </div>
            <!----------------------------------------- sidebar ---------------------------------->
            <!----------------------------------------- content ---------------------------------->
            <section  class="dashboard">
               <div class='overlay'></div>
               <div class="container">
                  <div class="row mt-5 header">
                     <div class="col-sm-6">
                        <img src="img/Apps.png" width="250px">
                     </div>
                     <div class="col-sm-6">
                        <p class="text-right txt_1">21st August 2020</p>
                     </div>
                  </div>
                  <div class="row mt-5 dekstop">
                     <div class="col-xs-3">
                        <img src="img/icons/burger-menu.png" class="img-responsive icon_click">
                     </div>
                     <div class="col-xs-6">
                        <img src="img/Apps.png" class="img-responsive">
                     </div>
                     <div class="col-xs-3">
                        <div class="header_profile ">
                           <img src="img/profile.png" class="img-responsive left_click">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-12">
                        <div class="line"></div>
                     </div>
                  </div>
                  <div class="row mt-5 add">
                     <div class="col-sm-8 ">
                        <h5 class="txt_2">Dashboard - <span class="light_font">OneProductSale - Sale</span></h5>
                     </div>
                     <div class="col-sm-4">
                        <!-- datepicker  -->
                           <div id="pageWrapper">
                            <div id="pageMasthead" class="pageSection"></div>
                                 <form>
                                    <!-- <label for="txtDateRange">Date Range:</label> -->
                                    <input type="text" id="txtDateRange" name="txtDateRange" class="inputField shortInputField dateRangeField" placeholder="Select a date-range" data-from-field="txtDateFrom" data-to-field="txtDateTo" />
                                    <input type="hidden" id="txtDateFrom" value="" />
                                    <input type="hidden" id="txtDateTo" value="" />
                                 </form>
                           </div>
                        <!-- datepicker  -->
                        <div >
                           <button class="btn btn-primary">Submit</button>
                        </div>
                     </div>
                  </div>
                  
               </div>
            </section>
            <!----------------------------------------- content ---------------------------------->
            <!----------------------------------------- left-side-bar ---------------------------------->
            <div class="left-sidebar">
               <nav class="left-menu">
                  <!-- <ul >
                     <li><i class="fa fa-long-arrow-left fa-2x arrow icon_click"></i></li>
                     </ul> -->
                  <ul class="">
                     <li>
                        <a href="#">
                           <div class="noti left_click mt-5">
                              <div class="profile ">
                                 <img src="img/profile.png" class="img-responsive">
                              </div>
                              <span class="notification">5</span>
                           </div>
                        </a>
                     </li>
                  </ul>
                  <ul class="logout">
                     <li class="remove_click">
                        <a href="#">
                        <i class="fa fa-long-arrow-right fa-2x"></i>
                        </a>
                     </li>
                     <div class="main_logout">
                        <p>Logout</p>
                        <div class="profile ">
                           <img src="img/profile.png" class="img-responsive">
                        </div>
                        <div class="txt_3">
                           <h3>Andrew Smith</h3>
                           <p>Director</p>
                        </div>
                     </div>
                     <li style="display:flex; margin-top:20px"> <img src="img/icons/edit-details.png" class="pl">
                        <a  href="#" > Edit My Details  </a>
                     </li>
                     <!-- <div class="dropdown-divider"></div> -->
                     <li style="display:flex; margin-top:20px"> <img src="img/icons/change-password.png" class="pl">
                        <a href="#" >Change Password</a>
                     </li>
                  </ul>
                  <ul>
                  </ul>
               </nav>
            </div>
            <!----------------------------------------- left-side-bar ---------------------------------->
            <script src="js/sidebar.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
            <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js" type="text/javascript"></script> -->
            <script src="js/calender.js"></script>
         </body>
      </html>
      <?php // include_once ('footer.php'); ?>