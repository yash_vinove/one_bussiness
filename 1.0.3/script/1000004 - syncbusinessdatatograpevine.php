<?php
//ini_set('memory_limit', '64M');
include('1000003 - config.php');
include('grapevineconfig.php');

$date = date('Y-m-d');
$datetime = date('Y-m-d H:i:s');

$date7 = date("Y-m-d", strtotime("- 7 days"));
$date30 = date("Y-m-d", strtotime("- 30 days"));

if (!$dbconn){
    echo "<br/>Error in connecting to postgresql server.<br/>";
}else
{
    $sql = "SELECT * FROM posts WHERE post_type = 'business' AND updated_at <= '$date7' LIMIT 500 ";
    $records = pg_query($dbconn, $sql) or die("Error in fetching data!");
    $data = pg_fetch_all($records);
    foreach($data as $res)
    {
        //echo "<br/>".json_encode($res);
        $postid = $res['id'];
        $name = $res['title'];
        $cellnumber = $res['cell_number'];
        $email = $res['email'];
        $website = $res['website'];
        $latitude = $res['latitude'];
        $longitude = $res['longitude'];
        $address = $res['street_1'];

        $salt = "h3f8s9en20vj3";
        $encryptedname = openssl_encrypt($name,"AES-128-ECB",$salt);
        $encryptedemail = openssl_encrypt($email,"AES-128-ECB",$salt);
        $encryptedaddress1 = openssl_encrypt($address,"AES-128-ECB",$salt);
        $encryptedphone = openssl_encrypt($cellnumber,"AES-128-ECB",$salt);
        
        $name = str_replace("'","\'",$res['title']);

        // Checking if post is already present in OneBusiness -- If no, then insert else update
        $checkdb = "SELECT * FROM zzbusiness INNER JOIN customer ON zzbusiness.customerid = customer.customerid 
        WHERE zzbusiness.grapevinepostid = $postid";
        $checkdb = mysql_query($checkdb);
        if(mysql_num_rows($checkdb) == 0)
        {
            $insertcustomerrecord = "INSERT INTO customer (customername, datecreated, customertypeid, salesstageid, address1, countryid, emailaddress, phonenumber, latitude, longitude, businessunitid, website) 
            VALUES ('$encryptedname', '$date', 1000001, 1000001, '$encryptedaddress1', 1000001, '$encryptedemail', '$encryptedphone', '$latitude', '$longitude', 1000001, '$website')";
            //echo "<br/><br/>SQL : ".$insertcustomerrecord;
            $insertcustomerrecord = mysql_query($insertcustomerrecord);
            $insertid = mysql_insert_id();

            $insertbusinessrecord = "INSERT INTO zzbusiness (zzbusinessname, customerid, datecreated, grapevinepostid, lastsyncedtograpevine) 
            VALUES ('$name', $insertid, '$date', $postid, '$datetime')";
            $insertbusinessrecord = mysql_query($insertbusinessrecord);
            
        }
        else
        {
            while($fetchdata = mysql_fetch_array($checkdb))
            {
                $customerid = $fetchdata['customerid'];
                $oldname = $fetchdata['customername'];
                $oldemail = $fetchdata['emailaddress'];
                $oldphone = $fetchdata['phonenumber'];
                $oldaddress = $fetchdata['address1'];

                $needupdate = array();
                if($oldname != $encryptedname)
                {
                    array_push($needupdate, "customername = '$encryptedname'");
                }
                if($oldemail != $encryptedemail)
                {
                    array_push($needupdate, "emailaddress = '$encryptedemail'");
                }
                if($oldphone != $encryptedphone)
                {
                    array_push($needupdate, "phonenumber = '$encryptedphone'");
                }
                if($oldaddress != $encryptedaddress1)
                {
                    array_push($needupdate, "address1 = '$encryptedaddress1'");
                }

                if(count($needupdate) > 0)
                {
                    $needupdate = implode(",", $needupdate);
                    $updatecustomer = "UPDATE customer SET ".$needupdate." WHERE customerid = $customerid"; 
                    $updatecustomer = mysql_query($updatecustomer);
                }

                $updatesql = "UPDATE zzbusiness SET lastsyncedtograpevine = '$datetime'
                WHERE zzbusiness.customerid = $customerid ";
                $updatesql = mysql_query($updatesql);
            }
        }
    }
}

// Insert into Grapevine
$businessdata = "SELECT * FROM zzbusiness INNER JOIN customer ON zzbusiness.customerid = customer.customerid
LEFT JOIN structurefunctiondocument ON structurefunctiondocument.structurefunctionid = 1000001 AND structurefunctiondocument.rowid = zzbusinessid
    WHERE grapevinepostid = 0 AND zzbusiness.lastsyncedtograpevine <= '$date30'";

$businessdata = mysql_query($businessdata);
while($fetchrow = mysql_fetch_array($businessdata))
{
    $zzbusinessid = $fetchrow['zzbusinessid'];
    $structurefunctiondocumentname = $fetchrow['structurefunctiondocumentname'];
    $salt = "h3f8s9en20vj3";

    $title = str_replace("'","\'",$fetchrow['zzbusinessname']);
    $subcategoryid = $fetchrow['subcategoryid'];
    $cell_number = openssl_decrypt($fetchrow['phonenumber'],"AES-128-ECB",$salt);
    $email = openssl_decrypt($fetchrow['emailaddress'],"AES-128-ECB",$salt);
    $website = $fetchrow['website'];
    $latitude = $fetchrow['latitude'];
    $longitude = $fetchrow['longitude'];
    $address1 = openssl_decrypt($fetchrow['address1'],"AES-128-ECB",$salt);
    $address2 = openssl_decrypt($fetchrow['address2'],"AES-128-ECB",$salt);
    $address3 = openssl_decrypt($fetchrow['address3'],"AES-128-ECB",$salt);
    $towncity = openssl_decrypt($fetchrow['towncity'],"AES-128-ECB",$salt);
    $stateregion = openssl_decrypt($fetchrow['stateregion'],"AES-128-ECB",$salt);
    $postzipcode = openssl_decrypt($fetchrow['postzipcode'],"AES-128-ECB",$salt);

    $address = $address1." ".$address2." ".$address3." ".$towncity." ".$stateregion." ".$postzipcode;

    // Check if any post is present with the same data
    $checkgrapevine = "SELECT * FROM posts WHERE post_type = 'business' AND title = '$title' AND email = '$email' AND website = '$website'";
    $checkgrapevine = pg_query($dbconn, $checkgrapevine) or die("Error in fetching data!");
    if(pg_num_rows($checkgrapevine) > 0)
    {
        $grapevinedata = pg_fetch_all($checkgrapevine);
        foreach($grapevinedata as $result)
        {
            $postid = $result['id'];
            $cellnumber = $result['cell_number'];
            $email = $result['email'];
            $website = $result['website'];
            $latitude = $result['latitude'];
            $longitude = $result['longitude'];

            $grapevineupdate = array();
            // Updating empty fields only in Grapevine -- Don't overwrite Grapevine data
            if($cellnumber == '' || $cellnumber == null)
            {
                array_push($grapevineupdate, "cell_number = '$cell_number'");
            }
            if($email == '' || $email == null)
            {
                array_push($grapevineupdate, "email = '$email'");
            }
            if($website == '' || $website == null)
            {
                array_push($grapevineupdate, "website = '$website'");
            }
            if($latitude == '' || $latitude == null)
            {
                array_push($grapevineupdate, "latitude = '$latitude'");
            }
            if($longitude == '' || $longitude == null)
            {
                array_push($grapevineupdate, "longitude = '$longitude'");
            }

            if(count($grapevineupdate) > 0)
            {
                $needupdate = implode(",", $grapevineupdate);
                $updatepost = "UPDATE posts SET ".$needupdate." WHERE id = $postid"; 
                $updatepost = pg_query($dbconn, $updatepost);
            }

            $updatesql = "UPDATE zzbusiness SET grapevinepostid = $postid, lastsyncedtograpevine = '$datetime' 
            WHERE zzbusinessid = $zzbusinessid ";
            $updatesql = mysql_query($updatesql);
        }
    }
    else
    {
        // Insert into Grapevine
        $sqlquery = "INSERT INTO posts (datetime, subcategory_id, title, cell_number, email, website, latitude, longitude, street_1, post_type, created_at, updated_at) 
        VALUES ('$datetime', $subcategoryid, '$title', '$cell_number', '$email', '$website', '$latitude', '$longitude', '$address', 'business', '$datetime', '$datetime') RETURNING id";
        //echo "<br/>pgquery: ".$sqlquery;
        $pgrecords = pg_query($dbconn, $sqlquery) or die("Error in inserting data!");
        $lastid = pg_fetch_row($pgrecords);
        // Get inserted id
        $postid = $lastid[0];
        
        // Check if structurefunctiondocument is present or not
        if($structurefunctiondocumentname != NULL)
        {
            $returnInfo = uploadImage($structurefunctiondocumentname, $postid);
            if($returnInfo)
            {
                $filename = "uploads/post_pictures/".$returnInfo;
                
                $updatepostquery = "UPDATE posts SET main_picture = '$filename' WHERE id = $postid";
                $updatepostquery = pg_query($dbconn, $updatepostquery);
                
                $postpictureinsert = "INSERT INTO post_pictures (post_id, description, picture, created_at, updated_at) VALUES ('$postid', '', '$filename', '$datetime', '$datetime')";
                
                $pgrecords = pg_query($dbconn, $postpictureinsert);
            }
        }
        
        
        // Update zzbusiness.grapevinepostid field
        $updatesql = "UPDATE zzbusiness SET grapevinepostid = $postid, lastsyncedtograpevine = '$datetime' 
        WHERE zzbusinessid = $zzbusinessid ";
        $updatesql = mysql_query($updatesql);
    }
}
pg_close($dbconn);

// function for connecting to grapevine server using SFTP
function uploadImage($structurefunctiondocumentname, $postid)
{
    require_once 'phpseclib/Net/SSH2.php';
    require_once 'phpseclib/Net/SFTP.php';
    require_once 'phpseclib/Crypt/RSA.php';
    require_once 'phpseclib/Math/BigInteger.php';
    
    set_include_path(get_include_path() . PATH_SEPARATOR . 'phpseclib');
    
    $rsa = new Crypt_RSA();
    $rsa->loadKey(file_get_contents("grapevine_new.pem"));
    $sftp = new Net_SFTP('3.13.197.15', 22);
    if (!$sftp->login("ubuntu", $rsa)) {
        return false;
    }
    
    $sftp->chdir('/var/www/html/grapevine/public/uploads/post_pictures');
    
    $rand = substr(md5(microtime()),0,20);
    //echo $rand;
    
    $remoteFileName = $postid." - ".$rand.".jpg";
    
    $sftp->put($remoteFileName, '../../zzbusiness/files1/'.$structurefunctiondocumentname, NET_SFTP_LOCAL_FILE);
    
    if($sftp->size($remoteFileName) > 0)
    {
        return $remoteFileName;
    }
    else
    {
        return false;
    }
    
    
}

?>