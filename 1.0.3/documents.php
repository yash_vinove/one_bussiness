<?php session_start(); ?>
<?php include('config.php'); ?>
<?php include('sessionconfig.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<title>OneBusiness</title>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script type="text/javascript">
    function redirectMe (sel) {
        var url = sel[sel.selectedIndex].value;
        window.location = url;
    }
    </script>
    <link rel='stylesheet' type='text/css' href='style.php' />
</head>
  
<body class='body'>
	<?php include_once ('headerthree.php');	?>
	
	
	<?php
	//LANGUAGE COLLECTION SECTION
	$mysqli = new mysqli($server, $user_name, $password, $database);
	$languageid = mysqli_real_escape_string($mysqli, $_SESSION['languageid']);
	if($stmt = $mysqli->prepare("SELECT languagerecordid, languagerecordtextname FROM $masterdatabase.languagerecordtext 
	where languageid = ? and languagerecordid in (14,19,93,94,95,96,97,98,99)")){
		$stmt->bind_param('i', $languageid);
		$stmt->execute();
		$result = $stmt->get_result();
		while ($row = $result->fetch_assoc()){
			$langid = $row['languagerecordid'];
			${"langval$langid"} = $row['languagerecordtextname'];
			//echo "<br/>".$langid." -".${"langval$langid"};
		}
	}
	$mysqli->close();
	
	
	$structurefunctionname = isset($_GET['structurefunctionname']) ? $_GET['structurefunctionname'] : '';
	$rowid = isset($_GET['rowid']) ? $_GET['rowid'] : '';
   	$submit = isset($_POST['submit']) ? $_POST['submit'] : ''; 
	
			   
   ?>
   
	<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding:10px;">
		<!--search results header bar for desktop-->
		<div class='bodyheader'>
			<h3><?php echo $langval93 ?></h3>
		</div>
		
		<div class='bodycontent'>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			</div>
			
			<?php 
			$submitdoc = isset($_POST['submitdoc']) ? $_POST['submitdoc'] : ''; 
			$doctype = isset($_POST['doctype']) ? $_POST['doctype'] : ''; 
			$database2 = str_replace("andrewnorth_", "", $database);
			if($submitdoc) {
				$filename=$_FILES["file"]["name"];
				
				if ($filename<>"" && $doctype<>"") {
					
     				//check if directory exists for this structurefunction
					$filedir = '../documents/'. $database2."/sf/".$structurefunctionname."/";
					if(file_exists($filedir)){
						//get current max directory
						$files = scandir($filedir);
						$results = array();
						foreach($files as $key => $value){
					    	$path = realpath($filedir.DIRECTORY_SEPARATOR.$value);
					      	if(is_dir($path)) {
					      		if (strpos($path, 'files') !== false) {
									array_push($results, $path);  	
								}																			       	
					      	}
					  	}
						//echo json_encode($results);
						//die();
						if(count($results) == 0){
							$file_directory = "../documents/".$database2."/sf/".$structurefunctionname."/files1/";
							if(!file_exists($file_directory)){
								mkdir($file_directory);
							}
						}	
						if(!isset($file_directory)){
							$numbers = array();
							foreach($results as $folder){
								$folderno = substr($folder, strpos($folder, "files") + 5);
								//echo "<br/>folderno: ".$folderno;	
								array_push($numbers, $folderno);																		
							}
							$maxfolder = max($numbers);
							//echo "<br/>maxfolder: ".$maxfolder;		
							
							//check size of folder
							$fd = "../documents/".$database2."/sf/".$structurefunctionname."/files".$maxfolder."/";
							$output = exec('du -sk ' . $fd);
							$repositorysize = trim(str_replace($fd, '', $output));
							$numberfiles = scandir($fd);
							$numberfiles = count($numberfiles) - 1;
							//echo "<br/>repositorysize - ".$repositorysize."<br/>";	
							//echo "<br/>numberfiles - ".$numberfiles."<br/>";	
							if($repositorysize >= 300000000 || $numberfiles >= 5000){
								$folderno = $maxfolder + 1;
								$file_directory = "../documents/".$database2."/sf/".$structurefunctionname."/files".$folderno."/";
								if(!file_exists($file_directory)){
									mkdir($file_directory);
								}
							}
							else {
								$file_directory = $fd;							
							}																								
						}
					}
					else {
						$file_directory = "../documents/".$database2."/sf/".$structurefunctionname."/";
						mkdir($file_directory);
					}
					
					//save the file
					$filename=$_FILES["file"]["name"];
					$newfilename=$rowid." - ".$filename;
					$url = $file_directory.$newfilename;

					// if (@move_uploaded_file($_FILES["file"]["tmp_name"], $url)) {
					// 	print "Received {$_FILES['file']['name']} - its size is {$_FILES['file']['size']}";
					// } else {
					// 	print "Upload failed!";
					// 	print_r(error_get_last());
					// }

					move_uploaded_file($_FILES["file"]["tmp_name"], $url);
					$urlstart = str_replace("../documents/", "", $file_directory);
					$urlstart = $urlstart.$newfilename;
					
					$mysqli = new mysqli($server, $user_name, $password, $database);					$structurefunctionname = mysqli_real_escape_string($mysqli, $structurefunctionname);					if($stmt = $mysqli->prepare("select * from structurefunction
					where tablename = ?")){					   $stmt->bind_param('s', $structurefunctionname);					   $stmt->execute();					   $result = $stmt->get_result();					   if($result->num_rows > 0){					    	while($row = $result->fetch_assoc()){					     		$structurefunctionid = $row['structurefunctionid'];					    	}					   	}					}					$mysqli->close();

     				$date = date("Y-m-d");
					
     				//insert into structurefunctiondocument
     				$mysqli = new mysqli($server, $user_name, $password, $database);					$newfilename = mysqli_real_escape_string($mysqli, $newfilename);
     				$date = mysqli_real_escape_string($mysqli, $date);
     				$urlstart = mysqli_real_escape_string($mysqli, $urlstart);
     				$doctype = mysqli_real_escape_string($mysqli, $doctype);
     				$structurefunctionname = mysqli_real_escape_string($mysqli, $structurefunctionname);
     				$rowid = mysqli_real_escape_string($mysqli, $rowid);					if($stmt = $mysqli->prepare("insert into structurefunctiondocument (structurefunctiondocumentname, datecreated,
     				documenturl, doctypename, structurefunctionid, rowid) 
     				values (?, ?, ?, ?, ?, ?)")){					   $stmt->bind_param('ssssii', $newfilename, $date, $urlstart, $doctype, $structurefunctionid, $rowid);					   $stmt->execute();					   					}					$mysqli->close();
     				
     				
     				
     				//redirect to any additional inserted logic pages
     				$mysqli = new mysqli($server, $user_name, $password, $database);					$structurefunctionname = mysqli_real_escape_string($mysqli, $structurefunctionname);					if($stmt = $mysqli->prepare("select * from structurefunction 
				   	inner join structurefunctionpage on structurefunction.structurefunctionid = structurefunctionpage.structurefunctionid
					where tablename = ?")){					   $stmt->bind_param('s', $structurefunctionname);					   $stmt->execute();					   $result = $stmt->get_result();					   if($result->num_rows > 0){					    	while($structurefunctionrow = $result->fetch_assoc()){					     		$runscriptdocupload = $structurefunctionrow['runscriptdocupload'];					    	}					   	}					}					$mysqli->close();

     				if($runscriptdocupload <> ""){
					   	include_once('./'.$runscriptdocupload);
					}		
   				}
   				else{
					echo "<p class='background-warning'>Please select a document and a Document Type.</p><br/>";   				
   				}
			}
			
			
			//get dropdown options
			$mysqli = new mysqli($server, $user_name, $password, $database);			$structurefunctionname = mysqli_real_escape_string($mysqli, $structurefunctionname);			if($stmt = $mysqli->prepare("select doctype from structurefunction 
			where tablename = ?")){			   $stmt->bind_param('s', $structurefunctionname);			   $stmt->execute();			   $result = $stmt->get_result();			   if($result->num_rows > 0){			    	while($getoptionrow = $result->fetch_assoc()){			     		$doctype = $getoptionrow['doctype'];			    	}			   	}			}			$mysqli->close();
			
			?>
							
			<form action="" method="post" enctype="multipart/form-data" name="form1" id="form1"> 
				<table><tr><td style="width:200px;height:40px;">			  	 
			  	<label for="file"><?php echo $langval98 ?> *</label>
			  	</td>
				<td><input name="file" type="file" id="file" /></td></tr> 
				<tr><td><label for="DocType">Document Type *</label></td>
	    	  	<td><?php
    		  	echo "<select class='form-control' name='doctype'>";
	        	$array = explode(',', $doctype); 
				echo "<option value=''>".$langval19."</option>";
	         	foreach($array as $value) {
	         		echo "<option value='".$value."'>".$value."</option>";	             
	         	}
	     		echo "</select>";
	      		?></td></tr></table>
				<input class="btn btn-primary" type="submit" name="submitdoc" value="<?php echo $langval99 ?>" /> 																									
			</form> 
			<br/>
			<script>
			$(document).ready(function(){
			    $('#myTable4').dataTable({
			 });
			});
			</script>
			<div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
			<br/>
			<?php			
			$mysqli = new mysqli($server, $user_name, $password, $database);			$structurefunctionname = mysqli_real_escape_string($mysqli, $structurefunctionname);			$rowid = mysqli_real_escape_string($mysqli, $rowid);			if($stmt = $mysqli->prepare("select structurefunctiondocument.datecreated, doctypename, documenturl, structurefunctiondocumentid, 
		   	structurefunctionname, structurefunctiondocumentname, tablename
		   	from structurefunctiondocument
		   	inner join structurefunction on structurefunction.structurefunctionid = structurefunctiondocument.structurefunctionid
		   	where tablename = ? and rowid = ?
		   	order by structurefunctiondocument.datecreated desc")){			   $stmt->bind_param('si', $structurefunctionname, $rowid);			   $stmt->execute();			   $result = $stmt->get_result();			   if($result->num_rows > 0){
				   	echo "<div class='table-responsive'><table id='myTable4' class='table table-bordered'>";
		         	echo "<thead><tr>";
		        	echo "<th>".$langval14."</th>";
		        	echo "<th>".$langval95."</th>";
		        	echo "<th>".$langval96."</th>";
		         	echo "<th>".$langval97."</th>";
		         	echo "</tr></thead>";	         			    	while($row = $result->fetch_assoc()){			     		$structurefunctiondocumentid = $row['structurefunctiondocumentid'];
		         		$structurefunctionname = $row['tablename'];
		         		echo "<tr>";
		         		echo '<td>';
		        		?>
		        		<select class='form-control' name='action' id='action' onchange='redirectMe(this);'>";
			         	<?php
			         	echo "<option value=''>".$langval14.'</option>';
			         	echo "<option value='documentdelete.php?structurefunctiondocumentid=".$structurefunctiondocumentid."&structurefunctionname=".$structurefunctionname.'&rowid='.$rowid."'>".$langval94.'</option>';
			     		echo '</select>';
			        	echo '</td>';
			         	echo "<td>".$row['datecreated']."</td>";
	        			echo "<td>".$row['doctypename']."</td>";
	            		echo "<td><a href='../documents/".$row['documenturl']."'>".$row['structurefunctiondocumentname']."</a></td>";
	            		echo "</tr>";			    	}
			    	echo "</table></div>";			   	}			}			$mysqli->close();
		   	    		
       ?>
			
			</div>		
		<br/><br/><br/><br/><br/><br/>
		<br/><br/><br/><br/><br/><br/>
		<br/><br/><br/><br/><br/><br/>
		<br/><br/><br/><br/><br/><br/>
		<br/><br/><br/><br/><br/><br/>
		<br/><br/><br/><br/><br/><br/>
		
		
	</div>	
	</div>	
	<?php include_once ('footer.php'); ?>
</body>
