<?php
$rowid = isset($_GET['rowid']) ? $_GET['rowid'] : ''; 

$file = '../../documents/onebusinessmaster/Tristom Labs Ltd Invoice - '.$rowid.'.pdf';

//echo $file;

if (file_exists($file)) {
	//echo "here";
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($file).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
    exit;
}

//redirect back to the page
$url = '../pagegrid.php?pagetype=accountbillinginvoice&updated=3';
echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">';   				


?>
