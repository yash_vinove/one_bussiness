<?php 
//echo "userid: ".$userid;
$date = date("Y-m-d");
$submit = isset($_POST['submit']) ? $_POST['submit'] : ''; 
if($submit) {
	
	//delete existing import for this user, if there is one
	$mysqli = new mysqli($server, $user_name, $password, $database);
	$userid = mysqli_real_escape_string($mysqli, $userid);
	if($stmt = $mysqli->prepare("delete from datauploadtemp where userid = ?")){
	   $stmt->bind_param('i', $userid);
	   $stmt->execute();				   
	}
	$mysqli->close();
	
	$validation = ""; 
	
	if ($_FILES['csv']['size'] > 0) {
		//get the csv file 
	   	$file = $_FILES['csv']['tmp_name'];
	   	ini_set('auto_detect_line_endings',TRUE);
	   	$handle = fopen($file,"r"); 
	   	$counter56 = 1;
		for($i = 0; $i <= 100; $i++) {
  			${"content$i"} = "";
		}
		
		$data = array('','','');
		//echo "<br/><br/>data: ".json_encode($data);
		$numcols = 0;
		//loop through the csv file and insert into database
		$p = 0;
	   	do { 
	   		if ($data[0] || $data[1] || $data[2]) {
	   			$p = $p + 1;
	   			//check for no apostrophies and html tags in all fields
	   			//echo "<br/>data: ".$data[0];
	   			if($counter56 == 1){
	   				$counter56 = $counter56+1;
		     		for($i = 0; $i <= 100; $i++) {
		     			if(!isset($data[$i])) {
							$data[$i] = '';					     			
		     			}
		     			${'content'.$i} = str_replace("'", "", $data[$i]);
		     			${'content'.$i} = str_replace('"', "", $data[$i]);
		     			//echo "<br/>content: ".${'content'.$i};
		     			if(${'content'.$i} == ''){
		     				${'content'.$i} = "";
		     				$numcols = $i;
		     				break;				     			
		     			}
		     		}
		     		$numcols = $numcols-1;
		     	}
		     	if($counter56 >= 1){
		     		for($i = 0; $i <= $numcols; $i++) {
		     			${'content'.$i} = str_replace("'", "", $data[$i]);
		     			${'content'.$i} = str_replace('"', "", $data[$i]);
		     			//echo "<br/>content: ".${'content'.$i};
		     		}
		     	}
	     		//add record to database
	     		if($p <> 1){
		     		$querybuilder = "INSERT INTO datauploadtemp (userid, ";
		     		for($i = 1; $i <= 100; $i++) {
	  					$querybuilder = $querybuilder."field".$i.", ";
					}
					$querybuilder = rtrim($querybuilder, ', ');
		     		$querybuilder = $querybuilder.") VALUES (";
		         	$querybuilder = $querybuilder." '".$userid."',";
		         	for($i = 0; $i <= 99; $i++) {
		         		if($i == 1){
							${'content'.$i} = (int)(${'content'.$i}+1-1);     
							//echo "<br/>i1: ".${'content'.$i};
							 		
		         		}
			         	$querybuilder = $querybuilder." '".addslashes(${'content'.$i})."', ";
		         	}
		         	$querybuilder = rtrim($querybuilder, ', ');
		         	$querybuilder = $querybuilder.")";
		     		$query = mysql_query($querybuilder); 
		        	//echo "<br/>querybuilder: ".$querybuilder;
	        	}
	      	} 
	   } while ($data = fgetcsv($handle, 0)); 
	}	
	
	$p = $p - 1;
	
	echo "<br/><br/>Number Inserted From CSV File: ".$p;
	
	//get orderid's combined
	$getdata = mysql_query("select * from datauploadtemp 
	where userid = $userid");	
	$orders = array();
	while($getdatarow = mysql_fetch_array($getdata)){
		$orderid = $getdatarow['field1'];
		$orderitemid = $getdatarow['field2'];
		$orderidcombined = $orderid."///".$orderitemid;
		if($p <> 1){
			array_push($orders, $orderidcombined);
		}	
	}
	
	$updateid = mysql_query("update datauploadtemp set field50 = concat (field1, '///', field2) 
	where userid = $userid");
	
	
	if($p >= 1){
		
		//PROCESS ORDERS INTO ZZCUSTOMERORDERS TABLE
		//check for duplicates 		
		//echo "<br/><br/>orders: ".json_encode($orders);
		$searchorders = "select combinedorderid from zzcustomerorder where combinedorderid in (";
		foreach($orders as $orderidcombined){
			$searchorders = $searchorders.'"'.$orderidcombined.'",';	
		}
		$searchorders = rtrim($searchorders, ",");
		$searchorders = $searchorders.")";
		//echo "<br/><br/>searchorders: ".$searchorders;
		$searchorders = mysql_query($searchorders);
		$deleteorders = "delete from datauploadtemp where userid = $userid and field50 in (";
		$z = 0;
		while($searchorderrow = mysql_fetch_array($searchorders)){
			$foundcombinedorderid = $searchorderrow['combinedorderid'];
			//echo "<br/>foundcombinedorderid: ".$foundcombinedorderid;
			$deleteorders = $deleteorders.'"'.$foundcombinedorderid.'",';
			$z = $z + 1;
		}
		
		//remove orders that we already have
		echo "<br/>Number Ignored As We Already Have Them: ".$z;
		if($z >= 1){
			$deleteorders = rtrim($deleteorders, ",");
			$deleteorders = $deleteorders.")";
			$deleteorders = mysql_query($deleteorders);
		}
		
		
		//IMPORT THE REMAINING RECORDS
		$orderimport = mysql_query("select * from datauploadtemp 
		where userid = $userid");
		$k = 0;
		while($orderimportrow = mysql_fetch_array($orderimport)){
			$orderid = $orderimportrow['field1'];
			$orderitemid = $orderimportrow['field2'];
			$purchasedate = $orderimportrow['field3'];
			$emailaddress = mysql_real_escape_string($orderimportrow['field8']);
			$buyername = mysql_real_escape_string($orderimportrow['field17']);
			$sku = mysql_real_escape_string($orderimportrow['field11']);
			$phonenumber = mysql_real_escape_string($orderimportrow['field10']);
			$productname = mysql_real_escape_string($orderimportrow['field12']);
			$quantitypurchased = mysql_real_escape_string($orderimportrow['field13']);
			$shippingtype = mysql_real_escape_string($orderimportrow['field16']);
			$address1 = mysql_real_escape_string($orderimportrow['field18']);
			$address2 = mysql_real_escape_string($orderimportrow['field19']);
			$address3 = mysql_real_escape_string($orderimportrow['field20']);
			$address4 = mysql_real_escape_string($orderimportrow['field21']);
			$address5 = mysql_real_escape_string($orderimportrow['field22']);
			$address6 = mysql_real_escape_string($orderimportrow['field23']);
			$isprime = mysql_real_escape_string($orderimportrow['field29']);
			$combinedorderid = mysql_real_escape_string($orderimportrow['field50']);
			
			$name = explode(" ", $buyername);
			if(isset($name[0])){
				$firstname = $name[0];
			}
			else {
				$firstname = "";			
			}
			if(isset($name[1])){
				$lastname = $name[1];
			}
			else {
				$lastname = "";			
			}
			
			$b = 1;
			while($b <= $quantitypurchased){
				$insertorder = "insert into zzcustomerorder (source, orderid, orderitemid, emailaddress, 
				firstname, lastname, buyername, sku, productname, purchasedate, combinedorderid, address1, address2, address3, 
				address4, address5, address6, phonenumber, datecreated, shippingtype, amazonisprime) 
				values ('Amazon', '$orderid', '$orderitemid', '$emailaddress', '$firstname', '$lastname', '$buyername', 
				'$sku', '$productname', '$purchasedate', '$combinedorderid', '$address1', '$address2', '$address3', '$address4', 
				'$address5', '$address6', '$phonenumber', '$date', '$shippingtype', $isprime)";
				//echo "<br/>insertorder: ".$insertorder;
				$insertorder = mysql_query($insertorder);
				$b = $b + 1;
				$k = $k + 1;
			}
		}
		
		echo "<br/>Number Of New Orders Imported: ".$k;
		
		if($k >= 1){
			$updateproductid = mysql_query("update zzcustomerorder 
			inner join product on product.productuniqueid = zzcustomerorder.sku 
			set zzcustomerorder.productid = product.productid
			where zzcustomerorder.productid = 0");	
			$numberupdated = mysql_affected_rows();
			
			echo "<br/>Number Of Rows Updated With Product ID: ".$numberupdated;
			
			$checknumberwithoutproductid = mysql_query("select productid from zzcustomerorder 
			where productid = 0");
			$numberwithoutproductid = mysql_num_rows($checknumberwithoutproductid);
			echo "<br/>Total Number Of Rows Without Matched Product: ".$numberwithoutproductid;	
		}
		
	}
	
	
	
	
	
	//reassign products that have duplicate purchases and can be moved over to a different product
	//echo "<br/><b>REASSIGN PRODUCTS THAT HAVE DUPLICATE PURCHASES</b><br/>";
	$reassignproduct = mysql_query("SELECT orderid, count(zzcustomerorderid) as 'numpurchased', sku, productname, 
	buyername, zzproductdata.productid, zzproductdata.masterproductid, zzproductdata.itempackagequantity, sizename, fulfillmentchannel,
	zzproductdata.colorname
	FROM zzcustomerorder 
	inner join zzproductdata on zzproductdata.productid = zzcustomerorder.productid
	where masterproductid <> 0 and itempackagequantity <> 0
	group by orderid, sku
	having count(zzcustomerorderid) >= 2");
	while($reassignproductrow = mysql_fetch_array($reassignproduct)){
		$orderid = $reassignproductrow['orderid'];	
		$numpurchased = $reassignproductrow['numpurchased'];	
		$sku = $reassignproductrow['sku'];	
		$productname = $reassignproductrow['productname'];	
		$buyername = $reassignproductrow['buyername'];	
		$masterproductid = $reassignproductrow['masterproductid'];	
		$sizename = $reassignproductrow['sizename'];	
		$fulfillmentchannel = $reassignproductrow['fulfillmentchannel'];	
		$itempackagequantity = $reassignproductrow['itempackagequantity'];	
		$colorname = $reassignproductrow['colorname'];
		
		echo "<br/>".$orderid." - ".$numpurchased." - ".$sku." - ".$buyername." - ".$masterproductid." - ".$itempackagequantity;
		echo "-".$sizename." - ".$fulfillmentchannel." - ".$colorname;
		
		$getproductids = mysql_query("SELECT zzcustomerorderid from zzcustomerorder 
		where sku = '$sku' and orderid = '$orderid'");
		$array1 = array();
		$k = 1;
		$zzcustomerorderidrest = "";
		while($getproductidsrow = mysql_fetch_array($getproductids)){
			$zzcustomerorderid = $getproductidsrow['zzcustomerorderid'];
			array_push($array1, $zzcustomerorderid);
			if($k == 1){
				$zzcustomerorderidfirst = $zzcustomerorderid;			
			}
			else {
				$zzcustomerorderidrest = $zzcustomerorderidrest.$zzcustomerorderid.", ";			
			}
			$k = $k + 1;
		}
		$zzcustomerorderidrest = rtrim($zzcustomerorderidrest, ", ");
		echo "<br/>zzcustomerorderidfirst: ".$zzcustomerorderidfirst;		
		echo "<br/>zzcustomerorderidrest: ".$zzcustomerorderidrest;		
		
		$totalnum = 0;
		if($itempackagequantity >= 1){	
			$totalnum = $numpurchased * $itempackagequantity;	

			$reassignproduct2 = mysql_query("select zzproductdataid, zzproductdata.productid, zzproductdata.masterproductid, 
			itempackagequantity, productname, productuniqueid 
			from zzproductdata 
			inner join product on product.productid = zzproductdata.productid
			where zzproductdata.masterproductid = $masterproductid and itempackagequantity = $totalnum 
			and sizename = '$sizename' and fulfillmentchannel = '$fulfillmentchannel'");
			if(mysql_num_rows($reassignproduct2) >= 1){
				$reassignproduct2row = mysql_fetch_array($reassignproduct2);
				$newproductid = $reassignproduct2row['productid'];
				$productname = $reassignproduct2row['productname'];
				$productuniqueid = $reassignproduct2row['productuniqueid'];
				
				echo "<br/>newproductid: ".$newproductid;
				echo "<br/>productname: ".$productname;
				echo "<br/>productuniqueid: ".$productuniqueid;
				
				if($zzcustomerorderidfirst > 0){
					$updatekeeprecord = mysql_query("update zzcustomerorder 
					set productid = $newproductid, productname = '$productname', sku = '$productuniqueid', ordervalue = 0 
					where zzcustomerorderid = $zzcustomerorderidfirst");				
				}
				
				if($zzcustomerorderidrest <> ''){
					$updaterestrecords = mysql_query("update zzcustomerorder 
					set productid = 9999999, productname = 'merged', sku = 'merged', ordervalue = 0 
					where zzcustomerorderid in ($zzcustomerorderidrest)");				
				}		
			}
			else 
			{
				$reassignproduct3 = mysql_query("select zzproductdataid, zzproductdata.productid, zzproductdata.masterproductid, 
				itempackagequantity, productname, productuniqueid 
				from zzproductdata 
				inner join product on product.productid = zzproductdata.productid
				where zzproductdata.masterproductid = $masterproductid and zzproductdata.colorname = '$colorname' 
				and sizename = '$sizename' and fulfillmentchannel = '$fulfillmentchannel'");

				if(mysql_num_rows($reassignproduct3) >= 1) {
					$arr = array();
					while($reassignproduct3row = mysql_fetch_array($reassignproduct3)) {
						array_push($arr, array(
							'zzproductdataid' => $reassignproduct3row['zzproductdataid'],
							'productid' => $reassignproduct3row['productid'],
							'itempackagequantity' => $reassignproduct3row['itempackagequantity'],
							'productname' => $reassignproduct3row['productname'],
							'productuniqueid' => $reassignproduct3row['productuniqueid']
						));
					}

					array_multisort(array_column($arr, 'itempackagequantity'), SORT_DESC, $arr);

					//print_r($arr);
					$num = $totalnum;
					$finalarr = array();

					foreach ($arr as $key => $value) 
					{
						$checkval = $value['itempackagequantity'];
						if($checkval <= $num && $num != 0) 
						{
							if($num%$checkval == 0) {
								array_push($finalarr, array('q' => $num/$checkval, 'val' => $checkval));
								$num = 0;
								break;
							}
							else {
								$remainder = $num%$checkval;
								array_push($finalarr, array('q' => floor($num/$checkval), 'val' => $checkval));
								$num = $remainder;   
							}
						}
					}

					if($num == 0) 
					{
						foreach($finalarr as $finalkey => $finalvalue)
						{
							$quantity = $finalvalue['q'];
							for($i=0;$i<$quantity;$i++)
							{
								$key2 = array_search($finalvalue['val'], array_column($arr, 'itempackagequantity'));
								$newproductid = $arr[$key2]['productid'];
								$productuniqueid = $arr[$key2]['productuniqueid'];
								$productname = $arr[$key2]['productname'];
								$zzcustomerorderidfirst = array_shift($array1);

								$updatekeeprecord = mysql_query("update zzcustomerorder 
								set productid = $newproductid, productname = '$productname', sku = '$productuniqueid', ordervalue = 0 
								where zzcustomerorderid = $zzcustomerorderidfirst");
							}
						}

						if(count($array1) > 0)
						{
							$zzcustomerorderidrest = implode(",", $array1);

							$updaterestrecords = mysql_query("update zzcustomerorder 
							set productid = 9999999, productname = 'merged', sku = 'merged', ordervalue = 0 
							where zzcustomerorderid in ($zzcustomerorderidrest)");
						}
					}
				}
			}
		}
	}
	
	//assign ebay pricing to records
	//echo "<br/><b>ASSIGN EBAY PRICING TO RECORDS</b><br/>";
	$updateebayprice = mysql_query("update zzcustomerorder 
	inner join zzdropshipproduct on zzdropshipproduct.productid = zzcustomerorder.productid
	set ordervalue = price");
	
	
	echo "<br/><br/>";
	
}


if(isset($validation)) {
	if($validation <> ""){
		echo "<br/><p class='background-warning'>".$validation."</p>";
	}
} 
if(isset($success)) {
	if($success <> ""){
		echo "<br/><p class='background-success'>".$success."</p>";
	}
} 
?>

<form action="" method="post" enctype="multipart/form-data" name="form1" id="form1"> 
	<b>Upload File</b><br/><br/> 
	<input name="csv" type="file" id="csv" /> 
	<br/>
	<input class="button-primary" type="submit" name="submit" value="Submit" /> 
</form> 	