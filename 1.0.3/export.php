<?php session_start(); ?>
<?php include('config.php'); ?>
<?php include('sessionconfig.php'); ?>
<?php
/*
 This file will generate our CSV table. There is nothing to display on this page, it is simply used
 to generate our CSV file and then exit. That way we won't be re-directed after pressing the export
 to CSV button on the previous page.
*/

//First we'll generate an output variable called out. It'll have all of our text for the CSV file.
$out = '';

//Next we'll check to see if our variables posted and if they did we'll simply append them to out.
$csvarray = array();
if (isset($_POST['query'])) {
	//build spreadsheet
	$rowquery = mysql_query($_POST['query']);
	if(isset($_POST['csv_hdr'])){
		$csv_hdr = $_POST['csv_hdr'];
		$exploded = array_filter(explode(",", $csv_hdr));
		array_push($csvarray, $exploded);
	}
	else {
		$csv_hdr = "";
		//construct headers
		$countoffields = 0;
		for($i = 0; $i < mysql_num_fields($rowquery); $i++) {
		    $field_info = mysql_fetch_field($rowquery, $i);
		    $csv_hdr .= $field_info->name;
		    if($i <> mysql_num_fields($rowquery)){ 
	   			$csv_hdr .= ",";	   			
	   		}
	   		$countoffields = $countoffields+1;
		}		
		$exploded = array_filter(explode(",", $csv_hdr));
		array_push($csvarray, $exploded);
	}
	//echo "csvarray: ".json_encode($csvarray);
	
	// Print the data
	
	$salt = "h3f8s9en20vj3";
	
	if($rowquery <> ""){
		while($row = mysql_fetch_row($rowquery)) {
		    $counter3 = 1;
		    $p = 0;
		    $csv_output = "";
		    foreach($row as $_column) {
		    	if($csvarray[0][$p] == "First Name" || $csvarray[0][$p] == "Firstname" || $csvarray[0][$p] == "Last Name"
		    	 || $csvarray[0][$p] == "Lastname" || $csvarray[0][$p] == "Email" || $csvarray[0][$p] == "Email Address"){
					$_column = $_column = openssl_decrypt($_column,"AES-128-ECB",$salt);	    	
		    	}
		    	$_column = str_replace('"', "", $_column);
		    	$_column = str_replace("'", "", $_column);
		    	$_column = str_replace("\n", "\r\n", $_column);
		    	$csv_output .= $_column;
		     	if($counter3 <> $countoffields){ 
	      			$csv_output .= " ||| ";
	      		}
	      		else {
	      			$csv_output .= "\n";
	      		}
	         	$counter3 = $counter3 +1;	 
	         	$p = $p +1;	 
		  	}
		   	$exploded = explode(" ||| ", $csv_output);
			array_push($csvarray, $exploded);
		}
	}
}
if (isset($csv_hdr)) {
$out .= $csv_hdr;
$out .= "\n";
}

if (isset($csv_output)) {
$out .= $csv_output;
}

//save the export to securitydataexport table 
$securitydate = date("Y-m-d");
$securitydatetime = date("Y-m-d H:i:s");
$securitydataexportname = "Export";
$pagetype = isset($_GET['pagetype']) ? $_GET['pagetype'] : '';
$pageexportedfrom = $pagetype;
$recordcount = $counter3;
$mysqli = new mysqli($server, $user_name, $password, $database);if($stmt = $mysqli->prepare("insert into securitydataexport (securitydataexportname, userid, pageexportedfrom, recordcount, 
dateexported, datetimeexported, disabled, datecreated, masteronly) 
values (?, ?, ?, ?, ?, ?, 0, ?, 0)")){
	$stmt->bind_param('sisisss', $securitydataexportname, $userid, $pageexportedfrom, $recordcount,
	$securitydate, $securitydatetime, $securitydate);
	$stmt->execute();					   }$mysqli->close();

//check if the user has exported more than 10,000 rows in last 24 hours
$mysqli = new mysqli($server, $user_name, $password, $database);$downloadedoneday = date('Y-m-d H:i:s', strtotime("- 1 day"));if($stmt = $mysqli->prepare("select sum(recordcount) as 'totalexported' from securitydataexport 
where userid = ? and datetimeexported >= ?")){   $stmt->bind_param('is', $userid, $downloadedoneday);   $stmt->execute();   $result = $stmt->get_result();
   if($result->num_rows > 0){    	while($gettotaldownloaded = $result->fetch_assoc()){     		$totalexported = $gettotaldownloaded['totalexported'];
			
			if($totalexported >= 10000){
				//log security incident
				$securityincidenttype = 'More Than 10,000 Data Records Exported In 1 Day';
				$securityuserid = $userid;
				$securityipaddress = '';
				include('securityemail.php');		 
			}   		   	}
   	}   }

//Now we're ready to create a file. This method generates a filename based on the current date & time.
$filename = "export"."_".date("Y-m-d_H-i",time());

$db5 = str_replace("andrewnorth_", "", $database);
$file = '../documents/'.$db5.'/'.$filename.'.csv';
$fp = fopen($file, 'w');

foreach ($csvarray as $fields) {
    fputcsv($fp, $fields);
}

fclose($fp);

header('Content-Description: File Transfer');
header('Content-Disposition: attachment; filename='.basename($file));
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize($file));
header("Content-Type: text/csv");
readfile($file);

//Exit the script
exit;
?>