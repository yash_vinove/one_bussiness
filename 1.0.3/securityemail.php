<?php

$securitydate = date("Y-m-d");
$securitydatetime = date("Y-m-d H:i:s");

$securityalertname = $securityincidenttype;
if($securityuserid <> 0){
	$securityalertname = $securityalertname." - ".$securityuserid;
}
if($securityipaddress <> 0){
	$securityalertname = $securityalertname." - ".$securityipaddress;
}


$securityusername = "";
if($securityuserid >= 1 && $securityuserid <> ''){
	$mysqli = new mysqli($server, $user_name, $password, $database);	if($stmt = $mysqli->prepare("select * from user where userid = ?")){
		$stmt->bind_param('i', $securityuserid);	   $stmt->execute();	   $result = $stmt->get_result();	   if($result->num_rows > 0){
	   		while($getuser = $result->fetch_assoc()){	     		$securityusername = $getuser['username'];
	     	}
	  	}
	}
}
//check if need to send email
$emailalertsent = 0; 
$mysqli = new mysqli($server, $user_name, $password, $database);if($stmt = $mysqli->prepare("select * from securityalertemailconfig where disabled = 0")){   $stmt->execute();   $result = $stmt->get_result();   if($result->num_rows > 0){
   		$emailalertsent = 1;    	while($getemail = $result->fetch_assoc()){     		$emailaddress = $getemail['emailaddress'];
     		$emailtext = "Hello <br/><br/>";
     		$emailtext = $emailtext."New security alert: <br/>";
     		$emailtext = $emailtext."<b>Alert Type: </b>".$securityincidenttype."<br/>";
     		$emailtext = $emailtext."<b>User ID: </b>".$securityuserid."<br/>";
     		$emailtext = $emailtext."<b>User Name: </b>".$securityusername."<br/>";
     		$emailtext = $emailtext."<b>IP Address: </b>".$securityipaddress."<br/>";
     		$emailtext = $emailtext."<b>Date/Time Added: </b>".$securitydatetime."<br/>";
     		$emailtext = $emailtext."<br/>Regards<br/>System Administrator";
     		
     		//echo "<br/><br/><b>SEND EMAIL</b>";
			$subject = $database.' - '.$securityalertname;
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			$headers .= 'From: <info@lam-way.com>' . "\r\n";
			mail($emailaddress,$subject,$emailtext,$headers);
   		}
   	}
}	

//add record to securityalert table
$mysqli = new mysqli($server, $user_name, $password, $database);$securityipaddress = mysqli_real_escape_string($mysqli, $securityipaddress);
if($stmt = $mysqli->prepare("insert into securityalert (securityalertname, alerttype, userid, ipaddress, emailalertsent, 
dateadded, datetimeadded, disabled, datecreated, masteronly) 
values (?, ?, ?, ?, ?, ?, ?, 0, ?, 0)")){
	$stmt->bind_param('ssisisss', $securityalertname, $securityincidenttype, $securityuserid, $securityipaddress, $emailalertsent, 
	$securitydate, $securitydatetime, $securitydate);   $stmt->execute();					   }$mysqli->close();

?>