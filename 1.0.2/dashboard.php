<?php session_start(); ?>
<?php
//deal with old redirect issues	
	$i = isset($_GET['i']) ? $_GET['i'] : '';
	if($i <> ""){
		$url = 'index.php';
		echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">'; 
		die;	
	}
?>
<?php include('config.php'); ?>
<?php include('sessionconfig.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<link href="./fontAwesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="screen" title="stylesheet" />

 	<title>Clubtray</title>

 	<!-- Bootstrap -->
 	<link href="bootstrap-3.3.1/dist/css/bootstrap.css" rel="stylesheet">

 	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
 	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">  
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script type="text/javascript">
    function redirectMe (sel) {
        var url = sel[sel.selectedIndex].value;
        window.location = url;
    }
    </script>
 	
</head>
  
<body class='greybody' style=''>

<?php include_once ('headerthree.php');	?>
	
	
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:10px;">
	<!--header bar for desktop-->
	<div style="padding:10px;background-color:#e9e9e9;border-bottom:1px solid #A0A0A0;">
		<b>Dashboard</b>
	</div>
	<div style="padding:10px;background-color:#ffffff;border-bottom:1px solid #A0A0A0;">
		
		<?php
			
		$getcharts = mysql_query("select * from chart 
		inner join chartsection on chart.chartsectionid = chartsection.chartsectionid
		where chart.disabled = 0 order by chartsectionname, sortorder asc");	
		$number = 1;
		
		for($i = 0; $i <= 100; $i++) {
  			${"jsontable$i"} = "";
  			${"getdata$i"} = "";
  			${"data$i"} = "";
  			${"string$i"} = "";
  			${"csv_hdr$i"} = "";
  			${"csv_output$i"} = "";
		}
		
		$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
	   if(!isset($_POST['submit'])) {
			$time = date("Y-m-d");
			$startdate=date('Y-m-d', strtotime('-3 year'));
			$enddate = date("Y-m-d");	 
		} 	
		else {
			$startdate = isset($_POST['startdate']) ? $_POST['startdate'] : '';
	   	$enddate = isset($_POST['enddate']) ? $_POST['enddate'] : '';			
		}
	
		?>			
			
		<form class="form-horizontal" action='dashboard.php' method="post">
			<div class="form-group">
				<label for="Start Date" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label">Start Date</label>
		    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		      	<input type="date" class="form-control" name="startdate" value="<?php echo $startdate ?>">
		    	</div>
		  		<label for="End Date" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label">End Date</label>
		    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		      	<input type="date" class="form-control" name="enddate" value="<?php echo $enddate ?>">
		    	</div>
		  	  	<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
		    		<div style="text-align:center;">
		      		<button type='submit' name='submit' value='Submit' class="btn btn-primary">Submit</button>																									
		    		</div>
		    	</div>		
			</div>
    	</form>
		
		<?php 
	  	$savedchartsectionname = "";	
		while($getchartrow = mysql_fetch_array($getcharts)){
			$chartname = $getchartrow['chartname'];
			$chartsectionname = $getchartrow['chartsectionname'];
			$chartsectionid = $getchartrow['chartsectionid'];
			
			//check permissions to section
			$getpermission = mysql_query("select * from userrole 
			inner join permissionroledashboardsection on permissionroledashboardsection.permissionroleid = userrole.permissionroleid
			where permissionroledashboardsection.disabled = '0' and userid = '$userid' and chartsectionid = '$chartsectionid'");
			
			if(mysql_num_rows($getpermission)>0) {
			
				//check if chart section has changed and write new section heading if so
				if($chartsectionname <> $savedchartsectionname) {
					echo "<h4 style='padding-left:5px;'>".$chartsectionname."</h4>";			
					$savedchartsectionname = $chartsectionname;
				}	
				
				$query = $getchartrow['query'];
				$querygroupby = $getchartrow['querygroupby'];
				$querywhere = $getchartrow['querywhere'];
				$datefilterfield = $getchartrow['datefilterfield'];
				$datefilterfieldoveride = $getchartrow['datefilterfieldoveride'];
				if($datefilterfieldoveride == 0){ 
					if($startdate <> '' && $enddate <> '') {
						$query = $query." where ".$datefilterfield.">='".$startdate."' and ".$datefilterfield."<='".$enddate."'";	
					}
					if($startdate <>'' && $enddate =='') {
						$query = $query." where ".$datefilterfield.">='".$startdate."'";	
					}
					if($startdate =='' && $enddate <>'') {
						$query = $query." where ".$datefilterfield."<='".$enddate."'";	
					}
				}
				$query = $query." ".$querywhere;
				$query = $query." ".$querygroupby;
				//echo $query;
				//echo $number;
				$charttype = $getchartrow['charttype'];
				
				
								
									${'getdata'.$number} = mysql_query($query);
									
									$num = mysql_num_rows(${'getdata'.$number});
									$numfields = mysql_num_fields(${'getdata'.$number});
									//echo $numfields;
									
									$string1 = "";
									$number10 = 0;
									$number11 = 1;
							    	while ($numfields>$number10) {
							    		$value = mysql_field_name(${'getdata'.$number}, $number10);
							    		${'string'.$number11} = $value;
								      	$number10 = $number10+1;
								      	$number11 = $number11+1;
							    	}		
							    	
							    	if($charttype == "Pie") {
										$sth = mysql_query($query);
										
										$rows = array();
										//flag is not needed
										$flag = true;
										$table = array();
										$table['cols'] = array(
											// Labels for your chart, these represent the column titles
									    	// Note that one column is in "string" format and another one is in "number" format as pie chart only required "numbers" for calculating percentage and string will be used for column title
									    	array('label' => $string1, 'type' => 'string'),
									    	array('label' => $string2, 'type' => 'number')
										);
										
										//echo json_encode($table['cols']);
										
										$rows = array();
										while($r = mysql_fetch_assoc($sth)) {
										    $temp = array();
										    // the following line will be used to slice the Pie chart
										    $temp[] = array('v' => (string) $r[$string1]); 
										
										    // Values of each slice
										    $temp[] = array('v' => (int) $r[$string2]); 
										    $rows[] = array('c' => $temp);
										}
										
										$table['rows'] = $rows;
										$data = $table;
									}
							    	
		
							    	//echo $string1;
							    	if($number11 == 3) {
										$data[0] = array($string1, $string2);
									}
									if($number11 == 4) {
										$data[0] = array($string1, $string2, $string3);
									}
									if($number11 == 5) {
										$data[0] = array($string1, $string2, $string3, $string4);
									}
									if($number11 == 6) {
										$data[0] = array($string1, $string2, $string3, $string4, $string5);
									}
									//echo json_encode($data[0]);		
							   	for ($i=1; $i<($num+1); $i++){
								   	if($number11 == 3) {
									     	$data[$i] = array(mysql_result(${'getdata'.$number}, $i-1, $string1),
											(int) mysql_result(${'getdata'.$number}, $i-1, $string2));
										}
										if($number11 == 4) {
									     	$data[$i] = array(mysql_result(${'getdata'.$number}, $i-1, $string1),
											(int) mysql_result(${'getdata'.$number}, $i-1, $string2),
											(int) mysql_result(${'getdata'.$number}, $i-1, $string3));
										}
										if($number11 == 5) {
									     	$data[$i] = array(mysql_result(${'getdata'.$number}, $i-1, $string1),
											(int) mysql_result(${'getdata'.$number}, $i-1, $string2),
											(int) mysql_result(${'getdata'.$number}, $i-1, $string3),
											(int) mysql_result(${'getdata'.$number}, $i-1, $string4));
										}
										if($number11 == 6) {
									     	$data[$i] = array(mysql_result(${'getdata'.$number}, $i-1, $string1),
											(int) mysql_result(${'getdata'.$number}, $i-1, $string2),
											(int) mysql_result(${'getdata'.$number}, $i-1, $string3),
											(int) mysql_result(${'getdata'.$number}, $i-1, $string4),
											(int) mysql_result(${'getdata'.$number}, $i-1, $string5));
										}
									}	
								   	
								   	
								  	//echo json_encode($data);
								  	${'jsonTable'.$number} = json_encode($data);
								 	?>
								 	
								 	
								 	
								 	
					<div style='display:inline-block;width:32%;margin-top:0px;margin-left:5px;margin-right:0px;border-top:1px solid #A0A0A0;border-left:1px solid #A0A0A0;border-right:1px solid #A0A0A0;border-bottom:1px solid #A0A0A0;'>
						<div style='width:100%;min-height:4px;background-color:#5d9ef4;'>
						</div>
						<table style='width:100%;min-height:250px;'>
							<tr>
								<td style="padding:10px;vertical-align:top;">
									
									
									
										<?php
								   	//build spreadsheet
										$rowquery = mysql_query($query);
										$csv_hdr[$i] = "";
								      	
								      	
										//echo "<table><tr>";
										$countoffields = 0;
										for($p = 0; $p < mysql_num_fields($rowquery); $p++) {
										    $field_info = mysql_fetch_field($rowquery, $p);
										    //echo "<th>{$field_info->name}</th>";
										    $csv_hdr[$i] .= $field_info->name;
										    if($p <> mysql_num_fields($rowquery)){ 
							         			$csv_hdr[$i] .= ",";
							         		}
							         		$countoffields = $countoffields+1;
										}
										
										// Print the data
										$csv_output[$i] = "";
										while($row = mysql_fetch_row($rowquery)) {
										    //echo "<tr>";
										    $counter3 = 1;
										    foreach($row as $_column) {
										      	//echo "<td>{$_column}</td>";
										     	$csv_output[$i] .= $_column;
										     	if($counter3 <> $countoffields){ 
							            			$csv_output[$i] .= ",";
							            		}
							            		else {
							            			$csv_output[$i] .= "\n";
							            		}
								            	$counter3 = $counter3 +1;	 
										    }
										    //echo "</tr>";
										    
										}
										
										//echo "</table>";				
												      	
								     	//show spreadsheet button
										?>
										<form name="export" action="export.php" method="post">
											<div style='margin-bottom:10px;'><b><?php echo $chartname; ?></b>
										  	<input type="submit" value="=">
										  	<input type="hidden" value='<?php echo $csv_hdr[$i]; ?>' name='csv_hdr'>
										  	<input type="hidden" value='<?php echo $csv_output[$i]; ?>' name='csv_output'>
										</form>
									</div>
									
									
									
						
									<!-- Load jQuery -->
								  	<script language="javascript" type="text/javascript" 
								  	src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js">
								  	</script>
								   	<!-- Load Google JSAPI -->
								   	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
								   	<?php 
								   	
								   	if($charttype == "Line") {
											?>						   	
								   		<script type="text/javascript">
									      	google.load("visualization", "1", { packages: ["corechart"] });
									      	google.setOnLoadCallback(drawChart);
									      	function drawChart() {
									         	var jsonData = $.ajax({
									            	url: "dashboard.php",
									            	dataType: "json",
									            	async: false
									        	}).responseText;
									
									         	var data = new google.visualization.arrayToDataTable(<?=${'jsonTable'.$number}?>);
									         	var options = {chartArea:{left:40,top:20,width:"100%",height:"65%"},legend:{position:'bottom'}};
									         	var chart = new google.visualization.LineChart(
									         	document.getElementById('chart_div<?php echo $number ?>'));
									         	chart.draw(data, options);
									     		}
									     		$(window).resize(function(){location.reload();});										
									    	</script>
								    		<?php
								    	}
								    
								    	if($charttype == "Bar"){
											?>		
											<script language="javascript" type="text/javascript" 
										  	src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js">
										  	</script>
										   	<!-- Load Google JSAPI -->
										   	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
										   	<script type="text/javascript">
										      	google.load("visualization", "1", { packages: ["corechart"] });
										      	google.setOnLoadCallback(drawChart);
										      	function drawChart() {
										         	var jsonData = $.ajax({
										            	url: "dashboard.php",
										            	dataType: "json",
										            	async: false
										        	}).responseText;
														var data = new google.visualization.arrayToDataTable(<?=${'jsonTable'.$number}?>);
										         	var options = {chartArea:{left:40,top:20,width:"100%",height:"65%"},legend:{position:'bottom'},isStacked: true};
										         	var chart = new google.visualization.BarChart(
										         	document.getElementById('chart_div<?php echo $number ?>'));
										         	chart.draw(data, options);
										     		}
										     		$(window).resize(function(){location.reload();});										
										    	</script>							    	
								    		<?php
								    	}
								    
								    	if($charttype == "Pie"){
											?>		
											<script type="text/javascript" src="https://www.google.com/jsapi"></script>
									    	<script type="text/javascript">
												google.load('visualization', '1.0', {'packages':['corechart']});
												google.setOnLoadCallback(drawChart);
												function drawChart() {
													var data = new google.visualization.DataTable(<?=${'jsonTable'.$number}?>);
													var options = {'chartArea':{left:20,top:10,width:"100%",height:"85%"},
																				 'titleTextStyle':{fontSize:16,color:'#5e5e5e'},											 
																				 'width':300,'legend':{position: 'bottom'},
										                       'height':200};
													var chart = new google.visualization.PieChart(document.getElementById('chart_div<?php echo $number ?>'));
										        chart.draw(data, options);
												}
									    	</script>						    	
								    		<?php
								    	}
								    	?>
								   	<div id="chart_div<?php echo $number ?>">
								   	</div>
								   
										
									
								</td>
							</tr>
						</table>
					</div>
					<?php
			
		
				//reset variables		
				$number = $number+1;
				$data = "";
				$jsonTable = json_decode($data);
				
			}
		}
		
		?>
		
		
	</div>
	
		<br/><br/><br/>
</div>
	
	<?php include_once ('footer.php'); ?>
</body>
