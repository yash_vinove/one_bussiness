<?php 
$link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
$link = str_replace("/view.php", "", $link);
$link = trim(substr($link, strrpos($link, '/') + 1));
//echo $link;


$includeurl = "../".$link."/fpdf181/fpdf.php";
require($includeurl);

class PDF extends FPDF
{
	
// Page header
function Header()
{
    // Logo
    $includeurl = "../documents/onebusinesstenant109business1000002/packitsafelogo.png";
	$this->Image($includeurl,80,10,50);
    // Line break
    $this->Ln(15);
    // Arial bold 15
    $this->SetFont('Arial','B',13);
}


// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
}

}
				
$type = isset($_GET['type']) ? $_GET['type'] : '';
$date = isset($_GET['date']) ? $_GET['date'] : '';
$batch = isset($_GET['batch']) ? $_GET['batch'] : '';

if($type <> ''){
	//echo "<br/>type: ".$type;
	//echo "<br/>date: ".$date;

	//check if there are any records on that date
	$getcustomerorder = "select emailaddress, firstname, lastname, buyername, address1, address2, 
	address3, address4, address5, address6, sum(ordervalue) as 'totalordervalue', purchasedate, orderid, shippingtype 
	from zzcustomerorder 
	where datecreated >= '$date 00:00:00' and datecreated <= '$date 23:59:59' and sku <> 'merged' 
	and ordervalue <> 0 and uploadbatchnumber = '$batch'
	group by orderid
	order by orderid desc";
	//echo "<br/>getcustomerorder: ".$getcustomerorder;
	$getcustomerorder = mysql_query($getcustomerorder);
	
	if(mysql_num_rows($getcustomerorder) >= 1){
		$continue = 1;	
	}
	else {
		$continue = 0;
		echo "<br/>ERROR: There are no records on ".$date.". Upload customer orders and then you can download the files.<br/><br/>";
	}

	if($continue == 1){
		//generate customer order file
		if($type == 'csvcustomer' || $type == 'zip'){
			//echo "<br/>csvcustomer";
			$getcustomerorder = "select min(zzcustomerorder.zzcustomerorderid) as customerorderid, emailaddress, firstname, lastname, buyername, address1, address2, 
			address3, address4, address5, address6, sum(ordervalue) as 'totalordervalue', purchasedate, orderid, shippingtype, phonenumber 
			from zzcustomerorder 
			where datecreated >= '$date 00:00:00' and datecreated <= '$date 23:59:59' and sku <> 'merged'
			and ordervalue <> 0 and uploadbatchnumber = '$batch'
			group by orderid
			order by orderid desc";
			//echo "<br/>getcustomerorder: ".$getcustomerorder;
			$getcustomerorder = mysql_query($getcustomerorder);
			
			if(mysql_num_rows($getcustomerorder) >= 1){
				$out = '';
				$csvarray = array();
				$csv_hdr = "New Order ID, Old Order ID, Firstname, Lastname, Buyer Name, Address 1, Address 2, Address 3, Address 4, Address 5, Address 6, Total Order Value, Purchase Date, Shipping Type, Phone Number";	 
				$exploded = array_filter(explode(",", $csv_hdr));
				array_push($csvarray, $exploded);
				while($getcustomerorderrow = mysql_fetch_array($getcustomerorder)){
					$orderid = $getcustomerorderrow['customerorderid'];//$getcustomerorderrow['orderid'];
					$oldorderid = $getcustomerorderrow['orderid'];
					$firstname = $getcustomerorderrow['firstname'];
					$lastname = $getcustomerorderrow['lastname'];
					$buyername = $getcustomerorderrow['buyername'];
					$address1 = $getcustomerorderrow['address1'];
					$address2 = $getcustomerorderrow['address2'];
					$address3 = $getcustomerorderrow['address3'];
					$address4 = $getcustomerorderrow['address4'];
					$address5= $getcustomerorderrow['address5'];
					$address6 = $getcustomerorderrow['address6'];
					$totalordervalue = round($getcustomerorderrow['totalordervalue'], 2);
					$purchasedate = $getcustomerorderrow['purchasedate'];
					$shippingtype = $getcustomerorderrow['shippingtype'];
					$phonenumber = $getcustomerorderrow['phonenumber'];
					$phonenumber = strval($phonenumber);
					if($phonenumber <> ""){
						if (preg_match("~^0\d+$~", $phonenumber)) {
		    			}
						else {
						     $phonenumber = "0".$phonenumber;
						}
					}
					
					
					array_push($csvarray, array('PAIT'.$orderid, $oldorderid, $firstname, $lastname, $buyername, $address1, $address2, $address3, $address4, 
					$address5, $address6, $totalordervalue, $purchasedate, $shippingtype, $phonenumber));
				}
				//echo "csvarray: ".json_encode($csvarray);
				
			}
			
			$filename = "CustomerOverviewExport"."_".date("Y-m-d_H-i",time()).rand(1000,50000);
	
			$file = '../documents/onebusinesstenant109business1000002/'.$filename.'.csv';
			$fp = fopen($file, 'w');
			
			foreach ($csvarray as $fields) {
			    fputcsv($fp, $fields);
			}
			
			fclose($fp);
			
			if($type == 'csvcustomer'){
				$file = "../../../".$filename.'.csv';
				$url = '../documents/onebusinesstenant109business1000002/sf/script/files1/1000010 - export.php?file='.$file.'&filetype=csv';
			   	echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">'; 
		   	}
		   	else {
				$filecsvcustomer = $file;	   	
		   	} 
		}
		
		
		//generate customer product order file
		if($type == 'csvcustomerproduct' || $type == 'zip'){
			//echo "<br/>csvcustomerproduct";
			$getcustomerorder = "select min(zzcustomerorder.zzcustomerorderid) as customerorderid, emailaddress, firstname, lastname, buyername, address1, address2, 
			address3, address4, address5, address6, ordervalue, purchasedate, orderid, orderitemid, sku, productname, shippingtype, 
			phonenumber 
			from zzcustomerorder 
			where datecreated >= '$date 00:00:00' and datecreated <= '$date 23:59:59' and sku <> 'merged'
			and ordervalue <> 0 and uploadbatchnumber = '$batch'
			group by combinedorderid
			order by combinedorderid asc";
			//echo "<br/>getcustomerorder: ".$getcustomerorder;
			$getcustomerorder = mysql_query($getcustomerorder);
			
			if(mysql_num_rows($getcustomerorder) >= 1){
				$out = '';
				$csvarray = array();
				$csv_hdr = "New Order ID, Old Order ID, Order Item ID, SKU, Product Name, Firstname, Lastname, Buyer Name, Address 1, Address 2, Address 3, Address 4, Address 5, Address 6, Order Value, Purchase Date, Shipping Type, Phone Number";	 
				$exploded = array_filter(explode(",", $csv_hdr));
				array_push($csvarray, $exploded);
				while($getcustomerorderrow = mysql_fetch_array($getcustomerorder)){
					$orderid = $getcustomerorderrow['customerorderid'];//$getcustomerorderrow['orderid'];
					$oldorderid = $getcustomerorderrow['orderid'];
					$orderitemid = $getcustomerorderrow['orderitemid'];
					$sku = $getcustomerorderrow['sku'];
					$productname = $getcustomerorderrow['productname'];
					//$orderid = $getcustomerorderrow['orderid'];
					$firstname = $getcustomerorderrow['firstname'];
					$lastname = $getcustomerorderrow['lastname'];
					$buyername = $getcustomerorderrow['buyername'];
					$address1 = $getcustomerorderrow['address1'];
					$address2 = $getcustomerorderrow['address2'];
					$address3 = $getcustomerorderrow['address3'];
					$address4 = $getcustomerorderrow['address4'];
					$address5= $getcustomerorderrow['address5'];
					$address6 = $getcustomerorderrow['address6'];
					$ordervalue = round($getcustomerorderrow['ordervalue'], 2);
					$purchasedate = $getcustomerorderrow['purchasedate'];
					$shippingtype = $getcustomerorderrow['shippingtype'];
					$phonenumber = $getcustomerorderrow['phonenumber'];
					$phonenumber = strval($phonenumber);
					if($phonenumber <> ""){
						if (preg_match("~^0\d+$~", $phonenumber)) {
		    			}
						else {
						     $phonenumber = "0".$phonenumber;
						}
					}
					
					array_push($csvarray, array('PAIT'.$orderid, $oldorderid, $orderitemid, $sku, $productname, $firstname, $lastname, 
					$buyername, $address1, $address2, $address3, $address4, 
					$address5, $address6, $ordervalue, $purchasedate, $shippingtype, $phonenumber));
				}
				//echo "csvarray: ".json_encode($csvarray);
				
			}
			
			$filename = "CustomerDetailExport"."_".date("Y-m-d_H-i",time()).rand(1000,50000);
	
			$file = '../documents/onebusinesstenant109business1000002/'.$filename.'.csv';
			$fp = fopen($file, 'w');
			
			foreach ($csvarray as $fields) {
			    fputcsv($fp, $fields);
			}
			
			fclose($fp);
			
			if($type == 'csvcustomerproduct'){
				$file = "../../../".$filename.'.csv';
				$url = '../documents/onebusinesstenant109business1000002/sf/script/files1/1000010 - export.php?file='.$file.'&filetype=csv';
			   	echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">';
		   	}
		   	else {
				$filecsvcustomerproduct = $file;	   	
		   	} 
		}

		//generate customer tracking order file
		if($type == 'csvtracking' || $type == 'zip'){
			//echo "<br/>csvtracking";
			$getcustomerorder = "select min(zzcustomerorder.zzcustomerorderid) as customerorderid, orderid, address6 from zzcustomerorder 
			where datecreated >= '$date 00:00:00' and datecreated <= '$date 23:59:59' and sku <> 'merged'
			and ordervalue <> 0 and (amazonisprime = 1 or shippingtype in ('Next Day', 'Second Day', 'NextDay', 'SecondDay')) and uploadbatchnumber = '$batch'
			group by combinedorderid
			order by combinedorderid asc";
			//echo "<br/>getcustomerorder: ".$getcustomerorder;
			$getcustomerorder = mysql_query($getcustomerorder);
			
			$csvarray = array();
			if(mysql_num_rows($getcustomerorder) >= 1){
				$out = '';
				$csv_hdr = "New Order ID, Postcode, Old Order ID";	 
				$exploded = array_filter(explode(",", $csv_hdr));
				array_push($csvarray, $exploded);
				while($getcustomerorderrow = mysql_fetch_array($getcustomerorder)){
					$orderid = $getcustomerorderrow['customerorderid'];
					$postcode = $getcustomerorderrow['address6'];
					$oldorderid = $getcustomerorderrow['orderid'];
					array_push($csvarray, array('PAIT'.$orderid, $postcode, $oldorderid));
				}
				//echo "csvarray: ".json_encode($csvarray);
				
			}
			
			$filename = "CustomerTrackingExport"."_".date("Y-m-d_H-i",time()).rand(1000,50000);
	
			$file = '../documents/onebusinesstenant109business1000002/'.$filename.'.csv';
			$fp = fopen($file, 'w');
			
			foreach ($csvarray as $fields) {
			    fputcsv($fp, $fields);
			}
			
			fclose($fp);
			
			if($type == 'csvtracking'){
				$file = "../../../".$filename.'.csv';
				$url = '../documents/onebusinesstenant109business1000002/sf/script/files1/1000010 - export.php?file='.$file.'&filetype=csv';
			   	echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">';
		   	}
		   	else {
				$filecsvcustomertracking = $file;	   	
		   	} 
		}
		
		if($type == 'zip'){
			//echo "<br/>zip";
			
			//echo "<br/>filecsvcustomer: ".$filecsvcustomer;
			//echo "<br/>filecsvcustomerproduct: ".$filecsvcustomerproduct;
			
			$getcustomerorder = "select min(zzcustomerorder.zzcustomerorderid) as customerorderid, emailaddress, firstname, lastname, buyername, address1, address2, 
			address3, address4, address5, address6, sum(ordervalue) as 'totalordervalue', purchasedate, orderid, phonenumber, shippingtype 
			from zzcustomerorder 
			where datecreated >= '$date 00:00:00' and datecreated <= '$date 23:59:59'
			and ordervalue <> 0 and uploadbatchnumber = '$batch'
			group by orderid
			order by orderid desc"; 
			//echo "<br/>getcustomerorder: ".$getcustomerorder;
			$getcustomerorder = mysql_query($getcustomerorder);
			if(mysql_num_rows($getcustomerorder) >= 1){
				//generate folder for pdf
				$foldername = "exportfile"."_".date("Y-m-d_H-i",time()).rand(5000,50000);
				//echo "<br/>foldername: ".$foldername;
				mkdir('../documents/onebusinesstenant109business1000002/'.$foldername, 0777, true);
				
				//move csv files into folder
				$filecsvcustomer = str_replace("../documents/onebusinesstenant109business1000002/", "", $filecsvcustomer);
				$filecsvcustomerproduct = str_replace("../documents/onebusinesstenant109business1000002/", "", $filecsvcustomerproduct);
				$filecsvcustomertracking = str_replace("../documents/onebusinesstenant109business1000002/", "", $filecsvcustomertracking);

				rename('../documents/onebusinesstenant109business1000002/'.$filecsvcustomer, '../documents/onebusinesstenant109business1000002/'.$foldername.'/'.$filecsvcustomer);
				rename('../documents/onebusinesstenant109business1000002/'.$filecsvcustomerproduct, '../documents/onebusinesstenant109business1000002/'.$foldername.'/'.$filecsvcustomerproduct);
				rename('../documents/onebusinesstenant109business1000002/'.$filecsvcustomertracking, '../documents/onebusinesstenant109business1000002/'.$foldername.'/'.$filecsvcustomertracking);

				
				//iterate through and create each pdf for order
				while($getcustomerorderrow = mysql_fetch_array($getcustomerorder)){
					//get record details		
					$orderid = $getcustomerorderrow['customerorderid'];
					$oldorderid = $getcustomerorderrow['orderid'];
					$firstname = $getcustomerorderrow['firstname'];
					$lastname = $getcustomerorderrow['lastname'];
					$buyername = $getcustomerorderrow['buyername'];
					$address1 = $getcustomerorderrow['address1'];
					$address2 = $getcustomerorderrow['address2'];
					$address3 = $getcustomerorderrow['address3'];
					$address4 = $getcustomerorderrow['address4'];
					$address5= $getcustomerorderrow['address5'];
					$address6 = $getcustomerorderrow['address6'];
					$phonenumber = $getcustomerorderrow['phonenumber'];
					$totalordervalue = round($getcustomerorderrow['totalordervalue'], 2);
					$purchasedate = $getcustomerorderrow['purchasedate'];
					$shippingtype = $getcustomerorderrow['shippingtype'];
					
					$nextday = 0;
					if($shippingtype == 'Next Day' || $shippingtype == 'NextDay'){
						if($totalordervalue < 12){
							$nextday = 1;		
						}			
					}
					
					if($phonenumber <> ""){
						if (preg_match("~^0\d+$~", $phonenumber)) {
		    			}
						else {
						     $phonenumber = "0".$phonenumber;
						}
					}
					$timestart3 = microtime(true);
			        //echo "<br/><br/>Inside While -- At start of PDF : ";
			        
					//generate pdf 
					$pdf = new PDF();
					$pdf->AliasNbPages();
					$pdf->AddPage();
					$pdf->Ln(25);
					$pdf->SetFont('Arial','',10);
					$pdf->MultiCell(0,5,'Packitsafe Ltd',0,1);
					$pdf->Ln(5);
					$pdf->MultiCell(0,5,'Order Number: PAIT'.$orderid,0,1);
					$pdf->Ln(5);
					$pdf->MultiCell(0,5,$buyername,0,1);
					if($address1 <> ''){
						$pdf->MultiCell(0,5,$address1,0,1);
					}
					if($address2 <> ''){
						$pdf->MultiCell(0,5,$address2,0,1);
					}
					if($address3 <> ''){
						$pdf->MultiCell(0,5,$address3,0,1);
					}
					if($address4 <> ''){
						$pdf->MultiCell(0,5,$address4,0,1);
					}
					if($address5 <> ''){
						$pdf->MultiCell(0,5,$address5,0,1);
					}
					if($address6 <> ''){
						$pdf->MultiCell(0,5,$address6,0,1);
					}
					$pdf->Ln(5);
					if($phonenumber <> ''){
						$pdf->MultiCell(0,5,$phonenumber,0,1);
					}
					$pdf->Ln(20);
					$pdf->SetFont('Arial','',10);
					$pdf->MultiCell(0,5,'Order Details',0,1);
					$pdf->Ln(5);
					
					$prices = "";
					$dropshipproductids = "";
					
					$timestart5 = microtime(true);
			        //echo "<br/><br/>Inside While -- At start of Query : ";
			        
					$getorderitems = "select sku, GROUP_CONCAT(productname SEPARATOR ', ') AS productname, count(zzcustomerorderid) as 'quantitypurchased', SUM(ordervalue) AS ordervalue, 
					dropshipproductid 
					from zzcustomerorder 
					inner join zzdropshipproduct on zzcustomerorder.productid = zzdropshipproduct.productid
					where orderid = '$oldorderid' and ordervalue <> 0
					group by orderitemid";
					//echo "<br/>Query: ".$getorderitems;
					$getorderitems = mysql_query($getorderitems);
					
					$timestart6 = microtime(true);
			        //echo "<br/><br/>Inside While -- At end of Query : ".($timestart6-$timestart5);
			        
					$numitems1 = mysql_num_rows($getorderitems);
					while($getorderitemrow = mysql_fetch_array($getorderitems)){
						$sku = $getorderitemrow['sku'];
						$productname = $getorderitemrow['productname'];
						$quantitypurchased = $getorderitemrow['quantitypurchased'];
						$ordervalue = $getorderitemrow['ordervalue'];
						$dropshipproductid = $getorderitemrow['dropshipproductid'];
						
						$pdf->SetFont('Arial','',10);
						$pdf->MultiCell(0,5,$quantitypurchased." x ".$productname,0,1);
						$pdf->Ln(5);
						
						$v = 1;
						while($v <= $quantitypurchased){
							$prices = $prices."GBP ".$ordervalue." / ";
							$dropshipproductids = $dropshipproductids.$dropshipproductid." / ";
							$v = $v + 1;
						}
					}
					$prices = rtrim($prices, " / ");
					$dropshipproductids = rtrim($dropshipproductids, " / ");
					
					if($nextday == 1){
						$pdf->Ln(10);
						$pdf->SetFont('Arial','BU',14);
						$pdf->Cell(0,10,'NEXT DAY UPGRADE',0,1);
					}
					if($numitems1 == 1){
						$pdf->Ln(30);
					}
					if($numitems1 == 2){
						$pdf->Ln(20);
					}
					if($numitems1 == 3){
						$pdf->Ln(10);
					}
					if($numitems1 >= 4){
						$pdf->Ln(5);
					}
					$pdf->Cell(0,10,'----------------------------------------------------------------------------------------------------------------------------------------------------------',0,1);
					$pdf->Ln(10);
					$pdf->SetFont('Arial','BU',10);
					$pdf->SetTextColor(194,8,8);
					$pdf->Cell(0,10,'WE TEAR OFF THIS SECTION WE TEAR OFF THIS SECTION WE TEAR OFF THIS SECTION',0,1);
					$pdf->SetTextColor(0,0,0);
					$pdf->Ln(5);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(0,10,'Order No:                PAIT'.$orderid,0,1);
					$pricecode = 'PRICE CODE: EBAY                '.$prices;		
					if($nextday == 1){
						$pricecode = $pricecode.' plus next day upgrade';				
					}
					$pdf->Cell(0,10,$pricecode,0,1);
					$pdf->Cell(0,10,'IF EBAY : ITEM NO                '.$dropshipproductids,0,1);
					
					
					//place pdf in folder 
					$filename="../documents/onebusinesstenant109business1000002/".$foldername."/PAIT".$orderid.".pdf";
					$pdf->Output($filename,'F');
					
					$timestart4 = microtime(true);
			        //echo "<br/><br/>Inside While -- At end of PDF : ".($timestart4-$timestart3);
				}
				
				//convert entire file to a zip and download it
				// Enter the name of directory 
				$pathdir = "../documents/onebusinesstenant109business1000002/".$foldername."/";  
				  
				// Enter the name to creating zipped directory 
				$zipcreated = "../documents/onebusinesstenant109business1000002/".$foldername.".zip"; 
				  
				// Create new zip class 
				$zip = new ZipArchive; 
				   
				if($zip -> open($zipcreated, ZipArchive::CREATE ) === TRUE) { 
				      
				    // Store the path into the variable 
				    $dir = opendir($pathdir); 
				       
				    while($file = readdir($dir)) { 
				        if(is_file($pathdir.$file)) { 
				            $zip -> addFile($pathdir.$file, $file); 
				        } 
				    } 
				    $zip ->close(); 
				} 
				
				//download zip file
				$file = "../../../".$foldername.'.zip';
				$url = '../documents/onebusinesstenant109business1000002/sf/script/files1/1000010 - export.php?file='.$file.'&filetype=zip';
			   	echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">'; 
				 
			}
		}
	}
	
}


echo "<h2>Dropship Order Manager</h2>";
$date = date("Y-m-d");


echo "<table class='table table-bordered' style='width:60%;'>";
echo "<thead>";
echo "<td><b>Date</b></td>";
echo "<td><b>Upload Batch Number</b</td>";
echo "<td><b>Download CSV Customer</b></td>";
echo "<td><b>Download CSV Customer Product</b></td>";
echo "<td><b>Download CSV Tracking</b></td>";
echo "<td><b>Download ZIP</b></td>";
echo "</thead>";
$i = 0;
while($i <= 30){
	$date = date("Y-m-d", strtotime("- $i days"));
	
	$getbatchnumber = "select uploadbatchnumber from zzcustomerorder 
	where datecreated >= '$date 00:00:00' and datecreated <= '$date 23:59:59'
	group by uploadbatchnumber
	order by zzcustomerorderid desc";
	
	$getbatchnumber = mysql_query($getbatchnumber);
	
	while($getrow = mysql_fetch_array($getbatchnumber))
	{
	    $batch = $getrow['uploadbatchnumber'];
    	echo "<tr>";	
    	echo "<td>".$date."</td>";
    	echo "<td>".$batch."</td>";
    	echo "<td><a href = 'view.php?viewid=1000003&type=csvcustomer&date=$date&batch=$batch'>Download CSV Customer</a></td>";
    	echo "<td><a href = 'view.php?viewid=1000003&type=csvcustomerproduct&date=$date&batch=$batch'>Download CSV Customer Product</a></td>";
    	echo "<td><a href = 'view.php?viewid=1000003&type=csvtracking&date=$date&batch=$batch'>Download CSV Tracking</a></td>";
    	echo "<td><a href = 'view.php?viewid=1000003&type=zip&date=$date&batch=$batch'>Download ZIP</a></td>";
    	echo "</tr>";
	}
	$i = $i + 1;
}

echo "</table><br/>";

?>
