<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include('config.php');
$date = date('Y-m-d');
$min = date('i');
echo "<br/>min: ".$min;
$date90 = date('Y-m-d', strtotime("- 90 days"));
$date145 = date('Y-m-d', strtotime("- 145 days"));
$date180 = date('Y-m-d', strtotime("- 180 days"));

//SET MIN PRICE FOR SHOPIFY 2ND HAND
//assign minimum price 2nd hand stage 1 - if book is less than 3 months old, assign £1.00 purchase cost
if($min <= 20){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$assignminprice2ndhandstage1 = mysql_query("update ecommercelistinglocation 
	set currentminimumprice = 1 
	where ecommercesiteconfigid = 1000005 and ecommercelistinglocationstatusid in (1,2) 
	and datecreated >= '$date90'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Set min price - £1: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
}

//assign minimum price 2nd hand stage 2 - if book is less than 4.5 months old, assign £0.50 purchase cost
if($min > 20 && $min <=40){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$assignminprice2ndhandstage1 = mysql_query("update ecommercelistinglocation 
	set currentminimumprice = 0.5 
	where ecommercesiteconfigid = 1000005 and ecommercelistinglocationstatusid in (1,2) 
	and datecreated < '$date90' and datecreated > '$date145'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Set min price - £0.50: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
}

//assign minimum price 2nd hand stage 3 - if book is less than 6 months old, assign £0.01 purchase cost
if($min > 40){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$assignminprice2ndhandstage1 = mysql_query("update ecommercelistinglocation 
	set currentminimumprice = 0.01 
	where ecommercesiteconfigid = 1000005 and ecommercelistinglocationstatusid in (1,2) 
	and datecreated <= '$date180'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Set min price - £0.01: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
}


//SET MIN PRICE FOR EBAY 2ND HAND
if($min <= 20){
	//assign minimum price 2nd hand - if book is less than 100g then set min price of 2.49
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$setminpriceebay = mysql_query("update ecommercelistinglocation 
	inner join product on product.productid = ecommercelistinglocation.productid
	set currentminimumprice = 2.49
	where ecommercesiteconfigid = 1000006 and ecommercelistinglocationstatusid in (1,2) and kgweight < 0.1");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Set min price ebay 2nd hand - £2.49: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	//assign minimum price 2nd hand - if book is less than 250g then set min price of 3.19
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$setminpriceebay = mysql_query("update ecommercelistinglocation 
	inner join product on product.productid = ecommercelistinglocation.productid
	set currentminimumprice = 3.19
	where ecommercesiteconfigid = 1000006 and ecommercelistinglocationstatusid in (1,2) and kgweight < 0.25 
	and kgweight >= 0.1");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Set min price ebay 2nd hand - £3.19: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	//assign minimum price 2nd hand - if book is greater than 250g then set min price of 5.99
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$setminpriceebay = mysql_query("update ecommercelistinglocation 
	inner join product on product.productid = ecommercelistinglocation.productid
	set currentminimumprice = 5.99
	where ecommercesiteconfigid = 1000006 and ecommercelistinglocationstatusid in (1,2) and kgweight >= 0.25 
	and kgweight < 0.5");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Set min price ebay 2nd hand - £5.99: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	//assign minimum price 2nd hand - if book is greater than 250g then set min price of 5.99
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$setminpriceebay = mysql_query("update ecommercelistinglocation 
	inner join product on product.productid = ecommercelistinglocation.productid
	set currentminimumprice = 8.99
	where ecommercesiteconfigid = 1000006 and ecommercelistinglocationstatusid in (1,2) and kgweight >= 0.5");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Set min price ebay 2nd hand - £5.99: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
}


echo "<br/><br/>";
include('recordjobstatistics.php');

?>