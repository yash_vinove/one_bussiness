<?php
$timestamp= gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time());
$shopdomain = $apikey1;
$apikey = $apikey2;
$secret = $apikey3;

if($integrationproductid <> ''){
	$pieces = explode("//", $integrationproductid);
	$integrationproductid = $pieces[0];
	$integrationvariantid = $pieces[1];
}
if($integrationorderid <> ''){
	$pieces = explode("//", $integrationorderid);
	$integrationorderid = $pieces[0];
	$integrationlineitemid = $pieces[1];
}

//NOTHING
if($currentstatus == "Listed" && $updatedstatus == "Relist"){
	//simple scenario - just update the status to Listed (2)
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("update ecommercelistinglocation set ecommercelistinglocationstatusid = 2 
	where ecommercelistinglocationid = '$ecommercelistinglocationid'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	//echo "<br/>Get list of news sources: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
}

//PAUSE
if(($currentstatus == "Listed" && $updatedstatus == "Paused") || $currentstatus == "Paused Integration Failed"){
	//unpublish the product - it keeps it in Shopify, but it removes the publish date
	echo "<br/>Pause: ".$integrationproductid;
	$url = "https://".$apikey.":".$secret."@".$shopdomain."/admin/products/".$integrationproductid.".json";
	$data = '{"product":{"id":"'.$integrationproductid.'","published_at": ""';
	$data = $data.'}}';
	
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
	//comment out returntransfer line below to see the json response for debugging purposes
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		
	$result = curl_exec($ch);
	curl_close($ch);

	//update the status in ecommercelistinglocation
	echo $result;
	$result = json_decode($result, true); 
	if(isset($result['product']['id'])){
		$productid = $result['product']['id'];
		echo "<br/><br/>product created: ".$productid."<br/><br/>";
		
		//update status and productid within  ecommercelistinglocation 
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$updatestatus = mysql_query("update ecommercelistinglocation set ecommercelistinglocationstatusid = 4, 
		integrationerrormessage = '', integrationerrorretrynumber = 0, integrationstatusname = 'Paused', 
		updategoogleshoppingproductdata = 1
		where ecommercelistinglocationid = '$ecommercelistinglocationid'");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		//echo "<br/>Get list of news sources: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	}
	if(isset($result['errors'])){
		$errors = $result['errors'];
		$errors = json_encode($errors); 
		echo "<br/><br/>errors: ".$errors."<br/><br/>";
		$integrationerrorretrynumber = $integrationerrorretrynumber + 1;
		//update status and error details within ecommercelistinglocation
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$updatestatus = mysql_query("update ecommercelistinglocation set ecommercelistinglocationstatusid = 10, 
		integrationerrormessage = '$errors', integrationerrorretrynumber = '$integrationerrorretrynumber', 
		integrationstatusname = 'Paused Integration Failed'
		where ecommercelistinglocationid = '$ecommercelistinglocationid'");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		//echo "<br/>Get list of news sources: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	}
}

//RELIST
if(($currentstatus == "Relist" && $updatedstatus == "Listed") || 
($currentstatus == "Paused" && $updatedstatus == "Relist") || $currentstatus == "Relist Integration Failed"){
	//unpublish the product - it keeps it in Shopify, but it removes the publish date
	$url = "https://".$apikey.":".$secret."@".$shopdomain."/admin/products/".$integrationproductid.".json";
	$data = '{"product":{"id":"'.$integrationproductid.'","published_at": "'.$timestamp.'"';
	$data = $data.'}}';
	
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
	//comment out returntransfer line below to see the json response for debugging purposes
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		
	$result = curl_exec($ch);
	curl_close($ch);

	//update the status in ecommercelistinglocation
	echo $result;
	$result = json_decode($result, true); 
	if(isset($result['product']['id'])){
		$productid = $result['product']['id'];
		echo "<br/><br/>product created: ".$productid."<br/><br/>";
		
		//update status and productid within  ecommercelistinglocation 
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$updatestatus = mysql_query("update ecommercelistinglocation set ecommercelistinglocationstatusid = 2, 
		integrationerrormessage = '', integrationerrorretrynumber = 0, integrationstatusname = 'Listed',
		updategoogleshoppingproductdata = 1
		where ecommercelistinglocationid = '$ecommercelistinglocationid'");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		//echo "<br/>Get list of news sources: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	}
	if(isset($result['errors'])){
		$errors = $result['errors'];
		$errors = json_encode($errors); 
		echo "<br/><br/>errors: ".$errors."<br/><br/>";
		$integrationerrorretrynumber = $integrationerrorretrynumber + 1;
		//update status and error details within ecommercelistinglocation
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$updatestatus = mysql_query("update ecommercelistinglocation set ecommercelistinglocationstatusid = 9, 
		integrationerrormessage = '$errors', integrationerrorretrynumber = '$integrationerrorretrynumber', 
		integrationstatusname = 'Relist Integration Failed'
		where ecommercelistinglocationid = '$ecommercelistinglocationid'");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		//echo "<br/>Get list of news sources: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	}
}

//REMOVE LISTING - UNSOLD (happens when the ecommercelisting is sold via another ecommerce platform)
if($currentstatus <> "Not Sold" && $updatedstatus == "Not Sold"){
	if($productindividual == 0){
		//get the current inventory level
		$integrationproductid2 = $integrationproductid."//".$integrationvariantid;
		$getinventory = "select count(ecommercelistinglocationid) as 'Inventory', integrationproductid from $database.ecommercelistinglocation
		inner join $database.ecommercelisting on ecommercelisting.ecommercelistingid = ecommercelistinglocation.ecommercelistingid
		inner join $database.productstockitem on productstockitem.productstockitemid = ecommercelisting.productstockitemid
		where integrationproductid = '$integrationproductid2' and ecommercelistinglocationstatusid in (1,2,3) 
		and ecommercesiteconfigid=$ecommercesiteconfigid";	
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$getinventory = mysql_query($getinventory);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		//echo "<br/>Get list of news sources: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		$getinventoryrow = mysql_fetch_array($getinventory);
		$inventorycount = $getinventoryrow['Inventory'];
		
		//find out if master product
		$integrationproductid2 = $integrationproductid."//".$integrationvariantid;
		$getmaster = "select masterproductid 
		from product 
		inner join ecommercelistinglocation on ecommercelistinglocation.productid = product.productid
		where integrationproductid = '$integrationproductid2' and ecommercesiteconfigid=$ecommercesiteconfigid";	
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$getmaster = mysql_query($getmaster);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		//echo "<br/>Get list of news sources: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		$getmasterrow = mysql_fetch_array($getmaster);
		$masterproductid = $getmasterrow['masterproductid'];
		
		//just update the inventory number
		if($masterproductid == 0){
			//get the products inventory_item_id
			$url = "https://".$apikey.":".$secret."@".$shopdomain."/admin/products/".$integrationproductid.".json";
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
			//comment out returntransfer line below to see the json response for debugging purposes
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			curl_close($ch);
			
			echo "<br/><br/>productdata: ".$result;
			$result = json_decode($result, true); 
			
			$inventoryitemid = $result['product']['variants'][0]['inventory_item_id'];
			echo "<br/><br/>inventoryitemid: ".$inventoryitemid;
			
			$url = "https://".$apikey.":".$secret."@".$shopdomain."/admin/inventory_levels/set.json";
			$data = '{"location_id": '.$apikey5.',"inventory_item_id": '.$inventoryitemid.',"available": '.$inventorycount.'}';
			echo "<br/><br/>data: ".$data;
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
			//comment out returntransfer line below to see the json response for debugging purposes
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				
			$result = curl_exec($ch);
			curl_close($ch);
			
		}
		else {
			//force it to run a product update on the full inventory for masterproduct records, instead of processing it here
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updatemaster = mysql_query("update ecommercelistinglocation 
			set productdatachanged = 1 
			where integrationproductid = '$integrationproductid2' and ecommercelistinglocationstatusid in (1,2,3) 
			and ecommercesiteconfigid=$ecommercesiteconfigid");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			//echo "<br/>Get list of news sources: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			$getmasterrow = mysql_fetch_array($getmaster);
			$masterproductid = $getmasterrow['masterproductid'];
			
		}
	}
	else {
		//remove the product
		echo "<br/>integrationproductid: ".$integrationproductid;
		$url = "https://".$apikey.":".$secret."@".$shopdomain."/admin/products/".$integrationproductid.".json";
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		curl_setopt($ch, CURLOPT_POST, true);
		//comment out returntransfer line below to see the json response for debugging purposes
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		$result = curl_exec($ch);
		curl_close($ch);
	}
	echo "<br/>result: ".$result;
	$result = json_decode($result, true); 
	if(!isset($result['errors'])){
		echo "<br/><br/>product deleted: ".$integrationproductid."<br/><br/>";
		
		//update status and productid within  ecommercelistinglocation 
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$updatestatus = mysql_query("update ecommercelistinglocation 
		set integrationproductid = '', integrationerrormessage = '', integrationerrorretrynumber = 0, integrationstatusname = '',
		updategoogleshoppingproductdata = 1
		where ecommercelistinglocationid = '$ecommercelistinglocationid'");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		//echo "<br/>Get list of news sources: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	}
	if(isset($result['errors'])){
		$errors = $result['errors'];
		$errors = json_encode($errors); 
		echo "<br/><br/>errors: ".$errors."<br/><br/>";
		$integrationerrorretrynumber = $integrationerrorretrynumber + 1;
		//update status and error details within ecommercelistinglocation
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$updatestatus = mysql_query("update ecommercelistinglocation 
		set integrationerrormessage = '$errors', integrationerrorretrynumber = '$integrationerrorretrynumber'
		where ecommercelistinglocationid = '$ecommercelistinglocationid'");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		//echo "<br/>Get list of news sources: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	}
}

//UPDATE ORDER ITEM TO DESPATCHED
if ($ecommercelistingstatusname == "Despatched" && $currentstatus == "Sold"){
	//update order line item as fulfilled
	$url = "https://".$apikey.":".$secret."@".$shopdomain."/admin/orders/".$integrationorderid."/fulfillments.json";
	$data = '{"fulfillment": {"tracking_number": null,"notify_customer": true,"location_id": '.$apikey5.',
	"line_items": [{"id": '.$integrationlineitemid.'}]}}';
	echo $data;
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
	//comment out returntransfer line below to see the json response for debugging purposes
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		
	$result = curl_exec($ch);
	curl_close($ch);	
		
	//update the status in ecommercelistinglocation
	echo $result;
	$result = json_decode($result, true); 
	if(isset($result['fulfillment']['id'])){
		$fulfillmentid = $result['fulfillment']['id'];
		echo "<br/><br/>product marked despatched: ".$fulfillmentid."<br/><br/>";
		//update status and productid within  ecommercelistinglocation 
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$updatestatus = mysql_query("update ecommercelistinglocation set ecommercelistinglocationstatusid = 5, 
		integrationerrormessage = '', integrationerrorretrynumber = 0, integrationstatusname = 'Despatched'
		where ecommercelistinglocationid = '$ecommercelistinglocationid'");	
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		//echo "<br/>Get list of news sources: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	}
	if(isset($result['errors'])){
		$errors = $result['errors']['order'][0];
		if($errors == "is already fulfilled"){
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updatestatus = mysql_query("update ecommercelistinglocation set ecommercelistinglocationstatusid = 5, 
			integrationerrormessage = '', integrationerrorretrynumber = 0, integrationstatusname = 'Despatched'
			where ecommercelistinglocationid = '$ecommercelistinglocationid'");	
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			//echo "<br/>Get list of news sources: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		}	
	}
}


//PROCESS RETURNED ITEM REFUND
if($ecommercelistingstatusid == 5 and $currentstatus == 'Despatched'){
	if($autorefundreturn == 1){
		//find out refund amount
		$url = "https://".$apikey.":".$secret."@".$shopdomain."/admin/orders/".$integrationorderid."/refunds/calculate.json";
		$data = '{"refund":{"shipping":{"full_refund": true}';
		$data = $data.',"refund_line_items":[{"line_item_id": '.$integrationlineitemid.',"quantity": 1}]';
		$data = $data.'}}';
		
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		//comment out returntransfer line below to see the json response for debugging purposes
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		$result = curl_exec($ch);
		curl_close($ch);
		echo $result;
		$result = json_decode($result, true); 
		
		//get data for refund
		$shippingamount = $result['refund']['shipping']['amount'];
		$shippingtaxamount = $result['refund']['shipping']['tax'];
		$productamount = $result['refund']['refund_line_items'][0]['price'];
		$gateway = $result['refund']['transactions'][0]['gateway'];
		$producttaxamount = $result['refund']['refund_line_items'][0]['total_tax'];
		$productrefund = $productamount + $producttaxamount;
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$countrec = mysql_query("select * from $database.ecommercelistinglocation where integrationorderid like '%$integrationorderid%'");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		//echo "<br/>Get list of news sources: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		$noofrecords = mysql_num_rows($countrec);
		$shippingamount = $shippingamount / $noofrecords;		
		$shippingtaxamount = $shippingtaxamount / $noofrecords;		
		$shippingrefund = $shippingamount;
		echo "shippingamount - ".$shippingamount."<br/>";
		echo "shippingtaxamount - ".$shippingtaxamount."<br/>";
		echo "gateway - ".$gateway."<br/>";
		echo "productamount - ".$productamount."<br/>";
		echo "producttaxamount - ".$producttaxamount."<br/>";
		if(isset($result['refund']['transactions'][0]['parent_id'])){
			$transactionparentid = $result['refund']['transactions'][0]['parent_id'];
			echo "transactionparentid - ".$transactionparentid."<br/>";
		}
		echo "<br/>";
			
		//refund just the one item that cannot be fulfilled - do not cancel the order
		$url = "https://".$apikey.":".$secret."@".$shopdomain."/admin/orders/".$integrationorderid."/refunds.json";
		$data = '{"refund": {"restock": false,"notify": true,"note": "Goods Returned","shipping": {"amount": '.$shippingrefund.'},';
		$data = $data.'"refund_line_items": [{"line_item_id": '.$integrationlineitemid.',"quantity": 1}],';
		$data = $data.'"transactions": [{"parent_id": '.$transactionparentid.',"amount": '.$productrefund.', "kind": "refund", "gateway": "'.$gateway.'"}]';
		$data = $data.'}}';
		//echo $data;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		//comment out returntransfer line below to see the json response for debugging purposes
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		$result = curl_exec($ch);
		curl_close($ch);
		echo $result;
		$result = json_decode($result, true); 	
		
		//update ecommercelisting
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$update = mysql_query("update $database.ecommercelisting set ecommercelistingstatusid = 6, returnrefundemailsent = 1 
		where ecommercelistingid = '$ecommercelistingid'");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		//echo "<br/>Get list of news sources: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	}
	else{
		if($returnrefundemailsent == 0){
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$getproductinfo = mysql_query("select * from $database.ecommercelisting 
			inner join $database.productstockitem on ecommercelisting.productstockitemid = productstockitem.productstockitemid
			inner join $database.product on product.productid = productstockitem.productid 
			where ecommercelistingid = '$ecommercelistingid' ");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			//echo "<br/>Get list of news sources: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			$getproductinforow = mysql_fetch_array($getproductinfo);
			$productproducer = $getproductinforow['productproducer'];
			$productname = $getproductinforow['productname'];
			$currencyid = $getproductinforow['sellcurrencyid'];
			$customerid = $getproductinforow['sellcustomerid'];
			$sellprice = $getproductinforow['sellprice'];
			$selltaxprice = $getproductinforow['selltaxprice'];
			$totalrefundamount = $sellprice + $selltaxprice;
			
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$getinfo = mysql_query("select * from $database.currency where currencyid = '$currencyid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			//echo "<br/>Get list of news sources: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			$getinforow = mysql_fetch_array($getinfo);
			$currencycode = $getinforow['currencycode'];
			
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$getinfocust = mysql_query("select * from $database.customer where customerid = '$customerid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			//echo "<br/>Get list of news sources: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			$getinfocustrow = mysql_fetch_array($getinfocust);
			$customername = $getinfocustrow['customername'];		
			$salt = "h3f8s9en20vj3";
			$customername = openssl_decrypt($customername,"AES-128-ECB",$salt);
			$content1 = $customername;
			$content2 = $productproducer;
			$content3 = $productname;
			$content4 = $ecommercelistingid;
			$content5 = $currencycode;
			$content6 = $totalrefundamount;
			echo "c1: ".$content1."<br/>";
			echo "c2: ".$content2."<br/>";
			echo "c3: ".$content3."<br/>";
			echo "c4: ".$content4."<br/>";
			echo "c5: ".$content5."<br/>";
			echo "c6: ".$content6."<br/>";
			$emailcontentid = 15;
			$emailurl = $versionname."/sendemail.php";	
			include($emailurl);
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$process = mysql_query("update $database.ecommercelisting set	returnrefundemailsent = 1
			where ecommercelistingid = '$ecommercelistingid'");	
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			//echo "<br/>Get list of news sources: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			echo "email sent<br/>";
		}
	}
}

?>







