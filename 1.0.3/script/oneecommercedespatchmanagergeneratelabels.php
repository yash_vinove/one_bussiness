<?php session_start(); ?>
<?php include('../config.php'); ?>
<?php include('../sessionconfig.php'); ?>
<?php

$selldate = isset($_GET['selldate']) ? $_GET['selldate'] : ''; 
$businessunitid = isset($_GET['businessunitid']) ? $_GET['businessunitid'] : ''; 
$storagelocationname = isset($_GET['storagelocationname']) ? $_GET['storagelocationname'] : ''; 
$storagelocationsectionname = isset($_GET['storagelocationsectionname']) ? $_GET['storagelocationsectionname'] : ''; 
$orderdatetimehour = isset($_GET['orderdatetimehour']) ? $_GET['orderdatetimehour'] : '';
$orderid = isset($_GET['orderid']) ? $_GET['orderid'] : ''; 

$includeurl = "../fpdf181/fpdf.php";
require($includeurl);

class PDF extends FPDF{}

//check if businessunitid field exists and user is not admin user	
$querybuilder = "select ecommerceorder.ecommerceorderid, orderdatetime, sellcustomerid, if(recipientname <> '', recipientname, customername) as 'customername', recipientname, 
firstname, lastname, fulfillmentaddress1, fulfillmentaddress2, fulfillmentaddresstowncity, fulfillmentaddressstatecounty, 
fulfillmentaddresspostzipcode, tb1.countryname as countrynamefulfillment
from $database.ecommerceorder 
inner join $database.ecommerceorderitem on ecommerceorderitem.ecommerceorderid = ecommerceorder.ecommerceorderid
inner join $database.ecommercelisting on ecommercelisting.ecommercelistingid = ecommerceorderitem.ecommercelistingid
inner join $database.customer on customer.customerid = ecommercelisting.sellcustomerid
inner join $database.country as tb1 on tb1.countryid = ecommercelisting.fulfillmentaddresscountryid
inner join $database.productstockitem on productstockitem.productstockitemid = ecommercelisting.productstockitemid
inner join $database.storagelocationsection on storagelocationsection.storagelocationsectionid = productstockitem.storagelocationsectionid
inner join $database.storagelocation on storagelocation.storagelocationid = storagelocationsection.storagelocationid
where ecommercelisting.selldate = '$selldate' ";
if($businessunitid <> '') {
	$querybuilder = $querybuilder." and ecommercelisting.businessunitid = $businessunitid";
}
if($_SESSION["userbusinessunit"] <> '' && $_SESSION["superadmin"] == 0){
  	$querybuilder = $querybuilder." and ecommercelisting.businessunitid in (".$_SESSION["userbusinessunit"].")";
}
if($storagelocationname <> '') {
	$querybuilder = $querybuilder." and storagelocationname = '$storagelocationname'";
}
if($storagelocationsectionname <> '') {
	$querybuilder = $querybuilder." and storagelocationsectionname = '$storagelocationsectionname'";
}
if($orderdatetimehour <> '') {
	$querybuilder = $querybuilder." and HOUR(orderdatetime) = '$orderdatetimehour'";
}
if($orderid <> '') {
	$querybuilder = $querybuilder." and ecommerceorder.ecommerceorderid = $orderid";
}
$querybuilder = $querybuilder." group by sellcustomerid, if(recipientname <> '', recipientname, customername), 
ecommercelisting.businessunitid, fulfillmentaddress1, 
fulfillmentaddresspostzipcode 
order by sellcustomerid";
//echo $querybuilder;
$querybuilder = mysql_query($querybuilder);

$getconfig = mysql_query("select * from $database.ecommerceinvoicegenerationconfig
where disabled = 0 
order by ecommerceinvoicegenerationconfigid desc");
$getconfigrow = mysql_fetch_array($getconfig);
$labelstartx = $getconfigrow['labelstartx'];
$labelstarty = $getconfigrow['labelstarty'];
$labelgapx = $getconfigrow['labelgapx'];
$labelgapy = $getconfigrow['labelgapy'];
$labelnumlabelsx = $getconfigrow['labelnumlabelsx'];
$labelnumlabelsy = $getconfigrow['labelnumlabelsy'];
$labelwidth = $getconfigrow['labelwidth'];
$labelheight = $getconfigrow['labelheight'];
$labelfontsize = $getconfigrow['labelfontsize'];
$labelsperpage = $labelnumlabelsx * $labelnumlabelsy;

$pdf = new PDF();
$pdf->SetMargins($labelstartx,$labelstarty);
$pdf->AddPage();
$pdf->SetFont('Arial','',$labelfontsize);
$count = 0;
$x = $pdf->GetX();
$y = $pdf->GetY();
	
while($row22=mysql_fetch_array($querybuilder)){
	
	$count = $count+1;
	$customerid = $row22['sellcustomerid'];
	$customername = $row22['customername'];
	$salt = "h3f8s9en20vj3";
	$customername = openssl_decrypt($customername,"AES-128-ECB",$salt);
	$recipientname = $row22['recipientname'];
	if($recipientname <> ""){
		$customername = $recipientname;	
	}
	$firstname = $row22['firstname'];
	$firstname = openssl_decrypt($firstname,"AES-128-ECB",$salt);
	$lastname = $row22['lastname'];
	$lastname = openssl_decrypt($lastname,"AES-128-ECB",$salt);
	
	//echo $customerid."<br/>";
	$fulfillmentaddress1 = $row22['fulfillmentaddress1'];
	$fulfillmentaddress2 = $row22['fulfillmentaddress2'];
	$fulfillmentaddresstowncity = $row22['fulfillmentaddresstowncity'];
	$fulfillmentaddressstatecounty = $row22['fulfillmentaddressstatecounty'];
	$fulfillmentaddresspostzipcode = $row22['fulfillmentaddresspostzipcode'];
	$fulfillmentaddresscountryname = $row22['countrynamefulfillment'];
	
	// Instantiation of inherited class
	$pdf->AliasNbPages();
	$combined = "";
	$address = $combined."\n";
   	if($customername <> $combined){
		$address = $address.$customername."\n";
	}
	if($fulfillmentaddress1 <> ''){
		$address = $address.$fulfillmentaddress1."\n";
	}
	if($fulfillmentaddress2 <> ''){
		$address = $address.$fulfillmentaddress2."\n";
	}
	if($fulfillmentaddresstowncity <> ''){
		$address = $address.$fulfillmentaddresstowncity."\n";
	}
	if($fulfillmentaddressstatecounty <> ''){
		$address = $address.$fulfillmentaddressstatecounty."\n";
	}
	if($fulfillmentaddresspostzipcode <> ''){
		$address = $address.$fulfillmentaddresspostzipcode."\n";
	}
	if($fulfillmentaddresscountryname <> ''){
		$address = $address.$fulfillmentaddresscountryname;
	}
	
	$x = $pdf->GetX();
	$y = $pdf->GetY();
	$pdf->MultiCell($labelwidth, 5, $address, 0,1);
	$pdf->SetXY($x + $labelwidth, $y);
	$x = $pdf->GetX();
	$y = $pdf->GetY();
	$pdf->MultiCell($labelgapx, 5, '', 0,1);
	$pdf->SetXY($x + $labelgapx, $y);
	
	$checknewrow = $count/$labelnumlabelsx;
	if ($checknewrow == round($checknewrow)) {
	    $pdf->Ln($labelheight);
	    $pdf->Ln($labelgapy);
	}
	$checknewpage = $count/$labelsperpage;
	if ($checknewpage == round($checknewpage)) {
	    $pdf->AddPage();
	}
	
}

//uncomment this, if you want to see an example in the browser for testing. However, comment it out once you are finished otherwise the whilst loop will not run.
$pdf->Output();

?>				
