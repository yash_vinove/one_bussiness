<?php

//LANGUAGE COLLECTION SECTION
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (622,623,624,625,626,627,628,629,630,631,632,633,634,635,636,637,638)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}

$folderid = isset($_GET['folderid']) ? $_GET['folderid'] : '';
$deletefile = isset($_GET['deletefile']) ? $_GET['deletefile'] : '';
$deletefolder = isset($_GET['deletefolder']) ? $_GET['deletefolder'] : '';
$confirm = isset($_GET['confirm']) ? $_GET['confirm'] : '';
$uid = $_SESSION['userid'];
if($_SESSION['userbusinessunit'] <> ""){
	$ubusinessunits = $_SESSION['userbusinessunit'];	
}	
//only used for testing - line below - comment out in normal use	
//$ubusinessunits = "1,2,3,4,5";

//GET PERMISSIONS
$masterallowedit = 0;
$masterallowaccess = 0;

//check if everyone has drive access
$getdata = mysql_query("select * from $database.documentpermission 
where documentdriveid = '$did' and everyone = '1'");
if(mysql_num_rows($getdata) >= 1){
	//echo "Everyone has access<br/>";
	$getdatarow = mysql_fetch_array($getdata);
	$allowedit = $getdatarow['allowedit'];
	$masterallowaccess = $masterallowaccess + 1;
	if($allowedit == 1){
		$masterallowedit = $masterallowedit + 1;	
	}
}

//check if you are the drive owner
$getdata = mysql_query("select * from documentdrive 
where driveownerid = '$uid' and documentdriveid = '$did'");
if(mysql_num_rows($getdata) >= 1){
	//echo "Owner of the drive<br/>";
	$getdatarow = mysql_fetch_array($getdata);
	$allowedit = 1;
	$masterallowaccess = $masterallowaccess + 1;
	if($allowedit == 1){
		$masterallowedit = $masterallowedit + 1;		
	}
}

//check if you have user access to the drive
$getdata = mysql_query("select * from $database.documentpermission 
where documentdriveid = '$did' and userid = '$uid'");
if(mysql_num_rows($getdata) >= 1){
	//echo "User has access<br/>";
	$getdatarow = mysql_fetch_array($getdata);
	$allowedit = $getdatarow['allowedit'];
	$masterallowaccess = $masterallowaccess + 1;
	if($allowedit == 1){
		$masterallowedit = $masterallowedit + 1;	
	}
}

//check if you have businessunit access to the drive
if(isset($ubusinessunits)){
	$getdata = mysql_query("select * from $database.documentpermission
	where documentdriveid = '$did' and documentpermission.businessunitid in ($ubusinessunits)");
	if(mysql_num_rows($getdata) >= 1){
		//echo "Business Unit has access<br/>";
		$getdatarow = mysql_fetch_array($getdata);
		$allowedit = $getdatarow['allowedit'];
		$masterallowaccess = $masterallowaccess + 1;
		if($allowedit == 1){
			$masterallowedit = $masterallowedit + 1;	
		}
	}
	
	//check if you have businessunithierarchy access to the drive
	$getbuhierarchy = mysql_query("select businessunithierarchyid from $database.businessunit
	where businessunitid in ($ubusinessunits)");
	while($getbuhierarchyrow = mysql_fetch_array($getbuhierarchy)){
		$businessunithierarchyid = $getbuhierarchyrow['businessunithierarchyid'];
		$getdata = mysql_query("select * from $database.documentpermission 
		where documentdriveid = '$did' and businessunithierarchyid = '$businessunithierarchyid'");
		if(mysql_num_rows($getdata) >= 1){
			//echo "Business Unit Hierarchy has access<br/>";
			$getdatarow = mysql_fetch_array($getdata);
			$allowedit = $getdatarow['allowedit'];
			$masterallowaccess = $masterallowaccess + 1;
			if($allowedit == 1){
				$masterallowedit = $masterallowedit + 1;	
			}
		}
	}
}
//echo "masterallowaccess: ".$masterallowaccess;
//echo "<br/>masterallowedit: ".$masterallowedit."<br/>";

//declare variables
$editablefolder = array();
$viewablefolder = array();
$editabledocument = array();
$viewabledocument = array();

//everyone has access to a folder
$getdata = mysql_query("select documentpermission.documentfolderid, allowedit from $database.documentpermission 
inner join $database.documentfolder on documentfolder.documentfolderid = documentpermission.documentfolderid
where everyone = '1' and documentfolder.documentdriveid = '$did'");
while($getdatarow = mysql_fetch_array($getdata)){
	$documentfolderid = $getdatarow['documentfolderid'];
	$allowedit = $getdatarow['allowedit'];
	if($allowedit == 1){
		if(!in_array($documentfolderid, $editablefolder)){
			array_push($editablefolder, $documentfolderid);
		}	
	}
	else {
		if(!in_array($documentfolderid, $viewablefolder)){
			array_push($viewablefolder, $documentfolderid);	
		}
	}
}

//everyone has access to a document
$getdata = mysql_query("select documentpermission.documentid, allowedit from $database.documentpermission 
inner join $database.document on document.documentid = documentpermission.documentid
inner join $database.documentfolder on documentfolder.documentfolderid = document.documentfolderid
where everyone = '1' and documentfolder.documentdriveid = '$did'");
while($getdatarow = mysql_fetch_array($getdata)){
	$documentid = $getdatarow['documentid'];
	$allowedit = $getdatarow['allowedit'];
	if($allowedit == 1){
		if(!in_array($documentid, $editabledocument)){
			array_push($editabledocument, $documentid);	
		}
	}
	else {
		if(!in_array($documentid, $viewabledocument)){
			array_push($viewabledocument, $documentid);	
		}
	}
}

//owner has access to a folder
$getdata = mysql_query("select documentfolderid from $database.documentfolder 
where folderownerid = '$uid' and documentdriveid = '$did'");
while($getdatarow = mysql_fetch_array($getdata)){
	$documentfolderid = $getdatarow['documentfolderid'];
	if(!in_array($documentfolderid, $editablefolder)){
		array_push($editablefolder, $documentfolderid);	
	}
}

//owner has access to a document
$getdata = mysql_query("select documentid from $database.document
inner join $database.documentfolder on documentfolder.documentfolderid = document.documentfolderid
where documentownerid = '$uid' and documentdriveid = '$did'");
while($getdatarow = mysql_fetch_array($getdata)){
	$documentid = $getdatarow['documentid'];
	if(!in_array($documentid, $editabledocument)){
		array_push($editabledocument, $documentid);	
	}
}

//user has access to a folder
$getdata = mysql_query("select documentpermission.documentfolderid, allowedit from $database.documentpermission 
inner join $database.documentfolder on documentfolder.documentfolderid = documentpermission.documentfolderid
where userid = '$uid' and documentfolder.documentdriveid = '$did'");
while($getdatarow = mysql_fetch_array($getdata)){
	$documentfolderid = $getdatarow['documentfolderid'];
	$allowedit = $getdatarow['allowedit'];
	if($allowedit == 1){
		if(!in_array($documentfolderid, $editablefolder)){
			array_push($editablefolder, $documentfolderid);
		}	
	}
	else {
		if(!in_array($documentfolderid, $viewablefolder)){
			array_push($viewablefolder, $documentfolderid);	
		}
	}
}

//user has access to a document
$getdata = mysql_query("select documentpermission.documentid, allowedit from $database.documentpermission 
inner join $database.document on document.documentid = documentpermission.documentid
inner join $database.documentfolder on documentfolder.documentfolderid = document.documentfolderid
where userid = '$uid' and documentfolder.documentdriveid = '$did'");
while($getdatarow = mysql_fetch_array($getdata)){
	$documentid = $getdatarow['documentid'];
	$allowedit = $getdatarow['allowedit'];
	if($allowedit == 1){
		if(!in_array($documentid, $editabledocument)){
			array_push($editabledocument, $documentid);	
		}
	}
	else {
		if(!in_array($documentid, $viewabledocument)){
			array_push($viewabledocument, $documentid);	
		}
	}
}

//business unit has access to a folder
if(isset($ubusinessunits)){
	$getdata = mysql_query("select documentpermission.documentfolderid, allowedit from $database.documentpermission
	inner join $database.documentfolder on documentfolder.documentfolderid = documentpermission.documentfolderid
	where documentfolder.documentdriveid = '$did' and documentpermission.businessunitid in ($ubusinessunits)");
	while($getdatarow2 = mysql_fetch_array($getdata)){
		$documentfolderid = $getdatarow2['documentfolderid'];
		$allowedit = $getdatarow2['allowedit'];
		if($allowedit == 1){
			if(!in_array($documentfolderid, $editablefolder)){
				array_push($editablefolder, $documentfolderid);	
			}
		}
		else {
			if(!in_array($documentfolderid, $viewablefolder)){
				array_push($viewablefolder, $documentfolderid);	
			}
		}
	}
	
	//business unit hierarchy has access to a folder
	$getbuhierarchy = mysql_query("select businessunithierarchyid from $database.businessunit
	where businessunitid in ($ubusinessunits)");
	while($getbuhierarchyrow = mysql_fetch_array($getbuhierarchy)){
		$businessunithierarchyid = $getbuhierarchyrow['businessunithierarchyid'];
		$getdata = mysql_query("select documentpermission.documentfolderid, allowedit from $database.documentpermission 
		inner join $database.documentfolder on documentfolder.documentfolderid = documentpermission.documentfolderid
		where documentfolder.documentdriveid = '$did' and documentpermission.businessunithierarchyid = '$businessunithierarchyid'");
		while($getdatarow3 = mysql_fetch_array($getdata)){
			$documentfolderid = $getdatarow3['documentfolderid'];
			$allowedit = $getdatarow3['allowedit'];
			if($allowedit == 1){
				if(!in_array($documentfolderid, $editablefolder)){
					array_push($editablefolder, $documentfolderid);	
				}
			}
			else {
				if(!in_array($documentfolderid, $viewablefolder)){
					array_push($viewablefolder, $documentfolderid);	
				}
			}
		}
	}
}

//business unit has access to a document
if(isset($ubusinessunits)){
	$getdata = mysql_query("select documentpermission.documentid, allowedit from $database.documentpermission
	inner join $database.document on document.documentid = documentpermission.documentid	
	inner join $database.documentfolder on documentfolder.documentfolderid = document.documentfolderid
	where documentfolder.documentdriveid = '$did' and documentpermission.businessunitid in ($ubusinessunits)");
	while($getdatarow2 = mysql_fetch_array($getdata)){
		$documentid = $getdatarow2['documentid'];
		$allowedit = $getdatarow2['allowedit'];
		if($allowedit == 1){
			if(!in_array($documentid, $editabledocument)){
				array_push($editabledocument, $documentid);	
			}
		}
		else {
			if(!in_array($documentid, $viewabledocument)){
				array_push($viewabledocument, $documentid);	
			}
		}
	}
	
	//business unit hierarchy has access to a document
	$getbuhierarchy = mysql_query("select businessunithierarchyid from $database.businessunit
	where businessunitid in ($ubusinessunits)");
	while($getbuhierarchyrow = mysql_fetch_array($getbuhierarchy)){
		$businessunithierarchyid = $getbuhierarchyrow['businessunithierarchyid'];
		$getdata = mysql_query("select documentpermission.documentid, allowedit from $database.documentpermission 
		inner join $database.document on document.documentid = documentpermission.documentid	
		inner join $database.documentfolder on documentfolder.documentfolderid = document.documentfolderid
		where documentfolder.documentdriveid = '$did' and documentpermission.businessunithierarchyid = '$businessunithierarchyid'");
		while($getdatarow3 = mysql_fetch_array($getdata)){
			$documentid = $getdatarow3['documentid'];
			$allowedit = $getdatarow3['allowedit'];
			if($allowedit == 1){
				if(!in_array($documentid, $editabledocument)){
					array_push($editabledocument, $documentid);	
				}
			}
			else {
				if(!in_array($documentid, $viewabledocument)){
					array_push($viewabledocument, $documentid);	
				}
			}
		}
	}
}

//echo "editablefolder: ".json_encode($editablefolder);
//echo "<br/>viewablefolder: ".json_encode($viewablefolder);
//echo "<br/>editabledocument: ".json_encode($editabledocument);
//echo "<br/>viewabledocument: ".json_encode($viewabledocument);
//echo "<br/>";




//PROCESS UPDATES
//delete a file
if($deletefile >= 1){
	$getfilelocation = mysql_query("select * from $database.document
	where documentid = '$deletefile'");
	$getfilelocationrow = mysql_fetch_array($getfilelocation);
	$documentfolderid = $getfilelocationrow['documentfolderid'];
	$documentname = $getfilelocationrow['documentname'];
	$path = "../documents/".$database."/onedoc/".$did."/";
	//get additional folder url 
	$t = 1;
	$folderidsaved = $documentfolderid;
	$pathend = "";
	while($t <= 100){
		$getfolder = mysql_query("select * from $database.documentfolder
		where documentfolderid = '$folderidsaved'");
		$getfolderrow = mysql_fetch_array($getfolder);
		$masterfolderid = $getfolderrow['masterfolderid'];
		if($masterfolderid > 0){
			$pathend = $masterfolderid."/".$pathend;
			$folderidsaved = $pathend;
		}
		$t = $t + 1;
	}
	$documentend = substr($documentname, strpos($documentname, ".") + 1);
	$path = $path.$pathend.$folderid."/doc".$deletefile.".".$documentend;
	//echo $path;
	unlink($path);
	
	$deleterecord = mysql_query("delete from $database.document 
	where documentid = '$deletefile'");
	
	$url = 'view.php?viewid=28&did='.$did.'&folderid='.$folderid;
	echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">';
}

//delete folder step 2
if($deletefolder >= 1 && $confirm == 1){
	$path = "../documents/".$database."/onedoc/".$did."/";
	//get additional folder url 
	$t = 1;
	$folderidsaved = $deletefolder;
	$pathend = "";
	while($t <= 100){
		$getfolder = mysql_query("select * from $database.documentfolder
		where documentfolderid = '$folderidsaved'");
		$getfolderrow = mysql_fetch_array($getfolder);
		$masterfolderid = $getfolderrow['masterfolderid'];
		if($masterfolderid > 0){
			$pathend = $masterfolderid."/".$pathend;
			$folderidsaved = $pathend;
		}
		$t = $t + 1;
	}
	$path = $path.$pathend.$deletefolder."/";
	
	//delete all files/directories
	$folderidlist = array($deletefolder);
	function rmdir_recursive($dir) {
		global $folderidlist;	
	  	foreach(scandir($dir) as $file) {
	        if ('.' === $file || '..' === $file) continue;
	        	if (is_dir("$dir/$file")) {
	        		$file2 = str_replace("/", "", $file);
	        		array_push($folderidlist, $file2);
	        		rmdir_recursive("$dir/$file");
	        	}
	        	else{
	        		unlink("$dir/$file");
	        	}
	    }
	    rmdir($dir);
	}
	rmdir_recursive($path);
	
	$folderidlist = implode(', ', $folderidlist);			    
	//echo $folderidlist;   
	
	//delete all folders
	$deletefolder = mysql_query("delete from $database.documentfolder
	where documentfolderid in ($folderidlist)");
	
	//delete all files
	$deletefolder = mysql_query("delete from $database.document
	where documentfolderid in ($folderidlist)");
	
	$url = 'view.php?viewid=28&did='.$did.'&folderid='.$folderid;
	echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">';
}

//add folder 
$submitfolder = isset($_POST['submitfolder']) ? $_POST['submitfolder'] : ''; 
$foldername = isset($_POST['foldername']) ? $_POST['foldername'] : ''; 
if($submitfolder) {
	if($foldername<>"") {
    	$getfoldername = mysql_query("select * from $database.documentfolder
    	where documentdriveid = '$did' and masterfolderid = '$folderid' and documentfoldername = '$foldername'");
    	if(mysql_num_rows($getfoldername)>= 1){
			echo "<p class='background-warning'>There is already a folder with that name, within this folder. Please input a different name.</p><br/>";	    	
    	}
    	else {
			//add folder record
			$date = date("Y-m-d");
			$insertrecord = mysql_query("insert into $database.documentfolder (documentfoldername, folderownerid, masterfolderid, 
			documentdriveid, disabled, datecreated, masteronly) values ('$foldername', '$uid', '$folderid', '$did', '0', '$date', '0')");
			$recordid = mysql_insert_id();		
			//add the folder
			$path = "../documents/".$database."/onedoc/".$did."/";
			if($folderid >= 1){
				//if folder is a sub-folder then show up one level option
				$getfolder = mysql_query("select sum(masterfolderid) as 'hasmaster' from $database.documentfolder
				where masterfolderid = '$folderid' group by masterfolderid");
				$getfolderrow = mysql_fetch_array($getfolder);
				$hasmaster = $getfolderrow['hasmaster'];
				$getfolder2 = mysql_query("select * from $database.documentfolder
				where documentfolderid = '$folderid'");
				$getfolder2row = mysql_fetch_array($getfolder2);
				$mfid = $getfolder2row['masterfolderid'];
				
				//get additional folder url 
				$t = 1;
				$folderidsaved = $folderid;
				$pathend = "";
				while($t <= 100){
					$getfolder = mysql_query("select * from $database.documentfolder
					where documentfolderid = '$folderidsaved'");
					$getfolderrow = mysql_fetch_array($getfolder);
					$masterfolderid = $getfolderrow['masterfolderid'];
					if($masterfolderid > 0){
						$pathend = $masterfolderid."/".$pathend;
						$folderidsaved = $pathend;
					}
					$t = $t + 1;
				}
				$path = $path.$pathend.$folderid."/";
			}
			$dirs = array();
			$dir = dir($path);
			while (false !== ($entry = $dir->read())) {
			    if ($entry != '.' && $entry != '..') {
			       if (is_dir($path . '/' .$entry)) {
			            $dirs[] = $entry; 
			       }
			    }
			}
			
			//add new directory
			$filedir = $path.$recordid."/";
			if(file_exists($filedir)){
			}
			else {
				$file_directory = $path.$recordid."/";
				mkdir($file_directory);
			}			    	
    	}
   	}
   	else {
		echo "<p class='background-warning'>".$langval622."</p><br/>";
   	} 		
}

//add file
$submitfile = isset($_POST['submitfile']) ? $_POST['submitfile'] : ''; 
$filename = isset($_POST['filename']) ? $_POST['filename'] : ''; 
if($submitfile) {
	$filename=$_FILES["file"]["name"];
	if($filename<>"") {
		//get filesize
		$filesize = $_FILES["file"]["size"];		
		
		//create the record
		$date = date("Y-m-d");
		$createrecord = mysql_query("insert into $database.document (documentname, documentownerid, documentfolderid, lastupdated, 
		filesize, disabled, datecreated, masteronly) values ('$filename', '$uid', '$folderid', '$date', '$filesize', '0', '$date', '0')");
		$recordid = mysql_insert_id();
		
		//add the folder
		$path = "../documents/".$database."/onedoc/".$did."/";
		if($folderid >= 1){
			//if folder is a sub-folder then show up one level option
			$getfolder = mysql_query("select sum(masterfolderid) as 'hasmaster' from $database.documentfolder
			where masterfolderid = '$folderid' group by masterfolderid");
			$getfolderrow = mysql_fetch_array($getfolder);
			$hasmaster = $getfolderrow['hasmaster'];
			$getfolder2 = mysql_query("select * from $database.documentfolder
			where documentfolderid = '$folderid'");
			$getfolder2row = mysql_fetch_array($getfolder2);
			$mfid = $getfolder2row['masterfolderid'];
			
			//get additional folder url 
			$t = 1;
			$folderidsaved = $folderid;
			$pathend = "";
			while($t <= 100){
				$getfolder = mysql_query("select * from $database.documentfolder
				where documentfolderid = '$folderidsaved'");
				$getfolderrow = mysql_fetch_array($getfolder);
				$masterfolderid = $getfolderrow['masterfolderid'];
				if($masterfolderid > 0){
					$pathend = $masterfolderid."/".$pathend;
					$folderidsaved = $pathend;
				}
				$t = $t + 1;
			}
			$path = $path.$pathend.$folderid."/";
		}
		$dirs = array();
		$dir = dir($path);
		while (false !== ($entry = $dir->read())) {
		    if ($entry != '.' && $entry != '..') {
		       if (is_dir($path . '/' .$entry)) {
		            $dirs[] = $entry; 
		       }
		    }
		}
			
		//save the file
		$array = explode('.', $_FILES['file']['name']);
		$filetype = end($array);
		$newfilename = "doc".$recordid.".".$filetype;
		$path = $path.$newfilename;
		move_uploaded_file($_FILES["file"]["tmp_name"], $path);     				
	}
	else {
		echo "<p class='background-warning'>".$langval623."</p><br/>";  
	}
}

if($did >= 1){
	$getdrivedetail = mysql_query("select * from $database.documentdrive 
	where documentdriveid = '$did'");
	$getdrivedetailrow = mysql_fetch_array($getdrivedetail);
	$documentdriveid = $getdrivedetailrow['documentdriveid'];
	$documentdrivename = $getdrivedetailrow['documentdrivename'];
	$driveownerid = $getdrivedetailrow['driveownerid'];
	
	//view contents of a drive
	echo "<a class='button-primary' href='view.php?viewid=28'>".$langval624."</a><br/><br/>";
	echo "<h2>".$langval625." - ".$documentdrivename."</h2>";
	echo "<p>".$langval626."</p>";
	
	//calculate if allowed to edit/permission the drive because they own the drive
	$allowedit = 0;
	if($driveownerid == $uid){
		$allowedit = 1;		
	}		
	
	//get folders list
	echo "<div style='overflow-y: scroll; max-height:400px;'>";
	echo "<table class='table table-bordered' style='width:80%'>";
	echo "<thead>";
	echo "<td style='width:1%;white-space:nowrap;text-align:center;'><b>T</b></td>";
	echo "<td><b>".$langval627."</b></td>";
	echo "<td><b>".$langval628."</b></td>";
	echo "<td><b>".$langval629."</b></td>";
	echo "<td><b>".$langval630."</b></td>";
	echo "<td><b>".$langval631."</b></td>";
	echo "</thead>";
	
	$path = "../documents/".$database."/onedoc/".$did."/";
	if($folderid >= 1){
		//if folder is a sub-folder then show up one level option
		$getfolder = mysql_query("select sum(masterfolderid) as 'hasmaster' from $database.documentfolder
		where masterfolderid = '$folderid' group by masterfolderid");
		$getfolderrow = mysql_fetch_array($getfolder);
		$hasmaster = $getfolderrow['hasmaster'];
		$getfolder2 = mysql_query("select * from $database.documentfolder
		where documentfolderid = '$folderid'");
		$getfolder2row = mysql_fetch_array($getfolder2);
		$mfid = $getfolder2row['masterfolderid'];
		if(count($editablefolder) >= 1 || count($viewablefolder) >= 1){
			if(in_array($mfid, $editablefolder) || in_array($mfid, $viewablefolder)){			
			}		
			else {
				$mfid = 0;			
			}
		}
		if(($hasmaster >= 1 || $mfid >= 1)){
			echo "<tr><td style='text-align:center;'><i class='fa fa-angle-double-left'></i></td>";
			echo "<td><a href='view.php?viewid=28&did=".$did."&folderid=".$mfid."'><b>- ".$langval632."</b></a></td>";		
			echo "<td></td><td></td><td></td><td></td></tr>";		
		}	
		
		//get additional folder url 
		$t = 1;
		$folderidsaved = $folderid;
		$pathend = "";
		while($t <= 100){
			$getfolder = mysql_query("select * from $database.documentfolder
			where documentfolderid = '$folderidsaved'");
			$getfolderrow = mysql_fetch_array($getfolder);
			$masterfolderid = $getfolderrow['masterfolderid'];
			if($masterfolderid > 0){
				$pathend = $masterfolderid."/".$pathend;
				$folderidsaved = $pathend;
			}
			$t = $t + 1;
		}
		$path = $path.$pathend.$folderid."/";
	}
	if($masterallowaccess >= 1 || $folderid >= 1){
		$dirs = array();
		$dir = dir($path);
		while (false !== ($entry = $dir->read())) {
		    if ($entry != '.' && $entry != '..') {
		       if (is_dir($path . '/' .$entry)) {
		            $dirs[] = $entry; 
		       }
		    }
		}
		//echo json_encode($dirs);
	}
	else {
		if(count($editablefolder) >= 1 || count($viewablefolder) >= 1){
			$dirs = array_unique(array_merge($editablefolder,$viewablefolder), SORT_REGULAR);
		}
		$string = rtrim(implode(',', $dirs), ',');
		$getdocfolders = mysql_query("select * from $database.documentfolder 
		where documentfolderid in ($string)
		order by documentfoldername asc");
		$dirs = array();
		while ($row46 = mysql_fetch_array($getdocfolders)){
			$documentfolderid = $row46['documentfolderid'];
			array_push($dirs, $documentfolderid);
		}
		//echo json_encode($files);
		
	}
	
	//echo $path;
	
	//show drive folders
	foreach($dirs as $documentfolderid){
		$getfolder = mysql_query("select * from $database.documentfolder 
		inner join $database.user on user.userid = documentfolder.folderownerid
		where documentfolderid = '$documentfolderid'");
		$datafolderrow = mysql_fetch_array($getfolder);
		$documentfoldername = $datafolderrow['documentfoldername'];
		$folderownerid = $datafolderrow['folderownerid'];
		$datecreated = $datafolderrow['datecreated'];
		$folderownername = $datafolderrow['firstname']." ".$datafolderrow['lastname'];
		
		//display folder
		echo "<tr>";
		echo "<td style='text-align:center;'><i class='fa fa-folder-o'></i></td>";
		echo "<td><a href='view.php?viewid=28&did=".$did."&folderid=".$documentfolderid."'><b>+ ".$documentfoldername."</b></a></td>";
		echo "<td>".$folderownername."</td>";
		echo "<td>".$datecreated."</td>";
		if($masterallowedit >= 1 || in_array($documentfolderid, $editablefolder)){
			echo "<td><a href='pageedit.php?pagetype=documentfolder&rowid=".$documentfolderid."&pagename=".$pagename."'>".$langval630."</a></td>";
		}
		else {
			echo "<td></td>";			
		}
		if($deletefolder == $documentfolderid && $confirm <> 1){
			echo "<td><font style='color:red;'>Are you sure? </font><a href='view.php?viewid=28&did=".$did."&folderid=".$folderid."&deletefolder=".$documentfolderid."&confirm=1'>".$langval633."</a></td>";
		}
		else {
			if($masterallowedit >= 1 || in_array($documentfolderid, $editablefolder)){
				echo "<td><a href='view.php?viewid=28&did=".$did."&folderid=".$folderid."&deletefolder=".$documentfolderid."'>".$langval633."</a></td>";
			}
			else {
				echo "<td></td>";			
			}
		}
		echo "</tr>";
	}
	
	//show drive contents
	if($masterallowaccess >= 1 || $folderid >= 1){
		$files = array();
		if($handle = opendir($path)) {
		  	while (false !== ($entry = readdir($handle))) {
		      	if($entry != "." && $entry != ".." && $entry != ".DS_Store") {
		        	if (strpos($entry, 'doc') !== false) {
		        		array_push($files, $entry);
					}       
		     	}
		 	}	
		  	closedir($handle);
		}
	}
	else {
		if(count($editabledocument) >= 1 || count($viewabledocument) >= 1){
			$files = array_unique(array_merge($editabledocument,$viewabledocument), SORT_REGULAR);
		}
		$string = rtrim(implode(',', $files), ',');
		$getdocs = mysql_query("select * from $database.document 
		where documentid in ($string)
		order by documentname asc");
		$files = array();
		while ($row45 = mysql_fetch_array($getdocs)){
			$documentid = $row45['documentid'];
			$documentname = $row45['documentname'];
			$ending = substr($documentname, strrpos($documentname, '.') + 1);
			$name = "doc".$documentid.".".$ending;
			array_push($files, $name);
		}
		//echo json_encode($files);
		
	}
	//echo json_encode($files);
	
	//show drive files
	if($folderid >= 1 || count($files) >= 1){
		foreach($files as $documenturl){
			$documentid = str_replace("doc", "", $documenturl);
			$documentid = substr($documentid, 0, strpos($documentid, "."));
			//echo "<br/>".$documentid;
			$getdocument = mysql_query("select document.datecreated, documentid, documentname, documentownerid, firstname, lastname
			from $database.document
			inner join $database.user on user.userid = document.documentownerid
			where documentid = '$documentid'");
			$datadocumentrow = mysql_fetch_array($getdocument);
			$documentid = $datadocumentrow['documentid'];
			$datecreated = $datadocumentrow['datecreated'];
			$documentname = $datadocumentrow['documentname'];
			$documentownerid = $datadocumentrow['documentownerid'];
			$documentownername = $datadocumentrow['firstname']." ".$datadocumentrow['lastname'];
			//display folder
			echo "<tr>";
			echo "<td style='text-align:center;'><i class='fa fa-file-o'></i></td>";
			echo "<td><a href='".$path.$documenturl."' download=".$documentname.">".$documentname."</a></td>";
			echo "<td>".$documentownername."</td>";
			echo "<td>".$datecreated."</td>";
			echo "<td></td>";
			if($masterallowedit >= 1 || in_array($folderid, $editablefolder) || in_array($documentid, $editabledocument)){
				echo "<td><a href='view.php?viewid=28&did=".$did."&folderid=".$folderid."&deletefile=".$documentid."'>".$langval634."</a></td>";
			}	
			else {
				echo "<td></td>";			
			}		
			echo "</tr>";			
		}	
	}
	//end table	
	echo "</table>";
	echo "</div>";
	echo "<br/>";
	if(in_array($folderid, $editablefolder) || $masterallowedit >= 1){
	?>
	<form action="view.php?viewid=28&did=<?php echo $did ?>&folderid=<?php echo $folderid ?>" method="post" enctype="multipart/form-data" name="formaddfolder" id="formaddfolder"> 
		<table>
			<tr>
				<td style="min-width:100px;">			  	 
  					<b><?php echo $langval635 ?>:&nbsp;&nbsp;</b>
    			</td>
				<td style="padding-right:10px;min-width:273px;">
					<input type="text" class="form-control" width="100%" name="foldername" value="">							    	
    			</td>
    			<td>
    				<input class="btn btn-primary" type="submit" name="submitfolder" value="<?php echo $langval636 ?>" /> 			
    			</td>
			</tr> 
		</table>																							
  	</form> 
	<?php
	}
	
	if($folderid >= 1){
		if(in_array($folderid, $editablefolder) || $masterallowedit >= 1){
	
		?>
		<form action="view.php?viewid=28&did=<?php echo $did ?>&folderid=<?php echo $folderid ?>" method="post" enctype="multipart/form-data" name="form1" id="form1"> 
			<table>
				<tr>
					<td style="min-width:100px;">			  	 
		  				<label for="file"><?php echo $langval637 ?>:&nbsp;&nbsp;</label>
		  			</td>
					<td style="padding-right:8px;">
						<input name="file" type="file" id="file" />
					</td>
					<td>
						<input class="btn btn-primary" type="submit" name="submitfile" value="<?php echo $langval638 ?>" /> 	
					</td>
				</tr> 
			</table>
																											
		</form> 
	
		<?php 
		}
	}
	include ('onedocmydocdrivesearch.php');
	echo "<br/>";
	
}

include('onedocmydocupdatepermissions.php');

?>