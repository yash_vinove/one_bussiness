<?php

//LANGUAGE COLLECTION SECTION
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (390,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}


//add project button
$pagename = $_SERVER['REQUEST_URI'];
$pagename = str_replace("&", "xxxxxxxxxx", $pagename);	
echo "<a class='button-primary' href='pageadd.php?pagetype=websitesearchanalysis&pagename=".$pagename."'>".$langval390."</a>";
echo "<br/><br/>";

//get businessunits
$businessunitswhere = '';
if($_SESSION["superadmin"] == 0){
	$businessunitswhere = $_SESSION["userbusinessunit"];
}							

//update completion percentages
$getdatacomp = "select * from $database.websitesearchanalysis 
where websitesearchanalysis.datecompleted = '0000-00-00' and websitesearchanalysis.disabled = '0'";
if($businessunitswhere <> ''){
	$getdatacomp = $getdatacomp." and websitesearchanalysis.businessunitid in (".$businessunitswhere.")";
}
$getdatacomp = mysql_query($getdatacomp);
while($row99 = mysql_fetch_array($getdatacomp)){
	$cwebsitesearchanalysisid = $row99['websitesearchanalysisid'];
	
	//check how many tasks completed
	$getcompleted = mysql_query("select websitesearchanalysistaskid from $database.websitesearchanalysistask
	where websitesearchanalysisid = '$cwebsitesearchanalysisid' and websitesearchtaskstatusid='4'");
	$completedcount = mysql_num_rows($getcompleted);
	
	//check how many tasks not completed
	$getuncompleted = mysql_query("select websitesearchanalysistaskid from $database.websitesearchanalysistask
	where websitesearchanalysisid = '$cwebsitesearchanalysisid' and websitesearchtaskstatusid in (1,2,3)");
	$notcompletedcount = mysql_num_rows($getuncompleted);
	
	//calculate % complete
	$total = $completedcount + $notcompletedcount;
	if($notcompletedcount == 0){
		$percentage = 100;		
	}
	else {
		$percentage = round(($completedcount / $total) * 100, 0);
	}
	
	//update values in websitesearchanalysis
	$updatecounts = mysql_query("update $database.websitesearchanalysis set completionpercentage = '$percentage', 
	nooftaskscompleted = '$completedcount', nooftasksoutstanding = '$notcompletedcount', datecompleted = '0000-00-00' 
	where websitesearchanalysisid = '$cwebsitesearchanalysisid'");
}

//Active Search Analysis
echo "<h2>".$langval391."</h2>";
$getdata = "select * from $database.websitesearchanalysis 
inner join $database.website on websitesearchanalysis.onewebsiteid = website.websiteid
where websitesearchanalysis.datecompleted = '0000-00-00' and websitesearchanalysis.disabled = '0'";
if($businessunitswhere <> ''){
	$getdata = $getdata." and websitesearchanalysis.businessunitid in (".$businessunitswhere.")";
}
$getdata = $getdata." order by websitesearchanalysis.datecreated desc";
$getdata = mysql_query($getdata);
if(mysql_num_rows($getdata)>= 1){
	echo "<table class='table table-bordered'>";
	echo "<thead>";
	echo "<td><b>".$langval392."</b></td>";
	echo "<td><b>".$langval393."</b></td>";
	echo "<td><b>".$langval394."</b></td>";
	echo "<td><b>".$langval395."</b></td>";
	echo "<td><b>".$langval396."</b></td>";
	echo "<td><b>".$langval397."</b></td>";
	echo "<td><b>".$langval398."</b></td>";
	echo "<td><b>".$langval399."</b></td>";
	echo "<td><b>".$langval400."</b></td>";
	echo "<td><b>".$langval401."</b></td>";
	echo "<td><b>".$langval402."</b></td>";
	echo "</thead>";
	while($datarow = mysql_fetch_array($getdata)){
		$websitesearchanalysisid = $datarow['websitesearchanalysisid'];
		$websitesearchanalysisname = $datarow['websitesearchanalysisname'];
		$websitename = $datarow['websitename'];
		$url = $datarow['websiteurl'];
		$startdate = $datarow['datecreated'];
		$reportingemailaddresses = $datarow['reportingemailaddresses'];
		$completionpercentage = $datarow['completionpercentage']. "%";
		$nooftaskscompleted = $datarow['nooftaskscompleted'];
		$nooftasksoutstanding = $datarow['nooftasksoutstanding'];
		echo "<tr>";
		echo "<td>".$websitesearchanalysisid."</td>";
		echo "<td>".$websitesearchanalysisname."</td>";
		echo "<td>".$websitename."</td>";
		echo "<td>".$url."</td>";
		echo "<td>".$startdate."</td>";
		echo "<td>".$reportingemailaddresses."</td>";
		echo "<td>".$completionpercentage."</td>";
		echo "<td>".$nooftaskscompleted."</td>";
		echo "<td>".$nooftasksoutstanding."</td>";
		echo "<td><a href='pageedit.php?pagetype=websitesearchanalysis&rowid=".$websitesearchanalysisid."&pagename=".$pagename."'>".$langval401."</a></td>";
			echo "<td><a href='view.php?viewid=25&rowid=".$websitesearchanalysisid."'>".$langval402."</a></td>";
		echo "</tr>";
	}
	echo "</table>";
}

$thisdate = date('Y-m-d');
$thisdate = date('Y-m-d',date(strtotime("-7 days", strtotime($thisdate))));

//Recently Completed
echo "<br/><h2>".$langval403."</h2>";
$getdata2 = "select * from $database.websitesearchanalysis 
inner join $database.website on websitesearchanalysis.onewebsiteid = website.websiteid
where websitesearchanalysis.datecompleted <> '0000-00-00' 
and websitesearchanalysis.datecompleted >= '$thisdate' and websitesearchanalysis.disabled = '0'";
if($businessunitswhere <> ''){
	$getdata2 = $getdata2." and websitesearchanalysis.businessunitid in (".$businessunitswhere.")";
}
$getdata2 = $getdata2." order by websitesearchanalysis.datecompleted desc";
$getdata2 = mysql_query($getdata2);
if(mysql_num_rows($getdata2)>= 1){
	echo "<table class='table table-bordered'>";
	echo "<thead>";
	echo "<td><b>".$langval392."</b></td>";
	echo "<td><b>".$langval393."</b></td>";
	echo "<td><b>".$langval394."</b></td>";
	echo "<td><b>".$langval395."</b></td>";
	echo "<td><b>".$langval396."</b></td>";
	echo "<td><b>".$langval404."</b></td>";
	echo "<td><b>".$langval397."</b></td>";
	echo "<td><b>".$langval401."</b></td>";
	echo "<td><b>".$langval405."</b></td>";
	echo "</thead>";
	while($datarow2 = mysql_fetch_array($getdata2)){
		$websitesearchanalysisid = $datarow2['websitesearchanalysisid'];
		$websitesearchanalysisname = $datarow2['websitesearchanalysisname'];
		$websitename = $datarow2['websitename'];
		$url = $datarow2['websiteurl'];
		$startdate = $datarow2['datecreated'];
		$completeddate = $datarow2['datecompleted'];
		$reportingemailaddresses = $datarow2['reportingemailaddresses'];
		echo "<tr>";
		echo "<td>".$websitesearchanalysisid."</td>";
		echo "<td>".$websitesearchanalysisname."</td>";
		echo "<td>".$websitename."</td>";
		echo "<td>".$url."</td>";
		echo "<td>".$startdate."</td>";
		echo "<td>".$completeddate."</td>";
		echo "<td>".$reportingemailaddresses."</td>";
		echo "<td><a href='pageedit.php?pagetype=websitesearchanalysis&rowid=".$websitesearchanalysisid."&pagename=".$pagename."'>".$langval401."</a></td>";
			echo "<td><a href='view.php?viewid=25&rowid=".$websitesearchanalysisid."'>".$langval405."</a></td>";
		echo "</tr>";
	}
	echo "</table>";
}

?>