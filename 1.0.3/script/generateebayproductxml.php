<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include('config.php');
$date = date("Y-m-d");
$hour = date("H");
$minute = date("i");
$date180 = date('Y-m-d', strtotime("- 180 days"));
echo "<br/>hour: ".$hour;
echo "<br/>minute: ".$minute;					
					
echo "<br/><br/><b>GET ECOMMERCE LISTING LOCATION RECORDS WITHOUT EBAY PRODUCT XML</b>";
//create products that have listing locations
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$getlistinglocations = "select ecommercelistinglocationid, ecommercelistingid, productid, listingprice, productindividual, apikey3
from $database.ecommercelistinglocation
inner join ecommercesiteconfig on ecommercesiteconfig.ecommercesiteconfigid = ecommercelistinglocation.ecommercesiteconfigid
where ecommercelistinglocationstatusid in (1,2,3,4,7,8,9,10,11,12,14,15,16,17) and ecommercelistinglocation.ecommercesiteconfigid in (2) 
and amazonproductxml = '' and listingprice > 0
limit 500";
echo "<br/><br/>".$getlistinglocations;
$getlistinglocations = mysql_query($getlistinglocations);
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get list of products that require processing: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
$listingids = "";
$ecommercelistinglocationarray = array();
while($row11 = mysql_fetch_array($getlistinglocations)){
	$ecommercelistinglocationid = $row11['ecommercelistinglocationid'];
	$ecommercelistingid = $row11['ecommercelistingid'];
	$listingprice = $row11['listingprice'];
	$productid = $row11['productid'];
	$productindividual = $row11['productindividual'];
	$channelid = $row11['apikey3'];	
	$lang = 'en_US';	
	if($channelid == "EBAY_UK"){ $lang = 'en_GB'; }
	if($channelid == "EBAY_US"){ $lang = 'en_US'; }
	$listingids = $listingids.$ecommercelistingid.",";	
	array_push($ecommercelistinglocationarray, array('ecommercelistinglocationid'=>$ecommercelistinglocationid,
	'listingprice'=>$listingprice,'ecommercelistingid'=>$ecommercelistingid, 'productindividual'=>$productindividual,
	'lang'=>$lang, 'productid'=>$productid));
}
$listingids = rtrim($listingids, ",");
echo "<br/><br/>listingids: ".$listingids;
echo "<br/><br/>ecommercelistinglocationarray: ".json_encode($ecommercelistinglocationarray);
echo "<br/><br/>";

if($listingids <> ""){
	//get ecommercelisting records
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getlistings = "select ecommercelistingid, productstockitemid
	from $database.ecommercelisting
	where ecommercelistingid in ($listingids)";
	echo "<br/><br/>".$getlistings;
	$getlistings = mysql_query($getlistings);
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get list of ecommercelisting records: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	$productstockitemids = "";
	$ecommercelistingarray = array();
	while($row11 = mysql_fetch_array($getlistings)){
		$ecommercelistingid = $row11['ecommercelistingid'];
		$productstockitemid = $row11['productstockitemid'];
		$productstockitemids = $productstockitemids.$productstockitemid.",";	
		array_push($ecommercelistingarray, array('ecommercelistingid'=>$ecommercelistingid,
		'productstockitemid'=>$productstockitemid));
	}
	
	echo "<br/><br/>ecommercelistingarray: ".json_encode($ecommercelistingarray);
	$productstockitemids = rtrim($productstockitemids, ",");
	echo "<br/><br/>productstockitemids: ".$productstockitemids;
	echo "<br/><br/>";
	
	
	//get productstockitem records
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getproductstockitem = "select productstockitemid, productitemdescription, productid
	from $database.productstockitem
	where productstockitemid in ($productstockitemids)";
	echo "<br/><br/>".$getproductstockitem;
	$getproductstockitem = mysql_query($getproductstockitem);
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get list of productstockitem records: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	$productids = "";
	$productstockitemarray = array();
	while($row11 = mysql_fetch_array($getproductstockitem)){
		$productstockitemid = $row11['productstockitemid'];
		$productitemdescription = $row11['productitemdescription'];
		$productitemdescription = str_replace(" ", "", $productitemdescription);
		$productitemdescription = str_replace("-", "", $productitemdescription);
		$productid = $row11['productid'];
		$productids = $productids.$productid.",";	
		array_push($productstockitemarray, array('productstockitemid'=>$productstockitemid,
		'productitemdescription'=>$productitemdescription,'productid'=>$productid));
	}
	
	echo "<br/><br/>productstockitemarray: ".json_encode($productstockitemarray);
	$productids = rtrim($productids, ",");
	echo "<br/><br/>productids: ".$productids;
	echo "<br/><br/>";
	
	
	
	//get product records
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getproduct = "select productid, productname, productlongdescription, 
	productproducer, productuniqueid, retailprice, kgweight, productimage1, productimage2, productimage3, masterproductid, 
	productalternativename1
	from $database.product
	where productid in ($productids)";
	echo "<br/><br/>".$getproduct;
	$getproduct = mysql_query($getproduct);
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get list of product records: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	$productarray = array();
	$productimagearray = array();
	while($row11 = mysql_fetch_array($getproduct)){
		$productid = $row11['productid'];	
		$masterproductid = $row11['masterproductid'];	
		$productname = $row11['productname'];	
		$productalternativename1 = $row11['productalternativename1'];	
		if($productalternativename1 <> ""){
			$productname = $productalternativename1;		
		}
		$productname = htmlspecialchars($productname);
		$productlongdescription = $row11['productlongdescription'];	
		$productproducer = htmlspecialchars($row11['productproducer']);	
		$productuniqueid = $row11['productuniqueid'];	
		$retailprice = $row11['retailprice'];	
		$kgweight = $row11['kgweight'];	
		$productimage1 = $row11['productimage1'];	
		$productimage2 = $row11['productimage2'];	
		$productimage3 = $row11['productimage3'];	
		
		if($productimage1 == '' && $productimage2 == '' && $productimage3 == ''){
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$getproductimage = "select documenturl, doctypename from structurefunctiondocument
			where rowid = $productid and structurefunctionid = 74";
			echo "<br/><br/>".$getproductimage;
			$getproductimage = mysql_query($getproductimage);
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Get list of product image records: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			$z = 1;
			while($getproductimagerow = mysql_fetch_array($getproductimage)){
				$documenturl = $getproductimagerow['documenturl'];
				$doctypename = $getproductimagerow['doctypename'];
				if($z == 1){
					$productimage1 = "https://www.onebusiness-liveserver.com/onebusiness/documents/".$documenturl;					
				}
				if($z == 2){
					$productimage2 = "https://www.onebusiness-liveserver.com/onebusiness/documents/".$documenturl;					
				}
				if($z == 3){
					$productimage3 = "https://www.onebusiness-liveserver.com/onebusiness/documents/".$documenturl;					
				}
				$z = $z + 1;
			}
		}
		
		
		array_push($productarray, array('productid'=>$productid, 'masterproductid'=>$masterproductid,
		'productname'=>$productname,'productlongdescription'=>$productlongdescription,
		'productproducer'=>$productproducer,'productuniqueid'=>$productuniqueid,
		'retailprice'=>$retailprice,'kgweight'=>$kgweight));
		array_push($productimagearray, array('productid'=>$productid,
		'productimage1'=>$productimage1,'productimage2'=>$productimage2,'productimage3'=>$productimage3));
	}
	
	echo "<br/><br/>productarray: ".json_encode($productarray);
	echo "<br/><br/>";
	echo "<br/><br/>productimagearray: ".json_encode($productimagearray);
	echo "<br/><br/>";
	
	
	//get zzproductdata records
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getproductdata = "select productid, mpn, additionalebayattributexml
	from $database.zzproductdata
	where productid in ($productids)";
	echo "<br/><br/>".$getproductdata;
	$getproductdata = mysql_query($getproductdata);
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get list of zzproductdata records: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	$productdataarray = array();
	while($row11 = mysql_fetch_array($getproductdata)){
		$productid = $row11['productid'];	
		$mpn = $row11['mpn'];	
		$additionalebayattributexml = $row11['additionalebayattributexml'];	
		array_push($productdataarray, array('productid'=>$productid,
		'mpn'=>$mpn,'additionalebayattributexml'=>$additionalebayattributexml));
	}
	
	echo "<br/><br/>productdataarray: ".json_encode($productdataarray);
	echo "<br/><br/>";
	
	
	
	//construct the xml and process update
	$updatequery = "update ecommercelistinglocation set productdatachanged = 1, 
	amazonproductxml = case ecommercelistinglocationid ";
	$updatequerywhere = "";
	if(count($productarray) >= 1){
		foreach($ecommercelistinglocationarray as $ecommercelistinglocation){
			
			//get all of the variables needed for the listing
			$ecommercelistinglocationid = $ecommercelistinglocation['ecommercelistinglocationid'];
			$ecommercelistingid = $ecommercelistinglocation['ecommercelistingid'];
			$productindividual = $ecommercelistinglocation['productindividual'];
			$productid = $ecommercelistinglocation['productid'];
			$lang = $ecommercelistinglocation['lang'];
			$listingprice = $ecommercelistinglocation['listingprice'];
			echo "<br/>listing: ".$ecommercelistinglocationid." - ".$ecommercelistingid." - ".$listingprice;
			$key = array_search($ecommercelistingid, array_column($ecommercelistingarray, 'ecommercelistingid'));
			$productstockitemid = $ecommercelistingarray[$key]['productstockitemid'];
			echo " - ".$productstockitemid;
			$key = array_search($productid, array_column($productarray, 'productid'));
			$masterproductid = $productarray[$key]['masterproductid'];	
			$productname = htmlspecialchars($productarray[$key]['productname']);					
			$key = array_search($productstockitemid, array_column($productstockitemarray, 'productstockitemid'));
			$productitemdescription = $productstockitemarray[$key]['productitemdescription'];
			echo " - ".$productid;
			
			//use https://www.textfixer.com/html/compress-html-compression.php to condense this to one line
			$htmldescriptionbegin = '<style type="text/css">#SuperWrapper {width: 800px;margin-left: auto;margin-right: auto;font-family: arial, Helvetica, sans-serif;font-size: 12px;}#SuperWrapper p {margin: 0px;padding: 0px 0px 5px 0px;line-height: 20px;}#SuperWrapper h1 {padding: 10px 0px 5px 0px;margin: 0px;font-size: 26px;font-weight: bold;letter-spacing: -1px;color: #003366;}h2 {padding: 10px 0px 5px 0px;margin: 0px;font-size: 16px;font-weight: bold;letter-spacing: -1px;color: #003366;}#SuperWrapper a {font-weight: bold;text-decoration: underline;color: #003366;}#SuperWrapper a:hover {text-decoration:none;}#SuperContentsWrapper {width: 800px;background-image: url("https://www.onebusiness-liveserver.com/bluewondercommerce/images/Contents.jpg");}#SuperContents {width: 800px;background-image: url("https://www.onebusiness-liveserver.com/bluewondercommerce/images/ContentsTop.jpg");background-repeat: no-repeat;}#SuperContentsSub {padding: 30px 40px 0px 40px;}#SuperFooter {width: 800px;height: 44px;background-image:url("https://www.onebusiness-liveserver.com/bluewondercommerce/images/Footer.jpg");}#SuperFooterLink {width: 800px;background-image:url("https://www.onebusiness-liveserver.com/bluewondercommerce/images/BG.jpg");height: 100px;}#SuperBoxContents {padding: 0px 80px 0px 80px;margin: 0px;}#SuperBoxContents p {padding: 0px 0px 10px 0px;margin: 0px;line-height: 20px;}#SuperBoxContents ul {padding: 0px 0px 0px 28px;margin: 0px;list-style-type: disc;}#SuperBoxContents li {line-height: 20px;}/* HTML5 ELEMENTS *//* sub images > thumbnail list */ul#SuperThumbs, ul#SuperThumbs li {margin: 0;padding: 0;list-style: none;}ul#SuperThumbs li {float: left;background: #ffffff;border: 1px solid #cccccc;margin: 0px 0px 10px 10px;padding: 8px;-moz-border-radius: 10px;border-radius: 10px;}ul#SuperThumbs a {float: left;display: block;width: 150px;height: 150px;line-height: 100px;overflow: hidden;position: relative;z-index: 1;}ul#SuperThumbs a img {float: left;width: 100%;height: 100%;border: 0px;}/* sub images > mouse over */ul#SuperThumbs a:hover {overflow: visible;z-index: 1000;border: none;}ul#SuperThumbs a:hover img {background: #ffffff;border: 1px solid #cccccc;padding: 10px;-moz-border-radius: 10px;border-radius: 10px; position: absolute; top:-20px; left:-50px; width: auto; height: auto; }/* sub images > clearing floats */ul#SuperThumbs:after, li#SuperThumbs:after {content: ".";display: block;height: 0;clear: both;visibility: hidden;}ul#SuperThumbs, li#SuperThumbs {display: block;}ul#SuperThumbs, li#SuperThumbs {min-height: 1%;}* html ul#SuperThumbs, * html li#SuperThumbs {height: 1%;}</style><div id="SuperWrapper"><div id="SuperContentsWrapper"><div id="SuperContents"><div id="SuperContentsSub">';
			$htmldescriptionend = '<br/><h2>About Us</h2>Welcome to Blue Wonder Commerce. We only sell the very best consumer products, sourced from trusted suppliers/manufacturers who have passed our stringent tests for product quality and consistency of service.<br/><br/>We will always go one step further to make your journey with us an enjoyable one. Place your trust in us to deliver on our promises to you.<br/><br/><h2>Payment Policy</h2>Payment is required before item will be despatched. You can pay using either Paypal or Credit Card.<br/><br/><h2>Shipping Policy</h2>Items are usually despatched within 24 hours. Items will definitely be shipped within 1-2 business days. We only use reputable shipping providers.<br/><br/><h2>Returns Policy</h2>We have a 30 day returns policy. If there is anything defective please return within 30 days in the same condition as you received the item, in order to get your money back. Please contact us before returning, so that we can provide you with return details.<ul><li>Return within 30 days of purchase.</li><li>Return with all original packaging.</li><li>Return the item in the same condition as you received.</li></ul><br/><br/></div></div></div></div>';

			
			if($masterproductid == 0){
				echo "<br/><br/><b>PROCESS SINGLE PRODUCT RECORD</b>";
				$key = array_search($productid, array_column($productarray, 'productid'));
				//$productlongdescription = str_replace("<p>", "", $productlongdescription);
				//$productlongdescription = str_replace("</p>", "", $productlongdescription);
				//$productlongdescription = str_replace("<br/>", " ", $productlongdescription);
				$productlongdescription = $productarray[$key]['productlongdescription'];	
				//$productlongdescription = htmlspecialchars($productlongdescription);	
				$productproducer = $productarray[$key]['productproducer'];	
				$productuniqueid = $productarray[$key]['productuniqueid'];
				$retailprice = $productarray[$key]['retailprice'];
				$kgweight = $productarray[$key]['kgweight'];	
				$key = array_search($productid, array_column($productarray, 'productid'));
				$key = array_search($productid, array_column($productimagearray, 'productid'));
				$productimage1 = $productimagearray[$key]['productimage1'];	
				$productimage2 = $productimagearray[$key]['productimage2'];	
				$productimage3 = $productimagearray[$key]['productimage3'];	
				$mpn = $productdataarray[$key]['mpn'];	
				$additionalebayattributexml = $productdataarray[$key]['additionalebayattributexml'];	
				$weightmajor = round($kgweight, 0, PHP_ROUND_HALF_DOWN);
				$weightminor = ($kgweight - $weightmajor)*1000;
				echo " - ".$productname." - ".$productlongdescription." - ".$productproducer;
				echo " - ".$productuniqueid." - ".$retailprice." - ".$kgweight;
				$productuniqueid = trim($productuniqueid);
				if($productindividual == 0){
					$sku = "P".$productid;
				}
				else {
					$sku = "PSI".$productstockitemid;
				}
				if($mpn == ""){
					$mpn = "Does Not Apply";			
				}
				if($productitemdescription == ""){
					$condition = 'New';			
				}
				else {
					$condition = $productitemdescription;			
				}
				if($productuniqueid == ""){
					$productuniqueid == "N/A";			
				}
				$imageurl1 = str_replace(" ", "%20", $productimage1);
				$imageurl2 = str_replace(" ", "%20", $productimage2);
				$imageurl3 = str_replace(" ", "%20", $productimage3);
				$htmldescriptionbegin = $htmldescriptionbegin."<h1>".$productname."</h1>";			
			
				
				$proddesc = "<![CDATA[".$htmldescriptionbegin.$productlongdescription.$htmldescriptionend."]]>";
	echo "<br/><br/>";
	echo "<br/><br/>";
	echo "proddesc: ".$proddesc;
	echo "<br/><br/>";
	echo "<br/><br/>";
				
				//construct product xml string
				$ebayproductdatasegment = '<product>
			<SKU>'.$sku.'</SKU>
			<productInformation localizedFor="'.$lang.'">
				<title>'.$productname.'</title>
				<subtitle></subtitle>
				<description>
					<productDescription>'.$proddesc.'</productDescription>
					<additionalInfo></additionalInfo>
				</description>
				<attribute name="Brand">'.$productproducer.'</attribute>
				<attribute name="MPN">'.$mpn.'</attribute>'
				.$additionalebayattributexml.
				'<UPC></UPC>
				<ISBN></ISBN>
				<EAN>'.$productuniqueid.'</EAN>
				<MPN></MPN>
				<Brand></Brand>
				<ePID></ePID>
				<pictureURL>'.$imageurl1.'</pictureURL>';
				if($imageurl2 <> ''){
					$ebayproductdatasegment = $ebayproductdatasegment.'<pictureURL>'.$imageurl2.'</pictureURL>';
				}
				if($imageurl3 <> ''){
					$ebayproductdatasegment = $ebayproductdatasegment.'<pictureURL>'.$imageurl3.'</pictureURL>';
				}
				$ebayproductdatasegment = $ebayproductdatasegment.'
				<conditionInfo>
					<condition>'.$condition.'</condition>
					<conditionDescription></conditionDescription>
				</conditionInfo>
				<shippingDetails measurementSystem="METRIC">
					<weightMajor>'.$weightmajor.'</weightMajor>
					<weightMinor>'.$weightminor.'</weightMinor>
					<length></length>
					<width></width>
					<height></height>
					<packageType></packageType>
				</shippingDetails>
			</productInformation>
		</product>';
				echo "<br/><br/>".nl2br(htmlentities($ebayproductdatasegment));
				echo "<br/><br/>";
			    $ebayproductdatasegment = mysql_real_escape_string($ebayproductdatasegment);
			    $updatequery = $updatequery." when ".$ecommercelistinglocationid." then '".$ebayproductdatasegment."' ";
				$updatequerywhere = $updatequerywhere.$ecommercelistinglocationid.",";
				
			}
			
			
			if($masterproductid <> 0){
				echo "<br/><br/><b>PROCESS MASTER PRODUCT RECORD</b>";
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$masterproduct = mysql_query("select productname, productalternativename1, productlongdescription, 
				productproducer, productuniqueid, retailprice, kgweight, productimage1, productimage2, productimage3,
				additionalebayattributexml, additionalebayvariationvector, mpn
				from product 
				inner join zzproductdata on zzproductdata.productid = product.productid
				where product.productid = $masterproductid");
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Get the master product details: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;		
				while($masterproductrow = mysql_fetch_array($masterproduct)){
					$productname = $masterproductrow['productname'];
					$productalternativename1 = $masterproductrow['productalternativename1'];
					if($productalternativename1 <> ""){
						$productname = $productalternativename1;		
					}
					$productname = htmlspecialchars($productname);
					$productlongdescription = $masterproductrow['productlongdescription'];
					$productproducer = htmlspecialchars($masterproductrow['productproducer']);
					$productuniqueid = $masterproductrow['productuniqueid'];
					$masterproductuniqueid = $masterproductrow['productuniqueid'];
					$retailprice = $masterproductrow['retailprice'];
					$kgweight = $masterproductrow['kgweight'];
					$mpn = $masterproductrow['mpn'];
					$productimage1 = $masterproductrow['productimage1'];
					$productimage2 = $masterproductrow['productimage2'];
					$productimage3 = $masterproductrow['productimage3'];
					$additionalebayattributexml = $masterproductrow['additionalebayattributexml'];
					$additionalebayvariationvector = $masterproductrow['additionalebayvariationvector'];
					$kgweight = $masterproductrow['kgweight'];	
					$weightmajor = round($kgweight, 0, PHP_ROUND_HALF_DOWN);
					$weightminor = ($kgweight - $weightmajor)*1000;
				
				
					if($productimage1 == '' && $productimage2 == '' && $productimage3 == ''){
						$nosqlqueries = $nosqlqueries + 1;
						$sqlstarttime = microtime(true);
						$getproductimage = "select documenturl, doctypename from structurefunctiondocument
						where rowid = $masterproductid and structurefunctionid = 74";
						echo "<br/><br/>".$getproductimage;
						$getproductimage = mysql_query($getproductimage);
						$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
						$differencemilliseconds = microtime(true) - $sqlstarttime;
						echo "<br/>Get list of product image records: ";
						echo "<br/>differencemilliseconds: ".$differencemilliseconds;
						echo "<br/>sqlqueriestime: ".$sqlqueriestime;
						$z = 1;
						while($getproductimagerow = mysql_fetch_array($getproductimage)){
							$documenturl = $getproductimagerow['documenturl'];
							$doctypename = $getproductimagerow['doctypename'];
							if($z == 1){
								$productimage1 = "https://www.onebusiness-liveserver.com/onebusiness/documents/".$documenturl;					
							}
							if($z == 2){
								$productimage2 = "https://www.onebusiness-liveserver.com/onebusiness/documents/".$documenturl;					
							}
							if($z == 3){
								$productimage3 = "https://www.onebusiness-liveserver.com/onebusiness/documents/".$documenturl;					
							}
							$z = $z + 1;
						}
					}
					$imageurl1 = str_replace(" ", "%20", $productimage1);
					$imageurl2 = str_replace(" ", "%20", $productimage2);
					$imageurl3 = str_replace(" ", "%20", $productimage3);
					$htmldescriptionbegin = $htmldescriptionbegin."<h1>".$productname."</h1>";			
					$proddesc = "<![CDATA[".$htmldescriptionbegin.$productlongdescription.$htmldescriptionend."]]>";
					if($mpn == ""){
						$mpn = "Does Not Apply";			
					}
				
					
					$ebayproductdatasegment = '<productVariationGroup>
<groupID>'.$productuniqueid.'</groupID>
 <groupInformation localizedFor="'.$lang.'">
<variationVector>
'.$additionalebayvariationvector.'
</variationVector>
<sharedProductInformation>
<title>
'.$productname.'
</title>
<subtitle/>
<description>
<productDescription>
'.$proddesc.'
</productDescription>
<additionalInfo/>
</description>
<attribute name="Brand">'.$productproducer.'</attribute>
<attribute name="MPN">'.$mpn.'</attribute>				
'.$additionalebayattributexml.'
<pictureURL>'.$imageurl1.'</pictureURL>';
if($imageurl2 <> ''){
	$ebayproductdatasegment = $ebayproductdatasegment.'<pictureURL>'.$imageurl2.'</pictureURL>';
}
if($imageurl3 <> ''){
	$ebayproductdatasegment = $ebayproductdatasegment.'<pictureURL>'.$imageurl3.'</pictureURL>';
}				
$ebayproductdatasegment = $ebayproductdatasegment.'
<shippingDetails measurementSystem="METRIC">
<weightMajor>'.$weightmajor.'</weightMajor>
<weightMinor>'.$weightminor.'</weightMinor>
<length></length>
<width></width>
<height></height>
<packageType></packageType>
</shippingDetails>
</sharedProductInformation>
</groupInformation>
</productVariationGroup>';

					//get product variants
					$nosqlqueries = $nosqlqueries + 1;
					$sqlstarttime = microtime(true);
					$productvariant = mysql_query("select product.productid, additionalebayattributexml, productimage1, productimage2, 
					productimage3, productuniqueid from product 
					inner join zzproductdata on zzproductdata.productid = product.productid
					where product.masterproductid = $masterproductid and product.disabled = 0");
					$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
					$differencemilliseconds = microtime(true) - $sqlstarttime;
					echo "<br/>Get the master products variants: ";
					echo "<br/>differencemilliseconds: ".$differencemilliseconds;
					echo "<br/>sqlqueriestime: ".$sqlqueriestime;		
					while($productvariantrow = mysql_fetch_array($productvariant)){
						$sku = "P".$productvariantrow['productid'];		
						$additionalebayattributexml = $productvariantrow['additionalebayattributexml'];		
						$productuniqueid = $productvariantrow['productuniqueid'];
						$productimage1 = $productvariantrow['productimage1'];
						$productimage2 = $productvariantrow['productimage2'];
						$productimage3 = $productvariantrow['productimage3'];
					
						if($productimage1 == '' && $productimage2 == '' && $productimage3 == ''){
							$nosqlqueries = $nosqlqueries + 1;
							$sqlstarttime = microtime(true);
							$getproductimage = "select documenturl, doctypename from structurefunctiondocument
							where rowid = $masterproductid and structurefunctionid = 74";
							echo "<br/><br/>".$getproductimage;
							$getproductimage = mysql_query($getproductimage);
							$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
							$differencemilliseconds = microtime(true) - $sqlstarttime;
							echo "<br/>Get list of product image records: ";
							echo "<br/>differencemilliseconds: ".$differencemilliseconds;
							echo "<br/>sqlqueriestime: ".$sqlqueriestime;
							$z = 1;
							while($getproductimagerow = mysql_fetch_array($getproductimage)){
								$documenturl = $getproductimagerow['documenturl'];
								$doctypename = $getproductimagerow['doctypename'];
								if($z == 1){
									$productimage1 = "https://www.onebusiness-liveserver.com/onebusiness/documents/".$documenturl;					
								}
								if($z == 2){
									$productimage2 = "https://www.onebusiness-liveserver.com/onebusiness/documents/".$documenturl;					
								}
								if($z == 3){
									$productimage3 = "https://www.onebusiness-liveserver.com/onebusiness/documents/".$documenturl;					
								}
								$z = $z + 1;
							}
						}
						$imageurl1 = str_replace(" ", "%20", $productimage1);
						$imageurl2 = str_replace(" ", "%20", $productimage2);
						$imageurl3 = str_replace(" ", "%20", $productimage3);
						
						if($productitemdescription == ""){
							$condition = 'New';			
						}
						else {
							$condition = $productitemdescription;			
						}			
					
					 	$ebayproductdatasegment = $ebayproductdatasegment.'
<product>
<SKU variantOf="'.$masterproductuniqueid.'">'.$sku.'</SKU>
<productInformation localizedFor="'.$lang.'">
'.$additionalebayattributexml.'
<pictureURL>'.$imageurl1.'</pictureURL>';
$ebayproductdatasegment = $ebayproductdatasegment.'
<conditionInfo>
	<condition>'.$condition.'</condition>
	<conditionDescription></conditionDescription>
</conditionInfo>
<UPC></UPC>
<ISBN></ISBN>
<EAN>'.$productuniqueid.'</EAN>
<MPN></MPN>
<Brand></Brand>
<ePID></ePID>
</productInformation>
</product>';

					}
					
					echo "<br/><br/>".nl2br(htmlentities($ebayproductdatasegment));
					echo "<br/><br/>";
				    $ebayproductdatasegment = mysql_real_escape_string($ebayproductdatasegment);
				    $updatequery = $updatequery." when ".$ecommercelistinglocationid." then '".$ebayproductdatasegment."' ";
					$updatequerywhere = $updatequerywhere.$ecommercelistinglocationid.",";

				}
			}
			
			
		}
	}
	if($updatequerywhere <> ""){
		$updatequerywhere = rtrim($updatequerywhere, ",");
		$updatequery = $updatequery."else '' end where ecommercelistinglocationid in (".$updatequerywhere.")";
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		echo "<br/><br/>".$updatequery;
		$updatequery = mysql_query($updatequery);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Insert the amazon id of the file being processed: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	}
}

include('recordjobstatistics.php');
?>