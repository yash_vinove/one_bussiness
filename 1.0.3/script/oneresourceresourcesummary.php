<?php

//LANGUAGE COLLECTION SECTION
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (4,5,9,10,19,582,583,584,585,586,587,588,589,590,591,592,593,594,595,596,597,598,599,600,601,602,603,
604,605,606,607,608,610,677,678,679,680,681,682,683,684,685,686,687,688,689,670,671,672,673,674,675,676,677,678,679,680,
681,682,683,684,685,686,687,688,689,690,691,692,693,694,695,696,697,698,699,700,701,702,703,704,705,706,707,708,709,710,
711,712,713,714,803,804,805,806,807,808,809,810,811,812,813,814,815,816,817,818,819,820,821,822,823,824,825,826,827,828)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}

//FILTERS
$pagename = $_SERVER['REQUEST_URI'];
$pagename = str_replace("&", "xxxxxxxxxx", $pagename);								

$displaytype = isset($_GET['displaytype']) ? $_GET['displaytype'] : '';
$resourceid = isset($_POST['resourceid']) ? $_POST['resourceid'] : '';
$resid = isset($_GET['resid']) ? $_GET['resid'] : '';
$startdate = isset($_POST['startdate']) ? $_POST['startdate'] : '';
$start = isset($_GET['start']) ? $_GET['start'] : '';
$enddate = isset($_POST['enddate']) ? $_POST['enddate'] : '';
$end = isset($_GET['end']) ? $_GET['end'] : '';
//echo "<br/>resourceid: ".$resourceid;
//echo "<br/>startdate: ".$startdate;
//echo "<br/>enddate: ".$enddate;

if($resid <> ""){
	$resourceid = $resid;
}
if($start <> ""){
	$startdate = $start;
}
if($end <> ""){
	$enddate = $end;
}
?>
<form class="form-horizontal" action='view.php?viewid=50' method="post" enctype="multipart/form-data">
	<div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
		<div class="form-group">
		 	<label for="Resource" class="col-xs-6 col-sm-2 col-md-1 col-lg-1 control-label"><?php echo $langval803 ?></label>
	    	<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
    			<?php
    			$resulttype = mysql_query("select resourceid, resourcename from resource where disabled = 0");
				echo "<select class='form-control' name='resourceid'>";
		      	echo "<option value=''>$langval804</option>";
         		while($resourcerow=mysql_fetch_array($resulttype)){
         			if ($resourceid == $resourcerow['resourceid']) {
               		echo "<option value=".$resourcerow['resourceid']." selected='true'>".$resourcerow['resourcename']."</option>";
          		}
          		else {
            			echo "<option value=".$resourcerow['resourceid']." >".$resourcerow['resourcename']."</option>";
          		}
             }
  				echo "</select>";
  				?>
	    	</div>
	  		<label for="Start Date" class="col-xs-6 col-sm-1 col-md-1 col-lg-1 control-label"><?php echo $langval805 ?></label>
	    	<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
	      	<input type="date" class="form-control" name="startdate" value="<?php echo $startdate ?>">
	    	</div>
	  		<label for="End Date" class="col-xs-6 col-sm-1 col-md-1 col-lg-1 control-label"><?php echo $langval806 ?></label>
	    	<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
	      	<input type="date" class="form-control" name="enddate" value="<?php echo $enddate ?>">
	    	</div>
	    	<div class="col-md-3 col-lg-3 col-sm-2 col-xs-12">
    		<div style="text-align:center;">
      			<button type='submit' name='submit' value='Submit' class="button-primary"><?php echo $langval807 ?></button>		
 			</div>
    	</div>
	  	</div>
  	</div>
</form>	

<?php 
echo "<br/><br/>";

if($resourceid <> ''){
	//get resource details
	$getresource = mysql_query("select * from resource 
	where resourceid = $resourceid");
	while($getresourcerow = mysql_fetch_array($getresource)){
		$resourcename = $getresourcerow['resourcename'];
		$userid = $getresourcerow['userid'];
	}
	
	if($userid <> 0){
		
		//calculate project % complete values for all projects
		$getprojects = "Select project.projectid from project 
		inner join projectteammember on project.projectid = projectteammember.projectid 
		where projectteammember.userid = '".$userid."' and projectteammember.disabled = 0 and 
		project.disabled = 0 group by project.projectid";
		//echo $getprojects;
		$getprojects = mysql_query($getprojects);
		while($row = mysql_fetch_array($getprojects)){
			$value = $row['projectid'];
			$gettasktotal = mysql_query("select sum(minstodate) as 'timesofar', sum(minsestimated) as 'estimated' from $database.task 
			where oneprojectid = '$value' group by oneprojectid");
			$gettasktotalrow = mysql_fetch_array($gettasktotal);
			$timesofar = $gettasktotalrow['timesofar'];
			$estimated = $gettasktotalrow['estimated'];
			//echo "<br/>timesofar: ".$timesofar;
			//echo "<br/>estimated: ".$estimated;
			if($timesofar <> 0 && $estimated <> 0){
				$perccomplete = round(($timesofar / $estimated) * 100, 0);
				if($perccomplete > 100){
					$perccomplete = 100;				
				}
			}
			else {
				$perccomplete = 0;			
			}
			//echo "<br/>perccomplete: ".$perccomplete;
			if($perccomplete > 0){
				$updateproject = mysql_query("update $database.project set progresspercentage = '$perccomplete' 
				where projectid = '$value'");		
			}
		}
		
		
		//DISPLAY ALLOCATIONS
		$getallocation = "select resourceallocationid, resourceallocationname, projectname, taskname, startdatetime, enddatetime, 
		resourceallocation.progresspercent, resourceallocationstatusname from resourceallocation 
		inner join resourceallocationstatus on resourceallocationstatus.resourceallocationstatusid = resourceallocation.resourceallocationstatusid
		left join project on project.projectid = resourceallocation.projectid
		left join task on task.taskid = resourceallocation.taskid
		where resourceid = $resourceid";
		$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
		if($startdate <> '' || $enddate <> '') {
			//add dates to query
			if($startdate <> "" && $enddate <> ""){
				$getallocation = $getallocation." and enddatetime <= '".$enddate."' and startdatetime >= '".$startdate."'";
			}	
			if($startdate <> "" && $enddate == ""){
				$getallocation = $getallocation." and startdatetime >= '".$startdate."'";
			}	
			if($startdate == "" && $enddate <> ""){
				$getallocation = $getallocation." and enddatetime <= '".$enddate."'";
			}	
		}
		$getallocation = $getallocation." order by startdatetime asc limit 500";
		//echo $getallocation;
		$getallocation = mysql_query($getallocation);
		
		
		echo "<h2>$langval808</h2>";
		echo "<a class='button-primary' href='pageadd.php?pagetype=resourceallocation&pagename=".$pagename."'>$langval809</a>&nbsp;&nbsp;&nbsp;&nbsp;";
		echo "<b>".$langval587."</b><a href='view.php?viewid=50&resid=".$resourceid."&start=".$startdate."&end=".$enddate."&displaytype=calendar'>$langval810</a>";
		echo " | <a href='view.php?viewid=50&resid=".$resourceid."&start=".$startdate."&end=".$enddate."&displaytype=tasklist'>".$langval589."</a>";
		echo "<br/><br/>";
				
		if($displaytype == "tasklist" || $displaytype == ""){
			if(mysql_num_rows($getallocation) >= 1){		
				echo "<table class='table table-bordered'>";
				echo "<thead><tr>";
				echo "<th>$langval811</th>";
				echo "<th>$langval812</th>";
				echo "<th>$langval813</th>";
				echo "<th>$langval814</th>";
				echo "<th>$langval815</th>";
				echo "<th>$langval816</th>";
				echo "<th>$langval817</th>";
				echo "<th>$langval818</th>";
				echo "<th>$langval819</th>";
				echo "</tr></thead>";
			}
			while($row23 = mysql_fetch_array($getallocation)){
				$resourceallocationid = $row23['resourceallocationid'];
				$resourceallocationname = $row23['resourceallocationname'];
				$projectname = $row23['projectname'];
				$taskname = $row23['taskname'];
				$progresspercent = $row23['progresspercent'];
				$status = $row23['resourceallocationstatusname'];
				$startdate1 = $row23['startdatetime'];
				$enddate1 = $row23['enddatetime'];
				echo "<tr>";	
				echo "<td>".$resourceallocationid."</td>";
				echo "<td>".$resourceallocationname."</td>";
				echo "<td>".$projectname."</td>";
				echo "<td>".$taskname."</td>";
				echo "<td>".$progresspercent."</td>";
				echo "<td>".$status."</td>";
				echo "<td>".$startdate1."</td>";
				echo "<td>".$enddate1."</td>";
				echo "<td><a href='pageedit.php?pagetype=resourceallocation&rowid=".$resourceallocationid."&pagename=".$pagename."'>".$langval599."</a></td>";			
				echo "</tr>";	
			}
			if(mysql_num_rows($getallocation) >= 1){
				echo "</table>";
			}
		}
		
		if($displaytype == "calendar"){
			//include('fullcalendar-3.2.0/demos/default.html');
			?>
			<script type="text/javascript" >
			
				$(document).ready(function() {
			
					$('#calendar2').fullCalendar({
						defaultDate: '<?php echo $date ?>',
						editable: true,
						eventLimit: true, // allow "more" link when too many events
						events: [ 
						<?php
						$events = "";
						$title = '';
						$start = '';
						$end = '';
						$url = '';
						$colour = '';
						while ($row5 = mysql_fetch_assoc($getallocation)){
							$title1 = $row5['resourceallocationname'];
							$start1 = $row5['startdatetime'];
							$end1 = $row5['enddatetime'];
							$colour1 = 'blue';
							$url1 = "pageedit.php?pagetype=resourceallocation&rowid=".$row5['resourceallocationid'];
							$tip1 = $row5['resourceallocationname'].' - '.$row5['resourceallocationstatusname'];
							$events = $events."{";
							$events = $events."title: '".$title1."',";
							$events = $events."start: '".$start1."',";
							$events = $events."end: '".$end1."',";
							$events = $events."tip: '".$tip1."',";
							$events = $events."color: '".$colour1."',";
							$events = $events."url: '".$url1."'";
							$events = $events."},";
						}
						$events = substr(trim($events), 0, -1);
					 	echo $events;
						?>
							
						],
						eventRender: function(event, element) {
				            element.attr('title', event.tip);
				        },
						eventClick: function(event) {
			        	if (event.url) {
			            	window.open(event.url);
			            	return false;
			        	}
			    	}
			    	});
					
				});
			
			</script>
			<div id='calendar2' style="width:100%;height:auto;overflow:hidden;z-index:-1"></div>
			<link href='fullcalendar-3.2.0/fullcalendar.min.css' rel='stylesheet' />
			<link href='fullcalendar-3.2.0/fullcalendar.print.min.css' rel='stylesheet' media='print' />
			<script src='fullcalendar-3.2.0/lib/jquery.min.js'></script>
			<script src='fullcalendar-3.2.0/lib/moment.min.js'></script>
			<script src='fullcalendar-3.2.0/fullcalendar.min.js'></script>
			<br/><br/>
			<?php
		}		
		
		
		//GET PROJECTS
		$getprojects = mysql_query("select distinct project.projectid, startdate, enddate from project
		inner join projectteammember on projectteammember.projectid = project.projectid
		where userid = $userid");
		$projectsidlist = "";
		while($getprojectrow = mysql_fetch_array($getprojects)){
			$projectid = $getprojectrow['projectid'];
			$projectsidlist = $projectsidlist.$projectid.",";	
		}
		$projectsidlist = rtrim($projectsidlist, ",");
		//echo '<br/>projectsidlist:'.$projectsidlist;
		
		
		//DISPLAY PROJECTS
		$getproject = "select distinct project.projectid, projectname, startdate, enddate, description, progresspercentage, customername from project
		inner join projectteammember on projectteammember.projectid = project.projectid
		left join customer on customer.customerid = project.onecustomerid
		where userid = $userid";
		$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
		if($startdate <> '' || $enddate <> '') {
			//add dates to query
			if($startdate <> "" && $enddate <> ""){
				$getproject = $getproject." and enddate <= '".$enddate."' and startdate >= '".$startdate."'";
			}	
			if($startdate <> "" && $enddate == ""){
				$getproject = $getproject." and startdate >= '".$startdate."'";
			}	
			if($startdate == "" && $enddate <> ""){
				$getproject = $getproject." and enddate <= '".$enddate."'";
			}	
		}
		$getproject = $getproject." order by startdate asc limit 500";
		//echo $getproject;
		$getproject = mysql_query($getproject);
		
		
		echo "<h2>Current Project List</h2>";
		echo "<a class='button-primary' href='pageadd.php?pagetype=project&pagename=".$pagename."'>$langval820</a>&nbsp;&nbsp;&nbsp;&nbsp;";
		echo "<b>".$langval587."</b><a href='view.php?viewid=50&resid=".$resourceid."&start=".$startdate."&end=".$enddate."&displaytype=calendar'>$langval821</a>";
		echo " | <a href='view.php?viewid=50&resid=".$resourceid."&start=".$startdate."&end=".$enddate."&displaytype=tasklist'>".$langval589."</a>";
		echo "<br/><br/>";
				
		if($displaytype == "tasklist" || $displaytype == ""){
			if(mysql_num_rows($getproject) >= 1){		
				echo "<table class='table table-bordered'>";
				echo "<thead><tr>";
				echo "<th>$langval822</th>";
				echo "<th>$langval823</th>";
				echo "<th>$langval824</th>";
				echo "<th>$langval825</th>";
				echo "<th>$langval826</th>";
				echo "<th>$langval827</th>";
				echo "<th>$langval828</th>";
				echo "</tr></thead>";
			}
			while($row23 = mysql_fetch_array($getproject)){
				$projectid = $row23['projectid'];
				$projectname = $row23['projectname'];
				$description = $row23['description'];
				$progresspercentage = $row23['progresspercentage'];
				$startdate1 = $row23['startdate'];
				$enddate1 = $row23['enddate'];
				if(strlen($description) >= 200){
					$description = substr($description, 0, 200)." . . .";
				}
				echo "<tr>";	
				echo "<td>".$projectid."</td>";
				echo "<td>".$projectname."</td>";
				echo "<td>".$description."</td>";
				echo "<td>".$progresspercentage."</td>";
				echo "<td>".$startdate1."</td>";
				echo "<td>".$enddate1."</td>";
				echo "<td><a href='pageedit.php?pagetype=project&rowid=".$projectid."&pagename=".$pagename."'>".$langval599."</a></td>";			
				echo "</tr>";	
			}
			if(mysql_num_rows($getproject) >= 1){
				echo "</table>";
			}
		}
		
		if($displaytype == "calendar"){
			//include('fullcalendar-3.2.0/demos/default.html');
			?>
			<script type="text/javascript" >
			
				$(document).ready(function() {
			
					$('#calendar1').fullCalendar({
						defaultDate: '<?php echo $date ?>',
						editable: true,
						eventLimit: true, // allow "more" link when too many events
						events: [ 
						<?php
						$events = "";
						$title = '';
						$start = '';
						$end = '';
						$url = '';
						$colour = '';
						while ($row5 = mysql_fetch_assoc($getproject)){
							$title1 = $row5['projectname'];
							$start1 = $row5['startdate'];
							$end1 = $row5['enddate'];
							$colour1 = 'blue';
							$url1 = "pageedit.php?pagetype=project&rowid=".$row5['projectid'];
							$tip1 = $row5['projectname'].' - '.$row5['progresspercentage'].'% Complete';
							$events = $events."{";
							$events = $events."title: '".$title1."',";
							$events = $events."start: '".$start1."',";
							$events = $events."end: '".$end1."',";
							$events = $events."tip: '".$tip1."',";
							$events = $events."color: '".$colour1."',";
							$events = $events."url: '".$url1."'";
							$events = $events."},";
						}
						$events = substr(trim($events), 0, -1);
					 	echo $events;
						?>
							
						],
						eventRender: function(event, element) {
				            element.attr('title', event.tip);
				        },
						eventClick: function(event) {
			        	if (event.url) {
			            	window.open(event.url);
			            	return false;
			        	}
			    	}
			    	});
					
				});
			
			</script>
			<div id='calendar1' style="width:100%;height:auto;overflow:hidden;z-index:-1"></div>
			<link href='fullcalendar-3.2.0/fullcalendar.min.css' rel='stylesheet' />
			<link href='fullcalendar-3.2.0/fullcalendar.print.min.css' rel='stylesheet' media='print' />
			<script src='fullcalendar-3.2.0/lib/jquery.min.js'></script>
			<script src='fullcalendar-3.2.0/lib/moment.min.js'></script>
			<script src='fullcalendar-3.2.0/fullcalendar.min.js'></script>
			<br/><br/>
			<?php
		}
		
		
		//DISPLAY TASKS
		$gettask = "select taskid, projectname, taskname, task.description, minsestimated, minstodate, 
		datetargetcompletion, taskstatusname, task.taskstatusid from $database.task 
		inner join $masterdatabase.taskstatus on taskstatus.taskstatusid = task.taskstatusid
		inner join $database.project on project.projectid = task.oneprojectid
		where assignedto = '$userid' and task.taskstatusid in (1,2,3) and task.oneprojectid in (".$projectsidlist.") ";
		$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
		if($startdate <> '' || $enddate <> '') {
			//add dates to query
			if($startdate <> "" && $enddate <> ""){
				$gettask = $gettask." and datetargetcompletion <= '".$enddate."' and datetargetcompletion >= '".$startdate."'";
			}	
			if($startdate <> "" && $enddate == ""){
				$gettask = $gettask." and datetargetcompletion >= '".$startdate."'";
			}	
			if($startdate == "" && $enddate <> ""){
				$gettask = $gettask." and datetargetcompletion <= '".$enddate."'";
			}	
		}
		$gettask = $gettask." order by datetargetcompletion asc limit 500";
		//echo $gettask;
		$gettask = mysql_query($gettask);
		
		
		echo "<h2>".$langval585."</h2>";
		echo "<a class='button-primary' href='pageadd.php?pagetype=task&pagename=".$pagename."'>".$langval586."</a>&nbsp;&nbsp;&nbsp;&nbsp;";
		echo "<b>".$langval587."</b><a href='view.php?viewid=50&resid=".$resourceid."&start=".$startdate."&end=".$enddate."&displaytype=calendar'>$langval821</a>";
		echo " | <a href='view.php?viewid=50&resid=".$resourceid."&start=".$startdate."&end=".$enddate."&displaytype=tasklist'>".$langval589."</a>";
		echo "<br/><br/>";
				
		if($displaytype == "tasklist" || $displaytype == ""){
			if(mysql_num_rows($gettask) >= 1){		
				echo "<table class='table table-bordered'>";
				echo "<thead><tr>";
				echo "<th>".$langval590."</th>";
				echo "<th>".$langval591."</th>";
				echo "<th>".$langval592."</th>";
				echo "<th>".$langval593."</th>";
				echo "<th>".$langval594."</th>";
				echo "<th>".$langval595."</th>";
				echo "<th>".$langval596."</th>";
				echo "<th>".$langval597."</th>";
				echo "<th>".$langval599."</th>";
				echo "</tr></thead>";
			}
			while($row22 = mysql_fetch_array($gettask)){
				$taskid = $row22['taskid'];
				$projectname = $row22['projectname'];
				$taskname = $row22['taskname'];
				$description = $row22['description'];
				$minsestimated = $row22['minsestimated'];
				$minstodate = $row22['minstodate'];
				$datetargetcompletion = $row22['datetargetcompletion'];
				$taskstatusname = $row22['taskstatusname'];
				if(strlen($description) >= 200){
					$description = substr($description, 0, 200)." . . .";
				}
				echo "<tr>";	
				echo "<td>".$taskid."</td>";
				echo "<td>".$projectname."</td>";
				echo "<td>".$taskname."</td>";
				echo "<td>".$description."</td>";
				echo "<td>".$minsestimated."</td>";
				echo "<td>".$minstodate."</td>";
				echo "<td>".$datetargetcompletion."</td>";
				echo "<td>".$taskstatusname."</td>";
				echo "<td><a href='pageedit.php?pagetype=task&rowid=".$taskid."&pagename=".$pagename."'>".$langval599."</a></td>";			
				echo "</tr>";	
			}
			if(mysql_num_rows($gettask) >= 1){
				echo "</table>";
			}
		}
		
		if($displaytype == "calendar"){
			//include('fullcalendar-3.2.0/demos/default.html');
			?>
			<script type="text/javascript" >
			
				$(document).ready(function() {
			
					$('#calendar').fullCalendar({
						defaultDate: '<?php echo $date ?>',
						editable: true,
						eventLimit: true, // allow "more" link when too many events
						events: [ 
						<?php
						$events = "";
						$title = '';
						$start = '';
						$end = '';
						$url = '';
						$colour = '';
						while ($row5 = mysql_fetch_assoc($gettask)){
							$title1 = $row5['taskname'];
							$start1 = $row5['datetargetcompletion'];
							$end1 = $row5['datetargetcompletion'];
							$colour1 = 'blue';
							$url1 = "pageedit.php?pagetype=task&rowid=".$row5['taskid'];
							$tip1 = $row5['projectname'].' - '.$row5['taskname'].' - '.$row5['taskstatusname'];
							$events = $events."{";
							$events = $events."title: '".$title1."',";
							$events = $events."start: '".$start1."',";
							$events = $events."end: '".$end1."',";
							$events = $events."tip: '".$tip1."',";
							$events = $events."color: '".$colour1."',";
							$events = $events."url: '".$url1."'";
							$events = $events."},";
						}
						$events = substr(trim($events), 0, -1);
					 	echo $events;
						?>
							
						],
						eventRender: function(event, element) {
				            element.attr('title', event.tip);
				        },
						eventClick: function(event) {
			        	if (event.url) {
			            	window.open(event.url);
			            	return false;
			        	}
			    	}
			    	});
					
				});
			
			</script>
			<div id='calendar' style="width:100%;height:auto;overflow:hidden;z-index:-1"></div>
			<link href='fullcalendar-3.2.0/fullcalendar.min.css' rel='stylesheet' />
			<link href='fullcalendar-3.2.0/fullcalendar.print.min.css' rel='stylesheet' media='print' />
			<script src='fullcalendar-3.2.0/lib/jquery.min.js'></script>
			<script src='fullcalendar-3.2.0/lib/moment.min.js'></script>
			<script src='fullcalendar-3.2.0/fullcalendar.min.js'></script>
			<br/><br/>
			<?php
		}
		
		
	}
}

?>