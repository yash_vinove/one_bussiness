<?php session_start(); ?>
<?php include('config.php'); ?>
<?php include('sessionconfig.php'); ?>
<?php
$action = isset($_GET['action']) ? $_GET['action'] : ''; 
$rowid = isset($_GET['rowid']) ? $_GET['rowid'] : ''; 
$pagetype = isset($_GET['pagetype']) ? $_GET['pagetype'] : ''; 
$pagename = isset($_GET['pagename']) ? $_GET['pagename'] : '';
$date = date("Y-m-d");

$mysqli = new mysqli($server, $user_name, $password, $database);$pagetype = mysqli_real_escape_string($mysqli, $pagetype);if($stmt = $mysqli->prepare("select * from structurefunction where tablename = ?")){   $stmt->bind_param('s', $pagetype);   $stmt->execute();   $result = $stmt->get_result();   if($result->num_rows > 0){    	while($pagedetails = $result->fetch_assoc()){     		$structurefunctionid = $pagedetails['structurefunctionid'];
     		$primarykeyfield = $pagedetails['primarykeyfield'];
			$enablescript = $pagedetails['runscriptenable'];
			$disablescript = $pagedetails['runscriptdisable'];    	}   	}}

$createddate = date("Y-m-d");;
//echo "membershipmanagerentitymembershiptypeid: ".$membershipmanagerentitymembershiptypeid."<br/>";
	
//activate
if($action == "activate") {
	
	$mysqli = new mysqli($server, $user_name, $password, $database);	$pagetype = mysqli_real_escape_string($mysqli, $pagetype);
	$database = mysqli_real_escape_string($mysqli, $database);
	$primarykeyfield = mysqli_real_escape_string($mysqli, $primarykeyfield);	$rowid = mysqli_real_escape_string($mysqli, $rowid);	if($stmt = $mysqli->prepare("update ".$database.".".$pagetype." set disabled = 0 where $primarykeyfield = ?")){	   $stmt->bind_param('i', $rowid);	   $stmt->execute();	   	}
	
	$mysqli = new mysqli($server, $user_name, $password, $database);
	if($stmt = $mysqli->prepare("insert into trackchanges (trackchangesname, structurefunctionid, rowid, 
	userid, type, subtype, disabled, datecreated, masteronly) 
	values ('Enable Record', ?, ?, ?, 'Enable', 'Enable', 0, ?, 0)")){
		$stmt->bind_param('iiis', $structurefunctionid, $rowid, $userid, $date);
		$stmt->execute();
	}
	
	$recordid = $rowid;
	if($enablescript <> ""){
   		include_once('./'.$enablescript);
   }	
	//redirect back to the page
	if($pagename == ''){
 		$url = 'pagegrid.php?pagetype='.$pagetype.'&updated=1';
		echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">';   
 	}
 	else{
 		$pagename = str_replace("xxxxxxxxxx", "&", $pagename);	    					
 		$url = $pagename;
 		echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">'; 
 	}
					
}
	
	
//deactivate
if($action == "deactivate") {
	
	$mysqli = new mysqli($server, $user_name, $password, $database);	$pagetype = mysqli_real_escape_string($mysqli, $pagetype);
	$database = mysqli_real_escape_string($mysqli, $database);
	$primarykeyfield = mysqli_real_escape_string($mysqli, $primarykeyfield);	$rowid = mysqli_real_escape_string($mysqli, $rowid);	if($stmt = $mysqli->prepare("update $pagetype set disabled = 1 where $primarykeyfield = ?")){	   $stmt->bind_param('i', $rowid);	   $stmt->execute();	   	}	
	$mysqli = new mysqli($server, $user_name, $password, $database);
	if($stmt = $mysqli->prepare("insert into trackchanges (trackchangesname, structurefunctionid, rowid, 
	userid, type, subtype, disabled, datecreated, masteronly) 
	values ('Disable Record', ?, ?, ?, 'Disable', 'Disable', 0, ?, 0)")){
		$stmt->bind_param('iiis', $structurefunctionid, $rowid, $userid, $date);
		$stmt->execute();
	}
	
	$recordid = $rowid;
	if($disablescript <> ""){
   		include_once('./'.$disablescript);
   }	
	//redirect back to the page
	if($pagename == ''){
		$url = 'pagegrid.php?pagetype='.$pagetype.'&updated=1';
		echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">';  
 	}
 	else{
 		$pagename = str_replace("xxxxxxxxxx", "&", $pagename);	    					
 		$url = $pagename;
 		echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">'; 
 	}
	 				
}
	