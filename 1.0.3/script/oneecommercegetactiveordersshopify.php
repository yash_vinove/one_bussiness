<?php

echo "<br/><br/><b>PROCESS SHOPIFY ORDERS</b><br/><br/>";

$shopdomain = $apikey1;
$apikey = $apikey2;
$secret = $apikey3;

$url = "https://".$apikey.":".$secret."@".$shopdomain."/admin/orders.json";
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
//comment out returntransfer line below to see the json response for debugging purposes
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
$result = curl_exec($ch);
curl_close($ch);

//update the status in ecommercelistinglocation
echo $result;
$result = json_decode($result, true); 

for($i=0; $i<count($result['orders']); $i++){
	$orderid = $result['orders'][$i]['id'];
	$createdat = $result['orders'][$i]['created_at'];
	$updatedat = $result['orders'][$i]['updated_at'];
	//$createdat = substr($createdat, 0, strpos($createdat, "T"));
	//$updatedat = substr($updatedat, 0, strpos($updatedat, "T"));
	
	echo "<br/><br/><b>ORDER ID: ".$orderid."</b><br/>";
	echo "createdat: ".$createdat."<br/>";
	echo "updatedat: ".$updatedat."<br/>";
	$date1 = date('Y-m-d H:i:s', strtotime("-2 hours"));
	echo "date1: ".$date1."<br/>";
	if($createdat >= $date1){
		echo "<br/>yes";	
	}
	else {
		echo "<br/>no";	
	}
	$die = 0;
	if($createdat >= $date1 && $die == 0){
		//customer details	
		$customeremail = $result['orders'][$i]['customer']['email'];
		$customerfirstname = $result['orders'][$i]['customer']['first_name'];
		$customerlastname = $result['orders'][$i]['customer']['last_name'];
		$customerphonebilling = $result['orders'][$i]['billing_address']['phone'];
		$customerphoneshipping = $result['orders'][$i]['shipping_address']['phone'];
		$customerphoneoverview = $result['orders'][$i]['customer']['phone'];
		if($customerphoneoverview == '' && $customerphonebilling == '' && $customerphoneshipping == ''){
			$customerphone = '';	
		} 
		else{
			if($customerphoneoverview <> ''){
				$customerphone = $customerphoneoverview;
			}
			else {
				if($customerphonebilling <> ''){
					$customerphone = $customerphonebilling;
				}
				else {
					$customername = $customerphoneshipping;
				}	
			}	
		}
		$customercompanynameshipping = $result['orders'][$i]['shipping_address']['company'];
		$customercompanynamebilling = $result['orders'][$i]['billing_address']['company'];
		if($customercompanynameshipping == '' && $customercompanynamebilling == ''){
			$customername = $customerfirstname." ".$customerlastname;	
		} 
		else{
			if($customercompanynameshipping <> ''){
				$customername = $customercompanynameshipping;
			}
			else {
				$customername = $customercompanynamebilling;
			}	
		}
		$customermarketingoptin = $result['orders'][$i]['buyer_accepts_marketing'];
		if($customermarketingoptin == 1){
			$customermarketingoptin	= 0;
		}
		else{
			$customermarketingoptin	= 1;
		}
		
		
		echo "customeremail: ".$customeremail."<br/>";
		echo "customername: ".$customername."<br/>";
		echo "customerfirstname: ".$customerfirstname."<br/>";
		echo "customerlastname: ".$customerlastname."<br/>";
		echo "customerphone: ".$customerphone."<br/>";
		echo "customermarketingoptin: ".$customermarketingoptin."<br/><br/>";
		
		//customer shipping details
		$customershippingaddress1 = $result['orders'][$i]['shipping_address']['address1'];
		$customershippingaddress2 = $result['orders'][$i]['shipping_address']['address2'];
		$customershippingcity = $result['orders'][$i]['shipping_address']['city'];
		$customershippingstate = $result['orders'][$i]['shipping_address']['province'];
		$customershippingzip = $result['orders'][$i]['shipping_address']['zip'];
		$customershippingcountry = $result['orders'][$i]['shipping_address']['country'];
		$customershippingcountrycode = $result['orders'][$i]['shipping_address']['country_code'];
		$customershippinglatitude = $result['orders'][$i]['shipping_address']['latitude'];
		$customershippinglongitude = $result['orders'][$i]['shipping_address']['longitude'];
		
		$customershippingaddress1 = str_replace("'", "", $customershippingaddress1);		
		$customershippingaddress2 = str_replace("'", "", $customershippingaddress2);		
		$customershippingcity = str_replace("'", "", $customershippingcity);		
		$customershippingstate = str_replace("'", "", $customershippingstate);		
		$customershippingzip = str_replace("'", "", $customershippingzip);
		$customershippingaddress1 = str_replace('"', "", $customershippingaddress1);		
		$customershippingaddress2 = str_replace('"', "", $customershippingaddress2);		
		$customershippingcity = str_replace('"', "", $customershippingcity);		
		$customershippingstate = str_replace('"', "", $customershippingstate);		
		$customershippingzip = str_replace('"', "", $customershippingzip);		
		
		if($customershippinglatitude == ''){
			//get latitude and longitude from google maps api
			$address = $customershippingaddress1.", ".$customershippingaddress2.", ".$customershippingcity.", ".$customershippingstate.", ".$customershippingzip.", ".$customershippingcountry;
			$prepAddr = str_replace(' ','+',$address);
			$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
			//echo $geocode;
			$output= json_decode($geocode);
			if(isset($output->results[0]->geometry->location->lat)){
				$customershippinglatitude = $output->results[0]->geometry->location->lat;
				$customershippinglongitude = $output->results[0]->geometry->location->lng;						
			}
		}
		echo "customershippingaddress1: ".$customershippingaddress1."<br/>";
		echo "customershippingaddress2: ".$customershippingaddress2."<br/>";
		echo "customershippingcity: ".$customershippingcity."<br/>";
		echo "customershippingstate: ".$customershippingstate."<br/>";
		echo "customershippingzip: ".$customershippingzip."<br/>";
		echo "customershippingcountry: ".$customershippingcountry."<br/>";
		echo "customershippingcountrycode: ".$customershippingcountrycode."<br/>";
		echo "customershippinglatitude: ".$customershippinglatitude."<br/>";
		echo "customershippinglongitude: ".$customershippinglongitude."<br/><br/>";
		
		//customer billing details
		$customerbillingaddress1 = $result['orders'][$i]['billing_address']['address1'];
		$customerbillingaddress2 = $result['orders'][$i]['billing_address']['address2'];
		$customerbillingcity = $result['orders'][$i]['billing_address']['city'];
		$customerbillingstate = $result['orders'][$i]['billing_address']['province'];
		$customerbillingzip = $result['orders'][$i]['billing_address']['zip'];
		$customerbillingcountry = $result['orders'][$i]['billing_address']['country'];
		$customerbillingcountrycode = $result['orders'][$i]['billing_address']['country_code'];
		$customerbillinglatitude = $result['orders'][$i]['billing_address']['latitude'];
		$customerbillinglongitude = $result['orders'][$i]['billing_address']['longitude'];
		
		$customerbillingaddress1 = str_replace("'", "", $customerbillingaddress1);		
		$customerbillingaddress2 = str_replace("'", "", $customerbillingaddress2);		
		$customerbillingcity = str_replace("'", "", $customerbillingcity);		
		$customerbillingstate = str_replace("'", "", $customerbillingstate);		
		$customerbillingzip = str_replace("'", "", $customerbillingzip);		
		$customerbillingaddress1 = str_replace('"', "", $customerbillingaddress1);		
		$customerbillingaddress2 = str_replace('"', "", $customerbillingaddress2);		
		$customerbillingcity = str_replace('"', "", $customerbillingcity);		
		$customerbillingstate = str_replace('"', "", $customerbillingstate);		
		$customerbillingzip = str_replace('"', "", $customerbillingzip);		
				
		if($customerbillinglatitude == ''){
			//get latitude and longitude from google maps api
			$address = $customerbillingaddress1.", ".$customerbillingaddress2.", ".$customerbillingcity.", ".$customerbillingstate.", ".$customerbillingzip.", ".$customerbillingcountry;
			$prepAddr = str_replace(' ','+',$address);
			$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
			//echo $geocode;
			$output= json_decode($geocode);
			if(isset($output->results[0]->geometry->location->lat)){
				$customerbillinglatitude = $output->results[0]->geometry->location->lat;
				$customerbillinglongitude = $output->results[0]->geometry->location->lng;	
			}					
		}
		echo "customerbillingaddress1: ".$customerbillingaddress1."<br/>";
		echo "customerbillingaddress2: ".$customerbillingaddress2."<br/>";
		echo "customerbillingcity: ".$customerbillingcity."<br/>";
		echo "customerbillingstate: ".$customerbillingstate."<br/>";
		echo "customerbillingzip: ".$customerbillingzip."<br/>";
		echo "customerbillingcountry: ".$customerbillingcountry."<br/>";
		echo "customerbillingcountrycode: ".$customerbillingcountrycode."<br/>";
		echo "customerbillinglatitude: ".$customerbillinglatitude."<br/>";
		echo "customerbillinglongitude: ".$customerbillinglongitude."<br/><br/>";
		
		//get countryid 
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$getcountry = mysql_query("select * from $database.country where countryshortcode = '$customerbillingcountrycode'");	
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		//echo "<br/>Get list of news sources: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		if(mysql_num_rows($getcountry)>=1){
			//get countryid of existing record
			$getcountryrow = mysql_fetch_array($getcountry);
			$countryid = $getcountryrow['countryid'];
		}
		else {
			//add a new country to the system
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$addcountry = mysql_query("insert into $database.country set countryname = '$customerbillingcountry', 
			countryshortcode = '$customershippingcountrycode', 
			dateformat = 'd-m-y', numberformatdecimal = '2', numberformatdecimalpoint = ',', numberformatseparator = '.', 
			disabled = '0', datecreated = '$date'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			//echo "<br/>Get list of news sources: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			$countryid = mysql_insert_id();
		}
		echo "countryid: ".$countryid."<br/>";
		
		//get currencycode
		$currencycode = $result['orders'][$i]['currency'];
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$getcurrency = mysql_query("select * from $database.currency where currencycode = '$currencycode'");	
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		//echo "<br/>Get list of news sources: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		if(mysql_num_rows($getcurrency)>=1){
			//get countryid of existing record
			$getcurrencyrow = mysql_fetch_array($getcurrency);
			$currencyid = $getcurrencyrow['currencyid'];
		}
		else {
			//add a new country to the system
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$addcurrency = mysql_query("insert into $database.currency set currencyname = '$currencycode', 
			currencycode = '$currencycode', 
			currencysymbol = '', disabled = '0', datecreated = '$date'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			//echo "<br/>Get list of news sources: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			$currencyid = mysql_insert_id();
		}
		echo "currencyid: ".$currencyid."<br/>";	
		
		//get countryidshipping
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$getcountry = mysql_query("select * from $database.country where countryshortcode = '$customershippingcountrycode'");	
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		//echo "<br/>Get list of news sources: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		if(mysql_num_rows($getcountry)>=1){
			//get countryid of existing record
			$getcountryrow = mysql_fetch_array($getcountry);
			$countryidshipping = $getcountryrow['countryid'];
		}
		else {
			//add a new country to the system
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$addcountry = mysql_query("insert into $database.country set countryname = '$customershippingcountry', 
			countryshortcode = '$customershippingcountrycode', 
			dateformat = 'd-m-y', numberformatdecimal = '2', numberformatdecimalpoint = ',', numberformatseparator = '.', 
			disabled = '0', datecreated = '$date'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			//echo "<br/>Get list of news sources: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			$countryidshipping = mysql_insert_id();
		}
		echo "countryidshipping: ".$countryidshipping."<br/>";	
		
		//check if customer exists in database
		$salt = "h3f8s9en20vj3";
		$customeremailencrypt = openssl_encrypt($customeremail,"AES-128-ECB",$salt);
		$customernameencrypt = openssl_encrypt($customername,"AES-128-ECB",$salt);
		$customerfirstnameencrypt = openssl_encrypt($customerfirstname,"AES-128-ECB",$salt);
		$customerlastnameencrypt = openssl_encrypt($customerlastname,"AES-128-ECB",$salt);
		$customerbillingaddress1encrypt = openssl_encrypt($customerbillingaddress1,"AES-128-ECB",$salt);
		$customerbillingaddress2encrypt = openssl_encrypt($customerbillingaddress2,"AES-128-ECB",$salt);
		$customerbillingcityencrypt = openssl_encrypt($customerbillingcity,"AES-128-ECB",$salt);
		$customerbillingstateencrypt = openssl_encrypt($customerbillingstate,"AES-128-ECB",$salt);
		$customerbillingzipencrypt = openssl_encrypt($customerbillingzip,"AES-128-ECB",$salt);
		$customerphoneencrypt = openssl_encrypt($customerphone,"AES-128-ECB",$salt);
		echo "customeremailencrypted: ".$customeremailencrypt."<br/>";
				
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$getcustomer = mysql_query("select * from $database.customer where emailaddress = '$customeremailencrypt'");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		//echo "<br/>Get list of news sources: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		if(mysql_num_rows($getcustomer)>=1){
			//update existing customer
			$rowcust = mysql_fetch_array($getcustomer);
			$customerid = $rowcust['customerid'];	
			$updatecustomer = "update $database.customer set customername = '$customernameencrypt', firstname = '$customerfirstnameencrypt',
			lastname = '$customerlastnameencrypt', customertypeid = '$customertypeid', salesstageid = '$salesstageid', 
			address1 = '$customerbillingaddress1encrypt', address2 = '$customerbillingaddress2encrypt', address3 = '', towncity = '$customerbillingcityencrypt', 
			stateregion = '$customerbillingstateencrypt', postzipcode = '$customerbillingzipencrypt', 
			longitude = '$customerbillinglongitude', latitude = '$customerbillinglatitude', countryid = '$countryid', 
			phonenumber = '$customerphoneencrypt', emailunsubscribe = '$customermarketingoptin', disabled = '0'
			where customerid = '$customerid'";
			//echo $updatecustomer;
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updatecustomer = mysql_query($updatecustomer);
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			//echo "<br/>Get list of news sources: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		}
		else {
			//create the customer record
			$createcustomer = "insert into $database.customer (customername, firstname, lastname, customertypeid, salesstageid, address1, 
			address2, address3, towncity, stateregion, postzipcode, longitude, latitude, countryid, phonenumber, emailunsubscribe, disabled,
			datecreated, businessunitid, emailaddress) 
			values ('$customernameencrypt', '$customerfirstnameencrypt', '$customerlastnameencrypt', '$customertypeid', '$salesstageid', 
			'$customerbillingaddress1encrypt', '$customerbillingaddress2encrypt', '', '$customerbillingcityencrypt', '$customerbillingstateencrypt',
			'$customerbillingzipencrypt', '$customerbillinglongitude', '$customerbillinglatitude', '$countryid', '$customerphoneencrypt', 
			'$customermarketingoptin', '0', '$date', '$businessunitid', '$customeremailencrypt')";
			//echo $createcustomer;
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$createcustomer = mysql_query($createcustomer);
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			//echo "<br/>Get list of news sources: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			$customerid = mysql_insert_id();
		}
		echo "customerid: ".$customerid."<br/>";	
		
		//work out shipping/discount details
		$numitems = 0;
		for($p=0; $p<count($result['orders'][$i]['line_items']); $p++){
			$quantity = $result['orders'][$i]['line_items'][$p]['quantity'];	
			$numitems = $numitems + $quantity;
		}	
		if(isset($result['orders'][$i]['shipping_lines'][0]['title'])){
			$shippingoptionname = $result['orders'][$i]['shipping_lines'][0]['title'];
			$shippingprice = round(($result['orders'][$i]['shipping_lines'][0]['price'] / $numitems), 2);
			if(isset($result['orders'][$i]['shipping_lines'][0]['tax_lines'][0]['price'])){
				$shippingtax = round(($result['orders'][$i]['shipping_lines'][0]['tax_lines'][0]['price'] / $numitems), 2);			
			}
			else {
				$shippingtax = 0;			
			}
		}
		else {
			$shippingoptionname = '';
			$currencyid = 0;
			$shippingprice = 0;
			$shippingtax = 0;
		}
		if(isset($result['orders'][$i]['total_discounts'])){
			$totaldiscounts = $result['orders'][$i]['total_discounts'];
			echo "totaldiscounts: ".$totaldiscounts."<br/>";
			$discountperitem = $totaldiscounts / $numitems;
		}
		else {
			$discountperitem = 0;		
		}
		echo "discountperitem: ".$discountperitem."<br/>";
		echo "shippingoptionname: ".$shippingoptionname."<br/>";
		echo "shippingpricecurrency: ".$currencyid."<br/>";
		echo "shippingpriceperitem: ".$shippingprice."<br/>";
		echo "shippingtaxperitem: ".$shippingtax."<br/>";			
		
		//iterate through orders
		for($p=0; $p<count($result['orders'][$i]['line_items']); $p++){
			//echo "here";
			echo "<br/><br/>order id: ".$p."<br/>";
			$quantity = $result['orders'][$i]['line_items'][$p]['quantity'];
			$lineitemid = $result['orders'][$i]['line_items'][$p]['id'];
			$productid = $result['orders'][$i]['line_items'][$p]['product_id'];
			$variantid = $result['orders'][$i]['line_items'][$p]['variant_id'];
			$integrationproductid = $productid."//".$variantid;
			$sku = $result['orders'][$i]['line_items'][$p]['sku'];
			$title = $result['orders'][$i]['line_items'][$p]['title'];
			$price = round($result['orders'][$i]['line_items'][$p]['price'], 2);
			$financialstatus = $result['orders'][$i]['financial_status'];
			$fulfillmentstatus = $result['orders'][$i]['fulfillment_status'];
			if(isset($result['orders'][$i]['line_items'][$p]['tax_lines'][0]['price'])){
				$taxprice = round($result['orders'][$i]['line_items'][$p]['tax_lines'][0]['price'], 2);
				$taxpercentage = $result['orders'][$i]['line_items'][$p]['tax_lines'][0]['rate'] * 100;
			}
			else {
				$taxprice = 0;
				$taxpercentage = 0;		
			}
			
			$iterate = 1;
			while($quantity >=  $iterate){
				$integrationorderid = $orderid."//".$lineitemid."//".$iterate;
				echo "integrationproductid: ".$integrationproductid."<br/>";
				echo "sku: ".$sku."<br/>";
				echo "quantity: ".$quantity."<br/>";
				echo "price: ".$price."<br/>";
				$price = round($price - $discountperitem, 2);
				echo "priceminusdiscount: ".$price."<br/>";
				echo "taxprice: ".$taxprice."<br/>";
				echo "taxpercentage: ".$taxpercentage."<br/>";
				$priceincludingtax = round(($price + $taxprice), 2);
				echo "priceincludingtax: ".$priceincludingtax."<br/>";
				echo "financialstatus: ".$financialstatus."<br/>";
				echo "fulfillmentstatus: ".$fulfillmentstatus."<br/>";
				echo "integrationorderid: ".$integrationorderid."<br/>";
			
				//check if order already exists in database
				$checkorder = "select ecommercelistinglocationid, ecommercelistingid from $database.ecommercelistinglocation 
				where integrationorderid = '$integrationorderid' 
				limit 1";
				//echo $checkorder;
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$checkorder = mysql_query($checkorder);
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Get order from ecommercelistinglocation table: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
				if(mysql_num_rows($checkorder)>=1){
					$checkorderrow = mysql_fetch_array($checkorder);
					$ecommercelistinglocationid = $checkorderrow['ecommercelistinglocationid'];
					$ecommercelistingid = $checkorderrow['ecommercelistingid'];
					echo "<br/>ecommercelistinglocationid: ".$ecommercelistinglocationid;
					echo "<br/>ecommercelistingid: ".$ecommercelistingid;
					
					//this means that order id has already been associated to an ecommercelistinglocation record
					//check and update the status of the order
					$nosqlqueries = $nosqlqueries + 1;
					$sqlstarttime = microtime(true);
					$getrecord = "select * from ecommercelistinglocation 
					inner join ecommercelisting on ecommercelistinglocation.ecommercelistingid = ecommercelisting.ecommercelistingid
					where ecommercelistinglocation.ecommercelistinglocationid = $ecommercelistinglocationid";
					echo "<br/><br/>getrecord: ".$getrecord;
					$getrecord = mysql_query($getrecord);
					$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
					$differencemilliseconds = microtime(true) - $sqlstarttime;
					echo "<br/>Get the ecommercelistinglocation record: ";
					echo "<br/>differencemilliseconds: ".$differencemilliseconds;
					echo "<br/>sqlqueriestime: ".$sqlqueriestime;
					$getrecordrow = mysql_fetch_array($getrecord);
					$ecommercelistingid = $getrecordrow['ecommercelistingid'];
					if($financialstatus == 'pending'){
						$ecommercelistingstatusid = 4;				
					}
					else {
						$ecommercelistingstatusid = 3;				
					}
					$nosqlqueries = $nosqlqueries + 1;
					$sqlstarttime = microtime(true);
					$updatestatus = mysql_query("update $database.ecommercelisting 
					set ecommercelistingstatusid = '$ecommercelistingstatusid'
					where ecommercelistingid = '$ecommercelistingid' and ecommercelistingstatusid in (1,4,3)");
					$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
					$differencemilliseconds = microtime(true) - $sqlstarttime;
					//echo "<br/>Get list of news sources: ";
					echo "<br/>differencemilliseconds: ".$differencemilliseconds;
					echo "<br/>sqlqueriestime: ".$sqlqueriestime;
				}
				else {
					//add the order to the system
					//work out ecommercelistinglocation records
					$getlocationlisting = "select * from $database.ecommercelistinglocation 
					inner join ecommercelisting on ecommercelisting.ecommercelistingid = ecommercelistinglocation.ecommercelistingid
					inner join productstockitem on productstockitem.productstockitemid = ecommercelisting.productstockitemid
					where ecommercelistinglocation.businessunitid = '$businessunitid' 
					and integrationproductid = '$integrationproductid' and 
					ecommercesiteconfigid = '$ecommercesiteconfigid' and ecommercelistinglocationstatusid in (2,3,4,8)
					and integrationorderid = '' and showasnotstock = 0
					order by ecommercelistinglocationid asc";
					echo $getlocationlisting;
					$nosqlqueries = $nosqlqueries + 1;
					$sqlstarttime = microtime(true);
					$getlocationlisting = mysql_query($getlocationlisting);
					$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
					$differencemilliseconds = microtime(true) - $sqlstarttime;
					//echo "<br/>Get list of news sources: ";
					echo "<br/>differencemilliseconds: ".$differencemilliseconds;
					echo "<br/>sqlqueriestime: ".$sqlqueriestime;
					$countlocationlisting = mysql_num_rows($getlocationlisting);
					echo "number of potential records: ".$countlocationlisting."<br/>";		
					$outofstock = 0;
					$ecommercelistinglocationid = 0;
					if($countlocationlisting >= 1){
						//get the record id
						$getrecord = mysql_fetch_array($getlocationlisting);
						$ecommercelistinglocationid = $getrecord['ecommercelistinglocationid'];		
					}
					else {
						//item must already be sold - mark it as out of stock
						$outofstock = 1;

						// MB-168 Start
						$getanotherlocationlisting = "select * from $database.ecommercelistinglocation 
						inner join ecommercelisting on ecommercelisting.ecommercelistingid = ecommercelistinglocation.ecommercelistingid
						inner join productstockitem on productstockitem.productstockitemid = ecommercelisting.productstockitemid
						where integrationproductid = '$integrationproductid' and ecommercelistinglocationstatusid in (5,13,18)
						and integrationorderid = '$integrationorderid' and showasnotstock = 0";

						$getanotherlocationlisting = mysql_query($getanotherlocationlisting);
						if(mysql_num_rows($getanotherlocationlisting) > 0)
						{
							$outofstock = 0;
						}
						// MB-168 End
					}
					echo "outofstock: ".$outofstock."<br/>";		
					echo "ecommercelistinglocationid: ".$ecommercelistinglocationid."<br/><br/>";		
					
					//if out of stock then run this code
					if($outofstock == 1){
						//check record doesn't already exist
						$nosqlqueries = $nosqlqueries + 1;
						$sqlstarttime = microtime(true);
						$getrecord = mysql_query("select * from ecommerceoutofstock where integrationorderid = '$integrationorderid'");
						$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
						$differencemilliseconds = microtime(true) - $sqlstarttime;
						//echo "<br/>Get list of news sources: ";
						echo "<br/>differencemilliseconds: ".$differencemilliseconds;
						echo "<br/>sqlqueriestime: ".$sqlqueriestime;
						if(mysql_num_rows($getrecord)< 1){
							//calculate refundamount 
							$totalrefundamount = $priceincludingtax + $shippingprice + $shippingtax - $discountperitem;
							echo "totalrefundamount: ".$totalrefundamount."<br/>";
		
							//add record to ecommerceoutofstock table
							$nosqlqueries = $nosqlqueries + 1;
							$sqlstarttime = microtime(true);
							$insertoutofstock = mysql_query("insert into ecommerceoutofstock (ecommerceoutofstockname, disabled, datecreated,
							masteronly, customerid, productid, integrationorderid, ecommercesiteconfigid, requestdate, ecommerceoutofstockstatusid, 
							businessunitid, totalrefundamount, refundcurrencyid) 
							values ('$title', '0', '$date', '0', '$customerid', '$sku', '$integrationorderid', '$ecommercesiteconfigid', '$date',
							'1', '$businessunitid', '$totalrefundamount', '$currencyid')");		
							$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
							$differencemilliseconds = microtime(true) - $sqlstarttime;
							//echo "<br/>Get list of news sources: ";
							echo "<br/>differencemilliseconds: ".$differencemilliseconds;
							echo "<br/>sqlqueriestime: ".$sqlqueriestime;
						}			
					}
					
					//if found record then run this code
					if($ecommercelistinglocationid >= 1 && $outofstock == 0){
						//update this ecommercelistinglocation record as sold
						$nosqlqueries = $nosqlqueries + 1;
						$sqlstarttime = microtime(true);
						$update = mysql_query("update $database.ecommercelistinglocation set ecommercelistinglocationstatusid = '5',
						integrationorderid = '$integrationorderid', integrationstatusname = 'Sold'
						where ecommercelistinglocationid = '$ecommercelistinglocationid'");
						$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
						$differencemilliseconds = microtime(true) - $sqlstarttime;
						//echo "<br/>Get list of news sources: ";
						echo "<br/>differencemilliseconds: ".$differencemilliseconds;
						echo "<br/>sqlqueriestime: ".$sqlqueriestime;
						
						//update all other ecommercelistinglocation records as not sold
						$ecommercelistingid = $getrecord['ecommercelistingid'];	
						$nosqlqueries = $nosqlqueries + 1;
						$sqlstarttime = microtime(true);
						$update = mysql_query("update $database.ecommercelistinglocation set ecommercelistinglocationstatusid = '6'
						where ecommercelistinglocationid <> '$ecommercelistinglocationid' and ecommercelistingid = '$ecommercelistingid'");
						$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
						$differencemilliseconds = microtime(true) - $sqlstarttime;
						//echo "<br/>Get list of news sources: ";
						echo "<br/>differencemilliseconds: ".$differencemilliseconds;
						echo "<br/>sqlqueriestime: ".$sqlqueriestime;
						
						//update ecommercelisting record as sold
						$siteconf = $getrecord['ecommercesiteconfigid'];	
						if($financialstatus == 'pending'){
							$ecommercelistingstatusid = 4;				
						}
						else {
							$ecommercelistingstatusid = 3;				
						}
						$nosqlqueries = $nosqlqueries + 1;
						$sqlstarttime = microtime(true);
						$update = mysql_query("update $database.ecommercelisting 
						set soldecommercesiteconfigid = '$siteconf', selldate = '$date', 
						sellcustomerid = '$customerid', ecommercelistingstatusid = '$ecommercelistingstatusid', 
						billingaddress1 = '$customerbillingaddress1', billingaddress2 = '$customerbillingaddress2', 
						billingaddresstowncity = '$customerbillingcity', billingaddressstatecounty = '$customerbillingstate', 
						billingaddresspostzipcode = '$customerbillingzip', billingaddresscountryid = '$countryid', 
						fulfillmentaddress1 = '$customershippingaddress1', fulfillmentaddress2 = '$customershippingaddress2', 
						fulfillmentaddresstowncity = '$customershippingcity', fulfillmentaddressstatecounty = '$customershippingstate', 
						fulfillmentaddresspostzipcode = '$customershippingzip', fulfillmentaddresscountryid = '$countryidshipping', 
						shippingoptionname = '$shippingoptionname', shippingpricecurrencyid = '$currencyid', 						
						shippingprice = '$shippingprice', shippingtax = '$shippingtax'
						where ecommercelistingid = '$ecommercelistingid'");
						$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
						$differencemilliseconds = microtime(true) - $sqlstarttime;
						//echo "<br/>Get list of news sources: ";
						echo "<br/>differencemilliseconds: ".$differencemilliseconds;
						echo "<br/>sqlqueriestime: ".$sqlqueriestime;
						
						//update productstockitem record as sold 
						$nosqlqueries = $nosqlqueries + 1;
						$sqlstarttime = microtime(true);
						$getproductstock = mysql_query("select * from ecommercelisting where ecommercelistingid = '$ecommercelistingid'");
						$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
						$differencemilliseconds = microtime(true) - $sqlstarttime;
						//echo "<br/>Get list of news sources: ";
						echo "<br/>differencemilliseconds: ".$differencemilliseconds;
						echo "<br/>sqlqueriestime: ".$sqlqueriestime;
						$getproductstockrow = mysql_fetch_array($getproductstock);
						$productstockitemid = $getproductstockrow['productstockitemid'];
						$nosqlqueries = $nosqlqueries + 1;
						$sqlstarttime = microtime(true);
						$update = mysql_query("update $database.productstockitem
						set productstockitemstatusid = '2', sellprice = '$price', saleschannelid = '$saleschannelid', selldate = '$date', 
						customerid = '$customerid', sellcurrencyid = '$currencyid', selltaxrate = '$taxpercentage', 
						selltaxprice = '$taxprice', sellpriceincludingtax = '$priceincludingtax'
						where productstockitemid = '$productstockitemid'");	
						$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
						$differencemilliseconds = microtime(true) - $sqlstarttime;
						//echo "<br/>Get list of news sources: ";
						echo "<br/>differencemilliseconds: ".$differencemilliseconds;
						echo "<br/>sqlqueriestime: ".$sqlqueriestime;			
				
						//create customerpurchase table record
						$nosqlqueries = $nosqlqueries + 1;
						$sqlstarttime = microtime(true);
						$getproduct = mysql_query("select * from productstockitem where productstockitemid = '$productstockitemid'");
						$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
						$differencemilliseconds = microtime(true) - $sqlstarttime;
						//echo "<br/>Get list of news sources: ";
						echo "<br/>differencemilliseconds: ".$differencemilliseconds;
						echo "<br/>sqlqueriestime: ".$sqlqueriestime;
						$getproductrow = mysql_fetch_array($getproduct);
						$productid = $getproductrow['productid'];
						$currencyid = $getproductrow['currencyid'];
						$nosqlqueries = $nosqlqueries + 1;
						$sqlstarttime = microtime(true);
						$create = mysql_query("insert into customerpurchase (customerid, productid, currencyid, purchasevalue, 
						datepurchased, businessunitid, disabled, datecreated) values ('$customerid', '$productid', '$currencyid', '$price', 
						'$date', '$businessunitid', '0', '$date')");
						$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
						$differencemilliseconds = microtime(true) - $sqlstarttime;
						//echo "<br/>Get list of news sources: ";
						echo "<br/>differencemilliseconds: ".$differencemilliseconds;
						echo "<br/>sqlqueriestime: ".$sqlqueriestime;

						// new update
						$ordertableentry = mysql_query("INSERT INTO ecommerceorder (ecommerceordername, 
						datecreated, disabled, customerid, orderdatetime, ecommercesiteconfigid) VALUES 
						('customer order', '$date', '0', '$customerid', '$date', '$ecommercesiteconfigid')");
						$ordertableid = mysql_insert_id($ordertableentry);

						$orderitemtableentry = mysql_query("INSERT INTO ecommerceorderitem (ecommerceorderitemname, 
						disabled, datecreated, ecommerceorderid, ecommercelistinglocationid, ecommercelistingid, 
						productstockitemid) VALUES ('customer order item', '0', '$date', '$ordertableid', '$ecommercelistinglocationid', 
						'$ecommercelistingid', '$productstockitemid')");
						// new update
						
					}	
				}
				$iterate = $iterate+1;
			}
		}	
		
	}
}
?>





