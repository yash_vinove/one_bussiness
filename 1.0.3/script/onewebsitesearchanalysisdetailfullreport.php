<?php

//LANGUAGE COLLECTION SECTION
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (209,210,211,212,213,214,215,216,217,218,230,231,232,233,234,235,236,237,238,239,240,
257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,
276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,
303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,
329,330,331,332,333,334,335,336,406,407,408,409,410,411,412,413,414,415,416,417,418,419,
420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,
446,447,448,449,450,451,452,567,568,569,570,571,572,573,574,575,576,577,578,579,580,581)");

while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}


//GENERATE DOCUMENT

$getrecord = mysql_query("select * from $database.websitesearchanalysis 
inner join $database.website on website.websiteid = websitesearchanalysis.onewebsiteid
where websitesearchanalysis.websitesearchanalysisid = '$rowid'");
$getrecordrow = mysql_fetch_array($getrecord);
$websitesearchanalysisid = $getrecordrow['websitesearchanalysisid'];
$websitename = $getrecordrow['websitename'];
$websiteurl = $getrecordrow['websiteurl'];



//date created
$date = date("d-M-Y");
$date2 = date("Y-m-d");

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','B',13);
$pdf->Cell(75);
$pdf->Cell(40,10,$langval567.$websitename,0,0,'C');
$pdf->Ln(15);
$pdf->SetFont('Arial','',10);
$pdf->MultiCell(0,5,$langval211.$date,0,1);
$pdf->MultiCell(0,5,$langval212.$websiteurl,0,1);
$pdf->Ln(6);
$pdf->SetFont('Arial','U',12);
$pdf->MultiCell(0,5,$langval575,0,1);
$pdf->Ln(6);
$pdf->SetFont('Arial','',10);
$pdf->MultiCell(0,5,$langval568,0,1);
$pdf->Ln(2);

$i = 1;
if(isset($profilereportattachmentpath)){
	$pdf->MultiCell(0,5,$i.") ".$langval569,0,1);
	$i = $i + 1;
}		
if(isset($competitorreportattachmentpath)){
	$pdf->MultiCell(0,5,$i.") ".$langval570,0,1);
	$i = $i + 1;
}			
if(isset($keywordreportattachmentpath)){
	$pdf->MultiCell(0,5,$i.") ".$langval572,0,1);
	$i = $i + 1;
}	
if(isset($seoreportattachmentpath)){
	$pdf->MultiCell(0,5,$i.") ".$langval571,0,1);
	$i = $i + 1;
}		
if(isset($yourlinkreportattachmentpath)){
	$pdf->MultiCell(0,5,$i.") ".$langval573,0,1);
	$i = $i + 1;
}	
if(isset($competitorlinkreportattachmentpath)){
	$pdf->MultiCell(0,5,$i.") ".$langval574,0,1);
	$i = $i + 1;
}

if(isset($profilereportattachmentpath)){
	$pdf->SetFont('Arial','B',13);
	$pdf->Ln(10);
	$pdf->Cell(75);
	$pdf->Cell(40,10,$langval576,0,0,'C');
	$pdf->Ln(10);
	$recordid = $rprofilereportid;
	include('onewebsitesearchprofilereport.php');	
}		
if(isset($competitorreportattachmentpath)){
	$pdf->SetFont('Arial','B',13);
	$pdf->Ln(10);
	$pdf->Cell(75);
	$pdf->Cell(40,10,$langval577,0,0,'C');
	$pdf->Ln(10);
	$websitesearchcompetitorreviewid = $rcompetitorreviewid;
	include('onewebsitesearchcompetitorreport.php');	
}	
if(isset($keywordreportattachmentpath)){
	$pdf->SetFont('Arial','B',13);
	$pdf->Ln(10);
	$pdf->Cell(75);
	$pdf->Cell(40,10,$langval578,0,0,'C');
	$pdf->Ln(10);
	$websitesearchanalysisid = $rowid;
	include('onewebsitesearchanalysisdetailactionkeywordsreportsection.php');
}	
if(isset($seoreportattachmentpath)){
	$pdf->SetFont('Arial','B',13);
	$pdf->Ln(10);
	$pdf->Cell(75);
	$pdf->Cell(40,10,$langval579,0,0,'C');
	$pdf->Ln(10);
	include('onewebsitesearchanalysisdetailactiononpageseoreportsection.php');
}
if(isset($yourlinkreportattachmentpath)){
	$pdf->SetFont('Arial','B',13);
	$pdf->Ln(10);
	$pdf->Cell(75);
	$pdf->Cell(40,10,$langval580,0,0,'C');
	$pdf->Ln(10);
	include('onewebsitesearchanalysisdetailactioncurrentlinksreportsection.php');
}
if(isset($competitorlinkreportattachmentpath)){
	$pdf->SetFont('Arial','B',13);
	$pdf->Ln(10);
	$pdf->Cell(75);
	$pdf->Cell(40,10,$langval581,0,0,'C');
	$pdf->Ln(10);
	include('onewebsitesearchanalysisdetailactioncompetitorlinksreportsection.php');
}

//uncomment this, if you want to see an example in the browser for testing. However, comment it out once you are finished otherwise the whilst loop will not run.
//$pdf->Output();

//check if onewebsite storage folder exists, if not, then create
$database10 = str_replace("andrewnorth_", '', $database);
$filedir = '../documents/'. $database10."/sf/websitesearchanalysis/";
if(file_exists($filedir)){
}
else {
	$file_directory = "../documents/".$database10."/sf/websitesearchanalysis/";
	mkdir($file_directory);
}

$filename="../documents/".$database10."/sf/websitesearchanalysis/".$rowid." - Website SEO Full Report.pdf";
$pdf->Output($filename,'F');

//create document record in structurefunctiondocument
$recordname = $rowid." - Website SEO Full Report.pdf";
$docurl = "../documents/".$database10."/sf/websitesearchanalysis/".$recordname;
$insertdocrecord = mysql_query("insert into $database.structurefunctiondocument (structurefunctiondocumentname, datecreated, 
documenturl, doctypename, structurefunctionid, rowid) values ('$recordname', '$date2', '$docurl', 
'Full Report', '218', '$rowid')");
$fullreportattachmentpath = $docurl;

?>