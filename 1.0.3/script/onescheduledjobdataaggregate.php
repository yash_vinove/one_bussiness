<?php
set_time_limit(500);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$hour = date('H');
$date = date('Y-m-d');
$date2 = date('Y-m-d', strtotime("- 2 days"));
$date3 = date('Y-m-d H:i:s', strtotime("- 5 minutes"));
echo "<br/>hour: ".$hour."<br/>";

$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$query = mysql_query("delete from scheduledjobstataggregate 
where daterun >= '$date2'");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Delete last couple of days of data: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;

$insertarray = array();


echo "<br/><br/><b>Aggregate Data</b>";
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$query = "select DATE_FORMAT(startdatetime, '%Y-%m-%d') as 'DateRun', scripturl as 'Script', 
count(scheduledjobstatid) as 'No Runs', round(sum(executiontime), 2) as 'Avg Execution Time', 
round(sum(nosqlqueries), 0) as 'Avg No SQL Queries', round(sum(sqlqueriestime), 2) as 'Avg SQL Queries Time', 
SUM(CASE WHEN enddatetime = '0000-00-00 00:00:00' THEN 1 ELSE 0 END) as 'No of Failures' 
from scheduledjobstat
where startdatetime >= '$date2' and startdatetime <= '$date3'
group by DATE_FORMAT(startdatetime, '%Y-%m-%d') asc, scripturl ASC";
echo "<br/><br/>".$query."<br/>";
$query = mysql_query($query);
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get scheduled job stats for last couple of days: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
while($queryrow = mysql_fetch_array($query)){
	$daterun = $queryrow['DateRun'];
	$noruns = $queryrow['No Runs'];
	$scripturl = $queryrow['Script'];
	$scripturl = str_replace("http://www.onebusiness-liveserver.com/onebusiness/script/", "", $scripturl);
	$scripturl = str_replace("http://www.onebusiness-liveserver.com/", "", $scripturl);
	$avgexecutiontime = $queryrow['Avg Execution Time'];
	$avgnosqlqueries = $queryrow['Avg No SQL Queries'];
	$avgsqlqueriestime = $queryrow['Avg SQL Queries Time'];
	$nooffailures = $queryrow['No of Failures'];

	array_push($insertarray, array("daterun"=>$daterun, "noruns"=>$noruns, "scripturl"=>$scripturl, "avgexecutiontime"=>$avgexecutiontime, 
	"avgnosqlqueries"=>$avgnosqlqueries,"avgsqlqueriestime"=>$avgsqlqueriestime,"nooffailures"=>$nooffailures));
}


//INSERT ALL THE NEW DATA
echo "<br/><br/>insertarray: ".json_encode($insertarray);
$i = 1;
foreach($insertarray as $insertrow){
	$daterun = $insertrow['daterun'];
	$noruns = $insertrow['noruns'];
	$scripturl = $insertrow['scripturl'];
	$avgexecutiontime = $insertrow['avgexecutiontime'];
	$avgnosqlqueries = $insertrow['avgnosqlqueries'];
	$avgsqlqueriestime = $insertrow['avgsqlqueriestime'];
	$nooffailures = $insertrow['nooffailures'];
	$scripturl = mysql_real_escape_string($scripturl);
	if($i == 1){
		$insert = "insert into scheduledjobstataggregate (daterun, noofruns, scripturl, scheduledjobstataggregatename, avgexecutiontime,
		avgnosqlqueries, avgsqlqueriestime, nooffailures, disabled, datecreated, masteronly) values ";	
	}
	$insert = $insert."('$daterun', '$noruns', '$scripturl', '$scripturl', '$avgexecutiontime', '$avgnosqlqueries', '$avgsqlqueriestime', 
	'$nooffailures', '0', '$date', '0'), ";
	if($i == 1000){
		$insert = rtrim($insert, ", ");
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$insert = mysql_query($insert);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Insert 1000 rows: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;		
		$i = 1;
	}
	else {
		$i = $i + 1;
	}
}
if($i <> 1){
	$insert = rtrim($insert, ", ");
	echo "<br/><br/>insert: ".$insert;
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$insert = mysql_query($insert);
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Insert remaining rows: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;	
}

//delete all stat records that are older than 11 days
$date11 = date('Y-m-d', strtotime("- 11 days"));
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$insert = mysql_query("delete from scheduledjobstat where startdatetime <= '$date11'");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Delete old records: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;		

echo "<br/><br/>";
?>