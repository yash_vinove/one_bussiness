<?php
#ini_set('display_errors',1);
#ini_set('display_startup_errors',1);
#error_reporting(-1);

require_once('TwitterAPIExchange.php');
define("ROOT_URL","https://api.twitter.com/1.1/");
define("ROOT_URL2","https://data-api.twitter.com/1.1/");
define("HOST_URL","http://104.131.178.30/");

//prevent errors from showing if empty, and also provide a way to call commands from the url via cron
if(isset($_GET['action'])){
	$action = $_GET['action'];
}else{
	$action = 'index';
}

function index(){
	//don't do anything... just prevents errors.
	//echo "Custom Twitter API ready";
}

function auth_via_handle($tweetid){
	global $database;
	$settings_query = mysql_query("select * from $database.socialpostsendout	 
	inner join socialtwitteraccount on socialtwitteraccount.socialtwitteraccountid = socialpostsendout.socialtwitteraccountid 
	where socialpostsendoutid = '$tweetid'");
	if($settings_query != false){
		while($row12 = mysql_fetch_array($settings_query)){
			$oauth_access_token = $row12['oauth_access_token'];
			$oauth_access_token_secret = $row12['oauth_access_token_secret'];
			$consumer_key = $row12['consumer_key'];
			$consumer_secret = $row12['consumer_secret'];
			$socialtwitteraccountid = $row12['socialtwitteraccountid'];
			$settings = array('oauth_access_token' => $oauth_access_token,
			'oauth_access_token_secret' => $oauth_access_token_secret,
			'consumer_key' => $consumer_key, 
			'consumer_secret' => $consumer_secret);
		}
		return $settings;
	}else{
		//bad handle
		return array('status'=>'false','message'=>'invalid twitter handle');
	}
}

function auth_via_account($twitteraccountid){
	global $database;
	$settings_query = mysql_query("select * from $database.socialtwitteraccount 
	where socialtwitteraccountid = '$twitteraccountid'");
	if($settings_query != false){
		while($row12 = mysql_fetch_array($settings_query)){
			$oauth_access_token = $row12['oauth_access_token'];
			$oauth_access_token_secret = $row12['oauth_access_token_secret'];
			$consumer_key = $row12['consumer_key'];
			$consumer_secret = $row12['consumer_secret'];
			$socialtwitteraccountid = $row12['socialtwitteraccountid'];
			$settings = array('oauth_access_token' => $oauth_access_token,
			'oauth_access_token_secret' => $oauth_access_token_secret,
			'consumer_key' => $consumer_key, 
			'consumer_secret' => $consumer_secret);
		}
		return $settings;
	}else{
			//bad handle
		return array('status'=>'false','message'=>'invalid twitter handle');
	}
}





function send_tweet($tweetid,$tweet_message,$tweet_pic){
	//get all scheduled tweets that meet schedueled critera
	global $database;
	global $nosqlqueries;
	global $sqlstarttime;
	global $sqlqueriestime;
			
	$now = time();
	$message = $tweet_message;
	$pic = $tweet_pic;
	if(empty($tweet_pic)){
		$post = post_tweet($tweetid,$message);
	}else{
		$post = post_tweet_pic($tweetid,$message,$pic);
	}
	
	if(isset($post)){
		$post_obj = json_decode($post);
		
		//echo json_encode($post_obj);
		if(isset($post_obj->id)){
			$twitter_tweet_id = $post_obj->id;
		}
		else {
			$twitter_tweet_id = "";	
		}
		$date = date("Y-m-d");
		
		if($twitter_tweet_id != false){
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updatesent = mysql_query("update $database.socialpostsendout 
			set tweetid = '$twitter_tweet_id', socialpostsendoutstatusid = '1', datesent = '$date' 
			where socialpostsendoutid = '$tweetid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			//echo "<br/>Get list of news sources: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		}else{
			if(isset($post_obj->errors[0]->code)){
				$error = $post_obj->errors[0]->code;
			}
			else {
				$error = "";		
			}
			if(isset($post_obj->errors[0]->message)){
				$message = $post_obj->errors[0]->message;
			}
			else {
				$message = "";		
			}
			$errormessage = $error." - ".$message;		
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updatesent = mysql_query("update $database.socialpostsendout 
			set tweetid = '$twitter_tweet_id', socialpostsendoutstatusid = '2', datesent = '$date', tweeterror = '$errormessage' 
			where socialpostsendoutid = '$tweetid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			//echo "<br/>Get list of news sources: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			//echo "Unable to post tweet.";
		}
	}
}


function get_accounts(){
	$db = new Db_model();
	$accounts = $db->read('*','twitter_account',"1=1");
	if($accounts != false){
		//return all accounts
		return $accounts;
	}else{
		return false;
	}
}


function send_post_request($url,$data){
	$options = array(
		'http' => array(
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => 'POST',
			'content' => http_build_query($data),
			),
		);
	$context  = stream_context_create($options);
	$url = str_replace(" ", "-", $url);
	$result = file_get_contents($url, false, $context);
	if(!empty($result)){
		return true;
	}else{
		return false;
	}
}


function post_tweet_pic($tweetid,$message,$pic){
	$settings = auth_via_handle($tweetid);
	$url = "https://upload.twitter.com/1.1/media/upload.json";
	$requestMethod = "POST";
	//uncomment line below if testing on localhost
  	//$pic = "https://www.facebook.com/images/fb_icon_325x325.png";
  	$postfields = array(
		'media_data' => base64_encode(file_get_contents($pic))
		);
	//echo json_encode($postfields);
	try {
		$twitter = new TwitterAPIExchange($settings);
		$post = $twitter->buildOauth($url, $requestMethod)
		->setPostfields($postfields)
		->performRequest();
		//echo json_encode($post);
		$post = json_decode($post);
		if(isset($post->media_id_string)){
			$mediaid = $post->media_id_string;	
			//echo $media_id;
		}
		else {
			return $post;		
		}
	} catch (Exception $ex) {
		//echo $ex->getMessage();
	}
	
	if(isset($post->media_id_string)){
		$postfields = array(
		'media_ids' => $mediaid,
		'status' => $message);
		$url = ROOT_URL.'statuses/update.json';
		$requestMethod = 'POST';
		try {
			$twitter = new TwitterAPIExchange($settings);
			$post = $twitter->buildOauth($url, $requestMethod)
			->setPostfields($postfields)
			->performRequest();
			//echo $post;
			return $post;
		} catch (Exception $ex) {
			//echo $ex->getMessage();
		}
	}
}

function post_tweet($tweetid,$message){
	$settings = auth_via_handle($tweetid);
	$postfields = array('status' => $message);
	$url = ROOT_URL.'statuses/update.json';
	$requestMethod = 'POST';
	try {
		$twitter = new TwitterAPIExchange($settings);
		$post = $twitter->buildOauth($url, $requestMethod)
		->setPostfields($postfields)
		->performRequest();
		//echo $post;
		return $post;
	} catch (Exception $ex) {
		//echo $ex->getMessage();
	}
}

function update_follows(){
	$accounts = get_accounts();
	foreach($accounts as $account){
		update_follower_count($account->handle);
	}
}

function get_followerlist($handle,$accountstring){
	global $database;
	global $nosqlqueries;
	global $sqlstarttime;
	global $sqlqueriestime;
	global $statusupdatesuccessfulquery;
	global $statusupdateunfollowquery;
	$settings = auth_via_account($handle);
	$url = ROOT_URL.'friendships/lookup.json'; 
	$getfield = '?screen_name='.$accountstring;
	$requestMethod = 'GET';
	$twitter = new TwitterAPIExchange($settings);
		#print_r($twitter);
	$json = $twitter->setGetfield($getfield)
	->buildOauth($url, $requestMethod)
	->performRequest();
	//echo $json;
	$response = json_decode($json, true);
	if (strpos($json,'followed_by') !== false) {
    	$connections = "Yes";
    	echo " - follower:".$connections."<br/>";
		$statusupdatesuccessfulquery = $statusupdatesuccessfulquery."'".$accountstring."',";
	}	
	else {
		$connections = "No";
		echo " - follower:".$connections."<br/>";
		$statusupdateunfollowquery = $statusupdateunfollowquery."'".$accountstring."',";
    	//echo "<br/>".$update."<br/>";		
	}

}

function post_unfollow($handle,$screenname){
	//run code to unfollow here
	global $database;
	global $nosqlqueries;
	global $sqlstarttime;
	global $sqlqueriestime;
	global $updatefailedquery;
	$postfields = array('screen_name' => $screenname);
	$settings = auth_via_account($handle);
	$url = ROOT_URL.'friendships/destroy.json';
	$requestMethod = 'POST';
	$twitter = new TwitterAPIExchange($settings);
	$post = $twitter->buildOauth($url, $requestMethod)->setPostfields($postfields)->performRequest();
	$response = json_decode($post);
	//echo $post;
	
	//run code to remove user from admin table
	$updatefailedquery = $updatefailedquery."(socialtwitteraccountid = '$handle' 
 	and socialfollowautofollowname = '$screenname') or ";
 	echo "<br/>Unfollowed: ".$screenname;
}


function get_publictweets_activitynameincluded($activityname,$twitteraccountid,$number){
	$settings = auth_via_account($twitteraccountid);
	global $database;
	global $countryid;
	global $countryname;
	global $countryscode;
	global $nosqlqueries;
	global $sqlstarttime;
	global $sqlqueriestime;
	$date = date("Y-m-d");
	$url = ROOT_URL.'search/tweets.json'; 
	$getfield = '?q='.$activityname.'&result_type=recent&count=30';
	$requestMethod = 'GET';
	$twitter = new TwitterAPIExchange($settings);
		#print_r($twitter);
	$json = $twitter->setGetfield($getfield)
	->buildOauth($url, $requestMethod)
	->performRequest();
	
	echo $json;
	$response = json_decode($json);
	$p = 1;
	$counter9 = 0;
	foreach($response->statuses as $tweet) {
		//get each tweet
		$location = "";
		$countryshortcode = "";
		$countrylongname = "";
		$tweet_text = $tweet->text;
		$createdat = $tweet->created_at;
		$tweet_screenname = $tweet->user->screen_name;
		echo "<br/>time: ".date("Y-m-d H:i:s");
		echo "<br/>createdat: ".$createdat;
		echo "<br/>createdat: ".strtotime($createdat);
		if(time() - strtotime($createdat) < 3599){
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$getexisting = mysql_query("select * from $database.socialfollowautofollow
		  	where socialfollowautofollowname = '$tweet_screenname' and socialtwitteraccountid <> 0 
		  	and socialfollowautofollowstatusid in (1,2,3)");
		  	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			//echo "<br/>Get list of news sources: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		  	if(mysql_num_rows($getexisting) >= 1){
		  		//already following, or waiting to confirm follow. No need to refollow.
		  		echo "<br/>record exists and so stopped";
		  	}	
		  	else {					  	
				if(isset($tweet->user->location)){
					$location = $tweet->user->location;
					if($location <> ""){
						echo "<br/><br/>";
						echo "<b>".$activityname."</b> - screenname: ".$tweet_screenname." text: ".$tweet_text;	
						echo "<br/>location: ".$location;								  	
						if($counter9 <= $number){
							$location = mysql_real_escape_string($location);
							$nosqlqueries = $nosqlqueries + 1;
							$sqlstarttime = microtime(true);
							$getlocation = mysql_query("select * from $database.socialfollowlocationcountry 
							inner join country on country.countryid = socialfollowlocationcountry.countryid
							where socialfollowlocationcountryname = '$location'");
							$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
							$differencemilliseconds = microtime(true) - $sqlstarttime;
							//echo "<br/>Get list of news sources: ";
							echo "<br/>differencemilliseconds: ".$differencemilliseconds;
							echo "<br/>sqlqueriestime: ".$sqlqueriestime;
							if(mysql_num_rows($getlocation) >= 1){
								$getlocationrow = mysql_fetch_array($getlocation);
								$countryshortcode = $getlocationrow['countryshortcode'];
								$countrylongname = $getlocationrow['countryname'];
								echo "<br/>location was found in existing data store";
							}
							else {
								echo "<br/>location was not found in existing data store, checking elsewhere";
								$prepAddr = explode(",", $location);
								$prepAddr2 = $prepAddr[0];
								$prepAddr4 = trim($prepAddr2);
						     	$geocode=file_get_contents('https://www.meteoblue.com/en/server/search/query3?query='.$prepAddr4.'&itemsPerPage=1');
						     	echo "<br/>prepaddr: ".$prepAddr4;
						     	$output= json_decode($geocode);
						     	echo "<br/>returned result: ".json_encode($output);
					     		if(isset($output->results[0]->country)){
									$countrylongname = $output->results[0]->country;
									$countryshortcode = $output->results[0]->iso2;
									echo "<br/>found country details";
									echo "<br/>countryshortcodefrom: ".$countryshortcode;
								  	echo "<br/>countrylongnamefrom: ".$countrylongname;
									
									//save to local
									$nosqlqueries = $nosqlqueries + 1;
									$sqlstarttime = microtime(true);
									$getcountry = mysql_query("select * from $database.country where countryname = '$countrylongname' or 
									countryshortcode = '$countryshortcode'");
									$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
									$differencemilliseconds = microtime(true) - $sqlstarttime;
									//echo "<br/>Get list of news sources: ";
									echo "<br/>differencemilliseconds: ".$differencemilliseconds;
									echo "<br/>sqlqueriestime: ".$sqlqueriestime;
									if(mysql_num_rows($getcountry)>= 1){
										$getcountryrow = mysql_fetch_array($getcountry);
										$cid = $getcountryrow['countryid'];
										$date5 = date("Y-m-d");
										$nosqlqueries = $nosqlqueries + 1;
										$sqlstarttime = microtime(true);
										$savelocation = mysql_query("insert into $database.socialfollowlocationcountry (socialfollowlocationcountryname, 
										countryid, disabled, datecreated) values ('$location', '$cid', '0', '$date5')");
										$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
										$differencemilliseconds = microtime(true) - $sqlstarttime;
										//echo "<br/>Get list of news sources: ";
										echo "<br/>differencemilliseconds: ".$differencemilliseconds;
										echo "<br/>sqlqueriestime: ".$sqlqueriestime;
									}
								}
							}
							//add to backlog to sendout
						  	if($countryname == $countrylongname || $countryscode == $countryshortcode || $countryname == ""){
						  		//either completely new, or failed to follow last time. Follow.
						  		$counter9 = $counter9 + 1;
						  		echo "<br/>record does not exist and so checking whether i already have enough records for this run - ".$p." - ".$number;
						  		if($p <= $number){
						  			echo "<br/>record is needed, and so adding";
									$nosqlqueries = $nosqlqueries + 1;
									$sqlstarttime = microtime(true);
									$deleteexistingfailures = mysql_query("delete from $database.socialfollowautofollow 
									where socialfollowautofollowname = '$tweet_screenname' and socialfollowautofollowstatusid = 4");
									$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
									$differencemilliseconds = microtime(true) - $sqlstarttime;
									//echo "<br/>Get list of news sources: ";
									echo "<br/>differencemilliseconds: ".$differencemilliseconds;
									echo "<br/>sqlqueriestime: ".$sqlqueriestime;
									
									$insertrequest = "insert into $database.socialfollowautofollow (socialfollowautofollowname, 
									tweettext, socialfollowautofollowstatusid, socialtwitteraccountid, datefollowed, disabled, datecreated, masteronly, 
									hashtag, socialfollowtypeid)
									values ('$tweet_screenname', '$tweet_text', '1', '$twitteraccountid', '$date', '0', '$date', '0', '$activityname', '1')";
									$nosqlqueries = $nosqlqueries + 1;
									$sqlstarttime = microtime(true);
									$insertrequest = mysql_query($insertrequest);
									$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
									$differencemilliseconds = microtime(true) - $sqlstarttime;
									//echo "<br/>Get list of news sources: ";
									echo "<br/>differencemilliseconds: ".$differencemilliseconds;
									echo "<br/>sqlqueriestime: ".$sqlqueriestime;
									
									if($p == $number){
										break;									
									}
								}
								$p = $p + 1;							
							}
							else {
								echo "<br/>does not match my country and so skipping";									
							}
						}
					}
				}
			}
		}
	} 
}


//used in onesocialinteract
function get_publictweets_findsomethingtoretweet($searchterm,$twitteraccountid,$optiondkeywordmatches){
	$settings = auth_via_account($twitteraccountid);
	global $database;
	global $finaltweet;
	global $finaltweetid;
	global $nosqlqueries;
	global $sqlstarttime;
	global $sqlqueriestime;
	$date = date("Y-m-d");
	$url = ROOT_URL.'statuses/user_timeline.json'; 
	$getfield = '?screen_name='.$searchterm.'&result_type=recent&count=10';
	$requestMethod = 'GET';
	$twitter = new TwitterAPIExchange($settings);
		#print_r($twitter);
	$json = $twitter->setGetfield($getfield)
	->buildOauth($url, $requestMethod)
	->performRequest();
	
	//echo $json;
	$response = json_decode($json);
	foreach($response as $tweet) {
		//get each tweet
		if(isset($tweet->text)){
			$tweettext = $tweet->text;
			$tweetid = $tweet->id;
			$sensitive = $tweet->possibly_sensitive;
			$createdat = $tweet->created_at;
			$tweetscreenname = $tweet->user->screen_name;
			$array = explode('QWERTY', $optiondkeywordmatches);
			$notaddressedto = 0;
			if (stripos($tweettext, "@") !== false) {
				$notaddressedto = 1;
			}
			if($finaltweet == "" && $sensitive == 0 && $notaddressedto == 0){
				//echo "<br/><br/>".$tweetscreenname." - ".$createdat." - ".$tweettext;
				foreach($array as $value) {
					//echo "<br/>".$value;
					if (stripos($tweettext, $value) !== false) {
			    		//echo ' - true';
			    		$finaltweet = $tweettext;
			    		$finaltweetid = $tweetid;
			    		break;
					}
					else {
						//echo ' - false';			
					}
				}
			}
		}
		
	} 
}

//used in onesocialinteract
function post_retweet($tweetid,$twitteraccountid){
	//now add them as a follower to the account
	$postfields = array();
	$settings = auth_via_account($twitteraccountid);
	global $database;
	$url = ROOT_URL.'statuses/retweet/'.$tweetid.'.json';
	$requestMethod = 'POST';

	$twitter = new TwitterAPIExchange($settings);
	$post = $twitter->buildOauth($url, $requestMethod)->setPostfields($postfields)->performRequest();

	//echo "<br/><br/>".$post;
}

//used in onesocialinteract
function post_like($tweetid,$twitteraccountid){
	//now add them as a follower to the account
	$postfields = array('id' => $tweetid);
	$settings = auth_via_account($twitteraccountid);
	global $database;
	$url = ROOT_URL.'favorites/create.json';
	$requestMethod = 'POST';

	$twitter = new TwitterAPIExchange($settings);
	$post = $twitter->buildOauth($url, $requestMethod)->setPostfields($postfields)->performRequest();

	echo "<br/><br/>".$post;
}

function get_publicusers_categorynameincluded($categoryname,$twitteraccountid,$number){
	$settings = auth_via_account($twitteraccountid);
	global $database;
	$url = ROOT_URL.'users/search.json'; 
	$getfield = '?q='.$categoryname.'&count='.$number;
	$requestMethod = 'GET';
	$twitter = new TwitterAPIExchange($settings);
		#print_r($twitter);
	$json = $twitter->setGetfield($getfield)
	->buildOauth($url, $requestMethod)
	->performRequest();
	
	echo $json;
	$response = json_decode($json);

}


function post_follow($screenname,$twitteraccountid){
	//now add them as a follower to the account
	$postfields = array('screen_name' => $screenname, 'follow' => 'true');
	$settings = auth_via_account($twitteraccountid);
	global $database;
	global $nosqlqueries;
	global $sqlstarttime;
	global $sqlqueriestime;
	
	$url = ROOT_URL.'friendships/create.json';
	$requestMethod = 'POST';

	$twitter = new TwitterAPIExchange($settings);
	$post = $twitter->buildOauth($url, $requestMethod)->setPostfields($postfields)->performRequest();
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$updateasfollowed = mysql_query("update $database.socialfollowautofollow 
	set socialfollowautofollowstatusid = '2' 
	where socialfollowautofollowname = '$screenname' and socialfollowautofollowstatusid = '1'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	//echo "<br/>Get list of news sources: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	echo "<br/>Followed: ".$screenname;
	return $post;
}


function clubtray_get_search($activityname,$clubtray){
	$settings = auth_via_clubtray($clubtray);

	$url = ROOT_URL.'search/tweets.json'; 
	$getfield = '?q='.$activityname.'&count=10';
	$requestMethod = 'GET';
	$twitter = new TwitterAPIExchange($settings);
		#print_r($twitter);
	$json = $twitter->setGetfield($getfield)
	->buildOauth($url, $requestMethod)
	->performRequest();
	
	//echo $json;
	$response = json_decode($json);
	foreach($response->statuses as $tweet)
	{
		
		//get each tweet
		$tweet_text = $tweet->text;
		$tweet_screenname = $tweet->user->screen_name;
	  //	echo "<br/><br/>";
	  	//echo "screenname: ".$tweet_screenname." text: ".$tweet_text;
	  
	  	//follow the tweeter
	  	$postfields = array('screen_name' => $tweet_screenname, 'follow' => 'true');
		$settings = auth_via_clubtray($clubtray);
	
		$url = ROOT_URL.'friendships/create.json';
		$requestMethod = 'POST';
	
		$twitter = new TwitterAPIExchange($settings);
		$post = $twitter->buildOauth($url, $requestMethod)->setPostfields($postfields)->performRequest();
	
		if($post){
			return $post;
		}else{
			return false;
		}
		$response = json_decode($post);
	  	//echo "<br/><br/>".$response;
		
	} 
}






















function get_stats_overall($socialmonitoraccountid,$twitteraccountid,$screenname){
	$settings = auth_via_account($twitteraccountid);
	global $database;
	global $nosqlqueries;
	global $sqlstarttime;
	global $sqlqueriestime;
	global $updatesarray;
	$date = date("Y-m-01");
	$url = ROOT_URL.'users/search.json'; 
	$getfield = '?q='.$screenname.'&count=1';
	$requestMethod = 'GET';
	$twitter = new TwitterAPIExchange($settings);
		#print_r($twitter);
	$json = $twitter->setGetfield($getfield)
	->buildOauth($url, $requestMethod)
	->performRequest();
	//echo $json;
	$response = json_decode($json);
	foreach($response as $data) {
		$nooffollowers = $data->followers_count;
		echo "<br/>nooffollowers: ".$nooffollowers."<br/>";
		array_push($updatesarray, array("socialmonitoraccountid"=>$socialmonitoraccountid,"nooffollowers"=>$nooffollowers,
		"accounttype"=>"Twitter","resultdate"=>$date));
		
		
	}
}

function get_followers($twitteraccountid,$screenname,$socialmonitoraccountid){
	$settings = auth_via_account($twitteraccountid);
	global $database;
	global $nosqlqueries;
	global $sqlstarttime;
	global $sqlqueriestime;
	$date = date("Y-m-d");
	$url = ROOT_URL.'followers/ids.json'; 
	$getfield = '?screen_name='.$screenname.'&count=5000';
	$requestMethod = 'GET';
	$twitter = new TwitterAPIExchange($settings);
		#print_r($twitter);
	$json = $twitter->setGetfield($getfield)
	->buildOauth($url, $requestMethod)
	->performRequest();
	//echo $json;
	echo "<br/>Get list of last 5000 followers<br/>";
	$response = json_decode($json);
	$insertrecord = "insert into $database.socialmonitoraccountcurrentfollower 
	(followerid, disabled, datecreated, masteronly, socialmonitoraccountid, status, followdate) 
	values ";
	$checkrecords = array();
	$z = 0;
	$l = 0;
	foreach($response->ids as $followerid) {
		array_push($checkrecords, $followerid);
		$l = $l + 1;		
		
		if($l == 500){
			$checkrecords2 = "";
			foreach($checkrecords as $follower){
				$checkrecords2 = $checkrecords2.$follower.",";
			}	
			$checkrecords2 = rtrim($checkrecords2, ',');
			echo "<br/><br/>list of twitter followers: ".json_encode($checkrecords);
			$checkifexists = "select * from $database.socialmonitoraccountcurrentfollower 
			where followerid in ($checkrecords2) and socialmonitoraccountid = '$socialmonitoraccountid'";
			//echo $checkifexists;
			$checkifexists = mysql_query($checkifexists);
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Check if followers exist, in bulk: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;	
			$existingrecords = array();
			
			if(mysql_num_rows($checkifexists) >= 1){
				while($row67 = mysql_fetch_array($checkifexists)){
					$followerid = $row67['followerid'];
					array_push($existingrecords, $followerid);
				}
				echo "<br/><br/>list of twitter followers that exist in our system: ".json_encode($existingrecords);	
			}
			
			$missingrecords = array_diff($checkrecords, $existingrecords);	
			echo "<br/><br/>list of twitter followers that do not exist in our system: ".json_encode($missingrecords);
			if(count($missingrecords) >= 1){
				foreach($missingrecords as $followerid){
					$insertrecord = $insertrecord."('$followerid', '0', '$date', '0', '$socialmonitoraccountid', 'Follower', '$date'),";
					$z = $z + 1;
				}
			}
			$checkrecords = array();
			$checkrecords2 = "";
			$existingrecords = array();
			$missingrecords = array();
			$l = 0;
		}
		
		
		if($z == 500){
			$insertrecord = rtrim($insertrecord, ',');			
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$insertrecord = mysql_query($insertrecord);
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Insert follower if does not exist: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;	
			$insertrecord = "insert into $database.socialmonitoraccountcurrentfollower 
			(followerid, disabled, datecreated, masteronly, socialmonitoraccountid, status, followdate) 
			values ";
			$z = 0;
		}
	}
	if($z <= 500){
		$insertrecord = rtrim($insertrecord, ',');			
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$insertrecord = mysql_query($insertrecord);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Insert follower if does not exist: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;	
	}
}

function get_follower_detail($followeridstring,$twitteraccountid){
	$settings = auth_via_account($twitteraccountid);
	global $database;
	global $masterdatabase;
	global $nosqlqueries;
	global $sqlstarttime;
	global $sqlqueriestime;
	global $languages;
	$date = date("Y-m-d");
	$followeridstring = str_replace("###", ",", $followeridstring);
	$url = ROOT_URL.'users/lookup.json'; 
	$getfield = '?user_id='.$followeridstring;
	$requestMethod = 'GET';
	$twitter = new TwitterAPIExchange($settings);
		#print_r($twitter);
	$json = $twitter->setGetfield($getfield)
	->buildOauth($url, $requestMethod)
	->performRequest();
	//echo "<br/>".$json."<br/>";
	$response = json_decode($json);
	
	foreach($response as $data) {
		$followerid = $data->id;
		$screenname = $data->screen_name;
		$name = $data->name;
		$name = str_replace("'", "", $name);
		$location = $data->location;
		$location = str_replace("'", "", $location);
		$followerscount = $data->followers_count;
		$friendscount = $data->friends_count;
		$statusescount = $data->statuses_count;
		$lang = $data->lang;
		//echo "<br/><br/>lang: ".$lang;
		//echo "<br/>followerid: ".$followerid."<br/>";
		//echo "screenname: ".$screenname."<br/>";
		//echo "name: ".$name."<br/>";
		//echo "location: ".$location."<br/>";
		//echo "followerscount: ".$followerscount."<br/>";
		//echo "friendscount: ".$friendscount."<br/>";
		//echo "statusescount: ".$statusescount."<br/>";
		//echo "lang: ".$lang."<br/>";
		$languageitem = array_search($lang, array_column($languages, 'languageshortcode'));
		$languageid = $languages[$languageitem]['languageid'];
		//echo "<br/><br/>languageitem: ".$languageid;
		if($languageid == ""){
			$languageid = 0;		
		}
		$updaterecord = "update $database.socialmonitoraccountcurrentfollower
		set socialmonitoraccountcurrentfollowername = '$name', screenname = '$screenname', 
		location = '$location', followerscount = '$followerscount', friendscount = '$friendscount', 
		statusescount = '$statusescount', language = '$lang', languageid = '$languageid', lastcheckedtwitterdata = '$date' 
		where followerid = '$followerid'";
		//echo $updaterecord;
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$updaterecord = mysql_query($updaterecord);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>For each follower, update their details into database: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	}
	
}

function check_still_following($twitteraccountid,$followeridstring,$socialmonitoraccountid){
	global $database;
	global $nosqlqueries;
	global $sqlstarttime;
	global $sqlqueriestime;
	$date = date("Y-m-d");
	$settings = auth_via_account($twitteraccountid);
	$followeridstring = str_replace("xxx", ",", $followeridstring);
	$url = ROOT_URL.'friendships/lookup.json'; 
	$getfield = '?user_id='.$followeridstring;
	$requestMethod = 'GET';
	$twitter = new TwitterAPIExchange($settings);
		#print_r($twitter);
	$json = $twitter->setGetfield($getfield)
	->buildOauth($url, $requestMethod)
	->performRequest();
	//echo $json;
	$response = json_decode($json, true);
	$num = count($response);
	if(count($response) > 1){
		for($i = 0; $i < $num;$i++){
			$followerid = $response[$i]['id'];
			$con = json_encode($response[$i]['connections']);
			if($followerid <> ''){
				if(strpos($con, 'followed_by') !== false) {
			    	$connections = "Yes";
			    	echo $followerid." - follower:".$connections."<br/>";
				}	
				else {
					$connections = "No";
					echo $followerid." - not follower:".$connections."<br/>";
					$nosqlqueries = $nosqlqueries + 1;
					$sqlstarttime = microtime(true);
					$updateconnection = mysql_query("update $database.socialmonitoraccountcurrentfollower 
			    	set lastcheckedifstillfollower = '$date', status = 'Unfollowed', unfollowdate = '$date'
			    	where followerid = '$followerid' and socialmonitoraccountid = '$socialmonitoraccountid'");
			    	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
					$differencemilliseconds = microtime(true) - $sqlstarttime;
					//echo "<br/>Get list of news sources: ";
					echo "<br/>differencemilliseconds: ".$differencemilliseconds;
					echo "<br/>sqlqueriestime: ".$sqlqueriestime;
				}
			}
		}
	}
}

function get_post_list($twitteraccountid,$screenname,$socialmonitoraccountid){
	global $database;
	global $nosqlqueries;
	global $sqlstarttime;
	global $sqlqueriestime;
	$date = date("Y-m-d");
	$settings = auth_via_account($twitteraccountid);
	$url = ROOT_URL.'statuses/user_timeline.json'; 
	$getfield = '?screen_name='.$screenname.'&count=300';
	$requestMethod = 'GET';
	$twitter = new TwitterAPIExchange($settings);
		#print_r($twitter);
	$json = $twitter->setGetfield($getfield)
	->buildOauth($url, $requestMethod)
	->performRequest();
	//echo "<br/>".$json."<br/><br/>";
	$response = json_decode($json, true);
	foreach($response as $post){
		$createdtime = date('Y-m-d', strtotime($post['created_at']));
		$posttext = $post['text'];
		if(strpos($posttext, "RT @") == '0'){
			$posttext = str_replace("'", "", $posttext);
			$postid = $post['id'];
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$checkrecord = mysql_query("select * from $database.socialmonitoraccountpoststat 
			where socialmonitoraccountid = '$socialmonitoraccountid' and socialpostid = '$postid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Get existing post stats: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			if(mysql_num_rows($checkrecord) == 0){
				echo "<br/>record added: ".$postid." - ".$createdtime." - ".$posttext;
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$addrecord = mysql_query("insert into $database.socialmonitoraccountpoststat
				(socialmonitoraccountpoststatname, posttext, socialpostid, socialmonitoraccountid, disabled, datecreated, masteronly, 
				postdatetime) 
				values ('Twitter Post', '$posttext', '$postid', '$socialmonitoraccountid', '0', '$date', '0', '$createdtime')");
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Create record if does not exist: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			}
		}	
	}
}


function get_posts_details($postidstring,$twitteraccountid){
	$settings = auth_via_account($twitteraccountid);
	global $database;
	global $socialmonitoraccountid;
	global $date6;
	global $nosqlqueries;
	global $sqlstarttime;
	global $sqlqueriestime;
	$date = date("Y-m-d");
	$postidstring = str_replace("###", ",", $postidstring);
	$url = ROOT_URL.'statuses/lookup.json'; 
	$getfield = '?id='.$postidstring;
	$requestMethod = 'GET';
	$twitter = new TwitterAPIExchange($settings);
		#print_r($twitter);
	$json = $twitter->setGetfield($getfield)
	->buildOauth($url, $requestMethod)
	->performRequest();
	//echo "<br/>".$json."<br/>";
	$response = json_decode($json);
	foreach($response as $data) {
		$postid = $data->id;
		$noofshares = $data->retweet_count;
		$nooflikes = $data->favorite_count;
		echo "<br/>".$postid;
		
		//get this posts existing stats
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$getexistingstats = mysql_query("select * from $database.socialmonitoraccountpoststat
		where socialpostid = '$postid' and socialmonitoraccountid = '$socialmonitoraccountid'");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Get posts details: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		$getexistingstatsrow = mysql_fetch_array($getexistingstats);
		$originalnooflikes = $getexistingstatsrow['nooflikes'];		
		$originalnoofshares = $getexistingstatsrow['noofshares'];	
		
		//get the stats for the overall account
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$getaccountstats = mysql_query("select * from $database.socialmonitoraccountoverallstat
		where socialmonitoraccountid = '$socialmonitoraccountid' and resultdate = '$date6'");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Get the overall account stats: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		$getaccountstatsrow = mysql_fetch_array($getaccountstats);
		$existinglikes = $getaccountstatsrow['nooflikes'];
		$existingshares = $getaccountstatsrow['noofshares'];
		
		//add on the new stats to the existing overall account stats
		$newlikes = $nooflikes - $originalnooflikes + $existinglikes;	
		$newshares = $noofshares - $originalnoofshares + $existingshares;	
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$updateaccountstats = mysql_query("update $database.socialmonitoraccountoverallstat
		set nooflikes = '$newlikes', noofshares = '$newshares' 
		where socialmonitoraccountid = '$socialmonitoraccountid' and resultdate = '$date6'");		
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Add the new stats to the overall account stats: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$updateoverallstats = mysql_query("update $database.socialmonitoraccountpoststat
		set nooflikes = '$nooflikes', noofcomments = 0, noofshares = '$noofshares', 
		noofimpressions = 0, noofclicks = 0 
		where socialpostid = '$postid' and socialmonitoraccountid = '$socialmonitoraccountid'");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Update the posts number of statistics: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		
		//echo "<br/>nooflikes: ".$nooflikes;
		//echo "<br/>noofshares: ".$noofshares."<br/>";
		
		if($nooflikes >= 1){
			$url = ROOT_URL.'favorites/list.json'; 
			$getfield = '?screen_name='.$postid;
			$requestMethod = 'GET';
			$twitter = new TwitterAPIExchange($settings);
				#print_r($twitter);
			$json2 = $twitter->setGetfield($getfield)
			->buildOauth($url, $requestMethod)
			->performRequest();
			//echo "<br/>".$json2."<br/>";
			$response2 = json_decode($json2);
				
		}
	}
	
}

function get_posts_retweeters($postid,$twitteraccountid){
	$settings = auth_via_account($twitteraccountid);
	global $database;
	global $socialmonitoraccountid;
	global $socialmonitoraccountpoststatid;
	global $nosqlqueries;
	global $sqlstarttime;
	global $sqlqueriestime;
	$date = date("Y-m-d");
	$url = ROOT_URL.'/statuses/retweeters/ids.json'; 
	$getfield = 'id='.$postid.'&count=100';
	$requestMethod = 'GET';
	$twitter = new TwitterAPIExchange($settings);
		#print_r($twitter);
	$json = $twitter->setGetfield($getfield)
	->buildOauth($url, $requestMethod)
	->performRequest();
	//echo "<br/>".$json."<br/>";
	$response = json_decode($json);
	if(count($response ->ids) >= 1){
		foreach($response -> ids as $userid){
			//echo "<br/>".$userid;
			$followerid = 0;
			$screenname = '';
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$checkuser = mysql_query("select * from $database.socialmonitoraccountcurrentfollower
			where followerid = '$userid' and socialmonitoraccountid = '$socialmonitoraccountid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Get follower record: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			if(mysql_num_rows($checkuser) >= 1){
				$checkuserrow = mysql_fetch_array($checkuser);
				$socialmonitoraccountcurrentfollowerid = $checkuserrow['socialmonitoraccountcurrentfollowerid'];				
				$screenname = $checkuserrow['screenname'];				
				$followerid = $checkuserrow['socialmonitoraccountcurrentfollowerid'];		
			}
			else {
				$url = ROOT_URL.'/users/lookup.json'; 
				$getfield = '?user_id='.$userid;
				$requestMethod = 'GET';
				$twitter = new TwitterAPIExchange($settings);
					#print_r($twitter);
				$json2 = $twitter->setGetfield($getfield)
				->buildOauth($url, $requestMethod)
				->performRequest();
				//echo "<br/>".$json2."<br/>";
				$response2 = json_decode($json2);
				if(count($response2)>=1){
					$screenname = $response2[0]->screen_name;			
				}
			}
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$getexisting = mysql_query("select * from $database.socialmonitoraccountpoststatfollower 
			where socialmonitoraccountpoststatid = '$socialmonitoraccountpoststatid' and interactionfrom = '$screenname'
			and socialmonitoraccountid = '$socialmonitoraccountid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Get existing follower: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			if(mysql_num_rows($getexisting) == 0){
				//echo "<br/>adding screenname: ".$screenname;		
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$insertnew = mysql_query("insert into $database.socialmonitoraccountpoststatfollower 
				(socialmonitoraccountpoststatfollowername, socialmonitoraccountid, socialmonitoraccountpoststatid, 
				socialmonitoraccountcurrentfollowerid, interactiondate, interactiontype, interactionid, interactionfrom, notes, 
				disabled, datecreated, masteronly) values ('Social Interaction', '$socialmonitoraccountid', 
				'$socialmonitoraccountpoststatid', '$followerid', '$date', 
				'Share', '', '$screenname', '', '0', '$date', '0')");	
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Add follower if does not exist: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;				
			}	
			
			
		}	
	}
}


/*

function get_results_twitter($twitteraccountid){
	$handle = $twitteraccountid;
	$settings = auth_via_account($handle);
		#print_r($settings);

	$url = ROOT_URL.'followers/ids.json'; 
	$getfield = '?q='.$handle."&count=10000";
	$requestMethod = 'GET';
	$twitter = new TwitterAPIExchange($settings);
		#print_r($twitter);
	$json = $twitter->setGetfield($getfield)
	->buildOauth($url, $requestMethod)
	->performRequest();

	$obj = json_decode($json);
	$followercount = count($obj->ids);
	$date = date("Y-m-d");

	//echo $followercount;
	$updateaccountstats = mysql_query("update twitteraccount set followercount = '$followercount', lastupdated='$date' where twitteraccountid = '$twitteraccountid'");
}

function get_tweet_results($twitteraccountid,$tweetid){
	#this is a rate limited request. 15 request, in a 15 minute window. So, this cron should be set to 1 per minute or less to prevent Twitter from throttling your account.
	$settings = auth_via_account($handle);

	$url = ROOT_URL.'statuses/show/'.$tweet_id.'.json'; 
	$getfield = '?q='.$handle;
	$requestMethod = 'GET';
	$twitter = new TwitterAPIExchange($settings);
		#print_r($twitter);
	$json = $twitter->setGetfield($getfield)
	->buildOauth($url, $requestMethod)
	->performRequest();

	$obj = json_decode($json);
	#$obj = $obj[0];
	$retweet_count = $obj->retweet_count;
	$favorite_count = $obj->favorite_count;
	#uncomment this if you want to see the full tweet object that gets returned from twitter. There is some other cool stuff you may want to grab?? Just throwing it out there
	//echo "<pre>";
	//print_r($obj);
	//echo "</pre>";
	//echo $retweet_count." retweets.<br>";
	//echo $favorite_count. " favorites.";


	return array("retweet_count"=>$retweet_count,"favorite_count"=>$favorite_count);
	
	
}


function get_resultsoftweet($handle,$tweet_id){
	#this is a rate limited request. 15 request, in a 15 minute window. So, this cron should be set to 1 per minute or less to prevent Twitter from throttling your account.
	$settings = auth_via_handle($handle);

	$url = ROOT_URL.'statuses/show/'.$tweet_id.'.json'; 
	$getfield = '?q='.$handle;
	$requestMethod = 'GET';
	$twitter = new TwitterAPIExchange($settings);
		#print_r($twitter);
	$json = $twitter->setGetfield($getfield)
	->buildOauth($url, $requestMethod)
	->performRequest();

	$obj = json_decode($json);
	#$obj = $obj[0];
	$retweet_count = $obj->retweet_count;
	$favorite_count = $obj->favorite_count;
	#uncomment this if you want to see the full tweet object that gets returned from twitter. There is some other cool stuff you may want to grab?? Just throwing it out there
	#echo "<pre>";
	#print_r($obj);
	#echo "</pre>";
	#echo $retweet_count." retweets.<br>";
	#echo $favorite_count. " favorites.";


	return array("retweet_count"=>$retweet_count,"favorite_count"=>$favorite_count);
	
	
}

function update_tweet_data(){
	#this is a rate limited request. 15 request, in a 15 minute window. So, this cron should be set to 1 per minute or less to prevent Twitter from throttling your account.
	#https://dev.twitter.com/rest/public/rate-limiting
	#it is per application, so if each user has a different app, that means we can do 1 per minute, per user without getting limited.
	#set this cron to run ever 1.5 minutes to be safe.
	#this updates the next tweet in line, for every account on the system.
	#if all tweets have been updated within the time constraints, then it will reset the loop and start back over. As you add more and more accounts, and tweet more, it could be difficult to reliably determine
	#these stats. Perhaps, your guys can figure something out long term, but this will work for now and there are 1,440 minutes in a day.
	#so each account can have upto 1,440 tweets using this current method, before a new solution is needed. The only reason this is a problem, is the rate limit set by Twitter.

	#using the tweet table
	$db = new Db_model();

	$tweet_data = $db->read("*","tweet","1=1"); #may need to optimize this to be more specific to time that is relevant to your application, for now it grabs everything
	#print_r($tweet_data);
	foreach($tweet_data as $tweet){
		//update the tweet data if it has not been checked in the last 24 hours ++ this number could be extended to provide more tweets to check
		$now = time();
		$last_checked = $tweet->last_checked;
		$last_checked_plus_one_day = strtotime('+1 day', $last_checked);
		if($last_checked_plus_one_day < $now){
			//then we need to update the count
			$tweet_results = get_tweet_results($tweet->handle,$tweet->twitter_tweet_id);
			$tweet_results['last_checked'] = $now;
			
			#echo "<pre>";
			#print_r($tweet_results);
			#echo "</pre>";


			//update the tweet
			#echo $tweet->tweet_id;
			$update = $db->update('tweet', $tweet_results, array('tweet_id', $tweet->tweet_id));

			if($update == false){
				//log error
				//echo json_encode(array('status'=>'error','tweet_id'=>$tweet->tweet_id));
			}

		}else{
			#these are the tweets that are being skipped, because we've already updated them today. So, they are considered "current" tweets.
			$passed_tweets['current_tweets'][] = $tweet;
		}
		
	}

	/*
	Uncomment this if you want to see what tweets are being considered current
		if(count($passed_tweets) > 0){
			echo "<pre>";
			print_r($passed_tweets);
			echo "</pre>";
		}	
	*/
/*
}
*/
/*
function calc_total_stats(){
//updates account data to reflect all current favorited tweet counts and retweeted counts
	$db = new Db_model();

	$accounts = get_accounts();
	foreach($accounts as $account){
		//get the handle, and update by handle
		$handle = $account->handle;
		$total_rewtweet_count_sql = "SELECT SUM(`retweet_count`) AS total FROM `tweet` WHERE `handle` = '$handle'";
		$retweet_count_query = $db->query($total_rewtweet_count_sql);

		$total_favorite_count_sql = "SELECT SUM(`favorite_count`) AS total FROM `tweet` WHERE `handle` = '$handle'";
		$favorite_count_query = $db->query($total_favorite_count_sql);
		//update the db
		
		$retweet_count = $retweet_count_query[0]->total;
		$favorite_count = $retweet_count_query[0]->total;

		$follower_count = get_follower_count($handle);

		$now = date('M d Y')." at ".date('h:i:s a');


		$update_stats = $db->update('twitter_account',array('total_retweets'=>$retweet_count,'total_favorites'=>$favorite_count,'follower_count'=>$follower_count,'last_updated'=>$now),array('handle',$handle));
		if($update_stats == false){
			//echo json_encode(array('status'=>'error','message'=>"There was a problem updating the stats."));
		}
	}

}
*/


//check to make sure its a function and run if it is
if(function_exists($action)){
	$action();
}else{
	//echo "Invalid Request!";
}


