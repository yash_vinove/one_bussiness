<?php session_start(); ?>
<?php include('config.php'); ?>
<?php include('sessionconfig.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	
 	<title>OneBusiness</title>

 	<!-- Bootstrap -->
 	<link href="bootstrap-3.3.1/dist/css/bootstrap.css" rel="stylesheet">

 	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
 	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">   
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
	<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script type="text/javascript">
    function redirectMe (sel) {
        var url = sel[sel.selectedIndex].value;
        window.location = url;
    }
    </script>
</head>
  
<body class='greybody' style=''>
	<?php include_once ('headerthree.php');	?>
	
	
	<?php
	//work out if the user has submitted something
	$submit = isset($_POST['submit']) ? $_POST['submit'] : ''; 
	//get the type of page e.g. currency
   	$pagetype = isset($_GET['pagetype']) ? $_GET['pagetype'] : '';
   	
   	//get additional information on the structurefunction based on the type of page
	$getpagedata = mysql_query("select * from structurefunction where structurefunction.tablename = '$pagetype'");
	$pagedatarow = mysql_fetch_array($getpagedata);
	$functionname = $pagedatarow['functionname'];	   	
	$gridorderfield = $pagedatarow['gridorderfield'];	   	
	$gridorderdirection = $pagedatarow['gridorderdirection'];	   	
	$tablename = $pagedatarow['tablename'];	   	
	$datefilterfield = $pagedatarow['datefilterfield'];	   	
	$searchfilterfieldexternaltable = $pagedatarow['searchfilterfieldexternaltable'];	  
	$uploaderactive = $pagedatarow['uploaderactive'];	  
	$gridhelptext = $pagedatarow['gridhelptext'];	  
	
   
	$structurefunctionid = $pagedatarow['structurefunctionid'];	   	
	$structurefunctionsectionid = $pagedatarow['structurefunctionsectionid'];	   	
	//check permissions to page
	//check permissions to section
	$getpermission = mysql_query("select * from userrole 
	inner join permissionrolestructurefunctionsection on permissionrolestructurefunctionsection.permissionroleid = userrole.permissionroleid
	where permissionrolestructurefunctionsection.disabled = '0' and userid = '$userid' and structurefunctionsectionid = '$structurefunctionsectionid'");
	//check permissions to pages inside section
	$getpermission2 = mysql_query("select * from userrole 
	inner join permissionrolestructurefunction on permissionrolestructurefunction.permissionroleid = userrole.permissionroleid
	where permissionrolestructurefunction.disabled = '0' and userid = '$userid' and structurefunctionid = '$structurefunctionid'");
	if(mysql_num_rows($getpermission)>0 || mysql_num_rows($getpermission2)>0) {	
	
	}
	else {
		echo "You do not have permission to this page.";
		die;	
	}					   
   ?>
   
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:10px;">
		<!--search results header bar for desktop-->
		<div style="padding:10px;background-color:#e9e9e9;border-bottom:1px solid #A0A0A0;">
			<b><?php echo $functionname ?></b>
		</div>
		
		<?php	
	   	//create 100 variables ready for the columns needed for the grid
	   	for($i = 0; $i <= 100; $i++) {
  			${"fieldname$i"} = "";
		}
	   	for($i = 0; $i <= 100; $i++) {
  			${"displayname$i"} = "";
		}
		for($i = 0; $i <= 100; $i++) {
  			${"structurefieldtypeid$i"} = "";
		}
		for($i = 0; $i <= 100; $i++) {
  			${"searchparam$i"} = "";
		}	
	   	
	   	//get other variables if data has been updated
	   	$action = isset($_POST['action']) ? $_POST['action'] : '';
	   	$rowid = isset($_POST['rowid']) ? $_POST['rowid'] : '';
	   	$updated = isset($_GET['updated']) ? $_GET['updated'] : '';
	   
	   	
	   	//if the user has submitted a search then set the variables for the search, otherwise just create empty variables
		$submitsearch = isset($_POST['submitsearch']) ? $_POST['submitsearch'] : '';
	   if(!isset($_POST['submitsearch'])) {
			$startdate = '';
			$enddate = '';
			$searchterm = '';
		} 	
		else {
			$startdate = isset($_POST['startdate']) ? $_POST['startdate'] : '';
	   		$enddate = isset($_POST['enddate']) ? $_POST['enddate'] : '';			
	   		$searchterm = isset($_POST['searchterm']) ? $_POST['searchterm'] : '';			
		}
	
		?>			
		
		
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:10px;background-color:#ffffff;border-bottom:1px solid #A0A0A0;">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			
			<?php 
			//if user has disabled/enabled a record then show that the status has updated	  		
	  		if(isset($updated) && $updated == 1) {
				echo "<br/><p class='bg-success'>The status has been updated.</p>";
			} 
			?>
			</div>
			
			<?php
			echo $gridhelptext."<br/><br/>";
			?>
		
				<a class="btn btn-primary" href='pageadd.php?pagetype=<?php echo $pagetype ?>'>Add New <?php echo $functionname ?></a>
				
				<?php
				if($uploaderactive == 1){
					?> 
					<a class="btn btn-primary" href='pagegridupload.php?pagetype=<?php echo $pagetype ?>'>Bulk Upload <?php echo $functionname ?></a>
					<?php
				}				
				?>
				<br/><br/>
				<script>
				$(document).ready(function(){
				    $('#myTable2').dataTable({
				    	"searching": false
				 });
				});
				</script>
			<!--grid-->
				
				
				<form class="form-horizontal" action='pagegrid.php?pagetype=<?php echo $pagetype?>' method="post">
					<div class="form-group">
						<label for="Search Term" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label">Search Term</label>
				    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
				      	<input type="text" class="form-control" name="searchterm" value="<?php echo $searchterm ?>">
				    	</div>
				  		<label for="Start Date" class="col-xs-12 col-sm-1 col-md-1 col-lg-1 control-label">Start Date</label>
				    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
				      	<input type="date" class="form-control" name="startdate" value="<?php echo $startdate ?>">
				    	</div>
				  		<label for="End Date" class="col-xs-12 col-sm-1 col-md-1 col-lg-1 control-label">End Date</label>
				    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
				      	<input type="date" class="form-control" name="enddate" value="<?php echo $enddate ?>">
				    	</div>
				  	  	<div class="col-md-1 col-lg-1 col-sm-1 col-xs-1">
				    		<div style="text-align:center;">
				      		<button type='submit' name='submitsearch' value='Submit' class="btn btn-primary">Submit</button>																									
				    		</div>
				    	</div>		
					</div>
		    	</form>		
							
					
				<?php
				if ($db_found) {
					//here we get the column field data for the grid and assign the data to variables
					$gridcontents = mysql_query("select * from structurefield where structurefunctionid = '$structurefunctionid' and disabled=0
					order by gridposition asc");
					$fieldid = 1;
					while($gridcontentsrow=mysql_fetch_array($gridcontents)){
						$fieldname = $gridcontentsrow['fieldname'];
						$displayname = $gridcontentsrow['displayname'];
						$structurefieldtypeid = $gridcontentsrow['structurefieldtypeid'];
						//echo $fieldid." - ".$fieldname."<br/>";
						${'fieldname'.$fieldid} = $fieldname;
						${'displayname'.$fieldid} = $displayname;
						${'structurefieldtypeid'.$fieldid} = $structurefieldtypeid;
						$fieldid = $fieldid +1;
					}			
					$countoffields = $fieldid - 1;
					//echo $countoffields;
					$stringconstruct = 1;
					$fieldstring = "";
					//build a string of all of the columns in order to get ready to query the table containing the data
					while($stringconstruct<= $countoffields) {
						$fieldstring = 	$fieldstring.$tablename.".".${'fieldname'.$stringconstruct};		
						if($stringconstruct <> $countoffields){
							$fieldstring = $fieldstring.", ";						
						}	
						$stringconstruct = $stringconstruct + 1;
					}			
					
					//echo $fieldstring;		
					//query the data table in order to get the actual data
					$querybuilder = "select ".$fieldstring." from ".$tablename;
					$searchfilterfieldexternaltable = str_replace("####", $searchterm, $searchfilterfieldexternaltable); 	
					//if the user has run a search then also add the search terms to the query for the data
				   if(isset($_POST['submitsearch'])) {
				   		if($searchfilterfieldexternaltable <> "") {
				   			$querybuilder = $querybuilder." ".$searchfilterfieldexternaltable." and ";
				   		}
				   		else {
				   			$querybuilder = $querybuilder." where ";
				   		}
				   		//add date ranges to the query if user has searched for a date
					   if($startdate <> '' && $enddate <> '') {
							$querybuilder = $querybuilder.$tablename.".".$datefilterfield.">='".$startdate."' and ".$tablename.".".$datefilterfield."<='".$enddate."'";	
						}
						if($startdate <>'' && $enddate =='') {
							$querybuilder = $querybuilder.$tablename.".".$datefilterfield.">='".$startdate."'";	
						}
						if($startdate =='' && $enddate <>'') {
							$querybuilder = $querybuilder.$tablename.".".$datefilterfield."<='".$enddate."'";	
						}
						if($startdate <> '' || $enddate <> '') {
							$querybuilder = $querybuilder." and (";
						} 
						else {
							$querybuilder = $querybuilder." (";
						}
						//add search term to the query if user has searched for a search term
						$querybuilder = rtrim($querybuilder, ' or '); 
						$querybuilder = $querybuilder.")";
						$querybuilder = rtrim($querybuilder, '()');
						$querybuilder = rtrim($querybuilder, ' and ');
					}
					//finalise the query contents
					$querybuilder = $querybuilder." order by ".$gridorderfield." ".$gridorderdirection;
			     	$query = $querybuilder;
			     	
			     	//echo $query;
			      //run the query
			      	$result = mysql_query($query);
			      echo "<div style='padding:0px;'>";
			      	//echo "<h4>".$functionname."</h4>";
			      	
			      	//$csv_hdr = $fieldname1.", ".$fieldname2.", ";
			      	
			      	//assign the column headers to the spreadsheet export
			      	$counter4 = 1;
			      	$csv_hdr = "";
	        		while($counter4 <= $countoffields) {
	        			$value = ${'displayname'.$counter4};
	        			$csv_hdr .= $value;
	            		if($counter4 <> $countoffields){ 
	            			$csv_hdr .= ",";
	            		}
	            		$counter4 = $counter4 +1;	            		
	        		}
			      	
			      	//write the column headers into the grid
			      	$csv_output ="";
			      	if(mysql_num_rows($result)>=0){
			         	echo "<div class='table-responsive'><table id='myTable2' class='table table-bordered'>";
			         	echo "<thead><tr>";
			        	echo "<th>Actions</th>";
			        	$counter1 = 1;
			        	while($counter1 <= $countoffields) {
			        		$checkgridshow = mysql_query("select * from structurefield where structurefunctionid = '$structurefunctionid' and fieldname = '${'fieldname'.$counter1}'");
			        		$checkgridshowrow = mysql_fetch_array($checkgridshow);
			        		$showingrid = $checkgridshowrow['showingrid'];
			        		if($showingrid == 1) {
			        			echo "<th>".${'displayname'.$counter1}."</th>";
			        		}
			        		$counter1 = $counter1 +1;
			        	}
			       	echo "</thead>";
			       	//write the data into the spreadsheet
			         	while($row=mysql_fetch_array($result)){
			            	$rowid = $row[$fieldname1];
			            	$counter3 = 1;
			        		while($counter3 <= $countoffields) {
			        			//find out what type of field the column is so we know how to show the data
			        			$value = ${'fieldname'.$counter3};
			        			$structurefieldtypeid = ${'structurefieldtypeid'.$counter3};
			        			$checkstructurefieldtype = mysql_query("select * from structurefieldtype where structurefieldtypeid = '$structurefieldtypeid'");
			        			$rowcheckstructurefieldtype = mysql_fetch_array($checkstructurefieldtype);
			        			$fieldtypename = $rowcheckstructurefieldtype['fieldtypename'];
			        			if($fieldtypename == 'Status') {
									//if field is status type then correct formatting to Active or Inactive				            		
				            		if($row[$value] == 1) {
										$csv_output .= "Inactive";				            		
				            		}
				            		else {
				            			$csv_output .= "Active";		
				            		}
				            	}
				            //if the data type is Table Connect then run another query to get the actual content from the associated table so you show the name instead of the id
				            	if($fieldtypename == 'Table Connect') {
									//if field is table connect type then get the content of the related table				            		
				            		$checkgridshow = mysql_query("select * from structurefield where structurefunctionid = '$structurefunctionid' and fieldname = '${'fieldname'.$counter3}'");
				        			$checkgridshowrow = mysql_fetch_array($checkgridshow);
				        			$tableconnect = $checkgridshowrow['tableconnect'];
				        			$tablefieldconnect = $checkgridshowrow['tablefieldconnect'];
				        			$querybuild = "select * from ".$tableconnect." where ".$tableconnect."id = '".$row[$value]."'";
									$gettableconnectrecord = mysql_query($querybuild); 
									$gettableconnectrecordrow =	mysql_fetch_array($gettableconnectrecord);	
									$value = $gettableconnectrecordrow[$tablefieldconnect];	
									$csv_output .= $value;	            		
				            	}
				            //if the data type is text then just write it
					         	if($fieldtypename == 'Text') {
									//if field is text type then just show the content				            		
				            		$csv_output .= $row[$value];
				            	}
				            //if the data type is Date Select then just write it
				            	if($fieldtypename == 'Date Select') {
									//if field is text type then just show the content				            		
				            		$csv_output .= $row[$value];
				            	}
				            //if the data type is Value then just write it
				            	if($fieldtypename == 'Value') {
									//if field is text type then just show the content				            		
				            		$csv_output .= $row[$value];
				            	}
				            //if the data type is Unique Table id then just write it
				            	if($fieldtypename == 'Unique TableID') {
									//if field is unique tableid type then just show the content				            		
				            		$csv_output .= $row[$value];
				            	}
			        			
			        			//add a new row to the spreadsheet
			        			if($counter3 <> $countoffields){ 
			            			$csv_output .= ",";
			            		}
			            		else {
			            			$csv_output .= "\n";
			            		}
				            	$counter3 = $counter3 +1;	            		
			        		}
			            	echo "<tr>";
		        			echo "<td>";
		        			?>
		        			<select class='form-control' name='action' id='action' onchange='redirectMe(this);'>";
			            	<?php
			            	echo "<option value=''>Select Action</option>";
			            	echo "<option value='pageedit.php?pagetype=".$pagetype."&rowid=".$rowid."'>Edit</option>";
			            	$inactive = $row['disabled'];
			            	//echo $inactive;
			            	if($inactive == '0') {
								echo "<option value='pagechangestatus.php?pagetype=".$pagetype."&rowid=".$rowid."&action=deactivate'>Deactivate</option>";
			            	} 
			            	else {
			            		echo "<option value='pagechangestatus.php?pagetype=".$pagetype."&rowid=".$rowid."&action=activate'>Activate</option>";
			            	}			            	
			            	echo "</td>";
			            	$counter2 = 1;
			        		while($counter2 <= $countoffields) {
			        			//find out what type of field the column is so we know how to show the data
			        			$value = ${'fieldname'.$counter2};
			        			$structurefieldtypeid = ${'structurefieldtypeid'.$counter2};
			        			$checkstructurefieldtype = mysql_query("select * from structurefieldtype where structurefieldtypeid = '$structurefieldtypeid'");
			        			$rowcheckstructurefieldtype = mysql_fetch_array($checkstructurefieldtype);
			        			$fieldtypename = $rowcheckstructurefieldtype['fieldtypename'];
								$checkgridshow = mysql_query("select * from structurefield where structurefunctionid = '$structurefunctionid' and fieldname = '${'fieldname'.$counter2}'");
				        		$checkgridshowrow = mysql_fetch_array($checkgridshow);
				        		$showingrid = $checkgridshowrow['showingrid'];
				        		$tableconnect = $checkgridshowrow['tableconnect'];
				        		$tablefieldconnect = $checkgridshowrow['tablefieldconnect'];
				        		if($showingrid == 1) {
			        				//if field is status type then correct formatting to Active or Inactive				            		
				            		if($fieldtypename == 'Status') {
										//if field is status type then correct formatting to Active or Inactive				            		
					            		if($row[$value] == 1) {
											echo "<td>Inactive</td>";				            		
					            		}
					            		else {
					            			echo "<td>Active</td>";
					            		}
					            	}
					            	//if the data type is Table Connect then run another query to get the actual content from the associated table so you show the name instead of the id
				            		if($fieldtypename == 'Table Connect') {
										//if field is table connect type then get the content of the related table				            		
					            		$querybuild = "select * from ".$tableconnect." where ".$tableconnect."id = '".$row[$value]."'";
										//echo $querybuild;
										$gettableconnectrecord = mysql_query($querybuild); 
										$gettableconnectrecordrow =	mysql_fetch_array($gettableconnectrecord);	
										$value = $gettableconnectrecordrow[$tablefieldconnect];	
										echo "<td>".$value."</td>";	            		
					            	}
					            //if the data type is text then just write it
					            	if($fieldtypename == 'Text') {
										//if field is text type then just show the content				            		
					            		echo "<td>".$row[$value]."</td>";
					            	}
					            //if the data type is Date Select then just write it
					            	if($fieldtypename == 'Date Select') {
										//if field is text type then just show the content				            		
					            		echo "<td>".$row[$value]."</td>";
					            	}
					            //if the data type is Value then just write it
					            	if($fieldtypename == 'Value') {
										//if field is text type then just show the content				            		
					            		echo "<td>".$row[$value]."</td>";
					            	}
					            //if the data type is Unique Table ID then just write it
					            	if($fieldtypename == 'Unique TableID') {
										//if field is unique tableid type then just show the content				            		
					            		echo "<td>".$row[$value]."</td>";
					            	}
			        			}
			        			$counter2 = $counter2 +1;
			        		}
			            	echo "</tr>";
	            		}
	      	    		echo "</table></div>";
	          	}
				}
				else {
					print "Database NOT Found ";
					mysql_close($db_handle);
				}
				if(mysql_num_rows($result) >= 1) {
					//show the spreadsheet export button
				?>
				
				<form name="export" action="export.php" method="post">
				    <input class="btn btn-primary" type="submit" value="Export">
				    <input type="hidden" value='<?php echo $csv_hdr; ?>' name='csv_hdr'>
				    <input type="hidden" value='<?php echo $csv_output; ?>' name='csv_output'>
				</form>
				<?php 
				echo "</div>";
				}
				?>
			
			<br/><br/><br/>				
			
			</div>		
		<br/><br/><br/><br/><br/><br/>
		</div>	
		
	</div>	
	<?php include_once ('footer.php'); ?>
</body>
