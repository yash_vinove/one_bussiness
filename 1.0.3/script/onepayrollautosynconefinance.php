<?php 

echo "<br/><br/>";

$date = date("Y-m-d");
$dateplus5 = date('Y-m-d', strtotime("+ 5 days"));

$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$getconfig = mysql_query("select * from payrollconfiguration");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get configuration settings: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
while($getconfigrow = mysql_fetch_array($getconfig)){
	$allowautosynctoonefinance = $getconfigrow['allowautosynctoonefinance'];	$importpayrollpaymentstatusid = $getconfigrow['importpayrollpaymentstatusid'];	$businessunitid = $getconfigrow['businessunitid'];	$defaultfinancialtransactionstatusid = $getconfigrow['defaultfinancialtransactionstatusid'];	$defaultfinancialaccountid = $getconfigrow['defaultfinancialaccountid'];
	if($allowautosynctoonefinance == 1){
		echo "<br/><br/><b>BUSINESS UNIT: ".$businessunitid."</b>";
		
		//ADD NEW MISSING RECORDS
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$getdata = mysql_query("select * from payrollpayment 
		inner join employee on employee.employeeid = payrollpayment.employeeid
		where payrollpaymentstatusid = $importpayrollpaymentstatusid and financialtransactionid = 0");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Get records that have not been setup yet: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		$numrecords = mysql_num_rows($getdata);
		echo "<br/><br/><br/>NUMBER OF NEW RECORDS TO PROCESS: ".$numrecords;
		if($numrecords >= 1){
			while($getdatarow = mysql_fetch_array($getdata)){
				$payrollpaymentid = $getdatarow['payrollpaymentid'];			
				$currencyid = $getdatarow['currencyid'];			
				$paiddate = $getdatarow['paiddate'];			
				$amount = $getdatarow['amount'];			
				$financesubcategoryid = $getdatarow['financesubcategoryid'];			
				$employeename = $getdatarow['employeename'];			
				echo "<br/><br/>Payroll Payment ID: ".$payrollpaymentid." - ".$currencyid." ".$amount;
				
				//get financialbudgetperiodid	
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$getfinancialbudgetperiod = mysql_query("select * from financialbudgetperiod 
				where startdate <= '$paiddate' and enddate >= '$paiddate'");
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Get financialbudgetperiod record: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
				if(mysql_num_rows($getfinancialbudgetperiod) >= 1){
					$getfinancialbudgetperiodrow = mysql_fetch_array($getfinancialbudgetperiod);
					$financialbudgetperiodid = $getfinancialbudgetperiodrow['financialbudgetperiodid'];
				
					//add new record
					$insertquery = "insert into financialtransaction (financialtransactionname, disabled, datecreated, masteronly, 
					businessunitid, financesubcategoryid, amount, amountincludingtax, transactiondate, financialbudgetperiodid, financialtransactionstatusid, 
					currencyid, financialaccountid) values ";
					$financialtransactionname = mysql_real_escape_string("Payroll Payment - ".$employeename);
					$insertquery = $insertquery."('$financialtransactionname', '0', '$date', '0', '$businessunitid', '$financesubcategoryid', 
					'$amount', '$amount', '$paiddate', '$financialbudgetperiodid', '$defaultfinancialtransactionstatusid', '$currencyid', 
					'$defaultfinancialaccountid')";
					//echo "<br/><br/>insertquery: ".$insertquery;
					$nosqlqueries = $nosqlqueries + 1;
					$sqlstarttime = microtime(true);
					$insertquery = mysql_query($insertquery);
					$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
					$differencemilliseconds = microtime(true) - $sqlstarttime;
					echo "<br/>Insert new payroll payment records: ";
					echo "<br/>differencemilliseconds: ".$differencemilliseconds;
					echo "<br/>sqlqueriestime: ".$sqlqueriestime;
					$financialtransactionid = mysql_insert_id();
					
					$nosqlqueries = $nosqlqueries + 1;
					$sqlstarttime = microtime(true);
					$updatequery = mysql_query("update payrollpayment set financialtransactionid = $financialtransactionid 
					where payrollpaymentid = $payrollpaymentid");
					$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
					$differencemilliseconds = microtime(true) - $sqlstarttime;
					echo "<br/>Update payrollpayment with financialtransactionid: ";
					echo "<br/>differencemilliseconds: ".$differencemilliseconds;
					echo "<br/>sqlqueriestime: ".$sqlqueriestime;
				}	
			}		
		}
		
		//CHECK FOR CHANGES IN EXISTING RECORDS
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$getdata = mysql_query("select payrollpayment.currencyid, payrollpayment.amount, payrollpayment.financialtransactionid, 
		payrollpaymentid from payrollpayment 
		inner join financialtransaction on financialtransaction.financialtransactionid = payrollpayment.financialtransactionid 
		where payrollpayment.currencyid <> financialtransaction.currencyid or payrollpayment.amount <> financialtransaction.amount");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Get records that have changed currency or amount: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		$numrecords = mysql_num_rows($getdata);
		echo "<br/><br/><br/>NUMBER OF CHANGES TO PROCESS: ".$numrecords;
		if($numrecords >= 1){
			while($getdatarow = mysql_fetch_array($getdata)){
				$payrollpaymentid = $getdatarow['payrollpaymentid'];			
				$financialtransactionid = $getdatarow['financialtransactionid'];			
				$currencyid = $getdatarow['currencyid'];			
				$amount = $getdatarow['amount'];			
				
				echo "<br/><br/>Payroll Payment ID: ".$payrollpaymentid." - ".$currencyid." ".$amount;
				
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$updatequery = mysql_query("update financialtransaction 
				set currencyid = $currencyid, amount = $amount 
				where financialtransactionid = $financialtransactionid");
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Update financialtransaction with the new values from payrollpayment: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		
			}
		}
		
		
	}
}

echo "<br/><br/>";

?>