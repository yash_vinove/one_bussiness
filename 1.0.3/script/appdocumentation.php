<?php

$appid = isset($_GET['appid']) ? $_GET['appid'] : '';
$sectionname = isset($_GET['sectionname']) ? $_GET['sectionname'] : '';
$structurefunctionpageid = isset($_GET['structurefunctionpageid']) ? $_GET['structurefunctionpageid'] : '';



if($appid<1){
	
	$selectapp = mysql_query("select * from $masterdatabase.appsetupapplication 
	where disabled = 0 order by appsetupapplicationid asc");
	echo "<br/>Here you will find a full list of all of the default Apps available in the system. You can read a little about them, and then view the documentation to see how they work. You can also use the documentation to train your team on the functionality. Please note, that all documentation is based on the default setup of the App - any custom changes you make to the App will not be included in the documentation - you can however download a document version and make your own updates.";
	echo "<br/><br/>";
	echo "<table class='table table-bordered' style='width:100%;'>";
	while ($app = mysql_fetch_array($selectapp)){
		$appname = $app['appsetupapplicationname'];
		$appdocumentationshortdescription = $app['appdocumentationshortdescription'];
		$appid = $app['appsetupapplicationid'];
		$url = "view.php?viewid=8&appid=".$appid;
		echo "<tr>";
		echo "<td>".$appname."</td>";
		echo "<td>".$appdocumentationshortdescription."</td>";
		echo "<td><a href='".$url."'>Documentation</td>";
		echo "</tr>";
	}
	echo "</table>";
	
}
else {
	//decide where to show documentation from
	//check if onecustomerconfig table exists in the database
	$checktable = mysql_query("SELECT table_name
	FROM information_schema.tables
	WHERE table_schema = '$database'
	AND table_name = 'appsetupapplicationbusiness'");
	if(mysql_num_rows($checktable)>= 1){
		$getapp = mysql_query("select * from $database.appsetupapplicationbusiness 
		where appsetupapplicationid = '$appid'");
		if(mysql_num_rows($getapp)>= 1){
			$docdatabase = $database;	
		}
		else{
			$docdatabase = $masterdatabase;	
		}
	}
	else {
		$docdatabase = $masterdatabase;	
	}

	//get app details
	$selectapp = mysql_query("select * from $masterdatabase.appsetupapplication 
	where appsetupapplicationid = '$appid'");
	$approw = mysql_fetch_array($selectapp);
	$businessgridpages = $approw['businessgridpages'];
	$businessviews = $approw['businessviews'];
	$businesspermissionroles = $approw['businesspermissionroles'];
	$businessreportsections = $approw['businessreportsections'];
	$businessdashboardsections = $approw['businessdashboardsections'];
	$installationguide = $approw['appdocumentationinstallationguide'];

	
	echo "<div style='min-height:3000px;'>";
	
	//Contents
	echo "<table class='table table-bordered' style='margin-right:10px;width:25%;position:relative;float:left;'>";
	echo "<tr><td><a href='view.php?viewid=8&appid=".$appid."&sectionname=introduction'>Introduction</a></td></tr>";
	echo "<tr><td><a href='view.php?viewid=8&appid=".$appid."&sectionname=launchguide'>Launch/Configuration Guide</a></td></tr>";
	echo "<tr><td><a href='view.php?viewid=8&appid=".$appid."&sectionname=userrolepermissions'>User Role Permissions</a></td></tr>";
	echo "<tr><td><a href='view.php?viewid=8&appid=".$appid."&sectionname=pagesummary'>Page Summary</a></td></tr>";
	echo "<tr><td><a href='view.php?viewid=8&appid=".$appid."&sectionname=apisummary'>API Summary</a></td></tr>";
	if($businessgridpages <> ""){
		echo "<tr><td>Data Pages";
		$array = explode(',', $businessgridpages); //split string into array seperated by ', '
		foreach($array as $value) //loop over values
		{
			$getstructurefunctionpage = mysql_query("select * from $docdatabase.structurefunctionpage 
			inner join $docdatabase.structurefunctionsection on structurefunctionsection.structurefunctionsectionid = structurefunctionpage.structurefunctionsectionid
			where structurefunctionid = '$value'");
			while($row23 = mysql_fetch_array($getstructurefunctionpage)){
				$structurefunctionpagename = $row23['structurefunctionpagename'];
				$structurefunctionpageid = $row23['structurefunctionpageid'];
				echo "<br/> - <a href='view.php?viewid=8&appid=".$appid;
				echo "&sectionname=pagegrid&structurefunctionpageid=".$structurefunctionpageid."'>";
				echo $structurefunctionpagename."</a>";
			}
		}
		echo "</td></tr>";
	}
	
	if($businessviews <> ""){
		echo "<tr><td>Views";
		$array = explode(',', $businessviews); //split string into array seperated by ', '
		foreach($array as $value) //loop over values
		{
			$getstructurefunctionpage = mysql_query("select * from $docdatabase.structurefunctionpage 
			inner join $docdatabase.structurefunctionsection on structurefunctionsection.structurefunctionsectionid = structurefunctionpage.structurefunctionsectionid
			where viewid = '$value'");
			while($row23 = mysql_fetch_array($getstructurefunctionpage)){
				$structurefunctionpagename = $row23['structurefunctionpagename'];
				$structurefunctionpageid = $row23['structurefunctionpageid'];
				echo "<br/> - <a href='view.php?viewid=8&appid=".$appid;
				echo "&sectionname=view&structurefunctionpageid=".$structurefunctionpageid."'>";
				echo $structurefunctionpagename."</a>";
			}
		}
		echo "</td></tr>";
	}
	
	if($businessdashboardsections <> ""){
		echo "<tr><td><a href='view.php?viewid=8&appid=".$appid."&sectionname=dashboards'>Dashboards</a></td></tr>";
	}	
	if($businessreportsections <> ""){
		echo "<tr><td><a href='view.php?viewid=8&appid=".$appid."&sectionname=reports'>Reports</a></td></tr>";
	}	
	echo "</table>";
	
	
	
	echo "<table class='table table-bordered' style='margin:10px;width:72%;''><tr><td>";
	
	
	//Page 1 - Introduction
	if($sectionname == 'introduction' || $sectionname == ''){
		$appname = $approw['appsetupapplicationname'];
		$appdocumentationintroduction = $approw['appdocumentationintroduction'];
		$appdocumentationtermsofuse = $approw['appdocumentationtermsofuse'];
		echo "<h2>".$appname." Introduction</h2>";
		echo "<p>".$appdocumentationintroduction."</p><br/>";
		echo "<h2>Documentation Terms of Use</h2>";
		echo "<p>".$appdocumentationtermsofuse."</p><br/>";
	}
	
	//Page 3 - launchguide
	if($sectionname == 'launchguide'){
		echo "<h2>Launch/Configuration Guide</h2>";
		echo $installationguide;
	}
	
	//Page 4 - userrolepermissions
	if($sectionname == 'userrolepermissions'){
		echo "<h2>User Role Permissions</h2>";
		echo "<p>The list below provides you with an overview of each of the standard user roles within OneBusiness.</p>";
		echo "<p>You can change the permissions for each of these roles by going to the Users section in the main navigation, and assigning/removing additional permissions.</p>";
		echo "<table class='table table-bordered' style='width:100%;''>";
		echo "<thead><th style='width:20%;'>Role Name</th><th style='width:40%;'>Role Description</th><th style='width:40%;'>Features Accessed by Default</th></thead>";
		//get businessgridpages
		$array = explode(',', $businesspermissionroles); //split string into array seperated by ', '
		foreach($array as $value) //loop over values
		{
			$getpermissionrole = mysql_query("select * from $docdatabase.permissionrole
			where permissionroleid = '$value'");
			while($row23 = mysql_fetch_array($getpermissionrole)){
				$permissionrolename = $row23['permissionrolename'];
				$helpdocumentation = $row23['helpdocumentation'];
				echo "<tr><td>".$permissionrolename."</td>";
				echo "<td>".$helpdocumentation."</td>";
				
				$features = "";
				//calculate features - page section
				$getperm = mysql_query("select * from $docdatabase.permissionrolestructurefunctionsection
				inner join $docdatabase.structurefunctionsection on structurefunctionsection.structurefunctionsectionid = permissionrolestructurefunctionsection.structurefunctionsectionid
				where permissionroleid = '$value'");
				if(mysql_num_rows($getperm)>= 1){
					$features = $features."All pages within these Main Navigation Sections:<br/><ul>";	
					while($row = mysql_fetch_array($getperm)){
						$featurename = $row['structurefunctionsectionname'];
						$features = $features."<li>".$featurename."</li>";
					}
					$features = $features."</ul>";			
				}
				
				//calculate features - pages
				$getperm = mysql_query("select * from $docdatabase.permissionrolestructurefunctionpage
				inner join $docdatabase.structurefunctionpage on structurefunctionpage.structurefunctionpageid = permissionrolestructurefunctionpage.structurefunctionpageid
				where permissionroleid = '$value'");
				if(mysql_num_rows($getperm)>= 1){
					$features = $features."All of these Pages:<br/><ul>";	
					while($row = mysql_fetch_array($getperm)){
						$featurename = $row['structurefunctionpagename'];
						$features = $features."<li>".$featurename."</li>";
					}
					$features = $features."</ul>";			
				}
				
				
				//calculate features - dashboard sections
				$getperm = mysql_query("select * from $docdatabase.permissionroledashboardsection
				inner join $docdatabase.chartsection on chartsection.chartsectionid = permissionroledashboardsection.chartsectionid
				where permissionroleid = '$value'");
				if(mysql_num_rows($getperm)>= 1){
					$features = $features."All charts within these Dashboard Sections:<br/><ul>";	
					while($row = mysql_fetch_array($getperm)){
						$featurename = $row['chartsectionname'];
						$features = $features."<li>".$featurename."</li>";
					}
					$features = $features."</ul>";			
				}
				
				//calculate features - report sections				
				$getperm = mysql_query("select * from $docdatabase.permissionrolereportsection
				inner join $docdatabase.reportsection on reportsection.reportsectionid = permissionrolereportsection.reportsectionid
				where permissionroleid = '$value'");
				if(mysql_num_rows($getperm)>= 1){
					$features = $features."All reports within these Report Sections:<br/><ul>";	
					while($row = mysql_fetch_array($getperm)){
						$featurename = $row['reportsectionname'];
						$features = $features."<li>".$featurename."</li>";
					}
					$features = $features."</ul>";			
				}
				
				
				echo "<td>".$features."</td></tr>";
			}
		}
		echo "</table>";
		
	}
	
	//Page 5 - pagesummary	
	if($sectionname == 'pagesummary'){		
		echo "<h2>Page Summary</h2>";
		echo "<p>The list below provides you with an overview of each of the pages within the application, and their core purpose. Click on the Page Name link in order to see further information on how to use the page.</p>";
		echo "<table class='table table-bordered' style='width:100%;''>";
		echo "<thead><th>Page Name</th><th>Navigation Section</th><th>Page Type</th><th>Page Description</th></thead>";
		if($businessgridpages <> ""){
			//get businessgridpages
			$array = explode(',', $businessgridpages); //split string into array seperated by ', '
			foreach($array as $value) //loop over values
			{
				$getstructurefunctionpage = mysql_query("select * from $docdatabase.structurefunctionpage 
				inner join $docdatabase.structurefunctionsection on structurefunctionsection.structurefunctionsectionid = structurefunctionpage.structurefunctionsectionid
				where structurefunctionid = '$value'");
				while($row23 = mysql_fetch_array($getstructurefunctionpage)){
					$structurefunctionpagename = $row23['structurefunctionpagename'];
					$structurefunctionpageid = $row23['structurefunctionpageid'];
					$documentationpagedescription = $row23['documentationpagedescription'];
					$structurefunctionsectionname = $row23['structurefunctionsectionname'];
					echo "<tr><td><a href='view.php?viewid=8&appid=".$appid;
					echo "&sectionname=pagegrid&structurefunctionpageid=".$structurefunctionpageid."'>";
					echo $structurefunctionpagename."</a></td><td>".$structurefunctionsectionname."</td>";
					echo "<td>Data Grid</td><td>".$documentationpagedescription."</td></tr>";
				}
			}
		}
		
		//get views
		if($businessviews <> ""){
			$array = explode(',', $businessviews); //split string into array seperated by ', '
			foreach($array as $value) //loop over values
			{
				$getstructurefunctionpage = mysql_query("select * from $docdatabase.structurefunctionpage 
				inner join $docdatabase.structurefunctionsection on structurefunctionsection.structurefunctionsectionid = structurefunctionpage.structurefunctionsectionid
				where viewid = '$value'");
				while($row23 = mysql_fetch_array($getstructurefunctionpage)){
					$structurefunctionpagename = $row23['structurefunctionpagename'];
					$structurefunctionpageid = $row23['structurefunctionpageid'];
					$documentationpagedescription = $row23['documentationpagedescription'];
					$structurefunctionsectionname = $row23['structurefunctionsectionname'];
					echo "<tr><td><a href='view.php?viewid=8&appid=".$appid;
					echo "&sectionname=view&structurefunctionpageid=".$structurefunctionpageid."'>";
					echo $structurefunctionpagename."</a></td><td>".$structurefunctionsectionname."</td>";
					echo "<td>View</td><td>".$documentationpagedescription."</td></tr>";
				}
			}

		}
		echo "</table>";
	}	
	
				
	//Page 5b - apisummary
	if($sectionname == 'apisummary'){
		echo "<h2>API Summary</h2>";
		echo "<p>The list below provides you with an overview of each of the scripts within the API connection, and their core purpose.</p><br/>";
		$getstructurefunctionpage = mysql_query("select * from $masterdatabase.systemapi 
		where apisetupapplicationid = '$appid'");
		while($row23 = mysql_fetch_array($getstructurefunctionpage)){
			
			$documentationapidescription = $row23['documentationapidescription'];
			$systemapiname = $row23['systemapiname'];
			echo "<h2>".$systemapiname."</h2>";
			echo "<p>".$documentationapidescription."</p>";
			echo "<table class='table table-bordered' style='width:100%;''>";
			$documentationscriptdescription1 = $row23['documentationscriptdescription1'];
			$scriptlocation1 = $row23['scriptlocation1'];
			$documentationscriptdescription2 = $row23['documentationscriptdescription2'];
			$scriptlocation2 = $row23['scriptlocation2'];
			$documentationscriptdescription3 = $row23['documentationscriptdescription3'];
			$scriptlocation3 = $row23['scriptlocation3'];
			$documentationscriptdescription4 = $row23['documentationscriptdescription4'];
			$scriptlocation4 = $row23['scriptlocation4'];
			$documentationscriptdescription5 = $row23['documentationscriptdescription5'];
			$scriptlocation5 = $row23['scriptlocation5'];
			$documentationscriptdescription6 = $row23['documentationscriptdescription6'];
			$scriptlocation6 = $row23['scriptlocation6'];
			$documentationscriptdescription7 = $row23['documentationscriptdescription7'];
			$scriptlocation7 = $row23['scriptlocation7'];
			$documentationscriptdescription8 = $row23['documentationscriptdescription8'];
			$scriptlocation8 = $row23['scriptlocation8'];
			$documentationscriptdescription9 = $row23['documentationscriptdescription9'];
			$scriptlocation9 = $row23['scriptlocation9'];
			$documentationscriptdescription10 = $row23['documentationscriptdescription10'];
			$scriptlocation10 = $row23['scriptlocation10'];
			if($documentationscriptdescription1 <> ''){
				echo "<thead><th>API Script</th><th>Script Description</th></thead>";
				echo "<tr><td>".$scriptlocation1."</td>";
				echo "<td>".$documentationscriptdescription1."</td></tr>";
				if($documentationscriptdescription2 <> ''){
					echo "<tr><td>".$scriptlocation2."</td>";
					echo "<td>".$documentationscriptdescription2."</td></tr>";
				}
				if($documentationscriptdescription3 <> ''){
					echo "<tr><td>".$scriptlocation3."</td>";
					echo "<td>".$documentationscriptdescription3."</td></tr>";
				}
				if($documentationscriptdescription4 <> ''){
					echo "<tr><td>".$scriptlocation4."</td>";
					echo "<td>".$documentationscriptdescription4."</td></tr>";
				}
				if($documentationscriptdescription5 <> ''){
					echo "<tr><td>".$scriptlocation5."</td>";
					echo "<td>".$documentationscriptdescription5."</td></tr>";
				}
				if($documentationscriptdescription6 <> ''){
					echo "<tr><td>".$scriptlocation6."</td>";
					echo "<td>".$documentationscriptdescription6."</td></tr>";
				}
				if($documentationscriptdescription7 <> ''){
					echo "<tr><td>".$scriptlocation7."</td>";
					echo "<td>".$documentationscriptdescription7."</td></tr>";
				}
				if($documentationscriptdescription8 <> ''){
					echo "<tr><td>".$scriptlocation8."</td>";
					echo "<td>".$documentationscriptdescription8."</td></tr>";
				}
				if($documentationscriptdescription9 <> ''){
					echo "<tr><td>".$scriptlocation9."</td>";
					echo "<td>".$documentationscriptdescription9."</td></tr>";
				}
				if($documentationscriptdescription10 <> ''){
					echo "<tr><td>".$scriptlocation10."</td>";
					echo "<td>".$documentationscriptdescription10."</td></tr>";
				}
				
			}	
			echo "</table>";
		}		
	}	
	
	//Page 5 - pagegrid
	if($sectionname == 'pagegrid'){
		$structurefunctionpageid = isset($_GET['structurefunctionpageid']) ? $_GET['structurefunctionpageid'] : '';
		$selectfunction = mysql_query("select * from $docdatabase.structurefunctionpage 
		inner join $docdatabase.structurefunction on structurefunction.structurefunctionid = structurefunctionpage.structurefunctionid
		inner join $docdatabase.structurefunctionsection on structurefunctionsection.structurefunctionsectionid = structurefunctionpage.structurefunctionsectionid
		where structurefunctionpage.structurefunctionpageid = '$structurefunctionpageid'");
		$functionrow = mysql_fetch_array($selectfunction);
		$structurefunctionpagename = $functionrow['structurefunctionpagename'];
		$documentationpagedescription = $functionrow['documentationpagedescription'];
		$structurefunctionsectionname = $functionrow['structurefunctionsectionname'];
		$structurefunctionid = $functionrow['structurefunctionid'];
		$uploaderactive = $functionrow['uploaderactive'];
		$addhelptext = $functionrow['addhelptext'];
		$edithelptext = $functionrow['edithelptext'];
		$gridhelptext = $functionrow['gridhelptext'];
		$runscriptextra1documentation = $functionrow['runscriptextra1documentation'];
		$runscriptextra2documentation = $functionrow['runscriptextra2documentation'];
		$runscriptextra3documentation = $functionrow['runscriptextra3documentation'];
		$runscriptextra4documentation = $functionrow['runscriptextra4documentation'];
		$runscriptextra5documentation = $functionrow['runscriptextra5documentation'];
		$runscriptextra1name = $functionrow['runscriptextra1name'];
		$runscriptextra2name = $functionrow['runscriptextra2name'];
		$runscriptextra3name = $functionrow['runscriptextra3name'];
		$runscriptextra4name = $functionrow['runscriptextra4name'];
		$runscriptextra5name = $functionrow['runscriptextra5name'];
	
		echo "<h2>Data Page - ".$structurefunctionpagename."</h2>";
		
		//Overview		
		echo "<p>",$documentationpagedescription."</p>";		
		echo "<p>You can find the <b>".$structurefunctionpagename."</b> page in the main navigation section called <b>";
		echo $structurefunctionsectionname."</b>.</p><br/>";		
		
		
		//Field Grid Overview
		echo "<h2>Grid Fields</h2>";
		echo "<p>Within this section we provide a short breakdown of all of the fields within this Data Grid Page.</p>";
		echo "<table class='table table-bordered' style='width:100%;''>";
		echo "<thead><th>Field Name</th><th>Field Type</th><th>Required Field</th><th>Database Field Name</th></thead>";
		if ($database == $masterdatabase){
			$getfields = mysql_query("select * from $docdatabase.structurefield
			inner join $masterdatabase.structurefieldtype on structurefieldtype.structurefieldtypeid = structurefield.structurefieldtypeid
			where structurefield.structurefunctionid = '$structurefunctionid' and fieldposition >= 1");
		}	
		else {
			$getfields = "select * from $docdatabase.structurefield
			inner join $masterdatabase.structurefieldtype on structurefieldtype.structurefieldtypeid = structurefield.structurefieldtypeid
			where structurefield.structurefunctionid = '$structurefunctionid' and fieldposition >= 1 and structurefield.masteronly = 0";
			//echo "<br/>getfields: ".$getfields;
			$getfields = mysql_query($getfields);
		}	
		while($getfieldrow = mysql_fetch_array($getfields)){
			$fielddisplayname = $getfieldrow['displayname'];		
			$fieldtypename = $getfieldrow['fieldtypename'];		
			$docdatabasename = $getfieldrow['structurefieldname'];		
			$requiredfield = $getfieldrow['requiredfield'];	
			if($requiredfield == 1){
				$requiredfield = 'Yes';		
			}	
			else {
				$requiredfield = 'No';
			}
			echo "<tr><td>".$fielddisplayname."</td><td>".$fieldtypename."</td>";
			echo "<td>".$requiredfield."</td><td>".$docdatabasename."</td></tr>";
		}		
		echo "</table><br/>";
		
		
		//Search Grid
		echo "<h2>Grid Layout</h2>";
		echo "<p>Every page has a Grid as the landing page. You can carry out the following actions on the grid: </p>";
		if($gridhelptext <> ""){		
			echo "<p>".$gridhelptext."</p>";
		}
		echo "<ul>";
		echo "<li><b>Add Item:</b> Click on 'Add ".$structurefunctionpagename."' to add a new record to the data grid.</li>";
		echo "<li><b>Edit Item:</b> To Edit an item click on 'Actions' and then select 'Edit'.</li>";
		echo "<li><b>Deactivate Item:</b> To Deactivate an item click on 'Actions' and then select 'Deactivate'.</li>";
		echo "<li><b>Searching:</b> You can input a date range and/or a search term and then click 'Submit' button.</li>";
		echo "<li><b>Spreadsheet Download:</b> At the bottom of the page you can click on 'Export' button to download the contents of the grid.</li>";
		if($uploaderactive == 1){		
			echo "<li><b>Bulk Spreadsheet Upload:</b> You can import data in bulk by clicking 'Bulk Upload ".$structurefunctionpagename."' and following the instructions.</li>";
		}
		echo "<br/></ul>";
		
		//Add Item
		echo "<h2>Add ".$structurefunctionpagename."</h2>";
		if($addhelptext <> ""){		
			echo "<p>".$addhelptext."</p>";
		}
		echo "<p>To add a ".$structurefunctionpagename." to the system follow these steps:</p>";
		echo "<ul>";
		echo "<li>Click on 'Add ".$structurefunctionpagename."' button.</li>";
		echo "<li>The page will reload on the 'Add' page.</li>";
		if($database == $masterdatabase){
			$getfields = mysql_query("select * from $docdatabase.structurefield
			inner join $masterdatabase.structurefieldtype on structurefieldtype.structurefieldtypeid = structurefield.structurefieldtypeid
			where structurefield.structurefunctionid = '$structurefunctionid' and fieldposition >= 2
			order by fieldposition asc");
		}
		else {
			$getfields = mysql_query("select * from $docdatabase.structurefield
			inner join $masterdatabase.structurefieldtype on structurefieldtype.structurefieldtypeid = structurefield.structurefieldtypeid
			where structurefield.structurefunctionid = '$structurefunctionid' and fieldposition >= 2 and structurefield.masteronly = 0
			order by fieldposition asc");
		}
		while($getfieldrow = mysql_fetch_array($getfields)){
			$fielddisplayname = $getfieldrow['displayname'];		
			$fieldtypename = $getfieldrow['fieldtypename'];		
			$docdatabasename = $getfieldrow['structurefieldname'];		
			$requiredfield = $getfieldrow['requiredfield'];	
			$characters = $getfieldrow['textlimit'];	
			$helptext = $getfieldrow['helptext'];	
			if($requiredfield == 0){
				$requiredfield = 'Yes';
				$requiredstatement = 'This field is not required. ';		
			}	
			else {
				$requiredfield = 'No';
				$requiredstatement = 'This field is required. ';		
			}
			$fieldtext = "";
			if($fieldtypename == 'Text'){
				$fieldtext = "Fill in the Text field '".$fielddisplayname."', up to a maximum of ".$characters." characters.";			
			}
			if($fieldtypename == 'Value'){
				$fieldtext = "Fill in the Value field '".$fielddisplayname."'.";			
			}
			if($fieldtypename == 'Date Select'){
				$fieldtext = "Click into '".$fielddisplayname."' and select a date.";			
			}
			if($fieldtypename == 'Table Connect'){
				$fieldtext = "Select an item from the '".$fielddisplayname."' dropdown. If nothing is listed, you will need to fill in the associated table with some content before continuing to add the '".$fielddisplayname."'.";			
			}
			if($fieldtypename == 'Checkbox'){
				$fieldtext = "Tick the box on '".$fielddisplayname."' if you wish to answer in the affirmative.";			
			}
			echo "<li>".$fieldtext." ".$requiredstatement." ".$helptext."</li>";
		}
		echo "<li>Click on 'Save'. If everything is correct then you will be redirected back to the grid.</li>";
		echo "<li>If you have made a mistake then a red validation error will appear, showing you what is wrong. Correct the errors and then click 'Save' once again.</li>";
		echo "</ul><br/>";		
		
		//Edit Item
		echo "<h2>Edit ".$structurefunctionpagename."</h2>";
		if($edithelptext <> ""){		
			echo "<p>".$edithelptext."</p>";
		}
		echo "<p>To edit an existing ".$structurefunctionpagename.":</p>";
		echo "<ul>";
		echo "<li>Click on 'Action' dropdown within the grid, next to the record you want to update.</li>";
		echo "<li>Select 'Edit'</li>";
		echo "<li>The page will reload on the 'Edit' page for the specific '".$structurefunctionpagename."' record.</li>";
		if($database == $masterdatabase){
			$getfields = mysql_query("select * from $docdatabase.structurefield
			inner join $masterdatabase.structurefieldtype on structurefieldtype.structurefieldtypeid = structurefield.structurefieldtypeid
			where structurefield.structurefunctionid = '$structurefunctionid' and fieldposition >= 2
			order by fieldposition asc");
		}
		else {
			$getfields = mysql_query("select * from $docdatabase.structurefield
			inner join $masterdatabase.structurefieldtype on structurefieldtype.structurefieldtypeid = structurefield.structurefieldtypeid
			where structurefield.structurefunctionid = '$structurefunctionid' and fieldposition >= 2 and structurefield.masteronly = 0
			order by fieldposition asc");
		}
		while($getfieldrow = mysql_fetch_array($getfields)){
			$fielddisplayname = $getfieldrow['displayname'];		
			$fieldtypename = $getfieldrow['fieldtypename'];		
			$docdatabasename = $getfieldrow['structurefieldname'];		
			$requiredfield = $getfieldrow['requiredfield'];	
			$characters = $getfieldrow['textlimit'];	
			$helptext = $getfieldrow['helptext'];	
			if($requiredfield == 0){
				$requiredfield = 'Yes';
				$requiredstatement = 'This field is not required. ';		
			}	
			else {
				$requiredfield = 'No';
				$requiredstatement = 'This field is required. ';		
			}
			$fieldtext = "";
			if($fieldtypename == 'Text'){
				$fieldtext = "Update the Text field '".$fielddisplayname."', up to a maximum of ".$characters." characters.";			
			}
			if($fieldtypename == 'Value'){
				$fieldtext = "Update the Value field '".$fielddisplayname."'.";			
			}
			if($fieldtypename == 'Date Select'){
				$fieldtext = "Click into '".$fielddisplayname."' and update the date.";			
			}
			if($fieldtypename == 'Table Connect'){
				$fieldtext = "Select an alternative item from the '".$fielddisplayname."' dropdown.";			
			}
			if($fieldtypename == 'Checkbox'){
				$fieldtext = "Tick the box on '".$fielddisplayname."' if you wish to answer in the affirmative.";			
			}
			echo "<li>".$fieldtext." ".$requiredstatement." ".$helptext."</li>";
		}
		echo "<li>Click on 'Save'. If everything is correct then you will be redirected back to the grid.</li>";
		echo "<li>If you have made a mistake then a red validation error will appear, showing you what is wrong. Correct the errors and then click 'Save' once again.</li>";
		echo "</ul><br/>";		
		
		//Disable Item
		echo "<h2>Deactivate ".$structurefunctionpagename."</h2>";
		echo "<p>You can Deactivate a record by doing the following:</p>";
		echo "<ul>";
		echo "<li>Click on 'Action' dropdown within the grid, next to the record you want to update.</li>";
		echo "<li>Select 'Deactivate'.</li>";
		echo "<li>The page will reload and a green message will appear 'The status has been updated'.</li>";
		echo "</ul>";
		echo "<p>Deactivating a record means that it will no longer be available to other data tables, and will no longer be returned in Dashboards/Reports/Data Sets etc. You should always check that your specific functionality has excluded Deactivated records.</p><br/>";
		
		//Enable Item
		echo "<h2>Activate ".$structurefunctionpagename."</h2>";
		echo "<p>You can Activate a record by doing the following:</p>";
		echo "<ul>";
		echo "<li>Click on 'Action' dropdown within the grid, next to the record you want to update.</li>";
		echo "<li>Select 'Activate'.</li>";
		echo "<li>The page will reload and a green message will appear 'The status has been updated'.</li>";
		echo "</ul><br/>";
		
		
		//Bulk Upload
		if($uploaderactive == 1){		
			echo "<h2>Bulk Upload ".$structurefunctionpagename."</h2>";
			echo "<p>Within the system you can carry out a Bulk Upload of your data. Please take special care with this feature, as you are affecting a large quantity of records.</p>";
			echo "<p>Follow these steps to upload your data: </p>";
			echo "<ul>";
			echo "<li>Click on the 'Bulk Upload ".$structurefunctionpagename."' button.</li>";
			echo "<li>Click on the 'Download Spreadsheet Sample' button.</li>";
			echo "<li>Fill in the data within the spreadsheet. Do not remove or move any columns in the spreadsheet.</li>";
			echo "<li>Once you are ready to import click on 'Choose File' and select the file from your machine.</li>";
			echo "<li>Click 'Submit'.</li>";
			echo "<li>If everything is okay in your file, then the file will be uploaded and the records will appear in the grid.</li>";
			echo "<li>If something is wrong then a results grid will appear. Fix the issues in your spreadsheet and then attempt to reupload..</li>";
			echo "</ul>";	
			echo "<p>Within one file you can insert records and update records. To Insert a record simply leave the ID field (first column in spreadsheet) blank, and the system will Insert the record. To Update a record make sure you fill in the ID field (first column in spreadsheet).</p><br/>";	
		}
		
		//Special Scripts
		if($runscriptextra1name <> ""){
			echo "<h2>Run '".$runscriptextra1name."'</h2>";
			echo "<p>Within ".$structurefunctionpagename." there is a special additional option within the 'Actions' column.</p>";
			echo "<p>".$runscriptextra1documentation."</p>";	
			echo "<p>You can run this special additional option by:</p>";	
			echo "<ul>";
			echo "<li>Clicking on 'Action' dropdown within the grid, next to the record you want to update.</li>";
			echo "<li>Select '".$runscriptextra1name."'.</li>";
			echo "<li>Follow any additional instructions on the page.</li>";
			echo "</ul><br/>";	
		}
		if($runscriptextra2name <> ""){
			echo "<h2>Run '".$runscriptextra2name."'</h2>";
			echo "<p>Within ".$structurefunctionpagename." there is a special additional option within the 'Actions' column.</p>";
			echo "<p>".$runscriptextra2documentation."</p>";	
			echo "<p>You can run this special additional option by:</p>";	
			echo "<ul>";
			echo "<li>Clicking on 'Action' dropdown within the grid, next to the record you want to update.</li>";
			echo "<li>Select '".$runscriptextra2name."'.</li>";
			echo "<li>Follow any additional instructions on the page.</li>";
			echo "</ul><br/>";	
		}
		if($runscriptextra3name <> ""){
			echo "<h2>Run '".$runscriptextra3name."'</h2>";
			echo "<p>Within ".$structurefunctionpagename." there is a special additional option within the 'Actions' column.</p>";
			echo "<p>".$runscriptextra3documentation."</p>";	
			echo "<p>You can run this special additional option by:</p>";	
			echo "<ul>";
			echo "<li>Clicking on 'Action' dropdown within the grid, next to the record you want to update.</li>";
			echo "<li>Select '".$runscriptextra3name."'.</li>";
			echo "<li>Follow any additional instructions on the page.</li>";
			echo "</ul><br/>";	
		}
		if($runscriptextra4name <> ""){
			echo "<h2>Run '".$runscriptextra4name."'</h2>";
			echo "<p>Within ".$structurefunctionpagename." there is a special additional option within the 'Actions' column.</p>";
			echo "<p>".$runscriptextra4documentation."</p>";	
			echo "<p>You can run this special additional option by:</p>";	
			echo "<ul>";
			echo "<li>Clicking on 'Action' dropdown within the grid, next to the record you want to update.</li>";
			echo "<li>Select '".$runscriptextra4name."'.</li>";
			echo "<li>Follow any additional instructions on the page.</li>";
			echo "</ul><br/>";	
		}
		if($runscriptextra5name <> ""){
			echo "<h2>Run '".$runscriptextra5name."'</h2>";
			echo "<p>Within ".$structurefunctionpagename." there is a special additional option within the 'Actions' column.</p>";
			echo "<p>".$runscriptextra5documentation."</p>";	
			echo "<p>You can run this special additional option by:</p>";	
			echo "<ul>";
			echo "<li>Clicking on 'Action' dropdown within the grid, next to the record you want to update.</li>";
			echo "<li>Select '".$runscriptextra5name."'.</li>";
			echo "<li>Follow any additional instructions on the page.</li>";
			echo "</ul><br/>";	
		}
		
		
	}
	
	
	//Page 6 - views
	if($sectionname == 'view'){
		$structurefunctionpageid = isset($_GET['structurefunctionpageid']) ? $_GET['structurefunctionpageid'] : '';
		$selectfunction = mysql_query("select * from $docdatabase.structurefunctionpage 
		inner join $docdatabase. structurefunctionsection on structurefunctionpage.structurefunctionsectionid = structurefunctionsection.structurefunctionsectionid
		inner join $docdatabase.view on view.viewid = structurefunctionpage.viewid
		where structurefunctionpage.structurefunctionpageid = '$structurefunctionpageid'");
		$functionrow = mysql_fetch_array($selectfunction);
		$structurefunctionpagename = $functionrow['structurefunctionpagename'];
		$documentationpagedescription = $functionrow['documentationpagedescription'];
		$structurefunctionsectionname = $functionrow['structurefunctionsectionname'];
		echo "<h2>View - ".$structurefunctionpagename."</h2>";
		
		//Introduction
		echo $documentationpagedescription."<br/><br/>";		
		echo "You can find the <b>".$structurefunctionpagename."</b> View in the main navigation section called <b>";
		echo $structurefunctionsectionname."</b><br/><br/>";		
		
		//Section Summary 
		echo "<h2>View Sections</h2>";
		echo "<p>Within this section we provide a short breakdown of all of the sections within the View.</p>";
		echo "<table class='table table-bordered' style='width:100%;''>";
		echo "<thead><th>Section Name</th><th>Section Type</th><th>Section Description</th></thead>";
		$getfields = mysql_query("select * from $docdatabase.structurefunctionpage
		inner join $docdatabase.view on structurefunctionpage.viewid = view.viewid
		inner join $docdatabase.viewsection on view.viewid = viewsection.viewid
		where structurefunctionpageid = '$structurefunctionpageid' and viewsection.disabled = 0
		order by viewsection.position asc");
		while($getfieldrow = mysql_fetch_array($getfields)){
			$sectionname = $getfieldrow['viewsectionname'];		
			$sectionhelptext = $getfieldrow['documentationhelp'];		
			$viewtextid = $getfieldrow['viewtextid'];		
			$viewdatasetid = $getfieldrow['viewdatasetid'];	
			$viewmapid = $getfieldrow['viewmapid'];	
			$viewcalendarid = $getfieldrow['viewcalendarid'];	
			$viewspecialcodeurl = $getfieldrow['viewspecialcodeurl'];	
			if($viewtextid >= 1){
				$sectiontype = "Text";			
			}			
			if($viewdatasetid >= 1){
				$sectiontype = "Data Grid";			
			}			
			if($viewmapid >= 1){
				$sectiontype = "Map";			
			}			
			if($viewcalendarid >= 1){
				$sectiontype = "Calendar";			
			}			
			if($viewspecialcodeurl <> ""){
				$sectiontype = "Special Code";			
			}			
			echo "<tr><td>".$sectionname."</td><td>".$sectiontype."</td>";
			echo "<td>".$sectionhelptext."</td></tr>";
		}		
		echo "</table><br/>";
		
		$getfields = mysql_query("select * from $docdatabase.structurefunctionpage
		inner join $docdatabase.view on structurefunctionpage.viewid = view.viewid
		inner join $docdatabase.viewsection on view.viewid = viewsection.viewid
		where structurefunctionpageid = '$structurefunctionpageid' and viewsection.disabled = 0
		order by viewsection.position asc");
		while($getfieldrow = mysql_fetch_array($getfields)){
			$sectionname = $getfieldrow['viewsectionname'];		
			$sectionhelptext = $getfieldrow['documentationhelp'];		
			$viewtextid = $getfieldrow['viewtextid'];		
			$viewdatasetid = $getfieldrow['viewdatasetid'];	
			$viewmapid = $getfieldrow['viewmapid'];	
			$viewcalendarid = $getfieldrow['viewcalendarid'];	
			$viewspecialcodeurl = $getfieldrow['viewspecialcodeurl'];	
			if($viewtextid >= 1){
				$sectiontype = "Text";			
			}			
			if($viewdatasetid >= 1){
				$sectiontype = "Data Grid";			
			}			
			if($viewmapid >= 1){
				$sectiontype = "Map";			
			}			
			if($viewcalendarid >= 1){
				$sectiontype = "Calendar";			
			}			
			if($viewspecialcodeurl <> ""){
				$sectiontype = "Special Code";			
			}			
			if($viewcalendarid >= 1){
				//Usage Explanation - Calendar
				$getcalendar = mysql_query("select * from $docdatabase.viewcalendar 
				where viewcalendarid = '$viewcalendarid'");
				$getcalendarrow = mysql_fetch_array($getcalendar);
				$calendarname = $getcalendarrow['viewcalendarname'];
				echo "<h2>Calendar '".$calendarname."'</h2>";
				echo "<p>Within this Calendar you can do the following:</p>";
				echo "<ul>";
				echo "<li>On the right hand side, you can move months by clicking on the back and forward arrow.</li>";
				echo "<li>On the right hand side, if you are not in this month then you can press the 'Today' button to go back to today on the calendar.</li>";
				echo "<li>Within the grid, you can click on an item to navigateto the Edit page for that item.</li>";
				echo "<li>You can review the colour coding assigned to each item.</li>";
				echo "<li>You can hover over an item to see the full name of the item, if the item name is not fully showing.</li>";
				echo "</ul><br/>";			
			}
			if($viewmapid >= 1){
				//Usage Explanation - Map
				$getmap = mysql_query("select * from $docdatabase.viewmap 
				where viewmapid = '$viewmapid'");
				$getmaprow = mysql_fetch_array($getmap);
				$mapname = $getmaprow['viewmapname'];
				$viewlocationpins = $getmaprow['viewlocationpins'];
				$viewheat = $getmaprow['viewheat'];
				$allowsearch = $getmaprow['allowsearch'];
				echo "<h2>Map '".$mapname."'</h2>";
				echo "<p>Within this Map you can do the following:</p>";
				echo "<ul>";
				echo "<li>Review all of the items on the map, and click into a single item to see further details.</li>";
				if($allowsearch == 1){		
					$getfilters = mysql_query("select * from $docdatabase.viewmapfilter
					inner join $docdatabase.datasetcolumn on datasetcolumn.datasetcolumnid = viewmapfilter.datasetcolumnid
					where viewmapfilter.viewmapid = '$viewmapid'");
					$customfields = "";
					while ($row99 = mysql_fetch_array($getfilters)){
						$fieldname = $row99['datasetcolumnname'];
						if($customfields == ""){
							$customfields = $fieldname;							
						} 
						else {
							$customfields = $customfields.", ".$fieldname;	
						}					
					}
					echo "<li>Search by the following custom fields: ".$customfields."</li>";
				}	
				if($viewheat == 1 && $viewlocationpins == 1){					
					echo "<li>You can switch the Map Type between 'Heat Map' and 'Marker Location Map'.</li>";
				}
				if($viewheat == 1){					
					echo "<li>A 'Heat Map' shows all of your items based on their location and density within a specific area. The darker the colour the more density of items are in that area.</li>";
				}
				if($viewlocationpins == 1){	
					echo "<li>A 'Marker Location Map' shows each item as a specific location you can click into.</li>";
				}
				echo "</ul><br/>";
			}
		}
		
	}
	
	
	//Page 7 - dashboards
	if($sectionname == 'dashboards' && $businessdashboardsections <> ""){
		echo "<h2>Dashboards</h2>";
		echo "<p>The application comes with the following Dashboard Sections already installed. You can add additional Dashboard Sections and Dashboard Charts as needed.</p>";
		echo "<p>You can carry out the following actions on the page:</p>";		
		echo "<ul><li>Change the date range the charts are looking at by inputting a new Start/End Date and clicking 'Submit'.</li>";		
		echo "<li>Download the chart table by clicking on the spreadsheet icon next to each chart name.</li></ul><br/>";		
		echo "<table class='table table-bordered' style='width:100%;''>";
		echo "<thead><th style='width:10%'>Dashboard Section</th><th style='width:15%'>Chart Name</th><th style='width:10%'>Type</th><th style='width:35%'>Chart Description</th><th style='width:40%'>Chart Settings</th></thead>";
		$array = explode(',', $businessdashboardsections); //split string into array seperated by ', '
		foreach($array as $value) //loop over values
		{
			$getcharts = mysql_query("select * from $docdatabase.chartsection 
			inner join $docdatabase.chart on chart.chartsectionid = chartsection.chartsectionid
			inner join $masterdatabase.charttype on chart.charttypeid = charttype.charttypeid
			where chartsection.chartsectionid = '$value' and chart.disabled = 0");
			while($row23 = mysql_fetch_array($getcharts)){
				$chartname = $row23['chartname'];
				$chartsectionname = $row23['chartsectionname'];
				$chartdescription = $row23['chartdescription'];
				$charttype = $row23['charttypename'];
				$chartid = $row23['chartid'];
				$datasetid = $row23['datasetid'];
				echo "<tr><td>".$chartsectionname."</td>";
				echo "<td>".$chartname."</td>";
				echo "<td>".$charttype."</td>";
				echo "<td>".$chartdescription."</td>";
				echo "<td>";
				$columns = "";
				$getcolumns = mysql_query("select * from $docdatabase.datasetcolumn 
				inner join $docdatabase.dataset on dataset.datasetid = datasetcolumn.datasetid
				where datasetcolumn.datasetid = '$datasetid' order by sortorder asc");
				$count = 0;
				while ($row34 = mysql_fetch_array($getcolumns)){
					if ($count <=1){
						$columnname = $row34['datasetcolumnname'];
						if($charttype == 'Pie'){
							if($count == 0){
								$columns = $columns."<b>Segments:</b> ".$columnname."<br/>";							
							}		
							if($count == 1)	{
								$columns = $columns."<b>Value:</b> ".$columnname;		
							}		
						}
						if($charttype == 'Line'){
							if($count == 0){
								$columns = $columns."<b>X Value:</b> ".$columnname."<br/>";							
							}		
							if($count == 1)	{
								$columns = $columns."<b>Y Value:</b> ".$columnname;		
							}		
						}
						if($charttype == 'Bar'){
							if($count == 0){
								$columns = $columns."<b>Y Value:</b> ".$columnname."<br/>";							
							}		
							if($count == 1)	{
								$columns = $columns."<b>X Value:</b> ".$columnname;		
							}		
						}
						$count = $count +1;
					}
				}
				echo $columns;
				echo "</td></tr>";
			}
		}
		echo "</table>";
		echo "<h2>Customising Charts</h2>";
		echo "<p>You have the option to add additional charts to the system. To view how to do this, please go to the Charts configuration section within the OneBusiness Business documentation, or click <a href='http://localhost:8888/onebusiness/1.0.3/view.php?viewid=8&appid=2&sectionname=pagegrid&structurefunctionpageid=47'>here</a>.</p>";
	}
	
	
	//Page 8 - reports
	if($sectionname == 'reports' && $businessreportsections <> ""){
		echo "<h2>Reports</h2>";
		echo "<p>The application comes with the following Reports already installed. You can add additional Reports as needed, and also add columns to these reports.</p>";
		echo "<table class='table table-bordered' style='width:100%;''>";
		echo "<thead><th style='width:10%'>Report Section</th><th style='width:15%'>Report Name</th><th style='width:45%'>Report Description</th><th style='width:40%'>Report Columns</th></thead>";
		$array = explode(',', $businessreportsections); //split string into array seperated by ', '
		foreach($array as $value) //loop over values
		{
			$getreports = mysql_query("select * from $docdatabase.reportsection 
			inner join $docdatabase.report on report.reportsectionid = reportsection.reportsectionid
			where reportsection.reportsectionid = '$value' and report.disabled = 0");
			while($row23 = mysql_fetch_array($getreports)){
				$reportname = $row23['reportname'];
				$reportsectionname = $row23['reportsectionname'];
				$reportdescription = $row23['reportdescription'];
				$reportid = $row23['reportid'];
				$datasetid = $row23['datasetid'];
				echo "<tr><td>".$reportsectionname."</td>";
				echo "<td>".$reportname."</td>";
				echo "<td>".$reportdescription."</td>";
				echo "<td>";
				$columns = "<ul>";
				$getcolumns = mysql_query("select * from $docdatabase.datasetcolumn 
				inner join $docdatabase.dataset on dataset.datasetid = datasetcolumn.datasetid
				where datasetcolumn.datasetid = '$datasetid'");
				while ($row34 = mysql_fetch_array($getcolumns)){
					$columnname = $row34['datasetcolumnname'];
					$columns = $columns."<li>".$columnname."</li>";				
				}
				echo $columns;
				echo "</td></tr>";
			}
		}
		echo "</table>";
		echo "<h2>Customising Reports</h2>";
		echo "<p>You have the option to add additional reports to the system, and also add fields to the existing reports. To view how to do this, please go to the Reports configuration section within the OneBusiness Business documentation, or click <a href='view.php?viewid=8&appid=2&sectionname=pagegrid&structurefunctionpageid=49'>here</a>.</p>";
	}
	
}	
echo "</td></tr></table>";
echo "</div>";
?>





