<?php
require($versionname.'/fpdf181/fpdf.php');
class PDF extends FPDF
	{
		
	// Page header
	function Header()
	{
	    // Arial bold 15
	    $this->SetFont('Arial','B',13);
	}
	
	
	// Page footer
	function Footer()
	{
	    // Position at 1.5 cm from bottom
	    $this->SetY(-15);
	    // Arial italic 8
	    $this->SetFont('Arial','I',8);
	    // Page number
	    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	}
	
	}
	
$date = date("Y-m-d");
//get all configs
$getconfig = mysql_query("select * from invoiceconfiguration 
inner join invoicefrequency on invoicefrequency.invoicefrequencyid = invoiceconfiguration.invoicefrequencyid
where invoiceconfiguration.disabled = 0 and autoinvoice = 1");
while($getconfigrow = mysql_fetch_array($getconfig)){
	$businessunitid = $getconfigrow['businessunitid'];
	$invoicefrequencyname = $getconfigrow['invoicefrequencyname'];
	$invoicedayofweek = $getconfigrow['invoicedayofweek'];
	$invoicedayofmonth = $getconfigrow['invoicedayofmonth'];
	$autoemailinvoice = $getconfigrow['autoemailinvoice'];
	$emailinvoicesubject = $getconfigrow['emailinvoicesubject'];
	$emailinvoicetext = $getconfigrow['emailinvoicetext'];
	$emailinvoicefromname = $getconfigrow['emailinvoicefromname'];
	$emailinvoicefromemail = $getconfigrow['emailinvoicefromemail'];
	
	echo "<br/><br/><br/><b>BUSINESS UNIT: ".$businessunitid."</b>";
	
	//check if due to process invoices
	$processinvoices = 0;
	if($invoicefrequencyname == "Weekly"){
		if($invoicedayofweek == ""){
			echo "<br/>Failure - no day of week specified";
		}
		else {
			$dayofweek = date("l");
			echo "<br/>dayofweek: ".$dayofweek;
			if($invoicedayofweek == $dayofweek){
				$processinvoices = 1;
			}
			else{
				echo "<br/>Not due to be processed today";
			}
		}
	}
	if($invoicefrequencyname == "Monthly"){
		if($invoicedayofmonth == 0){
			echo "<br/>Failure - no day of month specified";
		}
		else {
			if($invoicedayofmonth >= 29){
				$lastdayofthismonth = date("t");
				echo "<br/>lastdayofthismonth: ".$lastdayofthismonth;		
				$invoicedayofmonth = $lastdayofthismonth;	
			}
			$dayofmonth = date("d");
			echo "<br/>dayofmonth: ".$dayofmonth;
			if($invoicedayofmonth == $dayofmonth){
				$processinvoices = 1;
			}
			else{
				echo "<br/>Not due to be processed today";
			}
		}
	}
	
	//process new invoices
	echo "<br/>processinvoices: ".$processinvoices;
	if($processinvoices == 1){
		//work out dates to be processed
		if($invoicefrequencyname == "Monthly"){
			$enddate = date("Y-m-d");
			$startdate = date('Y-m-d', strtotime("- 1 month"));		
		}
		if($invoicefrequencyname == "Weekly"){
			$enddate = date("Y-m-d");
			$startdate = date('Y-m-d', strtotime("- 7 days"));		
		}
		echo "<br/>startdate: ".$startdate;		
		echo "<br/>enddate: ".$enddate;		
		
		if(isset($startdate)){
			$savedcustomerid = 0;
			//get all financialtransactions that are between two dates and have not got invoices assigned to them yet
			$getfinancialtransactions = mysql_query("select financialtransaction.financialtransactionid, financialtransaction.businessunitid, 
			financialtransaction.customerid, financialtransaction.currencyid, financialtransaction.amount, financialtransaction.salestaxid, 
			financialtransaction.amountincludingtax, financialtransactionname from financialtransaction 
			left join invoiceitem on financialtransaction.financialtransactionid = invoiceitem.financialtransactionid
			where transactiondate >= '$startdate' and transactiondate <= '$enddate' and financialtransactionstatusid = 6
			and invoiceitem.invoiceitemid is null and financialtransaction.businessunitid = $businessunitid
			order by financialtransaction.customerid desc");
			$financialtransactionidlist = "";
			$invoiceidlist = "";
			$p = 0;
			while($financialtransactionrow = mysql_fetch_array($getfinancialtransactions)){
				$financialtransactionid = $financialtransactionrow['financialtransactionid'];
				$businessunitid = $financialtransactionrow['businessunitid'];
				$customerid = $financialtransactionrow['customerid'];
				$currencyid = $financialtransactionrow['currencyid'];
				$amount = $financialtransactionrow['amount'];
				$salestaxid = $financialtransactionrow['salestaxid'];
				$amountincludingtax = $financialtransactionrow['amountincludingtax'];
				$financialtransactionname = mysql_real_escape_string($financialtransactionrow['financialtransactionname']);
				
				echo "<br/>financialtransactionid: ".$financialtransactionid." - ".$customerid;
				if($savedcustomerid == $customerid){
					//create invoiceitem record
					$createinvoiceitem = mysql_query("insert into invoiceitem (invoiceitemname, financialtransactionid, invoiceid, customerid, 
					businessunitid, currencyid, amount, salestaxid, amountincludingtax, disabled, datecreated, masteronly) 
					values ('$financialtransactionname', '$financialtransactionid', '$invoiceid', '$customerid', '$businessunitid', '$currencyid', 
					'$amount', '$salestaxid', '$amountincludingtax', '0', '$date', '0')");					
				}	
				else {
					$savedcustomerid = $customerid;
					$p = $p + 1;
					if($p >= 20){
						break;					
					}
					//create invoice record
					$createinvoice = mysql_query("insert into invoice (invoicename, dateissued, customerid, businessunitid, disabled, datecreated, 
					masteronly) 
					values ('Invoice - $financialtransactionname', '$date', '$customerid', '$businessunitid', '0', '$date', '0')");
					$invoiceid = mysql_insert_id();
					$invoiceidlist = $invoiceidlist.$invoiceid.",";
			
					//create invoiceitem record
					$createinvoiceitem = mysql_query("insert into invoiceitem (invoiceitemname, financialtransactionid, invoiceid, customerid, 
					businessunitid, currencyid, amount, salestaxid, amountincludingtax, disabled, datecreated, masteronly) 
					values ('$financialtransactionname', '$financialtransactionid', '$invoiceid', '$customerid', '$businessunitid', '$currencyid', 
					'$amount', '$salestaxid', '$amountincludingtax', '0', '$date', '0')");	
				}	
				$financialtransactionidlist = $financialtransactionidlist.$financialtransactionid.",";	
				
			}
			
			//update financialtransaction to invoiced
			if($financialtransactionidlist <> ""){
				$financialtransactionidlist = rtrim($financialtransactionidlist, ",");
				$updatefinancialtransaction = mysql_query("update financialtransaction 
				set financialtransactionstatusid = 4 
				where financialtransactionid in ($financialtransactionidlist)");
			}
			
			//Process sending invoices for created records			
			if($invoiceidlist <> ""){
				$invoiceidlist = rtrim($invoiceidlist, ",");
				$getinvoices = mysql_query("select * from invoice 
				inner join customer on customer.customerid = invoice.customerid
				where invoiceid in ($invoiceidlist)");
				$date = date("Y-m-d");
				while($getinvoicerow = mysql_fetch_array($getinvoices)){
					$invoiceid = $getinvoicerow['invoiceid'];		
					$customername = $getinvoicerow['customername'];		
					$firstname = $getinvoicerow['firstname'];		
					$lastname = $getinvoicerow['lastname'];		
					$emailaddress = $getinvoicerow['emailaddress'];		
					
					include('oneinvoicebuildinvoice.php');
					$rand = rand(1000,9999);
					$filename="documents/".$database."/sf/invoice/".$invoiceid." - Invoice - ".$rand.".pdf";
					$pdf->Output($filename,'F');	
					$insertdocument = mysql_query("insert into structurefunctiondocument (structurefunctiondocumentname, 
					datecreated, documenturl, doctypename, structurefunctionid, rowid) 
					values ('$invoiceid - Invoice - $rand', '$date', '$database/sf/invoice/$invoiceid - Invoice - $rand.pdf', 
					'Invoice', '378', '$invoiceid')");
					
					//send email containing invoice file
					if($autoemailinvoice == 1 && $emailinvoicesubject <> "" && $emailinvoicetext <> ""
					&& $emailinvoicefromname <> "" && $emailinvoicefromemail <> ""){
						echo "<br/>SEND EMAIL - ".$invoiceid." - ".$emailaddress;
						$emailtext = $emailinvoicetext;
						$emailtext = str_replace("{customername}", $customername, $emailtext);
						$emailtext = str_replace("{invoiceid}", $invoiceid, $emailtext);
						$emailtext = str_replace("{contactfirstname}", $firstname, $emailtext);
						$emailtext = str_replace("{contactlastname}", $lastname, $emailtext);
						$emailinvoicesubject = $emailinvoicesubject." - ".$invoiceid;
						
						//update email as sent
						$date = date("Y-m-d");
						$updateinvoice = mysql_query("update invoice set emailsent = '$date' 
						where invoiceid = $invoiceid");	
						
						//send email
						require_once($versionname.'/PHPMailer/class.phpmailer.php');
						$email = new PHPMailer();
						$email->From      = $emailinvoicefromemail;
						$email->FromName  = $emailinvoicefromname;
						$email->Subject   = $emailinvoicesubject;
						$email->Body      = $emailtext;
						$email->AddAddress( $emailaddress );
						
						$file_to_attach = 'documents/';
						
						$email->AddAttachment( $file_to_attach , $filename );
						
						$email->Send();
						
						
					}					
					
				}
				
			}
			
		}
	}

}
echo "<br/><br/>";

?>