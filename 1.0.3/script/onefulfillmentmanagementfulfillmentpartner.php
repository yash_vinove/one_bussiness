<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$date = date('Y-m-d');
$datetime = date('Y-m-d H:i:s');
// $date2 = date('Y-m-d', strtotime("- 365 days"));
// echo "<br/>Date 2 : ".$date2;

$records = "SELECT ecommerceorder.ecommerceorderid AS orderid, count(ecommerceorder.ecommerceorderid) AS itemsinorder, ecommercelistinglocation.ecommercelistinglocationid AS listinglocationid, shippingoptionname, productstockitem.productid,
    product.fulfillmentpackageweight, product.fulfillmentpackagewidth, product.fulfillmentpackageheight, product.fulfillmentpackagelength, product.retailprice
    FROM ecommerceorder 
    INNER JOIN ecommerceorderitem ON ecommerceorderitem.ecommerceorderid = ecommerceorder.ecommerceorderid
    INNER JOIN ecommercelistinglocation ON ecommercelistinglocation.ecommercelistinglocationid = ecommerceorderitem.ecommercelistinglocationid
    INNER JOIN ecommercelisting ON ecommercelisting.ecommercelistingid = ecommerceorderitem.ecommercelistingid
    INNER JOIN productstockitem ON productstockitem.productstockitemid = ecommerceorderitem.productstockitemid
    INNER JOIN product ON product.productid = productstockitem.productid
    WHERE ecommercelistinglocation.fulfillmentpartnerid = 0 AND ecommercelistinglocation.fulfillmentshippingruleid = 0 
    GROUP BY ecommerceorder.ecommerceorderid ";

echo "<br/><br/>Initial records : ".$records."<br/>";
$records = mysql_query($records);
while($row = mysql_fetch_array($records))
{
    $itemsinorder = $row['itemsinorder'];
    $shippingoptionname = $row['shippingoptionname'];
    $listinglocationid = $row['listinglocationid'];
    $productid = $row['productid'];
    $fulfillmentpackageweight = $row['fulfillmentpackageweight'];
    $fulfillmentpackagewidth = $row['fulfillmentpackagewidth'];
    $fulfillmentpackageheight = $row['fulfillmentpackageheight'];
    $fulfillmentpackagelength = $row['fulfillmentpackagelength'];
    $retailprice = $row['retailprice'];
    $ecommerceorderid = $row['orderid'];

    echo "itemsinorder : ".$itemsinorder."<br/>";

    // Single Item Order
    if($itemsinorder == 1)
    {
        $shippingrulerecord = "SELECT * FROM fulfillmentshippingrule WHERE thirdpartydispatchname = '$shippingoptionname' ";
        
        echo "<br/>shippingrulerecord : ".$shippingrulerecord;
        
        $shippingrulerecord = mysql_query($shippingrulerecord);
        if(mysql_num_rows($shippingrulerecord) > 0)
        {
            $arr = array();

            while($fetchrow = mysql_fetch_row($shippingrulerecord))
            {
                $fulfillmentshippingruleid = $fetchrow['fulfillmentshippingruleid'];
                $fulfillmentshippingrulename = $fetchrow['fulfillmentshippingrulename'];
                $fulfillmentpartnerid = $fetchrow['fulfillmentpartnerid'];
                $priority = $fetchrow['priority'];
                $fulfillmentshippingrulemeasurementtypeid = $fetchrow['fulfillmentshippingrulemeasurementtypeid'];
                $kgweightminimum = $fetchrow['kgweightminimum'];
                $kgweightmaximum = $fetchrow['kgweightmaximum'];
                $widthmaximum = $fetchrow['widthmaximum'];
                $depthmaximum = $fetchrow['depthmaximum'];
                $heightmaximum = $fetchrow['heightmaximum'];
                $ordervalue = $fetchrow['ordervalue'];
                // $countryid = $fetchrow['countryid'];

                $flag = false;

                if($fulfillmentpackageweight >= $kgweightminimum && $fulfillmentpackageweight <= $kgweightmaximum)
                {
                    $flag = true;
                }
                else
                {
                    $flag = false;
                }
                if($fulfillmentshippingrulemeasurementtypeid == 1)
                {
                    // Width * Depth of parcel must be less than depthmaximum * widthmaximum
                    if(($fulfillmentpackagewidth * $fulfillmentpackageheight) < ($widthmaximum * $depthmaximum))
                    {
                        $flag = true;
                    }
                    else
                    {
                        $flag = false;
                    }
                }
                if($fulfillmentshippingrulemeasurementtypeid == 2)
                {
                    // Width + Depth + Height must be less than depthmaximum+widthmaximum+heightmaximum
                    if(($fulfillmentpackagewidth + $fulfillmentpackagelength + $fulfillmentpackageheight) < ($depthmaximum + $widthmaximum + $heightmaximum))
                    {
                        $flag = true;
                    }
                    else
                    {
                        $flag = false;
                    }
                }
                if($fulfillmentpackageheight < $heightmaximum)
                {
                    $flag = true;
                }
                else
                {
                    $flag = false;
                }
                if($retailprice <= $ordervalue)
                {
                    $flag = true;
                }
                else
                {
                    $flag = false;
                }

                if($flag)
                {
                    array_push($arr, array('fulfillmentpartnerid' => $fulfillmentpartnerid, 'fulfillmentshippingruleid' => $fulfillmentshippingruleid, 'priority' => $priority));
                }
            }

            if(count($arr) > 0)
            {
                foreach($arr as $val)
                {
                    if($val['priority'] == 1)
                    {
                        $fpid = $val['fulfillmentpartnerid'];
                        $fsrid = $val['fulfillmentshippingruleid'];
                        break;
                    }
                    elseif($val['priority'] == 2)
                    {
                        $fpid = $val['fulfillmentpartnerid'];
                        $fsrid = $val['fulfillmentshippingruleid'];
                        break;
                    }
                    else
                    {
                        $fpid = $val['fulfillmentpartnerid'];
                        $fsrid = $val['fulfillmentshippingruleid'];
                        break;
                    }
                }
                $updatelistinglocation = "UPDATE ecommercelistinglocation SET fulfillmentpartnerid = '$fpid', fulfillmentshippingruleid = '$fsrid' WHERE ecommercelistinglocationid = '$listinglocationid'";
                $updatelistinglocation = mysql_query($updatelistinglocation);
            }
        }
        else
        {
            // Add shipping rule exception

            $getshippingruleexception = "SELECT fulfillmentpartnerid, fulfillmentshippingruleid FROM fulfillmentshippingruleexception 
            WHERE productid1 = $productid OR productid2 = $productid OR productid3 = $productid 
            OR productid4 = $productid OR productid5 = $productid OR productid6 = $productid 
            OR productid7 = $productid OR productid8 = $productid OR productid9 = $productid OR productid10 = $productid ";
            $getshippingruleexception = mysql_query($getshippingruleexception);

            if(mysql_num_rows($getshippingruleexception) > 0) 
            {
                while($getshippingruleexceptionrow = mysql_fetch_array($getshippingruleexception))
                {
                    $fulfillmentshippingruleid = $getshippingruleexceptionrow['fulfillmentshippingruleid'];
                    $fulfillmentpartnerid = $getshippingruleexceptionrow['fulfillmentpartnerid'];

                    $updatelistinglocation = "UPDATE ecommercelistinglocation SET fulfillmentpartnerid = '$fulfillmentpartnerid', fulfillmentshippingruleid = '$fulfillmentshippingruleid' WHERE ecommercelistinglocationid = '$listinglocationid'";
                    $updatelistinglocation = mysql_query($updatelistinglocation);
                }
            }
            else
            {
                $query = "INSERT INTO fulfillmentshippingruleexception (fulfillmentshippingruleexceptionname, disabled, 
                datecreated, masteronly, countryid, fulfillmentpartnerid, fulfillmentshippingruleid, productid1) 
                VALUES ('Exception - United Kingdom - $itemsinorder', 0, '$date', 0, 1000001, 0, 0, $productid)";

                echo "Insert shipping rule exception : ".$query."<br/>";
                $query = mysql_query($query);
            }
        }
        
    }
    else
    {
        // Multi Item Orders

        $allitems = "SELECT ecommerceorder.ecommerceorderid AS orderid, ecommercelistinglocation.ecommercelistinglocationid AS listinglocationid, shippingoptionname, productstockitem.productid,
        product.fulfillmentpackageweight, product.fulfillmentpackagewidth, product.fulfillmentpackageheight, product.fulfillmentpackagelength, product.retailprice
        FROM ecommerceorder 
        INNER JOIN ecommerceorderitem ON ecommerceorderitem.ecommerceorderid = ecommerceorder.ecommerceorderid
        INNER JOIN ecommercelistinglocation ON ecommercelistinglocation.ecommercelistinglocationid = ecommerceorderitem.ecommercelistinglocationid
        INNER JOIN ecommercelisting ON ecommercelisting.ecommercelistingid = ecommerceorderitem.ecommercelistingid
        INNER JOIN productstockitem ON productstockitem.productstockitemid = ecommerceorderitem.productstockitemid
        INNER JOIN product ON product.productid = productstockitem.productid
        WHERE ecommerceorder.ecommerceorderid = $ecommerceorderid AND ecommercelistinglocation.fulfillmentpartnerid = 0 AND ecommercelistinglocation.fulfillmentshippingruleid = 0 ";

        $allitems = mysql_query($allitems);

        $ecomlistinglocationids = array();
        //$shippinoptionnamearray = array();
        $productids = array();
        $totalpackageweight = 0;
        $totalpackageheight = 0;
        $totalpackagewidth = 0;
        $totalpackagelength = 0;
        $totalpackagevalue = 0;

        while($allitemsrow = mysql_fetch_array($allitems))
        {
            array_push($ecomlistinglocationids, $allitemsrow['listinglocationid']);
            //array_push($shippinoptionnamearray, $allitemsrow['shippingoptionname']);
            array_push($productids, $allitemsrow['productid']);
            $totalpackageweight += $allitemsrow['fulfillmentpackageweight'];
            $totalpackageheight += $allitemsrow['fulfillmentpackageheight'];
            $totalpackagewidth += $allitemsrow['fulfillmentpackagewidth'];
            $totalpackagelength += $allitemsrow['fulfillmentpackagelength'];
            $totalpackagevalue += $allitemsrow['retailprice'];
        }

        $shippingrulerecord = "SELECT * FROM fulfillmentshippingrule WHERE thirdpartydispatchname = '$shippingoptionname' ";
        echo "<br/>shippingrulerecord : ".$shippingrulerecord;
        $shippingrulerecord = mysql_query($shippingrulerecord);
        if(mysql_num_rows($shippingrulerecord) > 0)
        {
            $arr = array();

            while($fetchrow = mysql_fetch_row($shippingrulerecord))
            {
                $fulfillmentshippingruleid = $fetchrow['fulfillmentshippingruleid'];
                $fulfillmentshippingrulename = $fetchrow['fulfillmentshippingrulename'];
                $fulfillmentpartnerid = $fetchrow['fulfillmentpartnerid'];
                $priority = $fetchrow['priority'];
                $fulfillmentshippingrulemeasurementtypeid = $fetchrow['fulfillmentshippingrulemeasurementtypeid'];
                $kgweightminimum = $fetchrow['kgweightminimum'];
                $kgweightmaximum = $fetchrow['kgweightmaximum'];
                $widthmaximum = $fetchrow['widthmaximum'];
                $depthmaximum = $fetchrow['depthmaximum'];
                $heightmaximum = $fetchrow['heightmaximum'];
                $ordervalue = $fetchrow['ordervalue'];
                // $countryid = $fetchrow['countryid'];

                $flag = false;

                if($totalpackageweight >= $kgweightminimum && $totalpackageweight <= $kgweightmaximum)
                {
                    $flag = true;
                }
                else
                {
                    $flag = false;
                }
                if($fulfillmentshippingrulemeasurementtypeid == 1)
                {
                    // Width * Depth of parcel must be less than depthmaximum * widthmaximum
                    if(($totalpackagewidth * $totalpackageheight) < ($widthmaximum * $depthmaximum))
                    {
                        $flag = true;
                    }
                    else
                    {
                        $flag = false;
                    }
                }
                if($fulfillmentshippingrulemeasurementtypeid == 2)
                {
                    // Width + Depth + Height must be less than depthmaximum+widthmaximum+heightmaximum
                    if(($totalpackagewidth + $totalpackagelength + $totalpackageheight) < ($depthmaximum + $widthmaximum + $heightmaximum))
                    {
                        $flag = true;
                    }
                    else
                    {
                        $flag = false;
                    }
                }
                if($totalpackageheight < $heightmaximum)
                {
                    $flag = true;
                }
                else
                {
                    $flag = false;
                }
                if($totalpackagevalue <= $ordervalue)
                {
                    $flag = true;
                }
                else
                {
                    $flag = false;
                }

                if($flag)
                {
                    array_push($arr, array('fulfillmentpartnerid' => $fulfillmentpartnerid, 'fulfillmentshippingruleid' => $fulfillmentshippingruleid, 'priority' => $priority));
                }
            }

            if(count($arr) > 0)
            {
                foreach($arr as $val)
                {
                    if($val['priority'] == 1)
                    {
                        $fpid = $val['fulfillmentpartnerid'];
                        $fsrid = $val['fulfillmentshippingruleid'];
                        break;
                    }
                    elseif($val['priority'] == 2)
                    {
                        $fpid = $val['fulfillmentpartnerid'];
                        $fsrid = $val['fulfillmentshippingruleid'];
                        break;
                    }
                    else
                    {
                        $fpid = $val['fulfillmentpartnerid'];
                        $fsrid = $val['fulfillmentshippingruleid'];
                        break;
                    }
                }

                $ecomlistinglocationids = implode(",", $ecomlistinglocationids);
                $updatelistinglocation = "UPDATE ecommercelistinglocation SET fulfillmentpartnerid = '$fpid', fulfillmentshippingruleid = '$fsrid' WHERE ecommercelistinglocationid IN (".$ecomlistinglocationids.")";
                $updatelistinglocation = mysql_query($updatelistinglocation);
            }
        }
        else
        {
            // Add shipping rule exception

            $productids = implode(",", $productids);
            $ecomlistinglocationids = implode(",", $ecomlistinglocationids);

            $getshippingruleexception = "SELECT fulfillmentpartnerid, fulfillmentshippingruleid FROM fulfillmentshippingruleexception 
            WHERE productid1 IN ($productids) OR productid2 IN ($productids) OR productid3 IN ($productids) 
            OR productid4 IN ($productids) OR productid5 IN ($productids) OR productid6 IN ($productids) 
            OR productid7 IN ($productids) OR productid8 IN ($productids) OR productid9 IN ($productids) OR productid10 IN ($productids) ";
            $getshippingruleexception = mysql_query($getshippingruleexception);

            if(mysql_num_rows($getshippingruleexception) > 0) 
            {
                while($getshippingruleexceptionrow = mysql_fetch_array($getshippingruleexception))
                {
                    $fulfillmentshippingruleid = $getshippingruleexceptionrow['fulfillmentshippingruleid'];
                    $fulfillmentpartnerid = $getshippingruleexceptionrow['fulfillmentpartnerid'];

                    $updatelistinglocation = "UPDATE ecommercelistinglocation SET fulfillmentpartnerid = '$fulfillmentpartnerid', fulfillmentshippingruleid = '$fulfillmentshippingruleid' WHERE ecommercelistinglocationid IN (".$ecomlistinglocationids.")";
                    $updatelistinglocation = mysql_query($updatelistinglocation);
                }
            }
            else
            {
                $productids = explode(",", $productids);
                for($i=1;$i<=10;$i++)
                {
                    ${'product'.$i} = isset($productids[$i-1]) ? $productids[$i-1] : 0; 
                }
                $query = "INSERT INTO fulfillmentshippingruleexception (fulfillmentshippingruleexceptionname, disabled, 
                datecreated, masteronly, countryid, fulfillmentpartnerid, fulfillmentshippingruleid, productid1, productid2, productid3, productid4, productid5, productid6, productid7, productid8, productid9, productid10) 
                VALUES ('Exception - United Kingdom - $itemsinorder', 0, '$date', 0, 1000001, 0, 0, $product1, $product2, $product3, $product4, $product5, $product6, $product7, $product8, $product9, $product10)";

                echo "Insert shipping rule exception : ".$query."<br/>";
                $query = mysql_query($query);
            }
        }

    }
}

?>