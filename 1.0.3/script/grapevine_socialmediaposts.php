<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include('config.php'); // need update
$date = date('Y-m-d');
$datetime = date('Y-m-d H:i:s');
$min = date('i');
$day = date('l');
$hour = date('H');
echo "<br/>min: ".$min;
echo "<br/>day: ".$day;
echo "<br/>hour: ".$hour;
echo "<br/>datetime: ".$datetime;
$dateplus30 = date('Y-m-d', strtotime("+ 30 days"));
$date180 = date('Y-m-d', strtotime("- 180 days"));
$date20 = date('Y-m-d', strtotime("- 20 days"));
$date15 = date('Y-m-d', strtotime("- 15 days"));
$dateminus1 = date('Y-m-d', strtotime("- 1 days"));

//testing variables
//$hour = 18;
//$day = "Wednesday";

//get shopify config details
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$getsystemapi = mysql_query("select ecommercesiteconfigid, apikey1, apikey2, apikey3
from ecommercesiteconfig
where ecommercesiteconfigid = 1000001");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
//echo "<br/>Get list of news sources: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
while($systemapirow = mysql_fetch_array($getsystemapi)){
	$shopdomain = $systemapirow['apikey1'];
	$apikey = $systemapirow['apikey2'];
	$secret = $systemapirow['apikey3'];
}

//BLOG POST
if(($hour == 10 && $day == "Tuesday") || ($hour == 12 && $day == "Thursday") || ($hour == 13 && $day == "Saturday") 
|| ($hour == 18 && $day == "Wednesday") || ($hour == 14 && $day == "Monday") || ($hour == 19 && $day == "Friday")){
	echo "<br/><br/><b>BLOG POST</b><br/>";
	
	//get list of blog posts from shopify
	$publishedmin = $date20."T02:15:47-04:00";
	$url = "https://".$apikey.":".$secret."@".$shopdomain."/admin/api/2019-04/blogs/13146914874/articles.json?published_at_min=".$publishedmin;
	echo $url;	
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
	//comment out returntransfer line below to see the json response for debugging purposes
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			
	$result = curl_exec($ch);
	curl_close($ch);
	
	//update the status in ecommercelistinglocation
	//echo $result;
	$result = json_decode($result, true); 

	$numresults = count($result['articles']);
	echo "<br/>numresults: ".$numresults;
		
	if($numresults > 0){	
		$randnum = rand(1, $numresults);
		$randnum = $randnum - 1;
		echo "<br/>randnum: ".$randnum;
		
		//echo "<br/>article: ".json_encode($article);
		$id = $result['articles'][$randnum]['id'];	
		$title = $result['articles'][$randnum]['title'];	
		$published = $result['articles'][$randnum]['published_at'];	
		$handle = $result['articles'][$randnum]['handle'];	
		$link = "https://mumblebooks.co.uk/blogs/blog/".$handle;
		$imageurl = $result['articles'][$randnum]['image']['src'];	
		$ending = "jpeg";
		if(strpos($imageurl, '.png') !== false){
			$ending = "png";	
		}
		if(strpos($imageurl, '.jpeg') !== false){
			$ending = "jpeg";	
		}
		if(strpos($imageurl, '.jpg') !== false){
			$ending = "jpg";	
		}
		$randimagename = rand(1, 9000000000);
		
		//$local_file = "../onebusiness/documents/onebusinesstenant108business1000003/sf/socialpost/".$randimagename.".".$ending;
		$ch = curl_init();
		$fp = fopen ($local_file, 'w+');
		$ch = curl_init($imageurl);
		curl_setopt($ch, CURLOPT_TIMEOUT, 50);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_ENCODING, "");
		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_exec($ch);
		curl_close($ch); 
		fclose($fp);
		//$imageurl = "https://www.onebusiness-liveserver.com/onebusiness/documents/onebusinesstenant108business1000003/sf/socialpost/".$randimagename.".png";
	
		echo "<br/><br/>id: ".$id;
		echo "<br/>title: ".$title;
		echo "<br/>published: ".$published;
		echo "<br/>link: ".$link;
		echo "<br/>imageurl: ".$imageurl;
		
		$randtext = rand(1,5);
		if($randtext == 1){
			$text = "Check out this great blog post - ".$title." #blog #reading #book";
		}
		if($randtext == 2){
			$text = "Hope this blog post helps you - ".$title." #blog #reading #book";
		}
		if($randtext == 3){
			$text = "We have launched our latest blog post - ".$title." #blog #reading #book";
		}
		if($randtext == 4){
			$text = $title." - click link to view our latest blog post. #blog #reading #book";
		}
		if($randtext == 5){
			$text = "Check out our fantastic blog post - ".$title." #blog #reading #book";
		}
		
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$insertpost = "insert into $database.socialpost
		(socialpostname, disabled, datecreated, masteronly, businessunitid, text, imagevideolocation, link, sendoutdate, 
		socialpoststatusid, socialtwitteraccountid1, socialfacebookpageid1) values ('$text', '0', '$date', '0', '1000001', 
		'$text', '$imageurl', '$link', '$date', '1', '1000001', '1000000')";
		echo "<br/>insertpost: ".$insertpost;
		$insertpost = mysql_query($insertpost);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Insert into socialpost: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	}
}


//OFFER
if(($hour == 12 && $day == "Monday") || ($hour == 10 && $day == "Wednesday") || ($hour == 19 && $day == "Sunday")
 || ($hour == 13 && $day == "Thursday") || ($hour == 17 && $day == "Saturday")){
	echo "<br/><br/><b>OFFER</b><br/>";
	
	//get list of price rules from shopify
	$startsatmax = $date."T02:15:47-04:00";
	$endsatmin = $dateplus30."T02:15:47-04:00";
	$url = "https://".$apikey.":".$secret."@".$shopdomain."/admin/api/2019-04/price_rules.json?starts_at_max=".$startsatmax."&ends_at_min=".$endsatmin;
	echo $url;	
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
	//comment out returntransfer line below to see the json response for debugging purposes
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			
	$result = curl_exec($ch);
	curl_close($ch);
	
	//update the status in ecommercelistinglocation
	echo "<br/>pricerules: ".$result;
	
	$result = json_decode($result, true); 
	$id = "";
	foreach($result['price_rules'] as $pricerule){	
		$id = $pricerule['id'];	
		$customerselection = $pricerule['customer_selection'];	
		$targettype = $pricerule['target_type'];	
		$valuetype = $pricerule['value_type'];	
		$value = $pricerule['value'];
		$title = $pricerule['title'];
		
		echo "<br/><br/>id: ".$id;	
		echo "<br/>customerselection: ".$customerselection;	
		echo "<br/>targettype: ".$targettype;	
		echo "<br/>valuetype: ".$valuetype;	
		echo "<br/>value: ".$value;	
		echo "<br/>title: ".$title;	
		
		if($customerselection == "all"){		
			break;		
		}
	}
	
	if($id <> ""){
		$link = "https://mumblebooks.co.uk";
		if($valuetype == "percentage" && $targettype == "line_item"){
			$value = $value * -1;
			$imageurl = "https://www.onebusiness-liveserver.com/mumblebooks/PostImages/Offers/percentageoff.png";
			$text = "Check out our latest offer - ".$value."% off all orders at Mumble Books. Visit our store today to take advantage of the offer. #book #sale #offer #discount #moneyoff";
		}
		
		if($valuetype == "fixed_amount"){
			$value = $value * -1;
			$imageurl = "https://www.onebusiness-liveserver.com/mumblebooks/PostImages/Offers/pricecut.png";
			$text = "Check out our latest offer - £".$value." off all orders at Mumble Books. Visit our store today to take advantage of the offer. #book #sale #offer #discount #moneyof";
		}
		
		if($valuetype == "percentage" && $targettype == "shipping_line"){
			$value = $value * -1;
			$imageurl = "https://www.onebusiness-liveserver.com/mumblebooks/PostImages/Offers/freeshipping.png";
			$text = "Check out our latest offer - free shipping on all orders at Mumble Books. Visit our store today to take advantage of the offer. #book #sale #offer #discount #moneyof";
		}
		
		
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$insertpost = "insert into $database.socialpost
		(socialpostname, disabled, datecreated, masteronly, businessunitid, text, imagevideolocation, link, sendoutdate, 
		socialpoststatusid, socialtwitteraccountid1, socialfacebookpageid1) values ('$text', '0', '$date', '0', '1000001', 
		'$text', '$imageurl', '$link', '$date', '1', '1000001', '1000000')";
		echo "<br/>insertpost: ".$insertpost;
		$insertpost = mysql_query($insertpost);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Insert into socialpost: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	}
	
}


//BOOK OF THE DAY
if(($hour == 12 && ($day == "Monday" || $day == "Saturday"))
|| ($hour == 18 && ($day == "Tuesday" || $day == "Thursday" || $day == "Sunday"))){
	echo "<br/><br/><b>BOOK OF THE DAY</b><br/>";
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getbookoftheday = mysql_query("select product.productid, quantity, productname, productproducer, 
	websiteurl, product.productsubcategoryid, productsubcategoryname, documenturl from zzdropshipstock 
	inner join product on product.productid = zzdropshipstock.productid
	inner join productsubcategory on product.productsubcategoryid = productsubcategory.productsubcategoryid
	inner join ecommercelistinglocation on ecommercelistinglocation.productid = zzdropshipstock.productid
	inner join structurefunctiondocument on structurefunctiondocument.rowid = product.productid
	where quantity >= 50 and websiteurl <> '' and ecommercesiteconfigid = 1000001 and productcategoryid = 1000003 and doctypename = 'Primary Image' and productlongdescription <> ''
	order by rand()
	limit 1");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get book of the day: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($getbookofthedayrow = mysql_fetch_array($getbookoftheday)){
		$productname = $getbookofthedayrow['productname'];
		$productproducer = $getbookofthedayrow['productproducer'];
		$link = $getbookofthedayrow['websiteurl'];
		$imageurl = "https://www.onebusiness-liveserver.com/onebusiness/documents/".$getbookofthedayrow['documenturl'];
		$text = "Book of the day - ".$productname. " by ".$productproducer." - check it out at Mumble Books #book #author #reading";
		
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$insertpost = "insert into $database.socialpost
		(socialpostname, disabled, datecreated, masteronly, businessunitid, text, imagevideolocation, link, sendoutdate, 
		socialpoststatusid, socialtwitteraccountid1, socialfacebookpageid1) values ('$text', '0', '$date', '0', '1000001', 
		'$text', '$imageurl', '$link', '$date', '1', '1000001', '1000000')";
		echo "<br/>insertpost: ".$insertpost;
		$insertpost = mysql_query($insertpost);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Insert into socialpost: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	}
}


//USED BOOKS SECTION
if($hour == 18 && ($day == "Wednesday" || $day == "Sunday")){
	echo "<br/><br/><b>USED BOOKS SECTION</b><br/>";
	
	$randtext = rand(1,4);
	if($randtext == 1){
		$text = "We have launched a Used Books section on the website. Check it out here. #books #reading #secondhand #offer";
	}
	if($randtext == 2){
		$text = "Used books from 1 penny available on Mumble Books. Visit website. #books #reading #secondhand #offer";
	}
	if($randtext == 3){
		$text = "Our pre-owned books section is a great place to find a bargain. #books #reading #secondhand #offer";
	}
	if($randtext == 4){
		$text = $title."Looking for a cheap book - check out our pre-loved used books section. #books #reading #secondhand #offer";
	}
	
	$randimage = rand(1,3);
	if($randimage == 1){
		$imageurl = "https://www.onebusiness-liveserver.com/mumblebooks/PostImages/UsedBooks/usedbooks1.png";
	}
	if($randimage == 2){
		$imageurl = "https://www.onebusiness-liveserver.com/mumblebooks/PostImages/UsedBooks/usedbooks2.png";
	}
	if($randimage == 3){
		$imageurl = "https://www.onebusiness-liveserver.com/mumblebooks/PostImages/UsedBooks/usedbooks3.png";
	}
	
	$link = "https://mumblebooks.co.uk/collections/used-books";
		
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$insertpost = "insert into $database.socialpost
	(socialpostname, disabled, datecreated, masteronly, businessunitid, text, imagevideolocation, link, sendoutdate, 
	socialpoststatusid, socialtwitteraccountid1, socialfacebookpageid1) values ('$text', '0', '$date', '0', '1000001', 
	'$text', '$imageurl', '$link', '$date', '1', '1000001', '1000000')";
	echo "<br/>insertpost: ".$insertpost;
	$insertpost = mysql_query($insertpost);
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Insert into socialpost: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
}


//AUTHOR QUOTES
if(($hour == 12 && $day == "Monday") || ($hour == 16 && $day == "Saturday") 
|| ($hour == 19 && $day == "Wednesday")){
	echo "<br/><br/><b>AUTHOR QUOTES</b><br/>";
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getauthorquote = mysql_query("select distinct socialpostname, text, imagevideolocation, link from socialpost 
	where sendoutdate <= '$date15' and socialpostname like '%Author Quotes - %' 
	and socialpost.disabled = 0
	order by rand()
	limit 1");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get book of the day: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($getauthorquoterow = mysql_fetch_array($getauthorquote)){
		$socialpostname = $getauthorquoterow['socialpostname'];
		$link = $getauthorquoterow['link'];
		$text = $getauthorquoterow['text'];
		$imageurl = $getauthorquoterow['imagevideolocation'];
		
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$insertpost = "insert into $database.socialpost
		(socialpostname, disabled, datecreated, masteronly, businessunitid, text, imagevideolocation, link, sendoutdate, 
		socialpoststatusid, socialtwitteraccountid1, socialfacebookpageid1) values ('$socialpostname', '0', '$date', '0', '1000001', 
		'$text', '$imageurl', '$link', '$date', '1', '1000001', '1000000')";
		echo "<br/>insertpost: ".$insertpost;
		$insertpost = mysql_query($insertpost);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Insert into socialpost: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	}
}


//READING QUOTES
if(($hour == 12 && $day == "Monday") || ($hour == 14 && $day == "Sunday") 
|| ($hour == 20 && $day == "Thursday")){
	echo "<br/><br/><b>READING QUOTES</b><br/>";
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getauthorquote = mysql_query("select distinct socialpostname, text, imagevideolocation, link from socialpost 
	where sendoutdate <= '$date15' and socialpostname like '%Reading Quotes - %' 
	and socialpost.disabled = 0
	order by rand()
	limit 1");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get book of the day: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($getauthorquoterow = mysql_fetch_array($getauthorquote)){
		$socialpostname = $getauthorquoterow['socialpostname'];
		$link = $getauthorquoterow['link'];
		$text = $getauthorquoterow['text'];
		$imageurl = $getauthorquoterow['imagevideolocation'];
		
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$insertpost = "insert into $database.socialpost
		(socialpostname, disabled, datecreated, masteronly, businessunitid, text, imagevideolocation, link, sendoutdate, 
		socialpoststatusid, socialtwitteraccountid1, socialfacebookpageid1) values ('$socialpostname', '0', '$date', '0', '1000001', 
		'$text', '$imageurl', '$link', '$date', '1', '1000001', '1000000')";
		echo "<br/>insertpost: ".$insertpost;
		$insertpost = mysql_query($insertpost);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Insert into socialpost: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	}
}


//INFOGRAPHIC
if(($hour == 12 && $day == "Monday") || ($hour == 14 && $day == "Saturday") 
|| ($hour == 19 && $day == "Wednesday")){
	echo "<br/><br/><b>INFOGRAPHIC</b><br/>";
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getinfographic = mysql_query("select distinct socialpostname, text, imagevideolocation, link from socialpost 
	where sendoutdate <= '$date15' and socialpostname like '%Infographic - %' 
	and socialpost.disabled = 0
	order by rand()
	limit 1");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get book of the day: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($getinfographicrow = mysql_fetch_array($getinfographic)){
		$socialpostname = $getinfographicrow['socialpostname'];
		$link = $getinfographicrow['link'];
		$text = $getinfographicrow['text'];
		$imageurl = $getinfographicrow['imagevideolocation'];
		
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$insertpost = "insert into $database.socialpost
		(socialpostname, disabled, datecreated, masteronly, businessunitid, text, imagevideolocation, link, sendoutdate, 
		socialpoststatusid, socialtwitteraccountid1, socialfacebookpageid1) values ('$socialpostname', '0', '$date', '0', '1000001', 
		'$text', '$imageurl', '$link', '$date', '1', '1000001', '1000000')";
		echo "<br/>insertpost: ".$insertpost;
		$insertpost = mysql_query($insertpost);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Insert into socialpost: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	}
}


//AUTHOR SHARE
if(($hour == 13 && $day == "Monday") || ($hour == 15 && $day == "Saturday") 
|| ($hour == 21 && $day == "Wednesday")){
	echo "<br/><br/><b>AUTHOR SHARE</b><br/>";
	
	$randtext = rand(1,3);
	if($randtext == 1){
		$text = "Calling all authors - do you have an upcoming book launch - get intouch for us to help you promote! #author #reading";
	}
	if($randtext == 2){
		$text = "Calling all authors - do you have a video you would like us to share with our followers? Get in touch. #author #reading";
	}
	if($randtext == 3){
		$text = "Calling all authors - let us know if you have some content / web page / blog you wish us to promote. #author #reading";
	}
	
	$randimage = rand(1,3);
	if($randimage == 1){
		$imageurl = "https://www.onebusiness-liveserver.com/mumblebooks/PostImages/AuthorShare/authorshare1.png";
	}
	if($randimage == 2){
		$imageurl = "https://www.onebusiness-liveserver.com/mumblebooks/PostImages/AuthorShare/authorshare2.png";
	}
	if($randimage == 3){
		$imageurl = "https://www.onebusiness-liveserver.com/mumblebooks/PostImages/AuthorShare/authorshare3.png";
	}
	
		
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$insertpost = "insert into $database.socialpost
	(socialpostname, disabled, datecreated, masteronly, businessunitid, text, imagevideolocation, link, sendoutdate, 
	socialpoststatusid, socialtwitteraccountid1, socialfacebookpageid1) values ('$text', '0', '$date', '0', '1000001', 
	'$text', '$imageurl', '', '$date', '1', '1000001', '1000000')";
	echo "<br/>insertpost: ".$insertpost;
	$insertpost = mysql_query($insertpost);
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Insert into socialpost: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
}


//GOOGLE NEWS
/*
if(($hour == 14 && $day == "Tuesday") 
|| ($hour == 15 && $day == "Wednesday") 
|| ($hour == 19 && $day == "Friday") 
|| ($hour == 12 && $day == "Sunday")){
	echo "<br/><br/><b>GOOGLE NEWS</b><br/>";
	
	$counter = -1;
	$newsarray = array();
	
	$rssfeedurl = "https://news.google.com/news/rss/search/section/q/books/books?hl=en-GB&gl=GB&ceid=GB:en";
	$feed = simplexml_load_file($rssfeedurl);
	//print_r($feed);
	//echo "<br/><br/>";
	
	//google news syndicated
	foreach ($feed->channel->item as $item) {
	  	$link = (string) $item->link;
	   $title       = (string) $item->title;
	  	$pubdate = (string) $item->pubDate;
	  	$timestamp = strtotime($pubdate);
		$datetime = date('Y-m-d H:i:s', $timestamp);
	  	$title = str_replace("'", "", $title);
	  	$title = str_replace('"', "", $title);
	  	$title = str_replace('’', "", $title);
	  	$title = str_replace('‘', "", $title);
	  	$title = str_replace('&', "and", $title);
	  	
	  	//$invalidcharacters = array("é","è","Ö","ä","à");
	  	$invalidcharacters = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 
	  	'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 
	  	'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 
	  	'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 
	  	'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 
	  	'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'Ά', 'ά', 'Έ', 'έ', 'Ό', 'ό', 'Ώ', 'ώ', 'Ί', 'ί', 
	  	'ϊ', 'ΐ', 'Ύ', 'ύ', 'ϋ', 'ΰ', 'Ή', 'ή');
  
		$add = 1;
		foreach($invalidcharacters as $char){
			if (strpos($title, $char) !== false) {
			  	//echo "char: ".$char.' - Match found - do not add<br/>';
			  	$add = $add + 1;			    
			}
		}
		$date1 = date("Y-m-d",strtotime($datetime));
		
		//work out whether to skip some articles
		$skip = 0;
		$skipreason = "";
		if(stripos($title, 'waterstone') !== false) {
    		$skip = 1;
    		$skipreason = $skipreason."Includes 'Waterstone'. ";
		}
		if(stripos($title, 'amazon') !== false) {
    		$skip = 1;
    		$skipreason = $skipreason."Includes 'Amazon'. ";
		}
		if(stripos($title, 'foyles') !== false) {
    		$skip = 1;
    		$skipreason = $skipreason."Includes 'Foyles'. ";
		}
		if(stripos($title, 'sex') !== false) {
    		$skip = 1;
    		$skipreason = $skipreason."Includes 'Sex'. ";
		}
		if(stripos($title, 'porn') !== false) {
    		$skip = 1;
    		$skipreason = $skipreason."Includes 'Porn'. ";
		}
		if($date1 <> $date && $date1 <> $dateminus1){
			$skip = 1;
    		$skipreason = $skipreason."Not new article today/yesterday. ";
		}
		
		if($skip == 0){
			echo "<br/><br/><b>Feed Item: ".$link."</b>";
		  	echo "<br/>date1: ".$date1;
		  	echo "<br/>title: ".$title;
		  	$counter = $counter + 1;
		  	array_push($newsarray, array("link" => $link, "title" => $title));
	  	}
	  	else {
			//echo "<br/><br/><b>Feed Item: ".$link."</b>";
		  	//echo "<br/>date1: ".$date1;
		  	//echo "<br/>title: ".$title;
		  	//echo "<br/>skip: ".$skip;
		  	//echo "<br/>skipreason: ".$skipreason;	  	
	  	}
	  			
	}	
	
	$randarticle = rand(0,$counter);
  	//echo "<br/><br/>newsarray: ".json_encode($newsarray);
  	
  	echo "<br/><br/><b>SELECT LINK AND PROCESS IT INTO A POST</b>";
  	if(isset($newsarray[$randarticle]['link'])){
  		$selectedlink = $newsarray[$randarticle]['link'];
  		$selectedtitle = $newsarray[$randarticle]['title'];
  		echo "<br/><br/>selectedlink: ".$selectedlink;
  		echo "<br/>selectedtitle: ".$selectedtitle;
  		$text = "News - ".$selectedtitle;
  		
  		$randimage = rand(1,3);
		if($randimage == 1){
			$imageurl = "https://www.onebusiness-liveserver.com/mumblebooks/PostImages/News/news1.png";
		}
		if($randimage == 2){
			$imageurl = "https://www.onebusiness-liveserver.com/mumblebooks/PostImages/News/news2.png";
		}
		if($randimage == 3){
			$imageurl = "https://www.onebusiness-liveserver.com/mumblebooks/PostImages/News/news3.png";
		}
	
  		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$insertpost = "insert into $database.socialpost
		(socialpostname, disabled, datecreated, masteronly, businessunitid, text, imagevideolocation, link, sendoutdate, 
		socialpoststatusid, socialtwitteraccountid1, socialfacebookpageid1) values ('$text', '0', '$date', '0', '1000001', 
		'$selectedtitle', '$imageurl', '$selectedlink', '$date', '1', '1000001', '1000000')";
		echo "<br/>insertpost: ".$insertpost;
		$insertpost = mysql_query($insertpost);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Insert into socialpost: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
  		
  	}			  			
}
*/

echo "<br/><br/>";
include('recordjobstatistics.php');

?>