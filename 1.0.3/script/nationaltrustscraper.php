<?php

//  Create a new file for writing
$newFile = fopen("nationaltrust.csv", "w");

// for($i=1;$i<=38;$i++)
// {
    // Scan the website
    $url = 'https://www.nationaltrust.org.uk/search?type=place&view=list&pageIndex=54&pageSize=610';    
    $text = file_get_contents($url);
    //echo "Text:".$text;
    $dom = new DOMDocument;
    @$dom->loadHTML($text);

    $classnameForUrl = "nt-link-chevron app-search-result-link";
    $classnameForLocation = "nt-masonry-single-result-location";

    $xpath = new DOMXpath($dom);

    // Fetch the node value (for website) having class name = 'wiwebsite' (direct call)
    $data = $xpath->query("//a[contains(@class, '$classnameForUrl')]");
    $dataForLocation = $xpath->query("//div[contains(@class, '$classnameForLocation')]");
    // Fetch the required p tag (for Postcode) inside li which is inside ul tag with class name = 'wi_details' (indirect call)
    //$extralinks = $xpath->query("//h3[@class='m-card__title h2']");

    // Used for reference --> $xpath->query(".//ul[@class='pages']/li[last()]/a/text()")->item(0)->nodeValue;

    $datanamearray = array();
    $dataurlarray = array();
    $datalocationarray = array();

    foreach($data as $result)
    {
        // $row = array();
        echo $result->textContent;
        echo "\t";
        echo ($result->getAttribute('href'));
        echo "<br/>";
        array_push($datanamearray, $result->textContent);
        array_push($dataurlarray, $result->getAttribute('href'));
        
        //  write data into new file
        //fputcsv($newFile, array_values($row));

    }

    foreach($dataForLocation as $result)
    {
        // $row = array();
        echo $result->nodeValue;
        echo "<br/>";
        array_push($datalocationarray, $result->nodeValue);
        // array_push($row, $branch->parentNode->getAttribute('href'));

    }

    //$wholedata = array_merge($datanamearray, $dataurlarray, $datalocationarray);
    // var_dump($wholedata);
    
    $count = count($datanamearray);
    for ($i = 0; $i < $count; $i++)
    {
        $wholedata = array();
        
        $wholedata[] = $datanamearray[$i];
        $wholedata[] = $dataurlarray[$i];
        $wholedata[] = $datalocationarray[$i];

        fputcsv($newFile, array_values($wholedata));
    }

// }


fclose($newFile);

?>