-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 20, 2014 at 12:04 AM
-- Server version: 5.5.38-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `social_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `facebook_post`
--

CREATE TABLE IF NOT EXISTS `facebook_post` (
  `facebook_post_id` int(11) NOT NULL AUTO_INCREMENT,
  `true_facebook_post_id` varchar(255) DEFAULT NULL,
  `facebook_page_id` bigint(20) NOT NULL,
  `status` text NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `send_on` varchar(255) NOT NULL,
  `sent_on` varchar(255) DEFAULT NULL,
  `facebook_post_pic` varchar(255) DEFAULT NULL,
  `post_likes` bigint(20) DEFAULT NULL,
  `last_updated` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`facebook_post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
