<?php

//LANGUAGE COLLECTION SECTION
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (715,716,717,718,719)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}

echo "<h2>".$projecttypename." ".$langval715."</h2>";

if($projecttypename == 'Project'){
	$hobject = "select projectphaseid as 'objectid', projectphasename as 'objectname', description, phasestartdate as 'startdate', phaseenddate as 'enddate',  
	progresspercentage, 'Phase' as 'Type' from $database.projectphase 
	where projectid = '$rowid' and projectphase.disabled='0'
	UNION
	select projectmilestoneid as 'objectid', projectmilestonename as 'objectname', description, milestonedate as 'startdate', milestonedate as 'enddate', 0 as progresspercentage,  
	'Milestone' as 'Type' from $database.projectmilestone
	where projectid = '$rowid' and projectmilestone.disabled='0'
	order by startdate";
}
if($projecttypename == 'Programme' || $projecttypename == 'Portfolio'){
	$hobject = "select projectphaseid as 'objectid', projectphasename as 'objectname', description, phasestartdate as 'startdate', phaseenddate as 'enddate',  
	progresspercentage, 'Programme Phase' as 'Type' from $database.projectphase 
	where projectid = '$rowid' and projectphase.disabled='0'
	UNION
	select project.projectid as 'objectid', projectname as 'objectname', description, startdate as 'startdate', enddate as 'enddate',  
	progresspercentage, 'Project' as 'Type'  from $database.projectgrouping 
	inner join $database.project on project.projectid = projectgrouping.projectid
	where masterprojectid = '$rowid' and projectgrouping.disabled='0' and project.disabled='0'
	UNION
	select projectmilestoneid as 'objectid', projectmilestonename as 'objectname', description, milestonedate as 'startdate', milestonedate as 'enddate', 0 as progresspercentage,  
	'Milestone' as 'Type' from $database.projectmilestone
	where projectid = '$rowid' and projectmilestone.disabled='0'
	order by startdate";
}

//get the minimum and maximum dates for the columns
$mindate = date("Y-m-d");
$maxdate = date("Y-m-d");
$getobject = mysql_query($hobject);
$i = 1;
while ($dates = mysql_fetch_array($getobject)){
	$startdate = $dates['startdate'];
	$enddate = $dates['enddate'];
	if($i == 1){
		$mindate = $startdate;
	}
	else {
		if($startdate <= $mindate){
			$mindate = $startdate;		
		}	
	}
	if($i == 1){
		$maxdate = $enddate;
	}
	else {
		if($enddate >= $maxdate){
			$maxdate = $enddate;		
		}	
	}
	$i = $i + 1;
}
$maxd = strtotime($maxdate);
$mind = strtotime($mindate);
$dateDiff = $maxd - $mind;
$numdays = floor($dateDiff/(60*60*24));
$headingdates = '';
if($numdays <= 12){
	$thisdate = date('d-M', $mind);
	for($p = 0; $p <= $numdays; $p++) {
		$headingdates = $headingdates."<td style='border: 1px solid #D3D3D3;padding:5px;'><b>".$thisdate."</b></td>";
		$thisdate = date('d-M',date(strtotime("+1 day", strtotime($thisdate))));
	}	
}
if($numdays > 12 && $numdays <= 84){
	$thisdate = date('d-M', $mind);
	$num2 = $numdays / 7;
	for($p = 0; $p <= $num2; $p++) {
		$headingdates = $headingdates."<td colspan='7' style='border: 1px solid #D3D3D3;padding:5px;'><b>".$thisdate."</b></td>";
		$thisdate = date('d-M',date(strtotime("+7 day", strtotime($thisdate))));
	}	
}
if($numdays > 84 && $numdays <= 360){
	$thisdate = date('d-M-y', $mind);
	$num2 = ($numdays / 30) + 1;
	for($p = 0; $p <= $num2; $p++) {
		$colspan = date('t', strtotime($thisdate));
		$thisdate2 = date('M-y', strtotime($thisdate));
		$headingdates = $headingdates."<td colspan='".$colspan."' style='border: 1px solid #D3D3D3;padding:5px;'><b>".$thisdate2."</b></td>";
		$thisdate = date('d-M-y',date(strtotime("+1 month", strtotime($thisdate))));
	}	
}
if($numdays > 360){
	$thisdate = date('d-M-Y', $mind);
	$num2 = ($numdays / 365) +1;
	for($p = 0; $p <= $num2; $p++) {
		$colspan = date('z', strtotime($thisdate));
		$thisdate2 = date('Y', strtotime($thisdate));
		$headingdates = $headingdates."<td colspan='".$colspan."' style='border: 1px solid #D3D3D3;padding:5px;'><b>".$thisdate2."</b></td>";
		$thisdate = date('d-M-Y',date(strtotime("+1 year", strtotime($thisdate))));
	}	
}

//echo $mindate." - ".$maxdate." - ".$numdays."<br/>";

//create table of timeline
$getobject = mysql_query($hobject);
if(mysql_num_rows($getobject)>= 1){
	echo "<table style='border: 1px solid #D3D3D3;width:100%;overflow:scroll;'>";
	echo "<thead style='border: 1px solid #D3D3D3;'><td style='padding:5px;'><b>Phase</b></td>".$headingdates."</thead>";
	echo "<tr>";
	$numdays = $numdays + 1;
	for($s = 0; $s <= $numdays; $s++) {
		echo "<td style='color:white;font-size:1px;border: 0px solid #D3D3D3;''>I</td>";
	}	
	echo "</tr>";
	while ($object = mysql_fetch_array($getobject)){
		$objectid = $object['objectid'];
		$objectname = $object['objectname'];
		$startdate = $object['startdate'];
		$enddate = $object['enddate'];
		$description = $object['description'];
		$ppercentage = $object['progresspercentage'];
		$type = $object['Type'];
		//echo $type." - ".$objectname." - ".$startdate." - ".$enddate." - ".$description." - ".$ppercentage."<br/>";
		$indentobjectname = $objectname;
		if($projecttypename == 'Programme' || $projecttypename == 'Portfolio'){
			if($type == 'Milestone' || $type == 'Project'){
				$indentobjectname = '&nbsp;&nbsp;&nbsp;&nbsp;'.$indentobjectname;
			}
		}
		$pagename = $_SERVER['REQUEST_URI'];
		$pagename = str_replace("&", "xxxxxxxxxx", $pagename);								
		if($type == 'Milestone'){
			echo "<tr><td style='width:20%;padding:5px;border-right: 1px solid #D3D3D3;''><b>".$indentobjectname."</b> ";
			if($allowprojectedit >= 1){
				echo "<a href='pageedit.php?pagetype=projectmilestone	&rowid=".$objectid."&pagename=".$pagename."'><i class='fa fa-pencil'></i></a> <a href='pagechangestatus.php?pagetype=projectmilestone&rowid=".$objectid."&action=deactivate&pagename=".$pagename."'><i class='fa fa-trash'></i></a>";
			}	
			echo "</td>";		
		}
		if($type == 'Project'){
			echo "<tr><td style='width:20%;padding:5px;border-right: 1px solid #D3D3D3;''><b>".$indentobjectname."</b> ";
			if($allowprojectedit >= 1){
				echo "<a href='pageedit.php?pagetype=project&rowid=".$objectid."&pagename=".$pagename."'><i class='fa fa-pencil'></i></a> <a href='pagechangestatus.php?pagetype=project&rowid=".$objectid."&action=deactivate&pagename=".$pagename."'><i class='fa fa-trash'></i></a>";
			}
			echo "</td>";
		}
		if($type == 'Phase' || $type == 'Programme Phase'){
			echo "<tr><td style='width:20%;padding:5px;border-right: 1px solid #D3D3D3;''><b>".$indentobjectname."</b> ";
			if($allowprojectedit >= 1){
				echo "<a href='pageedit.php?pagetype=projectphase&rowid=".$objectid."&pagename=".$pagename."'><i class='fa fa-pencil'></i></a> <a href='pagechangestatus.php?pagetype=projectphase&rowid=".$objectid."&action=deactivate&pagename=".$pagename."'><i class='fa fa-trash'></i></a>"; 
			}
			echo "</td>";
		}
		$d = $mindate;
		$rows = '';
		while (strtotime($d) <= strtotime($maxdate)) {
			if($d == $startdate){
				$startd = strtotime($startdate);
				$endd = strtotime($enddate);
				$dateDiff2 = $endd - $startd;
				$nd = floor($dateDiff2/(60*60*24));
				if($nd == 0){
					$nd = 1;			
				}
				$title = $objectname."&#13;&#10;".$description."&#13;&#10;".$startdate." - ".$enddate."&#13;&#10;".$ppercentage."% ".$langval716;
				if($type == 'Milestone'){
					$color = '#000000';
				}
				else {
					if($ppercentage==0){ $color = '#FF0000'; }
					if($ppercentage>=1 && $ppercentage <=10){ $color = '#FF0000'; }
					if($ppercentage>=11 && $ppercentage <=20){ $color = '#FF3300'; }
					if($ppercentage>=21 && $ppercentage <=30){ $color = '#ff6600'; }
					if($ppercentage>=31 && $ppercentage <=40){ $color = '#FFCC00'; }
					if($ppercentage>=41 && $ppercentage <=50){ $color = '#FFFF00'; }
					if($ppercentage>=51 && $ppercentage <=60){ $color = '#ccff00'; }
					if($ppercentage>=61 && $ppercentage <=70){ $color = '#99ff00'; }
					if($ppercentage>=71 && $ppercentage <=80){ $color = '#66ff00'; }
					if($ppercentage>=81 && $ppercentage <=90){ $color = '#33ff00'; }
					if($ppercentage>=91 && $ppercentage <=100){ $color = '#00FF00'; }
				}
				$rows = $rows."<td title='".$title."' colspan='".$nd."' style='background-color:".$color.";color:".$color.";'></td>";				
			}      
			else {
				$rows = $rows."<td></td>";	
			} 	
       	$d = date ("Y-m-d", strtotime("+1 day", strtotime($d)));
		}
		echo $rows;
		echo "</tr>";
	}
	echo "</table>";
	if($allowprojectedit >= 1){
		if($projecttypename == 'Programme' || $projecttypename == 'Portfolio'){
			echo "<table class='table table-bordered'>";
			echo "<tr><td>";
			echo "<a class='button-primary' href='pageadd.php?pagetype=projectphase&pagename=".$pagename."'>".$langval717."</a>";
			echo "&nbsp;&nbsp;<a class='button-primary' href='pageadd.php?pagetype=projectgrouping&pagename=".$pagename."'>".$langval718."</a>";
			echo "</td></tr>";
			echo "</table>";
		}
		else {
			echo "<table class='table table-bordered'>";
			echo "<tr><td>";
			echo "<a class='button-primary' href='pageadd.php?pagetype=projectphase&pagename=".$pagename."'>".$langval717."</a>";
			echo "&nbsp;&nbsp;<a class='button-primary' href='pageadd.php?pagetype=projectmilestone&pagename=".$pagename."'>".$langval719."</a>";
			echo "</td></tr>";
			echo "</table>";
		}
	}
	
}
?>