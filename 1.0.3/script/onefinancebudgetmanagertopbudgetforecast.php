<?php

$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (5,103,104)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}

$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
if(isset($_POST['submit'])){
	$financialbudgetperiodid = isset($_POST['financialbudgetperiodid']) ? $_POST['financialbudgetperiodid'] : '';
	$businessunitid = isset($_POST['businessunitid']) ? $_POST['businessunitid'] : '';
	$budgetforecast = mysql_query("select * from budgetforecast 
	where businessunitid = '$businessunitid' and financialbudgetperiodid = '$financialbudgetperiodid'");
	$budgetforecastrow = mysql_fetch_array($budgetforecast);
	$budgetforecastid = $budgetforecastrow['budgetforecastid'];	
	$url = 'view.php?viewid=13&budgetforecastid='.$budgetforecastid;
	echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">'; 		
}
else {
	$financialbudgetperiodid = isset($_GET['financialbudgetperiodid']) ? $_GET['financialbudgetperiodid'] : '';
	$businessunitid = isset($_GET['businessunitid']) ? $_GET['businessunitid'] : '';
	$budgetforecastid = isset($_GET['budgetforecastid']) ? $_GET['budgetforecastid'] : '';
}

//get budgetforecast record
if($budgetforecastid > 0){
	$budgetforecast = mysql_query("select * from budgetforecast where budgetforecastid = '$budgetforecastid'");
	$budgetforecastrow = mysql_fetch_array($budgetforecast);
	$businessunitid = $budgetforecastrow['businessunitid'];
	$financialbudgetperiodid = $budgetforecastrow['financialbudgetperiodid'];
	$budgetforecaststatusid = $budgetforecastrow['budgetforecaststatusid'];
}
else {
	$budgetforecast = mysql_query("select * from budgetforecast 
	where businessunitid = '$businessunitid' and financialbudgetperiodid = '$financialbudgetperiodid'");
	$budgetforecastrow = mysql_fetch_array($budgetforecast);
	$budgetforecastid = $budgetforecastrow['budgetforecastid'];
}

//echo $financialbudgetperiodid."<br/>";
//echo $businessunitid."<br/>";
//echo $budgetforecastid."<br/>";

//check I am allowed to see this business unit
//get users business units
$bulist = $_SESSION["userbusinessunit"];			
$array = explode(',', $bulist);
$check = 0;
foreach($array as $value) {
	if($value == $businessunitid){
		$check = $check + 1;
	}
}
if($check == 0 && $businessunitid >= 1 && $bulist <> ''){
	die;
}


//get number of budget years to display
$getconfig = mysql_query("select * from $database.onefinanceconfiguration where disabled = 0");
$getconfigrow = mysql_fetch_array($getconfig);
$budgetmanagernumyearsback = $getconfigrow['budgetmanagernumyearsback'];
$str = "-".$budgetmanagernumyearsback." years";
$dateadjust = date('Y-m-d', strtotime($str));
			
?>
<form class="form-horizontal" action='view.php?viewid=13' method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label class="col-xs-12 col-sm-3 col-md-3 col-lg-2 control-label"><?php echo $langval103 ?></label>			
		<div class="col-xs-12 col-md-5 col-sm-5 col-lg-5">
			<select class='form-control' name='financialbudgetperiodid' id='action'>
			<?php				
			$querytype = "select financialbudgetperiodid, concat(financialbudgettypename,' - ',financialbudgetname,' - ',financialbudgetperiodname)
			as 'financialbudgetperiodname' from $database.financialbudgetperiod 
			inner join $database.financialbudget on financialbudget.financialbudgetid = financialbudgetperiod.financialbudgetid
			inner join $database.financialbudgettype on financialbudgettype.financialbudgettypeid = financialbudget.financialbudgettypeid
			where financialbudgetperiod.disabled = 0 and activatebudgetsetup = '1'
			order by enddate desc";		    		  	
			$resulttype = mysql_query($querytype);
			echo "<option value=''>".$langval19."</option>";
			while($nttype=mysql_fetch_array($resulttype)){
				if($financialbudgetperiodid == $nttype['financialbudgetperiodid']){
					echo "<option value='".$nttype['financialbudgetperiodid']."' selected='true'>".$nttype['financialbudgetperiodname']."</option>";	
				}
				else{
					echo "<option value='".$nttype['financialbudgetperiodid']."'>".$nttype['financialbudgetperiodname']."</option>";	
				}
			}
			?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-xs-12 col-sm-3 col-md-3 col-lg-2 control-label"><?php echo $langval104 ?></label>			
		<div class="col-xs-12 col-md-5 col-sm-5 col-lg-5">
			<select class='form-control' name='businessunitid' id='action'>
			<?php
			$querybu = "select businessunitid, businessunitname from businessunit where disabled = 0";
			if($bulist <> ""){
				$querybu = $querybu." and businessunitid in ($bulist)"; 
			}  	
			echo $querybu;	  	
			$resultbu = mysql_query($querybu);
			echo "<option value=''>".$langval19."</option>";
			while($ntbu=mysql_fetch_array($resultbu)){
				if($businessunitid == $ntbu['businessunitid']){
					echo "<option value='".$ntbu['businessunitid']."' selected='true'>".$ntbu['businessunitname']."</option>";	
				}
				else{
					echo "<option value='".$ntbu['businessunitid']."'>".$ntbu['businessunitname']."</option>";	
				}
			}
			?>
			</select>
		</div>
	</div>
	<div class="form-group">
	 	<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
	 		<label class="col-xs-12 col-sm-3 col-md-3 col-lg-2 control-label"></label>
	 		<button type='submit' name='submit' value='Submit' class="button-primary"><?php echo $langval5 ?></button>																									
	 	</div>
	</div>
</form>
