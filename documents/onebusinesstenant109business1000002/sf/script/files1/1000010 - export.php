<?php
$file = isset($_GET['file']) ? $_GET['file'] : '';
$filetype = isset($_GET['filetype']) ? $_GET['filetype'] : '';

header('Content-Description: File Transfer');
header('Content-Disposition: attachment; filename='.basename($file));
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize($file));
if($filetype == 'csv'){
	header("Content-Type: text/csv");
}
if($filetype == 'zip'){
	header("Content-Type: application/zip");
}

readfile($file);
exit;

?>