<?php session_start(); ?>
<?php include('config.php'); ?>
<?php include('sessionconfig.php'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Security-Policy" content="
 		upgrade-insecure-requests;
    	default-src 'self' ajax.googleapis.com maxcdn.bootstrapcdn.com; 
    	object-src; base-uri 'self'; 
    	form-action 'self'; 
    	img-src 'self'; 
    	script-src 'unsafe-inline' 'self' http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js;
    	font-src 'self' maxcdn.bootstrapcdn.com cdnjs.cloudflare.com;
    	style-src 'unsafe-inline' 'self' maxcdn.bootstrapcdn.com cdnjs.cloudflare.com">
	<title>OneBusiness</title>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel='stylesheet' type='text/css' href='style.php' />
</head>

<body class='body'>
	<?php include_once('headerthree.php');	?>
	<?php
	//get view type
	$viewid = isset($_GET['viewid']) ? $_GET['viewid'] : '';
	$rowid = isset($_GET['rowid']) ? $_GET['rowid'] : '';
	$updated = isset($_GET['updated']) ? $_GET['updated'] : '';
	$mapsubmitsearch = isset($_POST['mapsubmitsearch']) ? $_POST['mapsubmitsearch'] : '';
	$date = date("Y-m-d");

	$mysqli = new mysqli($server, $user_name, $password, $database);
	$languageid = mysqli_real_escape_string($mysqli, $_SESSION['languageid']);
	if ($stmt = $mysqli->prepare("SELECT languagerecordid, languagerecordtextname FROM $masterdatabase.languagerecordtext 
	where languageid = ? and languagerecordid in (5,9,10,19,91)")) {
		$stmt->bind_param('i', $languageid);
		$stmt->execute();
		$result = $stmt->get_result();
		while ($row = $result->fetch_assoc()) {
			$langid = $row['languagerecordid'];
			${"langval$langid"} = $row['languagerecordtextname'];
		}
	}
	$stmt->close();

	//get additional information on the structurefunction based on the type of page
	$mysqli = new mysqli($server, $user_name, $password, $database);
	$viewid = mysqli_real_escape_string($mysqli, $viewid);
	if ($stmt = $mysqli->prepare("select * from view where view.viewid = ?")) {
		$stmt->bind_param('i', $viewid);
		$stmt->execute();
		$result = $stmt->get_result();
		if ($result->num_rows > 0) {
			while ($viewdatarow = $result->fetch_assoc()) {
				$viewname = $viewdatarow['viewname'];
			}
		}
	}
	$mysqli->close();


	//record access of this page in oneappusage
	$date = date("Y-m-d");
	$appmanagerusagename = $userid . " - " . $date;
	$mysqli = new mysqli($server, $user_name, $password, $database);
	$viewid = mysqli_real_escape_string($mysqli, $viewid);
	if ($stmt = $mysqli->prepare("select * from $masterdatabase.appsetupapplication where businessviews like '%$viewid%'")) {
		$stmt->execute();
		$result = $stmt->get_result();
		if ($result->num_rows > 0) {
			while ($getapprow = $result->fetch_assoc()) {
				$appsetupapplicationid = $getapprow['appsetupapplicationid'];

				$mysqli8 = new mysqli($server, $user_name, $password, $database);
				$appmanagerusagename = mysqli_real_escape_string($mysqli8, $appmanagerusagename);
				$appsetupapplicationid = mysqli_real_escape_string($mysqli8, $appsetupapplicationid);
				$userid = mysqli_real_escape_string($mysqli8, $userid);
				$date = mysqli_real_escape_string($mysqli8, $date);
				$viewid = mysqli_real_escape_string($mysqli8, $viewid);
				if ($stmt8 = $mysqli8->prepare("insert into appmanagerusage (appmanagerusagename, appsetupapplicationid, userid, dateused, viewtype, 
				structurefunctionid, viewid, datecreated, masteronly, disabled) values (?, ?, ?, ?, 'Access View', '0', ?, ?, '0', '0')")) {
					$stmt8->bind_param('siisis', $appmanagerusagename, $appsetupapplicationid, $userid, $date, $viewid, $date);
					$stmt8->execute();
				}
				$mysqli8->close();
			}
		}
	}
	$mysqli->close();
	?>

	<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10" style="padding:10px;">
		<!--search results header bar for desktop-->
		<div class='bodyheader'>
			<h3><?php echo $viewname ?></h3>
		</div>

		<div class='bodycontent'>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<?php
				//if user has disabled/enabled a record then show that the status has updated	  		
				if (isset($updated) && $updated == 1) {
					echo "<br/><p class='background-success'>" . $langval91 . "</p>";
				}
				?>
			</div>

			<?php
			if (isset($_POST['submitsearch'])) {
				$viewmapid = isset($_POST['viewmapid']) ? $_POST['viewmapid'] : '';
				$numfields = isset($_POST['numfields']) ? $_POST['numfields'] : '';
				$maptype = isset($_POST['maptype']) ? $_POST['maptype'] : '';
				for ($i = 0; $i <= 100; $i++) {
					${"structurefieldname$i"} = "";
				}
				for ($i = 0; $i <= 100; $i++) {
					${"data$i"} = "";
				}
				echo "<table>";
				$number10 = 1;
				foreach ($_POST as $key => $value) {
					if ($value <> "Submit" && $number10 <= $numfields) {
						${"structurefieldname$number10"} = $key;
						${"data$number10"} = $value;

						$mysqli = new mysqli($server, $user_name, $password, $database);
						$viewmapid = mysqli_real_escape_string($mysqli, $viewmapid);
						${"structurefieldname$number10"} = mysqli_real_escape_string($mysqli, ${"structurefieldname$number10"});
						if ($stmt = $mysqli->prepare("select * from viewmapfilter 
			        inner join datasetcolumn on viewmapfilter.datasetcolumnid = datasetcolumn.datasetcolumnid
			        inner join structurefield on structurefield.structurefieldid = datasetcolumn.structurefieldid
			        where viewmapid = ? and structurefieldname = ?")) {
							$stmt->bind_param('is', $viewmapid, ${"structurefieldname$number10"});
							$stmt->execute();
							$result = $stmt->get_result();
							if ($result->num_rows > 0) {
								while ($getdatarow = $result->fetch_assoc()) {
									${"displayname$number10"} = $getdatarow['displayname'];
									${"requiredfield$number10"} = $getdatarow['requiredfield'];
									${"structurefieldtypeid$number10"} = $getdatarow['structurefieldtypeid'];
									${'tableconnect' . $number10} = $getdatarow['tableconnect'];
									${"tablefieldconnectparent$number10"} = $getdatarow['tablefieldconnectparent'];
									${"tablefieldconnect$number10"} = $getdatarow['tablefieldconnect'];
									${"helptext$number10"} = $getdatarow['helptext'];
								}
							}
						}
						$mysqli->close();

						$number10 = $number10 + 1;
					}
				}
				echo "</table>";
			} else {
				for ($i = 0; $i <= 100; $i++) {
					${"structurefieldname$i"} = "";
				}
				for ($i = 0; $i <= 100; $i++) {
					${"data$i"} = "";
				}
			}

			$mysqli7 = new mysqli($server, $user_name, $password, $database);
			$viewid = mysqli_real_escape_string($mysqli7, $viewid);
			if ($stmt7 = $mysqli7->prepare("select * from viewsection where viewid = ? and disabled = 0 order by position")) {
				$stmt7->bind_param('i', $viewid);
				$stmt7->execute();
				$result7 = $stmt7->get_result();
				if ($result7->num_rows > 0) {
					while ($viewsectionrow = $result7->fetch_assoc()) {				     		//get fields
						$viewtextid = $viewsectionrow['viewtextid'];
						$viewdatasetid = $viewsectionrow['viewdatasetid'];
						$viewsectionid = $viewsectionrow['viewsectionid'];
						$viewmapid = $viewsectionrow['viewmapid'];
						$viewcalendarid = $viewsectionrow['viewcalendarid'];
						$viewspecialcodeurl = $viewsectionrow['viewspecialcodeurl'];
						//echo $viewmapid;

						//if viewspecialcodeurl
						if ($viewspecialcodeurl <> "") {
							if (strpos($viewspecialcodeurl, '../') !== false) {
								$viewspecialcodeurl = $viewspecialcodeurl;
							} else {
								$viewspecialcodeurl = "script/" . $viewspecialcodeurl;
							}
							include_once($viewspecialcodeurl);
						}

						//if viewcalendar
						if ($viewcalendarid <> 0) {
							$mysqli22 = new mysqli($server, $user_name, $password, $database);
							$viewcalendarid = mysqli_real_escape_string($mysqli22, $viewcalendarid);
							if ($stmt22 = $mysqli22->prepare("select * from viewcalendar
								inner join dataset on dataset.datasetid = viewcalendar.datasetid
								where viewcalendar.viewcalendarid = ? and viewcalendar.disabled = 0")) {
								$stmt22->bind_param('i', $viewcalendarid);
								$stmt22->execute();
								$result22 = $stmt22->get_result();
								if ($result22->num_rows > 0) {
									while ($getdatasetrow = $result22->fetch_assoc()) {
										$viewcalendarname = $getdatasetrow['viewcalendarname'];
										$datasetid = $getdatasetrow['datasetid'];
										$invert = $getdatasetrow['invert'];

										//build dataset
										$datasetrecordid = $datasetid;
										$datasetrecordfield = "datasetid";
										$datasetrecordtable = "dataset";
										$startdate = '';
										$enddate = '';
										include('dataset.php');
										//echo "<br/>".$query."<br/>";
										$result5 = mysql_query($query);
									}
								}
							}
							$mysqli22->close();



							//include('fullcalendar-3.2.0/demos/default.html');
			?>
							<script type="text/javascript">
								$(document).ready(function() {

									$('#calendar').fullCalendar({
										defaultDate: '<?php echo $date ?>',
										editable: true,
										eventLimit: true, // allow "more" link when too many events
										events: [
											<?php
											$events = "";
											$title = mysql_field_name($result5, 0);
											$start = mysql_field_name($result5, 1);
											$end = mysql_field_name($result5, 2);
											$url = mysql_field_name($result5, 3);
											$colour = mysql_field_name($result5, 4);
											while ($row5 = mysql_fetch_assoc($result5)) {
												$title1 = $row5[$title];
												$start1 = $row5[$start];
												$end1 = $row5[$end];
												$colour1 = $row5[$colour];
												$url1 = $row5[$url];
												$mysqli = new mysqli($server, $user_name, $password, $database);
												$url = mysqli_real_escape_string($mysqli, $url);
												$datasetid = mysqli_real_escape_string($mysqli, $datasetid);
												if ($stmt = $mysqli->prepare("select datasetcolumnname, format, aggregatecode, 
												structurefieldname, tablename, primarykeyfield, connectedtablename, hidecolumn from dataset
												inner join datasetcolumn on dataset.datasetid = datasetcolumn.datasetid
												inner join structurefield on structurefield.structurefieldid = datasetcolumn.structurefieldid
												inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
												left join $masterdatabase.datasetaggregate on datasetaggregate.datasetaggregateid = datasetcolumn.datasetaggregateid
												where dataset.datasetid = ? and datasetcolumnname = ? order by datasetcolumn.sortorder")) {
													$stmt->bind_param('ii', $datasetid, $url);
													$stmt->execute();
													$result = $stmt->get_result();
													if ($result->num_rows > 0) {
														while ($getdataseturlrow = $result->fetch_assoc()) {
															$urltable = $getdataseturlrow['tablename'];
														}
													}
												}
												$mysqli->close();
												$url1 = "pageedit.php?pagetype=" . $urltable . "&rowid=" . $url1;
												$tip1 = $title1;
												$events = $events . "{";
												$events = $events . "title: '" . $title1 . "',";
												$events = $events . "start: '" . $start1 . "',";
												$events = $events . "end: '" . $end1 . "',";
												$events = $events . "tip: '" . $tip1 . "',";
												$events = $events . "color: '" . $colour1 . "',";
												$events = $events . "url: '" . $url1 . "'";
												$events = $events . "},";
											}
											$events = substr(trim($events), 0, -1);
											echo $events;
											?>

										],
										eventRender: function(event, element) {
											element.attr('title', event.tip);
										},
										eventClick: function(event) {
											if (event.url) {
												window.open(event.url);
												return false;
											}
										}
									});

								});
							</script>
							<div id='calendar' style="width:100%;height:auto;overflow:hidden;z-index:-1"></div>
							<link href='fullcalendar-3.2.0/fullcalendar.min.css' rel='stylesheet' />
							<link href='fullcalendar-3.2.0/fullcalendar.print.min.css' rel='stylesheet' media='print' />
							<script src='fullcalendar-3.2.0/lib/jquery.min.js'></script>
							<script src='fullcalendar-3.2.0/lib/moment.min.js'></script>
							<script src='fullcalendar-3.2.0/fullcalendar.min.js'></script>
							<br /><br />
						<?php
						}

						//if viewmap
						if ($viewmapid <> 0) {
							$mysqli23 = new mysqli($server, $user_name, $password, $database);
							$viewmapid = mysqli_real_escape_string($mysqli23, $viewmapid);
							if ($stmt23 = $mysqli23->prepare("select * from viewmap
								inner join dataset on dataset.datasetid = viewmap.datasetid
								where viewmap.viewmapid = ? and viewmap.disabled = 0")) {
								$stmt23->bind_param('i', $viewmapid);
								$stmt23->execute();
								$result23 = $stmt23->get_result();
								if ($result23->num_rows > 0) {
									while ($getdatasetrow = $result23->fetch_assoc()) {
										$viewdatasetname = $getdatasetrow['datasetname'];
										$datasetid = $getdatasetrow['datasetid'];
										$viewmapname = $getdatasetrow['viewmapname'];
										$allowsearch = $getdatasetrow['allowsearch'];
										$viewlocationpins = $getdatasetrow['viewlocationpins'];
										$viewheat = $getdatasetrow['viewheat'];
										$heatmapvalue = $getdatasetrow['datasetcolumnid'];
										$invert = $getdatasetrow['invert'];
										if (!isset($_POST['submitsearch'])) {
											if ($viewlocationpins == 1 && $viewheat == 1) {
												$maptype = "marker";
											} else {
												if ($viewheat == 1) {
													$maptype = "heat";
												} else {
													$maptype = "marker";
												}
											}
										}

										//build dataset
										$datasetrecordid = $datasetid;
										$datasetrecordfield = "datasetid";
										$datasetrecordtable = "dataset";
										$startdate = '';
										$enddate = '';
										$filtertable = "viewmapfilter";
										$filterfield = "viewmapid";
										$filterfieldid = $viewmapid;
										include('dataset.php');
										$query = $query . " limit 1000";
										$query2 = $query;
									}
								}
							}
							$mysqli23->close();

							$result = mysql_query($query);
							$result2 = mysql_query($query2);
						?>
							<style>
								/* Always set the map height explicitly to define the size of the div
								 * element that contains the map. */
								#map {
									height: 100%;
									width: 100%;
								}

								/* Optional: Makes the sample page fill the window. */
							</style>

							<script type="text/javascript">
								var map, heatmap;

								function initMapheat() {
									map = new google.maps.Map(document.getElementById('map'), {
										zoom: 2,
										center: {
											lat: 0,
											lng: -0
										},
										mapTypeId: 'roadmap'
									});

									heatmap = new google.maps.visualization.HeatmapLayer({
										data: getPoints(),
										opacity: 0.6,
										radius: 20,
										map: map
									});
								}


								// Heatmap data: 500 Points
								function getPoints() {
									return [
										<?php
										$markers = "";
										while ($row = mysql_fetch_array($result)) {
											$lat = $row['Latitude'];
											$lon = $row['Longitude'];
											if ($heatmapvalue <> 0) {
												$mysqli = new mysqli($server, $user_name, $password, $database);
												$heatmapvalue = mysqli_real_escape_string($mysqli, $heatmapvalue);
												if ($stmt = $mysqli->prepare("select datasetcolumnname from datasetcolumn 
									 		where datasetcolumnid = ?")) {
													$stmt->bind_param('i', $heatmapvalue);
													$stmt->execute();
													$result = $stmt->get_result();
													if ($result->num_rows > 0) {
														while ($getweightvalrow = $result->fetch_assoc()) {
															$weightval = $getweightvalrow['datasetcolumnname'];
															$weight = $row[$weightval];
														}
													}
												}
												$mysqli->close();
											} else {
												$weight = 1;
											}

											$markers = $markers . "{location: new google.maps.LatLng(" . $lat . ", " . $lon . "), weight: " . $weight . "},";
										}
										$markers = rtrim($markers, ',');
										echo $markers;
										?>

									];
								}

								function initMapmarker() {

									var map = new google.maps.Map(document.getElementById('map'), {
										zoom: 2,
										center: {
											lat: 0,
											lng: 0
										}
									});

									<?php
									$markers = "";
									$i = 1;

									while ($row2 = mysql_fetch_assoc($result2)) {
										${'lat' . $i} = $row2['Latitude'];
										${'lon' . $i} = $row2['Longitude'];
										$b = 1;
										${'contentstring' . $i} = "'<p>";
										foreach ($row2 as $key => $value) {
											if ($b == 1) {
												${'contentstring' . $i} = ${'contentstring' . $i} . "<b>" . $key . ":</b> " . $value . "<br/>";
												${'title' . $i} = $value;
												$b = $b + 1;
											} else {
												${'contentstring' . $i} = ${'contentstring' . $i} . "<b>" . $key . ":</b> " . $value . "<br/>";
											}
										}
										${'contentstring' . $i} = ${'contentstring' . $i} . "</p>'";
									?>
										var <?php echo 'contentstring' . $i ?> = <?php echo ${'contentstring' . $i} ?>;

										var <?php echo 'infowindow' . $i ?> = new google.maps.InfoWindow({
											content: <?php echo 'contentstring' . $i ?>
										});
										var <?php echo 'marker' . $i ?> = new google.maps.Marker({
											position: {
												lat: <?php echo ${'lat' . $i} ?>,
												lng: <?php echo ${'lon' . $i} ?>
											},
											map: map,
											title: '<?php echo ${'title' . $i} ?>'
										});


										<?php echo 'marker' . $i ?>.addListener('click', function() {
											<?php echo 'infowindow' . $i ?>.open(map, <?php echo 'marker' . $i ?>);
										});
									<?php
										$i = $i + 1;
									}

									?>

								}
							</script>

							<?php
							echo "<h2><b>" . $viewmapname . "</b></h2><br/>";
							?>
							<div>
								<form class="form-horizontal" action='view.php?viewid=<?php echo $viewid ?>' method="post">

									<?php
									//get list of filters
									if ($allowsearch) {
										$getdatasetfilters = "";
										//echo $getdatasetfilters;
										$countfilter = 1;
										$querywhere = "";

										$mysqli = new mysqli($server, $user_name, $password, $database);
										$viewmapid = mysqli_real_escape_string($mysqli, $viewmapid);
										if ($stmt = $mysqli->prepare("select structurefieldname, tablename, tableconnect, logiccode, viewmapfiltername, 
											viewmapid, fieldtypename, tablefieldconnect, tablefieldconnectparent from dataset 
											inner join datasetcolumn on dataset.datasetid = datasetcolumn.datasetid 
											inner join viewmapfilter on viewmapfilter.datasetcolumnid = datasetcolumn.datasetcolumnid	
											inner join $masterdatabase.datasetfilterlogic on viewmapfilter.datasetfilterlogicid = datasetfilterlogic.datasetfilterlogicid 
											inner join structurefield on structurefield.structurefieldid = datasetcolumn.structurefieldid 
											inner join $masterdatabase.structurefieldtype on structurefieldtype.structurefieldtypeid = structurefield.structurefieldtypeid
											inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid 
											where viewmapid = ? 
											order by datasetcolumn.sortorder")) {
											$stmt->bind_param('i', $viewmapid);
											$stmt->execute();
											$result = $stmt->get_result();
											if ($result->num_rows > 0) {
												while ($rowfilter = $result->fetch_assoc()) {
													${'tablename' . $countfilter}	= $rowfilter['tablename'];
													${'logiccode' . $countfilter}	= $rowfilter['logiccode'];
													${'displayname' . $countfilter}	= $rowfilter['viewmapfiltername'];
													${'tableconnect' . $countfilter}	= $rowfilter['tableconnect'];
													${'tablefieldconnect' . $countfilter}	= $rowfilter['tablefieldconnect'];
													${'tablefieldconnectparent' . $countfilter}	= $rowfilter['tablefieldconnectparent'];
													${'fieldtypename' . $countfilter} = $rowfilter['fieldtypename'];
													${'structurefieldname' . $countfilter} = $rowfilter['structurefieldname'];
													//echo ${'displayname'.$countfilter}." - ".${'fieldtypename'.$countfilter}."<br/>";
													$label = ${'displayname' . $countfilter};

													$mysqli89 = new mysqli($server, $user_name, $password, $database);
													${'structurefieldname' . $countfilter} = mysqli_real_escape_string($mysqli89, ${'structurefieldname' . $countfilter});
													$_SESSION['languageid'] = mysqli_real_escape_string($mysqli89, $_SESSION['languageid']);
													if ($stmt89 = $mysqli89->prepare("select * from structurefieldlanguagerecordtext 
														where structurefieldname = ? and languageid = ?")) {
														$stmt89->bind_param('si', ${'structurefieldname' . $countfilter}, $_SESSION['languageid']);
														$stmt89->execute();
														$result89 = $stmt89->get_result();
														if ($result89->num_rows > 0) {
															while ($gettranslationrow = $result->fetch_assoc()) {
																$fieldnametranslated = $gettranslationrow['structurefieldlanguagerecordtextname'];
																$label = $fieldnametranslated;
															}
														}
													}
													$mysqli89->close();

													if (${'fieldtypename' . $countfilter} == 'Text' || ${'fieldtypename' . $countfilter} == 'Value') {
									?>

														<label for="<?php echo $label ?>" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label"><?php echo $label ?></label>
														<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-bottom:10px;">
															<input type="text" class="form-control" name="<?php echo ${'structurefieldname' . $countfilter} ?>" value="<?php echo ${'data' . $countfilter} ?>">
														</div>
													<?php
													}
													if (${'fieldtypename' . $countfilter} == 'Checkbox') {
													?>

														<label for="<?php echo $label ?>" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label"><?php echo $label ?></label>
														<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-bottom:10px;">
															<input type='hidden' value='0' name='<?php echo ${'structurefieldname' . $countfilter} ?>'>
															<input type='checkbox' name='<?php echo ${'structurefieldname' . $countfilter} ?>' value='1' <?php if (${'data' . $countfilter} == '1') { ?> checked='checked' <?php;
																																																					} ?> />
														</div>

													<?php
													}
													if (${'fieldtypename' . $countfilter} == 'Date Select') {
													?>

														<label for="<?php echo $label ?>" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label"><?php echo $label ?></label>
														<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-bottom:10px;">
															<input type="date" class="form-control" name="<?php echo ${'structurefieldname' . $countfilter} ?>" value="<?php echo ${'data' . $countfilter} ?>">
														</div>

													<?php
													}
													if (${'fieldtypename' . $countfilter} == 'Table Connect') {
													?>
														<label for="<?php echo $label ?>" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label"><?php echo $label ?></label>
														<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-bottom:10px;">
															<?php
															$tableconnect2 = strstr(${'tableconnect' . $countfilter}, '.');
															$tableconnect2 = str_replace(".", "", $tableconnect2);
															${'tableconnect' . $countfilter} = $tableconnect2;
															$idfield = $tableconnect2 . "id";
															$namefield = ${'tablefieldconnect' . $countfilter};

															if (${'tablefieldconnectparent' . $countfilter} == "") {
																$querytype = "select * from " . ${'tableconnect' . $countfilter} . " where masteronly = 0 ";
																if ($idfield == 'businessunitid' && $_SESSION["superadmin"] == 0) {
																	$querytype = $querytype . "and businessunitid in (" . $_SESSION["userbusinessunit"] . ") ";
																}
																$querytype = $querytype . "order by " . ${'tablefieldconnect' . $countfilter} . " asc";
															} else {
																$querytype = "select * from " . ${'tableconnect' . $countfilter} . " inner join " . ${'tablefieldconnectparent' . $countfilter} . " where masteronly = 0 ";
																if ($idfield == 'businessunitid' && $_SESSION["superadmin"] == 0) {
																	$querytype = $querytype . "and businessunitid in (" . $_SESSION["userbusinessunit"] . ") ";
																}
																$querytype = $querytype . "order by " . ${'tablefieldconnect' . $countfilter} . " asc";
															}

															$resulttype = mysql_query($querytype);
															echo "<select class='form-control' name='" . ${'structurefieldname' . $countfilter} . "'>";
															echo "<option value=''>" . $langval19 . "</option>";
															while ($nttype = mysql_fetch_array($resulttype)) {
																$parenttablefieldname = explode(' ', trim(${'tablefieldconnectparent' . $countfilter}))[0];
																if ($parenttablefieldname <> "") {
																	$parenttablefieldname = $parenttablefieldname . "name";
																	$parenttablefieldname = $nttype[$parenttablefieldname] . ": ";
																}
																if (${"data$countfilter"} == $nttype[$idfield]) {
																	echo "<option value=" . $nttype[$idfield] . " selected='true'>" . $parenttablefieldname . $nttype[$namefield] . "</option>";
																} else
																	echo "<option value=" . $nttype[$idfield] . " >" . $parenttablefieldname . $nttype[$namefield] . "</option>";
															}
															echo "</select>";
															?>
														</div>
										<?php
													}

													$countfilter = $countfilter + 1;
												}
											}
										}
										$mysqli->close();
									}

									if ($viewlocationpins == 1 || $viewheat == 1) {
										?>
										<label for="Map Type" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label">Map Type</label>
										<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
											<select class='form-control' name='maptype'>";
												<?php
												if ($viewheat == 1) {
													if ($maptype == 'heat') {
												?>
														<option value='heat' selected='true'>Heat Map</option>
													<?php
													} else {
													?>
														<option value='heat'>Heat Map</option>
												<?php
													}
												}
												?>

												<?php
												if ($viewlocationpins) {
													if ($maptype == 'marker') {
												?>
														<option value='marker' selected='true'>Marker Location Map</option>
													<?php
													} else {
													?>
														<option value='marker'>Marker Location Map</option>
												<?php
													}
												}
												?>

											</select>
										</div>
									<?php
									}
									?>
									<div class="col-md-1 col-lg-1 col-sm-1 col-xs-1" style="padding-bottom:10px;">
										<div style="text-align:center;">
											<input type="hidden" name="viewmapid" value="<?php echo $viewmapid; ?>">
											<input type="hidden" name="numfields" value="<?php echo $countfilter; ?>">
											<button type='submit' name='submitsearch' value='Submit' class="button-primary"><?php echo $langval5 ?></button>
										</div>
									</div>
							</div>

							</form>
							<div id="map" style="width:100%;height:500px;"></div>
							<!-- Filter Sorts. -->

							<!-- Replace the value of the key parameter with your own API key. -->
							<?php
							if ($maptype == 'heat') {
							?>
								<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzttYpH8HcN020YLeWwqk-AJu6P6H1jyk&libraries=visualization&callback=initMapheat">
								</script>
							<?php
							}
							if ($maptype == 'marker') {
							?>
								<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzttYpH8HcN020YLeWwqk-AJu6P6H1jyk&callback=initMapmarker">
								</script>
							<?php
							}
							if ($maptype == '') {
							?>
								<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzttYpH8HcN020YLeWwqk-AJu6P6H1jyk&libraries=visualization&callback=initMapmarker">
								</script>
							<?php
							}
							?>
							<br /><br />
			<?php

						}

						//if viewtext
						if ($viewtextid <> 0) {
							$mysqli = new mysqli($server, $user_name, $password, $database);
							$viewtextid = mysqli_real_escape_string($mysqli, $viewtextid);
							if ($stmt = $mysqli->prepare("select * from viewtext where viewtextid = ? and disabled = 0")) {
								$stmt->bind_param('i', $viewtextid);
								$stmt->execute();
								$result = $stmt->get_result();
								if ($result->num_rows > 0) {
									while ($getviewtextrow = $result->fetch_assoc()) {
										$text = $getviewtextrow['text'];
										$styleheadertag = $getviewtextrow['styleheadertag'];
										echo "<" . $styleheadertag . ">" . $text . "</" . $styleheadertag . "><br/>";
									}
								}
							}
							$mysqli->close();
						}

						//if viewdataset
						if ($viewdatasetid <> 0) {
							$alloweditdatasetfiltername = "";
							$allowdisabledatasetfiltername = "";
							$allowenabledatasetfiltername = "";
							$alloweditclear = 0;

							$mysqli90 = new mysqli($server, $user_name, $password, $database);
							$viewdatasetid = mysqli_real_escape_string($mysqli90, $viewdatasetid);
							if ($stmt90 = $mysqli90->prepare("select * from viewdataset 
								inner join dataset on dataset.datasetid = viewdataset.datasetid
								where viewdataset.viewdatasetid = ? and viewdataset.disabled = 0")) {
								$stmt90->bind_param('i', $viewdatasetid);
								$stmt90->execute();
								$result90 = $stmt90->get_result();
								if ($result90->num_rows > 0) {
									while ($getdatasetrow = $result90->fetch_assoc()) {
										$viewdatasetname = $getdatasetrow['viewdatasetname'];
										$showheadings = $getdatasetrow['showheadings'];
										$gridtextdescription = $getdatasetrow['gridtextdescription'];
										$datasetid = $getdatasetrow['datasetid'];
										$tablewidth = $getdatasetrow['tablewidth'];
										$invert = $getdatasetrow['invert'];
										$allowedituniquefieldid = $getdatasetrow['allowedituniquefieldid'];
										$structurefieldiddisable = $getdatasetrow['structurefieldiddisable'];
										$structurefieldidenable = $getdatasetrow['structurefieldidenable'];
										$pagelink = $getdatasetrow['pagelink'];
										$pagelinkname = $getdatasetrow['pagelinkname'];
										$requestconfigurationid = $getdatasetrow['requestconfigurationid'];
										$addnew = $getdatasetrow['addnew'];
										if ($allowedituniquefieldid <> 0) {
											$mysqli = new mysqli($server, $user_name, $password, $database);
											$viewdatasetid = mysqli_real_escape_string($mysqli, $viewdatasetid);
											if ($stmt = $mysqli->prepare("select * from viewdataset 
												inner join dataset on dataset.datasetid = viewdataset.datasetid
												left join datasetcolumn on datasetcolumn.datasetcolumnid = viewdataset.allowedituniquefieldid
												left join datasetfilter on datasetfilter.datasetcolumnid = datasetcolumn.datasetcolumnid
												left join structurefield on structurefield.structurefieldid = datasetcolumn.structurefieldid
												left join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
												where viewdataset.viewdatasetid = ? and viewdataset.disabled = 0")) {
												$stmt->bind_param('i', $viewdatasetid);
												$stmt->execute();
												$result = $stmt->get_result();
												if ($result->num_rows > 0) {
													while ($geteditfieldrow = $result->fetch_assoc()) {
														$allowedittablename = $geteditfieldrow['tablename'];
														$alloweditfieldname = $geteditfieldrow['structurefieldname'];
														$alloweditdatasetfiltername = $geteditfieldrow['datasetfiltername'];
													}
												}
											}
											$mysqli->close();
											if (strpos($alloweditdatasetfiltername, '$_') !== false) {
												$alloweditdatasetfiltername = substr($alloweditdatasetfiltername, 11);
												$alloweditdatasetfiltername = substr($alloweditdatasetfiltername, 0, -2);
												$alloweditdatasetfiltername = $_SESSION[$alloweditdatasetfiltername];
											} else {
												if (strpos($alloweditdatasetfiltername, '$') !== false) {
													$alloweditdatasetfiltername = substr($alloweditdatasetfiltername, 1);
													$alloweditdatasetfiltername = ${$alloweditdatasetfiltername};
												} else {
													$alloweditclear = 0;
												}
											}
										}
										if ($structurefieldiddisable <> 0) {
											$mysqli78 = new mysqli($server, $user_name, $password, $database);
											$viewdatasetid = mysqli_real_escape_string($mysqli78, $viewdatasetid);
											if ($stmt78 = $mysqli78->prepare("select * from viewdataset 
												inner join dataset on dataset.datasetid = viewdataset.datasetid
												left join datasetcolumn on datasetcolumn.datasetcolumnid = viewdataset.structurefieldiddisable
												left join datasetfilter on datasetfilter.datasetcolumnid = datasetcolumn.datasetcolumnid
												left join structurefield on structurefield.structurefieldid = datasetcolumn.structurefieldid
												left join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
												where viewdataset.viewdatasetid = ? and viewdataset.disabled = 0")) {
												$stmt78->bind_param('i', $viewdatasetid);
												$stmt78->execute();
												$result78 = $stmt78->get_result();
												if ($result78->num_rows > 0) {
													while ($getdisablefieldrow = $result78->fetch_assoc()) {
														$allowdisabletablename = $getdisablefieldrow['tablename'];
														$allowdisablefieldname = $getdisablefieldrow['structurefieldname'];
														$allowdisabledatasetfiltername = $getdisablefieldrow['datasetfiltername'];
													}
												}
											}
											$mysqli78->close();
											if (strpos($allowdisabledatasetfiltername, '$_') !== false) {
												$allowdisabledatasetfiltername = substr($allowdisabledatasetfiltername, 11);
												$allowdisabledatasetfiltername = substr($allowdisabledatasetfiltername, 0, -2);
												$allowdisabledatasetfiltername = $_SESSION[$allowdisabledatasetfiltername];
											} else {
												if (strpos($allowdisabledatasetfiltername, '$') !== false) {
													$allowdisabledatasetfiltername = substr($allowdisabledatasetfiltername, 1);
													$allowdisabledatasetfiltername = ${$allowdisabledatasetfiltername};
												} else {
													$allowdisableclear = 0;
												}
											}
										}
										if ($structurefieldidenable <> 0) {
											$mysqli78 = new mysqli($server, $user_name, $password, $database);
											$viewdatasetid = mysqli_real_escape_string($mysqli78, $viewdatasetid);
											if ($stmt78 = $mysqli78->prepare("select * from viewdataset 
												inner join dataset on dataset.datasetid = viewdataset.datasetid
												left join datasetcolumn on datasetcolumn.datasetcolumnid = viewdataset.structurefieldidenable
												left join datasetfilter on datasetfilter.datasetcolumnid = datasetcolumn.datasetcolumnid
												left join structurefield on structurefield.structurefieldid = datasetcolumn.structurefieldid
												left join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
												where viewdataset.viewdatasetid = ? and viewdataset.disabled = 0")) {
												$stmt78->bind_param('i', $viewdatasetid);
												$stmt78->execute();
												$result78 = $stmt78->get_result();
												if ($result78->num_rows > 0) {
													while ($getdisablefieldrow = $result78->fetch_assoc()) {
														$allowenabletablename = $getenablefieldrow['tablename'];
														$allowenablefieldname = $getenablefieldrow['structurefieldname'];
														$allowenabledatasetfiltername = $getenablefieldrow['datasetfiltername'];
													}
												}
											}
											$mysqli78->close();
											if (strpos($allowenabledatasetfiltername, '$_') !== false) {
												$allowenabledatasetfiltername = substr($allowenabledatasetfiltername, 11);
												$allowenabledatasetfiltername = substr($allowenabledatasetfiltername, 0, -2);
												$allowenabledatasetfiltername = $_SESSION[$allowenabledatasetfiltername];
											} else {
												if (strpos($allowenabledatasetfiltername, '$') !== false) {
													$allowenabledatasetfiltername = substr($allowenabledatasetfiltername, 1);
													$allowenabledatasetfiltername = ${$allowenabledatasetfiltername};
												} else {
													$allowenableclear = 0;
												}
											}
										}

										//build dataset
										$datasetrecordid = $datasetid;
										$datasetrecordfield = "datasetid";
										$datasetrecordtable = "dataset";
										$startdate = '';
										$enddate = '';
										$viewextra = 1;
										$filtertable = 'dataset';
										$filterfield = "datasetid";
										$filterfieldid = $datasetid;
										include('dataset.php');
										$query = $query . " limit 500";
										//echo $query;

										//build spreadsheet
										$rowquery = mysql_query($query);
										if ($addnew == 1 || mysql_num_rows($rowquery) >= 1) {
											echo "<h2><b>" . $viewdatasetname . "</b></h2>";
											if ($gridtextdescription <> "") {
												echo $gridtextdescription . "<br/><br/>";
											}
										}
										if ($addnew == 1) {
											$mysqli78 = new mysqli($server, $user_name, $password, $database);
											$datasetid = mysqli_real_escape_string($mysqli78, $datasetid);
											if ($stmt78 = $mysqli78->prepare("select * from datasetcolumn 
												inner join structurefield on structurefield.structurefieldid = datasetcolumn.structurefieldid
												inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
												where datasetid = ? order by sortorder asc")) {
												$stmt78->bind_param('i', $datasetid);
												$stmt78->execute();
												$result78 = $stmt78->get_result();
												if ($result78->num_rows > 0) {
													$primarypage = $result78->fetch_assoc();
													$primarypage = $primarypage['tablename'];
												}
											}
											$mysqli78->close();
											$pagename = $_SERVER['REQUEST_URI'];
											$pagename = str_replace("&", "xxxxxxxxxx", $pagename);
											echo "<a class='button-primary' href='pageadd.php?pagetype=" . $primarypage . "&pagename=" . $pagename . "'>Add New</a>";
											echo "<br/><br/>";
										}

										if (mysql_num_rows($rowquery) >= 1) {
											echo "<table class='table table-bordered' style='width:" . $tablewidth . "%;'>";
											$pagename = $_SERVER['REQUEST_URI'];
											$pagename = str_replace("&", "xxxxxxxxxx", $pagename);

											if ($invert == 0) {
												//create headings
												if ($showheadings == '1') {
													$field = mysql_num_fields($rowquery);
													echo "<thead><tr>";
													$counter1 = 1;
													for ($i = 0; $i < $field; $i++) {
														$names[] = mysql_field_name($rowquery, $i);
														echo "<th>" . $names[$i] . "</th>";
													}
													if ($allowedituniquefieldid <> 0) {
														echo "<th>Edit</th>";
													}
													if ($structurefieldidenable <> 0) {
														echo "<th>Activate</th>";
													}
													if ($structurefieldiddisable <> 0) {
														echo "<th>Disable</th>";
													}
													if ($pagelink <> "") {
														echo "<th>" . $pagelinkname . "</th>";
													}
													$names = array();
												}
												echo "</tr></thead>";
												while ($getdatasectionrow = mysql_fetch_array($rowquery)) {
													$numcols = mysql_num_fields($rowquery) - 1;
													//add data
													echo "<tr><td>";
													$counter5 = 0;
													while ($counter5 <= $numcols) {
														echo $getdatasectionrow[$counter5];
														if ($counter5 == 0) {
															$rowid = $getdatasectionrow[$counter5];
															if ($alloweditdatasetfiltername == "" || $alloweditclear == 1) {
																$alloweditclear = 1;
																$alloweditdatasetfiltername = $getdatasectionrow[$counter5];
															}
														}
														if ($counter5 == 0) {
															$rowid = $getdatasectionrow[$counter5];
															if ($allowdisabledatasetfiltername == "" || $allowdisableclear == 1) {
																$allowdisableclear = 1;
																$allowdisabledatasetfiltername = $getdatasectionrow[$counter5];
															}
														}
														if ($counter5 == 0) {
															$rowid = $getdatasectionrow[$counter5];
															if ($allowenabledatasetfiltername == "" || $allowenableclear == 1) {
																$allowenableclear = 1;
																$allowenabledatasetfiltername = $getdatasectionrow[$counter5];
															}
														}
														if ($counter5 <> $numcols) {
															echo "</td><td>";
														}
														$counter5 = $counter5 + 1;
													}
													echo "</td>";
													if ($allowedituniquefieldid <> 0) {
														echo "<td><a href='pageedit.php?pagetype=" . $allowedittablename . "&rowid=" . $alloweditdatasetfiltername . "&pagename=" . $pagename . "'>Edit</a></td>";
													}
													if ($structurefieldiddisable <> 0) {
														echo "<td><a href='pagechangestatus.php?pagetype=" . $allowdisabletablename . '&rowid=' . $allowdisabledatasetfiltername . "&action=deactivate&pagename=" . $pagename . "'>Disable</a></td>";
													}
													if ($structurefieldidenable <> 0) {
														echo "<td><a href='pagechangestatus.php?pagetype=" . $allowenabletablename . '&rowid=' . $allowenabledatasetfiltername . "&action=activate&pagename=" . $pagename . "'>Activate</a></td>";
													}
													if ($pagelink <> "") {
														echo "<td><a href='" . $pagelink . $rowid . "'>" . $pagelinkname . "</a></td>";
													}
													echo "</tr>";
												}
											} else {
												while ($getdatasectionrow = mysql_fetch_array($rowquery)) {
													$numcols = mysql_num_fields($rowquery) - 1;
													$counter5 = 0;
													while ($counter5 <= $numcols) {
														$heading[$counter5] = $getdatasectionrow[$counter5];
														if ($counter5 == 0) {
															$rowid = $getdatasectionrow[$counter5];
															if ($alloweditdatasetfiltername == "" || $alloweditclear == 1) {
																$alloweditclear = 1;
																$alloweditdatasetfiltername = $getdatasectionrow[$counter5];
															}
														}
														if ($counter5 == 0) {
															$rowid = $getdatasectionrow[$counter5];
															if ($allowdisabledatasetfiltername == "" || $allowdisableclear == 1) {
																$allowdisableclear = 1;
																$allowdisabledatasetfiltername = $getdatasectionrow[$counter5];
															}
														}
														if ($counter5 == 0) {
															$rowid = $getdatasectionrow[$counter5];
															if ($allowenabledatasetfiltername == "" || $allowenableclear == 1) {
																$allowenableclear = 1;
																$allowenabledatasetfiltername = $getdatasectionrow[$counter5];
															}
														}
														$counter5 = $counter5 + 1;
													}
												}
												if ($showheadings == '1') {
													$field = mysql_num_fields($rowquery);
													echo "<tr>";
													$counter1 = 1;
													$counter5 = 0;
													for ($i = 0; $i < $field; $i++) {
														$names[] = mysql_field_name($rowquery, $i);

														//manages decryption of the customer data table on the specific view
														$salt = "h3f8s9en20vj3";
														if ($viewname == "OneCustomer Customer Details") {
															echo $names[$i];
															if (
																$names[$i] == "Customer Name:" || $names[$i] == "First Name:"
																|| $names[$i] == "Last Name:" || $names[$i] == "Address 1:"
																|| $names[$i] == "Address 2: " || $names[$i] == "Address 3:"
																|| $names[$i] == "Town/City:" || $names[$i] == "State/Region:"
																|| $names[$i] == "Post/ZIP Code:" || $names[$i] == "Phone Number:"
																|| $names[$i] == "Mobile Number:" || $names[$i] == "Fax Number: "
																|| $names[$i] == "Email Address: "
															) {
																$heading[$counter5] = openssl_decrypt($heading[$counter5], "AES-128-ECB", $salt);
															}
														}

														echo "<tr><td><b>" . $names[$i] . "</b></td><td>" . $heading[$counter5] . "</td></tr>";
														$counter5 = $counter5 + 1;
													}
													if ($allowedituniquefieldid <> 0) {
														echo "<tr><td><a class='button-primary' href='pageedit.php?pagetype=" . $allowedittablename . "&rowid=" . $alloweditdatasetfiltername . "&pagename=" . $pagename . "'>Edit</a></td></tr>";
													}
													if ($structurefieldiddisable <> 0) {
														echo "<tr><td><a class='button-primary' href='pagechangestatus.php?pagetype=" . $allowdisabletablename . '&rowid=' . $allowdisabledatasetfiltername . "&action=deactivate&pagename=" . $pagename . "'>Disable</a></td></tr>";
													}
													if ($structurefieldidenable <> 0) {
														echo "<tr><td><a class='button-primary' href='pagechangestatus.php?pagetype=" . $allowenabletablename . '&rowid=' . $allowenabledatasetfiltername . "&action=activate&pagename=" . $pagename . "'>Activate</a></td></tr>";
													}
													$names = array();
												}
											}
											echo "</table>";
											if ($requestconfigurationid <> 0) {
												echo "<a class='button-primary' href='accessrequest.php?requestconfigurationid=" . $requestconfigurationid . "&pagename=" . $pagename . "'>Request Additional Access</a><br/><br/>";
											}
										}
										$rowid = isset($_GET['rowid']) ? $_GET['rowid'] : '';
									}
								}
							}
							$mysqli90->close();
						}
					}
				}
			}
			$mysqli7->close();


			?>

			<br /><br /><br />

		</div>
		<br /><br /><br /><br /><br /><br />
	</div>

	</div>
	<?php include_once('footer.php'); ?>
</body>