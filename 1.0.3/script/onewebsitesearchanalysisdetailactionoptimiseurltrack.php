<?php

//LANGUAGE COLLECTION SECTION
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (372,373,374,375,376)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}


//create websitesearchsitemap record
$check3 = mysql_query("select * from $database.websitesearchsitemap where websitesearchanalysisid = '$rowid'");
if(mysql_num_rows($check3) == 0){
	$createkeywordrecord = mysql_query("insert into $database.websitesearchsitemap (websitesearchsitemapname, websitesearchanalysisid,
	datecreated) values ('Sitemap', '$rowid', '$date')");	
	$websitesearchsitemapid = mysql_insert_id();
}
else {
	$check3row = mysql_fetch_array($check3);
	$websitesearchsitemapid = $check3row['websitesearchsitemapid'];
}
		
//scan website
$getwebsite = mysql_query("select * from $database.websitesearchanalysis 
inner join $database.website on website.websiteid = websitesearchanalysis.onewebsiteid
where websitesearchanalysisid = '$rowid'");
$getwebsiterow = mysql_fetch_array($getwebsite);
$topurl = $getwebsiterow['websiteurl'];
$excludedurls = $getwebsiterow['excludedwebaddresses'];
$slash = substr($topurl, -1);
if($slash <> "/"){
	$topurl = $getwebsiterow['websiteurl']."/";
}
if (!preg_match("~^(?:f|ht)tps?://~i", $topurl)) {
  $topurl = "http://" . $topurl;
}
 
 //create websitesearchsitemap record
	$check3 = mysql_query("select * from $database.websitesearchsitemap where websitesearchanalysisid = '$rowid'");
	if(mysql_num_rows($check3) == 0){
		$createkeywordrecord = mysql_query("insert into $database.websitesearchsitemap (websitesearchsitemapname, websitesearchanalysisid,
		datecreated) values ('Sitemap', '$rowid', '$date')");	
		$websitesearchsitemapid = mysql_insert_id();
	}
	else {
		$check3row = mysql_fetch_array($check3);
		$websitesearchsitemapid = $check3row['websitesearchsitemapid'];
	}
	
	   	
//check if onewebsite storage folder exists, if not, then create
$filedir = '../documents/'. $database."/sf/websitesearchsitemap/";
if(file_exists($filedir)){
}
else {
	$file_directory = "../documents/".$database."/sf/websitesearchsitemap/";
	mkdir($file_directory);
}

$urlarray = array();
$file="../documents/".$database."/sf/websitesearchsitemap/".$websitesearchsitemapid." - Sitemap.xml";

//create sitemap file
if (!file_exists($file)) {
	//create sitemap file
	include ('onewebsitesearch/xmlsitemapgenerator/sitemap-generator.php');    
} 
else {
	$date = date("y-m-d");
	$datetwo = filemtime($file);
	$datediff = ($date - $datetwo) / 60 / 60;
	if($datediff <= 24){
	   	//read sitemap
	   	$scanned = array();  
		$DomDocument = new DOMDocument();
		$DomDocument->preserveWhiteSpace = false;
		$DomDocument->load($file);
		$DomNodeList = $DomDocument->getElementsByTagName('loc');
		foreach($DomNodeList as $url) {
	    	$scanned[] = $url->nodeValue;
		}
		//echo json_encode($scanned);
	}
	else {
		//create sitemap file
		include ('onewebsitesearch/xmlsitemapgenerator/sitemap-generator.php'); 
	}
}

$urlarray = $scanned;
//echo "<br/>".json_encode($urlarray)."<br/>";

//get list of pages
include('onewebsitesearch/xmlsitemapgenerator/settings.php');
include('onewebsitesearch/htmlscraper/simple_html_dom.php');
//$html = file_get_html2($topurl);
//echo "<table class='table table-bordered'>";
//echo "<thead><td>masterurl</td><td>title</td><td>url</td><td>internalurl</td></thead>";
foreach ($urlarray as $url2) {
	$scan = 1;
	if($topurl == $url2){
		$scan = 1;
	}
	else {
		if(strpos($excludedurls,$url2) !== false) {
  			//do not scan this url as it is excluded from scan    
  			$scan = 0;
  			//echo "<br/>not scanned: ".$url2;
		} 			
	}
	if($scan == 1) {
		//get page data
		$title = '';
		$url = '';
		//check page attributes
		$html = file_get_html2($url2);
		$pageurl = $url2;
		
	  	//get specific meta tags for pages
	  	$deleteexisting = mysql_query("delete from $database.websitesearchpagelinkcheckpage 
	  	where websitesearchanalysisid = '$rowid' and masterpageurl = '$pageurl'");	  	
		foreach($html->find('a') as $element) {
			$url = $element->href;
			$title = $element->title;
			$urlbegin = substr($url, 0, 1);
			$urlbegin2 = substr($url, 0, 6);
			$urlbegin3 = str_replace("https://", "", $url);
			$urlbegin3 = str_replace("http://", "", $urlbegin3);
			$urlbegin3 = substr($urlbegin3, 0, 8);
			$internalurl = 0;
			if(strpos($url, 'http://www') !== false || strpos($url, 'https://www') !== false) {
	    		$pageurl2 = str_replace("https://www.", "", $pageurl);
	    		$pageurl2 = str_replace("http://www.", "", $pageurl2);
	    		$pageurl2 = str_replace("http://", "", $pageurl2);
	    		$pageurl2 = str_replace("http://", "", $pageurl2);
	    		$pageurl2 = strtok($pageurl2, '.');
	    		if(strpos($url, $pageurl2) !== false){
					$internalurl = 1;    		
	    		}
			}
			else {
				$internalurl = 1;		
			}
			
			if(filter_var($url, FILTER_VALIDATE_URL)){
				$surl = str_replace("https://", "", $topurl);
				$surl = str_replace("http://", "", $surl);
				$surl = str_replace("www.", "", $surl);
				//echo "<br/>topurl: ".$topurl." url: ".$surl;
					if($urlbegin <> '#' && $urlbegin2 <> 'mailto' && $urlbegin <> '/' && $url <> "" 
		  			&& $urlbegin3 <> "scontent" && strpos($url, $surl) == true){
						//echo "<tr><td>".$pageurl."</td><td>".$title."</td><td>".$url."</td><td>".$internalurl."</td></tr>";
				  	
					  	//validate title
					  	$validationerrortitle = '';
					  	if(strlen($title) >= 100){
							$validationerrortitle = $validationerrortitle."{####} ".$langval372;  	
					  	}
					  	if(strlen($title) <= 30 && $title <> ""){
							$validationerrortitle = $validationerrortitle."{####} ".$langval373;  	
					  	}
					  	if($title == ""){
							$validationerrortitle = $validationerrortitle."{####} ".$langval374;  	
					  	}
					  	
					 	//validate url
						$validationerrorurl = '';
						if(strlen($url) >= 70 && $internalurl == 1){
							$validationerrorurl = $validationerrorurl."{####} ".$langval375;  	
					  	}
					  	if(strlen($url) <= 15 && $internalurl == 1){
							$validationerrorurl = $validationerrorurl."{####} ".$langval376;  	
					  	}
					  	
					   //save the record
					  	$datecreated = date('Y-m-d');
					  	if($title == ""){
					  		$websitesearchpagelinkcheckpagename = $url;
					  	}
					  	else {
					  		$websitesearchpagelinkcheckpagename = $title;
					  	}
					  	$createnew = mysql_query("insert into $database.websitesearchpagelinkcheckpage (websitesearchpagelinkcheckpagename,			websitesearchanalysisid, masterpageurl, url, title, validationerrorurl, validationerrortitle, datecreated) 
					  	values ('$websitesearchpagelinkcheckpagename', '$rowid', '$pageurl', '$url', '$title', '$validationerrorurl', '$validationerrortitle', 
						'$datecreated')");
					}
				
			}
	  	}
  	}
}
//echo "</table>";

?>