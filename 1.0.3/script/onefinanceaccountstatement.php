<?php
$financialaccountid = isset($_GET['financialaccountid']) ? $_GET['financialaccountid'] : '';
$totalaccountbalance = 0;
$totalhqaccountbalance = 0;
//show overview grid of accounts
$currencycode = $_SESSION['currencycode'];

$pagename = $_SERVER['REQUEST_URI'];
$pagename = str_replace("&", "xxxxxxxxxx", $pagename);
	
//get dates for financial account history
$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
if(!isset($_POST['submit'])){
	$date = date("Y-m-d");
	$startdate = date('Y-m-d', strtotime("-12 months", strtotime($date)));
	$enddate = date('Y-m-d', strtotime("+12 months", strtotime($date)));
}
else {
	$startdate = isset($_POST['startdate']) ? $_POST['startdate'] : '';
	$enddate = isset($_POST['enddate']) ? $_POST['enddate'] : '';		
}
	
//get language details
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (5,8,9,10,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150, 
151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}							
 
echo "<h2>".$langval132."</h2>";
echo "<table class='table table-bordered' style='width:100%;'>";
echo "<thead>";
echo "<td><b>".$langval133."</b></td>";
echo "<td><b>".$langval134."</b></td>";
echo "<td><b>".$langval135."</b></td>";
echo "<td><b>".$langval136."</b></td>";
echo "<td><b>".$langval137."</b></td>";
echo "<td><b>".$langval138."</b></td>";
echo "<td><b>".$langval139."</b></td>";
echo "<td><b>".$langval140." (".$currencycode.")</b></td>";
if($financialaccountid == 0){
	echo "<td><b>".$langval141."</b></td>";
}
echo "</thead>";

$getaccount = "select * from financialaccount 
inner join currency on currency.currencyid = financialaccount.currencyid
inner join country on country.countryid = financialaccount.countryid
where financialaccount.disabled = 0";
if($financialaccountid >= 1){
	$getaccount = $getaccount." and financialaccountid = '$financialaccountid'";
}
$getaccount = $getaccount." order by financialaccountname";
$getaccount = mysql_query($getaccount);
while($row89=mysql_fetch_array($getaccount)){
	$finaccountid = $row89['financialaccountid'];
	$financialaccountname = $row89['financialaccountname'];
	$financialaccountcurrencyid = $row89['currencyid'];
	$financialaccountcurrencycode = $row89['currencycode'];
	$financialaccountcountryname = $row89['countryname'];
	$sortcode = $row89['sortcode'];
	$accountnumber = $row89['accountnumber'];
	echo "<tr>";
	echo "<td>".$finaccountid."</td>";
	echo "<td>".$financialaccountcountryname."</td>";
	echo "<td>".$financialaccountname."</td>";
	echo "<td>".$sortcode."</td>";
	echo "<td>".$accountnumber."</td>";
	echo "<td>".$financialaccountcurrencycode."</td>";
	
	//get account balance
	$accountbalance = 0;	
	$hqaccountbalance = 0;
	//get income
	$getfinancialtransaction = mysql_query("select sum(amountincludingtax) as 'Amount' from financialtransaction
	inner join financesubcategory on financesubcategory.financesubcategoryid = financialtransaction.financesubcategoryid
	where financialaccountid = '$finaccountid' and financecategoryid = '1' and financialtransaction.disabled = 0
	and financialtransactionstatusid in (1,3,4,5)");	
	$getfinancialtransactionrow = mysql_fetch_array($getfinancialtransaction);
	$income = $getfinancialtransactionrow['Amount'];
	//get expenditure
	$getfinancialtransaction = mysql_query("select sum(amountincludingtax) as 'Amount' from financialtransaction
	inner join financesubcategory on financesubcategory.financesubcategoryid = financialtransaction.financesubcategoryid
	where financialaccountid = '$finaccountid' and financecategoryid = '2' and financialtransaction.disabled = 0
	and financialtransactionstatusid in (1,3,4,5)");	
	$getfinancialtransactionrow = mysql_fetch_array($getfinancialtransaction);
	$expenditure = $getfinancialtransactionrow['Amount'];
	//get taxpayment
	$gettaxpayment = mysql_query("select sum(paymentamount) as 'Amount' from taxpayment 
	where financialaccountid = '$finaccountid' and taxpaymentstatusid = 2");
	$gettaxpaymentrow = mysql_fetch_array($gettaxpayment);
	$taxexpenditure = $gettaxpaymentrow['Amount'];
	//get transfersin
	$getaccounttransfersin = mysql_query("select sum(toamount) as 'Amount' from financialaccounttransfer 
	where tofinancialaccountid = '$finaccountid'");
	$getaccounttransfersinrow = mysql_fetch_array($getaccounttransfersin);
	$transfersin = $getaccounttransfersinrow['Amount'];
	//get transfersout
	$getaccounttransfersout = mysql_query("select sum(fromamount) as 'Amount' from financialaccounttransfer 
	where fromfinancialaccountid = '$finaccountid'");
	$getaccounttransfersoutrow = mysql_fetch_array($getaccounttransfersout);
	$transfersout = $getaccounttransfersoutrow['Amount'];
	//calculatetotal
	$accountbalance = $income - $expenditure - $taxexpenditure + $transfersin - $transfersout;
	
	//calculate hq currency value
	$getcurrencyfxrate = mysql_query("select * from currencyfxrate 
	where currencyid = '$financialaccountcurrencyid' and currencyfxrate.disabled = '0'");
	$getcurrencyfxraterow = mysql_fetch_array($getcurrencyfxrate);
	$currencyfxrate = $getcurrencyfxraterow['currencytohqcurrencycurrentfxrate'];
	$hqaccountbalance = $accountbalance / $currencyfxrate;
	
	echo "<td>".number_format($accountbalance, 2)."</td>";
	echo "<td>".number_format($hqaccountbalance, 2)."</td>";
	//echo "<td>income".number_format(round($income), 2)."</td>";
	//echo "<td>expenditure".number_format(round($expenditure), 2)."</td>";
	//echo "<td>taxexpenditure".number_format(round($taxexpenditure), 2)."</td>";
	//echo "<td>transfersin".number_format(round($transfersin), 2)."</td>";
	//echo "<td>transfersout".number_format(round($transfersout), 2)."</td>";
	if($financialaccountid == 0){
		echo "<td><a href='view.php?viewid=16&financialaccountid=".$finaccountid."'>".$langval141."</a></td>";
	}	
	echo "</tr>";
	$totalaccountbalance = $totalaccountbalance + $accountbalance;
	$totalhqaccountbalance = $totalhqaccountbalance + $hqaccountbalance;
}
if($financialaccountid == 0){
	echo "<tr><td></td><td></td><td></td><td></td><td></td><td></td>";
	echo "<td><b>".number_format(round($totalaccountbalance), 2)."</b></td>";
	echo "<td><b>".number_format(round($totalhqaccountbalance), 2)."</b></td><td></td></tr>";
}
echo "</table>";	

if($financialaccountid >= 1){
	//show back button
	echo "<a href='view.php?viewid=16'><button type='submit' name='submit' value='Submit' class='button-primary'>".$langval142."</button></a>";	
	echo "<br/><br/>";
	
	//show a list of financial transactions, tax payments, and account transfers with incorrect currency code
	$getbadtransactions = mysql_query("select * from financialtransaction 
	inner join financialaccount on financialaccount.financialaccountid = financialtransaction.financialaccountid
	where financialtransaction.financialaccountid = '$financialaccountid' and financialtransaction.currencyid <> financialaccount.currencyid");
	$count1 = mysql_num_rows($getbadtransactions);
	$getbadtaxpayments = mysql_query("select * from taxpayment 
	inner join financialaccount on financialaccount.financialaccountid = taxpayment.financialaccountid
	where taxpayment.financialaccountid = '$financialaccountid' and taxpayment.currencyid <> financialaccount.currencyid");
	$count2 = mysql_num_rows($getbadtaxpayments);
	$getfiinancialaccounttransfer = mysql_query("select * from financialaccounttransfer 
	inner join financialaccount on financialaccount.financialaccountid = financialaccounttransfer.fromfinancialaccountid
	where financialaccounttransfer.fromfinancialaccountid = '$financialaccountid' and 
	financialaccounttransfer.fromcurrencyid <> financialaccount.currencyid");
	$count3 = mysql_num_rows($getfiinancialaccounttransfer);
	$getfiinancialaccounttransfer2 = mysql_query("select * from financialaccounttransfer 
	inner join financialaccount on financialaccount.financialaccountid = financialaccounttransfer.tofinancialaccountid
	where financialaccounttransfer.tofinancialaccountid = '$financialaccountid' and 
	financialaccounttransfer.tocurrencyid <> financialaccount.currencyid");
	$count4 = mysql_num_rows($getfiinancialaccounttransfer2);
	$count = $count1 + $count2 + $count3 + $count4;
	
	if($count >= 1){
		echo "<h2>".$langval143."</h2>";
		echo $langval144;
		echo "<table class='table table-bordered' style='width:80%;'>";
		echo "<thead>";
		echo "<td><b>".$langval145."</b></td>";
		echo "<td><b>".$langval146."</b></td>";
		echo "<td><b>".$langval147."</b></td>";
		echo "<td><b>".$langval148."</b></td>";
		echo "<td><b>".$langval149."</b></td>";
		echo "</thead>";
		while($row34=mysql_fetch_array($getbadtransactions)){
			$id = $row34['financialtransactionid'];
			$date = $row34['transactiondate'];
			echo "<tr>";	
			echo "<td>".$langval150."</td>";
			echo "<td>FT".$id."</td>";
			echo "<td>".$date."</td>";
			echo "<td>".$langval151."</td>";
			echo "<td><a href='pageedit.php?pagetype=financialtransaction&rowid=".$id."&pagename=".$pagename."'>".$langval149."</a></td>";
			echo "</tr>";	
		}
		while($row34=mysql_fetch_array($getbadtaxpayments)){
			$id = $row34['taxpaymentid'];
			$date = $row34['paiddate'];
			echo "<tr>";	
			echo "<td>".$langval152."</td>";
			echo "<td>TP".$id."</td>";
			echo "<td>".$date."</td>";
			echo "<td>".$langval153."</td>";
			echo "<td><a href='pageedit.php?pagetype=taxpayment&rowid=".$id."&pagename=".$pagename."'>".$langval149."</a></td>";
			echo "</tr>";	
		}
		while($row34=mysql_fetch_array($getfiinancialaccounttransfer)){
			$id = $row34['financialaccounttransferid'];
			$date = $row34['transferdate'];
			echo "<tr>";	
			echo "<td>".$langval154."</td>";
			echo "<td>FAT".$id."</td>";
			echo "<td>".$date."</td>";
			echo "<td>".$langval155."</td>";
			echo "<td><a href='pageedit.php?pagetype=financialaccounttransfer&rowid=".$id."&pagename=".$pagename."'>".$langval149."</a></td>";
			echo "</tr>";	
		}
		while($row34=mysql_fetch_array($getfiinancialaccounttransfer2)){
			$id = $row34['financialaccounttransferid'];
			$date = $row34['transferdate'];
			echo "<tr>";	
			echo "<td>".$langval154."</td>";
			echo "<td>FAT".$id."</td>";
			echo "<td>".$date."</td>";
			echo "<td>".$langval155."</td>";
			echo "<td><a href='pageedit.php?pagetype=financialaccounttransfer&rowid=".$id."&pagename=".$pagename."'>".$langval149."</a></td>";
			echo "</tr>";	
		}
		echo "</table>";	
	}
		
	//date filters to filter financial transactions/account transfers list
	echo "<h2>".$langval156."</h2>";
	echo "<p>".$langval157."</p>";
		
	?>
	
	<form class="form-horizontal" action='view.php?viewid=16&financialaccountid=<?php echo $financialaccountid ?>' method="post">
		<div class="form-group">
			<label for="Start Date" class="col-xs-12 col-sm-2 col-md-2 col-lg-1 control-label"><?php echo $langval9 ?></label>
	    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
	      	<input type="date" class="form-control" name="startdate" value="<?php echo $startdate ?>">
	    	</div>
	  		<label for="End Date" class="col-xs-12 col-sm-2 col-md-2 col-lg-1 control-label"><?php echo $langval10 ?></label>
	    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
	      	<input type="date" class="form-control" name="enddate" value="<?php echo $enddate ?>">
	    	</div>
	  	  	<div class="col-md-3 col-lg-1 col-sm-3 col-xs-3">
	    		<div style="text-align:center;">
	      		<button type='submit' name='submit' value='Submit' class="button-primary"><?php echo $langval5 ?></button>																									
	    		</div>
	    	</div>		
		</div>
 	</form>
	
	<?php		
		
	//grid of all financial transactions, tax payments, and account transfers in date descending order
	echo "<table class='table table-bordered' style='width:80%;'>";
	echo "<thead>";
	echo "<td><b>".$langval147."</b></td>";
	echo "<td><b>".$langval146."</b></td>";
	echo "<td><b>".$langval145."</b></td>";
	echo "<td><b>".$langval158."</b></td>";
	echo "<td><b>".$langval159."</b></td>";
	echo "<td><b>".$langval160."</b></td>";
	echo "<td><b>".$langval161."</b></td>";
	echo "<td><b>".$langval149."</b></td>";
	echo "</thead>";
	$csv_hdr = $langval147.",".$langval146.",".$langval145.",".$langval158.",".$langval159.",".$langval160.",".$langval161;
	$csv_output ="";
			      	
	//calculate start balance value
	$balance = 0;
	$getoldval = mysql_query("select transactiondate as 'Date', concat('FT', financialtransactionid) as 'ID', 'Transaction' as 'Type', 
	financialtransactionname as 'Name', amount as 'Amount In', 0 as 'Amount Out' from financialtransaction 
	inner join financesubcategory on financesubcategory.financesubcategoryid = financialtransaction.financesubcategoryid
	where financialaccountid = '$financialaccountid' and financecategoryid = '1' and financialtransaction.disabled = 0 
	and financialtransactionstatusid in (3,5) and transactiondate < '$startdate'
	UNION
	select transactiondate as 'Date', concat('FT', financialtransactionid) as 'ID', 'Transaction' as 'Type', 
	financialtransactionname as 'Name', 0 as 'Amount In', amount as 'Amount Out' from financialtransaction 
	inner join financesubcategory on financesubcategory.financesubcategoryid = financialtransaction.financesubcategoryid
	where financialaccountid = '$financialaccountid' and financecategoryid = '2' and financialtransaction.disabled = 0 
	and financialtransactionstatusid in (3,5) and transactiondate < '$startdate'
	UNION
	select paiddate as 'Date', concat('TP', taxpaymentid) as 'ID', 'Tax Payment' as 'Type', taxpaymentname as 'Name', 
	if(paymentamount <= 0, paymentamount * -1, 0) as 'Amount In', if(paymentamount > 0, paymentamount, 0) as 'Amount Out' 
	from taxpayment 
	where financialaccountid = '$financialaccountid' and taxpayment.disabled = 0 and taxpaymentstatusid = 2
	and paiddate < '$startdate'
	UNION
	select transferdate as 'Date', concat('FAT', financialaccounttransferid) as 'ID', 'Account Transfer In' as 'Type',
	financialaccounttransfername as 'Name', toamount as 'Amount In', 0 as 'Amount Out' from financialaccounttransfer 
	where tofinancialaccountid = '$financialaccountid' and financialaccounttransfer.disabled = 0
	and transferdate < '$startdate'
	UNION
	select transferdate as 'Date', concat('FAT', financialaccounttransferid) as 'ID', 'Account Transfer Out' as 'Type',
	 financialaccounttransfername as 'Name', 0 as 'Amount In', fromamount as 'Amount Out' from financialaccounttransfer 
	where fromfinancialaccountid = '$financialaccountid' and financialaccounttransfer.disabled = 0
	and transferdate < '$startdate'");
	while($row11=mysql_fetch_array($getoldval)){
		$balance = $balance + $row11['Amount In'] - $row11['Amount Out'];
	}
	
	//get records
	$getrecord = mysql_query("select transactiondate as 'Date', concat('FT', financialtransactionid) as 'ID', 'Transaction' as 'Type', 
	financialtransactionname as 'Name', amountincludingtax as 'Amount In', 0 as 'Amount Out' from financialtransaction 
	inner join financesubcategory on financesubcategory.financesubcategoryid = financialtransaction.financesubcategoryid
	where financialaccountid = '$financialaccountid' and financecategoryid = '1' and financialtransaction.disabled = 0 
	and financialtransactionstatusid in (1,3,4,5) and transactiondate >='$startdate' and transactiondate <='$enddate'
	UNION
	select transactiondate as 'Date', concat('FT', financialtransactionid) as 'ID', 'Transaction' as 'Type', 
	financialtransactionname as 'Name', 0 as 'Amount In', amountincludingtax as 'Amount Out' from financialtransaction 
	inner join financesubcategory on financesubcategory.financesubcategoryid = financialtransaction.financesubcategoryid
	where financialaccountid = '$financialaccountid' and financecategoryid = '2' and financialtransaction.disabled = 0 
	and financialtransactionstatusid in (1,3,4,5) and transactiondate >='$startdate' and transactiondate <='$enddate'
	UNION
	select paiddate as 'Date', concat('TP', taxpaymentid) as 'ID', 'Tax Payment' as 'Type', taxpaymentname as 'Name', 
	if(paymentamount <= 0, paymentamount * -1, 0) as 'Amount In', if(paymentamount > 0, paymentamount, 0) as 'Amount Out' 
	from taxpayment 
	where financialaccountid = '$financialaccountid' and taxpayment.disabled = 0 and taxpaymentstatusid = 2
	and paiddate >='$startdate' and paiddate <='$enddate'
	UNION
	select transferdate as 'Date', concat('FAT', financialaccounttransferid) as 'ID', 'Account Transfer In' as 'Type',
	financialaccounttransfername as 'Name', toamount as 'Amount In', 0 as 'Amount Out' from financialaccounttransfer 
	where tofinancialaccountid = '$financialaccountid' and financialaccounttransfer.disabled = 0
	and transferdate >='$startdate' and transferdate <='$enddate'
	UNION
	select transferdate as 'Date', concat('FAT', financialaccounttransferid) as 'ID', 'Account Transfer Out' as 'Type',
	 financialaccounttransfername as 'Name', 0 as 'Amount In', fromamount as 'Amount Out' from financialaccounttransfer 
	where fromfinancialaccountid = '$financialaccountid' and financialaccounttransfer.disabled = 0
	and transferdate >='$startdate' and transferdate <='$enddate'
	order by Date asc");
	while($row99=mysql_fetch_array($getrecord)){
		$date = $row99['Date'];
		$id = $row99['ID'];
		$type = $row99['Type'];
		$name = $row99['Name'];
		$amountin = $row99['Amount In'];
		$amountout = $row99['Amount Out'];
		$balance = $balance + $row99['Amount In'] - $row99['Amount Out'];
		echo "<tr>";
		echo "<td>".$date."</td>";
		echo "<td>".$id."</td>";
		echo "<td>".$type."</td>";
		echo "<td>".$name."</td>";
		echo "<td align='right'>".number_format($amountin, 2)."</td>";
		echo "<td align='right'>".number_format($amountout, 2)."</td>";
		echo "<td align='right'>".number_format($balance, 2)."</td>";
		$csv_output .= $date.",".$id.",".$type.",".$name.",".$amountin.",".$amountout.",".$balance."\n";	
		if($type == "Account Transfer In" || $type == "Account Transfer Out"){
			echo "<td><a href='pageedit.php?pagetype=financialaccounttransfer&rowid=".$id."&pagename=".$pagename."'>".$langval149."</a></td>";
		}		
		if($type == "Tax Payment"){
			echo "<td><a href='pageedit.php?pagetype=taxpayment&rowid=".$id."&pagename=".$pagename."'>".$langval149."</a></td>";
		}		
		if($type == "Transaction"){
			echo "<td><a href='pageedit.php?pagetype=financialtransaction&rowid=".$id."&pagename=".$pagename."'>".$langval149."</a></td>";
		}		
		echo "</tr>";
	}
	echo "</table>";	
	
}

?>
<form name="export" action="export.php" method="post">
    <input class="button-primary" type="submit" value="<?php echo $langval8 ?>">
    <input type="hidden" value='<?php echo $csv_hdr; ?>' name='csv_hdr'>
    <input type="hidden" value='<?php echo $csv_output; ?>' name='csv_output'>
</form>
