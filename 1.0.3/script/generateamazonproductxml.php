<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include('config.php');
$date = date("Y-m-d");
$hour = date("H");
$minute = date("i");
echo "<br/>hour: ".$hour;
echo "<br/>minute: ".$minute;

echo "<br/><br/><b>DELETE XML IF PRODUCT LOADED ONTO AMAZON</b>";
//don't need to run this too often, and so randomising it so it only runs approximately every 5th time
$rand = rand(0,10);
if($rand == 3){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	//UPDATE 27/5/2019: added 1000003 to list of ecommercesiteconfigid's for 2nd hand books to be processed
	$updatexml = "update ecommercelistinglocation 
	set amazonproductxml = ''
	where ecommercelistinglocationstatusid not in (1,8,9) and amazonproductxml <> '' and ecommercesiteconfigid in (1000002, 1000003)";
	echo "<br/><br/>".$updatexml;
	$updatexml = mysql_query($updatexml);
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Update xml to remove if no longer needed: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
}
else {
	echo "<br/>Skipped";
}


echo "<br/><br/><b>GET ECOMMERCE LISTING LOCATION RECORDS WITHOUT PRODUCT XML</b>";
//create products that have listing locations
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
//UPDATE 27/5/2019: added 1000003 to list of ecommercesiteconfigid's for 2nd hand books to be processed
$getlistinglocations = "select ecommercelistinglocationid, ecommercelistingid, listingprice
from $database.ecommercelistinglocation
where ecommercelistinglocationstatusid = 1 and ecommercesiteconfigid in (1000002, 1000003) 
and amazonproductxml = '' and listingprice > 0
limit 500";
echo "<br/><br/>".$getlistinglocations;
$getlistinglocations = mysql_query($getlistinglocations);
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get list of products that require processing: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
$listingids = "";
$ecommercelistinglocationarray = array();
while($row11 = mysql_fetch_array($getlistinglocations)){
	$ecommercelistinglocationid = $row11['ecommercelistinglocationid'];
	$ecommercelistingid = $row11['ecommercelistingid'];
	$listingprice = $row11['listingprice'];
	$listingids = $listingids.$ecommercelistingid.",";	
	array_push($ecommercelistinglocationarray, array('ecommercelistinglocationid'=>$ecommercelistinglocationid,
	'listingprice'=>$listingprice,'ecommercelistingid'=>$ecommercelistingid));
}
$listingids = rtrim($listingids, ",");
echo "<br/><br/>listingids: ".$listingids;
echo "<br/><br/>ecommercelistinglocationarray: ".json_encode($ecommercelistinglocationarray);
echo "<br/><br/>";

if($listingids <> ""){
	//get ecommercelisting records
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getlistings = "select ecommercelistingid, productstockitemid
	from $database.ecommercelisting
	where ecommercelistingid in ($listingids)";
	echo "<br/><br/>".$getlistings;
	$getlistings = mysql_query($getlistings);
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get list of ecommercelisting records: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	$productstockitemids = "";
	$ecommercelistingarray = array();
	while($row11 = mysql_fetch_array($getlistings)){
		$ecommercelistingid = $row11['ecommercelistingid'];
		$productstockitemid = $row11['productstockitemid'];
		$productstockitemids = $productstockitemids.$productstockitemid.",";	
		array_push($ecommercelistingarray, array('ecommercelistingid'=>$ecommercelistingid,
		'productstockitemid'=>$productstockitemid));
	}
	
	echo "<br/><br/>ecommercelistingarray: ".json_encode($ecommercelistingarray);
	$productstockitemids = rtrim($productstockitemids, ",");
	echo "<br/><br/>productstockitemids: ".$productstockitemids;
	echo "<br/><br/>";
	
	
	//get productstockitem records
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getproductstockitem = "select productstockitemid, productitemdescription, productid
	from $database.productstockitem
	where productstockitemid in ($productstockitemids)";
	echo "<br/><br/>".$getproductstockitem;
	$getproductstockitem = mysql_query($getproductstockitem);
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get list of productstockitem records: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	$productids = "";
	$productstockitemarray = array();
	while($row11 = mysql_fetch_array($getproductstockitem)){
		$productstockitemid = $row11['productstockitemid'];
		$productitemdescription = $row11['productitemdescription'];
		$productitemdescription = str_replace(" ", "", $productitemdescription);
		$productitemdescription = str_replace("-", "", $productitemdescription);
		$productid = $row11['productid'];
		$productids = $productids.$productid.",";	
		array_push($productstockitemarray, array('productstockitemid'=>$productstockitemid,
		'productitemdescription'=>$productitemdescription,'productid'=>$productid));
	}
	
	echo "<br/><br/>productstockitemarray: ".json_encode($productstockitemarray);
	$productids = rtrim($productids, ",");
	echo "<br/><br/>productids: ".$productids;
	echo "<br/><br/>";
	
	
	
	//get product records
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getproduct = "select productid, productname, productlongdescription, 
	productproducer, productuniqueid, retailprice, kgweight
	from $database.product
	where productid in ($productids)";
	echo "<br/><br/>".$getproduct;
	$getproduct = mysql_query($getproduct);
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get list of product records: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	$productarray = array();
	while($row11 = mysql_fetch_array($getproduct)){
		$productid = $row11['productid'];	
		$productname = htmlspecialchars($row11['productname']);	
		$productlongdescription = addslashes(strip_tags($row11['productlongdescription']));	
		$productproducer = htmlspecialchars($row11['productproducer']);	
		$productuniqueid = $row11['productuniqueid'];	
		$retailprice = $row11['retailprice'];	
		$kgweight = $row11['kgweight'];	
		array_push($productarray, array('productid'=>$productid,
		'productname'=>$productname,'productlongdescription'=>$productlongdescription,
		'productproducer'=>$productproducer,'productuniqueid'=>$productuniqueid,
		'retailprice'=>$retailprice,'kgweight'=>$kgweight));
	}
	
	echo "<br/><br/>productarray: ".json_encode($productarray);
	echo "<br/><br/>";
	
	
	//get zzproductdata records
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getproductdata = "select productid, publishername, binding, publicationdate 
	from $database.zzproductdata
	where productid in ($productids)";
	echo "<br/><br/>".$getproductdata;
	$getproductdata = mysql_query($getproductdata);
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get list of zzproductdata records: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	$productdataarray = array();
	while($row11 = mysql_fetch_array($getproductdata)){
		$productid = $row11['productid'];	
		$publishername = htmlspecialchars($row11['publishername']);	
		$binding = htmlspecialchars($row11['binding']);	
		$publicationdate = $row11['publicationdate'];	
		array_push($productdataarray, array('productid'=>$productid,
		'publishername'=>$publishername,'binding'=>$binding,'publicationdate'=>$publicationdate));
	}
	
	echo "<br/><br/>productdataarray: ".json_encode($productdataarray);
	echo "<br/><br/>";
	
	
	
	//construct the xml and process update
	$updatequery = "update ecommercelistinglocation set amazonproductxml = case ecommercelistinglocationid ";
	$updatequerywhere = "";
	if(count($productdataarray) >= 1){
		foreach($ecommercelistinglocationarray as $ecommercelistinglocation){
			//get all of the variables needed for the listing
			$ecommercelistinglocationid = $ecommercelistinglocation['ecommercelistinglocationid'];
			$ecommercelistingid = $ecommercelistinglocation['ecommercelistingid'];
			$listingprice = $ecommercelistinglocation['listingprice'];
			echo "<br/>listing: ".$ecommercelistinglocationid." - ".$ecommercelistingid." - ".$listingprice;
			$key = array_search($ecommercelistingid, array_column($ecommercelistingarray, 'ecommercelistingid'));
			$productstockitemid = $ecommercelistingarray[$key]['productstockitemid'];
			echo " - ".$productstockitemid;
			$key = array_search($productstockitemid, array_column($productstockitemarray, 'productstockitemid'));
			$productid = $productstockitemarray[$key]['productid'];
			$productitemdescription = $productstockitemarray[$key]['productitemdescription'];
			echo " - ".$productid;
			$key = array_search($productid, array_column($productarray, 'productid'));
			$productname = htmlspecialchars($productarray[$key]['productname']);	
			$productlongdescription = addslashes(strip_tags($productarray[$key]['productlongdescription']));	
			$productlongdescription = htmlspecialchars($productlongdescription);	
			$productproducer = htmlspecialchars($productarray[$key]['productproducer']);	
			$productuniqueid = $productarray[$key]['productuniqueid'];	
			$retailprice = $productarray[$key]['retailprice'];	
			$kgweight = $productarray[$key]['kgweight'];	
			echo " - ".$productname." - ".$productlongdescription." - ".$productproducer;
			echo " - ".$productuniqueid." - ".$retailprice." - ".$kgweight;
			$key = array_search($productid, array_column($productdataarray, 'productid'));
			$publishername = htmlspecialchars($productdataarray[$key]['publishername']);	
			$publicationdate = $productdataarray[$key]['publicationdate'];	
			$binding = $productdataarray[$key]['binding'];	
			echo " - ".$publishername." - ".$binding." - ".$publicationdate;
			$lbweight = $kgweight*2.20462;
			$uniqueid = "PSI-".$productstockitemid;	
			$binding = str_replace(" & ", " and ", $binding);
			if(stripos($binding, "Library Binding") !== false) {
    			$binding = 'Library';
			}	
			if($binding == 'Print on Demand (Paperback)'){
				$binding = 'Podpaperback';			
			}
			if($binding == 'Print on Demand (Hardcover)'){
				$binding = 'Podhardback';			
			}
			if($binding == 'Audio Cassette'){
				$binding = 'Cassette';			
			}
			if($binding == 'Digital'){
				$binding = 'Cdrom';			
			}
			if($binding == 'Office Product'){
				$binding = 'Miscsupplies';			
			}
			if($binding == 'DVD'){
				$binding = 'Dvdrom';			
			}
			$binding = htmlspecialchars($binding);	
			$binding = str_replace("-", "", $binding);
			$binding = str_replace(" ", "", $binding);
			$binding = str_replace(".", "", $binding);
			$binding = strtolower($binding);
			$binding = ucfirst($binding);
			if($binding == 'Massmarketpaperback'){
				$binding = 'Massmarket';			
			}
			if($binding == 'Librarybinding'){
				$binding = 'Library';			
			}
			if($binding == 'Plasticcomb'){
				$binding = 'Unknownbinding';			
			}
			if($binding == 'Perfectpaperback'){
				$binding = 'Paperback';			
			}
			if($binding == '' || $binding == 'Singleissuemagazine' || $binding == 'Staplebound'){
				$binding = 'Unknownbinding';			
			}
			if($publicationdate == '0000-00-00'){
				$publicationdate = '2000-01-01';
			}
			if($publicationdate >= $date){
				$publicationdate = '2018-01-01';
			}
			if($productproducer == ''){
				$productproducer = "Unknown";
			}
			$productlongdescription = substr($productlongdescription,0,1950);
			$productuniqueid = trim($productuniqueid);
			$productuniqueid = str_replace("-", "", $productuniqueid);
			
			
			//construct product xml string
			$amazonproductdatasegment = '<Product>
		      <SKU>'.$uniqueid.'</SKU>
		      <StandardProductID>
		        <Type>ISBN</Type>
		        <Value>'.$productuniqueid.'</Value>
		      </StandardProductID>
		      <Condition>
		         <ConditionType>'.$productitemdescription.'</ConditionType>
		      </Condition>
		      <NumberOfItems>1</NumberOfItems>
		      <DescriptionData>
		        <Title>'.$productname.'</Title>
		        <Brand></Brand>
		        <Description>'.$productlongdescription.'</Description>
		        <MSRP currency="GBP">'.$retailprice.'</MSRP>
		        <Manufacturer>'.$publishername.'</Manufacturer>
		        <ItemType>example-item-type</ItemType>
		      </DescriptionData>    
				<ProductData>
		         <Books>
		           <ProductType>
		             <BooksMisc>
		               <Author>'.$productproducer.'</Author>
		               <Binding>'.$binding.'</Binding>
		               <PublicationDate>'.$publicationdate.'T00:00:00</PublicationDate>
		             </BooksMisc>
		           </ProductType>
		         </Books>
		       </ProductData>
		    </Product>';
		    $amazonproductdatasegment = mysql_real_escape_string($amazonproductdatasegment);
			$updatequery = $updatequery." when ".$ecommercelistinglocationid." then '".$amazonproductdatasegment."' ";
			$updatequerywhere = $updatequerywhere.$ecommercelistinglocationid.",";
		}
	}
	if($updatequerywhere <> ""){
		$updatequerywhere = rtrim($updatequerywhere, ",");
		$updatequery = $updatequery."else '' end where ecommercelistinglocationid in (".$updatequerywhere.")";
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		echo "<br/><br/>".$updatequery;
		$updatequery = mysql_query($updatequery);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Insert the amazon id of the file being processed: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	}
}



echo "<br/><br/><b>CHECK FOR COMMON ISSUES WITH SUBMITTED RECORDS AND FIX THEM</b>";

echo "<br/><br/>CHECK: Publication Date";
if($hour == 0 && $minute <= 5){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getwrongpublicationdate = mysql_query("select ecommercelistinglocationid, productid, productstockitemid, integrationerrormessage
	from ecommercelistinglocation where ecommercesiteconfigid = 1000002 
	and ecommercelistinglocationstatusid = 7 and ((integrationerrormessage like '%The SKU data provided is different from what%' 
	and integrationerrormessage like '%standard_product_id data provided matches ASIN%' 
	and integrationerrormessage like '%already in the Amazon catalog: publication_date%') or 
	(integrationerrormessage like '%The SKU data provided conflicts with the Amazon catalog%' and 
	integrationerrormessage like '%but some information contradicts with the Amazon catalog%'  and 
	integrationerrormessage like '%The following are the attribute value(s) that are conflicting: publication_date%'))");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get list of records with wrong publication date: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	$updatequery = "update zzproductdata set publicationdate = case productid ";
	$updateproductidlist = "";
	$ecommercelistinglocationidlist = "";
	while($getwrongpublicationdaterow = mysql_fetch_array($getwrongpublicationdate)){
		$ecommercelistinglocationid = $getwrongpublicationdaterow['ecommercelistinglocationid'];
		$productid = $getwrongpublicationdaterow['productid'];
		$productstockitemid = $getwrongpublicationdaterow['productstockitemid'];
		$integrationerrormessage = $getwrongpublicationdaterow['integrationerrormessage'];
		
		echo "<br/><br/>".$ecommercelistinglocationid." - ".$productid." - ".$productstockitemid;
		
		if(strpos($integrationerrormessage,'that are conflicting: publication_date') !== false) {
	    	$publicationstring = strstr($integrationerrormessage, 'that are conflicting: publication_date');
		}
		else {
			$publicationstring = strstr($integrationerrormessage, 'in the Amazon catalog: publication_date');
		}
		echo "<br/>RESULT: ";
		$publicationstring = str_replace('that are conflicting: publication_date', '', $publicationstring);
		$publicationstring = str_replace('in the Amazon catalog: publication_date', '', $publicationstring);
		$arr = explode(")", $publicationstring, 2);
		$publicationstring = $arr[0];	
		$publicationstring = str_replace('(', '', $publicationstring);
		$publicationstring = strstr($publicationstring, '/ Amazon: ');
		$publicationstring = str_replace("/ Amazon: '", '', $publicationstring);
		$publicationstring = str_replace("T00:00:01Z", '', $publicationstring);
		$publicationstring = str_replace("'", '', $publicationstring);
		if(strpos($publicationstring,'T') !== false) {
			echo $publicationstring." edit - ";
	    	$publicationstring = str_replace('T', '-01-01', $publicationstring);
		}
		
		if($publicationstring <> ''){
			$updatequery = $updatequery." when ".$productid." then '".$publicationstring."' ";
		}
		$updateproductidlist = $updateproductidlist.$productid.",";
		$ecommercelistinglocationidlist = $ecommercelistinglocationidlist.$ecommercelistinglocationid.",";
		
		echo $publicationstring;
	}
	
	if($updateproductidlist <> ''){
		$updateproductidlist = rtrim($updateproductidlist, ",");
		$ecommercelistinglocationidlist = rtrim($ecommercelistinglocationidlist, ",");
		$updatequery = $updatequery." end where productid in ($updateproductidlist)";
		echo "<br/><br/>updatequery: ".$updatequery;
		
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$updatequery = mysql_query($updatequery);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Update zzproductdata with new publication dates: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$updatequery2 = mysql_query("update ecommercelistinglocation 
		set ecommercelistinglocationstatusid = 1, integrationproductid = '', integrationerrormessage = '', 
		integrationerrorretrynumber = 0, amazonproductxml = '' 
		where ecommercelistinglocationid in ($ecommercelistinglocationidlist)");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Update ecommercelistinglocationrecords to set the records back to unprocessed: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	}
	$updateproductidlist = "";
}

if($hour == 3 && $minute <= 5){
	echo "<br/><br/>CHECK: Binding";
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getwrongpublicationdate = mysql_query("select ecommercelistinglocationid, productid, productstockitemid, integrationerrormessage
	from ecommercelistinglocation where ecommercesiteconfigid = 1000002 
	and ecommercelistinglocationstatusid = 7 and ((integrationerrormessage 
	like '%but the following data is different from %' and integrationerrormessage 
	like '%already in the Amazon catalog: binding%') or (integrationerrormessage 
	like '%conflicts with the Amazon catalog. The standard_product_id value(s) provided correspond to the ASIN%' and integrationerrormessage 
	like '%The following are the attribute value(s) that are conflicting: binding (%'))");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get list of records with wrong binding type: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	$updatequery = "update zzproductdata set binding = case productid ";
	$updateproductidlist = "";
	$ecommercelistinglocationidlist = "";
	while($getwrongpublicationdaterow = mysql_fetch_array($getwrongpublicationdate)){
		$ecommercelistinglocationid = $getwrongpublicationdaterow['ecommercelistinglocationid'];
		$productid = $getwrongpublicationdaterow['productid'];
		$productstockitemid = $getwrongpublicationdaterow['productstockitemid'];
		$integrationerrormessage = $getwrongpublicationdaterow['integrationerrormessage'];
		
		echo "<br/><br/>".$ecommercelistinglocationid." - ".$productid." - ".$productstockitemid;
		
		if(strpos($integrationerrormessage,'Amazon catalog: binding (') !== false) {
	    	$bindingstring = strstr($integrationerrormessage, 'Amazon catalog: binding (');
		}
		else {
			$bindingstring = strstr($integrationerrormessage, 'are conflicting: binding (');
		}
		$bindingstring = str_replace('Amazon catalog: binding (', '', $bindingstring);
		$arr = explode(").", $bindingstring, 2);
		$bindingstring = $arr[0];	
		$arr = explode(")", $bindingstring, 2);
		$bindingstring = $arr[0];	
		$bindingstring = strstr($bindingstring, "/ Amazon: '");
		$bindingstring = str_replace("/ Amazon: '", '', $bindingstring);
		$bindingstring = str_replace("'", '', $bindingstring);
		$bindingstring = str_replace("_", " ", $bindingstring);
		$bindingstring = ucwords($bindingstring);
		
		if($bindingstring <> ''){
			$updatequery = $updatequery." when ".$productid." then '".$bindingstring."' ";
		}
		$updateproductidlist = $updateproductidlist.$productid.",";
		$ecommercelistinglocationidlist = $ecommercelistinglocationidlist.$ecommercelistinglocationid.",";
		
		echo "<br/>RESULT: ".$bindingstring;
	}
	
	if($updateproductidlist <> ''){
		$updateproductidlist = rtrim($updateproductidlist, ",");
		$ecommercelistinglocationidlist = rtrim($ecommercelistinglocationidlist, ",");
		$updatequery = $updatequery." end where productid in ($updateproductidlist)";
		echo "<br/><br/>updatequery: ".$updatequery;
		
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$updatequery = mysql_query($updatequery);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Update zzproductdata with new binding: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$updatequery2 = mysql_query("update ecommercelistinglocation 
		set ecommercelistinglocationstatusid = 1, integrationproductid = '', integrationerrormessage = '', 
		integrationerrorretrynumber = 0, amazonproductxml = '' 
		where ecommercelistinglocationid in ($ecommercelistinglocationidlist)");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Update ecommercelistinglocationrecords to set the records back to unprocessed: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	}
	$updateproductidlist = "";
}


if($hour ==10 && $minute <= 5){
	echo "<br/><br/>CHECK: Book Name";
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getwrongpublicationdate = mysql_query("select ecommercelistinglocationid, productid, productstockitemid, integrationerrormessage
	from ecommercelistinglocation where ecommercesiteconfigid = 1000002 
	and ecommercelistinglocationstatusid = 7 and ((integrationerrormessage 
	like '%The SKU data provided conflicts with the Amazon catalog. The standard_product_id value(s) provided correspond to the ASIN%' and integrationerrormessage 
	like '%The following are the attribute value(s) that are conflicting: item_name (%') 
	or (integrationerrormessage 
	like '%already in the Amazon catalog. The standard_product_id data provided matches ASIN%' and integrationerrormessage 
	like '%already in the Amazon catalog: item_name (%'))");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get list of records with potentially different book name: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	$updatequery = "update product set productname = case productid ";
	$updateproductidlist = "";
	$ecommercelistinglocationidlist = "";
	while($getwrongpublicationdaterow = mysql_fetch_array($getwrongpublicationdate)){
		$ecommercelistinglocationid = $getwrongpublicationdaterow['ecommercelistinglocationid'];
		$productid = $getwrongpublicationdaterow['productid'];
		$productstockitemid = $getwrongpublicationdaterow['productstockitemid'];
		$integrationerrormessage = $getwrongpublicationdaterow['integrationerrormessage'];
		
		echo "<br/><br/>".$ecommercelistinglocationid." - ".$productid." - ".$productstockitemid;
		
		$bookname1 = strstr($integrationerrormessage, ': item_name (');
		$bookname1 = str_replace(': item_name (', '', $bookname1);
		$arr = explode(").", $bookname1, 2);
		$bookname1 = $arr[0];	
		$arr = explode("/", $bookname1, 2);
		$booknamemerchant = $arr[0];	
		$booknameamazon = $arr[1];	
		
		$booknamemerchant = str_replace("Merchant: '", '', $booknamemerchant);
		$booknamemerchant = str_replace("'", '', $booknamemerchant);
		$booknameamazon = str_replace("Amazon: '", '', $booknameamazon);
		$booknameamazon = str_replace("'", '', $booknameamazon);
		$compare1 = similar_text($booknamemerchant, $booknameamazon, $perc1);
		$compare2 = similar_text($booknameamazon, $booknamemerchant, $perc2);
		
		echo "<br/>RESULT-MERCHANT: ".$booknamemerchant;
		echo "<br/>RESULT-AMAZON: ".$booknameamazon;
		echo "<br/>COMPARE1: ".$compare1." - ".$perc1;
		echo "<br/>COMPARE2: ".$compare2." - ".$perc2;
		if($perc1 >= 45 && $perc2 >= 45){
			$booknameamazon = mysql_real_escape_string($booknameamazon);
			echo "<br/>MATCH";
			$updatequery = $updatequery." when ".$productid." then '".$booknameamazon."' ";
			$updateproductidlist = $updateproductidlist.$productid.",";
			$ecommercelistinglocationidlist = $ecommercelistinglocationidlist.$ecommercelistinglocationid.",";	
		}
	
	}
	
	
	if($updateproductidlist <> ''){
		$updateproductidlist = rtrim($updateproductidlist, ",");
		$ecommercelistinglocationidlist = rtrim($ecommercelistinglocationidlist, ",");
		$updatequery = $updatequery." end where productid in ($updateproductidlist)";
		echo "<br/><br/>updatequery: ".$updatequery;
		
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$updatequery = mysql_query($updatequery);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Update zzproductdata with new product name: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$updatequery2 = mysql_query("update ecommercelistinglocation 
		set ecommercelistinglocationstatusid = 1, integrationproductid = '', integrationerrormessage = '', 
		integrationerrorretrynumber = 0, amazonproductxml = '' 
		where ecommercelistinglocationid in ($ecommercelistinglocationidlist)");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Update ecommercelistinglocationrecords to set the records back to unprocessed: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	}
	$updateproductidlist = "";
}

include('recordjobstatistics.php');

?>


