<?php 
    
//LANGUAGE COLLECTION SECTION
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (4,5,9,10,19,582,583,584,585,586,587,588,589,590,591,592,593,594,595,596,597,598,599,600,601,602,603,
604,605,606,607,608)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}

$displaytype = isset($_GET['displaytype']) ? $_GET['displaytype'] : '';
$statuschange = isset($_GET['statuschange']) ? $_GET['statuschange'] : '';
$rowid = isset($_GET['rowid']) ? $_GET['rowid'] : '';
$date = date("Y-m-d");	

if($statuschange <> ""){
	if($statuschange == "inprogress"){
		$statuschangeid = 2;	
	}
	if($statuschange == "onhold"){
		$statuschangeid = 3;	
	}
	if($statuschange == "cancelled"){
		$statuschangeid = 5;	
	}
	if($statuschange == "complete"){
		$statuschangeid = 4;	
	}
	$updatestatus = "update $database.task set taskstatusid = '$statuschangeid'"; 
	if($statuschange == "complete"){
		$updatestatus = $updatestatus.", datecompleted = '".$date."' ";
	}
	else {
		$updatestatus = $updatestatus.", datecompleted = '0000-00-00' ";
	}
	$updatestatus = $updatestatus."where taskid = '".$rowid."'";
	$updatestatus = mysql_query($updatestatus);
	$url = 'view.php?viewid=27&displaytype='.$displaytype;
	echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">';
}

$pagename = $_SERVER['REQUEST_URI'];
$pagename = str_replace("&", "xxxxxxxxxx", $pagename);							

$uid = $_SESSION['userid'];
$gettask = "select taskid, projectname, taskname, task.description, minsestimated, minstodate, 
datetargetcompletion, taskstatusname, task.taskstatusid from $database.task 
inner join $masterdatabase.taskstatus on taskstatus.taskstatusid = task.taskstatusid
inner join $database.project on project.projectid = task.oneprojectid
where assignedto = '$uid' and task.taskstatusid in (1,2,3)";
$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
if(isset($_POST['submit'])) {
	//add projectid to query
	$project = isset($_POST['project']) ? $_POST['project'] : '';	
	if($project >= 1){	
		$gettask = $gettask." and task.oneprojectid = '".$project."' ";
	}
	
	//add dates to query
	$startdate = isset($_POST['startdate']) ? $_POST['startdate'] : '';
	$enddate = isset($_POST['enddate']) ? $_POST['enddate'] : '';	
	if($startdate <> "" && $enddate <> ""){
		$gettask = $gettask." and datetargetcompletion <= '".$enddate."' and datetargetcompletion >= '".$startdate."'";
	}	
	if($startdate <> "" && $enddate == ""){
		$gettask = $gettask." and datetargetcompletion >= '".$startdate."'";
	}	
	if($startdate == "" && $enddate <> ""){
		$gettask = $gettask." and datetargetcompletion <= '".$enddate."'";
	}	
}
$gettask = $gettask." order by datetargetcompletion asc";
$gettask = mysql_query($gettask);

echo "<h2>".$langval582."</h2>";
?>
<form class="form-horizontal" action='view.php?viewid=27&displaytype=<?php echo $displaytype ?>' method="post">
	<div class="form-group">
		<div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
      		<label for="Start Date"><?php echo $langval9 ?></label>
    		<input type="date" class="form-control" name="startdate" value="<?php echo $startdate ?>">
    	</div>
  		<div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
      		<label for="End Date"><?php echo $langval10 ?></label>
    		<input type="date" class="form-control" name="enddate" value="<?php echo $enddate ?>">
    	</div>
 		<div class="col-xs-12 col-sm-4 col-md-3 col-lg-2" style="padding-bottom:10px;">
			<label for="Project"><?php echo $langval583 ?></label>	    		  	
 		  	<?php
			$querytype1 = "select projectid, projectname from $database.project 
			where disabled = 0 order by projectname asc";	    		  	
 		  	$resulttype = mysql_query ($querytype1);
        	echo "<select class='form-control' name='project'>";
        	echo "<option value=''>".$langval19."</option>";
      		while($nttype=mysql_fetch_array($resulttype)){
          	if ($project==$nttype['projectid']) {
            		echo "<option value=".$nttype['projectid']." selected='true'>".$nttype['projectname']."</option>";
          	}
          	else
            		echo "<option value=".$nttype['projectid']." >".$nttype['projectname']."</option>";
          	}
  				echo "</select>";
  			?>
    	</div>		
 		<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
 			<div style="text-align:center;">
   				<button type='submit' name='submit' value='Submit' class="button-primary"><?php echo $langval584 ?></button>																									
 			</div>
 		</div>	
 	</div>			
 	</form>
<?php		

echo "<h2>".$langval585."</h2>";
echo "<a class='button-primary' href='pageadd.php?pagetype=task&pagename=".$pagename."'>".$langval586."</a>&nbsp;&nbsp;&nbsp;&nbsp;";
echo "<b>".$langval587."</b><a href='view.php?viewid=27&displaytype=kanban'>".$langval588."</a>";
echo " | <a href='view.php?viewid=27&displaytype=tasklist'>".$langval589."</a>";
echo "<br/><br/>";
		
if($displaytype == "tasklist" || $displaytype == ""){
	if(mysql_num_rows($gettask) >= 1){		
		echo "<table class='table table-bordered'>";
		echo "<thead><tr>";
		echo "<th>".$langval590."</th>";
		echo "<th>".$langval591."</th>";
		echo "<th>".$langval592."</th>";
		echo "<th>".$langval593."</th>";
		echo "<th>".$langval594."</th>";
		echo "<th>".$langval595."</th>";
		echo "<th>".$langval596."</th>";
		echo "<th>".$langval597."</th>";
		echo "<th>".$langval598."</th>";
		echo "<th>".$langval599."</th>";
		echo "<th style='width:20%'>".$langval600."</th>";
		echo "</tr></thead>";
	}
	while($row22 = mysql_fetch_array($gettask)){
		$taskid = $row22['taskid'];
		$projectname = $row22['projectname'];
		$taskname = $row22['taskname'];
		$description = $row22['description'];
		$minsestimated = $row22['minsestimated'];
		$minstodate = $row22['minstodate'];
		$datetargetcompletion = $row22['datetargetcompletion'];
		$taskstatusname = $row22['taskstatusname'];
		if(strlen($description) >= 200){
			$description = substr($description, 0, 200)." . . .";
		}
		echo "<tr>";	
		echo "<td>".$taskid."</td>";
		echo "<td>".$projectname."</td>";
		echo "<td>".$taskname."</td>";
		echo "<td>".$description."</td>";
		echo "<td>".$minsestimated."</td>";
		echo "<td>".$minstodate."</td>";
		echo "<td>".$datetargetcompletion."</td>";
		echo "<td>".$taskstatusname."</td>";
		echo "<td><a href='view.php?viewid=26&rowid=".$taskid."'>".$langval598."</a></td>";
		echo "<td><a href='pageedit.php?pagetype=task&rowid=".$taskid."&pagename=".$pagename."'>".$langval599."</a></td>";
		if($taskstatusname == "New"){
			echo "<td><a href='view.php?viewid=27&rowid=".$taskid."&displaytype=tasklist&statuschange=inprogress'>".$langval601."</a> | ";		
			echo "<a href='view.php?viewid=27&rowid=".$taskid."&displaytype=tasklist&statuschange=cancelled'>".$langval602."</a></td>";		
		}
		if($taskstatusname == "In Progress"){
			echo "<td><a href='view.php?viewid=27&rowid=".$taskid."&displaytype=tasklist&statuschange=complete'>".$langval603."</a> | ";		
			echo "<a href='view.php?viewid=27&rowid=".$taskid."&displaytype=tasklist&statuschange=onhold'>".$langval604."</a> | ";		
			echo "<a href='view.php?viewid=27&rowid=".$taskid."&displaytype=tasklist&statuschange=cancelled'>".$langval602."</a></td>";		
		}
		if($taskstatusname == "On Hold"){
			echo "<td><a href='view.php?viewid=27&rowid=".$taskid."&displaytype=tasklist&statuschange=inprogress'>".$langval601."</a> | ";		
			echo "<a href='view.php?viewid=27&rowid=".$taskid."&displaytype=tasklist&statuschange=complete'>".$langval603."</a> | ";		
			echo "<a href='view.php?viewid=27&rowid=".$taskid."&displaytype=tasklist&statuschange=cancelled'>".$langval602."</a></td>";		
		}
		echo "</tr>";	
	}
	if(mysql_num_rows($gettask) >= 1){
		echo "</table>";
	}
}

if($displaytype == "kanban"){
	if(mysql_num_rows($gettask) >= 1){		
		echo "<table class='table table-bordered'>";
		echo "<thead><tr>";
		echo "<th style='width:33%;'>".$langval605."</th>";
		echo "<th style='width:33%;'>".$langval601."</th>";
		echo "<th style='width:34%;'>".$langval604."</th>";
		echo "</tr></thead>";
	}
	$taskidarraynew = array();
	$taskidarrayinprogress = array();
	$taskidarrayonhold = array();
	while($row22 = mysql_fetch_array($gettask)){
		$taskid = $row22['taskid'];
		$taskstatusname = $row22['taskstatusname'];
		if($taskstatusname == 'New'){
			array_push($taskidarraynew, $taskid);		
		}
		if($taskstatusname == 'In Progress'){
			array_push($taskidarrayinprogress, $taskid);		
		}
		if($taskstatusname == 'On Hold'){
			array_push($taskidarrayonhold, $taskid);		
		}
	}
	$countnew = count($taskidarraynew);
	$countinprogress = count($taskidarrayinprogress);
	$countonhold = count($taskidarrayonhold);
	$i = $countnew;
	if($countinprogress > $i){
		$i = $countinprogress;	
	}
	if($countonhold > $i){
		$i = $countonhold;	
	}
	$p = 0;
	while($p < $i){
		echo "<tr>";
		if(isset($taskidarraynew[$p])){
			echo "<td>";
			$taskid = $taskidarraynew[$p];
			$getrow = mysql_query("select * from $database.task 
			inner join $database.project on project.projectid = task.oneprojectid
			where taskid = '$taskid'");
			$getrowr = mysql_fetch_array($getrow);
			$taskname = $getrowr['taskname'];
			$projectname = $getrowr['projectname'];
			$datetargetcompletion = $getrowr['datetargetcompletion'];
			echo "<b>".$projectname."</b><br/>";
			echo $taskname."<br/>";
			echo $datetargetcompletion."<br/><br/>";
			echo $langval606.": <a href='view.php?viewid=26&rowid=".$taskid."'>".$langval598."</a> | ";
			echo "<a href='pageedit.php?pagetype=task&rowid=".$taskid."&pagename=".$pagename."'>".$langval599."</a><br/>";
			echo $langval607.": <a href='view.php?viewid=27&rowid=".$taskid."&displaytype=kanban&statuschange=inprogress'>".$langval601."</a> | ";		
			echo "<a href='view.php?viewid=27&rowid=".$taskid."&displaytype=kanban&statuschange=cancelled'>".$langval602."</a>";		
			echo "</td>";
		}
		else {
			echo "<td></td>";		
		}
		if(isset($taskidarrayinprogress[$p])){
			echo "<td>";
			$taskid = $taskidarrayinprogress[$p];
			$getrow = mysql_query("select * from $database.task 
			inner join $database.project on project.projectid = task.oneprojectid
			where taskid = '$taskid'");
			$getrowr = mysql_fetch_array($getrow);
			$taskname = $getrowr['taskname'];
			$projectname = $getrowr['projectname'];
			$datetargetcompletion = $getrowr['datetargetcompletion'];
			echo "<b>".$projectname."</b><br/>";
			echo $taskname."<br/>";
			echo $datetargetcompletion."<br/><br/>";
			echo $langval606.": <a href='view.php?viewid=26&rowid=".$taskid."'>".$langval598."</a> | ";
			echo "<a href='pageedit.php?pagetype=task&rowid=".$taskid."&pagename=".$pagename."'>".$langval599."</a><br/>";
			echo $langval607.": <a href='view.php?viewid=27&rowid=".$taskid."&displaytype=kanban&statuschange=complete'>".$langval603."</a> | ";		
			echo "<a href='view.php?viewid=27&rowid=".$taskid."&displaytype=kanban&statuschange=onhold'>".$langval604."</a> | ";		
			echo "<a href='view.php?viewid=27&rowid=".$taskid."&displaytype=kanban&statuschange=cancelled'>".$langval602."</a>";		
			echo "</td>";
		}
		else {
			echo "<td></td>";		
		}
		if(isset($taskidarrayonhold[$p])){
			echo "<td>";
			$taskid = $taskidarrayonhold[$p];
			$getrow = mysql_query("select * from $database.task 
			inner join $database.project on project.projectid = task.oneprojectid
			where taskid = '$taskid'");
			$getrowr = mysql_fetch_array($getrow);
			$taskname = $getrowr['taskname'];
			$projectname = $getrowr['projectname'];
			$datetargetcompletion = $getrowr['datetargetcompletion'];
			echo "<b>".$projectname."</b><br/>";
			echo $taskname."<br/>";
			echo $datetargetcompletion."<br/><br/>";
			echo $langval606.": <a href='view.php?viewid=26&rowid=".$taskid."'>".$langval598."</a> | ";
			echo "<a href='pageedit.php?pagetype=task&rowid=".$taskid."&pagename=".$pagename."'>".$langval599."</a><br/>";
			echo $langval607.": <a href='view.php?viewid=27&rowid=".$taskid."&displaytype=kanban&statuschange=inprogress'>".$langval601."</a> | ";		
			echo "<a href='view.php?viewid=27&rowid=".$taskid."&displaytype=kanban&statuschange=complete'>".$langval603."</a> | ";		
			echo "<a href='view.php?viewid=27&rowid=".$taskid."&displaytype=kanban&statuschange=cancelled'>".$langval602."</a>";		
			echo "</td>";
		}
		else {
			echo "<td></td>";		
		}
		echo "</tr>";
		$p = $p + 1;
	}
	if(mysql_num_rows($gettask) >= 1){
		echo "</table>";
	}	
}

$dateweek = date('Y-m-d', strtotime($date. ' - 7 days'));
$getcompletetask = "select taskid, projectname, taskname, task.description, minsestimated, minstodate, 
datetargetcompletion, taskstatusname, task.taskstatusid, datecompleted from $database.task 
inner join $masterdatabase.taskstatus on taskstatus.taskstatusid = task.taskstatusid
inner join $database.project on project.projectid = task.oneprojectid
where assignedto = '$uid' and ((task.taskstatusid = 4 and datecompleted >= '$dateweek') or task.taskstatusid = 5)";
$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
if(isset($_POST['submit'])) {
	//add projectid to query
	$project = isset($_POST['project']) ? $_POST['project'] : '';	
	if($project >= 1){	
		$getcompletetask = $getcompletetask." and task.oneprojectid = '".$project."' ";
	}
	
	//add dates to query
	$startdate = isset($_POST['startdate']) ? $_POST['startdate'] : '';
	$enddate = isset($_POST['enddate']) ? $_POST['enddate'] : '';	
	if($startdate <> "" && $enddate <> ""){
		$getcompletetask = $getcompletetask." and datetargetcompletion <= '".$enddate."' and datetargetcompletion >= '".$startdate."'";
	}	
	if($startdate <> "" && $enddate == ""){
		$getcompletetask = $getcompletetask." and datetargetcompletion >= '".$startdate."'";
	}	
	if($startdate == "" && $enddate <> ""){
		$getcompletetask = $getcompletetask." and datetargetcompletion <= '".$enddate."'";
	}	
	
		
}
$getcompletetask = mysql_query($getcompletetask);

if(mysql_num_rows($getcompletetask) >= 1){
	echo "<br/><h2>Recently Completed Tasks</h2>";
	echo "<table class='table table-bordered'>";
	echo "<thead><tr>";
	echo "<th>".$langval590."</th>";
	echo "<th>".$langval591."</th>";
	echo "<th>".$langval592."</th>";
	echo "<th>".$langval593."</th>";
	echo "<th>".$langval594."</th>";
	echo "<th>".$langval595."</th>";
	echo "<th>".$langval596."</th>";
	echo "<th>".$langval608."</th>";
	echo "<th>".$langval597."</th>";
	echo "<th>".$langval598."</th>";
	echo "<th>".$langval599."</th>";
	echo "<th style='width:20%'>".$langval600."</th>";
	echo "</tr></thead>";
}
while($row23 = mysql_fetch_array($getcompletetask)){
	$taskid = $row23['taskid'];
	$projectname = $row23['projectname'];
	$taskname = $row23['taskname'];
	$description = $row23['description'];
	$minsestimated = $row23['minsestimated'];
	$minstodate = $row23['minstodate'];
	$datetargetcompletion = $row23['datetargetcompletion'];
	$datecompleted = $row23['datecompleted'];
	$taskstatusname = $row23['taskstatusname'];
	if(strlen($description) >= 200){
		$description = substr($description, 0, 200)." . . .";
	}
	echo "<tr>";	
	echo "<td>".$taskid."</td>";
	echo "<td>".$projectname."</td>";
	echo "<td>".$taskname."</td>";
	echo "<td>".$description."</td>";
	echo "<td>".$minsestimated."</td>";
	echo "<td>".$minstodate."</td>";
	echo "<td>".$datetargetcompletion."</td>";
	echo "<td>".$datecompleted."</td>";
	echo "<td>".$taskstatusname."</td>";
	echo "<td><a href='view.php?viewid=26&rowid=".$taskid."'>".$langval598."</a></td>";
	echo "<td><a href='pageedit.php?pagetype=task&rowid=".$taskid."&pagename=".$pagename."'>".$langval599."</a></td>";
	if($taskstatusname == "Cancelled"){
		echo "<td><a href='view.php?viewid=27&rowid=".$taskid."&displaytype=".$displaytype."&statuschange=inprogress'>".$langval601."</a> | ";		
		echo "<a href='view.php?viewid=27&rowid=".$taskid."&displaytype=".$displaytype."&statuschange=complete'>".$langval603."</a></td>";		
	}
	if($taskstatusname == "Complete"){
		echo "<td><a href='view.php?viewid=27&rowid=".$taskid."&displaytype=".$displaytype."&statuschange=inprogress'>".$langval601."</a> | ";		
		echo "<a href='view.php?viewid=27&rowid=".$taskid."&displaytype=".$displaytype."&statuschange=cancelled'>".$langval602."</a></td>";		
	}
	echo "</tr>";	
}
if(mysql_num_rows($getcompletetask) >= 1){
	echo "</table>";
}




?>