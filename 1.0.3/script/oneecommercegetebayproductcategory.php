<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$date = date('Y-m-d');
$datetime = date('Y-m-d H:i:s');
$notoinsert = 1000;

include('phpseclib1.0.15/Net/SFTP.php');
//http://phpseclib.sourceforge.net/sftp/examples.html


echo "<br/><br/><b>CHECK FOR PRODUCTS WITHOUT ecommerceproductcategory RECORDS</b>";
//check if there are any files already processing
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$getmissing = mysql_query("select product.productid from product 
inner join ecommercelistinglocation on ecommercelistinglocation.productid = product.productid
inner join ecommercesiteconfig on ecommercesiteconfig.ecommercesiteconfigid = ecommercelistinglocation.ecommercesiteconfigid
where amazonproductxml <> '' and product.productid not in (select productid from ecommerceproductcategory) and ecommercesiteid = 2
limit 1000");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get list of missing productids: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
$insertmissing = "insert into ecommerceproductcategory (productid, disabled, datecreated, masteronly) values ";
$missing = 0;
while($getmissingrow = mysql_fetch_array($getmissing)){
	$productid = $getmissingrow['productid'];
	echo "<br/>productid missing: ".$productid;
	$insertmissing = $insertmissing."($productid, 0, '$date', 0),";
	$missing = $missing + 1;
}
if($missing >= 1){
	$insertmissing = rtrim($insertmissing, ",");
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$insertmissing = mysql_query($insertmissing);
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Insert missing productids: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
}

			
echo "<br/><br/><b>CHECK IF ANY EXISTING BATCHES HAVE BEEN PROCESSED AND PROCESS RESULTS - PRODUCT IMPORT</b>";
//check if there are any files already processing
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$checkexistingfiles = mysql_query("select ecommerceprocessebayfilename, ecommercesiteconfig.ecommercesiteconfigid, 
apikey1, apikey2, apikey3, apikey4, apikey5, apikey6, datacentre, productindividual, ecommerceprocessebayfileid
from $database.ecommerceprocessebayfile
inner join $database.ecommercesiteconfig on ecommercesiteconfig.ecommercesiteconfigid = ecommerceprocessebayfile.ecommercesiteconfigid
where status = 'Get Category Data' and filetype = 'Category Data' and ecommerceprocessebayfilename <> ''");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get list of ebay config records: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
if(mysql_num_rows($checkexistingfiles) >= 1){
	//finish processing existing file
	$response = "";
	while($existingfile = mysql_fetch_array($checkexistingfiles)){
		$ecommerceprocessebayfileid = $existingfile['ecommerceprocessebayfileid'];
		$ecommercesiteconfigid = $existingfile['ecommercesiteconfigid'];
		$ecommerceprocessebayfilename = $existingfile['ecommerceprocessebayfilename'];
		$ftp_user_name = $existingfile['apikey1'];
		$ftp_user_pass = $existingfile['apikey2'];
		$channelid = $existingfile['apikey3'];		
		$shippingpolicy = $existingfile['apikey4'];
		$paymentpolicy = $existingfile['apikey5'];
		$returnpolicy = $existingfile['apikey6'];
		$ftp_server = $existingfile['datacentre'];
		$productindividual = $existingfile['productindividual'];
		
		
		$errorupdate = "update ecommercelistinglocation 
		set integrationerrorretrynumber = 1, ecommercelistinglocationstatusid = 7,
		integrationerrormessage = case integrationproductid ";
		$errorupdatewhere = "";
		
		
		echo "<br/><br/>Get the status of the feed: ".$ecommercesiteconfigid;
		echo " - ".$ecommerceprocessebayfilename." - ".$ecommerceprocessebayfilename;
		
		$found = 0;
		$sftp = new Net_SFTP($ftp_server);
		if (!$sftp->login($ftp_user_name, $ftp_user_pass)) {
		    exit('Login Failed');
		}
		else {
			echo "<br/><br/>Successful SFTP login<br/><br/>";	
			$today = date('M-d-Y');
			$nlist = json_encode($sftp->nlist('/store/classification/output/'.$today)); 
			//echo "<br/>nlist: ".$nlist;
			$nlist = str_replace("[", "", $nlist);
			$nlist = str_replace("]", "", $nlist);
			$nlist = str_replace('"', "", $nlist);
			$nlist = explode(",", $nlist);
			foreach($nlist as $file){
				//echo "<br/>file: ".$file;	
				$arr = explode(".", $file, 2);
				$num = $arr[0];
				//echo "<br/>filenum: ".$num;		
				if($num == $ecommerceprocessebayfileid){
					$found = 1;
					break;					
				}
			}
			if($found == 0){
				$today = date('M-d-Y', strtotime("- 1 days"));
				$nlist = json_encode($sftp->nlist('/store/classification/output/'.$today)); 
				//echo "<br/>nlist: ".$nlist;
				$nlist = str_replace("[", "", $nlist);
				$nlist = str_replace("]", "", $nlist);
				$nlist = str_replace('"', "", $nlist);
				$nlist = explode(",", $nlist);
				foreach($nlist as $file){
					//echo "<br/>file: ".$file;	
					$arr = explode(".", $file, 2);
					$num = $arr[0];
					//echo "<br/>filenum: ".$num;		
					if($num == $ecommerceprocessebayfileid){
						$found = 1;
						break;					
					}
				}		
			}
			if($found == 1){
				$response = $sftp->get('/store/classification/output/'.$today.'/'.$file);
			}
		}
		
		//echo "<br/>response: ".$response."<br/>";
		$response = str_replace("\t", "", $response);
		$response1 = explode(PHP_EOL, $response);
		//echo "<br/>response1: ".json_encode($response1)."<br/>";
		$response = array();
		foreach($response1 as $response2){
			$response2 = explode(',', $response2);
			array_push($response, $response2);
		}
		echo "<br/>response: ".json_encode($response)."<br/>";
		
		//process the result
		$i = 1;
		foreach($response as $category){
			if($i >= 2){
					
				$sku	= $category[0];
				if($sku <> ''){
					$sku	= $category[0];
					if(strpos($sku, 'PSI') !== false) {
						$sku = str_replace("PSI", "", $sku);
						
						$nosqlqueries = $nosqlqueries + 1;
						$sqlstarttime = microtime(true);
						$getproductid = mysql_query("select productid from productstockitem where productstockitemid = $sku");
						$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
						$differencemilliseconds = microtime(true) - $sqlstarttime;
						echo "<br/>Get productid: ";
						echo "<br/>differencemilliseconds: ".$differencemilliseconds;
						echo "<br/>sqlqueriestime: ".$sqlqueriestime;
						$getproductidrow = mysql_fetch_array($getproductid);
						$sku = $getproductidrow['productid'];
					}
					
					$sku = str_replace("P", "", $sku);
					$ebaycategoryid	= $category[4];
					$ebayfullcategoryresponse = mysql_real_escape_string(json_encode($category));
					echo "<br/><br/>ebayfullcategoryresponse: ".$ebayfullcategoryresponse;	
					echo "<br/>ebaycategoryid: ".$ebaycategoryid;
					echo "<br/>sku: ".$sku;
					
					$nosqlqueries = $nosqlqueries + 1;
					$sqlstarttime = microtime(true);
					$updatecategory = mysql_query("update ecommerceproductcategory set ebaycategoryid = $ebaycategoryid, 
					ebayfullcategoryresponse = '$ebayfullcategoryresponse', 
					lastcheckedebaycategoryid = '$datetime'
					where productid = $sku");
					$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
					$differencemilliseconds = microtime(true) - $sqlstarttime;
					echo "<br/>Get list of ebay config records: ";
					echo "<br/>differencemilliseconds: ".$differencemilliseconds;
					echo "<br/>sqlqueriestime: ".$sqlqueriestime;
					
				}
			}
			$i = $i + 1;
		}
		
		//mark the ecommerceprocesamazonfile processing as complete
		$xml = mysql_real_escape_string(json_encode($response));
		$xml = substr($xml,0,10000);
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$updatefile = mysql_query("update ecommerceprocessebayfile 
		set status = 'Processed', returnedxml = '$xml' 
		where ecommerceprocessebayfileid = '$ecommerceprocessebayfileid'");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Update ecommerceprocessebayfile as completed: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		
	}
}
else {
	echo "<br/><br/><b>PROCESS NEW EBAY CATEGORY SEARCH</b>";
	//create products that have listing locations
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getsiteconfig = mysql_query("select * from $database.ecommercesiteconfig
	where ecommercesiteid = 2 
	order by rand()
	limit 1");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get list of ebay config records: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($siteconfig = mysql_fetch_array($getsiteconfig)){
		$ecommercesiteconfigid = $siteconfig['ecommercesiteconfigid'];
		$ftp_user_name = $siteconfig['apikey1'];
		$ftp_user_pass = $siteconfig['apikey2'];
		$ftp_server = $siteconfig['datacentre'];
		$channelid = $siteconfig['apikey3'];		
		$shippingpolicy = $siteconfig['apikey4'];
		$paymentpolicy = $siteconfig['apikey5'];
		$returnpolicy = $siteconfig['apikey6'];
		$productindividual = $siteconfig['productindividual'];
		
		//create products that have listing locations
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$getlistinglocations = "select ecommercelistinglocation.productid, amazonproductxml from ecommercelistinglocation
		inner join ecommerceproductcategory on ecommerceproductcategory.productid = ecommercelistinglocation.productid
		where ebaycategoryid = '' and ecommercesiteconfigid = $ecommercesiteconfigid and amazonproductxml <> ''
		group by ecommercelistinglocation.productid
		limit 1000";
		echo "<br/>".$getlistinglocations;
		$getlistinglocations = mysql_query($getlistinglocations);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Get list of ecommercelistinglocation records that require processing: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		$i = 1;
		if(mysql_num_rows($getlistinglocations) >= 1){
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$createebayfile = mysql_query("insert into ecommerceprocessebayfile (status, filetype, ecommercesiteconfigid,
			disabled, datecreated, masteronly) values ('Get Category Data', 
			'Category Data', '$ecommercesiteconfigid', '0', '$date', '0')");
			$ebayfileid = mysql_insert_id();
			echo "<br/>ebayfileid: ".$ebayfileid;
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Insert a record into ecommerceprocessebayfile: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			
			$feed = '<?xml version="1.0" encoding="UTF-8"?>
<productRequest>';
			while($row92=mysql_fetch_array($getlistinglocations)){
				$productid = $row92['productid'];
				$ebayproductxml = $row92['amazonproductxml'];		
				//$ebayproductxml = str_replace("</product>", "", $ebayproductxml);	
				$ebayproductxml = lreplace("</product>","",$ebayproductxml);
				$feed = $feed.'
	'.$ebayproductxml;
				$feed = $feed.'
		<distribution localizedFor="en_US">
			<channelDetails>
				<channelID>'.$channelid.'</channelID>
			</channelDetails>
      </distribution>
   </product>';
			}
			$feed = $feed."
</productRequest>";
			echo "<br/><br/>".nl2br(htmlentities($feed));
			
			$remote_file = '/store/classification/'.$ebayfileid.".xml";
			echo "<br/>remote_file: ".$remote_file;
			$local_file = fopen('php://temp', 'r+');
			fwrite($local_file, $feed);
			rewind($local_file);       
			$sftp = new Net_SFTP($ftp_server);
			if (!$sftp->login($ftp_user_name, $ftp_user_pass)) {
			    exit('Login Failed');
			}
			else {
				echo "<br/><br/>Successful SFTP login<br/><br/>";	
				
				$sftp->put($remote_file, $local_file, NET_SFTP_LOCAL_FILE);		
				echo "<br/><br/>File Uploaded<br/><br/>";	
			}

			
			echo "<br/><br/>Process the insert classification feed: ";
			$ecommerceprocessebayfilename = "Classification Feed - ".$datetime;
			
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updateebayfileid ="update ecommerceprocessebayfile 
			set ecommerceprocessebayfilename = '$ecommerceprocessebayfilename', 
			submissiondatetime = '$datetime'
			where ecommerceprocessebayfileid = '$ebayfileid'";
			echo "<br/><br/>updateebayfileid: ".$updateebayfileid;
			$updateebayfileid = mysql_query($updateebayfileid);
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Insert the ebay id of the file being processed: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			
		}
	}
}

echo "<br/><br/>";

function lreplace($search, $replace, $subject){
    $pos = strrpos($subject, $search);
    if($pos !== false){
        $subject = substr_replace($subject, $replace, $pos, strlen($search));
    }
    return $subject;
}

?>