<?php

$submit = isset($_POST['submit']) ? $_POST['submit'] : ''; 
if($submit) {
	
    $filename = $_FILES['csv']['tmp_name'];
    ini_set('auto_detect_line_endings',TRUE);
    //Open the old file for reading
	$file = fopen($filename,"r"); 
    $newFile = fopen("newfilehandle.csv", "w"); //Create a new file for writing
    
    while (($data = fgetcsv($file)) !== FALSE) {
        
        array_push($row, $data[0]);
        array_push($row, $data[1]);
        array_push($row, $data[2]);
        array_push($row, $data[3]);
        array_push($row, $data[4]);
        if($data[5] == '' && $data[6] == '')
        {
            // echo "<br/>".$data[4]." ";
            
            // Scan the website
            $the_url = htmlspecialchars($data[4]);    
            $text = file_get_contents($the_url);

            $dom = new DOMDocument;
            @$dom->loadHTML($text);

            $classname="wiwebsite";

            $xpath = new DOMXpath($dom);

            /**
             * As the website have different segments using unique class names, we can fetch
             * the required data by using class names.
             * 
             * Also the class for fetching website value is inside the ul tag, so the code for $extralinks
             * can be changed a bit to avoid using $links but still 2 query calls need to be made, so
             * its better to have 1 direct call & 1 indirect call.
             */

            // Fetch the node value (for website) having class name = 'wiwebsite' (direct call)
            $links = $xpath->query("//*[contains(@class, '$classname')]");
            // Fetch the required p tag (for Postcode) inside li which is inside ul tag with class name = 'wi_details' (indirect call)
            $extralinks = $xpath->query("//ul[@class='wi_details']/li[2]/p");
            
            // Used for reference --> $xpath->query(".//ul[@class='pages']/li[last()]/a/text()")->item(0)->nodeValue;
            
            foreach($extralinks as $link)
            {
                //echo $link->textContent;
                array_push($row, $link->nodeValue);
            }
        
            foreach ($links as $link)
            {
                //echo $link->nodeValue;
                array_push($row, $link->nodeValue);
            }
        }
        fputcsv($newFile, array_values($row)); //write data into new file
    }
    
    fclose($file);
    fclose($newFile);
}
?>

<form action="" method="post" enctype="multipart/form-data" name="form1" id="form1"> 
	<b>Upload File</b><br/><br/> 
	<input name="csv" type="file" id="csv" /> 
	<br/>
	<input class="button-primary" type="submit" name="submit" value="Submit" /> 
</form> 	