<?php session_start(); ?>
<?php include('config.php'); ?>
<?php include('sessionconfig.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<meta http-equiv="Content-Security-Policy" content="
 		upgrade-insecure-requests;
    	default-src 'self' ajax.googleapis.com maxcdn.bootstrapcdn.com cdn.datatables.net; 
    	object-src; base-uri 'self'; 
    	form-action 'self'; 
    	img-src 'self'; 
    	script-src 'unsafe-inline' 'self' cdn.datatables.net http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js;
    	font-src 'self' maxcdn.bootstrapcdn.com cdnjs.cloudflare.com;
    	style-src 'unsafe-inline' 'self' maxcdn.bootstrapcdn.com cdnjs.cloudflare.com cdn.datatables.net">
 	<title>OneBusiness</title>
	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">   
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
	<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<link rel='stylesheet' type='text/css' href='style.php' />
	<script type="text/javascript" src="jquery-autocomplete-master/src/jquery.autocomplete.js"></script>
	<link rel="stylesheet" type="text/css" href="jquery-autocomplete-master/src/jquery.autocomplete.css">
	
</head>
  
<body class='body'>
	<?php include_once ('headerthree.php');	?>
	
	
	<?php
	
	//LANGUAGE COLLECTION SECTION
	$mysqli = new mysqli($server, $user_name, $password, $database);
	$languageid = mysqli_real_escape_string($mysqli, $_SESSION['languageid']);
	if($stmt = $mysqli->prepare("SELECT languagerecordid, languagerecordtextname FROM $masterdatabase.languagerecordtext 
	where languageid = ? and languagerecordid in (6,19,27,28,29,30,31,64)")){
		$stmt->bind_param('i', $languageid);
		$stmt->execute();
		$result = $stmt->get_result();
		while ($row = $result->fetch_assoc()){
			$langid = $row['languagerecordid'];
			${"langval$langid"} = $row['languagerecordtextname'];
			//echo "<br/>".$langid." -".${"langval$langid"};
		}
	}
	else {
		//echo $mysqli->error;
	}
	$stmt->close();
	
	
		$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
	   	$pagetype = isset($_GET['pagetype']) ? strip_tags($_GET['pagetype']) : '';
	   	$stringconstruct = isset($_POST['stringconstruct']) ? strip_tags($_POST['stringconstruct']) : '';
	   	
	   	$mysqli = new mysqli($server, $user_name, $password, $database);
		$pagetype = mysqli_real_escape_string($mysqli, $pagetype);
		if($stmt = $mysqli->prepare("select * from structurefunction 
	   	inner join structurefunctionpage on structurefunction.structurefunctionid = structurefunctionpage.structurefunctionid
		where tablename = ?")){
			$stmt->bind_param('s', $pagetype);
			$stmt->execute();
			$result = $stmt->get_result();
			while($structurefunctionrow = $result->fetch_assoc()){
				$functionname = $structurefunctionrow['structurefunctionname'];
			   	$runscriptadd = $structurefunctionrow['runscriptadd'];
			   	$structurefunctionid = $structurefunctionrow['structurefunctionid'];	   	
				$structurefunctionsectionid = $structurefunctionrow['structurefunctionsectionid'];	   	
				$addhelptext = $structurefunctionrow['addhelptext'];	
			}
		}
		else {
			//echo $mysqli->error;
		}
		$stmt->close();
		
	   	//translate help text
		$mysqli = new mysqli($server, $user_name, $password, $database);
		$structurefunctionid = mysqli_real_escape_string($mysqli, $structurefunctionid);
		$languageid = mysqli_real_escape_string($mysqli, $_SESSION['languageid']);
		if($stmt = $mysqli->prepare("select * from structurefunctionpagelanguagerecordtext 
		where structurefunctionid = ? and languageid = ?")){
			$stmt->bind_param('ii', $structurefunctionid, $languageid);
			$stmt->execute();
			$result = $stmt->get_result();
			if($result->num_rows > 0){
				while($gettranslationrow = $result->fetch_assoc()){
					$addhelptext = $gettranslationrow['addhelptext'];
				}
			}
		}
		else {
			//echo $mysqli->error;
		}
		$stmt->close();
		
		
		//check permissions to page
		//check permissions to section
		$mysqli = new mysqli($server, $user_name, $password, $database);
		$structurefunctionsectionid = mysqli_real_escape_string($mysqli, $structurefunctionsectionid);
		$userid = mysqli_real_escape_string($mysqli, $userid);
		if($stmt = $mysqli->prepare("select * from userrole 
		inner join permissionrolestructurefunctionsection on permissionrolestructurefunctionsection.permissionroleid = userrole.permissionroleid
		where permissionrolestructurefunctionsection.disabled = '0' and 
		userid = ? and structurefunctionsectionid = ?")){
			$stmt->bind_param('ii', $userid, $structurefunctionsectionid);
			$stmt->execute();
			$result = $stmt->get_result();
			$getpermissionrows = $result->num_rows;
		}
		else {
			//echo $mysqli->error;
		}
		$stmt->close();
		$mysqli = new mysqli($server, $user_name, $password, $database);
		$structurefunctionid = mysqli_real_escape_string($mysqli, $structurefunctionid);
		$userid = mysqli_real_escape_string($mysqli, $userid);
		if($stmt = $mysqli->prepare("select * from userrole 
		inner join permissionrolestructurefunctionpage on permissionrolestructurefunctionpage.permissionroleid = userrole.permissionroleid
		inner join structurefunctionpage on 
		structurefunctionpage.structurefunctionpageid = permissionrolestructurefunctionpage.structurefunctionpageid
		where permissionrolestructurefunctionpage.disabled = '0' and userid = ? and structurefunctionid = ?")){
			$stmt->bind_param('ii', $userid, $structurefunctionid);
			$stmt->execute();
			$result = $stmt->get_result();
			$getpermissionrows2 = $result->num_rows;
		}
		else {
			//echo $mysqli->error;
		}
		$stmt->close();
		if($getpermissionrows > 0 || $getpermissionrows2 > 0) {			
		}
		else {
			echo $langval27;
			die;	
		}	
		
	   
	   	if(isset($_POST['submit'])) {
				for($i = 0; $i <= 100; $i++) {
     				${"structurefieldname$i"} = "";
				}	   		
	   			for($i = 0; $i <= 100; $i++) {
     				${"data$i"} = "";
				}
	   			for($i = 0; $i <= 100; $i++) {
     				${"displayname$i"} = "";
				}
				for($i = 0; $i <= 100; $i++) {
     				${"requiredfield$i"} = "";
				}
				for($i = 0; $i <= 100; $i++) {
  					${"structurefieldtypeid$i"} = "";
				}
				for($i = 0; $i <= 100; $i++) {
	  				${"textlimit$i"} = "";
				}
				for($i = 0; $i <= 100; $i++) {
	  				${"tableconnect$i"} = "";
				}
				for($i = 0; $i <= 100; $i++) {
	  				${"tablefieldconnectparent$i"} = "";
				}
				for($i = 0; $i <= 100; $i++) {
	  				${"tablefieldconnect$i"} = "";
				}
				for($i = 0; $i <= 100; $i++) {
	  				${"helptext$i"} = "";
				}
	   			   		
	   		echo "<table>";
	   		$number10 = 2;
		    	foreach ($_POST as $key => $value) {
		    		//echo $number10;
		    		if($value <> "Submit"  && $key <> "pagename") {
				      	${"structurefieldname$number10"} = $key;
				      	${"data$number10"} = $value;
				        
						$mysqli = new mysqli($server, $user_name, $password, $database);
						$structurefunctionid = mysqli_real_escape_string($mysqli, $structurefunctionid);
						if($stmt = $mysqli->prepare("select * from $database.structurefield 
			        	inner join $masterdatabase.structurefieldtype on structurefield.structurefieldtypeid = structurefieldtype.structurefieldtypeid
					  	where structurefield.structurefunctionid = ? 
					  	and structurefieldname = ?")){
							$stmt->bind_param('is', $structurefunctionid, ${"structurefieldname$number10"});
							$stmt->execute();
							$result = $stmt->get_result();
							if($result->num_rows > 0){
								while($getdatarow = $result->fetch_assoc()){
									${"displayname$number10"} = $getdatarow['displayname'];
						        	${"requiredfield$number10"} = $getdatarow['requiredfield'];
						        	${"structurefieldtypeid$number10"} = $getdatarow['structurefieldtypeid'];
						        	//$tableconnect2 = substr($getdatarow['tableconnect'], strpos($getdatarow['tableconnect'], ".")+1);
							   		${'tableconnect'.$number10} = $getdatarow['tableconnect'];
							   		//echo ${'tableconnect'.$number10}."<br/>";
							   		${"textlimit$number10"} = $getdatarow['textlimit'];
							   		${"tablefieldconnect$number10"} = $getdatarow['tablefieldconnect'];
							   		${"tablefieldconnectparent$number10"} = $getdatarow['tablefieldconnectparent'];
							   		${"helptext$number10"} = $getdatarow['helptext'];
						        	//echo ${"fieldname$number10"}." " .${"structurefieldtypeid$number10"}."<br/>";
						        	//echo "<tr><td>".${"fieldname$number10"}."</td><td>".${"data$number10"}."</td></tr>";
						        	$structurefieldtypename = $getdatarow['fieldtypename'];
								}
							}
						}
						else {
							//echo $mysqli->error;
						}
						$stmt->close();			        
			        		      	
						$number10 = $number10+1;
			    	}
		    	}
				echo "</table>";
				
		}
	   	else {
	   		$pagename = isset($_GET['pagename']) ? $_GET['pagename'] : '';
				for($i = 0; $i <= 100; $i++) {
     				${"structurefieldname$i"} = "";
				}	   		
	   			for($i = 0; $i <= 100; $i++) {
     				${"data$i"} = "";
				}
	   			for($i = 0; $i <= 100; $i++) {
     				${"displayname$i"} = "";
				}
		   		for($i = 0; $i <= 100; $i++) {
	  				${"structurefieldtypeid$i"} = "";
				}
				for($i = 0; $i <= 100; $i++) {
	  				${"requiredfield$i"} = "";
				}
				for($i = 0; $i <= 100; $i++) {
	  				${"tableconnect$i"} = "";
				}
				for($i = 0; $i <= 100; $i++) {
	  				${"textlimit$i"} = "";
				}
				for($i = 0; $i <= 100; $i++) {
	  				${"tablefieldconnect$i"} = "";
				}
				for($i = 0; $i <= 100; $i++) {
	  				${"tablefieldconnectparent$i"} = "";
				}
				for($i = 0; $i <= 100; $i++) {
	  				${"helptext$i"} = "";
				}
			
				//get fields
				$fieldid = 1;
				if($database == $masterdatabase){
					$mysqli = new mysqli($server, $user_name, $password, $database);
					$pagetype = mysqli_real_escape_string($mysqli, $pagetype);
					if($stmt = $mysqli->prepare("select * from structurefield 
					inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
					where structurefunction.tablename = ? and structurefield.disabled = 0 and fieldposition > 0
					order by structurefield.fieldposition")){
						$stmt->bind_param('s', $pagetype);
						$stmt->execute();
						$result = $stmt->get_result();
						if($result->num_rows > 0){
							while($fieldrow = $result->fetch_assoc()){
								$primarykeyfield = $fieldrow['primarykeyfield'];
								${'structurefieldname'.$fieldid} = $fieldrow['structurefieldname'];
								${'structurefieldtypeid'.$fieldid} = $fieldrow['structurefieldtypeid'];
								${'displayname'.$fieldid} = $fieldrow['displayname'];
								${'requiredfield'.$fieldid} = $fieldrow['requiredfield'];
								${'textlimit'.$fieldid} = $fieldrow['textlimit'];
								//$tableconnect2 = substr($fieldrow['tableconnect'], strpos($fieldrow['tableconnect'], ".")+1);
							   	//${'tableconnect'.$fieldid} = $tableconnect2;
							   	${'tableconnect'.$fieldid} = $fieldrow['tableconnect'];
							   //	echo $fieldid;
								//echo ${'tableconnect'.$fieldid}."<br/>"; 					
								${'tablefieldconnect'.$fieldid} = $fieldrow['tablefieldconnect'];
								${'tablefieldconnectparent'.$fieldid} = $fieldrow['tablefieldconnectparent'];
								${'helptext'.$fieldid} = $fieldrow['helptext'];
								//echo ${'fieldname'.$fieldid}."<br/>";
								$fieldid = $fieldid + 1;
							}
						}
					}
					else {
						//echo $mysqli->error;
					}
					$stmt->close();
				}
				else {
					$mysqli = new mysqli($server, $user_name, $password, $database);
					$pagetype = mysqli_real_escape_string($mysqli, $pagetype);
					if($stmt = $mysqli->prepare("select * from structurefield 
					inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
					where structurefunction.tablename = ? and structurefield.disabled = 0
					and fieldposition > 0 and structurefield.masteronly = 0
					order by structurefield.fieldposition")){
						$stmt->bind_param('s', $pagetype);
						$stmt->execute();
						$result = $stmt->get_result();
						if($result->num_rows > 0){
							while($fieldrow = $result->fetch_assoc()){
								$primarykeyfield = $fieldrow['primarykeyfield'];
								${'structurefieldname'.$fieldid} = $fieldrow['structurefieldname'];
								${'structurefieldtypeid'.$fieldid} = $fieldrow['structurefieldtypeid'];
								${'displayname'.$fieldid} = $fieldrow['displayname'];
								${'requiredfield'.$fieldid} = $fieldrow['requiredfield'];
								${'textlimit'.$fieldid} = $fieldrow['textlimit'];
								//$tableconnect2 = substr($fieldrow['tableconnect'], strpos($fieldrow['tableconnect'], ".")+1);
							   	//${'tableconnect'.$fieldid} = $tableconnect2;
							   	${'tableconnect'.$fieldid} = $fieldrow['tableconnect'];
							   //	echo $fieldid;
								//echo ${'tableconnect'.$fieldid}."<br/>"; 					
								${'tablefieldconnect'.$fieldid} = $fieldrow['tablefieldconnect'];
								${'tablefieldconnectparent'.$fieldid} = $fieldrow['tablefieldconnectparent'];
								${'helptext'.$fieldid} = $fieldrow['helptext'];
								//echo ${'fieldname'.$fieldid}."<br/>";
								$fieldid = $fieldid + 1;
							}
						}
					}
					else {
						//echo $mysqli->error;
					}
					$stmt->close();
				}
				
				$countoffields = $fieldid - 1;
				//echo $countoffields;
				$stringconstruct = 1;
				$fieldstring = "";
				while($stringconstruct<= $countoffields) {
					$fieldstring = 	$fieldstring.${'structurefieldname'.$stringconstruct};		
					if($stringconstruct <> $countoffields){
						$fieldstring = $fieldstring.", ";						
					}	
					$stringconstruct = $stringconstruct + 1;
				}	
			   
	   }
	   
		?>
	<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10" style="padding:10px;">
		<!--search results header bar for desktop-->
		<div class='bodyheader'>
			<h3><?php echo $langval6 ?> <?php echo $functionname ?></h3>
		</div>
			<?php
				   	
			if(isset($_POST['submit'])) {
				$pagename = isset($_POST['pagename']) ? $_POST['pagename'] : '';
   				
				if($submit) {
					$stringconstruct = 1;
					$countoffields = $number10-1;
					$fieldstring = "";
					while($stringconstruct<= $countoffields) {
						$fieldstring = 	$fieldstring.${'structurefieldname'.$stringconstruct};		
						if($stringconstruct <> $countoffields){
							$fieldstring = $fieldstring.", ";						
						}	
						$stringconstruct = $stringconstruct + 1;
					}			
					
					//echo $fieldstring;
					
					//run validation		
					$validate = "";
					$number15 = 2;
					while($number15 <= $countoffields) {
						$mysqli = new mysqli($server, $user_name, $password, $database);
						$structurefunctionid = mysqli_real_escape_string($mysqli, $structurefunctionid);
						${"structurefieldname$number15"} = mysqli_real_escape_string($mysqli, ${"structurefieldname$number15"});
						if($stmt = $mysqli->prepare("select * from $database.structurefield 
						left join $masterdatabase.structurefieldtype on structurefieldtype.structurefieldtypeid = structurefield.structurefieldtypeid						
						where structurefieldname = ? and structurefunctionid = ?")){
							$stmt->bind_param('si', ${"structurefieldname$number15"}, $structurefunctionid);
							$stmt->execute();
							$result = $stmt->get_result();
							if($result->num_rows > 0){
								while($validationrow = $result->fetch_assoc()){
									$requiredfield = $validationrow['requiredfield'];
									$textlimit = $validationrow['textlimit'];
									$minvalue = $validationrow['lowvalue'];
									$maxvalue = $validationrow['highvalue'];
									$structurefieldname = $validationrow['structurefieldname'];
									$structurefieldtypename = $validationrow['fieldtypename'];
									$unique = $validationrow['uniqueval'];
								}
							}
						}
						else {
							echo $mysqli->error;
						}
						$stmt->close();
						
						//echo ${"data$number15"};
						//validation - requiredfield
						if($requiredfield == 1) {
							if(${"data$number15"} == "") {
								$validate .= ${"displayname$number15"}." ".$langval30." "; 	
					 		}
				 		}
				 	
				 		//validation - textlimit
						if(strlen(${"data$number15"}) > $textlimit && $textlimit <> 0) {
							$validate .= ${"displayname$number15"}." ".$langval28." ".$textlimit." ".$langval29." "; 	
				 		}
				 		
				 		//validation - uniquecheck
				 		if($unique == 1){
					 		$mysqli = new mysqli($server, $user_name, $password, $database);
							$pagetype = mysqli_real_escape_string($mysqli, $pagetype);
							$structurefieldname = mysqli_real_escape_string($mysqli, $structurefieldname);
							${"data$number15"} = mysqli_real_escape_string($mysqli, ${"data$number15"});
							if($stmt = $mysqli->prepare("select * from $pagetype where ? = ?")){
								$stmt->bind_param('ss', $structurefieldname, ${"data$number15"});
								$stmt->execute();
								$result = $stmt->get_result();
								$checkuniquerow = $result->num_rows;
								if($checkuniquerow >= 1 && $unique == 1) {
									$validate .= ${"displayname$number15"}." ".$langval64." "; 	
					 			}	
					 								
							}
							else {
								echo $mysqli->error;
							}
							$stmt->close();		
						}
										
						
						//validation - minvalue
						if(${"data$number15"} < $minvalue && $minvalue <> 0) {
							$validate .= ${"displayname$number15"}." ".$langval31." ".$minvalue." "; 	
				 		}
				 	
				 		//validation - maxvalue
						if(${"data$number15"} > $maxvalue && $maxvalue <> 0) {
							$validate .= ${"displayname$number15"}." ".$langval28." ".$maxvalue." . "; 	
				 		}
				 	
						//echo ${"data$number15"};
						
						//convert to just number for table connect fields
						if($structurefieldtypename == 'Table Connect' && ${"data$number15"} <> ''){
							if(!is_numeric(${"data$number15"})){
								preg_match('#\((.*?)\)#', ${"data$number15"}, $match);
								${"data$number15"} = $match[1];	
							}	
						}
						
						
						
						$number15 = $number15+1;
					}
						
					
					//run update if validated
					if($validate == "") {
						$datecreated = date("Y-m-d");						
						$fieldlist = "datecreated, ";
						$questionmark = "";
						$fieldtypelist = "";
						$number11 = 2;
						$datatrackingarray = array();
						while($number11 <= $countoffields) {
							${"structurefieldname$number11"} = mysql_real_escape_string(${"structurefieldname$number11"});							
							if($number11 == $countoffields) {
								$fieldlist .=${"structurefieldname$number11"};	
								$questionmark .="?";						
							}
							else {
								$fieldlist .=${"structurefieldname$number11"}.", ";
								$questionmark .="?, ";
							}
							
							//override values for customer table to encrypt the data for certain values
							$salt = "h3f8s9en20vj3";
							if($functionname == "Customer"){
								if(${"structurefieldname$number11"} == "customername" || ${"structurefieldname$number11"} == "firstname" || 
								${"structurefieldname$number11"} == "lastname" || ${"structurefieldname$number11"} == "address1" || 
								${"structurefieldname$number11"} == "address2" || ${"structurefieldname$number11"} == "address3" || 
								${"structurefieldname$number11"} == "towncity" || ${"structurefieldname$number11"} == "stateregion" || 
								${"structurefieldname$number11"} == "postzipcode" || ${"structurefieldname$number11"} == "phonenumber" || 
								${"structurefieldname$number11"} == "mobilenumber" || ${"structurefieldname$number11"} == "faxnumber" || 
								${"structurefieldname$number11"} == "emailaddress" || ${"structurefieldname$number11"} == "salutation"){
									${"data$number11"} = openssl_encrypt(${"data$number11"},"AES-128-ECB",$salt);
									//echo "<br/>data: ".${"data$number11"};
								}
							
							}
						
							array_push($datatrackingarray, array(${"structurefieldname$number11"} => ${"data$number11"}));
							
							if(${"structurefieldtypeid$number11"} == 1 || ${"structurefieldtypeid$number11"} == 3
							|| ${"structurefieldtypeid$number11"} == 8){
								$fieldtypelist .= 's';							
							}
							if(${"structurefieldtypeid$number11"} == 4 || ${"structurefieldtypeid$number11"} == 7 ||
							${"structurefieldtypeid$number11"} == 5 || ${"structurefieldtypeid$number11"} == 6){
								$fieldtypelist .= 'i';							
							}
							if(${"structurefieldtypeid$number11"} == 2){
								$fieldtypelist .= 'd';							
							}
							$number11 = $number11 + 1;					
						}
						
						//echo "<br/><br/>datatrackingarray: ".json_encode($datatrackingarray);
						
						
						//echo "<br/>pagetype: ".$pagetype;
						//echo "<br/>fieldlist: ".$fieldlist;
						//echo "<br/>fieldtypelist: ".$fieldtypelist;
						//echo "<br/>questionmark: ".$questionmark;
						//echo "<br/>countoffields: ".$countoffields;
							
						$mysqli = new mysqli($server, $user_name, $password, $database);	
						if($stmt = $mysqli->prepare("INSERT INTO $pagetype ($fieldlist) VALUES ('$datecreated', $questionmark)")){
							if($countoffields == 2){
								$stmt->bind_param($fieldtypelist, $data2);
							}
							if($countoffields == 3){
								$stmt->bind_param($fieldtypelist, $data2, $data3);
							}
							if($countoffields == 4){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4);
							}
							if($countoffields == 5){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5);
							}
							if($countoffields == 6){						
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6);
							}
							if($countoffields == 7){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7);
							}
							if($countoffields == 8){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8);
							}
							if($countoffields == 9){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9);
							}
							if($countoffields == 10){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10);
							}
							if($countoffields == 11){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11);
							}
							if($countoffields == 12){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12);
							}
							if($countoffields == 13){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13);
							}
							if($countoffields == 14){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14);
							}
							if($countoffields == 15){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15);
							}
							if($countoffields == 16){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16);
							}
							if($countoffields == 17){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17);
							}
							if($countoffields == 18){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18);
							}
							if($countoffields == 19){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19);
							}
							if($countoffields == 20){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20);
							}
							if($countoffields == 21){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21);
							}
							if($countoffields == 22){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22);
							}
							if($countoffields == 23){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23);
							}
							if($countoffields == 24){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24);
							}
							if($countoffields == 25){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25);
							}
							if($countoffields == 26){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26);
							}
							if($countoffields == 27){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27);
							}
							if($countoffields == 28){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28);
							}
							if($countoffields == 29){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29);
							}
							if($countoffields == 30){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30);
							}
							if($countoffields == 31){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30, $data31);
							}
							if($countoffields == 32){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30, $data31,
								$data32);
							}
							if($countoffields == 33){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30, $data31,
								$data32, $data33);
							}
							if($countoffields == 34){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30, $data31,
								$data32, $data33, $data34);
							}
							if($countoffields == 35){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30, $data31,
								$data32, $data33, $data34, $data35);
							}
							if($countoffields == 36){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30, $data31,
								$data32, $data33, $data34, $data35, $data36);
							}
							if($countoffields == 37){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30, $data31,
								$data32, $data33, $data34, $data35, $data36, $data37);
							}
							if($countoffields == 38){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30, $data31,
								$data32, $data33, $data34, $data35, $data36, $data37, $data38);
							}
							if($countoffields == 39){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30, $data31,
								$data32, $data33, $data34, $data35, $data36, $data37, $data38, $data39);
							}
							if($countoffields == 40){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30, $data31,
								$data32, $data33, $data34, $data35, $data36, $data37, $data38, $data39, $data40);
							}
							if($countoffields == 41){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30, $data31,
								$data32, $data33, $data34, $data35, $data36, $data37, $data38, $data39, $data40, $data41);
							}
							if($countoffields == 42){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30, $data31,
								$data32, $data33, $data34, $data35, $data36, $data37, $data38, $data39, $data40, $data41,
								$data42);
							}
							if($countoffields == 43){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30, $data31,
								$data32, $data33, $data34, $data35, $data36, $data37, $data38, $data39, $data40, $data41,
								$data42, $data43);
							}
							if($countoffields == 44){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30, $data31,
								$data32, $data33, $data34, $data35, $data36, $data37, $data38, $data39, $data40, $data41,
								$data42, $data43, $data44);
							}
							if($countoffields == 45){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30, $data31,
								$data32, $data33, $data34, $data35, $data36, $data37, $data38, $data39, $data40, $data41,
								$data42, $data43, $data44, $data45);
							}
							if($countoffields == 46){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30, $data31,
								$data32, $data33, $data34, $data35, $data36, $data37, $data38, $data39, $data40, $data41,
								$data42, $data43, $data44, $data45, $data46);
							}
							if($countoffields == 47){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30, $data31,
								$data32, $data33, $data34, $data35, $data36, $data37, $data38, $data39, $data40, $data41,
								$data42, $data43, $data44, $data45, $data46, $data47);
							}
							if($countoffields == 48){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30, $data31,
								$data32, $data33, $data34, $data35, $data36, $data37, $data38, $data39, $data40, $data41,
								$data42, $data43, $data44, $data45, $data46, $data47, $data48);
							}
							if($countoffields == 49){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30, $data31,
								$data32, $data33, $data34, $data35, $data36, $data37, $data38, $data39, $data40, $data41,
								$data42, $data43, $data44, $data45, $data46, $data47, $data48, $data49);
							}
							if($countoffields == 50){
								$stmt->bind_param($fieldtypelist, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11,
								$data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20, $data21,
								$data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30, $data31,
								$data32, $data33, $data34, $data35, $data36, $data37, $data38, $data39, $data40, $data41,
								$data42, $data43, $data44, $data45, $data46, $data47, $data48, $data49, $data50);
							}
							$stmt->execute();
							$stmt->close();
						}
						$recordid = $mysqli->insert_id;
						
						$mysqli->close();
						
						//add datatracking
						$date = date("Y-m-d");
						$datatracking = json_encode($datatrackingarray);
						//echo "<br/><br/>datatracking: ".$datatracking;
						//echo "<br/>structurefunctionid: ".$structurefunctionid;
						//echo "<br/>recordid: ".$recordid;
						//echo "<br/>date: ".$date;
						//echo "<br/>userid: ".$userid;
						$mysqli = new mysqli($server, $user_name, $password, $database);
						if($stmt = $mysqli->prepare("insert into trackchanges (trackchangesname, structurefunctionid, rowid, 
						userid, changes, type, subtype, disabled, datecreated, masteronly) 
						values ('Add New Record', ?, ?, ?, ?, 'Add', 'New', 0, ?, 0)")){
							$stmt->bind_param('iiiss', $structurefunctionid, $recordid, $userid, 
							$datatracking, $date);
							$stmt->execute();
							$stmt->close();
						}

						//echo "<br/><br/>recordid: ".$recordid;
						
						
					   //echo $recordid;
					   if($runscriptadd <> ""){
					   		include_once('./'.$runscriptadd);
					   }					   
					   
					   
			     		//record access of this page in oneappusage
						$date = date("Y-m-d");
						$appmanagerusagename = $userid." - ".$date;
						
						$mysqli = new mysqli($server, $user_name, $password, $database);
						$structurefunctionid2 = mysqli_real_escape_string($mysqli, "%".$structurefunctionid."%");
						if($stmt = $mysqli->prepare("select * from $masterdatabase.appsetupapplication 
						where businessgridpages like ?")){
							$stmt->bind_param('s', $structurefunctionid2);
							$stmt->execute();
							$result = $stmt->get_result();
							if($result->num_rows > 0){
								while($getapprow = $result->fetch_assoc()){
									$appsetupapplicationid = $getapprow['appsetupapplicationid'];
									
									$mysqli = new mysqli($server, $user_name, $password, $database);
									$appmanagerusagename = mysqli_real_escape_string($mysqli, $appmanagerusagename);
									$appsetupapplicationid = mysqli_real_escape_string($mysqli, $appsetupapplicationid);
									$userid = mysqli_real_escape_string($mysqli, $userid);
									$date = mysqli_real_escape_string($mysqli, $date);
									$structurefunctionid = mysqli_real_escape_string($mysqli, $structurefunctionid);
									if($stmt = $mysqli->prepare("insert into appmanagerusage (appmanagerusagename, appsetupapplicationid, userid, 
									dateused, viewtype, structurefunctionid, viewid, datecreated, masteronly, disabled) 
									values (?, ?, ?, ?, 'Add Grid Item', ?, '0', ?, '0', '0')")){
										$stmt->bind_param('siisis', $appmanagerusagename, $appsetupapplicationid, $userid, $date, $structurefunctionid, $date);
										$stmt->execute();									
									}
									else {
										//echo $mysqli->error;
									}
									//$stmt->close();
								}
							}
						}
						else {
							//echo $mysqli->error;
						}				
					
					  // echo $pagename;
					   //go to homepage
	    				if($pagename == ''){
	    					$url = 'pagegrid.php?pagetype='.$pagetype;
	      					echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">'; 
	    				}
	    				else{
	    					$pagename = str_replace("xxxxxxxxxx", "&", $pagename);	
	    					$pagename = str_replace("[recordid]", $recordid, $pagename);    
	    					//echo "<br/>".$pagename;
	    					$url = $pagename;
	    					echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">'; 
	    				}
	    				
	      			}
				}			 	
			}
			?>
			<div class='bodycontent'>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<?php
					if(isset($validate) && $validate <> "") {
						echo "<p class='background-warning'>".$validate."</p>";
					} 
					?>
				<?php
				echo $addhelptext."<br/><br/>";
				?>
		
			</div>
		
					<form class="form-horizontal" action='pageadd.php?pagetype=<?php echo $pagetype?>&pagename=<?php echo $pagename?>' method="post" enctype="multipart/form-data">
						<div class="col-xs-12 col-md-7 col-sm-8 col-lg-6">
							<?php 
							
							$number3 = 1;
							while($number3 < $stringconstruct) {
								$structurefieldtypeid = ${'structurefieldtypeid'.$number3};
								$mysqli = new mysqli($server, $user_name, $password, $database);
								$structurefieldtypeid = mysqli_real_escape_string($mysqli, $structurefieldtypeid);
			        			if($stmt = $mysqli->prepare("select * from $masterdatabase.structurefieldtype 
			        			where structurefieldtypeid = ?")){
									$stmt->bind_param('i', $structurefieldtypeid);
									$stmt->execute();
									$result = $stmt->get_result();
									while ($rowcheckstructurefieldtype = $result->fetch_assoc()){
										$fieldtypename = $rowcheckstructurefieldtype['fieldtypename'];									
									}
								}
								else {
									//echo $mysqli->error;
								}
								$stmt->close();
	
			        			$label = "";
			        			
			        			$mysqli = new mysqli($server, $user_name, $password, $database);
								${'displayname'.$number3} = mysqli_real_escape_string($mysqli, ${'displayname'.$number3});
								$languageid = mysqli_real_escape_string($mysqli, $_SESSION['languageid']);
			        			if($stmt = $mysqli->prepare("select * from structurefieldlanguagerecordtext 
								where structurefieldname = ? and languageid = ?")){
									$stmt->bind_param('si', ${'displayname'.$number3}, $languageid);
									$stmt->execute();
									$result = $stmt->get_result();
									if($result->num_rows > 0){
										while ($gettranslationrow = $result->fetch_assoc()){
											$fieldnametranslated = $gettranslationrow['structurefieldlanguagerecordtextname'];
											${'helptext'.$number3} = $gettranslationrow['helptext'];
											$label = $fieldnametranslated;
											if(${"requiredfield$number3"} == 1){
												$label = $label.' *';
											}								
										}
									}
									else {
										if(${"requiredfield$number3"} == 1){
											$label = ${"displayname$number3"}.' *';
										}
										else {
											$label = ${"displayname$number3"};
										}
									}		
								}
								else {
									//echo $mysqli->error;
								}
								$stmt->close();
								
			        			if(isset($fieldtypename)){						
									if($fieldtypename == 'Text' && ${'textlimit'.$number3} <= 300) {								
									?>
									<div class="form-group">
										<label for="<?php echo $label ?>" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label"><?php echo $label ?></label>
								    	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								      	<input type="text" class="form-control" name="<?php echo ${'structurefieldname'.$number3} ?>" value="<?php echo ${'data'.$number3} ?>">
								    	<?php echo ${'helptext'.$number3};	?>	
								    	</div>
								  	</div>
									<?php	
									}
									if($fieldtypename == 'Text' && ${'textlimit'.$number3} > 300) {								
									?>
									<div class="form-group">
										<label for="<?php echo $label ?>" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label"><?php echo $label ?></label>
								    	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								    	<textarea rows="4" cols="50" class="form-control" name="<?php echo ${'structurefieldname'.$number3} ?>"><?php echo ${'data'.$number3} ?></textarea>
								      <?php echo ${'helptext'.$number3};	?>	
								    	</div>
								  	</div>
									<?php	
									}
									if($fieldtypename == 'Value' ) {								
									?>
									<div class="form-group">
										<label for="<?php echo $label ?>" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label"><?php echo $label ?></label>
								    	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								      	<input type="text" class="form-control" name="<?php echo ${'structurefieldname'.$number3} ?>" value="<?php echo ${'data'.$number3} ?>">
								    	<?php echo ${'helptext'.$number3};	?>	
								    	</div>
								  	</div>
									<?php	
									}
									if($fieldtypename == 'Checkbox') {		
										
									?>
									<div class="form-group">
										<label for="<?php echo $label ?>" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label"><?php echo $label ?></label>
								    	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								    	<input type='hidden' value='0' name='<?php echo ${'structurefieldname'.$number3} ?>'>
								    	<input type='checkbox' name='<?php echo ${'structurefieldname'.$number3} ?>' value='1' <?php if (${'data'.$number3}=='1') { ?> checked='checked' <?php ;} ?> />
								      	<br/><?php echo ${'helptext'.$number3};	?>	
								    	</div>
								  	</div>
									<?php	
									}
									if($fieldtypename == 'Date Select') {								
									?>
									<div class="form-group">
										<label for="<?php echo $label ?>" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label"><?php echo $label ?></label>
								    	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								      	<input type="date" class="form-control" name="<?php echo ${'structurefieldname'.$number3} ?>" value="<?php echo ${'data'.$number3} ?>">
								    	<?php echo ${'helptext'.$number3};	?>	
								    	</div>
								  	</div>
									<?php	
									}
									
									if($fieldtypename == 'Date Time Select') {								
									?>
									<div class="form-group">
										<label for="<?php echo $label ?>" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label"><?php echo $label ?></label>
								    	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								      	<input type="datetime-local" class="form-control" name="<?php echo ${'structurefieldname'.$number3} ?>" value="<?php echo ${'data'.$number3} ?>">
								    	<?php echo ${'helptext'.$number3};	?>	
								    	</div>
								  	</div>
									<?php	
									}
									
									if($fieldtypename == 'Table Connect') {		
										//echo "<br/>number3: ".$number3;						
									?>
									<div class="form-group">
										 	<label for="<?php echo $label ?>" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label"><?php echo $label ?></label>
									    	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
								    		  	<?php
								    		  	//echo "4: ".${'tableconnect'.$number3};
								    		  	if($masterdatabase == $defaultmasterdb){
													${'tableconnect'.$number3} = str_replace("onebusinessmaster", $defaultmasterdb, ${'tableconnect'.$number3});				
												}				     									    		  	
								    		  	if (strpos(${'tableconnect'.$number3}, $masterdatabase) !== false) {       
								    		  		$tableconnect2 = ${'tableconnect'.$number3};   
													$fieldconnect2 = substr($tableconnect2, strpos($tableconnect2, ".")+1);	
													//echo $tableconnect2." - ".$fieldconnect2;	
							            		}
												else {
													//echo "<br/>2: ".${'tableconnect'.$number3};
													$tableconnect2 = substr(${'tableconnect'.$number3}, strpos(${'tableconnect'.$number3}, ".")+1);
						        					$tableconnect2 = str_replace("##database##", $database, $tableconnect2);		
						        					$fieldconnect2 = $tableconnect2;		
						        					//echo $tableconnect2." - ".$fieldconnect2;						
												}
							        			$limit = 3000000;
							        			
							        			
												$mysqli = new mysqli($server, $user_name, $password, $database);
												$database = mysqli_real_escape_string($mysqli, $database);
							        			${'tableconnect'.$number3} = mysqli_real_escape_string($mysqli, ${'tableconnect'.$number3});
												if($stmt = $mysqli->prepare("SELECT TABLE_NAME, SUM(TABLE_ROWS) as 'numrows'
											   	FROM INFORMATION_SCHEMA.TABLES 
											  	WHERE TABLE_SCHEMA = ? and TABLE_NAME = ?
											   	group by TABLE_NAME")){
													$stmt->bind_param('si', $database, ${'tableconnect'.$number3});
													$stmt->execute();
													$result = $stmt->get_result();
													$numrows = $result->num_rows;											
												}
												else {
													//echo $mysqli->error;
												}
												$stmt->close();				
														        			
							        			
							        			
																	        			
							        			${'tablefieldconnect'.$number3} = mysqli_real_escape_string($mysqli, ${'tablefieldconnect'.$number3});
											   	if($numrows <= 1000){
													$orderclause = "order by ".${'tablefieldconnect'.$number3}." asc";									   	
											   	}
											   	else {
											   		$orderclause = "";
											   	}
											   	$idfield1 = ${'tableconnect'.$number3}.'id';
											   	$val = $defaultmasterdb.".";
											   	$idfield1 = str_replace($val, "", $idfield1);
											   	$idfield1 = str_replace("onebusinessmaster.", "", $idfield1);
								    		  	${'tableconnect'.$number3} = $tableconnect2;
							        			//echo ${'tableconnect'.$number3}."<br/>";											
												$idfield = $fieldconnect2."id";
												if(strpos($idfield1, ';') !== false) {
													//die because someone is trying to inject something into the query
													die;
												}
												if(stripos($idfield1, 'delete') !== false) {
													//die because someone is trying to inject something into the query
													die;
												}
												if(stripos($idfield1, ' or ') !== false) {
													//die because someone is trying to inject something into the query
													die;
												}
												if(strpos(${'tablefieldconnect'.$number3}, ';') !== false) {
													//die because someone is trying to inject something into the query
													die;
												}
												if(stripos(${'tablefieldconnect'.$number3}, 'delete') !== false) {
													//die because someone is trying to inject something into the query
													die;
												}
												if(stripos(${'tablefieldconnect'.$number3}, ' or ') !== false) {
													//die because someone is trying to inject something into the query
													die;
												}
												if(strpos(${'tableconnect'.$number3}, ';') !== false) {
													//die because someone is trying to inject something into the query
													die;
												}
												if(stripos(${'tableconnect'.$number3}, 'delete') !== false) {
													//die because someone is trying to inject something into the query
													die;
												}
												if(stripos(${'tableconnect'.$number3}, ' or ') !== false) {
													//die because someone is trying to inject something into the query
													die;
												}
												if(strpos($orderclause, ';') !== false) {
													//die because someone is trying to inject something into the query
													die;
												}
												if(stripos($orderclause, 'delete') !== false) {
													//die because someone is trying to inject something into the query
													die;
												}
												if(stripos($orderclause, ' or ') !== false) {
													//die because someone is trying to inject something into the query
													die;
												}
								    		  	$namefield = ${'tablefieldconnect'.$number3};
								    		  	//echo $namefield;
								    		  	${'tablefieldconnect'.$number3} = mysql_real_escape_string(${'tablefieldconnect'.$number3});
								    		  	$orderclause = mysql_real_escape_string($orderclause);
								    		  	$idfield1 = mysql_real_escape_string($idfield1);
								    		  	${'tableconnect'.$number3} = mysql_real_escape_string(${'tableconnect'.$number3});
								    		  	if($database == $masterdatabase){
									    		  	if(${'tablefieldconnectparent'.$number3}==""){
									    		  		$querytype="select $idfield1, ".${'tablefieldconnect'.$number3}." from ".${'tableconnect'.$number3}." ".$orderclause;
									    		  	}
									    		  	else {							    		  		
									    		  		$querytype="select $idfield1, ".${'tablefieldconnect'.$number3}." from ".${'tableconnect'.$number3}." inner join ".${'tablefieldconnectparent'.$number3}." ".$orderclause;
													}
												}
												else{
													if(${'tablefieldconnectparent'.$number3}==""){
									    		  		$querytype="select $idfield1, ".${'tablefieldconnect'.$number3}." from ".${'tableconnect'.$number3}." where masteronly = 0 ";
									    		  		if($idfield == 'businessunitid' && $_SESSION["superadmin"] == 0){
															$querytype = $querytype."and businessunitid in (".$_SESSION["userbusinessunit"].") ";
														}
														$querytype = $querytype." ".$orderclause;
									    		  	}
									    		  	else {							    		  		
									    		  		$querytype="select $idfield1, ".${'tablefieldconnect'.$number3}." from ".${'tableconnect'.$number3}." inner join ".${'tablefieldconnectparent'.$number3}." where masteronly = 0 ";
									    		  		if($idfield == 'businessunitid' && $_SESSION["superadmin"] == 0){
															$querytype = $querytype."and businessunitid in (".$_SESSION["userbusinessunit"].") ";
														}
														$querytype = $querytype." ".$orderclause;											
													}												
												}
												$querytype = str_replace("##database##.", "", $querytype);
												//echo $querytype;			
												//this query is allowed to be not mysqli because of injection checks made above				    		  	
								    		  	$resulttype = mysql_query($querytype);
								    		  	if(isset($resulttype)){
													$numrowsdropdown = mysql_num_rows($resulttype);
												}
												else {
													$numrowsdropdown = 0;
												}				
																			
								    		  	if($numrowsdropdown <= 500 || ${'structurefieldname'.$number3} == 'customerid'){
								    		  		//if less than 500 records then show a dropdown								    		  		
										        	echo "<select class='form-control' name='".${'structurefieldname'.$number3}."'>";
										        	echo "<option value=''>".$langval19."</option>";
									         		while($nttype=mysql_fetch_array($resulttype)){
									         			$parenttablefieldname = explode(' ',trim(${'tablefieldconnectparent'.$number3}))[0];
									         			if($parenttablefieldname <> ""){
									         				$parenttablefieldname = $parenttablefieldname."name";
									         				$parenttablefieldname = $nttype[$parenttablefieldname].": ";
									         			}
									         			
									         			//unencrypt customername field
									         			if(${'structurefieldname'.$number3} == "customerid"){
									         				$salt = "h3f8s9en20vj3";
									         				$nttype[$namefield] = openssl_decrypt($nttype[$namefield],"AES-128-ECB",$salt);
														}
														
														//product name search update
														if(${'structurefieldname'.$number3} == "productid"){
															$productsql = mysql_query("select productuniqueid from product where productid = ".$nttype[$idfield]);
															$productresult = mysql_fetch_row($productsql);
															$nttype[$namefield] = $productresult[0]." - ".$nttype[$namefield]." (".$nttype[$idfield].")";
													   	}
									         			
														//display data									         			
									         			if (${"data$number3"}==$nttype[$idfield]) {
									               		echo "<option value=".$nttype[$idfield]." selected='true'>".$parenttablefieldname.$nttype[$namefield]."</option>";
								             	}
								             	else {
								               		echo "<option value=".$nttype[$idfield]." >".$parenttablefieldname.$nttype[$namefield]."</option>";
								             	}
								             }
							     				echo "</select>";
							     			}
							     			else {
							     				//if greater than 500 rows then show lazy loading search box
							     				$tname = ${'tableconnect'.$number3};
							     				$fieldid111 = 'tableconnect'.$number3.'id';
										    	?>
										    	<script>
												$(function() {		
													$("#ac<?php echo $fieldid111 ?>").autocomplete({
												        url: 'autocomplete.php?tablename=<?php echo $tname ?>',
												        useCache: false,
												        filterResults: false
												    });
											    });
												</script>
										    	<?php
										    	echo "<input type='text' id='ac$fieldid111' class='form-control' name='".${'structurefieldname'.$number3}."'>";
							     			}
								      		?>
								      		<?php echo ${'helptext'.$number3};	?>	
								    	</div>
								  	</div> 
									<?
									}
								}
								$number3 = $number3+1;	
												
							}
							
							?>						  	
						</div>
						<div class="form-group">
					    	<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					    		<div style="text-align:center;">
					      			<button type='submit' name='submit' value='Submit' class="button-primary"><?php echo $langval6 ?></button>																									
					    			<input type="hidden" value='<?php echo $pagename; ?>' name='pagename'>
				    			</div>
					    	</div>
					  	</div>
					</form>	
					
					<br/><br/><br/>
				</div>
	  		</div>
		</div>
			
	</div>	
	<?php include_once ('footer.php'); ?>
</body>
