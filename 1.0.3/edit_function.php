<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link href="./fontAwesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="screen" title="stylesheet" />
      <title>OneBusiness</title>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel='stylesheet' type='text/css' href='css/sidebar.css' />
      <link rel='stylesheet' type='text/css' href='css/content.css' />
      <link rel='stylesheet' type='text/css' href='css/header.css' />
      <script type="text/javascript" src="https://www.google.com/jsapi"></script>	
   </head>
   <body >
      <?php //include_once ('headerthree.php');	?>
      <html>
         <head>
         </head>
         <body class="content">
            <!----------------------------------------- sidebar ---------------------------------->
            <div class="sidebar">
               <nav class="main-menu">
                  <ul >
                     <li><i class="fa fa-long-arrow-left fa-2x arrow icon_click"></i></li>
                  </ul>
                  <ul class="navbar-nav flex-column w-100 mt-5">
                     <li class="nav-item active">
                        <img src="img/icons/my-apps.png" class="icon_click dropdown">
                        <a class="nav-link" href="my_app.php"  >My Apps</a>
                     </li>
                     <div class="dropdown-divider"></div>
                     <!-- Second Li Start-->
                     <li class="nav-item">
                        <img src="img/icons/dashboard-icon.png" class="icon_click dropdown">
                        <a class="nav-link" href="#" >
                        Dashboard
                        </a>
                     </li>
                     <!-- Second Li Close-->
                     <div class="dropdown-divider"></div>
                     <!-- <li class="nav-item">
                        <i class="fa fa-laptop fa-2x icon_click dropdown"></i>
                          <a class="nav-link" href="#" data-toggle="collapse" data-target="#navPages" aria-expanded="false" aria-controls="navPages" >
                             Reports   
                             <span class="ml-auto">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down">
                                   <polyline points="6 9 12 15 18 9"></polyline>
                                </svg>
                             </span>
                          </a>
                          <div id="navPages" class="collapse" data-parent="#sidebarNav">
                             <ul class="nav flex-column">
                                <li class="nav-item">
                                   <a class="nav-link" href="#" data-toggle="collapse" data-target="#navPagesSub" aria-expanded="false" aria-controls="navPagesSub" >
                                      Profile   
                                      <span class=" ml-auto">
                                         <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down">
                                            <polyline points="6 9 12 15 18 9"></polyline>
                                         </svg>
                                      </span>
                                   </a>
                                   <div class="collapse"  id="navPagesSub">
                                      <ul class="nav  flex-column navPagesSub">
                                         <li class="nav-item">
                                            <a class="nav-link" href="#" data-toggle="collapse" data-target="#navPagesSub2" aria-expanded="false" aria-controls="navPagesSub2" >
                                      Default Item   
                                      <span class=" ml-auto">
                                         <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down">
                                            <polyline points="6 9 12 15 18 9"></polyline>
                                         </svg>
                                      </span>
                                   </a>
                                           <div class="collapse"  id="navPagesSub2">
                                      <ul class="nav  flex-column">
                                         
                                         <li class="nav-item">
                                            <a class="nav-link" href="#">CMS</a>
                                         </li>
                                         <li class="nav-item">
                                            <a class="nav-link" href="#">Link</a>
                                         </li>
                                         <li class="nav-item">
                                            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                                         </li>
                                      </ul>
                                   </div>
                                         </li>
                                         <li class="nav-item">
                                            <a class="nav-link" href="#">CMS</a>
                                         </li>
                                         <li class="nav-item">
                                            <a class="nav-link" href="#">Link</a>
                                         </li>
                                         <li class="nav-item">
                                            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                                         </li>
                                      </ul>
                                   </div>
                                </li>
                                <li class="nav-item">
                                   <a class="nav-link" href="#">CMS</a>
                                </li>
                                <li class="nav-item">
                                   <a class="nav-link" href="#">Link</a>
                                </li>
                                <li class="nav-item">
                                   <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                                </li>
                             </ul>
                          </div>
                        </li>Second Li Close -->
                     <li class="nav-item ">
                        <img src="img/icons/reports-icon.png" class="icon_click dropdown">
                        <a class="nav-link" href="reports.php"  >
                        Reports</a>
                     </li>
                     <div class="dropdown-divider"></div>
                     <li class="nav-item ">
                        <img src="img/icons/my-details-icon.png" class="icon_click dropdown">
                        <a class="nav-link" href="#"  >
                        My Details 
                        </a>
                     </li>
                     <div class="dropdown-divider"></div>
                     <li class="nav-item ">
                        <img src="img/icons/users-icon.png" class="icon_click dropdown">
                        <a class="nav-link" href="#"  >
                        Users  
                        </a>
                        <!-- <div id="navDashboard4" class="collapse" data-parent="#sidebarNav">
                           <ul class="nav flex-column nav_1">
                              <li class="nav-item ">
                                 <a class="nav-link" href="#">Default</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link" href="#">CMS</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link" href="#">Link</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                              </li>
                           </ul>
                           </div> -->
                     </li>
                     <div class="dropdown-divider"></div>
                     <li class="nav-item ">
                        <img src="img/icons/site-builder-icon.png" class="icon_click dropdown">
                        <a class="nav-link" href="#"  >
                        Site Billing  
                        </a>
                     </li>
                     <div class="dropdown-divider"></div>
                     <li class="nav-item ">
                        <img src="img/icons/translation-icon.png" class="icon_click dropdown">
                        <a class="nav-link" href="#"  >Translation </a>
                     </li>
                     <div class="dropdown-divider"></div>
                     <li class="nav-item ">
                        <img src="img/icons/security-icon.png" class="icon_click dropdown">
                        <a class="nav-link" href="#"  >
                           Security
                           <!-- <span class="ml-auto">
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down">
                                 <polyline points="6 9 12 15 18 9"></polyline>
                              </svg>
                              </span> -->
                        </a>
                        <!-- <div id="navDashboard4" class="collapse" data-parent="#sidebarNav">
                           <ul class="nav flex-column nav_1">
                              <li class="nav-item ">
                                 <a class="nav-link" href="#">Default</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link" href="#">CMS</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link" href="#">Link</a>
                              </li>
                               <li class="nav-item">
                                 <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                              </li> 
                           </ul>
                           </div> -->
                     </li>
                     <div class="dropdown-divider"></div>
                     <li class="nav-item ">
                        <img src="img/icons/fave-icon-selected.png" class="icon_click dropdown">
                        <a class="nav-link" href="#" data-toggle="collapse" data-target="#navDashboard7" aria-expanded="false" aria-controls="navDashboard7" >
                           Favourites 
                           <span class="ml-auto">
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down">
                                 <polyline points="6 9 12 15 18 9"></polyline>
                              </svg>
                           </span>
                        </a>
                        <div id="navDashboard7" class="collapse" data-parent="#sidebarNav">
                           <ul class="nav flex-column nav_1">
                              <li class="nav-item ">
                                 <a class="nav-link" href="#">Stock</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link" href="#">Manage</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link" href="#">Finance</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link" href="#">Product</a>
                              </li>
                              <li class="link">
                                 <p>Manage Qick Link <img src="img/icons/add-icon.png" width="20px"></p>
                              </li>
                           </ul>
                        </div>
                     </li>
                  </ul>
                  <!-- <ul class="logout">
                     <li>
                        <a href="#">
                              <i class="fa fa-power-off fa-2x"></i>
                             <span class="nav-text">
                                 Logout
                             </span>
                         </a>
                     </li>  
                     </ul> -->
               </nav>
            </div>
            <!----------------------------------------- sidebar ---------------------------------->
            <!----------------------------------------- content ---------------------------------->
            <section  class="edit add ">
               <div class='overlay'></div>
               <div class="container">
                  <div class="row mt-5 header">
                     <div class="col-sm-6">
                        <img src="img/Apps.png" width="250px">
                     </div>
                     <div class="col-sm-6">
                        <p class="text-right txt_1">21st August 2020</p>
                     </div>
                  </div>
                  <div class="row mt-5 dekstop ">
                     <div class="col-xs-3">
                        <img src="img/icons/burger-menu.png" class="img-responsive icon_click">
                     </div>
                     <div class="col-xs-6">
                        <img src="img/Apps.png" class="img-responsive">
                     </div>
                     <div class="col-xs-3">
                        <div class="header_profile ">
                           <img src="img/profile.png" class="img-responsive left_click">
                        </div>
                     </div>
                  </div>
                  <div class="row ">
                     <div class="col-sm-12">
                        <div class="line"></div>
                     </div>
                  </div>
                  <div class="row mt-5 ">
                     <div class="col-sm-6 col-xs-12">
                        <h5 class="txt_2">Ecommerce Pricing Group</h5>
                     </div>
                     <div class="col-sm-6 d-sm-block d-none">
                        <p class="txt_3">Uploads</p>
                        <button class="btn-primary mb-2"><i class="fa fa-plus"></i> &nbsp; Add Ecommerce pricicng Group</button>
                        <button class="btn-primary mb-2"><i class="fa fa-upload"></i> &nbsp; Bulk upload Ecommerce pricicng Group</button>
                     </div>
                  </div>
                  <div class="back_board d-sm-block d-none">
                     <div class="row">
                        <div class="col-sm-3">
                           <div id="filterDate2">
                              <div class="input-group date" data-date-format="dd.mm.yyyy">
                                 <input  type="text" class="form-control" placeholder="dd.mm.yyyy">
                                 <div class="input-group-addon" >
                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-7 text-right">
                           <input type="text" id="searchInput" placeholder="Search..">
                           <div id='submitsearch' style="">
                              <span>Search</span>
                           </div>
                           <div class="page">
                              <p><b>Viewing 1-20 of 500</b></p>
                           </div>
                           <div class="icon">
                              <i class="fa fa-angle-left"></i>
                              <i class="fa fa-angle-right"></i>
                           </div>
                        </div>
                        <div class="col-sm-2">
                           <p><b>Advance Search</b> <i class="fa fa-cog"></i></p>
                        </div>
                     </div>
                     <div class="row mt-4">
                        <div class="col-sm-12">
                           <div class="table-responsive">
                              <table class="table table-bordered">
                                 <thead class="thead-dark">
                                    <tr>
                                       <th>eCom Pricing Group ID</th>
                                       <th>Disabled</th>
                                       <th>eCom Site Config</th>
                                       <th>eCom Site Config</th>
                                       <th>Config Rule Type</th>
                                       <th>Stay within Min/Max amounts?</th>
                                       <th>Daily increase Amount</th>
                                       <th>Daily decrease Amount</th>
                                       <th>Max runs per day per product</th>
                                       <th>Value Above/Below Lowest Price</th>
                                       <th>Increase/Decrease Price %</th>
                                       <th>Target Profit Margin</th>
                                       <th>Charging Postage</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <div class="dots_click">
                                             <span class="dot">&#8285;</span>
                                          </div>
                                          <div class="edit_click">
                                             <div class="edit_menu">
                                                <ul>
                                                   <li>EDIT</li>
                                                   <li>DEACTIVE</li>
                                                </ul>
                                                <div class="border_1"></div>
                                             </div>
                                          </div>
                                       </td>
                                       <td><span class="on">Active</span></td>
                                       <td>Ebay UK - 2nd Hand Books</td>
                                       <td>Ebay UK - 2nd Hand Books</td>
                                       <td>Match Lowest Price</td>
                                       <td>NO</td>
                                       <td>0.00</td>
                                       <td>0.00</td>
                                       <td>0.00</td>
                                       <td>0.00</td>
                                       <td>0.00</td>
                                       <td>0.00</td>
                                       <td>No</td>
                                    </tr>
                                    <tr>
                                       <td>
                                          <div class="dots_click1">
                                             <span class="dot">&#8285;</span>
                                          </div>
                                          <div class="edit_click1">
                                             <div class="edit_menu">
                                                <ul>
                                                   <li>EDIT</li>
                                                   <li>DEACTIVE</li>
                                                </ul>
                                                <div class="border_1"></div>
                                             </div>
                                          </div>
                                       </td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                    </tr>
                                    <tr>
                                       <td>
                                          <div class="dots_click2">
                                             <span class="dot">&#8285;</span>
                                          </div>
                                          <div class="edit_click2">
                                             <div class="edit_menu">
                                                <ul>
                                                   <li>EDIT</li>
                                                   <li>DEACTIVE</li>
                                                </ul>
                                                <div class="border_1"></div>
                                             </div>
                                          </div>
                                       </td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                    </tr>
                                    <tr>
                                       <td>
                                          <div class="dots_click3">
                                             <span class="dot">&#8285;</span>
                                          </div>
                                          <div class="edit_click3">
                                             <div class="edit_menu">
                                                <ul>
                                                   <li>EDIT</li>
                                                   <li>DEACTIVE</li>
                                                </ul>
                                                <div class="border_1"></div>
                                             </div>
                                          </div>
                                       </td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                    </tr>
                                    <tr>
                                       <td>
                                          <div class="dots_click4">
                                             <span class="dot">&#8285;</span>
                                          </div>
                                          <div class="edit_click4">
                                             <div class="edit_menu">
                                                <ul>
                                                   <li>EDIT</li>
                                                   <li>DEACTIVE</li>
                                                </ul>
                                                <div class="border_1"></div>
                                             </div>
                                          </div>
                                       </td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                    </tr>
                                    <tr>
                                       <td>
                                          <div class="dots_click5">
                                             <span class="dot">&#8285;</span>
                                          </div>
                                          <div class="edit_click5">
                                             <div class="edit_menu">
                                                <ul>
                                                   <li>EDIT</li>
                                                   <li>DEACTIVE</li>
                                                </ul>
                                                <div class="border_1"></div>
                                             </div>
                                          </div>
                                       </td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                    </tr>
                                 </tbody>
                                 <tfoot>
                                    <tr>
                                       <th scope="row" colspan="13"><img src="img/export.png" width="27px" style="margin-top:-3px"> Export</th>
                                    </tr>
                                 </tfoot>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <!--------------------------------------------- mobile -view ------------------------------ -->
            <section class="edit_menu1 d-sm-none d-block mt-2">
               <div class="container ">
                  <div class="row">
                     <div class="col-xs-8">
                        <div id="filterDate2">
                           <div class="input-group date" data-date-format="dd.mm.yyyy">
                              <input  type="text" class="form-control" placeholder="dd.mm.yyyy">
                              <div class="input-group-addon" >
                                 <i class="fa fa-angle-down" aria-hidden="true"></i>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-xs-4">
                        <p><b>Advance Search</b> <i class="fa fa-cog"></i></p>
                     </div>
                  </div>
                  <div class="row mt-3">
                     <div class="col-xs-12">
                        <input type="text" id="searchInput" placeholder="Search..">
                        <div id='submitsearch' style="">
                           <span>Search</span>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-xs-12 text-center mt-3">
                        <div class="page">
                           <p><b>Viewing 1-20 of 500</b></p>
                        </div>
                     </div>
                  </div>

                  <div class="card ">
                     <!--Top row-->
                     <div class="card-top" id="company-top">
                        <div class="header_one">
                           <p>ID: 1000007</p>
                        </div>
                        <div class="header_one text-center">
                           <p>Ebay UK - 2nd Hand Books</p>
                        </div>
                        <div class="more">
                           <button id="more-btn" class="more-btn">
                           <span class="more-dot"></span>
                           <span class="more-dot"></span>
                           <span class="more-dot"></span>
                           </button>
                           <div class="more-menu">
                              <div class="more-menu-caret">
                                 <div class="more-menu-caret-outer"></div>
                                 <div class="more-menu-caret-inner"></div>
                              </div>
                              <ul class="more-menu-items" tabindex="-1" role="menu" aria-labelledby="more-btn" aria-hidden="true">
                                 <li class="more-menu-item" role="presentation">
                                    <button type="button" class="more-menu-btn" role="menuitem">Edit</button>
                                 </li>
                                 <li class="more-menu-item" role="presentation">
                                    <button type="button" class="more-menu-btn" role="menuitem">Deactivate</button>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <!--Content-->
                     <div class="card-content">
                        <div class="row">
                           <div class="col-xs-4">
                              <img src="img/image.png" class="img-fluid border">
                           </div>
                           <div class="col-xs-8">
                              <p class="head_txt">Sub Category <span class="on">Active</span></p>
                              <p>Discription</p>
                              <p class="paragraph">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                 Ut enim ad minim veniam, quis nostrud exercitation ullamco labori.
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>

                  

               </div>
            </section>
            <!--------------------------------------------- mobile -view ------------------------------ -->
            <!----------------------------------------- content ---------------------------------->
            <!----------------------------------------- left-side-bar ---------------------------------->
            <div class="left-sidebar">
               <nav class="left-menu">
                  <ul class="">
                     <li>
                        <a href="#">
                           <div class="noti left_click mt-5">
                              <div class="profile ">
                                 <img src="img/profile.png" class="img-responsive">
                              </div>
                              <span class="notification">5</span>
                           </div>
                        </a>
                     </li>
                  </ul>
                  <ul class="logout">
                     <li class="remove_click">
                        <a href="#">
                        <i class="fa fa-long-arrow-right fa-2x"></i>
                        </a>
                     </li>
                     <div class="main_logout">
                        <p>Logout</p>
                        <div class="profile ">
                           <img src="img/profile.png" class="img-responsive">
                        </div>
                        <div class="txt_3">
                           <h3>Andrew Smith</h3>
                           <p>Director</p>
                        </div>
                     </div>
                     <li style="display:flex; margin-top:20px"> <img src="img/icons/edit-details.png" class="pl">
                        <a  href="#" > Edit My Details  </a>
                     </li>
                     <!-- <div class="dropdown-divider"></div> -->
                     <li style="display:flex; margin-top:20px"> <img src="img/icons/change-password.png" class="pl">
                        <a href="#" >Change Password</a>
                     </li>
                  </ul>
                  <ul>
                  </ul>
               </nav>
            </div>
            <!----------------------------------------- left-side-bar ---------------------------------->
            <script src="js/sidebar.js"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
            <script src="js/calender.js"></script>
            <script src="js/edit.js"></script>
         </body>
      </html>
      <?php // include_once ('footer.php'); ?>