<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="jquery-autocomplete-master/src/jquery.autocomplete.js"></script>
<link rel="stylesheet" type="text/css" href="jquery-autocomplete-master/src/jquery.autocomplete.css">

<?php

//LANGUAGE COLLECTION SECTION
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (4,5,9,10,19,582,583,584,585,586,587,588,589,590,591,592,593,594,595,596,597,598,599,600,601,602,603,
604,605,606,607,608,610,677,678,679,680,681,682,683,684,685,686,687,688,689,670,671,672,673,674,675,676,677,678,679,680,
681,682,683,684,685,686,687,688,689,690,691,692,693,694,695,696,697,698,699,700,701,702,703,704,705,706,707,708,709,710,
711,712,713,714,715,716,717,718,719,752,805,806,807,843,844,845,846,869,870,871,877,878,879,880,881)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}

//FILTERS

$businessunitid = isset($_POST['businessunitid']) ? $_POST['businessunitid'] : '';
$storagelocationsectionfromid = isset($_POST['storagelocationsectionfromid']) ? $_POST['storagelocationsectionfromid'] : '';
$storagelocationsectiontoid = isset($_POST['storagelocationsectiontoid']) ? $_POST['storagelocationsectiontoid'] : '';

$productid = isset($_POST['productid']) ? $_POST['productid'] : '';
$productuniqueid = isset($_POST['productuniqueid']) ? $_POST['productuniqueid'] : '';
$stocknumber = isset($_POST['stocknumber']) ? $_POST['stocknumber'] : '';
$producttransferreasonid = isset($_POST['producttransferreasonid']) ? $_POST['producttransferreasonid'] : '';

$flag = 0;
$date = date('Y-m-d');
$datetime = date('Y-m-d H:i:s');
$userid = $_SESSION["userid"];	

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
    if(!is_numeric($productid) && $productid <> ''){
        preg_match('#\((\d*?)\)#', $productid, $match);
        $productid = $match[1];	
    }
    
    if($productid <> '' || $productuniqueid <> '' )
    {
        if($productid <> '')
        {            
            $getrecords = "SELECT productstockitemid FROM productstockitem WHERE productid = $productid 
            AND productstockitemstatusid = 1 AND businessunitid = $businessunitid AND storagelocationsectionid = $storagelocationsectionfromid";
            //echo "<br/>Records : ".$getrecords;
            $getrecords = mysql_query($getrecords);
            $i = 0;
            while($getrecordsrow = mysql_fetch_array($getrecords))
            {
                $productstockitemid = $getrecordsrow['productstockitemid'];
                if($i<$stocknumber)
                {
                    mysql_query("UPDATE productstockitem SET storagelocationsectionid = $storagelocationsectiontoid
                    WHERE productstockitemid = $productstockitemid");
                    $i++;
                }
            }
            
            if($i>0)
            {
                $transfersql = "INSERT INTO productstockchangelog (productstockchangelogname, datecreated, userid, productstockchangetypeid, businessunitid, intostoragelocationsectionid, outstoragelocationsectionid, amountstockchange, productid, productstocktransferreasonid, datetimeadded) 
                VALUES ('Stock Transfer - $i items for product with ID - $productid', '$date', $userid, 3, $businessunitid, $storagelocationsectiontoid, $storagelocationsectionfromid, $i, $productid, $producttransferreasonid , '$datetime')";
                mysql_query($transfersql);
            }
        }
        else
        {
            $productid = '';
            $getproduct = mysql_query("SELECT productid FROM product WHERE productuniqueid = '$productuniqueid'");
            while($getproductrow = mysql_fetch_array($getproduct))
            {
                $productid = $getproductrow['productid'];
            }
            
            if($productid <> '')
            {
                $getrecords = "SELECT productstockitemid FROM productstockitem WHERE productid = $productid 
                AND productstockitemstatusid = 1 AND businessunitid = $businessunitid AND storagelocationsectionid = $storagelocationsectionfromid";
                //echo "<br/>Records : ".$getrecords;
                $getrecords = mysql_query($getrecords);
                $i = 0;
                while($getrecordsrow = mysql_fetch_array($getrecords))
                {
                    $productstockitemid = $getrecordsrow['productstockitemid'];
                    if($i<$stocknumber)
                    {
                        mysql_query("UPDATE productstockitem SET storagelocationsectionid = $storagelocationsectiontoid
                        WHERE productstockitemid = $productstockitemid");
                        $i++;
                    }
                }
                if($i>0)
                {
                    $transfersql = "INSERT INTO productstockchangelog (productstockchangelogname, datecreated, userid, productstockchangetypeid, businessunitid, intostoragelocationsectionid, outstoragelocationsectionid, amountstockchange, productid, productstocktransferreasonid, datetimeadded) 
                    VALUES ('Stock Transfer - $i items for product with ID - $productid', '$date', $userid, 3, $businessunitid, $storagelocationsectiontoid, $storagelocationsectionfromid, $i, $productid, $producttransferreasonid , '$datetime')";
                    mysql_query($transfersql);
                }
            }
        }
        $flag = 1;
        $validate = "";
        if($stocknumber != $i)
        {
            $storagelocation = array();
            $sql1 = mysql_query("select storagelocationsectionname from storagelocationsection where storagelocationsectionid IN ($storagelocationsectionfromid,$storagelocationsectiontoid)");
            while($row = mysql_fetch_array($sql1))
            {
                array_push($storagelocation, $row['storagelocationsectionname']);
            }
            $validate = "You requested to transfer $stocknumber items from location ".$storagelocation[0]." 
            to ".$storagelocation[1].". However, there are only $i items in the location ".$storagelocation[0].". 
            We have transferred $i items instead.";
        }
    }
    else
    {
        $validate = "Please input value for Product OR Product Barcode!";
    }
}

?>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <?php
    if (isset($validate) && $validate <> "") {
        echo "<p class='background-warning'>" . $validate . "</p>";
    }
    ?>
</div>

<form class="form-horizontal" action='view.php?viewid=56' method="post">
  <div class="form-group">
    <label class="control-label col-sm-2" for="Business Unit"><?php echo $langval752 ?></label>
    <div class="col-sm-4 col-md-4 col-lg-4">
    <?php
        $resulttype = mysql_query("select businessunitid, businessunitname from businessunit where disabled = 0");
        echo "<select class='form-control' name='businessunitid' required>";
        echo "<option value=''>Select Business Unit</option>";
        while($businessunitrow=mysql_fetch_array($resulttype)){
            if ($businessunitid == $businessunitrow['businessunitid']) {
            echo "<option value=".$businessunitrow['businessunitid']." selected='true'>".$businessunitrow['businessunitname']."</option>";
        }
        else {
                echo "<option value=".$businessunitrow['businessunitid']." >".$businessunitrow['businessunitname']."</option>";
        }
        }
        echo "</select>";
        ?>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="Storage Location From"><?php echo $langval877 ?></label>
    <div class="col-sm-4 col-md-4 col-lg-4"> 
    <?php
        $storagelocationsectionarray = array();
        $resulttype = mysql_query("select storagelocationsectionid, storagelocationsectionname from storagelocationsection where disabled = 0");
        echo "<select class='form-control' name='storagelocationsectionfromid' required>";
        echo "<option value=''>Select Storage Location Section</option>";
        while($storagelocationsectionrow=mysql_fetch_array($resulttype)){
            array_push($storagelocationsectionarray, array('id'=>$storagelocationsectionrow['storagelocationsectionid'],
            'name'=>$storagelocationsectionrow['storagelocationsectionname']));
        }
        usort($storagelocationsectionarray, function($a, $b){
            //return strnatcmp($a['manager'],$b['manager']); //Case sensitive
            return strnatcasecmp($a['name'],$b['name']); //Case insensitive
        });
        foreach($storagelocationsectionarray as $value) 
        {
            if ($storagelocationsectionfromid == $value['id']) {
            echo "<option value=".$value['id']." selected='true'>".$value['name']."</option>";
            }
            else {
                echo "<option value=".$value['id']." >".$value['name']."</option>";
            }
        }
        echo "</select>";
        ?>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-sm-2" for="Storage Location To"><?php echo $langval878 ?></label>
    <div class="col-sm-4 col-md-4 col-lg-4"> 
    <?php
        $storagelocationsectionarray = array();
        $resulttype = mysql_query("select storagelocationsectionid, storagelocationsectionname from storagelocationsection where disabled = 0");
        echo "<select class='form-control' name='storagelocationsectiontoid' required>";
        echo "<option value=''>Select Storage Location Section</option>";
        while($storagelocationsectionrow=mysql_fetch_array($resulttype)){
            array_push($storagelocationsectionarray, array('id'=>$storagelocationsectionrow['storagelocationsectionid'],
            'name'=>$storagelocationsectionrow['storagelocationsectionname']));
        }
        usort($storagelocationsectionarray, function($a, $b){
            //return strnatcmp($a['manager'],$b['manager']); //Case sensitive
            return strnatcasecmp($a['name'],$b['name']); //Case insensitive
        });
        foreach($storagelocationsectionarray as $value) 
        {
            if ($storagelocationsectiontoid == $value['id']) {
            echo "<option value=".$value['id']." selected='true'>".$value['name']."</option>";
            }
            else {
                echo "<option value=".$value['id']." >".$value['name']."</option>";
            }
        }
        echo "</select>";
        ?>
    </div>
  </div>

    <div class="form-group">
    <label class="control-label col-sm-2" for="Product"><?php echo $langval870 ?></label>
        <div class="col-sm-4 col-md-4 col-lg-4"> 
        <?php
        $resulttype = mysql_query("select productid, productname from product where disabled = 0 and excludefromstockmanager = 0");
        if(mysql_num_rows($resulttype) <= 500)
        {
            echo "<select class='form-control' name='productid' >";
            echo "<option value=''>Select Product</option>";
            while($productrow=mysql_fetch_array($resulttype)){
            //     if ($productid == $productrow['productid']) {
            //     echo "<option value=".$productrow['productid']." selected='true'>".$productrow['productname']."</option>";
            // }
            // else {
                echo "<option value=".$productrow['productid']." >".$productrow['productname']."</option>";
            //}
            }
            echo "</select>";
        }
        else
        {
        ?>
            <script>
                $(function() {
                    $("#acproductid").autocomplete({
                        url: 'stockautocomplete.php?tablename=product',
                        useCache: false,
                        filterResults: false,
                        maxItemsToShow: 200
                    });
                });
            </script>
        <?php
            echo "<input type='text' id='acproductid' class='form-control' name='productid' >";
        
        }
        
        ?>
        </div>
    </div>

    <div class="form-group">
    <label class="control-label col-sm-2" for="Product Barcode"><?php echo $langval871 ?></label>
        <div class="col-sm-4 col-md-4 col-lg-4"> 
        <input type="text" class="form-control" name="productuniqueid" <?php if($flag) echo "autofocus"; ?> >
        </div>
    </div>

    <div class="form-group">
    <label class="control-label col-sm-2" for="Number Stock Transfer"><?php echo $langval879 ?></label>
        <div class="col-sm-4 col-md-4 col-lg-4"> 
        <input type="number" class="form-control" name="stocknumber" min=0 required>
        </div>
    </div>

    <div class="form-group">
    <label class="control-label col-sm-2" for="Product Transfer Reason"><?php echo $langval880 ?></label>
        <div class="col-sm-4 col-md-4 col-lg-4"> 
        <?php
        $resulttype = mysql_query("select producttransferreasonid, producttransferreasonname from producttransferreason where disabled = 0");
        echo "<select class='form-control' name='producttransferreasonid' required > ";
        echo "<option value=''>Select Product Transfer Reason</option>";
        while($producttransferreasonrow=mysql_fetch_array($resulttype)){
        //     if ($producttransferreasonid == $producttransferreasonrow['producttransferreasonid']) {
        //     echo "<option value=".$producttransferreasonrow['producttransferreasonid']." selected='true'>".$producttransferreasonrow['producttransferreasonname']."</option>";
        // }
        // else {
                echo "<option value=".$producttransferreasonrow['producttransferreasonid']." >".$producttransferreasonrow['producttransferreasonname']."</option>";
        //}
        }
        echo "</select>";
        ?>
        </div>
    </div>
  
  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
    <button type='submit' name='submit' value='Transfer Stock' class="button-primary"><?php echo $langval881 ?></button>
    </div>
  </div>
</form>

<?php
echo "<br/><br/>";

?>