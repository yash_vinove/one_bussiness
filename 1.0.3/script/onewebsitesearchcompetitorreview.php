<?php

//LANGUAGE COLLECTION SECTION
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,
430,431,432,433,434,435,436,437,438,439,440)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}

$getrecord9 = mysql_query("select * from $database.websitesearchcompetitorreview 
where websitesearchcompetitorreviewid = '$recordid'");
$getrecordrow9 = mysql_fetch_array($getrecord9);
$websitesearchcompetitorreviewid = $getrecordrow9['websitesearchcompetitorreviewid'];
$competitorurl1 = $getrecordrow9['competitorurl1'];
$competitorurl2 = $getrecordrow9['competitorurl2'];
$competitorurl3 = $getrecordrow9['competitorurl3'];
$competitorurl4 = $getrecordrow9['competitorurl4'];
$competitorurl5 = $getrecordrow9['competitorurl5'];
$competitorurl6 = $getrecordrow9['competitorurl6'];
$competitorurl7 = $getrecordrow9['competitorurl7'];
$competitorurl8 = $getrecordrow9['competitorurl8'];
$competitorurl9 = $getrecordrow9['competitorurl9'];
$competitorurl10 = $getrecordrow9['competitorurl10'];

//update record to replace all https://
$competitorurl1 = str_replace("https://", "", $competitorurl1);
$competitorurl1 = str_replace("http://", "", $competitorurl1);
$competitorurl2 = str_replace("https://", "", $competitorurl2);
$competitorurl2 = str_replace("http://", "", $competitorurl2);
$competitorurl3 = str_replace("https://", "", $competitorurl3);
$competitorurl3 = str_replace("http://", "", $competitorurl3);
$competitorurl4 = str_replace("https://", "", $competitorurl4);
$competitorurl4 = str_replace("http://", "", $competitorurl4);
$competitorurl5 = str_replace("https://", "", $competitorurl5);
$competitorurl5 = str_replace("http://", "", $competitorurl5);
$competitorurl6 = str_replace("https://", "", $competitorurl6);
$competitorurl6 = str_replace("http://", "", $competitorurl6);
$competitorurl7 = str_replace("https://", "", $competitorurl7);
$competitorurl7 = str_replace("http://", "", $competitorurl7);
$competitorurl8 = str_replace("https://", "", $competitorurl8);
$competitorurl8 = str_replace("http://", "", $competitorurl8);
$competitorurl9 = str_replace("https://", "", $competitorurl9);
$competitorurl9 = str_replace("http://", "", $competitorurl9);
$competitorurl10 = str_replace("https://", "", $competitorurl10);
$competitorurl10 = str_replace("http://", "", $competitorurl10);

//update urls 
$updateurl = mysql_query("update $database.websitesearchcompetitorreview 
set competitorurl1 = '$competitorurl1', competitorurl2 = '$competitorurl2', competitorurl3 = '$competitorurl3', 
competitorurl4 = '$competitorurl4', competitorurl5 = '$competitorurl5', competitorurl6 = '$competitorurl6', 
competitorurl7 = '$competitorurl7', competitorurl8 = '$competitorurl8', competitorurl9 = '$competitorurl9', 
competitorurl10 = '$competitorurl10'
where websitesearchcompetitorreviewid = '$websitesearchcompetitorreviewid'");

//get record
$getrecord = mysql_query("select * from $database.websitesearchcompetitorreview 
inner join $database.websitesearchanalysis on websitesearchanalysis.websitesearchanalysisid = websitesearchcompetitorreview.websitesearchanalysisid
inner join $database.website on website.websiteid = websitesearchanalysis.onewebsiteid
where websitesearchcompetitorreviewid = '$recordid'");
$getrecordrow = mysql_fetch_array($getrecord);
$websitesearchanalysisid = $getrecordrow['websitesearchanalysisid'];
$websitesearchcompetitorreviewid = $getrecordrow['websitesearchcompetitorreviewid'];
$websitename = $getrecordrow['websitename'];
$websiteurl = $getrecordrow['websiteurl'];


//get list of competitors, clean up url, and add to array
$urllist = array();
for($i = 1; $i <= 10; $i++) {
	$urlfield = "competitorurl".$i;
	$compurl = $getrecordrow[$urlfield];
	if($compurl <> ''){
		array_push($urllist, $compurl);
	}
}	

//get my website, clean up url, and add to array
$getwebsite = mysql_query("select * from $database.websitesearchanalysis 
inner join $database.website on website.websiteid = websitesearchanalysis.onewebsiteid
where websitesearchanalysisid = '$websitesearchanalysisid'");
$getwebsiterow = mysql_fetch_array($getwebsite);
$myurl = $getwebsiterow['websiteurl'];
array_push($urllist, $myurl);

//$urllist = json_encode($urllist);

//class to run alexa
class UrlInfo {
    protected static $ServiceHost      = 'awis.amazonaws.com';
    public function UrlInfo($accessKeyId, $secretAccessKey, $site) {
        $this->accessKeyId = $accessKeyId;
        $this->secretAccessKey = $secretAccessKey;
        $this->site = $site;
    }
    public function getUrlInfo() {
        $queryParams = $this->buildQueryParams();
        $sig = $this->generateSignature($queryParams);
        $url = 'https://awis.amazonaws.com/?' . $queryParams . 
            '&Signature=' . $sig;
        $ret = self::makeRequest($url);
        //echo "\nResults for " . $this->site .":\n\n";
        $site = $this->site;
        self::parseResponse($ret,$site);
    }
	protected static function getTimestamp() {
        return gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time()); 
    }
    protected function buildQueryParams() {
        $params = array(
            'Action'            => 'UrlInfo',
            'ResponseGroup'     => 'Rank,LinksInCount,SiteData,Speed,UsageStats',
            'AWSAccessKeyId'    => $this->accessKeyId,
            'Timestamp'         => self::getTimestamp(),
            'Count'             => '10',
            'Start'             => '1',
            'SignatureVersion'  => '2',
            'SignatureMethod'   => 'HmacSHA256',
            'Url'               => $this->site
        );
        ksort($params);
        $keyvalue = array();
        foreach($params as $k => $v) {
            $keyvalue[] = $k . '=' . rawurlencode($v);
        }
        return implode('&',$keyvalue);
    }
    protected static function makeRequest($url) {
        //echo "$url\n\n";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
        	if (strpos($result, 'Fatal error') !== false) {
    			//echo 'text';
			}
    }
    public static function parseResponse($response,$site) {
        $xml = new SimpleXMLElement($response,null,false,
                                    'http://awis.amazonaws.com/doc/2005-07-11');
        if($xml->count() && $xml->Response->UrlInfoResult->Alexa->count()) {
            $info = $xml->Response->UrlInfoResult->Alexa;
            $nice_array = array(
                'linksincount' => $info->ContentData->LinksInCount,
                'rank' => $info->TrafficData->Rank,
                'sitedatatitle' => $info->ContentData->SiteData->Title,
                'description' => $info->ContentData->SiteData->Description,
                'onlinesince' => $info->ContentData->SiteData->OnlineSince,
               'sitespeedmedianloadtime' => $info->ContentData->Speed->MedianLoadTime,
                'sitespeedpercentile' => $info->ContentData->Speed->Percentile,
                'pagesperuser' => $info->TrafficData->UsageStatistics->UsageStatistic->PageViews->PerUser->Value
            );
        }
        //echo "<br/><br/>";
        $z = 0;
        global $database;
        global $websitesearchcompetitorreviewid;
        global $websitesearchanalysisid;
        global $myurl;
        
        $date = date ("Y-m-d");
        $site1 = "https://".$site;
        $site2 = "http://".$site;
        $mywebsite = 0;
        if($myurl == $site || $myurl == $site1 || $myurl == $site2){
        	$mywebsite = 1;
        }
        	//check if record exists
      		$getrecord = mysql_query("select * from $database.websitesearchcompetitorreviewmetric 
      		where (competitorurl = '$site' or competitorurl = '$site1' or competitorurl = '$site2') 
      		and websitesearchcompetitorreviewid = '$websitesearchcompetitorreviewid'");
      		$websitesearchcompetitorreviewmetricid = $getrecord['websitesearchcompetitorreviewmetricid'];
      		
      		if(isset($nice_array)){
	      		foreach($nice_array as $k => $v) {   		
	            //	echo $k . ': ' . $v ."<br/>";
	            	if($k == 'rank'){
						$rank = $v;		            	
	            	}
	            	if($k == 'linksincount'){
						$nooflinksin = $v;		            	
	            	}
	            	if($k == 'sitespeedmedianloadtime'){
						$pageloadtime = $v;		            	
	            	}	
	            	if($k == 'pagesperuser'){
						$pagesperuser = $v;		            	
	            	}
	        	}
	       } 	
      		//if record does not exist
      		if(mysql_num_rows($getrecord) >= 1){
      			//if record exists
      			$query = "update $database.websitesearchcompetitorreviewmetric set currentrank = '$rank', 
      			currentnooflinksin = '$nooflinksin', currentpageloadtime = '$pageloadtime', currentpagesperuser = '$pagesperuser'
      			where websitesearchcompetitorreviewid = '$websitesearchcompetitorreviewid' and 
      			(competitorurl = '$site' or competitorurl = '$site1' or competitorurl = '$site2')";
      			//echo $query;
      			$query = mysql_query($query);	
      		}
      		else {
      			//if record does not exist
      			$query = "insert into $database.websitesearchcompetitorreviewmetric (websitesearchcompetitorreviewid,
         		competitorurl, mywebsite, startrank, currentrank, startnooflinksin, currentnooflinksin,
         		startpageloadtime, currentpageloadtime, startpagesperuser, currentpagesperuser) 
				values ('$websitesearchcompetitorreviewid', '$site', '$mywebsite', 
				'$rank', '$rank', '$nooflinksin', '$nooflinksin', '$pageloadtime', '$pageloadtime', 
				'$pagesperuser', '$pagesperuser')";	
				//echo $query;
      			$query = mysql_query($query);					
      		}            		
        
        //echo "<br/><br/>";
    }
    protected function generateSignature($url) {
        $sign = "GET\n" . strtolower(self::$ServiceHost) . "\n/\n". $url;
        //echo "String to sign: \n" . $sign . "\n";
        $sig = base64_encode(hash_hmac('sha256', $sign, $this->secretAccessKey, true));
        //echo "\nSignature: " . $sig ."\n";
        return rawurlencode($sig);
    }
}

//iterate through array, getting results
for ($i = 0; $i < count($urllist); $i++) {
    $site = $urllist[$i];
	
	$accessKeyId = "AKIAJXEU5GQLLAUGN25Q";
	$secretAccessKey = "Wpsy7Lz4GsefIKwnk04MLcVw3B/wmoG6QfVj3wxj";
	
	$site = str_replace('https://', '', $site);	
	$site = str_replace('http://', '', $site);	
	//echo "<br/><br/>".$site."<br/><br/>";
	
	$urlInfo = new UrlInfo($accessKeyId, $secretAccessKey, $site);
	$urlInfo->getUrlInfo();
}

//build pdf report
$includeurl = "fpdf181/fpdf.php";
require($includeurl);

class PDF extends FPDF
{
	
// Page header
function Header()
{
    // Arial bold 15
    $this->SetFont('Arial','B',13);
}


// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}

}

//date created
$date = date("d-M-Y");
$date2 = date("Y-m-d");

// Instantiation of inherited class
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','B',13);
$pdf->Cell(75);
$pdf->Cell(40,10,$langval406.$websitename,0,0,'C');
$pdf->Ln(15);
$pdf->SetFont('Arial','',10);
$pdf->MultiCell(0,5,$langval407,0,1);
$pdf->Ln(5);
$pdf->SetFont('Arial','',10);
$pdf->MultiCell(0,5,$langval408.$date,0,1);
$pdf->MultiCell(0,5,$langval409.$websiteurl,0,1);


include('onewebsitesearchcompetitorreport.php');


$pdf->Ln(5);
$pdf->SetFont('Arial','U',12);
$pdf->Cell(0,10,$langval428,0,1);
$pdf->SetFont('Arial','',10);
$pdf->MultiCell(0,5,$langval429,0,1);
$getrecord7 = mysql_query("select * from $database.websitesearchcompetitorreviewmetric 
where websitesearchcompetitorreviewid = '$websitesearchcompetitorreviewid' and mywebsite = 1");
while($getrecord7row = mysql_fetch_array($getrecord7)){
	$competitorurl = $getrecord7row['competitorurl'];
	$currentrank = $getrecord7row['currentrank'];
	$currentnooflinksin = $getrecord7row['currentnooflinksin'];
	$currentpageloadtime = $getrecord7row['currentpageloadtime'] / 60 / 10;
	$currentpagesperuser = $getrecord7row['currentpagesperuser'];
	$startrank = $getrecord7row['startrank'];
	$startnooflinksin = $getrecord7row['startnooflinksin'];
	$startpageloadtime = $getrecord7row['startpageloadtime'] / 60 / 10;
	$startpagesperuser = $getrecord7row['startpagesperuser'];
	$pdf->Ln(6);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(50, 6, $langval430, 1, 'L', FALSE);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->SetXY($x, $y);
	$pdf->Cell(30,6, $langval431, 1, 0, "l");	
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->SetXY($x, $y);
	$pdf->Cell(30,6, $langval432, 1, 0, "l");	
	$pdf->Ln(6);
	$pdf->SetFont('Arial','',10);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->Cell(50, 6, $langval433, 1, 'L', FALSE);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->SetXY($x, $y);
	$pdf->Cell(30,6, $startrank, 1, 0, "l");	
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->SetXY($x, $y);
	$pdf->Cell(30,6, $currentrank, 1, 0, "l");	
	$pdf->Ln(6);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->Cell(50, 6, $langval434, 1, 'L', FALSE);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->SetXY($x, $y);
	$pdf->Cell(30,6, $startnooflinksin, 1, 0, "l");	
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->SetXY($x, $y);
	$pdf->Cell(30,6, $currentnooflinksin, 1, 0, "l");	
	$pdf->Ln(6);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->Cell(50, 6, $langval435, 1, 'L', FALSE);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->SetXY($x, $y);
	$pdf->Cell(30,6, $startpageloadtime, 1, 0, "l");	
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->SetXY($x, $y);
	$pdf->Cell(30,6, $currentpageloadtime, 1, 0, "l");	
	$pdf->Ln(6);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->Cell(50, 6, $langval436, 1, 'L', FALSE);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->SetXY($x, $y);
	$pdf->Cell(30,6, $startpagesperuser, 1, 0, "l");	
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->SetXY($x, $y);
	$pdf->Cell(30,6, $currentpagesperuser, 1, 0, "l");	
	$pdf->Ln(6);
}
$pdf->Ln(2);
$pdf->SetFont('Arial','I',9);
$pdf->MultiCell(0,6,$langval437,0,1);
$pdf->MultiCell(0,6,$langval438,0,1);
$pdf->MultiCell(0,6,$langval439,0,1);
$pdf->MultiCell(0,6,$langval440,0,1);

//uncomment this, if you want to see an example in the browser for testing. However, comment it out once you are finished otherwise the whilst loop will not run.
//$pdf->Output();

//check if onewebsite storage folder exists, if not, then create
$filedir = '../documents/'. $database."/sf/websitesearchcompetitorreview/";
if(file_exists($filedir)){
}
else {
	$file_directory = "../documents/".$database."/sf/websitesearchcompetitorreview/";
	mkdir($file_directory);
}

$filename="../documents/".$database."/sf/websitesearchcompetitorreview/".$websitesearchcompetitorreviewid." - Website Competitor Review.pdf";
$pdf->Output($filename,'F');

//create document record in structurefunctiondocument
$recordname = $websitesearchcompetitorreviewid." - Website Competitor Review.pdf";
$docurl = $database."/sf/websitesearchcompetitorreview/".$recordname;
$insertdocrecord = mysql_query("insert into $database.structurefunctiondocument (structurefunctiondocumentname, datecreated, 
documenturl, doctypename, structurefunctionid, rowid) values ('$recordname', '$date2', '$docurl', 
'Competitor Review', '226', '$recordid')");

//update status as in progress
$checkstatus = mysql_query("select * from $database.websitesearchanalysistask 
where websitesearchanalysisid = '$websitesearchanalysisid' and websitesearchtaskid = '5'");
$checkstatusrow = mysql_fetch_array($checkstatus);
$websitesearchtaskstatusid = $checkstatusrow['websitesearchtaskstatusid'];
if(!isset($websitesearchtaskid)){
	$websitesearchtaskid = 5;
}
$updatestatus = mysql_query("update $database.websitesearchanalysistask set websitesearchtaskstatusid = '2' 
where websitesearchanalysisid = '$websitesearchanalysisid' and websitesearchtaskid = '$websitesearchtaskid'");

?>