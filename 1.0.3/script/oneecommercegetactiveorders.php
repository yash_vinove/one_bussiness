<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$date =date("Y-m-d");

include('../1.0.3/phpAmazonMWS/includes/classes.php');
$orderidprocessedlist = array();

//find out what business units the logic needs to be run for
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$getbusinessunit = mysql_query("select businessunitid from $database.ecommercelisting group by businessunitid");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
//echo "<br/>Get list of news sources: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
while($row45=mysql_fetch_array($getbusinessunit)){
	$businessunitid = $row45['businessunitid'];
	echo "<br/><br/>Business Unit ID  - ".$businessunitid."<br/>";
	
	//get api details
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getsystemapi = mysql_query("select scriptlocation5, apikey1, apikey2, apikey3, apikey4, apikey5, apikey6, datacentre,
	productindividual, customertypeid, salesstageid, businessunitid, ecommercesiteconfigid,
	saleschannelid from $masterdatabase.systemapi 
	inner join $masterdatabase.ecommercesite on ecommercesite.systemapiid = systemapi.systemapiid
	inner join $database.ecommercesiteconfig on ecommercesiteconfig.ecommercesiteid = ecommercesite.ecommercesiteid
	where ecommercesiteconfig.businessunitid='$businessunitid' 
	and ecommercesiteconfig.disabled = 0");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	//echo "<br/>Get list of news sources: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($systemapirow = mysql_fetch_array($getsystemapi)){
		$apikey1 = $systemapirow['apikey1'];
		if($apikey1 <> ''){
			$apiscript = $systemapirow['scriptlocation5'];
			$apikey2 = $systemapirow['apikey2'];
			$apikey3 = $systemapirow['apikey3'];
			$apikey4 = $systemapirow['apikey4'];
			$apikey5 = $systemapirow['apikey5'];
			$apikey6 = $systemapirow['apikey6'];
			$datacentre = $systemapirow['datacentre'];
			$customertypeid = $systemapirow['customertypeid'];
			$ecommercesiteconfigid = $systemapirow['ecommercesiteconfigid'];
			$salesstageid = $systemapirow['salesstageid'];
			$saleschannelid = $systemapirow['saleschannelid'];
			if($apiscript == 'oneecommercegetactiveordersshopify.php' || $apiscript == 'oneecommercegetactiveordersamazon.php'){	
				include($apiscript);
				echo "<br/><br/>";
				//die;
			}
		}
	}
}


function getAmazonOrders() {
    try {
        $amz = new AmazonOrderList("Default"); //store name matches the array key in the config file
        $amz->setLimits('Modified', "- 336 hours"); //accepts either specific timestamps or relative times 
        $amz->setFulfillmentChannelFilter("MFN"); //no Amazon-fulfilled orders
        $amz->setOrderStatusFilter(
            array("Unshipped", "PartiallyShipped", "Canceled", "Unfulfillable")
            ); //no shipped or pending orders
        $amz->setUseToken(); //tells the object to automatically use tokens right away
        $amz->fetchOrders(); //this is what actually sends the request
        return $amz->getList();
    } catch (Exception $ex) {
        echo 'There was a problem with the Amazon library. Error: '.$ex->getMessage();
    }
}

function getAmazonOrderItemList($orderid) {
    try {
        $amz = new AmazonOrderItemList("Default", $orderid); //store name matches the array key in the config file
        $amz->setUseToken(); //tells the object to automatically use tokens right away
        $amz->fetchItems(); //this is what actually sends the request
        return $amz->getOrderId();
    } catch (Exception $ex) {
        echo 'There was a problem with the Amazon library. Error: '.$ex->getMessage();
    }
}

function sendDeleteFeed($feed) {
    try {
        $amz=new AmazonFeed(); //if there is only one store in config, it can be omitted
        $amz->setFeedType("_POST_PRODUCT_DATA_"); //feed types listed in documentation
        $amz->setFeedContent($feed); //can be either XML or CSV data; a file upload method is available as well
        $amz->submitFeed(); //this is what actually sends the request
        return $amz->getResponse();
    } catch (Exception $ex) {
        echo '<br/>There was a problem with the Amazon library. Error: '.$ex->getMessage();
    }
}

?>