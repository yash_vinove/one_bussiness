<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<?php


//LANGUAGE COLLECTION SECTION
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (720,721,722,723,724,725,726,727,728,729,730,731,732,733,734,735,736,737)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}


$rowid = isset($_GET['rowid']) ? $_GET['rowid'] : '';
$pagename = $_SERVER['REQUEST_URI'];
$pagename = str_replace("&", "xxxxxxxxxx", $pagename);								
$uid = $_SESSION['userid'];

echo "<a class='button-primary' href='view.php?viewid=22'>".$langval720."</a><br/><br/>";
	
//check project permissions
$getperm = mysql_query("select sum(allowprojectedit) as 'allowprojectedit', sum(allowmanagemember) as 'allowmanagemember' 
from $database.projectteammember 
where userid = '$uid' and projectid = '$rowid' and disabled='0'");
if(mysql_num_rows($getperm) >= 1){
	$getpermrow = mysql_fetch_array($getperm);
	$allowprojectedit = $getpermrow['allowprojectedit'];
	$allowmanagemember = $getpermrow['allowmanagemember'];
}
else {
	echo $langval721;
	die;
}

//get project details
$getproject = mysql_query("select projectname, projecttypename, customername, description, startdate, enddate, currencycode, 
budgetamount, progresspercentage, autocalculateprogress from $database.project 
inner join $masterdatabase.projecttype on projecttype.projecttypeid = project.projecttypeid
left join $database.customer on customer.customerid = project.onecustomerid
left join $database.currency on currency.currencyid = project.budgetcurrencyid
where project.projectid = '$rowid'");
$getprojectrow = mysql_fetch_array($getproject);
$projectname = $getprojectrow['projectname'];
$projecttypename = $getprojectrow['projecttypename'];
$customername = $getprojectrow['customername'];
$description = $getprojectrow['description'];
$startdate = $getprojectrow['startdate'];
$enddate = $getprojectrow['enddate'];
$autocalculateprogress = $getprojectrow['autocalculateprogress'];
$budgetcurrencycode = $getprojectrow['currencycode'];
$budgetamount = number_format($getprojectrow['budgetamount']);
$startdate = date($_SESSION["dateformat"], strtotime($startdate));			            		
$enddate = date($_SESSION["dateformat"], strtotime($enddate));	

//if programme/portfolio, then get other project names
$projectslist = '';
$projectsidlist = $rowid.",";
if($projecttypename == 'Programme' || $projecttypename == 'Portfolio'){
	$getprojects = mysql_query("select * from $database.projectgrouping 
	inner join $database.project on project.projectid = projectgrouping.projectid
	where masterprojectid = '$rowid' and projectgrouping.disabled = '0' and project.disabled='0'");
	while($row99 = mysql_fetch_array($getprojects)){
		$pname = $row99['projectname'];
		$projectgroupingid = $row99['projectgroupingid'];
		$pid = $row99['projectid'];
		$projectslist = $projectslist.$pname;
		$projectsidlist = $projectsidlist.$pid;
		if($allowprojectedit >= 1){
			$projectslist = $projectslist." <a href='pageedit.php?pagetype=project&rowid=".$pid."&pagename=".$pagename."'><i class='fa fa-pencil'></i></a> <a href='pagechangestatus.php?pagetype=projectgrouping&rowid=".$projectgroupingid."&action=deactivate&pagename=".$pagename."'><i class='fa fa-trash'></i></a>";
		}		
		$projectslist = $projectslist.", ";
		$projectsidlist = $projectsidlist.",";
	}
}
$projectslist = substr($projectslist, 0, -2);
$projectsidlist = substr($projectsidlist, 0, -1);

//calculate % complete for project
if($autocalculateprogress == 1){
	if($projecttypename == "Project"){
		$gettasktotal = mysql_query("select sum(minstodate) as 'timesofar', sum(minsestimated) as 'estimated' from $database.task 
		where oneprojectid = '$rowid' group by oneprojectid");
		$gettasktotalrow = mysql_fetch_array($gettasktotal);
		$timesofar = $gettasktotalrow['timesofar'];
		$estimated = $gettasktotalrow['estimated'];
		//echo "<br/>timesofar: ".$timesofar;
		//echo "<br/>estimated: ".$estimated;
		if($timesofar <> 0 && $estimated <> 0){
			$perccomplete = round(($timesofar / $estimated) * 100, 0);
			if($perccomplete > 100){
				$perccomplete = 100;				
			}
		}
		else {
			$perccomplete = 0;			
		}
		//echo "<br/>perccomplete: ".$perccomplete;
		if($perccomplete > 0){
			$updateproject = mysql_query("update $database.project set progresspercentage = '$perccomplete' 
			where projectid = '$rowid'");		
		}
		
		//get phases 
		$gettasktotalphase = mysql_query("select oneprojectphaseid, sum(minstodate) as 'timesofar', sum(minsestimated) as 'estimated' 
		from $database.task where oneprojectid = '$rowid' group by oneprojectphaseid");
		while($gettasktotalphaserow = mysql_fetch_array($gettasktotalphase)){
			$timesofar = $gettasktotalphaserow['timesofar'];
			$estimated = $gettasktotalphaserow['estimated'];
			$oneprojectphaseid = $gettasktotalphaserow['oneprojectphaseid'];
			//echo "<br/><br/>timesofarphase ".$oneprojectphaseid.": ".$timesofar;
			//echo "<br/>estimated ".$oneprojectphaseid.": ".$estimated;
			if($timesofar <> 0 && $estimated <> 0){
				$perccomplete = round(($timesofar / $estimated) * 100, 0);
				if($perccomplete > 100){
					$perccomplete = 100;				
				}
			}
			else {
				$perccomplete = 0;			
			}
			//echo "<br/>perccomplete: ".$perccomplete;
			if($perccomplete > 0){
				$updateproject = mysql_query("update $database.projectphase set progresspercentage = '$perccomplete' 
				where projectphaseid = '$oneprojectphaseid'");		
			}
		}
		
	}
	else {
		//get phases 
		$gettasktotalphase = mysql_query("select oneprojectphaseid, sum(minstodate) as 'timesofar', sum(minsestimated) as 'estimated' 
		from $database.task where oneprojectid = '$rowid' group by oneprojectphaseid");
		while($gettasktotalphaserow = mysql_fetch_array($gettasktotalphase)){
			$timesofar = $gettasktotalphaserow['timesofar'];
			$estimated = $gettasktotalphaserow['estimated'];
			$oneprojectphaseid = $gettasktotalphaserow['oneprojectphaseid'];
			//echo "<br/><br/>timesofarphase ".$oneprojectphaseid.": ".$timesofar;
			//echo "<br/>estimated ".$oneprojectphaseid.": ".$estimated;
			if($timesofar <> 0 && $estimated <> 0){
				$perccomplete = round(($timesofar / $estimated) * 100, 0);
				if($perccomplete > 100){
					$perccomplete = 100;				
				}
			}
			else {
				$perccomplete = 0;			
			}
			//echo "<br/>perccomplete: ".$perccomplete;
			if($perccomplete > 0){
				$updateproject = mysql_query("update $database.projectphase set progresspercentage = '$perccomplete' 
				where projectphaseid = '$oneprojectphaseid'");		
			}
		}
		$array = explode(',', $projectsidlist); 
		foreach($array as $value) {
			$gettasktotal = mysql_query("select sum(minstodate) as 'timesofar', sum(minsestimated) as 'estimated' from $database.task 
			where oneprojectid = '$value' group by oneprojectid");
			$gettasktotalrow = mysql_fetch_array($gettasktotal);
			$timesofar = $gettasktotalrow['timesofar'];
			$estimated = $gettasktotalrow['estimated'];
			//echo "<br/>timesofar: ".$timesofar;
			//echo "<br/>estimated: ".$estimated;
			if($timesofar <> 0 && $estimated <> 0){
				$perccomplete = round(($timesofar / $estimated) * 100, 0);
				if($perccomplete > 100){
					$perccomplete = 100;				
				}
			}
			else {
				$perccomplete = 0;			
			}
			//echo "<br/>perccomplete: ".$perccomplete;
			if($perccomplete > 0){
				$updateproject = mysql_query("update $database.project set progresspercentage = '$perccomplete' 
				where projectid = '$value'");		
			}
		}
	}	
}

//get project details
$getproject2 = mysql_query("select progresspercentage from $database.project 
where project.projectid = '$rowid'");
$getprojectrow2 = mysql_fetch_array($getproject2);
$progresspercentage = $getprojectrow2['progresspercentage'];


echo "<h2>".$projecttypename." ".$langval722."</h2>";
echo "<table class='table table-responsive'>";
echo "<tr><td width='50%' style='border:none;'>";
//project overview
echo "<table class='table table-bordered'>";
echo "<tr><td style='width:30%'><b>".$projecttypename." ".$langval723." </b><td>".$projectname."</td></tr>";
echo "<tr><td><b>".$langval724." </b><td>".$projecttypename."</td></tr>";
echo "<tr><td><b>".$langval725." </b><td>".$startdate;
if($enddate <> '0000-00-00'){
	echo " - ".$enddate;
}
echo "</td></tr>";
if($customername <> ''){
	echo "<tr><td><b>".$langval726." </b><td>".$customername."</td></tr>";
}
if($description <> ''){
	echo "<tr><td><b>".$langval727." </b><td>".$description."</td></tr>";
}
if($budgetcurrencycode <> '' && $budgetamount >= 0){
	echo "<tr><td><b>".$langval728." </b><td>".$budgetcurrencycode." ".$budgetamount."</td></tr>";
}
echo "<tr><td><b>".$langval729." % </b><td>".$progresspercentage."%</td></tr>";
if($allowprojectedit >= 1){
	echo "<tr><td><a class='button-primary' href='pageedit.php?pagetype=project&rowid=".$rowid."&pagename=".$pagename."'>".$langval730."</a></td></tr>";
}
echo "</table>";
echo "</td>";										
										
//project team
$getuser = mysql_query("select * from $database.projectteammember 
inner join $database.user on user.userid = projectteammember.userid
inner join $masterdatabase.projectteammembertype on projectteammembertype.projectteammembertypeid = projectteammember.projectteammembertypeid
where projectid = '$rowid' and projectteammember.disabled = '0'");
$projectowner = '';
$projectsponsor = '';
$projectmember = '';
$projectwatcher = '';
while($row22=mysql_fetch_array($getuser)){
	$firstname = $row22['firstname'];
	$lastname = $row22['lastname'];
	$projectteammemberid = $row22['projectteammemberid'];
	$projectteammembertypename = $row22['projectteammembertypename'];
	if($projectteammembertypename == 'Owner'){
		$projectowner = $projectowner.$firstname.' '.$lastname;
		if($allowmanagemember == 1){
			$projectowner = $projectowner." <a href='pageedit.php?pagetype=projectteammember&rowid=".$projectteammemberid."&pagename=".$pagename."'><i class='fa fa-pencil'></i></a> <a href='pagechangestatus.php?pagetype=projectteammember&rowid=".$projectteammemberid."&action=deactivate&pagename=".$pagename."'><i class='fa fa-trash'></i></a>";
		}
		$projectowner = $projectowner.", ";
	}
	if($projectteammembertypename == 'Watcher'){
		$projectwatcher = $projectwatcher.$firstname.' '.$lastname;
		if($allowmanagemember == 1){
			$projectwatcher = $projectwatcher." <a href='pageedit.php?pagetype=projectteammember&rowid=".$projectteammemberid."&pagename=".$pagename."'><i class='fa fa-pencil'></i></a> <a href='pagechangestatus.php?pagetype=projectteammember&rowid=".$projectteammemberid."&action=deactivate&pagename=".$pagename."'><i class='fa fa-trash'></i></a>";
		}
		$projectwatcher = $projectwatcher.", ";
	}
	if($projectteammembertypename == 'Member'){
		$projectmember = $projectmember.$firstname.' '.$lastname;
		if($allowmanagemember == 1){
			$projectmember = $projectmember." <a href='pageedit.php?pagetype=projectteammember&rowid=".$projectteammemberid."&pagename=".$pagename."'><i class='fa fa-pencil'></i></a> <a href='pagechangestatus.php?pagetype=projectteammember&rowid=".$projectteammemberid."&action=deactivate&pagename=".$pagename."'><i class='fa fa-trash'></i></a>";
		}
		$projectmember = $projectmember.", ";
	}
	if($projectteammembertypename == 'Sponsor'){
		$projectsponsor = $projectsponsor.$firstname.' '.$lastname;
		if($allowmanagemember >= 1){
			$projectsponsor = $projectsponsor." <a href='pageedit.php?pagetype=projectteammember&rowid=".$projectteammemberid."&pagename=".$pagename."'><i class='fa fa-pencil'></i></a> <a href='pagechangestatus.php?pagetype=projectteammember&rowid=".$projectteammemberid."&action=deactivate&pagename=".$pagename."'><i class='fa fa-trash'></i></a>";
		}
		$projectsponsor = $projectsponsor.", ";
	}
}
$projectowner = substr($projectowner, 0, -2);
$projectsponsor = substr($projectsponsor, 0, -2);
$projectmember = substr($projectmember, 0, -2);
$projectwatcher = substr($projectwatcher, 0, -2);
echo "<td style='border:none;'>";			
echo "<table class='table table-bordered'>";
if($projectowner <> ''){					
	echo "<tr><td style='width:30%'><b>".$langval731."</b><td>".$projectowner."</td></tr>";
}
if($projectsponsor <> ''){					
	echo "<tr><td><b>".$langval732."</b><td>".$projectsponsor."</td></tr>";
}
if($projectmember <> ''){					
	echo "<tr><td><b>".$langval733."</b><td>".$projectmember."</td></tr>";
}
if($projectwatcher <> ''){					
	echo "<tr><td><b>".$langval734."</b><td>".$projectwatcher."</td></tr>";
}
if($allowmanagemember >= 1){
	echo "<tr><td><a class='button-primary' href='pageadd.php?pagetype=projectteammember&pagename=".$pagename."'>".$langval735."</a></td></tr>";
}
echo "</table>";

//add projects list if this is a programme/portfolio
if($projecttypename <> "Project"){
	echo "<table class='table table-bordered'>";
	if($projectslist <> ''){
		echo "<tr><td style='width:30%'><b>".$langval736." </b><td>".$projectslist."</td></tr>";
	}
	if($allowprojectedit >= 1){
		echo "<tr><td>";
		echo "<a class='button-primary' href='pageadd.php?pagetype=projectgrouping&pagename=".$pagename."'>".$langval737."</a>";
		echo "</td></tr>";
	}
	echo "</table>";
}

echo "</td></tr></table>";
?>