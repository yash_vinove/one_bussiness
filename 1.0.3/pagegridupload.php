<?php session_start(); ?>
<?php include('config.php'); ?>
<?php include('sessionconfig.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<title>OneBusiness</title>
	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">   
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
	<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script type='text/javascript'>
	$(document).ready(function() {
		var option = 'intro-tekst';
		var url = window.location.href;
		option = url.match(/option=(.*)/)[1];
	});
	function showLess(option) {
		$('.boxes').hide();
		$('#' + option).show();
	} 
	</script>	
	<link rel='stylesheet' type='text/css' href='style.php' />
</head>
  
<body class='body'>
	<?php include_once ('headerthree.php');	?>
		
	<?php
	//LANGUAGE COLLECTION SECTION
	$mysqli = new mysqli($server, $user_name, $password, $database);
	$languageid = mysqli_real_escape_string($mysqli, $_SESSION['languageid']);
	if($stmt = $mysqli->prepare("SELECT languagerecordid, languagerecordtextname FROM $masterdatabase.languagerecordtext 
	where languageid = ? and languagerecordid in (21,22,23,5,24,25,26,64,74,75,76,77,78,79,80,81,82)")){
		$stmt->bind_param('i', $languageid);
		$stmt->execute();
		$result = $stmt->get_result();
		while ($row = $result->fetch_assoc()){
			$langid = $row['languagerecordid'];
			${"langval$langid"} = $row['languagerecordtextname'];
			//echo "<br/>".$langid." -".${"langval$langid"};
		}
	}
	else {
		//echo $mysqli->error;
	}
	$stmt->close();
	
	
	//get structurefunction details
	$pagetype = isset($_GET['pagetype']) ? $_GET['pagetype'] : '';
	$mysqli = new mysqli($server, $user_name, $password, $database);	$pagetype = mysqli_real_escape_string($mysqli, $pagetype);	if($stmt = $mysqli->prepare("select * from structurefunction where structurefunction.tablename = ?")){	   $stmt->bind_param('s', $pagetype);	   $stmt->execute();	   $result = $stmt->get_result();	   if($result->num_rows > 0){	    	while($pagedatarow = $result->fetch_assoc()){	     		$structurefunctionname = $pagedatarow['structurefunctionname'];	   	
				$structurefunctionid = $pagedatarow['structurefunctionid'];	  	    	}	   }	}	$mysqli->close();
	
	$valresult = "passed"; 	
	
	
	?>
	<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding:10px;">
		<!--search results header bar for desktop-->
		<div class='bodyheader'>
			<h3><?php echo $structurefunctionname ?> <?php echo $langval21?></h3>
		</div>
		<div class='bodycontent'>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			
			</div>
		
			<?php
			//define variables
			for($i = 0; $i <= 100; $i++) {
	  			${"fieldname$i"} = "";
			}
		   for($i = 0; $i <= 100; $i++) {
	  			${"fieldtypename$i"} = "";
			}
		   	for($i = 0; $i <= 100; $i++) {
	  			${"displayname$i"} = "";
			}
			for($i = 0; $i <= 100; $i++) {
	  			${"structurefieldtypeid$i"} = "";
			}
			
			
			//get list of all fields in structurefunction
			$mysqli = new mysqli($server, $user_name, $password, $database);			$structurefunctionid = mysqli_real_escape_string($mysqli, $structurefunctionid);			if($stmt = $mysqli->prepare("select * from structurefield 
			inner join structurefieldtype on structurefield.structurefieldtypeid = structurefieldtype.structurefieldtypeid			
			where structurefunctionid = ? and structurefield.disabled=0
			order by fieldposition asc")){			   $stmt->bind_param('i', $structurefunctionid);			   $stmt->execute();			   $result = $stmt->get_result();
			   $fieldid = 1;			   if($result->num_rows > 0){			    	while($gridcontentsrow = $result->fetch_assoc()){			     		$fieldname = $gridcontentsrow['structurefieldname'];
						$displayname = $gridcontentsrow['displayname'];
						$structurefieldtypeid = $gridcontentsrow['structurefieldtypeid'];
						$structurefieldtypename = $gridcontentsrow['fieldtypename'];
						//echo $fieldid." - ".$fieldname."<br/>";
						${'fieldname'.$fieldid} = $fieldname;
						${'displayname'.$fieldid} = $displayname;
						${'fieldtypename'.$fieldid} = $structurefieldtypename;
						${'structurefieldtypeid'.$fieldid} = $structurefieldtypeid;
						$fieldid = $fieldid +1;			    	}			   }
			   $countoffields = $fieldid - 1;			}			$mysqli->close();		
					
			
			//write fields to the csv header
			$counter4 = 1;
	      	$csv_hdr = "";
     		while($counter4 <= $countoffields) {
     			$value = ${'displayname'.$counter4};
     			$csv_hdr .= $value;
         		if($counter4 <> $countoffields){ 
         			$csv_hdr .= ",";
         		}
         		$counter4 = $counter4 +1;	            		
     		}
			
			//provide csv download button
			$query = "";
			?>
			<form name="export" action="export.php" method="post">
			    <input class="button-primary" type="submit" value="<?php echo $langval22 ?>">
			    <input type="hidden" value='<?php echo $csv_hdr; ?>' name='csv_hdr'>
			    <input type="hidden" value='<?php echo $query; ?>' name='query'>
			</form>
			<?php 
			
			//import data into temp table
			$submit = isset($_POST['submit']) ? $_POST['submit'] : ''; 
			if($submit) {
				//delete existing import for this user, if there is one
				$mysqli = new mysqli($server, $user_name, $password, $database);				$userid = mysqli_real_escape_string($mysqli, $userid);				if($stmt = $mysqli->prepare("delete from datauploadtemp where userid = ?")){				   $stmt->bind_param('i', $userid);				   $stmt->execute();				   				}				$mysqli->close();
				
		   		if ($_FILES['csv']['size'] > 0) {
		   			//get the csv file 
				   	$file = $_FILES['csv']['tmp_name'];
				   	ini_set('auto_detect_line_endings',TRUE);
				   	$handle = fopen($file,"r"); 
				   	$counter56 = 1;
					for($i = 0; $i <= 100; $i++) {
			  			${"content$i"} = "";
					}
					
					$data = array('','','');
					$numcols = 0;
					//loop through the csv file and insert into database
				   	do { 
				   		if ($data[0] || $data[1] || $data[2]) {
				   			//check for no apostrophies and html tags in all fields
				   			if($counter56 == 1){
				   				$counter56 = $counter56+1;
					     		for($i = 0; $i <= 100; $i++) {
					     			if(!isset($data[$i])) {
										$data[$i] = '';					     			
					     			}
					     			${'content'.$i} = str_replace("'", "", $data[$i]);
					     			if(${'content'.$i} == ''){
					     				${'content'.$i} = "";
					     				$numcols = $i;
					     				break;				     			
					     			}
					     		}
					     	$numcols = $numcols-1;
					     	}
					     	if($counter56 >= 1){
					     		for($i = 0; $i <= $numcols; $i++) {
					     			${'content'.$i} = str_replace("'", "", $data[$i]);
					     		}
					     	}
				     		//add record to database
				     		$querybuilder = "INSERT INTO datauploadtemp (userid, ";
				     		for($i = 1; $i <= 100; $i++) {
			  					$querybuilder = $querybuilder."field".$i.", ";
							}
							$querybuilder = rtrim($querybuilder, ', ');
				     		$querybuilder = $querybuilder.") VALUES (";
				         	$querybuilder = $querybuilder." '".$userid."',";
				         	for($i = 0; $i <= 99; $i++) {
					         	$querybuilder = $querybuilder." '".addslashes(${'content'.$i})."', ";
				         	}
				         	$querybuilder = rtrim($querybuilder, ', ');
				         	$querybuilder = $querybuilder.")";
				     		$query = mysql_query($querybuilder); 
				        	//echo $querybuilder;
				      	} 
				  // 	} while ($data = fgetcsv($handle,1000,",","'")); 
				   	} 
				   	while ($data = fgetcsv($handle,0)); 
				   
					//get all details of top row
					$querybuilder = "select min(datauploadtempid) as number, ";
					for($i = 1; $i <= 100; $i++) {
	  					$querybuilder = $querybuilder."field".$i.", ";
					}
					$querybuilder = rtrim($querybuilder, ', ');
					$userid = mysql_real_escape_string($userid);
		     		$querybuilder = $querybuilder." from datauploadtemp where userid = '".$userid."'";
		     		//echo $querybuilder;
					$gettop = mysql_query($querybuilder);
					$gettoprow = mysql_fetch_array($gettop);

					//check how many columns there are
					$numberofcols = 0;
					$fieldid = 1;
					for($i = 1; $i <= $numcols; $i++) {
						${'saveddisplayname'.$fieldid} = $gettoprow['field'.$i];
						//echo " ".${'displayname'.$fieldid}."=".${'saveddisplayname'.$fieldid}.", ";
						if(${'saveddisplayname'.$fieldid} == ""){
							if($numberofcols == 0) {
								$numberofcols = $i - 1;
							}
							
						}
						$fieldid = $fieldid +1;
					}					
					//echo $numberofcols;
					
				   	//redirect back to the page
					ini_set('auto_ detect_line_endings',FALSE); 
					
					//set validation values
					$validate = ""; 
					$validationresult = "passed";					
					
					//check that fieldnames are the same					
					$value = 0;
					$columnsvalidation = "";
					for($i = 1; $i <= $numberofcols; $i++) {
						if(${'displayname'.$i} == ${'saveddisplayname'.$fieldid}) {
			  				//echo "pass";
						}
						else {
							$value = $value+1;
			  				$columnsvalidation = $columnsvalidation.$langval69." ".${'saveddisplayname'.$fieldid}." ".$langval70." ".${'displayname'.$i}.". ";
						}
					}
					if($value == 0) {
						$columns = '1';
						$validationresult = "passed";
					}
					else {
						$columns = '0';
						$validationresult = "failed";					
					}
					//echo $columns;
					//echo $validationresult;
					//echo $numberofcols;
					
					if($validationresult == "passed") {
						//remove top header
						$mysqli = new mysqli($server, $user_name, $password, $database);						$userid = mysqli_real_escape_string($mysqli, $userid);						if($stmt = $mysqli->prepare("select min(datauploadtempid) as number from datauploadtemp 
						where userid = ?")){						   $stmt->bind_param('i', $userid);						   $stmt->execute();						   $result = $stmt->get_result();						   if($result->num_rows > 0){						    	while($gettoprow = $result->fetch_assoc()){						     		$deleteid = $gettoprow['number'];						    	}						   }						}						$mysqli->close();
						$mysqli = new mysqli($server, $user_name, $password, $database);						$deleteid = mysqli_real_escape_string($mysqli, $deleteid);						if($stmt = $mysqli->prepare("delete from datauploadtemp where datauploadtempid = ?")){						   $stmt->bind_param('i', $deleteid);						   $stmt->execute();						  						}						$mysqli->close();
						
						$numcols = $numcols +1;
						
						//check validation of each line
						$mysqli6 = new mysqli($server, $user_name, $password, $database);						$userid = mysqli_real_escape_string($mysqli6, $userid);						if($stmt6 = $mysqli6->prepare("select * from datauploadtemp where userid = ?")){						   $stmt6->bind_param('i', $userid);						   $stmt6->execute();						   $result6 = $stmt6->get_result();						   if($result6->num_rows > 0){						    	while($row5 = $result6->fetch_assoc()){						     		$validate = "";
							
									for($i = 1; $i <= $numcols; $i++) {
										${'value'.$i} = $row5['field'.$i];
		  								$datauploadtempid = $row5['datauploadtempid'];
		  								
		  								$mysqli = new mysqli($server, $user_name, $password, $database);										$structurefunctionid = mysqli_real_escape_string($mysqli, $structurefunctionid);										${"fieldname$i"} = mysqli_real_escape_string($mysqli, ${"fieldname$i"});										if($stmt = $mysqli->prepare("select * from structurefield 
		  								where structurefieldname = ? and structurefunctionid = ?")){										   $stmt->bind_param('si', ${"fieldname$i"}, $structurefunctionid);										   $stmt->execute();										   $result = $stmt->get_result();										   if($result->num_rows > 0){										    	while($validationrow = $result->fetch_assoc()){										     		$displayname = $validationrow['displayname'];
													$requiredfield = $validationrow['requiredfield'];
													$tableconnect = $validationrow['tableconnect'];
													$tablefieldconnect = $validationrow['tablefieldconnect'];
													$textlimit = $validationrow['textlimit'];
													$minvalue = $validationrow['lowvalue'];
													$maxvalue = $validationrow['highvalue'];
													$uniqueval = $validationrow['uniqueval'];
													//echo "i = ".$i."<br/>";
													//echo "value = ".${'value'.$i}."<br/>";
													//echo "displayname = ".$displayname."<br/>";
													//echo "requiredfield = ".$requiredfield."<br/>";
													//echo "textlimit = ".$textlimit."<br/>";
													//echo "fieldtypename = ".${'fieldtypename'.$i}."<br/>";										    	}										   }										}										$mysqli->close();

			  							//check for required fields - all field types
			  							if($requiredfield == 1 && ${'value'.$i} == "") {
											$validate = $validate.$displayname." ".$langval71." "; 
							 			}
							 		
							 			//validation - uniquecheck
										if($uniqueval == 1) {
											$mysqli = new mysqli($server, $user_name, $password, $database);											${'value'.$i} = mysqli_real_escape_string($mysqli, ${'value'.$i});											${'fieldname'.$i} = mysqli_real_escape_string($mysqli, ${'fieldname'.$i});											$pagetype = mysqli_real_escape_string($mysqli, $pagetype);											if($stmt = $mysqli->prepare("select * from $pagetype where ${'fieldname'.$i} = ?")){											   $stmt->bind_param('s', ${'value'.$i});											   $stmt->execute();											   $result = $stmt->get_result();											   $numrows = $result->num_rows;											   											}											$mysqli->close();

											if($numrows >= 1){
												$validate = $validate.$displayname." ".$langval64." "; 	
											}
								 		}
							 			
							 			//check that text is below maximum limit - 1
							 			if(${'fieldtypename'.$i} == 'Text' && strlen(${'value'.$i}) > $textlimit){
											$validate = $validate.$displayname." ".$langval72." ".$textlimit.". ";					 			
							 			}
							 			
							 			
							 			//check that unique tableid is not text - 2
							 			if(${'fieldtypename'.$i} == 'Unique TableID' && !is_numeric(${'value'.$i})){
							 				if (${'value'.$i} == ""){
							 				
							 				}
							 				else {
							 					$validate = $validate.$displayname." ".$langval73." ";		
							 				}			 			
							 			}
							 			
							 			//check that checkbox is either yes or no - 3a
							 			if(${'fieldtypename'.$i} == 'Checkbox'){
							 				if(${'value'.$i} == 'Yes' || ${'value'.$i} == 'No'){
							 					if(${'value'.$i} == 'Yes'){
													${'value'.$i} = 1;		
													$mysqli = new mysqli($server, $user_name, $password, $database);													${'value'.$i} = mysqli_real_escape_string($mysqli, ${'value'.$i});													$datauploadtempid = mysqli_real_escape_string($mysqli, $datauploadtempid);													if($stmt = $mysqli->prepare("update datauploadtemp set field".$i." = ? where datauploadtempid = ?")){													   $stmt->bind_param('si', ${'value'.$i}, $datauploadtempid);													   $stmt->execute();													}													$mysqli->close();																								
							 					}
							 					else {
							 						${'value'.$i} = 0;
							 						$mysqli = new mysqli($server, $user_name, $password, $database);													${'value'.$i} = mysqli_real_escape_string($mysqli, ${'value'.$i});													$datauploadtempid = mysqli_real_escape_string($mysqli, $datauploadtempid);													if($stmt = $mysqli->prepare("update datauploadtemp set field".$i." = ? where datauploadtempid = ?")){													   $stmt->bind_param('si', ${'value'.$i}, $datauploadtempid);													   $stmt->execute();													}													$mysqli->close();																									
							 					}
							 				}
							 				else {
							 					$validate = $validate.$displayname." ".$langval74." ";	
							 				}
										}
										
							 			//check that status is either active or disabled or blank - 3b
							 			if(${'fieldtypename'.$i} == 'Status'){
							 				if(${'value'.$i} == '1' || ${'value'.$i} == '0'){
							 				}
							 				else {
							 					$validate = $validate.$displayname." ".$langval75." ";	
							 				}
										}
							 			
							 			//check that table connects actually exist - 4 + update the value
							 			$valued7 = 0;
							 			if(${'fieldtypename'.$i} == 'Table Connect'){
							 				$tableconnect = strstr($tableconnect, '.');
							 				$tableconnect = str_replace('.', '', $tableconnect);
							 				$tableconnect = mysql_real_escape_string($tableconnect);
							 				$tablefieldconnect = mysql_real_escape_string($tablefieldconnect);
							 				$tableconnect = mysql_real_escape_string($tableconnect);
							 				$getrecord = "select ".$tableconnect."id, ".$tablefieldconnect." from ".$tableconnect." where disabled = 0";
							 				$getrecords = mysql_query($getrecord);
							 				//echo $getrecord;
							 				while($row10 = mysql_fetch_array($getrecords)) {
												$name = $row10[$tablefieldconnect];
												$id = $row10[$tableconnect.'id'];
												if(${'value'.$i} == $name) {
													$valued7 = 1;	
													$i = mysql_real_escape_string($i);
							 						$id = mysql_real_escape_string($id);
							 						$datauploadtempid = mysql_real_escape_string($datauploadtempid);
													$update = "update datauploadtemp set field".$i." = ".$id." where datauploadtempid = ".$datauploadtempid;
													$updateid = mysql_query($update);
													//echo $update."<br/><br/>";									
												}					 				
							 				}
							 				if($valued7 == 0) {
							 					$validate = $validate.$displayname." ".$langval76." ".$tableconnect." ".$langval77." ";
							 				}
							 			}
							 			
							 			
							 			//check that value is between min and max - 5
							 			if(${'fieldtypename'.$i} == 'Value'){
							 				if(${'value'.$i} < $minvalue && $minvalue <>0){
							 					$validate = $validate.$displayname." ".$langval78." ".$minvalue.". ";
							 				}
							 				if(${'value'.$i} > $maxvalue && $maxvalue <>0){
							 					$validate = $validate.$displayname." ".$langval79." ".$maxvalue.". ";
							 				}
							 			}
							 			
							 			//check that date formats are correct - 6
							 			if(${'fieldtypename'.$i} == 'Date Select'){
							 				//echo "<br/>value".$i.": ".${'value'.$i};
							 				${'value'.$i} = implode('-', array_reverse(explode('/', trim(${'value'.$i}))));
							 				//echo "<br/>value".$i.": ".${'value'.$i};
							 				$format = "Y-m-d";
											if(date($format, strtotime(${'value'.$i})) == date(${'value'.$i})) {
												$timestamp = strtotime(${'value'.$i});
								     			$timestamp = date('Y-m-d', $timestamp);
								     			$mysqli = new mysqli($server, $user_name, $password, $database);												$i = mysqli_real_escape_string($mysqli, $i);												$timestamp = mysqli_real_escape_string($mysqli, $timestamp);												$datauploadtempid = mysqli_real_escape_string($mysqli, $datauploadtempid);												if($stmt = $mysqli->prepare("update datauploadtemp set field".$i." = ? where datauploadtempid = ?")){												   $stmt->bind_param('si', $timestamp, $datauploadtempid);												   $stmt->execute();												   												}												$mysqli->close();
								     									
											} 
											else {
											    $validate = $validate.$displayname." ".$langval80." ";
											}
								 			
				  						}
								   }
									//echo "<b>".$validate."<br/><br/><br/></b>";
									if($validate == ""){
									}
									else {
										$valresult = "failed";							
									}
									$mysqli = new mysqli($server, $user_name, $password, $database);									$validate = mysqli_real_escape_string($mysqli, $validate);									$datauploadtempid = mysqli_real_escape_string($mysqli, $datauploadtempid);									if($stmt = $mysqli->prepare("update datauploadtemp set validationresult = ? where datauploadtempid = ?")){									   $stmt->bind_param('si', $validate, $datauploadtempid);									   $stmt->execute();												   									}									$mysqli->close();												 		
												    	}						   }						}						$mysqli6->close();
						
						
						
					
						
				 		//update the tables
						if($valresult == "passed") {
							//identify type of record and append type into table
							$mysqli = new mysqli($server, $user_name, $password, $database);							$userid = mysqli_real_escape_string($mysqli, $userid);							if($stmt = $mysqli->prepare("select * from datauploadtemp where userid = ?")){							   $stmt->bind_param('i', $userid);							   $stmt->execute();
							   $result = $stmt->get_result();							   if($result->num_rows > 0){							    	while($row77 = $result->fetch_assoc()){							     		//find out if update or add
										$check = $row77['field2'];
										$datauploadtempid = $row77['datauploadtempid'];
										if($check >= 1) {
											$datatrackingarray = array();
											//update records that have an id
											//echo "update".$datauploadtempid;
											$queryupdate = "update ".$pagetype." set ";
											for($i = 2; $i <= $numcols; $i++) {
									  			${"content$i"} = $row77['field'.$i];
									  			${"fieldname$i"} = mysql_real_escape_string(${"fieldname$i"});
									  			${"content$i"} = mysql_real_escape_string(${"content$i"});
									  			$queryupdate = $queryupdate.${"fieldname$i"}." = '".${"content$i"}."', ";
									  			array_push($datatrackingarray, array(${"fieldname$i"} => ${"content$i"}));
							
											}
											$queryupdate = rtrim($queryupdate, ', '); 
											$pagetype = mysql_real_escape_string($pagetype);
											$check = mysql_real_escape_string($check);
											$queryupdate = $queryupdate." where ".$pagetype."id = ".$check;
											$queryrun = mysql_query($queryupdate);
											//echo $queryupdate."<br/><br/>";
											//echo "<br/><br/>datatrackingarray: ".json_encode($datatrackingarray);
				
											//update data tracking 
											$date = date("Y-m-d");
											$datatracking = json_encode($datatrackingarray);
											$updatedatatracking = mysql_query("insert into trackchanges (trackchangesname, structurefunctionid, rowid, 
											userid, changes, type, subtype, disabled, datecreated, masteronly) 
											values ('Edit Record Via Grid Upload', $structurefunctionid, $check, $userid, '$datatracking', 
											'Edit', 'After', 0, '$date', 0)");
											
										}
										else {
											//add in new records
											$datatrackingarray = array();
											//echo "add".$datauploadtempid;
											$createddate = date("Y-m-d");
											$pagetype = mysql_real_escape_string($pagetype);
											$querybuilder = "insert into ".$pagetype." (";
											$number11 = 2;
											while($number11 <= $countoffields) {
												if($number11 == $countoffields) {
													$querybuilder .=${"fieldname$number11"};
												}
												else {
													$querybuilder .=${"fieldname$number11"}.", ";
												}
												$number11 = $number11 + 1;					
											}
											$querybuilder = $querybuilder.", datecreated) values (";
											$number11 = 2;
											
											while($number11 <= $countoffields) {
												${"content$number11"} = $row77['field'.$number11];
												${"content$number11"} = mysql_real_escape_string(${"content$number11"});
												if($number11 == $countoffields) {
													$querybuilder .="'".${"content$number11"}."'";
												}
												else {
													$querybuilder .="'".${"content$number11"}."', ";
												}
												array_push($datatrackingarray, array(${"fieldname$number11"} => ${"content$number11"}));
												$number11 = $number11 + 1;					
											}
											$createddate = mysql_real_escape_string($createddate);
											$querybuilder = $querybuilder.", '".$createddate."')";
											//echo "<br/>".$querybuilder."<br/><br/>";
										   //run the query
										   $insertrecord = mysql_query($querybuilder);
										   $recid = mysql_insert_id();
										   
										   //update data tracking 
											$date = date("Y-m-d");
											$datatracking = json_encode($datatrackingarray);
											$updatedatatracking = mysql_query("insert into trackchanges (trackchangesname, structurefunctionid, rowid, 
											userid, changes, type, subtype, disabled, datecreated, masteronly) 
											values ('Add New Record Via Grid Upload', $structurefunctionid, $recid, $userid, '$datatracking', 
											'Add', 'New', 0, '$date', 0)");
											
										}							    	}							   }							}							$mysqli->close();
							
							
														
						}
								
							}
							$url = 'pagegridupload.php?pagetype='.$pagetype.'&processed=1&validation='.$valresult.'&columns='.$columns.'&columnsvalidation='.$columnsvalidation;
							echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">';    												
						} 
					}
								

			?>
			<script>
				$(document).ready(function(){
				    $('#myTable').dataTable({
				    	"searching": false
				 });
				});
			</script>
			
			<br/>	
			<form action="" method="post" enctype="multipart/form-data" name="form1" id="form1"> 
			  <b><?php echo $langval23 ?> </b><br/><br/> 
			  <input name="csv" type="file" id="csv" /> 
			  <br/>
			  <input class="button-primary" type="submit" name="submit" value="<?php echo $langval5 ?>" /> 
			</form> 	
			<br/>
			<?php
			//show error
			if (!empty($_GET['processed'])) { 
				//return validation results
				$valresult = isset($_GET['validation']) ? strip_tags($_GET['validation']) : '';
				$columnsvalidation = isset($_GET['columnsvalidation']) ? strip_tags($_GET['columnsvalidation']) : '';
				$columns = isset($_GET['columns']) ? strip_tags($_GET['columns']) : '';
				if($valresult == "failed") {
					if($columns == '1') {
						echo "<p>".$langval24."</p>";
						$userid = mysql_real_escape_string($userid);
						$querybuilder = "select * from datauploadtemp where userid = '$userid' and validationresult <> ''";
					   	$querybuilder = $querybuilder." order by datauploadtempid";
				     	$query = $querybuilder;
				     	//echo $querybuilder;
				      	$result = mysql_query($query);
						if(mysql_num_rows($result)>0){
				         	echo "<div class='table-responsive'><table id='myTable' class='table table-bordered'>";
				         	echo "<thead><tr>";
				        	echo "<th>".$langval81." 1</th>";
				         	echo "<th>".$langval81." 2</th>";
				         	echo "<th>".$langval81." 3</th>";
				         	echo "<th>".$langval82."</th>";
				         	echo "</tr></thead>";
				         	while($row=mysql_fetch_array($result)){
				         		echo "<tr>";
			        			echo "<td>".$row['field1']."</td>";
			        			echo "<td>".$row['field2']."</td>";
			        			echo "<td>".$row['field3']."</td>";
			        			echo "<td>".$row['validationresult']."</td>";
			            		echo "</tr>";
		            		}
		      	    		echo "</table></div>";
		          	}
		          	else {
		            		echo "No results";
		          	}
		          }
		      		else {
						echo "<p>".$langval25."</p>";		      		
		      			echo $columnsvalidation;
		      		}				
				}
				else {
					echo "<b>".$langval26."</b><br><br>";
				}
			} 
		
			?> 
		
			</div>
			<br/><br/><br/>		
		</div>	
		<br/><br/><br/>	<br/><br/><br/>
	</div>	
	
	<?php include_once ('footer.php'); ?>
</body>
