<script type="text/javascript">
 function redirectMe (sel) {
     var url = sel[sel.selectedIndex].value;
     window.location = url;
 }
 </script>
 
<?php
$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
$financialbudgetperiodid = isset($_GET['financialbudgetperiodid']) ? $_GET['financialbudgetperiodid'] : '';
if($financialbudgetperiodid <=0){
	$financialbudgetperiodid = 0;
}

$getfinancialbudgetperiod = mysql_query("select * from financialbudgetperiod 
inner join $masterdatabase.financialbudgetperiodfrequency on financialbudgetperiodfrequency.financialbudgetperiodfrequencyid = 
financialbudgetperiod.financialbudgetperiodfrequencyid
where financialbudgetperiodid = '$financialbudgetperiodid'");
$getfinancialbudgetperiodrow = mysql_fetch_array($getfinancialbudgetperiod);
$financialbudgetperiodfrequencyid = $getfinancialbudgetperiodrow['financialbudgetperiodfrequencyid'];
$financialbudgetperiodfrequencyname = $getfinancialbudgetperiodrow['financialbudgetperiodfrequencyname'];
$previousfinancialbudgetperiodid = $getfinancialbudgetperiodrow['previousfinancialbudgetperiodid'];
$targetgrowthpercentage = $getfinancialbudgetperiodrow['targetgrowthpercentage'];
$targetprofitpercentage = $getfinancialbudgetperiodrow['targetprofitpercentage'];
$costincreasepercentage = $getfinancialbudgetperiodrow['costincreasepercentage'];
$getprevious = mysql_query("select * from financialbudgetperiod 
where financialbudgetperiodid = '$previousfinancialbudgetperiodid'");
$getpreviousrow = mysql_fetch_array($getprevious);
$startdate = $getpreviousrow['startdate'];			
$enddate = $getpreviousrow['enddate'];
$from = date('Y-m-d');
$noofdays = (strtotime($enddate) - strtotime($from)) / (60 * 60 * 24);
if($noofdays <= 0){
	$noofdays = 0;
}
//echo $noofdays."<br/><br/>"; 

$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (19,100,101,102)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}
	
//setup new budget period if submit button hit
if(isset($_POST['submit'])) {
	//get list of business units
	$getbu = mysql_query("select * from businessunit where disabled = 0");
	while($row2=mysql_fetch_array($getbu)){
		$businessunitid = $row2['businessunitid'];
		$businessunitname = $row2['businessunitname'];
		$currencyid = $row2['currencyid'];
		$name = "Budget Forecast: ".$businessunitname;
		
		//add budgetforecast records 
		$addrecord = mysql_query("insert into budgetforecast (budgetforecastname, businessunitid, financialbudgetperiodid, 
		budgetforecaststatusid, currencyid, disabled, datecreated, masteronly) values ('$name', '$businessunitid', 
		'$financialbudgetperiodid', '1', '$currencyid', '0', '$date', '0')");
		$budgetforecastid = mysql_insert_id();
		
		$totalexpenditureannualval = 0;
		$totalincomeannualval = 0;
		$lastyearincome = 0;
		$lastyearexpenditure = 0;
		
		//calculate details for budgetforecastsubcategory records
		$getcategories = mysql_query("select * from financesubcategory where financesubcategory.disabled = 0");
		while($row9=mysql_fetch_array($getcategories)){
			$financesubcategoryid = $row9['financesubcategoryid'];
			$financesubcategoryname = $row9['financesubcategoryname'];
			$financecategoryid = $row9['financecategoryid'];
			$budgetforecastsubcategoryname = "Budget Forecast: ".$businessunitname.": ".$financesubcategoryname;
			
			//add budgetforecastsubcategory records
			$addrecord = mysql_query("insert into budgetforecastsubcategory (budgetforecastsubcategoryname, financialbudgetperiodid, 
			financesubcategoryid, budgetforecastid, disabled, datecreated, masteronly) values ('$budgetforecastsubcategoryname', 
			'$financialbudgetperiodid', '$financesubcategoryid', '$budgetforecastid', '0', '$date', '0')");
			$budgetforecastsubcategoryid = mysql_insert_id();
			
			//calculate last years income/spend
			$getvalue = mysql_query("select sum(amount) as 'annualval' from financialtransaction 
			where financialbudgetperiodid = '$previousfinancialbudgetperiodid' and financesubcategoryid = '$financesubcategoryid'
			and businessunitid = '$businessunitid' and financialtransactionstatusid >= '3'");
			$getvaluerow = mysql_fetch_array($getvalue);
			$annualval = $getvaluerow['annualval'];
			//echo $financesubcategoryname.": ".$annualval." - ";
			
			//work out annual value if the previous budget period is not completely over
			$annualval = $annualval / ( 365 - $noofdays ) * 365;
			//echo $annualval." - ";
			
			//apply costincreasepercentage to expenditure records
			if($financecategoryid == 2){			
				$lastyearexpenditure = $lastyearexpenditure + $annualval;
				$annualval = $annualval / 100 * (100 + $costincreasepercentage);
				$annualval = round($annualval, 0);			
				$totalexpenditureannualval = $totalexpenditureannualval + $annualval;
			}
			
			//apply targetgrowthpercentage to income records 
			if($financecategoryid == 1){
				$lastyearincome = $lastyearincome + $annualval;
				$annualval = $annualval / 100 * (100 + $targetgrowthpercentage);	
				$annualval = round($annualval, 0);			
				$totalincomeannualval = $totalincomeannualval + $annualval;
			}
			
			//round final value to nearest whole value
			$annualval = round($annualval, 0);			
			//echo $annualval."<br/>";
			
			//create budgetforecastsubcategoryannually record
			if($financialbudgetperiodfrequencyname == "Annually"){
				$addrecord = mysql_query("insert into budgetforecastsubcategoryannually (budgetforecastsubcategoryannuallyname, 
				budgetforecastsubcategoryid, annualval, disabled, datecreated, masteronly) values ('$budgetforecastsubcategoryname', 
				'$budgetforecastsubcategoryid', '$annualval', '0', '$date', '0')");			
			}
			if($financialbudgetperiodfrequencyname == "Quarterly"){
				$quarterval = round($annualval / 4, 0);
				$addrecord = mysql_query("insert into budgetforecastsubcategoryquarterly (budgetforecastsubcategoryquarterlyname, 
				budgetforecastsubcategoryid, quarter1val, quarter2val, quarter3val, quarter4val, disabled, datecreated, masteronly) 
				values ('$budgetforecastsubcategoryname', '$budgetforecastsubcategoryid', 
				'$quarterval', '$quarterval', '$quarterval', '$quarterval', '0', '$date', '0')");			
			}
			if($financialbudgetperiodfrequencyname == "Monthly"){
				$monthval = round($annualval / 12, 0);
				$addrecord = mysql_query("insert into budgetforecastsubcategorymonthly (budgetforecastsubcategorymonthlyname, 
				budgetforecastsubcategoryid, month1val, month2val, month3val, month4val, month5val, month6val, month7val, month8val, 
				month9val, month10val, month11val, month12val, disabled, datecreated, masteronly) values ('$budgetforecastsubcategoryname', 
				'$budgetforecastsubcategoryid', '$monthval', '$monthval', '$monthval', '$monthval', '$monthval', '$monthval', '$monthval', 
				'$monthval', '$monthval', '$monthval', '$monthval', '$monthval', '0', '$date', '0')");			
			}
		}
		
		//update budgetforecast values
		$lastyearincome = round($lastyearincome, 0);
		$lastyearexpenditure = round($lastyearexpenditure, 0);
		$gettotalincome = mysql_query("update budgetforecast set totalincome = '$totalincomeannualval', 
		totalexpenditure = '$totalexpenditureannualval', lastyearprojectedincome = '$lastyearincome', 
		lastyearprojectedexpenditure = '$lastyearexpenditure' where budgetforecastid = '$budgetforecastid'");
	}
	
	//update activatebudgetstatus setting in budget period
	$update = mysql_query("update financialbudgetperiod set activatebudgetsetup = 1 
	where financialbudgetperiodid = '$financialbudgetperiodid'");
}
	
//work out users role
$budgetadmin = 0;
$roles = $_SESSION['userrole'];
$array = explode(', ', $roles); 
foreach($array as $value){
	if($value == 15 || $value == 16 || $value == 17 || $value == 18){
		$budgetadmin = 1;
	}
	else {
		$budgetadmin = 0;	
	}
}

//get number of budget years to display
$getconfig = mysql_query("select * from $database.onefinanceconfiguration where disabled = 0");
$getconfigrow = mysql_fetch_array($getconfig);
$budgetmanagernumyearsback = $getconfigrow['budgetmanagernumyearsback'];
$str = "-".$budgetmanagernumyearsback." years";
$dateadjust = date('Y-m-d', strtotime($str));

//select budget-budgetperiod
//get budget periods
$querytype = "select financialbudgetperiodid, concat(financialbudgettypename,' - ',financialbudgetname,' - ',financialbudgetperiodname)
as 'financialbudgetperiodname' from $database.financialbudgetperiod 
inner join $database.financialbudget on financialbudget.financialbudgetid = financialbudgetperiod.financialbudgetid
inner join $database.financialbudgettype on financialbudgettype.financialbudgettypeid = financialbudget.financialbudgettypeid
where financialbudgetperiod.disabled = 0 and financialbudgetperiod.enddate >= '$dateadjust' 
order by enddate desc";		    		  	
$resulttype = mysql_query($querytype);
?>
<div class="form-group">
<label class="col-xs-12 col-sm-2 col-md-2 col-lg-1 control-label"><?php echo $langval19 ?></label>			
<div class="col-xs-12 col-md-5 col-sm-5 col-lg-5">
<select class='form-control' name='action' id='action' onchange='redirectMe(this);'>
<?php				
echo "<option value='view.php?viewid=12&financialbudgetperiodid=0'>".$langval19."</option>";
while($nttype=mysql_fetch_array($resulttype)){
	if($financialbudgetperiodid == $nttype['financialbudgetperiodid']){
		echo "<option value='view.php?viewid=12&financialbudgetperiodid=".$nttype['financialbudgetperiodid']."' selected='true'>".$nttype['financialbudgetperiodname']."</option>";	
	}
	else{
		echo "<option value='view.php?viewid=12&financialbudgetperiodid=".$nttype['financialbudgetperiodid']."'>".$nttype['financialbudgetperiodname']."</option>";	
	}
}
?>
</select>
</div>
</div>

<?php
echo "<br/><br/>";
//find out if this budget period is activated for budget setup yet
$getbudgetperiod = mysql_query("select * from financialbudgetperiod where financialbudgetperiodid = '$financialbudgetperiodid'");
if(mysql_num_rows($getbudgetperiod)>= 1){
	$getbudgetperiodrow = mysql_fetch_array($getbudgetperiod);
	$activatebudgetsetup = $getbudgetperiodrow['activatebudgetsetup'];
	if($activatebudgetsetup == 0){
		echo "<p>".$langval101."</p>";
		?>
		<form class="form-horizontal" action="view.php?viewid=12&financialbudgetperiodid=<?php echo $financialbudgetperiodid ?>" method="post" enctype="multipart/form-data">
		<div class="col-xs-12 col-md-7 col-sm-8 col-lg-6">
			<div class="form-group">
		    	<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
		    		<div>
		      		<button type='submit' name='submit' value='Submit' class="button-primary"><?php echo $langval102 ?></button>																									
		    		</div>
		    	</div>
		  	</div>
		</div>  	
		</form>	
		<br/><br/><br/>			
		
		<?php
	}
}
?>



