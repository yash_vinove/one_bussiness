<?php session_start(); ?>
<?php include('../config.php'); ?>
<?php include('../sessionconfig.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	
 	<title>Style Configurator</title>

 	<!-- Bootstrap -->
 	<link href="bootstrap-3.3.1/dist/css/bootstrap.css" rel="stylesheet">

 	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
 	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">   
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
	<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script type="text/javascript">
    function redirectMe (sel) {
        var url = sel[sel.selectedIndex].value;
        window.location = url;
    }
    </script>
    <link rel='stylesheet' type='text/css' href='../style.php' />
</head>
  
<body class='body'>

	<?php include_once ('../headerthree.php');	?>
	
	<?php
		$rowid = isset($_GET['rowid']) ? strip_tags($_GET['rowid']) : '';
	 ?>
	<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding:10px;">
		<!--search results header bar for desktop-->
		<div class='bodyheader'>
			<h3>View Material</h3>
		</div>
		<div class='bodycontent'>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			
			<?php

			$rowid = isset($_GET['rowid']) ? $_GET['rowid'] : '';
			
			//get material template content
			$gettemplate = mysql_query("select template.content as content from material
			inner join template on template.templateid = material.templateid
			where materialid = '$rowid'");
			$gettemplaterow = mysql_fetch_array($gettemplate);
			$content = $gettemplaterow['content'];
			
			
			
			//iterate through the tags replacing the content
			$getmaterialcontent = mysql_query("select * from materialcontent
			inner join materialcontenttype on materialcontenttype.materialcontenttypeid = materialcontent.materialcontenttypeid
			where materialid = '$rowid' and materialcontent.disabled = 0");
			while($rowcontent = mysql_fetch_array($getmaterialcontent)){
				//get key variables
				$materialcontenttypename = $rowcontent['materialcontenttypename'];			
				$templatetagname = $rowcontent['templatetagname'];			
				$text = $rowcontent['text'];			
				$linkurl = $rowcontent['linkurl'];			
				$linkname = $rowcontent['linkname'];			
				$structurefieldid = $rowcontent['structurefieldid'];			
				$imagelocation = $rowcontent['imagelocation'];		
				
				//if Text 
				if($materialcontenttypename == 'Text'){
					$texttofind = "<onecustomertext name='".$templatetagname."'></onecustomertext>";
					$content = str_replace($texttofind, $text, $content);
				}
				
				//if Image
				if($materialcontenttypename == 'Image'){
					$texttofind = "<onecustomerimage name='".$templatetagname."'></onecustomerimage>";
					$content = str_replace($texttofind, $imagelocation, $content);
				}
				
				//if URL
				if($materialcontenttypename == 'URL'){
					$texttofind = "<onecustomerurl name='".$templatetagname."'></onecustomerurl>";
					$newstring = "<a href='".$linkurl."' target='blank'>".$linkname."</a>";
					$content = str_replace($texttofind, $newstring, $content);
				}
				
				//if Merge Field
				if($materialcontenttypename == 'Merge Field'){
					$getstructurefielddetails = mysql_query("select * from structurefield where structurefieldid = '$structurefieldid'");
					$getstructurefielddetailsrow = mysql_fetch_array($getstructurefielddetails);
					$structurefieldname = $getstructurefielddetailsrow['structurefieldname'];
					$getsamplecustomer = mysql_query("select * from customer 
					where disabled = 0 and $structurefieldname <> ''");
					$getsamplecustomerrow = mysql_fetch_array($getsamplecustomer);
					$newstring = $getsamplecustomerrow[$structurefieldname];
					$texttofind = "<onecustomermergefield name='".$templatetagname."'></onecustomermergefield>";
					$content = str_replace($texttofind, $newstring, $content);
				}
				
				//if Unsubscribe
				if($materialcontenttypename == 'Unsubscribe'){
					$texttofind = "<onecustomerunsubscribe name='".$templatetagname."'></onecustomerunsubscribe>";
					$content = str_replace($texttofind, "unsubscribetobebuiltlater", $content);
				}
				
			
			}
			
			echo $content;
			?>
		</div>
			
					
					<br/><br/><br/>
				</div>
	  		</div>
		</div>
			
	</div>	
	<?php include_once ('../footer.php'); ?>
</body>
