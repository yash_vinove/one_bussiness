<?php

//LANGUAGE COLLECTION SECTION
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (203,204)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}

//get all records
$goals = mysql_query("select * from $database.websiteanalyticsgoal 
inner join $masterdatabase.websiteanalyticsimprovementtype on websiteanalyticsimprovementtype.websiteanalyticsimprovementtypeid
= websiteanalyticsgoal.websiteanalyticsimprovementtypeid
inner join $masterdatabase.websiteanalyticsmetric on websiteanalyticsmetric.websiteanalyticsmetricid = 
websiteanalyticsgoal.websiteanalyticsmetricid
inner join $masterdatabase.websiteanalyticsfrequency on websiteanalyticsfrequency.websiteanalyticsfrequencyid = 
websiteanalyticsgoal.websiteanalyticsfrequencyid
where websiteanalyticsgoal.disabled = '0'");
while($row33 = mysql_fetch_array($goals)){
	$goalid = $row33['websiteanalyticsgoalid'];
	$goalname = $row33['websiteanalyticsgoalname'];
	$websiteanalyticswebsiteid = $row33['websiteanalyticswebsiteid'];
	$improvementtypename = $row33['websiteanalyticsimprovementtypename'];
	$metricname = $row33['websiteanalyticsmetricname'];
	$targetamount = $row33['targetamount'];
	$frequencyname = $row33['websiteanalyticsfrequencyname'];
	$startdate = $row33['startdate'];
	$targetdate = $row33['targetdate'];
	$definetargetcityname = $row33['definetargetcityname'];
	$definetargetcountryname = $row33['definetargetcountryname'];
	$definetargetpagename = $row33['definetargetpagename'];
	$structurefieldidvalue = $row33['structurefieldidvalue'];
	$structurefieldidfilter = $row33['structurefieldidfilter'];
	$structurefieldiddate = $row33['structurefieldiddate'];
	$metricmultiplier = $row33['metricmultiplier'];
	$filtercontent = $row33['filtercontent'];
	
	//get metric tables	
	$getmetric = mysql_query("select * from $database.structurefield 
	inner join $database.structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
	where structurefieldid = '$structurefieldidvalue'");
	$getmetricrow = mysql_fetch_array($getmetric);
	$structurefieldnamemetric = $getmetricrow['structurefieldname'];
	$tablenamemetric = $getmetricrow['tablename'];
	
	$getfilter = mysql_query("select * from $database.structurefield 
	inner join $database.structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
	where structurefieldid = '$structurefieldidfilter'");
	$getfilterrow = mysql_fetch_array($getfilter);
	$structurefieldnamefilter = $getfilterrow['structurefieldname'];
	$tablenamefilter = $getfilterrow['tablename'];
	
	$getdate = mysql_query("select * from $database.structurefield 
	inner join $database.structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
	where structurefieldid = '$structurefieldiddate'");
	$getdaterow = mysql_fetch_array($getdate);
	$structurefieldnamedate = $getdaterow['structurefieldname'];
	$tablenamedate = $getdaterow['tablename'];
	
	//calculate needed dates
	$today = date("Y-m-d");	
	if($frequencyname == 'Monthly'){
		$targetdatebegin	 = date ("Y-m-d", strtotime("-1 month", strtotime($today)));
		$startdatebegin = date ("Y-m-d", strtotime("-1 month", strtotime($startdate)));
	}
	if($frequencyname == 'Quarterly'){
		$targetdatebegin	 = date ("Y-m-d", strtotime("-3 months", strtotime($today)));
		$startdatebegin = date ("Y-m-d", strtotime("-3 months", strtotime($startdate)));
	}
	$today = date("Y-m-d");		
	
	//replace filter variable if specific variable is needed
	if($structurefieldnamefilter <> ''){
		if($structurefieldnamefilter == 'websiteanalyticsgoogletrackpageoverviewname'){
			$filtercontent = $definetargetpagename;	
		}
		if($structurefieldnamefilter == 'country'){
			$filtercontent = $definetargetcountryname;	
		}
		if($structurefieldnamefilter == 'city'){
			$filtercontent = $definetargetcityname;	
		}
	}	
	
	//echo $goalid." - ".$websiteanalyticswebsiteid." - ".$goalname." - ".$frequencyname."<br/>";
	
	//get starting value
	$getstartval = "select $metricmultiplier($structurefieldnamemetric) as 'startval' from $database.$tablenamemetric
	inner join $database.websiteanalyticswebsitetracking on websiteanalyticswebsitetracking.websiteanalyticswebsitetrackingid = 
	$tablenamemetric.websiteanalyticswebsitetrackingid
	where websiteanalyticswebsitetracking.websiteanalyticswebsiteid = '$websiteanalyticswebsiteid'
	and $tablenamedate.$structurefieldnamedate >= '$startdatebegin' 
	and $tablenamedate.$structurefieldnamedate < '$startdate'";
	if($structurefieldnamefilter <> ''){
		$getstartval = $getstartval." and $tablenamefilter.$structurefieldnamefilter = '$filtercontent'";
	}	
	//echo $getstartval."<br/>";
	$getstartval = mysql_query($getstartval);
	$getstartvalrow = mysql_fetch_array($getstartval);
	$startval = $getstartvalrow['startval'];	
	if($startval == ""){
		$startval = 0;	
	}
	
	
	
	//get new value
	$getnewval = "select $metricmultiplier($structurefieldnamemetric) as 'newval' from $database.$tablenamemetric
	inner join $database.websiteanalyticswebsitetracking on websiteanalyticswebsitetracking.websiteanalyticswebsitetrackingid = 
	$tablenamemetric.websiteanalyticswebsitetrackingid
	where websiteanalyticswebsitetracking.websiteanalyticswebsiteid = '$websiteanalyticswebsiteid'
	and $tablenamedate.$structurefieldnamedate >= '$targetdatebegin' 
	and $tablenamedate.$structurefieldnamedate < '$today'";
	if($structurefieldnamefilter <> ''){
		$getnewval = $getnewval." and $tablenamefilter.$structurefieldnamefilter = '$filtercontent'";
	}	
	//echo $getnewval."<br/>";
	$getnewval = mysql_query($getnewval);
	$getnewvalrow = mysql_fetch_array($getnewval);
	$newval = $getnewvalrow['newval'];	
	if($newval == ""){
		$newval = 0;	
	}
	
	//echo "startval: ".$startval."<br/>";
	//echo "newval: ".$newval."<br/><br/>";
	
	//calculate % complete
	if($improvementtypename == "Increase"){
		if($startval == 0){
			$startval = $newval * 0.9;		
		}
		if($newval >= $targetamount){
			$perc = 100;		
		}
		else {
			if($startval >= $targetamount){
				$startval = $targetamount * 0.9;			
			}
			$perc = round((($newval - $startval)/($targetamount - $startval))*100, 0);
		}
	}
	if($improvementtypename == "Decrease"){
		if($startval == 0){
			$startval = $newval * 1.1;		
		}
		if($newval <= $targetamount){
			$perc = 100;		
		}
		else {
			$perc = round((($startval - $newval)/($startval - $targetamount))*100, 0);
		}		
	}
	if($perc < 0){
		$perc = 0;	
	}
	if($perc > 100){
		$perc = 100;	
	}
	$newval = round($newval, 0);
	$startval = round($startval, 0);
	
	//update percentage
	$updatepercentage = mysql_query("update $database.websiteanalyticsgoal set currentpercentage = '$perc',
	startvalue = '$startval', currentvalue = '$newval' 
	where websiteanalyticsgoalid = '$goalid'");
}



//add new goal
$pagename = $_SERVER['REQUEST_URI'];
$pagename = str_replace("&", "xxxxxxxxxx", $pagename);							
echo $langval203." <a href='pageadd.php?pagetype=websiteanalyticsgoal&pagename=".$pagename."'>".$langval204."</a><br/><br/>";																								
					    			
?>

