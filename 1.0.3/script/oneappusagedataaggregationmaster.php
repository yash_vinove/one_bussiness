<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$date = date('Y-m-d');
echo "<br/><br/>";

if($masterdatabase == "andrewnorth_onebusinessmaster"){
	$prefix = "andrewnorth_";
}
else {
	$prefix = "";
}

$businessdatabasearray = array();


$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$gettenant = mysql_query("select * from $masterdatabase.tenant");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get list of tenants: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
while ($rowtenant = mysql_fetch_array($gettenant)){
	$tenantid = $rowtenant['tenantid'];
	$tenantdatabase = $prefix."onebusinesstenant".$tenantid;	
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getbusinesses = mysql_query("select * from $tenantdatabase.business");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get list of businesses: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while ($rowbusiness = mysql_fetch_array($getbusinesses)){
		$databasename = $rowbusiness['databasename'];
		array_push($businessdatabasearray, $databasename);	
	}
}

echo "<br/><br/>businessdatabasearray: ".json_encode($businessdatabasearray);

//get overall stats
$overallstatarray = array();
foreach($businessdatabasearray as $db1){
	$checktable = mysql_query("SELECT table_name
	FROM information_schema.tables
	WHERE table_schema = '$db1'
	AND table_name = 'appmanagerappstat'");
	$numrows = mysql_num_rows($checktable);
	if($numrows >= 1){
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$getstats = mysql_query("select * from $db1.appmanagerappstat");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Get overall stats: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		while($row12 = mysql_fetch_array($getstats)){
			$appsetupapplicationid = $row12['appsetupapplicationid'];
			$totaldatastorage = $row12['totaldatastorage'];
			$totalrecords = $row12['totalrecords'];
			$totalnotimesaccessedinlastthirtydays = $row12['totalnotimesaccessedinlastthirtydays'];
			$totalscriptexecutiontimeinlasttendays = $row12['totalscriptexecutiontimeinlasttendays'];
			$totalscriptnumberrunsinlasttendays = $row12['totalscriptnumberrunsinlasttendays'];
			$totalscriptnosqlqueriesinlasttendays = $row12['totalscriptnosqlqueriesinlasttendays'];
			$totalscriptsqlqueryruntimeinlasttendays = $row12['totalscriptsqlqueryruntimeinlasttendays'];
			array_push($overallstatarray, array("db1"=>$db1, "appsetupapplicationid"=>$appsetupapplicationid, 
			"totaldatastorage"=>$totaldatastorage, "totalrecords"=>$totalrecords, 
			"totalnotimesaccessedinlastthirtydays"=>$totalnotimesaccessedinlastthirtydays,
			"totalscriptexecutiontimeinlasttendays"=>$totalscriptexecutiontimeinlasttendays, 
			"totalscriptnumberrunsinlasttendays"=>$totalscriptnumberrunsinlasttendays, 
			"totalscriptnosqlqueriesinlasttendays"=>$totalscriptnosqlqueriesinlasttendays, 
			"totalscriptsqlqueryruntimeinlasttendays"=>$totalscriptsqlqueryruntimeinlasttendays));
		}
	}
}
echo "<br/><br/>overallstatarray: ".json_encode($overallstatarray);


//get total list of apps
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$getapps = mysql_query("select * from $masterdatabase.appsetupapplication");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get list of tenants: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
$overallstatarray2 = array();
while ($row13 = mysql_fetch_array($getapps)){
	$totaldatastorage = 0;
	$totalrecords = 0;
	$totalnotimesaccessedinlastthirtydays = 0;
	$totalscriptexecutiontimeinlasttendays = 0;
	$totalscriptnumberrunsinlasttendays = 0;
	$totalscriptnosqlqueriesinlasttendays = 0;
	$totalscriptsqlqueryruntimeinlasttendays = 0;
	$i = 0;
	$appsetupapplicationid = $row13['appsetupapplicationid'];
	$appsetupapplicationname = $row13['appsetupapplicationname'];
	foreach($overallstatarray as $item){
		$asaid = $item['appsetupapplicationid'];
		if($asaid == $appsetupapplicationid){
			$i = $i + 1;
			$newtotaldatastorage = $item['totaldatastorage'];		
			$newtotalrecords = $item['totalrecords'];		
			$newtotalnotimesaccessedinlastthirtydays = $item['totalnotimesaccessedinlastthirtydays'];		
			$newtotalscriptexecutiontimeinlasttendays = $item['totalscriptexecutiontimeinlasttendays'];		
			$newtotalscriptnumberrunsinlasttendays = $item['totalscriptnumberrunsinlasttendays'];		
			$newtotalscriptnosqlqueriesinlasttendays = $item['totalscriptnosqlqueriesinlasttendays'];		
			$newtotalscriptsqlqueryruntimeinlasttendays = $item['totalscriptsqlqueryruntimeinlasttendays'];		
			$totaldatastorage = $totaldatastorage + $newtotaldatastorage;
			$totalrecords = $totalrecords + $newtotalrecords;
			$totalnotimesaccessedinlastthirtydays = $totalnotimesaccessedinlastthirtydays + $newtotalnotimesaccessedinlastthirtydays;
			$totalscriptexecutiontimeinlasttendays = $totalscriptexecutiontimeinlasttendays + $newtotalscriptexecutiontimeinlasttendays;
			$totalscriptnumberrunsinlasttendays = $totalscriptnumberrunsinlasttendays + $newtotalscriptnumberrunsinlasttendays;
			$totalscriptnosqlqueriesinlasttendays = $totalscriptnosqlqueriesinlasttendays + $newtotalscriptnosqlqueriesinlasttendays;
			$totalscriptsqlqueryruntimeinlasttendays = $totalscriptsqlqueryruntimeinlasttendays + $newtotalscriptsqlqueryruntimeinlasttendays;
		}	
	}
	if($appsetupapplicationid >= 3){
			array_push($overallstatarray2, array("appsetupapplicationid"=>$appsetupapplicationid, 
			"totaldatastorage"=>$totaldatastorage, 
			"totalrecords"=>$totalrecords, "totalnoappinstalls"=>$i,
			"totalnotimesaccessedinlastthirtydays"=>$totalnotimesaccessedinlastthirtydays, 
			"totalscriptexecutiontimeinlasttendays"=>$totalscriptexecutiontimeinlasttendays, 
			"totalscriptnumberrunsinlasttendays"=>$totalscriptnumberrunsinlasttendays, 
			"totalscriptnosqlqueriesinlasttendays"=>$totalscriptnosqlqueriesinlasttendays, 
			"totalscriptsqlqueryruntimeinlasttendays"=>$totalscriptsqlqueryruntimeinlasttendays));	
		}
}

echo "<br/><br/>overallstatarray2: ".json_encode($overallstatarray2);


//save records to database
echo "<br/><br/>";
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$delete = mysql_query("delete from appmanageroverallappusage ");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Delete query ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;	
$insertquery = "insert into appmanageroverallappusage (appmanageroverallappusagename, appsetupapplicationid, 
totaldatastorage, totalrecords, totalnotimesaccessedinlastthirtydays, totalnoappinstalls, totalscriptexecutiontimeinlasttendays, 
totalscriptnumberrunsinlasttendays, totalscriptnosqlqueriesinlasttendays, totalscriptsqlqueryruntimeinlasttendays, 
datecreated, masteronly, disabled) values ";
$i = 0;
foreach($overallstatarray2 as $stat){
	$i = $i + 1;
	$appsetupapplicationid = $stat['appsetupapplicationid'];
	$totaldatastorage = $stat['totaldatastorage'];
	$totalrecords = $stat['totalrecords'];
	$totalnoappinstalls = $stat['totalnoappinstalls'];
	$totalnotimesaccessedinlastthirtydays = $stat['totalnotimesaccessedinlastthirtydays'];
	$totalscriptexecutiontimeinlasttendays = $stat['totalscriptexecutiontimeinlasttendays'];
	$totalscriptnumberrunsinlasttendays = $stat['totalscriptnumberrunsinlasttendays'];
	$totalscriptnosqlqueriesinlasttendays = $stat['totalscriptnosqlqueriesinlasttendays'];
	$totalscriptsqlqueryruntimeinlasttendays = $stat['totalscriptsqlqueryruntimeinlasttendays'];
	$insertquery = $insertquery."('Overall Stat', '$appsetupapplicationid', '$totaldatastorage', '$totalrecords', 
	'$totalnotimesaccessedinlastthirtydays', '$totalnoappinstalls', '$totalscriptexecutiontimeinlasttendays', 
	'$totalscriptnumberrunsinlasttendays', '$totalscriptnosqlqueriesinlasttendays', '$totalscriptsqlqueryruntimeinlasttendays', 
	'$date', '0', '0'),";
}
if($i >= 1){
	$insertquery = rtrim($insertquery, ",");
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$insertquery = mysql_query($insertquery);
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Insert query: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;	
}


echo "<br/><br/>";
?>