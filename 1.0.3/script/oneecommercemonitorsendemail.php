<?php

set_time_limit (500);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Sending email for any ecommercemonitortypeconfig record

// Collecting records from ecommercemonitortypeconfig table
$ecommercemonitortypeconfigrecords = mysql_query("
select ecommercemonitortypeconfigid,ecommercemonitortypeconfigname,alertemailaddresses,lastemailsent,
ecommercemonitoremailfrequency.ecommercemonitoremailfrequencyname as emailfrequencyname from ecommercemonitortypeconfig
inner join $masterdatabase.ecommercemonitoremailfrequency on ecommercemonitoremailfrequency.ecommercemonitoremailfrequencyid 
= ecommercemonitortypeconfig.ecommercemonitoremailfrequencyid 
where ecommercemonitortypeconfig.ecommercemonitoremailfrequencyid > 1 
and alertemailaddresses <> '' and ecommercemonitortypeconfig.disabled = 0
");

while($row3 = mysql_fetch_array($ecommercemonitortypeconfigrecords))
{
    $configid = $row3['ecommercemonitortypeconfigid'];
    $configname = $row3['ecommercemonitortypeconfigname'];
    $emailfrequency = $row3['emailfrequencyname'];
    $emailaddresses = $row3['alertemailaddresses'];
    $lastemailsent = $row3['lastemailsent'];
    $currentdatetime = date('Y-m-d h:i:s');

    // Calculating time diff in seconds for calculation of daily/weekly/monthly
    $timediff = strtotime($currentdatetime) - strtotime($lastemailsent);

    echo "<br/><br/> Email Frequency Name :".$emailfrequency;
    echo "<br/> Email Addresses :".$emailaddresses;
    echo "<br/> Last Email Sent DateTime :".$lastemailsent;
    echo "<br/> Current DateTime :".$currentdatetime;
    echo "<br/> TimeDiff :".$timediff;


    if($emailfrequency == 'Daily' && $timediff >= 86400)
    {
        sendemail($configid, $configname, $emailaddresses);
    }
    elseif($emailfrequency == 'Weekly' && $timediff >= 604800)
    {
        sendemail($configid, $configname, $emailaddresses);
    }
    elseif($emailfrequency == 'Monthly' && $timediff >= 2592000)
    {
        sendemail($configid, $configname, $emailaddresses);
    }
    elseif($emailfrequency == 'Instant')
    {
        sendemail($configid, $configname, $emailaddresses);
    }
    // This else block will hold true for 'Never' & any other value
    else
    {
        echo "<br/>No need of sending any email!<br/>";
    }

    echo "<br/> Updating ecommercemonitortypeconfig table with lastemailsent";
    $sql3 = mysql_query("update ecommercemonitortypeconfig set lastemailsent = '$currentdatetime' where ecommercemonitortypeconfigid = $configid");
}

function sendemail($configid, $configname, $emailaddresses) {
    // collecting records from ecommercemonitorissue table
    $sql1 = mysql_query("
        select productid,ecommercelistingid,ecommercelistinglocationid, productstockitemid,ecommercemonitorissuename from ecommercemonitorissue
        where ecommercemonitorissue.ecommercemonitortypeconfigid = $configid
    ");
		//REMOVED 06/07/2020:    and ecommercemonitorissue.emailed = 0
		
		
    //$productidarray = array();
    $idarray = array();
    $typearray = array();

    $issuenamearray = array();

    while($row4 = mysql_fetch_array($sql1)) {
        if($row4['productid'] != 0)
        {
            array_push($idarray, $row4['productid']);
            array_push($typearray, 'ProductID');

        }
        if($row4['ecommercelistingid'] != 0)
        {
            array_push($idarray, $row4['ecommercelistingid']);
            array_push($typearray, 'EcommerceListingID');
        }
        if($row4['ecommercelistinglocationid'] != 0)
        {
            array_push($idarray, $row4['ecommercelistinglocationid']);
            array_push($typearray, 'EcommerceListingLocationID');

        }
        if($row4['productstockitemid'] != 0)
        {
            array_push($idarray, $row4['productstockitemid']);
            array_push($typearray, 'ProductStockItemID');
        }
        //array_push($productidarray, $row4['productid']);
        array_push($issuenamearray, $row4['ecommercemonitorissuename']);
    }

    if(count($idarray) > 0)
    {
        $fromemail = 'alerts@onebusiness.com';
        $fromname = 'OneBusiness';
        $subject = 'Ecommerce Monitor New Alerts - '.$configname;

        $body = "Dear Administrator <br><br> There are new alerts for $configname. <br><br> 
        The following issues have been found: <br><br>"; 
        for($i=0;$i<count($idarray);$i++)
        {
            $body = $body.$typearray[$i]." - ".$idarray[$i]." - ".$issuenamearray[$i]." <br><br>";
        }
        
        $body = $body."Please login to see full details. <br><br> Many Thanks <br> OneBusiness Administration";

        echo "<br/>Body :".$body;

        //send email
        require_once('../1.0.3/PHPMailer/class.phpmailer.php');
        //require_once('../1.0.3/PHPMailer/class.smtp.php');

        $array = explode(',', $emailaddresses); 
        foreach($array as $value) {
            $value = trim($value);
            $email = new PHPMailer();

            $email->isHTML(TRUE);
            $email->From      = $fromemail;
            $email->FromName  = $fromname;
            $email->Subject   = $subject;
            $email->Body      = $body;
            $email->addAddress( $value );
            if(!$email->send()) 
            {
                echo "<br/>Mailer Error: " . $email->ErrorInfo;
            } 
            else 
            {
                echo "<br/>Message has been sent successfully";

                $sql2 = mysql_query("
                    update ecommercemonitorissue set emailed = 1
                    where ecommercemonitorissue.ecommercemonitortypeconfigid = $configid
                ");
            }
        }
    }
}

echo "<br/><br/>";

?>