<?php

$date = date("Y-m-d");

//find out what business units the logic needs to be run for
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$getbusinessunit = mysql_query("select businessunitid from $database.ecommercelisting group by businessunitid");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get list of businessunits to check: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
while($row45=mysql_fetch_array($getbusinessunit)){
	$businessunitid = $row45['businessunitid'];
	
	//PROFIT MARGIN OPTIMISATION
	//PROFIT MARGIN OPTIMISATION
	//PROFIT MARGIN OPTIMISATION
	//check where 'basic' pricing approach or 'ruleengine' pricing approach for profit optimisation
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$checkruleengine = mysql_query("select * from $database.ecommercesiteconfig
	where businessunitid = '$businessunitid' and enabletargetmargin = 1");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Check if siteconfig has target margin set to 1: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	if(mysql_num_rows($checkruleengine) >= 1){
		$checkruleengine = mysql_query("select * from $database.ecommerceruleengine
		where businessunitid = '$businessunitid'");
		$ruleenginerow = mysql_fetch_array($checkruleengine);
		
		//PROCESS 'RULEENGINE' APPROACH FOR PROFIT OPTIMISATION
		if(mysql_num_rows($checkruleengine) >= 1){
			$targetprofitpercentage = $ruleenginerow['targetprofitpercentage'];
			echo $businessunitid." - ";
			echo $targetprofitpercentage."<br/>";		
			
			//get the start and end date of the current month
			$startdate = date("Y-m-01");
			$enddate = date("Y-m-t", strtotime($startdate));
	
			//get current overall margin for the month
			$getprofit = mysql_query("select (((sum(sellprice)/sum(buyprice))-1)*100) as 'CurrentProfit' from $database.productstockitem 
			where businessunitid = '$businessunitid' and productstockitemstatusid = 2 
			and selldate <= '$enddate' and selldate >= '$startdate'");
			$row98 = mysql_fetch_array($getprofit);
			$currentprofit = round($row98['CurrentProfit'], 0);
			//echo $currentprofit."<br/>";	
	
			//see whether we're above or below profit
			$adjustprofit = $targetprofitpercentage - $currentprofit;
			//if above profit margin then relist all paused records in order to maximise product sales
			if($adjustprofit < 0){
				//echo $adjustprofit."<br/>";
				//reinstate any records that are Paused
				$update = mysql_query("update $database.ecommercelistinglocation 
				set ecommercelistinglocationstatusid = '8'
				where businessunitid = '$businessunitid' and ecommercelistinglocationstatusid = '4'");
				
				//there is another script that pauses the records within each integration
				//name of script is oneecommercechangelistingstatus.php
			}
			//if below profit margin then pause any products that will not achieve the profit margin
			else {
				//calculate the new target profit margin based on 2x the difference between current margin and target margin
				$adjustprofit = $targetprofitpercentage + ($adjustprofit * 2);			
				echo $adjustprofit."<br/>";	
				//pause any records that are underperforming against the profit %
				$getrecords = "select ecommercelistinglocationid, (((sum(listingprice)/sum(buyprice))-1)*100) as 'CurrentRecProfit' 
				from $database.ecommercelistinglocation
				inner join $database.productstockitem on productstockitem.productstockitemid = ecommercelistinglocation.productstockitemid
				where ecommercelistinglocationstatusid in (2,3,8)
				group by ecommercelistinglocationid";	
				//echo $getrecords;
				$getrecords = mysql_query($getrecords);
				while($row51=mysql_fetch_array($getrecords)){
					$ecommercelistinglocationid = $row51['ecommercelistinglocationid'];
					$currentrecprofit = $row51['CurrentRecProfit'];
					//echo "<br/>".$ecommercelistinglocationid." - ".$currentrecprofit;
					if($currentrecprofit <= $adjustprofit){
						//echo " - updated";
						//update any underperforming records to paused
						$update = mysql_query("update $database.ecommercelistinglocation 
						set ecommercelistinglocationstatusid = '4'
						where ecommercelistinglocationid = '$ecommercelistinglocationid'");
					}
				}					
			}		
		}
	}
	else {	
	
		//NO SALES MARGIN OPTIMISATION SO CHECK SALES VALUE INSTEAD
		//SALES VALUE OPTIMISATION
		//SALES VALUE OPTIMISATION
		//SALES VALUE OPTIMISATION
		//check for sales value optimisation
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$checkruleengine = "select * from ecommercesiteconfig
		where ecommercesiteconfig.businessunitid = '$businessunitid' and enabletargetsales = 1";
		echo "<br/>".$checkruleengine;
		$checkruleengine = mysql_query($checkruleengine);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Check if siteconfig has target sales set to 1: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		if(mysql_num_rows($checkruleengine) >= 1){
			$checkruleenginerow = mysql_fetch_array($checkruleengine);
			$currenttargetprofitpercentage = $checkruleenginerow['targetprofitpercentage'];
			$ecommercesiteconfigid = $checkruleenginerow['ecommercesiteconfigid'];
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$getruleengine = "select * from $database.ecommerceruleengine
			inner join $masterdatabase.ecommerceruleenginetargetsalesfrequency on 
			ecommerceruleenginetargetsalesfrequency.ecommerceruleenginetargetsalesfrequencyid = 
			ecommerceruleengine.ecommerceruleenginetargetsalesfrequencyid
			where businessunitid = '$businessunitid' limit 1";
			echo "<br/><br/>getruleengine: ".$getruleengine;
			$getruleengine = mysql_query($getruleengine);
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Get settings for target sales: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			while($row33 = mysql_fetch_array($getruleengine)){
				$ecommerceruleengineid = $row33['ecommerceruleengineid'];		
				$targetsalesvalue = $row33['targetsalesvalue'];		
				$targetsalesminimummarginpercentage = $row33['targetsalesminimummarginpercentage'];		
				$targetsalesmaximummarginpercentage = $row33['targetsalesmaximummarginpercentage'];		
				$ecommerceruleenginetargetsalesfrequencyname = $row33['ecommerceruleenginetargetsalesfrequencyname'];		
				$targetsalesnextfrequencyresetdate = $row33['targetsalesnextfrequencyresetdate'];	
				echo "<br/><br/><b>".$businessunitid." - TARGET SALES VALUE</b>";	
				echo "<br/>targetsalesvalue: ".$targetsalesvalue;
				echo "<br/>currenttargetprofitpercentage: ".$currenttargetprofitpercentage;
				echo "<br/>targetsalesminimummarginpercentage: ".$targetsalesminimummarginpercentage;
				echo "<br/>targetsalesmaximummarginpercentage: ".$targetsalesmaximummarginpercentage;
				echo "<br/>ecommerceruleenginetargetsalesfrequencyname: ".$ecommerceruleenginetargetsalesfrequencyname;
				if($targetsalesnextfrequencyresetdate == "0000-00-00" || $targetsalesnextfrequencyresetdate == ""){
					$targetsalesnextfrequencyresetdate = $date;				
				}
				
				//calculate dates for the period
				if($ecommerceruleenginetargetsalesfrequencyname == "Weekly"){
					$startdate = date('Y-m-d',strtotime('this monday -7 days'));
					$enddate   = date('Y-m-d',strtotime('this monday -1 days'));							
				}	
				if($ecommerceruleenginetargetsalesfrequencyname == "Monthly"){
					$startdate = date('Y-m-01');
					$enddate   = date("Y-m-t", strtotime($startdate));						
				}
				if($ecommerceruleenginetargetsalesfrequencyname == "Quarterly"){
					$current_quarter = ceil(date('n') / 3);
					$startdate = date('Y-m-d', strtotime(date('Y') . '-' . (($current_quarter * 3) - 2) . '-1'));
					$enddate = date('Y-m-t', strtotime(date('Y') . '-' . (($current_quarter * 3)) . '-1'));			
				}	
				$targetsalesnextfrequencyresetdate = $enddate;	
				echo "<br/>targetsalesnextfrequencyresetdate: ".$targetsalesnextfrequencyresetdate;
				echo "<br/>startdate: ".$startdate;
				echo "<br/>enddate: ".$enddate;
				
				//update nextresetdate
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$getruleengine = mysql_query("update ecommerceruleengine
				set targetsalesnextfrequencyresetdate = '$enddate' 
				where ecommerceruleengineid = '$ecommerceruleengineid'");
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				//echo "<br/><br/>Update ecommerceruleengine with next refresh date: ";
				//echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				//echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			
				//calculate number of days settings 
				$numberofdaysremaininginperiod = round((strtotime($enddate) - strtotime($date)) / (60 * 60 * 24));	
				$numberofdayscompletedinperiod = round((strtotime($date) - strtotime($startdate)) / (60 * 60 * 24));
				$numberofdaystotalinperiod	= round((strtotime($enddate) - strtotime($startdate)) / (60 * 60 * 24));
				echo "<br/><br/>numberofdaysremaininginperiod: ".$numberofdaysremaininginperiod;
				echo "<br/>numberofdayscompletedinperiod: ".$numberofdayscompletedinperiod;
				echo "<br/>numberofdaystotalinperiod: ".$numberofdaystotalinperiod;
				
				//get current sales amount for this period
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$getsales = "select sum(sellprice) as 'CurrentSales' from $database.productstockitem 
				where businessunitid = '$businessunitid' and productstockitemstatusid = 2 
				and selldate >= '$startdate' and selldate <= '$enddate'";
				//echo "<br/>".$getsales;
				$getsales = mysql_query($getsales);
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				//echo "<br/><br/>Get current sales for period: ";
				//echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				//echo "<br/>sqlqueriestime: ".$sqlqueriestime;
				if(mysql_num_rows($getsales) >= 1){
					$row98 = mysql_fetch_array($getsales);
					$currentsalesforperiod = round($row98['CurrentSales'], 2);
				}
				else {
					$currentsalesforperiod = 0;				
				}
				echo "<br/><br/>currentsalesforperiod: ".$currentsalesforperiod;
				
				//calculate whether to increase or decrease and by what %
				if($numberofdayscompletedinperiod >= 2 && 
				(($numberofdayscompletedinperiod % 2 == 0 && $ecommerceruleenginetargetsalesfrequencyname <> "Weekly")
				 || ($numberofdayscompletedinperiod % 1 == 0 && $ecommerceruleenginetargetsalesfrequencyname == "Weekly"))){
					$targetaveragesalesperday = round($targetsalesvalue / $numberofdaystotalinperiod, 2);
					$currentaveragesalesperday = round($currentsalesforperiod / $numberofdayscompletedinperiod, 2);
					$newtargetaveragesalesperday = round(($targetsalesvalue - $currentsalesforperiod) / $numberofdaysremaininginperiod, 2);
					echo "<br/><br/>targetaveragesalesperday: ".$targetaveragesalesperday;
					echo "<br/>currentaveragesalesperday: ".$currentaveragesalesperday;
					echo "<br/>newtargetaveragesalesperday: ".$newtargetaveragesalesperday;
					if($newtargetaveragesalesperday > $currentaveragesalesperday){
						$difference = round($currentaveragesalesperday / $newtargetaveragesalesperday, 2);
						echo "<br/>difference: ".$difference;	
						if($difference >= 0.5 || $difference == 0){
							$newtargetprofitpercentage = $currenttargetprofitpercentage	 - 5;		
						}	
						if($difference >= 0.3 && $difference < 0.5){
							$newtargetprofitpercentage = $currenttargetprofitpercentage	 - 3;		
						}
						if($difference >= 0.1 && $difference < 0.3){
							$newtargetprofitpercentage = $currenttargetprofitpercentage	 - 1;		
						}		
						echo "<br/>newtargetprofitpercentage: ".$newtargetprofitpercentage;		
					}
					else {
						$difference = round($currentaveragesalesperday / $newtargetaveragesalesperday, 2);
						echo "<br/>difference: ".$difference;	
						if($difference >= -0.5 || $difference == 0){
							$newtargetprofitpercentage = $currenttargetprofitpercentage	 + 5;		
						}	
						if($difference >= -0.3 && $difference < -0.5){
							$newtargetprofitpercentage = $currenttargetprofitpercentage	 + 3;		
						}
						if($difference >= 0 && $difference < -0.3){
							$newtargetprofitpercentage = $currenttargetprofitpercentage	 + 1;		
						}	
						echo "<br/>newtargetprofitpercentage: ".$newtargetprofitpercentage;
					}
					
					//check newtarget is not outside the boundaries of allowed margins
					if($newtargetprofitpercentage > $targetsalesmaximummarginpercentage){
						$newtargetprofitpercentage = $targetsalesmaximummarginpercentage;				
					}
					else {
						if($newtargetprofitpercentage < $targetsalesminimummarginpercentage)	{
							$newtargetprofitpercentage = $targetsalesminimummarginpercentage;				
						}				
					}
					echo "<br/>newtargetprofitpercentageadjusted: ".$newtargetprofitpercentage;
					
					//update the new value
					if($newtargetprofitpercentage <> $currenttargetprofitpercentage){
						$nosqlqueries = $nosqlqueries + 1;
						$sqlstarttime = microtime(true);
						$getsales = mysql_query("update ecommercesiteconfig 
						set targetprofitpercentage = '$newtargetprofitpercentage' 
						where ecommercesiteconfigid = '$ecommercesiteconfigid'");
						$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
						$differencemilliseconds = microtime(true) - $sqlstarttime;
						echo "<br/><br/>Update target profit margin: ";
						echo "<br/>differencemilliseconds: ".$differencemilliseconds;
						echo "<br/>sqlqueriestime: ".$sqlqueriestime;
					}
				}	
				else {
					echo "<br/>nothing to calculate today";				
				}			
				
				
			}
		}
		
		
	}
}

echo "<br/><br/>";
?>




