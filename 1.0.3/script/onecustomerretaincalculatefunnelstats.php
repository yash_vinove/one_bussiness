<?php

//get list of rules
$updatedisabled = mysql_query("update $database.customerretainfunnelstagecustomer 
set customerretainfunnelstagecustomer.disabled = 1");
$date = date('Y-m-d');
$getrules = "select noofdays, customerretainfunnelstageautoselectrulename, customerretainfunnelstage.customerretainfunnelid, 
customerretainfunnelstageautoselectrule.customerretainfunnelstageid, customerretainfunnelstageautoselectruletypename, 
loyaltylevel.loyaltylevelid, loyaltylevelname, audienceid, audiencename, customertype.customertypeid, customertypename, 
customerretainfunnelstageproductpurchasefrequencyname, productpurchaseminimumvalue, productpurchasemaximumvalue, 
productpurchaseminimumnumberofitems, productpurchasemaximumnumberofitems, loyaltyminimumnoofpoints, 
loyaltymaximumnoofpoints, loyaltyminimumnoofbadges, loyaltymaximumnoofbadges, loyaltymaximumnoofawards, 
loyaltyminimumnoofawards, loyaltyaward.loyaltyawardid, loyaltyawardname, loyaltybadge.loyaltybadgeid, loyaltybadgename
from $database.customerretainfunnelstageautoselectrule
inner join $database.customerretainfunnelstage on customerretainfunnelstage.customerretainfunnelstageid = customerretainfunnelstageautoselectrule.customerretainfunnelstageid
inner join $database.customerretainfunnelstageautoselectruletype on customerretainfunnelstageautoselectruletype.customerretainfunnelstageautoselectruletypeid 
= customerretainfunnelstageautoselectrule.customerretainfunnelstageautoselectruletypeid
left join $database.loyaltylevel on loyaltylevel.loyaltylevelid = customerretainfunnelstageautoselectrule.loyaltylevelid
left join $database.audience on audience.audienceid = customerretainfunnelstageautoselectrule.customeraudienceid
left join $database.customertype on customertype.customertypeid = customerretainfunnelstageautoselectrule.customertypeid
left join $database.loyaltybadge on loyaltybadge.loyaltybadgeid = customerretainfunnelstageautoselectrule.loyaltybadgeid
left join $database.loyaltyaward on loyaltyaward.loyaltyawardid = customerretainfunnelstageautoselectrule.loyaltyawardid
left join $masterdatabase.customerretainfunnelstageproductpurchasefrequency on 
customerretainfunnelstageproductpurchasefrequency.customerretainfunnelstageproductpurchasefrequencyid = 
customerretainfunnelstageautoselectrule.customerretainfunnelstageproductpurchasefrequencyid
where customerretainfunnelstageautoselectrule.disabled = 0 
order by customerretainfunnelstage.customerretainfunnelid asc, stageorder asc";
//echo $getrules;
$getrules = mysql_query($getrules);
while ($row55 = mysql_fetch_array($getrules)){
	$customerretainfunnelstageautoselectrulename = $row55['customerretainfunnelstageautoselectrulename'];
	$customerretainfunnelid = $row55['customerretainfunnelid'];
	$customerretainfunnelstageid = $row55['customerretainfunnelstageid'];
	$amountdays = $row55['noofdays'];
	$customerretainfunnelstageautoselectruletypename = $row55['customerretainfunnelstageautoselectruletypename'];
	//echo "<br/><br/><b>".$customerretainfunnelstageautoselectrulename."</b>";
	//echo "<br/>".$customerretainfunnelstageautoselectruletypename;
	
	//get all stageid's
	$getstages = mysql_query("select * from $database.customerretainfunnelstage
	where customerretainfunnelid = '$customerretainfunnelid' and disabled = 0");
	$funnelstageids = '';
	$i = 1;
	while($row11 = mysql_fetch_array($getstages)){
		$stageid = $row11['customerretainfunnelstageid'];
		if($i == 1){
			$funnelstageids = $stageid;
		}
		else {
			$funnelstageids = $funnelstageids.",".$stageid;
		}
		$i = $i + 1;
	}
	
	//Loyalty Programme - Club Level
	if($customerretainfunnelstageautoselectruletypename == "Loyalty Programme - Club Level"){
		$loyaltylevelid = $row55['loyaltylevelid'];
		$loyaltylevelname = $row55['loyaltylevelname'];
		//echo "<br/>".$loyaltylevelname;
		
		//get applicable customers
		if($loyaltylevelname <> ""){
			$getcustomers = mysql_query("select * from $database.loyaltylevelcustomer 
			where loyaltylevelid = '$loyaltylevelid'");
		}		
	}
	
	//Customer - Audience	
	if($customerretainfunnelstageautoselectruletypename == "Customer - Audience"){
		$audienceid = $row55['audienceid'];
		$audiencename = $row55['audiencename'];
		//echo "<br/>".$audiencename;
		
		//get applicable customers
		if($audiencename <> ""){
			$getcustomers = mysql_query("select * from $database.customeraudience 
			where audienceid = '$audienceid'");		
		}		
	}
	
	//Customer - Type	
	if($customerretainfunnelstageautoselectruletypename == "Customer - Type"){
		$customertypeid = $row55['customertypeid'];
		$customertypename = $row55['customertypename'];
		//echo "<br/>".$customertypename;
		
		//get applicable customers
		if($customertypename <> ""){
			$getcustomers = mysql_query("select * from $database.customer 
			where customertypeid = '$customertypeid'");		
		}		
	}
	
	//Product Purchase - Financial Value	
	if($customerretainfunnelstageautoselectruletypename == "Product Purchase - Financial Value"){
		$customerretainfunnelstageproductpurchasefrequencyname = $row55['customerretainfunnelstageproductpurchasefrequencyname'];
		$productpurchaseminimumvalue = $row55['productpurchaseminimumvalue'];
		$productpurchasemaximumvalue = $row55['productpurchasemaximumvalue'];
		//echo "<br/>".$customerretainfunnelstageproductpurchasefrequencyname;
		//echo "<br/>".$productpurchaseminimumvalue;
		//echo "<br/>".$productpurchasemaximumvalue;
		
		//get applicable customers
		if($customerretainfunnelstageproductpurchasefrequencyname <> ""){
			$getcustomers = "select customerid, 
			sum(purchasevalue / currencytohqcurrencycurrentfxrate) as hqamount from $database.customerpurchase
			inner join $database.currencyfxrate on currencyfxrate.currencyid = customerpurchase.currencyid
			where customerpurchase.disabled = 0";
			$k = "-".$amountdays." days";
			$datepurchased = date("Y-m-d", strtotime($date .$k));
			$getcustomers = $getcustomers." and datepurchased >= '".$datepurchased."'";	
			$getcustomers = $getcustomers." group by customerid";	
			if($productpurchaseminimumvalue >= 1 || $productpurchasemaximumvalue >= 1){
				$getcustomers = $getcustomers." having ";
			}
			if($productpurchaseminimumvalue >= 1){
				$getcustomers = $getcustomers." sum(purchasevalue / currencytohqcurrencycurrentfxrate) >= ".$productpurchaseminimumvalue; 
			}
			if($productpurchasemaximumvalue >= 1 && $productpurchaseminimumvalue >= 1){
				$getcustomers = $getcustomers." and sum(purchasevalue / currencytohqcurrencycurrentfxrate) <= ".$productpurchasemaximumvalue; 
			}
			if($productpurchasemaximumvalue >= 1 && $productpurchaseminimumvalue== 0){
				$getcustomers = $getcustomers." sum(purchasevalue / currencytohqcurrencycurrentfxrate) <= ".$productpurchasemaximumvalue; 
			}
			//echo $getcustomers;
			$getcustomers = mysql_query($getcustomers);	
		}		
	}
	
	//Product Purchase - Number of Items Purchased		
	if($customerretainfunnelstageautoselectruletypename == "Product Purchase - Number of Items Purchased"){
		$customerretainfunnelstageproductpurchasefrequencyname = $row55['customerretainfunnelstageproductpurchasefrequencyname'];
		$productpurchaseminimumnumberofitems = $row55['productpurchaseminimumnumberofitems'];
		$productpurchasemaximumnumberofitems = $row55['productpurchasemaximumnumberofitems'];
		//echo "<br/>".$customerretainfunnelstageproductpurchasefrequencyname;
		//echo "<br/>".$productpurchaseminimumnumberofitems;
		//echo "<br/>".$productpurchasemaximumnumberofitems;
		
		//get applicable customers
		if($customerretainfunnelstageproductpurchasefrequencyname <> ""){
			$getcustomers = "select customerid, 
			count(customerpurchaseid) as hqamount from $database.customerpurchase
			where customerpurchase.disabled = 0";
			$k = "-".$amountdays." days";
			$datepurchased = date("Y-m-d", strtotime($date .$k));
			$getcustomers = $getcustomers." and datepurchased >= '".$datepurchased."'";	
			$getcustomers = $getcustomers." group by customerid";	
			if($productpurchaseminimumnumberofitems >= 1 || $productpurchasemaximumnumberofitems >= 1){
				$getcustomers = $getcustomers." having ";
			}
			if($productpurchaseminimumnumberofitems >= 1){
				$getcustomers = $getcustomers." count(customerpurchaseid) >= ".$productpurchaseminimumnumberofitems; 
			}
			if($productpurchasemaximumnumberofitems >= 1 && $productpurchaseminimumnumberofitems >= 1){
				$getcustomers = $getcustomers." and count(customerpurchaseid) <= ".$productpurchasemaximumnumberofitems; 
			}
			if($productpurchasemaximumnumberofitems >= 1 && $productpurchaseminimumnumberofitems== 0){
				$getcustomers = $getcustomers." count(customerpurchaseid) <= ".$productpurchasemaximumnumberofitems; 
			}
			//echo $getcustomers;
			$getcustomers = mysql_query($getcustomers);	
		}		
	}
	
	//Loyalty Programme - Number of Points	
	if($customerretainfunnelstageautoselectruletypename == "Loyalty Programme - Number of Points"){
		$customerretainfunnelstageproductpurchasefrequencyname = $row55['customerretainfunnelstageproductpurchasefrequencyname'];
		$loyaltyminimumnoofpoints = $row55['loyaltyminimumnoofpoints'];
		$loyaltymaximumnoofpoints = $row55['loyaltymaximumnoofpoints'];
		//echo "<br/>".$customerretainfunnelstageproductpurchasefrequencyname;
		//echo "<br/>".$loyaltyminimumnoofpoints;
		//echo "<br/>".$loyaltymaximumnoofpoints;
		
		//get applicable customers
		if($customerretainfunnelstageproductpurchasefrequencyname <> ""){
			$getcustomers = "select customerid, 
			sum(pointsawarded) as hqamount from $database.loyaltypointcustomer
			where disabled = 0";
			$k = "-".$amountdays." days";
			$dateawarded = date("Y-m-d", strtotime($date .$k));
			$getcustomers = $getcustomers." and dateawarded >= '".$dateawarded."'";	
			$getcustomers = $getcustomers." group by customerid";	
			if($loyaltyminimumnoofpoints >= 1 || $loyaltymaximumnoofpoints >= 1){
				$getcustomers = $getcustomers." having ";
			}
			if($loyaltyminimumnoofpoints >= 1){
				$getcustomers = $getcustomers." sum(pointsawarded) >= ".$loyaltyminimumnoofpoints; 
			}
			if($loyaltymaximumnoofpoints >= 1 && $loyaltyminimumnoofpoints >= 1){
				$getcustomers = $getcustomers." and sum(pointsawarded) <= ".$loyaltymaximumnoofpoints; 
			}
			if($loyaltymaximumnoofpoints >= 1 && $loyaltyminimumnoofpoints== 0){
				$getcustomers = $getcustomers." sum(pointsawarded) <= ".$loyaltymaximumnoofpoints; 
			}
			//echo $getcustomers;
			$getcustomers = mysql_query($getcustomers);	
		}		
	}
	
	//Loyalty Programme - Number of Badges	
	if($customerretainfunnelstageautoselectruletypename == "Loyalty Programme - Number of Badges"){
		$customerretainfunnelstageproductpurchasefrequencyname = $row55['customerretainfunnelstageproductpurchasefrequencyname'];
		$loyaltyminimumnoofbadges = $row55['loyaltyminimumnoofbadges'];
		$loyaltymaximumnoofbadges = $row55['loyaltymaximumnoofbadges'];
		//echo "<br/>".$customerretainfunnelstageproductpurchasefrequencyname;
		//echo "<br/>".$loyaltyminimumnoofbadges;
		//echo "<br/>".$loyaltymaximumnoofbadges;
		
		//get applicable customers
		if($customerretainfunnelstageproductpurchasefrequencyname <> ""){
			$getcustomers = "select customerid, 
			count(loyaltybadgecustomerid) as hqamount from $database.loyaltybadgecustomer
			where disabled = 0";
			$k = "-".$amountdays." days";
			$dateawarded = date("Y-m-d", strtotime($date .$k));
			$getcustomers = $getcustomers." and dateawarded >= '".$dateawarded."'";	
			$getcustomers = $getcustomers." group by customerid";	
			if($loyaltyminimumnoofbadges >= 1 || $loyaltymaximumnoofbadges >= 1){
				$getcustomers = $getcustomers." having ";
			}
			if($loyaltyminimumnoofbadges >= 1){
				$getcustomers = $getcustomers." count(loyaltybadgecustomerid) >= ".$loyaltyminimumnoofbadges; 
			}
			if($loyaltymaximumnoofbadges >= 1 && $loyaltyminimumnoofbadges >= 1){
				$getcustomers = $getcustomers." and count(loyaltybadgecustomerid) <= ".$loyaltymaximumnoofbadges; 
			}
			if($loyaltymaximumnoofbadges >= 1 && $loyaltyminimumnoofbadges== 0){
				$getcustomers = $getcustomers." count(loyaltybadgecustomerid) <= ".$loyaltymaximumnoofbadges; 
			}
			//echo $getcustomers;
			$getcustomers = mysql_query($getcustomers);	
		}		
	}
	
	//Loyalty Programme - Number of Awards	
	if($customerretainfunnelstageautoselectruletypename == "Loyalty Programme - Number of Awards"){
		$customerretainfunnelstageproductpurchasefrequencyname = $row55['customerretainfunnelstageproductpurchasefrequencyname'];
		$loyaltyminimumnoofawards = $row55['loyaltyminimumnoofawards'];
		$loyaltymaximumnoofawards = $row55['loyaltymaximumnoofawards'];
		//echo "<br/>".$customerretainfunnelstageproductpurchasefrequencyname;
		//echo "<br/>".$loyaltyminimumnoofawards;
		//echo "<br/>".$loyaltymaximumnoofawards;
		
		//get applicable customers
		if($customerretainfunnelstageproductpurchasefrequencyname <> ""){
			$getcustomers = "select customerid, 
			count(loyaltyawardcustomerid) as hqamount from $database.loyaltyawardcustomer
			where disabled = 0";
			$k = "-".$amountdays." days";
			$dateawarded = date("Y-m-d", strtotime($date .$k));
			$getcustomers = $getcustomers." and dateawarded >= '".$dateawarded."'";	
			$getcustomers = $getcustomers." group by customerid";	
			if($loyaltyminimumnoofawards >= 1 || $loyaltymaximumnoofawards >= 1){
				$getcustomers = $getcustomers." having ";
			}
			if($loyaltyminimumnoofawards >= 1){
				$getcustomers = $getcustomers." count(loyaltyawardcustomerid) >= ".$loyaltyminimumnoofawards; 
			}
			if($loyaltymaximumnoofawards >= 1 && $loyaltyminimumnoofawards >= 1){
				$getcustomers = $getcustomers." and count(loyaltyawardcustomerid) <= ".$loyaltymaximumnoofawards; 
			}
			if($loyaltymaximumnoofawards >= 1 && $loyaltyminimumnoofawards== 0){
				$getcustomers = $getcustomers." count(loyaltyawardcustomerid) <= ".$loyaltymaximumnoofawards; 
			}
			//echo $getcustomers;
			$getcustomers = mysql_query($getcustomers);	
		}		
	}
	
	//Loyalty Programme - Specific Award	
	if($customerretainfunnelstageautoselectruletypename == "Loyalty Programme - Specific Award"){
		$loyaltyawardid = $row55['loyaltyawardid'];
		$loyaltyawardname = $row55['loyaltyawardname'];
		//echo "<br/>".$loyaltyawardname;
		
		//get applicable customers
		if($loyaltyawardname <> ""){
			$getcustomers = mysql_query("select * from $database.loyaltyawardcustomer 
			where loyaltyawardid = '$loyaltyawardid'");		
		}		
	}
	
	//Loyalty Programme - Specific Badge	
	if($customerretainfunnelstageautoselectruletypename == "Loyalty Programme - Specific Badge"){
		$loyaltybadgeid = $row55['loyaltybadgeid'];
		$loyaltybadgename = $row55['loyaltybadgename'];
		//echo "<br/>".$loyaltybadgename;
		
		//get applicable customers
		if($loyaltybadgename <> ""){
			$getcustomers = mysql_query("select * from $database.loyaltybadgecustomer 
			where loyaltybadgeid = '$loyaltybadgeid'");		
		}		
	}
	
	
	//Process the Customer check
	$p = 1;
	$customeridsaved = "";
	while($row44 = mysql_fetch_array($getcustomers)){
		$customerid = $row44['customerid'];
		//echo "<br/>".$customerid;
		
		//check existing record
		$addnew = 0;
		$checkexisting = "select * from $database.customerretainfunnelstagecustomer 
		where customerid = '$customerid' and customerretainfunnelstageid in ($funnelstageids)";
		$checkexisting = mysql_query($checkexisting);
		if(mysql_num_rows($checkexisting) >= 1){
			//delete record if needed
			$checkexistingrow = mysql_fetch_array($checkexisting);
			$customerretainfunnelstagecustomerid = $checkexistingrow['customerretainfunnelstagecustomerid'];
			$customerretainfunnelstageidexisting = $checkexistingrow['customerretainfunnelstageid'];
			if($customerretainfunnelstageidexisting <> $customerretainfunnelstageid){
				$deleterecord = mysql_query("delete from $database.customerretainfunnelstagecustomer 
				where customerid = '$customerid' and customerretainfunnelstageid in ($funnelstageids)");		
				$addnew = 1;			
			}
			else {
				$updatedisabled = mysql_query("update $database.customerretainfunnelstagecustomer 
				set customerretainfunnelstagecustomer.disabled = 0
				where customerretainfunnelstagecustomerid = '$customerretainfunnelstagecustomerid'");
			}			
		}
		else {
			$addnew = 1;
		}
		//add new record if needed
		if($addnew == 1){
			$date = date('Y-m-d');
			$addnew = mysql_query("insert into $database.customerretainfunnelstagecustomer (customerretainfunnelstagecustomername, 
			customerid, customerretainfunnelstageid, disabled, datecreated, masteronly) values ('$customerid', '$customerid', 
			'$customerretainfunnelstageid', '0', '$date', '0')");
		}
		
		//save customerid's
		if($p == 1){
			$customeridsaved = $customerid;
		}
		else {
			$customeridsaved = $customeridsaved.",".$customerid;
		}
	}
	
	
}

//delete any customers that need to be deleted
$deletedisabled = mysql_query("delete from $database.customerretainfunnelstagecustomer 
where disabled = 1");



//CALCULATE METRICS FOR RETENTION - OVERALL
//calculate retention
$datepurchased = date("Y-m-d", strtotime($date ."-365 days"));
$retained = mysql_query("select count(customerid) from $database.customerpurchase 
where customerpurchase.disabled = 0 and datepurchased >= '$datepurchased' group by customerid having count(customerid) >= 2");
$retainedcustomers = mysql_num_rows($retained);
$total = mysql_query("select count(customerid) from $database.customerpurchase 
where customerpurchase.disabled = 0 and datepurchased >= '$datepurchased' group by customerid");
$totalcustomers = mysql_num_rows($total);
if($retainedcustomers > 0 && $totalcustomers > 0){
	$overallretentionscore = round(($retainedcustomers / $totalcustomers)*100, 2);
}
else {
	$overallretentionscore = 0;
}

//calculate overalltrustscore
//TO BE BUILT LATER

//calculate overallnpsscore
//TO BE BUILT LATER

//calculate overalllifetimevalueaverage
$lva = mysql_query("select count(distinct customerid) as 'numcustomers', 
sum(purchasevalue / currencytohqcurrencycurrentfxrate) as 'totalpurchasevalue' 
from $database.customerpurchase
inner join $database.currencyfxrate on currencyfxrate.currencyid = customerpurchase.currencyid
where customerpurchase.disabled = 0");
$row77 = mysql_fetch_array($lva);
$numcustomers = $row77['numcustomers'];
$totalpurchasevalue = $row77['totalpurchasevalue'];
if($totalpurchasevalue > 0 && $numcustomers > 0){
	$overalllifetimevalueaverage = round($totalpurchasevalue / $numcustomers, 2);
}
else {
	$overalllifetimevalueaverage = 0;
}

//calculate overallnegativesentimentscore
//TO BE BUILT LATER

//calculate overallpositivesentimentscore
//TO BE BUILT LATER

//calculate overallattritionscore
$datepurchased2 = date("Y-m-d", strtotime($datepurchased ."-365 days"));
$getpreviouscustomers = mysql_query("select customerid from $database.customerpurchase
where customerpurchase.disabled = 0 and datepurchased < '$datepurchased' and datepurchased >= '$datepurchased2' 
group by customerid");
$repurchased = 0;
$attrition = 0;
while ($row11 = mysql_fetch_array($getpreviouscustomers)){
	$customerid = $row11['customerid'];
	//check if customer purchased something in last 12 months
	$checkcustomer = mysql_query("select customerid from $database.customerpurchase
	where customerpurchase.disabled = 0 and datepurchased >= '$datepurchased' and customerid = '$customerid'");
	$checkcustomercount = mysql_num_rows($checkcustomer);
	if($checkcustomercount >= 1){
		$repurchased = $repurchased + 1;
	}
	else {
		$attrition = $attrition + 1;	
	}
}
if($attrition > 0 || $repurchased > 0){
	$totalcustomers = $attrition + $repurchased;
	$overallattritionscore = round(($attrition / $totalcustomers) * 100, 2);
}
else {
	$overallattritionscore = 0;
}

//calculate overallrepurchaserate
$repurchaserate = mysql_query("select count(distinct customerid) as 'numcustomers', 
count(customerpurchaseid) as 'numberpurchases' 
from $database.customerpurchase
where customerpurchase.disabled = 0 and datepurchased >= '$datepurchased'");
$row78 = mysql_fetch_array($repurchaserate);
$numcustomers = $row78['numcustomers'];
$totalpurchasenumber = $row78['numberpurchases'];
if($totalpurchasenumber > 0 && $numcustomers > 0){
	$overallrepurchaserate = round($totalpurchasenumber / $numcustomers, 2);
}
else {
	$overallrepurchaserate = 0;
}

//add data to customerretainoverallvalue
$checkexisting = mysql_query("select * from $database.customerretainoverallvalue 
where daterecorded = '$date' and isoverall = 1");
if(mysql_num_rows($checkexisting) == 1){
	$getexisting = mysql_fetch_array($checkexisting);
	$customerretainoverallvalueid = $getexisting['customerretainoverallvalueid'];
	//update existing record
	$updatexisting = mysql_query("update $database.customerretainoverallvalue set overallretentionscore = '$overallretentionscore', 
	overalltrustscore = '0', overallnpsscore = '0', overalllifetimevalueaverage = '$overalllifetimevalueaverage', overallnegativesentimentscore 
	= '0', overallpositivesentimentscore = '0', overallattritionscore = '$overallattritionscore', 
	overallrepurchaserate = '$overallrepurchaserate', islatest = '1' 
	where customerretainoverallvalueid = '$customerretainoverallvalueid'");
}
else {
	//remove existing islatest
	$updateislatest = mysql_query("update $database.customerretainoverallvalue set islatest = '0' 
	where isoverall = '1'");	
	
	//add new record
	$addnew = mysql_query("insert into $database.customerretainoverallvalue (customerretainoverallvaluename, daterecorded, islatest, 
	isoverall, customerid, audienceid, funnelid, funnelstageid, overallretentionscore, overalltrustscore, overallnpsscore, 
	overalllifetimevalueaverage, overallnegativesentimentscore, overallpositivesentimentscore, overallattritionscore, overallrepurchaserate, 
	disabled, datecreated, masteronly) 
	values ('Overall', '$date', '1', '1', '0', '0', '0', '0', '$overallretentionscore', '0', '0', '$overalllifetimevalueaverage', '0', '0', 
	'$overallattritionscore', '$overallrepurchaserate', '0', '$date', '0')");
}


//CALCULATE METRICS FOR RETENTION - CUSTOMER
if(!isset($runcustomer)){
	$customers = mysql_query("select * from $database.customer 
	where customer.disabled = 0");
	while ($cust = mysql_fetch_array($customers)){
		$mastercustomerid = $cust['customerid'];
		//calculate retention
		$datepurchased = date("Y-m-d", strtotime($date ."-365 days"));
		$retained = mysql_query("select count(customerid) from $database.customerpurchase 
		where customerpurchase.disabled = 0 and datepurchased >= '$datepurchased' and customerid in ($mastercustomerid)
		group by customerid having count(customerid) >= 2");
		$retainedcustomers = mysql_num_rows($retained);
		$total = mysql_query("select count(customerid) from $database.customerpurchase 
		where customerpurchase.disabled = 0 and datepurchased >= '$datepurchased' and customerid in ($mastercustomerid)
		group by customerid");
		$totalcustomers = mysql_num_rows($total);
		if($retainedcustomers > 0 && $totalcustomers > 0){
			$overallretentionscore = round(($retainedcustomers / $totalcustomers)*100, 2);
		}
		else {
			$overallretentionscore = 0;
		}
		
		//calculate overalltrustscore
		//TO BE BUILT LATER
		
		//calculate overallnpsscore
		//TO BE BUILT LATER
		
		//calculate overalllifetimevalueaverage
		$lva = mysql_query("select count(distinct customerid) as 'numcustomers', 
		sum(purchasevalue / currencytohqcurrencycurrentfxrate) as 'totalpurchasevalue' 
		from $database.customerpurchase
		inner join $database.currencyfxrate on currencyfxrate.currencyid = customerpurchase.currencyid
		where customerpurchase.disabled = 0 and customerid in ($mastercustomerid)");
		$row77 = mysql_fetch_array($lva);
		$numcustomers = $row77['numcustomers'];
		$totalpurchasevalue = $row77['totalpurchasevalue'];
		if($totalpurchasevalue > 0 && $numcustomers > 0){
			$overalllifetimevalueaverage = round($totalpurchasevalue / $numcustomers, 2);
		}
		else {
			$overalllifetimevalueaverage = 0;
		}
		
		//calculate overallnegativesentimentscore
		//TO BE BUILT LATER
		
		//calculate overallpositivesentimentscore
		//TO BE BUILT LATER
		
		//calculate overallattritionscore
		$datepurchased2 = date("Y-m-d", strtotime($datepurchased ."-365 days"));
		$getpreviouscustomers = mysql_query("select customerid from $database.customerpurchase
		where customerpurchase.disabled = 0 and datepurchased < '$datepurchased' and datepurchased >= '$datepurchased2' 
		and customerid in ($mastercustomerid) group by customerid");
		$repurchased = 0;
		$attrition = 0;
		while ($row11 = mysql_fetch_array($getpreviouscustomers)){
			$customerid = $row11['customerid'];
			//check if customer purchased something in last 12 months
			$checkcustomer = mysql_query("select customerid from $database.customerpurchase
			where customerpurchase.disabled = 0 and datepurchased >= '$datepurchased' and customerid = '$customerid'
			and customerid in ($mastercustomerid)");
			$checkcustomercount = mysql_num_rows($checkcustomer);
			if($checkcustomercount >= 1){
				$repurchased = $repurchased + 1;
			}
			else {
				$attrition = $attrition + 1;	
			}
		}
		if($attrition > 0 || $repurchased > 0){
			$totalcustomers = $attrition + $repurchased;
			$overallattritionscore = round(($attrition / $totalcustomers) * 100, 2);
		}
		else {
			$overallattritionscore = 0;
		}
		
		//calculate overallrepurchaserate
		$repurchaserate = mysql_query("select count(distinct customerid) as 'numcustomers', 
		count(customerpurchaseid) as 'numberpurchases' 
		from $database.customerpurchase
		where customerpurchase.disabled = 0 and datepurchased >= '$datepurchased' and customerid in ($mastercustomerid)");
		$row78 = mysql_fetch_array($repurchaserate);
		$numcustomers = $row78['numcustomers'];
		$totalpurchasenumber = $row78['numberpurchases'];
		if($totalpurchasenumber > 0 && $numcustomers > 0){
			$overallrepurchaserate = round($totalpurchasenumber / $numcustomers, 2);
		}
		else {
			$overallrepurchaserate = 0;
		}
	
		//add data to customerretainoverallvalue
		$checkexisting = mysql_query("select * from $database.customerretainoverallvalue 
		where daterecorded = '$date' and customerid in ($mastercustomerid)");
		if(mysql_num_rows($checkexisting) == 1){
			$getexisting = mysql_fetch_array($checkexisting);
			$customerretainoverallvalueid = $getexisting['customerretainoverallvalueid'];
			//update existing record
			$updatexisting = mysql_query("update $database.customerretainoverallvalue set overallretentionscore = '$overallretentionscore', 
			overalltrustscore = '0', overallnpsscore = '0', overalllifetimevalueaverage = '$overalllifetimevalueaverage', overallnegativesentimentscore 
			= '0', overallpositivesentimentscore = '0', overallattritionscore = '$overallattritionscore', 
			overallrepurchaserate = '$overallrepurchaserate', islatest = '1' 
			where customerretainoverallvalueid = '$customerretainoverallvalueid'");
		}
		else {
			//remove existing islatest
			$updateislatest = mysql_query("update $database.customerretainoverallvalue set islatest = '0' 
			where customerid in ($mastercustomerid)");	
			
			//add new record
			$addnew = mysql_query("insert into $database.customerretainoverallvalue (customerretainoverallvaluename, daterecorded, islatest, 
			isoverall, customerid, audienceid, funnelid, funnelstageid, overallretentionscore, overalltrustscore, overallnpsscore, 
			overalllifetimevalueaverage, overallnegativesentimentscore, overallpositivesentimentscore, overallattritionscore, overallrepurchaserate, 
			disabled, datecreated, masteronly) 
			values ('Customer', '$date', '1', '0', '$mastercustomerid', '0', '0', '0', '$overallretentionscore', '0', '0', '$overalllifetimevalueaverage', '0', '0', 
			'$overallattritionscore', '$overallrepurchaserate', '0', '$date', '0')");
		}	
	}
}


//CALCULATE METRICS FOR RETENTION - AUDIENCE
$audience = mysql_query("select * from $database.audience 
where audience.disabled = 0");
while ($audiencerow = mysql_fetch_array($audience)){
	$audienceid = $audiencerow['audienceid'];
	$mastercustomerid = "";
	$customeraudience = mysql_query("select *  from $database.customeraudience 
	where audienceid = '$audienceid' and disabled = 0");
	while($customeraudiencerow = mysql_fetch_array($customeraudience)){
		$customerid = $customeraudiencerow['customerid'];
		$mastercustomerid = $mastercustomerid.$customerid.",";
	}	
	$mastercustomerid = rtrim($mastercustomerid,',');
	
	if($mastercustomerid <> ""){
		//calculate retention
		$datepurchased = date("Y-m-d", strtotime($date ."-365 days"));
		$retained = mysql_query("select count(customerid) from $database.customerpurchase 
		where customerpurchase.disabled = 0 and datepurchased >= '$datepurchased' and customerid in ($mastercustomerid)
		group by customerid having count(customerid) >= 2");
		$retainedcustomers = mysql_num_rows($retained);
		$total = mysql_query("select count(customerid) from $database.customerpurchase 
		where customerpurchase.disabled = 0 and datepurchased >= '$datepurchased' and customerid in ($mastercustomerid)
		group by customerid");
		$totalcustomers = mysql_num_rows($total);
		if($retainedcustomers > 0 && $totalcustomers > 0){
			$overallretentionscore = round(($retainedcustomers / $totalcustomers)*100, 2);
		}
		else {
			$overallretentionscore = 0;
		}
		
		//calculate overalltrustscore
		//TO BE BUILT LATER
		
		//calculate overallnpsscore
		//TO BE BUILT LATER
		
		//calculate overalllifetimevalueaverage
		$lva = mysql_query("select count(distinct customerid) as 'numcustomers', 
		sum(purchasevalue / currencytohqcurrencycurrentfxrate) as 'totalpurchasevalue' 
		from $database.customerpurchase
		inner join $database.currencyfxrate on currencyfxrate.currencyid = customerpurchase.currencyid
		where customerpurchase.disabled = 0 and customerid in ($mastercustomerid)");
		$row77 = mysql_fetch_array($lva);
		$numcustomers = $row77['numcustomers'];
		$totalpurchasevalue = $row77['totalpurchasevalue'];
		if($totalpurchasevalue > 0 && $numcustomers > 0){
			$overalllifetimevalueaverage = round($totalpurchasevalue / $numcustomers, 2);
		}
		else {
			$overalllifetimevalueaverage = 0;
		}
		
		//calculate overallnegativesentimentscore
		//TO BE BUILT LATER
		
		//calculate overallpositivesentimentscore
		//TO BE BUILT LATER
		
		//calculate overallattritionscore
		$datepurchased2 = date("Y-m-d", strtotime($datepurchased ."-365 days"));
		$getpreviouscustomers = mysql_query("select customerid from $database.customerpurchase
		where customerpurchase.disabled = 0 and datepurchased < '$datepurchased' and datepurchased >= '$datepurchased2' 
		and customerid in ($mastercustomerid) group by customerid");
		$repurchased = 0;
		$attrition = 0;
		while ($row11 = mysql_fetch_array($getpreviouscustomers)){
			$customerid = $row11['customerid'];
			//check if customer purchased something in last 12 months
			$checkcustomer = mysql_query("select customerid from $database.customerpurchase
			where customerpurchase.disabled = 0 and datepurchased >= '$datepurchased' and customerid = '$customerid'
			and customerid in ($mastercustomerid)");
			$checkcustomercount = mysql_num_rows($checkcustomer);
			if($checkcustomercount >= 1){
				$repurchased = $repurchased + 1;
			}
			else {
				$attrition = $attrition + 1;	
			}
		}
		if($attrition > 0 || $repurchased > 0){
			$totalcustomers = $attrition + $repurchased;
			$overallattritionscore = round(($attrition / $totalcustomers) * 100, 2);
		}
		else {
			$overallattritionscore = 0;
		}
		
		//calculate overallrepurchaserate
		$repurchaserate = mysql_query("select count(distinct customerid) as 'numcustomers', 
		count(customerpurchaseid) as 'numberpurchases' 
		from $database.customerpurchase
		where customerpurchase.disabled = 0 and datepurchased >= '$datepurchased' and customerid in ($mastercustomerid)");
		$row78 = mysql_fetch_array($repurchaserate);
		$numcustomers = $row78['numcustomers'];
		$totalpurchasenumber = $row78['numberpurchases'];
		if($totalpurchasenumber > 0 && $numcustomers > 0){
			$overallrepurchaserate = round($totalpurchasenumber / $numcustomers, 2);
		}
		else {
			$overallrepurchaserate = 0;
		}
	
		//add data to customerretainoverallvalue
		$checkexisting = mysql_query("select * from $database.customerretainoverallvalue 
		where daterecorded = '$date' and audienceid = '$audienceid'");
		if(mysql_num_rows($checkexisting) == 1){
			$getexisting = mysql_fetch_array($checkexisting);
			$customerretainoverallvalueid = $getexisting['customerretainoverallvalueid'];
			//update existing record
			$updatexisting = mysql_query("update $database.customerretainoverallvalue set overallretentionscore = '$overallretentionscore', 
			overalltrustscore = '0', overallnpsscore = '0', overalllifetimevalueaverage = '$overalllifetimevalueaverage', overallnegativesentimentscore 
			= '0', overallpositivesentimentscore = '0', overallattritionscore = '$overallattritionscore', 
			overallrepurchaserate = '$overallrepurchaserate', islatest = '1' 
			where customerretainoverallvalueid = '$customerretainoverallvalueid'");
		}
		else {
			//remove existing islatest
			$updateislatest = mysql_query("update $database.customerretainoverallvalue set islatest = '0' 
			where audienceid = '$audienceid'");	
			
			//add new record
			$addnew = mysql_query("insert into $database.customerretainoverallvalue (customerretainoverallvaluename, daterecorded, islatest, 
			isoverall, customerid, audienceid, funnelid, funnelstageid, overallretentionscore, overalltrustscore, overallnpsscore, 
			overalllifetimevalueaverage, overallnegativesentimentscore, overallpositivesentimentscore, overallattritionscore, overallrepurchaserate, 
			disabled, datecreated, masteronly) 
			values ('Audience', '$date', '1', '0', '0', '$audienceid', '0', '0', '$overallretentionscore', '0', '0', '$overalllifetimevalueaverage', '0', '0', 
			'$overallattritionscore', '$overallrepurchaserate', '0', '$date', '0')");
		}
	}	
	else {
		//add new record
		$addnew = mysql_query("insert into $database.customerretainoverallvalue (customerretainoverallvaluename, daterecorded, islatest, 
		isoverall, customerid, audienceid, funnelid, funnelstageid, overallretentionscore, overalltrustscore, overallnpsscore, 
		overalllifetimevalueaverage, overallnegativesentimentscore, overallpositivesentimentscore, overallattritionscore, overallrepurchaserate, 
		disabled, datecreated, masteronly) 
		values ('Funnel', '$date', '1', '0', '0', '$audienceid', '0', '0', '0', '0', '0', '0', 
		'0', '0', '0', '0', '0', '$date', '0')");
	}		
}



//CALCULATE METRICS FOR RETENTION - FUNNEL
$funnel = mysql_query("select * from $database.customerretainfunnel 
where customerretainfunnel.disabled = 0");
while ($funnelrow = mysql_fetch_array($funnel)){
	$customerretainfunnelid = $funnelrow['customerretainfunnelid'];
	$mastercustomerid = "";
	$customerfunnel = mysql_query("select *  from $database.customerretainfunnelstage 
	inner join $database.customerretainfunnelstagecustomer on customerretainfunnelstagecustomer.customerretainfunnelstageid 
	= customerretainfunnelstage.customerretainfunnelstageid
	where customerretainfunnelstage.customerretainfunnelid = '$customerretainfunnelid' and customerretainfunnelstage.disabled = 0");
	while($customerfunnelrow = mysql_fetch_array($customerfunnel)){
		$customerid = $customerfunnelrow['customerid'];
		$mastercustomerid = $mastercustomerid.$customerid.",";
	}	
	$mastercustomerid = rtrim($mastercustomerid,',');
	
	if($mastercustomerid <> ""){
		//calculate retention
		$datepurchased = date("Y-m-d", strtotime($date ."-365 days"));
		$retained = mysql_query("select count(customerid) from $database.customerpurchase 
		where customerpurchase.disabled = 0 and datepurchased >= '$datepurchased' and customerid in ($mastercustomerid)
		group by customerid having count(customerid) >= 2");
		$retainedcustomers = mysql_num_rows($retained);
		$total = mysql_query("select count(customerid) from $database.customerpurchase 
		where customerpurchase.disabled = 0 and datepurchased >= '$datepurchased' and customerid in ($mastercustomerid)
		group by customerid");
		$totalcustomers = mysql_num_rows($total);
		if($retainedcustomers > 0 && $totalcustomers > 0){
			$overallretentionscore = round(($retainedcustomers / $totalcustomers)*100, 2);
		}
		else {
			$overallretentionscore = 0;
		}
		
		//calculate overalltrustscore
		//TO BE BUILT LATER
		
		//calculate overallnpsscore
		//TO BE BUILT LATER
		
		//calculate overalllifetimevalueaverage
		$lva = mysql_query("select count(distinct customerid) as 'numcustomers', 
		sum(purchasevalue / currencytohqcurrencycurrentfxrate) as 'totalpurchasevalue' 
		from $database.customerpurchase
		inner join $database.currencyfxrate on currencyfxrate.currencyid = customerpurchase.currencyid
		where customerpurchase.disabled = 0 and customerid in ($mastercustomerid)");
		$row77 = mysql_fetch_array($lva);
		$numcustomers = $row77['numcustomers'];
		$totalpurchasevalue = $row77['totalpurchasevalue'];
		if($totalpurchasevalue > 0 && $numcustomers > 0){
			$overalllifetimevalueaverage = round($totalpurchasevalue / $numcustomers, 2);
		}
		else {
			$overalllifetimevalueaverage = 0;
		}
		
		//calculate overallnegativesentimentscore
		//TO BE BUILT LATER
		
		//calculate overallpositivesentimentscore
		//TO BE BUILT LATER
		
		//calculate overallattritionscore
		$datepurchased2 = date("Y-m-d", strtotime($datepurchased ."-365 days"));
		$getpreviouscustomers = mysql_query("select customerid from $database.customerpurchase
		where customerpurchase.disabled = 0 and datepurchased < '$datepurchased' and datepurchased >= '$datepurchased2' 
		and customerid in ($mastercustomerid) group by customerid");
		$repurchased = 0;
		$attrition = 0;
		while ($row11 = mysql_fetch_array($getpreviouscustomers)){
			$customerid = $row11['customerid'];
			//check if customer purchased something in last 12 months
			$checkcustomer = mysql_query("select customerid from $database.customerpurchase
			where customerpurchase.disabled = 0 and datepurchased >= '$datepurchased' and customerid = '$customerid'
			and customerid in ($mastercustomerid)");
			$checkcustomercount = mysql_num_rows($checkcustomer);
			if($checkcustomercount >= 1){
				$repurchased = $repurchased + 1;
			}
			else {
				$attrition = $attrition + 1;	
			}
		}
		if($attrition > 0 || $repurchased > 0){
			$totalcustomers = $attrition + $repurchased;
			$overallattritionscore = round(($attrition / $totalcustomers) * 100, 2);
		}
		else {
			$overallattritionscore = 0;
		}
		
		//calculate overallrepurchaserate
		$repurchaserate = mysql_query("select count(distinct customerid) as 'numcustomers', 
		count(customerpurchaseid) as 'numberpurchases' 
		from $database.customerpurchase
		where customerpurchase.disabled = 0 and datepurchased >= '$datepurchased' and customerid in ($mastercustomerid)");
		$row78 = mysql_fetch_array($repurchaserate);
		$numcustomers = $row78['numcustomers'];
		$totalpurchasenumber = $row78['numberpurchases'];
		if($totalpurchasenumber > 0 && $numcustomers > 0){
			$overallrepurchaserate = round($totalpurchasenumber / $numcustomers, 2);
		}
		else {
			$overallrepurchaserate = 0;
		}
	
		//add data to customerretainoverallvalue
		$checkexisting = mysql_query("select * from $database.customerretainoverallvalue 
		where daterecorded = '$date' and funnelid = '$customerretainfunnelid'");
		if(mysql_num_rows($checkexisting) == 1){
			$getexisting = mysql_fetch_array($checkexisting);
			$customerretainoverallvalueid = $getexisting['customerretainoverallvalueid'];
			//update existing record
			$updatexisting = mysql_query("update $database.customerretainoverallvalue set overallretentionscore = '$overallretentionscore', 
			overalltrustscore = '0', overallnpsscore = '0', overalllifetimevalueaverage = '$overalllifetimevalueaverage', overallnegativesentimentscore 
			= '0', overallpositivesentimentscore = '0', overallattritionscore = '$overallattritionscore', 
			overallrepurchaserate = '$overallrepurchaserate', islatest = '1' 
			where customerretainoverallvalueid = '$customerretainoverallvalueid'");
		}
		else {
			//remove existing islatest
			$updateislatest = mysql_query("update $database.customerretainoverallvalue set islatest = '0' 
			where funnelid = '$customerretainfunnelid'");	
			
			//add new record
			$addnew = mysql_query("insert into $database.customerretainoverallvalue (customerretainoverallvaluename, daterecorded, islatest, 
			isoverall, customerid, audienceid, funnelid, funnelstageid, overallretentionscore, overalltrustscore, overallnpsscore, 
			overalllifetimevalueaverage, overallnegativesentimentscore, overallpositivesentimentscore, overallattritionscore, overallrepurchaserate, 
			disabled, datecreated, masteronly) 
			values ('Funnel', '$date', '1', '0', '0', '0', '$customerretainfunnelid', '0', '$overallretentionscore', '0', '0', '$overalllifetimevalueaverage', 
			'0', '0', '$overallattritionscore', '$overallrepurchaserate', '0', '$date', '0')");
		}
	}
	else {
		//add new record
		$addnew = mysql_query("insert into $database.customerretainoverallvalue (customerretainoverallvaluename, daterecorded, islatest, 
		isoverall, customerid, audienceid, funnelid, funnelstageid, overallretentionscore, overalltrustscore, overallnpsscore, 
		overalllifetimevalueaverage, overallnegativesentimentscore, overallpositivesentimentscore, overallattritionscore, overallrepurchaserate, 
		disabled, datecreated, masteronly) 
		values ('Funnel', '$date', '1', '0', '0', '0', '$customerretainfunnelid', '0', '0', '0', '0', '0', 
		'0', '0', '0', '0', '0', '$date', '0')");
	}	
}


//CALCULATE METRICS FOR RETENTION - FUNNEL STAGE
$funnelstage = mysql_query("select * from $database.customerretainfunnelstage 
where customerretainfunnelstage.disabled = 0");
while ($funnelstagerow = mysql_fetch_array($funnelstage)){
	$customerretainfunnelstageid = $funnelstagerow['customerretainfunnelstageid'];
	$mastercustomerid = "";
	$customerfunnelstage = mysql_query("select *  from $database.customerretainfunnelstagecustomer 
	where customerretainfunnelstageid = '$customerretainfunnelstageid' and disabled = 0");
	while($customerfunnelstagerow = mysql_fetch_array($customerfunnelstage)){
		$customerid = $customerfunnelstagerow['customerid'];
		$mastercustomerid = $mastercustomerid.$customerid.",";
	}	
	$mastercustomerid = rtrim($mastercustomerid,',');
	
	if($mastercustomerid <> ""){
		//calculate retention
		$datepurchased = date("Y-m-d", strtotime($date ."-365 days"));
		$retained = mysql_query("select count(customerid) from $database.customerpurchase 
		where customerpurchase.disabled = 0 and datepurchased >= '$datepurchased' and customerid in ($mastercustomerid)
		group by customerid having count(customerid) >= 2");
		$retainedcustomers = mysql_num_rows($retained);
		$total = mysql_query("select count(customerid) from $database.customerpurchase 
		where customerpurchase.disabled = 0 and datepurchased >= '$datepurchased' and customerid in ($mastercustomerid)
		group by customerid");
		$totalcustomers = mysql_num_rows($total);
		if($retainedcustomers > 0 && $totalcustomers > 0){
			$overallretentionscore = round(($retainedcustomers / $totalcustomers)*100, 2);
		}
		else {
			$overallretentionscore = 0;
		}
		
		//calculate overalltrustscore
		//TO BE BUILT LATER
		
		//calculate overallnpsscore
		//TO BE BUILT LATER
		
		//calculate overalllifetimevalueaverage
		$lva = mysql_query("select count(distinct customerid) as 'numcustomers', 
		sum(purchasevalue / currencytohqcurrencycurrentfxrate) as 'totalpurchasevalue' 
		from $database.customerpurchase
		inner join $database.currencyfxrate on currencyfxrate.currencyid = customerpurchase.currencyid
		where customerpurchase.disabled = 0 and customerid in ($mastercustomerid)");
		$row77 = mysql_fetch_array($lva);
		$numcustomers = $row77['numcustomers'];
		$totalpurchasevalue = $row77['totalpurchasevalue'];
		if($totalpurchasevalue > 0 && $numcustomers > 0){
			$overalllifetimevalueaverage = round($totalpurchasevalue / $numcustomers, 2);
		}
		else {
			$overalllifetimevalueaverage = 0;
		}
		
		//calculate overallnegativesentimentscore
		//TO BE BUILT LATER
		
		//calculate overallpositivesentimentscore
		//TO BE BUILT LATER
		
		//calculate overallattritionscore
		$datepurchased2 = date("Y-m-d", strtotime($datepurchased ."-365 days"));
		$getpreviouscustomers = mysql_query("select customerid from $database.customerpurchase
		where customerpurchase.disabled = 0 and datepurchased < '$datepurchased' and datepurchased >= '$datepurchased2' 
		and customerid in ($mastercustomerid) group by customerid");
		$repurchased = 0;
		$attrition = 0;
		while ($row11 = mysql_fetch_array($getpreviouscustomers)){
			$customerid = $row11['customerid'];
			//check if customer purchased something in last 12 months
			$checkcustomer = mysql_query("select customerid from $database.customerpurchase
			where customerpurchase.disabled = 0 and datepurchased >= '$datepurchased' and customerid = '$customerid'
			and customerid in ($mastercustomerid)");
			$checkcustomercount = mysql_num_rows($checkcustomer);
			if($checkcustomercount >= 1){
				$repurchased = $repurchased + 1;
			}
			else {
				$attrition = $attrition + 1;	
			}
		}
		if($attrition > 0 || $repurchased > 0){
			$totalcustomers = $attrition + $repurchased;
			$overallattritionscore = round(($attrition / $totalcustomers) * 100, 2);
		}
		else {
			$overallattritionscore = 0;
		}
		
		//calculate overallrepurchaserate
		$repurchaserate = mysql_query("select count(distinct customerid) as 'numcustomers', 
		count(customerpurchaseid) as 'numberpurchases' 
		from $database.customerpurchase
		where customerpurchase.disabled = 0 and datepurchased >= '$datepurchased' and customerid in ($mastercustomerid)");
		$row78 = mysql_fetch_array($repurchaserate);
		$numcustomers = $row78['numcustomers'];
		$totalpurchasenumber = $row78['numberpurchases'];
		if($totalpurchasenumber > 0 && $numcustomers > 0){
			$overallrepurchaserate = round($totalpurchasenumber / $numcustomers, 2);
		}
		else {
			$overallrepurchaserate = 0;
		}
	
		//add data to customerretainoverallvalue
		$checkexisting = mysql_query("select * from $database.customerretainoverallvalue 
		where daterecorded = '$date' and funnelstageid = '$customerretainfunnelstageid'");
		if(mysql_num_rows($checkexisting) == 1){
			$getexisting = mysql_fetch_array($checkexisting);
			$customerretainoverallvalueid = $getexisting['customerretainoverallvalueid'];
			//update existing record
			$updatexisting = mysql_query("update $database.customerretainoverallvalue set overallretentionscore = '$overallretentionscore', 
			overalltrustscore = '0', overallnpsscore = '0', overalllifetimevalueaverage = '$overalllifetimevalueaverage', overallnegativesentimentscore 
			= '0', overallpositivesentimentscore = '0', overallattritionscore = '$overallattritionscore', 
			overallrepurchaserate = '$overallrepurchaserate', islatest = '1' 
			where customerretainoverallvalueid = '$customerretainoverallvalueid'");
		}
		else {
			//remove existing islatest
			$updateislatest = mysql_query("update $database.customerretainoverallvalue set islatest = '0' 
			where funnelstageid = '$customerretainfunnelstageid'");	
			
			//add new record
			$addnew = mysql_query("insert into $database.customerretainoverallvalue (customerretainoverallvaluename, daterecorded, islatest, 
			isoverall, customerid, audienceid, funnelid, funnelstageid, overallretentionscore, overalltrustscore, overallnpsscore, 
			overalllifetimevalueaverage, overallnegativesentimentscore, overallpositivesentimentscore, overallattritionscore, overallrepurchaserate, 
			disabled, datecreated, masteronly) 
			values ('Funnel Stage', '$date', '1', '0', '0', '0', '0', '$customerretainfunnelstageid', '$overallretentionscore', '0', '0', '$overalllifetimevalueaverage', 
			'0', '0', '$overallattritionscore', '$overallrepurchaserate', '0', '$date', '0')");
		}
	}
	else {
		//add new record
		$addnew = mysql_query("insert into $database.customerretainoverallvalue (customerretainoverallvaluename, daterecorded, islatest, 
		isoverall, customerid, audienceid, funnelid, funnelstageid, overallretentionscore, overalltrustscore, overallnpsscore, 
		overalllifetimevalueaverage, overallnegativesentimentscore, overallpositivesentimentscore, overallattritionscore, overallrepurchaserate, 
		disabled, datecreated, masteronly) 
		values ('Funnel', '$date', '1', '0', '0', '0', '0', '$customerretainfunnelstageid', '0', '0', '0', '0', 
		'0', '0', '0', '0', '0', '$date', '0')");
	}		
}



?>