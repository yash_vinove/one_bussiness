<?php

$shopdomain = $apikey1;
$apikey = $apikey2;
$secret = $apikey3;

if($integrationproductid <> ''){
	$pieces = explode("//", $integrationproductid);
	$integrationproductid = $pieces[0];
	$integrationvariantid = $pieces[1];
}

$url = "https://".$apikey.":".$secret."@".$shopdomain."/admin/products/".$integrationproductid.".json";
$data = '{"product":{"id":"'.$integrationproductid.'"';
$data = $data.',"variants":[{"id": '.$integrationvariantid.', "price": "'.$updatedprice.'"}]';
$data = $data.'}}';

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
//comment out returntransfer line below to see the json response for debugging purposes
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	
$result = curl_exec($ch);
curl_close($ch);

//update the status in ecommercelistinglocation
echo $result;
$result = json_decode($result, true); 
if(isset($result['product']['id'])){
	$productid = $result['product']['id'];
	echo "<br/><br/>price updated: ".$productid."<br/><br/>";
	
	//update status and productid within  ecommercelistinglocation 
	$updatestatus = mysql_query("update ecommercelistinglocation set ecommercelistinglocationstatusid = 3, 
	integrationerrormessage = '', integrationerrorretrynumber = 0, integrationstatusname = 'Price Drop', 
	listingprice = '$updatedprice', lastpricedropdate = '$date'
	where ecommercelistinglocationid = '$ecommercelistinglocationid'");
}
if(isset($result['errors'])){
	$errors = $result['errors'];
	$errors = json_encode($errors); 
	echo "<br/><br/>errors: ".$errors."<br/><br/>";
	$integrationerrorretrynumber = $integrationerrorretrynumber + 1;
	//update status and error details within ecommercelistinglocation
	$updatestatus = mysql_query("update ecommercelistinglocation set ecommercelistinglocationstatusid = 11, 
	integrationerrormessage = '$errors', integrationerrorretrynumber = '$integrationerrorretrynumber', 
	integrationstatusname = 'Price Drop Update Failed'
	where ecommercelistinglocationid = '$ecommercelistinglocationid'");
}
?>