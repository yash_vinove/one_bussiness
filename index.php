<?php
//phpinfo();
//error_reporting(E_ERROR);
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
session_start();
header("Set-Cookie: name=value; Secure; SameSite; httpOnly");
header("X-XSS-Protection: 1; mode=block");
header("X-Content-Type-Options: nosniff");
header("Content-Security-Policy: frame-ancestors 'none'");
header("Referrer-Policy: origin-when-cross-origin");
include 'config.php';
ob_start();
$_SESSION['authstation'] = (isset($_SESSION['authstation']) ? $_SESSION['authstation'] : null);
if (!isset($_SESSION['authstation'])) {
	$_SESSION['authstation'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
}
include 'config2.php';
if ($masterdatabase == 'andrewnorth_onebusinessmaster') {
	$prefix = 'andrewnorth_';
} else {
	$prefix = '';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Security-Policy" content="
    	default-src 'none';
    	object-src; base-uri 'self';
    	form-action 'self';
    	img-src 'self';
    	script-src 'self' ajax.googleapis.com maxcdn.bootstrapcdn.com;
    	font-src 'self';
    	style-src 'self'">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>OneBusiness</title>
	<link href="bootstrap-3.3.1/dist/css/bootstrap.css" rel="stylesheet">
	<link href="indexstyle.css" rel="stylesheet">

</head>

<body class="body1">
	<?php
	//declare variables
	$t = isset($_GET['t']) ? $_GET['t'] : '';
	$tenant = isset($_GET['tenant']) ? $_GET['tenant'] : '';
	$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
	$submit2 = isset($_POST['submit2']) ? $_POST['submit2'] : '';
	$emailaddress = isset($_POST['emailaddress']) ? strip_tags($_POST['emailaddress']) : '';
	$password1 = isset($_POST['password']) ? strip_tags($_POST['password']) : '';
	$saveddatabase = isset($_POST['saveddatabase']) ? strip_tags($_POST['saveddatabase']) : '';

	//check tenant variable for XSS
	if (strlen($tenant) > 0 && !is_numeric($tenant)) {
		echo "The Tenant ID is malformed, and we cannot continue. Please review the tenant variable to check that it is only numbers.";
		die;
	}

	if ($submit) {

		
		
		$mysqli = new mysqli($server, $user_name, $password, $database);
		if ($stmt = $mysqli->prepare("select max(versionid) as maxval from $masterdatabase.version")) {
			$stmt->execute();
			$result = $stmt->get_result();
			if ($result->num_rows > 0) {
				while ($getversionsrow = $result->fetch_assoc()) {
					$versions = $getversionsrow['maxval'];
				}
			}
		}
		$mysqli->close();
		$mysqli = new mysqli($server, $user_name, $password, $database);
		$versions = mysqli_real_escape_string($mysqli, $versions);
		if ($stmt = $mysqli->prepare("select * from $masterdatabase.version where versionid= ?")) {
			$stmt->bind_param('i', $versions);
			$stmt->execute();
			$result = $stmt->get_result();
			if ($result->num_rows > 0) {
				while ($getversionsrow = $result->fetch_assoc()) {
					$versionnum = $getversionsrow['versionfoldername'];
				}
			}
		}
		$mysqli->close();

		//validate all fields
		$validate = "";
		if ($emailaddress == "") {
			$validate = $validate . "Email address is a required field. ";
		}
		if ($saveddatabase == "") {
			$validate = $validate . "Business is a required field. ";
		}
		if ($password1 == "") {
			$validate = $validate . "Password is a required field. ";
		}
		if (strpos($emailaddress, '@') == false) {
			$validate = $validate . "Email address must contain an @ symbol. ";
		}
		if (strlen($password1) > 25 || strlen($password1) < 8) {
			$validate = $validate . "Your password must be between 8 and 25 characters. ";
		}
	}
	

	//validate password request
	if ($submit2) {
		//validate all fields
		$validate = "";
		if ($emailaddress == "") {
			$validate = $validate . "Email address is a required field. ";
		}
		if (strpos($emailaddress, '@') == false) {
			$validate = $validate . "Email address must contain an @ symbol. ";
		}
		if ($saveddatabase == "") {
			$validate = $validate . "Business is a required field. ";
		}
	}

	//run code if validation complete
	if ($submit) {
		
		if ($validate == "") {
			if ($tenant >= 1) {
				$db = $extension . "onebusinesstenant" . $tenant;
			} else {
				$db = $extension . "onebusinessmaster";
			}

			//get ip/country
			$ip = "";
			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
				$ip = $_SERVER['REMOTE_ADDR'];
			}
			$countryname = "";
			if ($ip != "" && $ip != "::1") {
				$geoPlugin_array = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $ip));
				$countryname = $geoPlugin_array['geoplugin_countryCode'];
			} else {
				$ip = "";
			}

			// echo "<br/>db:".$db;
			// echo "<br/>saveddatabase:".$saveddatabase;
			// die();
			$mysqli = new mysqli($server, $user_name, $password, $database);
			$saveddatabase = mysqli_real_escape_string($mysqli, $saveddatabase);
			if ($stmt = $mysqli->prepare("select * from $db.business
			left join currency on currency.currencyid = business.currencyid
	   		where businessid = ?")) {
				$stmt->bind_param('s', $saveddatabase);
				$stmt->execute();
				$result = $stmt->get_result();
				if ($result->num_rows > 0) {
					while ($getdatabasenamerow = $result->fetch_assoc()) {
						$saveddatabasename = $getdatabasenamerow['databasename'];
						$savedbusinessname = $getdatabasenamerow['businessname'];
						$savedcurrencyid = $getdatabasenamerow['currencyid'];
						$savedcurrencycode = $getdatabasenamerow['currencycode'];
						$_SESSION['currencycode'] = $savedcurrencycode;
						$savedcountryid = $getdatabasenamerow['countryid'];
						$savedlanguageid = $getdatabasenamerow['languageid'];
						$savedversionid = $getdatabasenamerow['versionid'];
						$database = $extension . $saveddatabasename;
					}
				}
			}
			$mysqli->close();
			//echo $saveddatabasename;

			//get tenant database name
			$tenantdbname = "";
			$mysqli = new mysqli($server, $user_name, $password, $database);
			$saveddatabase = mysqli_real_escape_string($mysqli, $saveddatabase);
			if ($stmt = $mysqli->prepare("select * from $db.business where businessid = '1000001'")) {
				$stmt->execute();
				$result = $stmt->get_result();
				if ($result->num_rows > 0) {
					while ($updatedatabaserow = $result->fetch_assoc()) {
						$tenantdbname = $updatedatabaserow['databasename'];
					}
				}
			}
			$mysqli->close();

			if (isset($_POST["CSRFtoken"], $_COOKIE["CSRFtoken"])) {
				if ($_POST["CSRFtoken"] == $_COOKIE["CSRFtoken"]) {
					//echo "<br/>CSRFtoken:".$_POST["CSRFtoken"];
					//echo "<br/>Cookie:".$_COOKIE["CSRFtoken"];
				} else {
					echo "There has been a failure verifying the cookie status. This suggests someone is trying to hijack the login form. We have cancelled
					this login request. Please contact a system administrator.";

					//log security incident
					$securityincidenttype = 'Attempt To Hijack Login Page CSRF Token';
					$securityuserid = 0;
					$securityipaddress = $ip;
					include $versionnum . '/securityemail.php';

					die;
				}
			}

			//check if account exists
			$mysqli = new mysqli($server, $user_name, $password, $database);
			$emailaddress = mysqli_real_escape_string($mysqli, $emailaddress);
			if ($stmt = $mysqli->prepare("select * from user where email = ? and disabled = 0")) {
				$stmt->bind_param('s', $emailaddress);
				$stmt->execute();
				$result = $stmt->get_result();
				$numrows = $result->num_rows;
				if ($result->num_rows > 0) {
					while ($row3 = $result->fetch_assoc()) {
						$savedpassword = $row3['password'];
						//echo $savedpassword;
						//echo $password1;
						$userid = $row3['userid'];
						$firstname = $row3['firstname'];
						$lastname = $row3['lastname'];
					}
				}
			}
			$mysqli->close();

			if ($numrows >= 1) {
				//check password
				//echo "<br/><br/>savedpassword: ".$savedpassword;
				//echo "<br/><br/>password1: ".$password1;
				//echo "<br/><br/>password2: ".md5($password1);
				
				if ($savedpassword == md5($password1)) {
					$logintype = "existing";
					$_SESSION["password"] = $password1;
					$_SESSION["email"] = $emailaddress;
					$_SESSION["userid"] = $userid;
					$_SESSION["firstname"] = $firstname;
					$_SESSION["lastname"] = $lastname;
					$_SESSION["savedmasterdatabasename"] = $extension . "onebusinessmaster";
					$_SESSION["saveddatabasename"] = $extension . $saveddatabasename;
					$_SESSION["savedbusinessname"] = $savedbusinessname;
					$_SESSION["savedtenantdatabasename"] = $extension . $tenantdbname;
					$_SESSION["savedversionid"] = $savedversionid;

					if ($saveddatabasename == 'onebusinessmaster') {
						$_SESSION["accesslevel"] = 'Master';
					} else {
						if (strlen($saveddatabasename) <= 26) {
							$_SESSION["accesslevel"] = 'Tenant';
						} else {
							$_SESSION["accesslevel"] = 'Business';
						}
					}

					$masterdatabase = $_SESSION["savedmasterdatabasename"];

					//get versionnum
					if ($saveddatabase > 1) {
						$mysqli = new mysqli($server, $user_name, $password, $database);
						$savedversionid = mysqli_real_escape_string($mysqli, $savedversionid);
						if ($stmt = $mysqli->prepare("select * from $masterdatabase.version where versionid= ?")) {
							$stmt->bind_param('i', $savedversionid);
							$stmt->execute();
							$result = $stmt->get_result();
							if ($result->num_rows > 0) {
								while ($getversionsrow = $result->fetch_assoc()) {
									$versionnum = $getversionsrow['versionfoldername'];
								}
							}
						}
						$mysqli->close();
					} else {
						$mysqli = new mysqli($server, $user_name, $password, $database);
						if ($stmt = $mysqli->prepare("select max(versionid) as maxval from $masterdatabase.version")) {
							$stmt->execute();
							$result = $stmt->get_result();
							if ($result->num_rows > 0) {
								while ($getversionsrow = $result->fetch_assoc()) {
									$versions = $getversionsrow['maxval'];
								}
							}
						}
						$mysqli->close();
						$mysqli = new mysqli($server, $user_name, $password, $database);
						$versions = mysqli_real_escape_string($mysqli, $versions);
						if ($stmt = $mysqli->prepare("select * from $masterdatabase.version where versionid= ?")) {
							$stmt->bind_param('i', $versions);
							$stmt->execute();
							$result = $stmt->get_result();
							if ($result->num_rows > 0) {
								while ($getversionsrow = $result->fetch_assoc()) {
									$versionnum = $getversionsrow['versionfoldername'];
								}
							}
						}
						$mysqli->close();
					}

					//set styling
					$mysqli = new mysqli($server, $user_name, $password, $database);
					if ($stmt = $mysqli->prepare("select styleitemname, styledetail from style
					inner join styleitem on styleitem.styleid = style.styleid where style.current=1")) {
						$stmt->execute();
						$result = $stmt->get_result();
						if ($result->num_rows > 0) {
							while ($row4 = $result->fetch_assoc()) {
								$styleitemname = $row4['styleitemname'];
								$styledetail = $row4['styledetail'];
								$_SESSION[$styleitemname] = $styledetail;
							}
						}
					}
					$mysqli->close();

					//get currency settings and assign to session handler
					if ($saveddatabase > 1) {
						$mysqli = new mysqli($server, $user_name, $password, $database);
						$savedcurrencyid = mysqli_real_escape_string($mysqli, $savedcurrencyid);
						if ($stmt = $mysqli->prepare("select * from $prefix$tenantdbname.currency where currencyid = ?")) {
							$stmt->bind_param('i', $savedcurrencyid);
							$stmt->execute();
							$result = $stmt->get_result();
							if ($result->num_rows > 0) {
								while ($getcurrencyrow = $result->fetch_assoc()) {
									$_SESSION["currencysymbol"] = $getcurrencyrow['currencysymbol'];
									$_SESSION["currencycode"] = $getcurrencyrow['currencycode'];
								}
							}
						}
						$mysqli->close();
					} else {
						$mysqli = new mysqli($server, $user_name, $password, $database);
						$savedcurrencyid = mysqli_real_escape_string($mysqli, $savedcurrencyid);
						if ($stmt = $mysqli->prepare("select * from $prefix$saveddatabasename.currency where currencyid = ?")) {
							$stmt->bind_param('i', $savedcurrencyid);
							$stmt->execute();
							$result = $stmt->get_result();
							if ($result->num_rows > 0) {
								while ($getcurrencyrow = $result->fetch_assoc()) {
									$_SESSION["currencysymbol"] = $getcurrencyrow['currencysymbol'];
									$_SESSION["currencycode"] = $getcurrencyrow['currencycode'];
								}
							}
						}
						$mysqli->close();
					}
					//echo $getcurrency;

					//get country settings and assign to session handler
					if ($saveddatabase > 1) {
						$mysqli = new mysqli($server, $user_name, $password, $database);
						$savedcountryid = mysqli_real_escape_string($mysqli, $savedcountryid);
						if ($stmt = $mysqli->prepare("select * from $prefix$tenantdbname.country where countryid = ?")) {
							$stmt->bind_param('i', $savedcountryid);
							$stmt->execute();
							$result = $stmt->get_result();
							if ($result->num_rows > 0) {
								while ($getcountryrow = $result->fetch_assoc()) {
									$_SESSION["countryname"] = $getcountryrow['countryname'];
									$_SESSION["countryshortcode"] = $getcountryrow['countryshortcode'];
									$_SESSION["dateformat"] = $getcountryrow['dateformat'];
									$_SESSION["numberformatdecimal"] = $getcountryrow['numberformatdecimal'];
									$_SESSION["numberformatseparator"] = $getcountryrow['numberformatseparator'];
									$_SESSION["numberformatdecimalpoint"] = $getcountryrow['numberformatdecimalpoint'];
								}
							}
						}
						$mysqli->close();
					} else {
						$mysqli = new mysqli($server, $user_name, $password, $database);
						$savedcountryid = mysqli_real_escape_string($mysqli, $savedcountryid);
						if ($stmt = $mysqli->prepare("select * from $prefix$saveddatabasename.country where countryid = ?")) {
							$stmt->bind_param('i', $savedcountryid);
							$stmt->execute();
							$result = $stmt->get_result();
							if ($result->num_rows > 0) {
								while ($getcountryrow = $result->fetch_assoc()) {
									$_SESSION["countryname"] = $getcountryrow['countryname'];
									$_SESSION["countryshortcode"] = $getcountryrow['countryshortcode'];
									$_SESSION["dateformat"] = $getcountryrow['dateformat'];
									$_SESSION["numberformatdecimal"] = $getcountryrow['numberformatdecimal'];
									$_SESSION["numberformatseparator"] = $getcountryrow['numberformatseparator'];
									$_SESSION["numberformatdecimalpoint"] = $getcountryrow['numberformatdecimalpoint'];
								}
							}
						}
						$mysqli->close();
					}

					//get language sessions and assign to session handler
					$_SESSION["languageid"] = $savedlanguageid;

					//set users user roles
					$superadmin = 0;
					$userrole = "";
					$mysqli = new mysqli($server, $user_name, $password, $database);
					$userid = mysqli_real_escape_string($mysqli, $userid);
					if ($stmt = $mysqli->prepare("select * from userrole
					inner join permissionrole on permissionrole.permissionroleid = userrole.permissionroleid
					where userid = ?")) {
						$stmt->bind_param('i', $userid);
						$stmt->execute();
						$result = $stmt->get_result();
						if ($result->num_rows > 0) {
							while ($getrolerow = $result->fetch_assoc()) {
								$permissionroleid = $getrolerow['permissionroleid'];
								if ($permissionroleid == 1 && $superadmin == 0) {
									$superadmin = 1;
								}
								$userrole = $userrole . $permissionroleid . ", ";
							}
						}
					}
					$mysqli->close();

					$userrole = rtrim($userrole, ', ');
					$_SESSION["userrole"] = $userrole;
					$_SESSION["superadmin"] = $superadmin;

					//set users business units
					//check if userbusinessunit table exists in the database
					$mysqli = new mysqli($server, $user_name, $password, $database);
					$saveddatabasename = mysqli_real_escape_string($mysqli, $saveddatabasename);
					if ($stmt = $mysqli->prepare("SELECT table_name
					FROM information_schema.tables
					WHERE table_schema = ?
					AND table_name = 'userbusinessunit'")) {
						$stmt->bind_param('s', $saveddatabasename);
						$stmt->execute();
						$result = $stmt->get_result();
						$numrows = $result->num_rows;
						if ($result->num_rows > 0) {
							while ($gettranslationrow = $result->fetch_assoc()) {
								$userbusinessunit = "";

								$mysqli2 = new mysqli($server, $user_name, $password, $database);
								$userid = mysqli_real_escape_string($mysqli2, $userid);
								if ($stmt2 = $mysqli2->prepare("select * from userbusinessunit
								where userid = ?")) {
									$stmt2->bind_param('i', $userid);
									$stmt2->execute();
									$result2 = $stmt2->get_result();
									if ($result2->num_rows > 0) {
										while ($getbusinessunitrow = $result2->fetch_assoc()) {
											$businessunitid = $getbusinessunitrow['businessunitid'];
											$userbusinessunit = $userbusinessunit . $businessunitid . ", ";
										}
									}
								}
								$mysqli2->close();
								$mysqli3 = new mysqli($server, $user_name, $password, $database);
								$userid = mysqli_real_escape_string($mysqli3, $userid);
								if ($stmt3 = $mysqli3->prepare("select * from userbusinessunithierarchy
								inner join businessunit on userbusinessunithierarchy.businessunithierarchyid = businessunit.businessunithierarchyid
								where userid = ?")) {
									$stmt3->bind_param('i', $userid);
									$stmt3->execute();
									$result3 = $stmt3->get_result();
									if ($result3->num_rows > 0) {
										while ($getbusinessunithierarchyrow = $result3->fetch_assoc()) {
											$businessunitid = $getbusinessunithierarchyrow['businessunitid'];
											$userbusinessunit = $userbusinessunit . $businessunitid . ", ";
										}
									}
								}
								$mysqli3->close();

								$userbusinessunit = rtrim($userbusinessunit, ', ');
								$_SESSION["userbusinessunit"] = $userbusinessunit;
							}
						} else {
							$_SESSION["userbusinessunit"] = '';
						}
					}
					$mysqli->close();
					//echo $_SESSION["userbusinessunit"];

					//check if database has any IP restrictions
					$ver = -1;
					$ipaddresslist = "";
					$mysqli10 = new mysqli($server, $user_name, $password, $database);
					$userid = mysqli_real_escape_string($mysqli10, $userid);
					if ($stmt10 = $mysqli10->prepare("select ipaddress from applicationaccessip
					where disabled = 0")) {
						$stmt10->execute();
						$result10 = $stmt10->get_result();
						if ($result10->num_rows > 0) {
							while ($getiprestrict = $result10->fetch_assoc()) {
								$ipaddress = $getiprestrict['ipaddress'];
								$ipaddresslist = $ipaddresslist . $ipaddress . ", ";
							}
						}
					}
					$mysqli10->close();
					//var_dump($ipaddresslist);
					if ($ipaddresslist != "") {
						//echo "My IP: ".$ip;
						//$ip = "212.36.33.14";
						$ver = 0;
						$ipexplode = explode(".", $ip);
						$ipaddresslist = rtrim($ipaddresslist, ", ");
						//echo "<br/>IP List: ".$ipaddresslist;
						$explodeip = explode(", ", $ipaddresslist);
						foreach ($explodeip as $approvedip) {
							//echo "<br/><br/>Approved IP: ".$approvedip;
							$approvedipexplode = explode(".", $approvedip);
							if ($ipexplode[0] == $approvedipexplode[0]) {
								//echo "<br/>first figure correct";
								if ($ipexplode[1] == $approvedipexplode[1]) {
									//echo "<br/>second figure correct";
									if ($ipexplode[2] == $approvedipexplode[2] || $approvedipexplode[2] == "x") {
										//echo "<br/>third figure correct";
										if ($ipexplode[3] == $approvedipexplode[3] || $approvedipexplode[3] == "x") {
											//echo "<br/>fourth figure correct";
											$ver = 1;
											break;
										} else {
											//echo "<br/>fourth figure wrong";
										}
									} else {
										//echo "<br/>third figure wrong";
									}
								} else {
									//echo "<br/>second figure wrong";
								}
							} else {
								//echo "<br/>first figure wrong";
							}
						}
						//echo "<br/>Confirmed: ".$ver;
					}
					//var_dump($ver);
					//record login
					if ($ver != 0) {
						// $datecreated = date("Y-m-d");
						// $userloginname = $_SESSION['firstname'] . " " . $_SESSION['lastname'] . " " . $datecreated;
						// $loggedin = date("Y-m-d H:i:s");
						// $userid = mysql_real_escape_string($userid);
						// $userloginname = mysql_real_escape_string($userloginname);
						// $loggedin = mysql_real_escape_string($loggedin);
						// $savedpassword = mysql_real_escape_string($savedpassword);
						// $ip = mysql_real_escape_string($ip);
						// $countryname = mysql_real_escape_string($countryname);


						$mysqli = new mysqli($server, $user_name, $password, $database);
						$userid = mysqli_real_escape_string($mysqli, $userid);
						$userloginname = mysqli_real_escape_string($mysqli, $userloginname);
						$datecreated = mysqli_real_escape_string($mysqli, $datecreated);
						$loggedin = mysqli_real_escape_string($mysqli, $loggedin);
						$savedpassword = mysqli_real_escape_string($mysqli, $savedpassword);
						$ip = mysqli_real_escape_string($mysqli, $ip);
						$countryname = mysqli_real_escape_string($mysqli, $countryname);
						if ($stmt = $mysqli->prepare("insert into userlogin (userid, userloginname, datecreated, masteronly, loggedin,
						passwordused, ipaddressused, country, attempttype)
						values (?, ?, ?, '0', ?, ?, ?, ?, 'Success')")) {
							$stmt->bind_param('issssss', $userid, $userloginname, $datecreated, $loggedin, $savedpassword, $ip, $countryname);
							$stmt->execute();
						}
						$mysqli->close();

						$cookie_name = "database";
						$cookie_value = $tenantdbname;
						setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
						
						//go to homepage
						$url = './' . $versionnum . '/dashboard.php';
						echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL=' . $url . '">';
					} else {
						echo "<br/>You do not have permission to access from this IP address. Please contact the administrator to understand your access rights.";
						die;
					}
				}
				if ($savedpassword != md5($password1)) {
					
					$validate = $validate . "Password is incorrect.";

					//record failure
					$mysqli = new mysqli($server, $user_name, $password, $database);
					$userid = mysqli_real_escape_string($mysqli, $userid);
					$userloginname = mysqli_real_escape_string($mysqli, $userloginname);
					$datecreated = mysqli_real_escape_string($mysqli, $datecreated);
					$ip = mysqli_real_escape_string($mysqli, $ip);
					$loggedin = date("Y-m-d H:i:s");
					$savedpassword = "failed";
					$countryname = "none";
					if ($stmt = $mysqli->prepare("insert into userlogin (userid, userloginname, datecreated, masteronly, loggedin,
					passwordused, ipaddressused, country, attempttype)
					values (?, ?, ?, '0', ?, ?, ?, ?, 'Failed')")) {
						$stmt->bind_param('issssss', $userid, $userloginname, $datecreated, $loggedin, $savedpassword, $ip, $countryname);
						$stmt->execute();
					}
					$mysqli->close();

					//check if failed 3 times in last 2 hours
					$mysqli = new mysqli($server, $user_name, $password, $database);
					$saveddatabase = mysqli_real_escape_string($mysqli, $saveddatabase);
					$loggedintwohours = date('Y-m-d', strtotime("- 2 hours"));
					if ($stmt = $mysqli->prepare("select * from userlogin where userid = ? and attempttype = 'Failed' and loggedin >= ?")) {
						$stmt->bind_param('is', $userid, $loggedintwohours);
						$stmt->execute();
						$result = $stmt->get_result();
						$numrows2 = $result->num_rows;
						if ($numrows2 >= 3) {
							echo "<br/>Your have tried to login too many times with the incorrect login details. Your account has been locked. Please contact the system administrator to have your account unlocked.";
							$mysqli = new mysqli($server, $user_name, $password, $database);
							$userid = mysqli_real_escape_string($mysqli, $userid);
							if ($stmt = $mysqli->prepare("update user set disabled = 1 where userid = ?")) {
								$stmt->bind_param('i', $userid);
								$stmt->execute();
							}
							$mysqli->close();

							//log security incident
							$securityincidenttype = 'Unsuccessful Login 3 Times In Last 2 Hours - Account Lock';
							$securityuserid = $userid;
							$securityipaddress = $ip;
							include $versionnum . '/securityemail.php';

							die;
						}
					}
					$mysqli->close();
				}
			} else {
				//Attempted login with a username that does not exist

				//record failure
				$mysqli = new mysqli($server, $user_name, $password, $database);
				$datecreated = mysqli_real_escape_string($mysqli, $datecreated);
				$ip = mysqli_real_escape_string($mysqli, $ip);
				$loggedin = date("Y-m-d H:i:s");
				$savedpassword = "failed";
				$countryname = "none";
				if ($stmt = $mysqli->prepare("insert into userlogin (userid, datecreated, masteronly, loggedin,
				passwordused, ipaddressused, country, attempttype)
				values (0, ?, '0', ?, ?, ?, ?, 'Failed')")) {
					$stmt->bind_param('sssss', $datecreated, $loggedin, $savedpassword, $ip, $countryname);
					$stmt->execute();
				}
				$mysqli->close();

				//check if failed 10 times in last 2 hours
				$mysqli = new mysqli($server, $user_name, $password, $database);
				$saveddatabase = mysqli_real_escape_string($mysqli, $saveddatabase);
				$loggedintwohours = date('Y-m-d', strtotime("- 2 hours"));
				if ($stmt = $mysqli->prepare("select * from userlogin
				where ipaddressused = ? and attempttype = 'Failed' and loggedin >= ?")) {
					$stmt->bind_param('ss', $ip, $loggedintwohours);
					$stmt->execute();
					$result = $stmt->get_result();
					$numrows2 = $result->num_rows;
					if ($numrows2 >= 10) {
						echo "<br/>Your have tried to login too many times with the incorrect login details.";

						//log security incident
						$securityincidenttype = 'Attempted Login Page Hijack Attempt - Incorrect Username 10 Times';
						$securityuserid = 0;
						$securityipaddress = $ip;
						include $versionnum . '/securityemail.php';

						die;
					}
				}
				$mysqli->close();
			}
		}
	}

	if ($submit2) {
		if ($validate == "") {

			$mysqli = new mysqli($server, $user_name, $password, $database);
			$saveddatabase = mysqli_real_escape_string($mysqli, $saveddatabase);
			if ($stmt = $mysqli->prepare("select * from business where businessid = ?")) {
				$stmt->bind_param('s', $saveddatabase);
				$stmt->execute();
				$result = $stmt->get_result();
				if ($result->num_rows > 0) {
					while ($getdatabasenamerow = $result->fetch_assoc()) {
						$saveddatabasename = $getdatabasenamerow['databasename'];
						$savedbusinessname = $getdatabasenamerow['businessname'];
					}
				}
			}
			$mysqli->close();
			$database = $saveddatabasename;

			//get tenant database name
			$mysqli = new mysqli($server, $user_name, $password, $database);
			if ($stmt = $mysqli->prepare("select * from business where businessid = '1'")) {
				$stmt->execute();
				$result = $stmt->get_result();
				if ($result->num_rows > 0) {
					while ($updatedatabaserow = $result->fetch_assoc()) {
						$tenantdbname = $updatedatabaserow['databasename'];
					}
				}
			}
			$mysqli->close();

			//check if account exists
			$mysqli = new mysqli($server, $user_name, $password, $database);
			$emailaddress = mysqli_real_escape_string($mysqli, $emailaddress);
			if ($stmt = $mysqli->prepare("select * from user where email = ?")) {
				$stmt->bind_param('s', $emailaddress);
				$stmt->execute();
				$result = $stmt->get_result();
				$numrows = $result->num_rows;
				if ($result->num_rows > 0) {
					while ($row2 = $result->fetch_assoc()) {
						$reminderfirstname1 = $row2['firstname'];
						$reminderemailaddress = $row2['email'];
					}
				}
			}
			$mysqli->close();

			if ($numrows >= 1) {

				//generate random new password
				$lengthpassword = 10;
				$characterspassword = "-0123456789abcdefghijklmnopqrstuvwxyz";
				$resetnewpassword = "";
				$pq = 0;
				for ($pq = 0; $pq < $lengthpassword; $pq++) {
					$resetnewpassword .= $characterspassword[mt_rand(0, strlen($characterspassword))];
				}
				$resetnewpassworddatabasevalue = md5($resetnewpassword);
				$resetnewpassworddatabasevalue = mysql_real_escape_string($resetnewpassworddatabasevalue);
				$reminderemailaddress = mysql_real_escape_string($reminderemailaddress);

				$mysqli = new mysqli($server, $user_name, $password, $database);
				$resetnewpassworddatabasevalue = mysqli_real_escape_string($mysqli, $resetnewpassworddatabasevalue);
				$reminderemailaddress = mysqli_real_escape_string($mysqli, $reminderemailaddress);
				if ($stmt = $mysqli->prepare("update user set password = ? where email = ?")) {
					$stmt->bind_param('ss', $resetnewpassworddatabasevalue, $reminderemailaddress);
					$stmt->execute();
				}
				$mysqli->close();

				$mysqli = new mysqli($server, $user_name, $password, $database);
				if ($stmt = $mysqli->prepare("select max(versionid) as maxval from $masterdatabase.version")) {
					$stmt->execute();
					$result = $stmt->get_result();
					if ($result->num_rows > 0) {
						while ($getversionsrow = $result->fetch_assoc()) {
							$versions = $getversionsrow['maxval'];
						}
					}
				}
				$mysqli->close();
				$mysqli = new mysqli($server, $user_name, $password, $database);
				$versions = mysqli_real_escape_string($mysqli, $versions);
				if ($stmt = $mysqli->prepare("select * from $masterdatabase.version where versionid= ?")) {
					$stmt->bind_param('i', $versions);
					$stmt->execute();
					$result = $stmt->get_result();
					if ($result->num_rows > 0) {
						while ($getversionsrow = $result->fetch_assoc()) {
							$versionnum = $getversionsrow['versionfoldername'];
						}
					}
				}
				$mysqli->close();

				//send activation email
				$content1 = $reminderfirstname1;
				//echo "content1 - ".$content1."<br/>";
				$content2 = $resetnewpassword;
				//echo "content2 - ".$content2."<br/>";
				$emailcontentid = 13;
				$emailto = $emailaddress;
				$url = $versionnum . "/sendemail.php";
				include $url;

				//go to homepage
				$url = 'index.php?tenant=' . $tenant . '&t=4';
				echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL=' . $url . '">';
			}
		}
		if ($validate != "") {
			$validate = $validate . "This email address does not exist in our list. Please either correct your email address or click Cancel and create a new account. ";
		}
	}

	if (!isset($_SESSION["userprofile"])) {
		if ($tenant >= 1) {
			$mysqli = new mysqli($server, $user_name, $password, $database);
			$tenant = mysqli_real_escape_string($mysqli, $tenant);
			if ($stmt = $mysqli->prepare("select * from $masterdatabase.tenant where tenantid = ? and disabled = '0'")) {
				$stmt->bind_param('i', $tenant);
				$stmt->execute();
				$result = $stmt->get_result();
				if ($result->num_rows > 0) {
				} else {
					echo "<p class='bg-danger'>This tenant is no longer active, please contact your vendor for more information.</p>";
					die;
				}
			}
			$mysqli->close();
		}
		$token = bin2hex(openssl_random_pseudo_bytes(16));
		setcookie("CSRFtoken", $token, time() + 60 * 60 * 24);
	?>
	<div class="login">
	<div class="square">
		<img src="images/sphere-gradient.svg" width=65%>
	</div>
	<div class="dot">
		<img src="images/dot-square.png">
	</div>
		<div class="container">

			<div class="row">
				<!-- <div class="hidden-xs col-sm-6 col-md-6 col-lg-6">
					<img src="images/loginpage.jpg" class="img-responsive" alt="">
				</div>
				<div class="col-xs-12 hidden-sm hidden-md hidden-lg">
					<img src="images/loginpage2.png" class="img-responsive2" alt="">
				</div> -->

				<!-- <div class="hidden-xs col-md-6 col-xs-6 col-sm-6">
					<br /><br /><br /><br />
					<br /><br /><br /><br />
				</div> -->

				<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
					<div class="col-md-12 col-sm-12 col-xs-12">

						<div class="row">
							<div class="col-xs-1 col-sm-1 col-md-1">
							</div>

							<div class="col-xs-12 col-sm-12 col-md-12 center">
								<div class='div19'>
									<div class="text-center">
									   <img src="images/one_logo.png" class="img-responsive">
									</div>
									
									<?php
									if ($t == 4) {
										echo "<p class='bg-success'>Your password request has been submitted</p>";
									?>
										<form class="form-horizontal" action='index.php?tenant=<?php echo $tenant ?>&t=1' method="post">
											<div class="form-group">
												<div class="col-sm-offset-0 col-sm-12">
													<a href="./index.php?tenant=<?php echo $tenant ?>&t=1" type="submit" class="btn btn-default">Go Back</a>
												</div>
											</div>
										</form>
									<?php
									}

									if ($t == "" || $t == 1) {
										echo "<h1 class='text-center txt'>Welcome back!</h1>";
										echo "<p>Log back into your account</p>";
										if (isset($validate) && $validate != "") {
											echo "<p class='bg-danger'>" . $validate . "</p>";
										}
									?>
										<div>
											<br />
											<form class="form-horizontal" action='index.php?tenant=<?php echo $tenant ?>&t=1' method="post">
												<div class="form-group">
													
													<div class="col-sm-12">
														<!-- <label for="inputEmail3" class="control-label">Email</label> -->
														<input type="text" class="form-control pl" name="emailaddress" value="<?php echo $emailaddress ?>" placeholder="Email">
													</div>
												</div>
												<div class="form-group">
													
													<div class="col-sm-12">
													   <!-- <label for="inputPassword3" class="control-label">Password</label> -->
														<input type="password" class="form-control pl" name="password" value="<?php echo $password ?>" placeholder="Password">
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-12">
														<?php
														if ($tenant != "") {
															$db = $extension . "onebusinesstenant" . $tenant;
														} else {
															$db = $extension . "onebusinessmaster";
														}
														
														$mysqli = new mysqli($server, $user_name, $password, $database);
														$db = mysqli_real_escape_string($mysqli, $db);
														if ($stmt = $mysqli->prepare("SELECT businessid, businessname from $db.business
														where disabled='0' order by businessname asc")) {
															$stmt->execute();
															$result = $stmt->get_result();
															$numresultsbus = $result->num_rows;
															$singlebusiness = 0;
															if ($numresultsbus == 2) {
																$singlebusiness = 1;
															}
														}
														//echo "<br/>singlebusiness: ".$numresultsbus;

														$mysqli = new mysqli($server, $user_name, $password, $database);
														$db = mysqli_real_escape_string($mysqli, $db);

														if ($stmt = $mysqli->prepare("SELECT businessid, businessname from $db.business
														where disabled='0' order by businessname asc")) {
															$stmt->execute();
															$result = $stmt->get_result();
															echo "<select class='form-control' name=saveddatabase>";
															echo "<option value=''>Business</option>";
															if ($result->num_rows > 0) {
																while ($nttype = $result->fetch_assoc()) {
																	if ($type == $nttype['businessid']) {
																		echo "<option value=$nttype[businessid] selected='true'>$nttype[businessname]</option>";
																	} else {
																		if ($singlebusiness == 1) {
																			echo "<option value=$nttype[businessid] selected='true'>$nttype[businessname]</option>";
																		} else {
																			echo "<option value=$nttype[businessid] >$nttype[businessname]</option>";
																		}
																	}
																}
															}
															echo "</select>";
														}
														$mysqli->close();

														?>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-0 col-sm-12">
														<input name="CSRFtoken" type="hidden" value='<?php echo $token ?>'>
														<button type='submit' name='submit' value='Submit' class="btn btn-primary submitBtn">Login</button>
														
														<div class="remember">
															<div class="left">
															<!-- <input type="checkbox" class="form-check-input" value="">&nbsp;Remember me -->
															<input type="checkbox" id="html"> <label for="html">Remember Me</label>
															</div>
															<div class="right">
															    <a href="./index.php?tenant=<?php echo $tenant ?>&t=2" type="submit">Forgot Password</a>
															</div>
														</div>
													</div>
												</div>
											</form>
										</div>
									<?php
									}
									if ($t == 2) {
										echo "<h4 class='text-center'>Forgot my password</h4>";
										if (isset($validate) && $validate != "") {
											echo "<p class='bg-danger'>" . $validate . "</p>";
										}
									?>
										<div>
											<br />
											<p class="text-center">Input your email address below, and we'll send you your password.</p>
											<form class="form-horizontal" action='index.php?tenant=<?php echo $tenant ?>&t=2' method="post">
												<div class="form-group">
													<!-- <label for="inputEmail3" class="col-sm-3 control-label">Email</label> -->
													<div class="col-sm-12">
														<input type="text" class="form-control" name="emailaddress" value="<?php echo $emailaddress ?>" placeholder="Email">
													</div>
												</div>
												<div class="form-group">
													<!-- <label for="saveddatabase" class="col-sm-3 control-label">Business</label> -->
													<div class="col-sm-12">
														<?php
														$mysqli = new mysqli($server, $user_name, $password, $database);
														if ($stmt = $mysqli->prepare("SELECT businessid, businessname from business
													where disabled='0' order by businessname asc")) {
															$stmt->execute();
															$result = $stmt->get_result();
															echo "<select class='form-control' name=saveddatabase>";
															echo "<option value=''>Select Business *</option>";
															if ($result->num_rows > 0) {
																while ($nttype = $result->fetch_assoc()) {
																	if ($type == $nttype['businessid']) {
																		echo "<option value=$nttype[businessid] selected='true'>$nttype[businessname]</option>";
																	} else {
																		echo "<option value=$nttype[businessid] >$nttype[businessname]</option>";
																	}
																}
															}
															echo "</select>";
														}
														$mysqli->close();

														?>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-offset-0 col-sm-12">
														<button type='submit' name='submit2' value='Submit' class="btn btn-primary submitBtn">Request Password</button>
														<a href="./index.php?tenant=<?php echo $tenant ?>&t=1" type="submit" class="btn btn-default submitBtn">Cancel</a>
													</div>
												</div>
											</form>
										</div>
									<?php
									}
									?>

								</div>
							</div>
							<div class="col-xs-1 col-sm-1 col-md-1">
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
		




	<?php
	} else {
		$url = './dashboard.php';
		echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL=' . $url . '">';
	}
	?>

</body>

</html>