<?php

$date3years = date('Y-m-d', strtotime('-3 days'));

//LANGUAGE COLLECTION SECTION
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (784,785,786,787,788,789,790,791,792,793,794,795)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}

$pagename = $_SERVER['REQUEST_URI'];
$pagename = str_replace("&", "xxxxxxxxxx", $pagename);

echo "<h2>Payroll Statements</h2>";
$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
$employee = isset($_POST['employee']) ? strip_tags($_POST['employee']) : '';	
//echo "searchemployeeid: ".$employee;
?>
<form class="form-horizontal" action='view.php?viewid=49' method="post">
	<div class="form-group">
		<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
			<select class='form-control' name='employee' id='employee'>
				<?php
				echo "<option value=''>".$langval784."</option>";
				$resultemployee = mysql_query("select employeeid, employeename from employee where disabled = 0");
	        	while($ntemployee = mysql_fetch_array($resultemployee)){
	          	if ($employeeid == $ntemployee['employeeid']) {
	            		echo "<option value=".$ntemployee['employeeid']." selected='true'>".$ntemployee['employeename']."</option>";
	          	}
	          	else {
	            		echo "<option value=".$ntemployee['employeeid']." >".$ntemployee['employeename']."</option>";
	          	}
	  				
	  			}
	         ?>
	       </select>
       </div>
		<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
 			<div style="text-align:center;">
   				<button type='submit' name='submit' value='Submit' class="button-primary"><?php echo $langval785 ?></button>	
   				<button type='submit' name='clear' value='Clear' class="button-secondary" onclick='<?php unset($_POST) ?>'><?php echo $langval786 ?></button>																								
 			</div>
 		</div>	
 	</div>			
</form>
<?php	
	
//Payroll Statements
$getdata = "select payrollpaymentid, payrollpaymentname, employeename, 
currencycode, amount, paiddate, payrollpaymentstatusname from payrollpayment 
inner join employee on employee.employeeid = payrollpayment.employeeid
inner join payrollpaymentstatus on payrollpaymentstatus.payrollpaymentstatusid = payrollpayment.payrollpaymentstatusid
inner join currency on currency.currencyid = payrollpayment.currencyid
where payrollpayment.disabled = 0 and (payrollpayment.datecreated >= '$date3years' or paiddate >= '$date3years')";
if($employee <> ''){
	$getdata = $getdata." and payrollpayment.employeeid = $employee";
}
$getdata = $getdata." order by payrollpaymentstatus.sortorder, employeename asc limit 500";
//echo "<br/>getdata: ".$getdata;
$getdata = mysql_query($getdata);
if(mysql_num_rows($getdata)>= 1){
	echo "<table class='table table-bordered'>";
	echo "<thead>";
	echo "<td><b>".$langval787."</b></td>";
	echo "<td><b>".$langval788."</b></td>";
	echo "<td><b>".$langval789."</b></td>";
	echo "<td><b>".$langval790."</b></td>";
	echo "<td><b>".$langval791."</b></td>";
	echo "<td><b>".$langval792."</b></td>";
	echo "<td><b>".$langval793."</b></td>";
	echo "<td><b>".$langval794."</b></td>";
	echo "<td><b>".$langval795."</b></td>";
	echo "</thead>";
	while($datarow = mysql_fetch_array($getdata)){
		$col1 = $datarow['payrollpaymentid'];
		$col2 = $datarow['payrollpaymentname'];
		$col3 = $datarow['employeename'];
		$col4 = $datarow['currencycode'];
		$col5 = $datarow['amount'];
		$col6 = $datarow['paiddate'];
		$col7 = $datarow['payrollpaymentstatusname'];
		echo "<tr>";
		echo "<td>".$col1."</td>";
		echo "<td>".$col2."</td>";
		echo "<td>".$col3."</td>";
		echo "<td>".$col4."</td>";
		echo "<td>".$col5."</td>";
		echo "<td>".$col6."</td>";
		echo "<td>".$col7."</td>";
		echo "<td><a href='pageedit.php?pagetype=payrollpayment&rowid=".$col1."&pagename=".$pagename."'>".$langval794."</a></td>";
		echo "<td><a href='script/onepayrollstatementmanagerpdf.php?rowid=$col1' target='_blank'>".$langval795."</a></td>";
		echo "</tr>";
	}
}
echo "</table><br/>";

?>