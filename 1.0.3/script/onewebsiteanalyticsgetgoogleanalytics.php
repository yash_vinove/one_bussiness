<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$hour = date('H');
$date = date('Y-m-d H:i:s');
echo $hour;

//get the google analytics accounts to be refreshed
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$gettracking = mysql_query("select * from $database.websiteanalyticswebsitetracking
inner join $masterdatabase.websiteanalyticswebsitetrackingapi on websiteanalyticswebsitetracking.websiteanalyticswebsitetrackingapiid 
= websiteanalyticswebsitetrackingapi.websiteanalyticswebsitetrackingapiid
where websiteanalyticswebsitetrackingapiname = 'Google Analytics' and websiteanalyticswebsitetracking.disabled = 0
order by lastdatechecked asc limit 1");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get website record from tracking: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
while($row21 = mysql_fetch_array($gettracking)){
	$websiteanalyticswebsitetrackingid = $row21['websiteanalyticswebsitetrackingid'];
	$updatetracking = mysql_query("update $database.websiteanalyticswebsitetracking 
	set lastdatechecked = '$date' 
	where websiteanalyticswebsitetrackingid = '$websiteanalyticswebsitetrackingid'");
	$apikey1 = $row21['apikey1'];
	$apikey2 = $row21['apikey2'];
	$websiteanalyticswebsitetrackingname = $row21['websiteanalyticswebsitetrackingname'];
	echo "<br/><br/>".$websiteanalyticswebsitetrackingid." - ".$websiteanalyticswebsitetrackingname;
	echo "<br/><br/>".$apikey2;
	
	
	//generate json file temporarily
	$database2 = str_replace("andrewnorth_", "", $database);
	$url = "../documents/".$database2."/service-account-credentials.json";
	$fp = fopen($url,"wb");
	fwrite($fp,$apikey2);
	fclose($fp);
	$KEY_FILE_LOCATION = '../documents/'.$database2.'/service-account-credentials.json';
	require __DIR__ .'../../googleanalytics/google-api-php-client-2.2.0/vendor/autoload.php';
	
	$test = 1;
	if($test == 1){
		//GET VISITOR OVERVIEW DATA
		$client = new Google_Client();
		$client->setApplicationName("Hello Analytics Reporting");
		$client->setAuthConfig($KEY_FILE_LOCATION);
		$client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
		$analytics = new Google_Service_Analytics($client);
		
		$date = date("Y-m-d");
		$startdate = date("Y-m-01");
		$maxdate = date ("Y-m-d", strtotime("-2 months", strtotime($startdate)));
		
		//get historical visitor overview data
		while($startdate >= $maxdate){
	    	$enddate = date ("Y-m-d", strtotime("+1 month", strtotime($startdate)));
	    	$enddate = date ("Y-m-d", strtotime("-1 day", strtotime($enddate)));
	    	//echo $startdate." - ".$enddate."<br/>";
			$results =  $analytics->data_ga->get(
				'ga:' . $apikey1,
				$startdate,
			  	$enddate,
			  	'ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:users,ga:pageviews,ga:pageviewsPerSession'
			);
			echo json_encode($results);
			if (count($results->getRows()) > 0) {
				$rows = $results->getRows();
				//echo json_encode($rows)."<br/>";	 
			 	$sessions = $rows[0][0];
			 	$bouncerate = $rows[0][1];
			 	$sessionduration = $rows[0][2];
			 	$users = $rows[0][3];
			 	$pageviews = $rows[0][4];
			 	$pageviewspersession = $rows[0][5];
				//delete existing record if there is one
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$deletevisitoroverview = mysql_query("delete from $database.websiteanalyticsgoogletrackvisitoroverview
				where createddate = '$startdate' and websiteanalyticswebsitetrackingid = '$websiteanalyticswebsitetrackingid'");
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Delete visitor overview data: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
				//update database with values
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$updatevisitoroverview = mysql_query("insert into $database.websiteanalyticsgoogletrackvisitoroverview (createddate, 
				websiteanalyticswebsitetrackingid,
				noofsessions, bouncerate, averagesessionduration, noofusers, noofpageviews, noofpageviewspersession, datecreated) values 
				('$startdate', '$websiteanalyticswebsitetrackingid', '$sessions', '$bouncerate', '$sessionduration', '$users', '$pageviews',
				'$pageviewspersession', '$date')");      
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Insert visitor overview data: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			}
			else {
			 	print "No results found.\n";
			}
			$startdate = date ("Y-m-01", strtotime("-1 month", strtotime($startdate)));
		}
	}
	
	//GET PAGEVIEW DATA
	$client = new Google_Client();
	$client->setApplicationName("Hello Analytics Reporting");
	$client->setAuthConfig($KEY_FILE_LOCATION);
	$client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
	$analytics = new Google_Service_Analytics($client);
	
	$date = date("Y-m-d");
	$startdate = date("Y-m-01");
	$maxdate = date ("Y-m-d", strtotime("-2 months", strtotime($startdate)));
	
	//get historical visitor overview data
	while($startdate >= $maxdate){
    	$enddate = date ("Y-m-d", strtotime("+1 month", strtotime($startdate)));
    	$enddate = date ("Y-m-d", strtotime("-1 day", strtotime($enddate)));
    	//echo $startdate." - ".$enddate."<br/>";
    	$params = array('dimensions' => 'ga:pagePath');
		$results =  $analytics->data_ga->get(
			'ga:' . $apikey1,
			$startdate,
		  	$enddate,
		  	'ga:pageviews,ga:uniquePageviews,ga:avgTimeOnPage',
		  	$params
		);
			
		if (count($results->getRows()) > 0) {
			//delete existing record if there is one
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$deletevisitoroverview = mysql_query("delete from $database.websiteanalyticsgoogletrackpageoverview
			where createddate = '$startdate' and websiteanalyticswebsitetrackingid = '$websiteanalyticswebsitetrackingid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Delete pageview data: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		
			$rows = $results->getRows();
			//echo json_encode($rows)."<br/>";	
			//echo "<table style='border:1px solid #A0A0A0;'>"; 
			$updatevisitoroverview = "insert into $database.websiteanalyticsgoogletrackpageoverview (
			createddate, noofpageviews, noofuniquepageviews, averagetimeonpage, websiteanalyticswebsitetrackingid, disabled, 			datecreated, masteronly, websiteanalyticsgoogletrackpageoverviewname) values ";				
			for($i=0; $i<count($results->getRows()); $i++){
			 	$pagename = $rows[$i][0];
			 	$pageviews = $rows[$i][1];
			 	$uniquepageviews = $rows[$i][2];
			 	$avgtimeonpage = $rows[$i][3];
			 	$pagename = ltrim($pagename, '/');
			 	//update database with values
				$updatevisitoroverview = $updatevisitoroverview."('$startdate', '$pageviews', '$uniquepageviews', '$avgtimeonpage',
				'$websiteanalyticswebsitetrackingid', '0', '$date', '0', '$pagename'),";    
			 	//echo "<tr><td>".$pagename."</td><td>".$pageviews."</td><td>".$uniquepageviews."</td><td>".$avgtimeonpage."</td></tr>";
		 	}
		 	$updatevisitoroverview = rtrim($updatevisitoroverview, ",");
		 	$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updatevisitoroverview = mysql_query($updatevisitoroverview);	
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Insert pageview data: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		 	//echo "</table>";
		 	
		}
		else {
		 	print "No results found.\n";
		}
		$startdate = date ("Y-m-01", strtotime("-1 month", strtotime($startdate)));
	}


	//GET AUDIENCE DEMOGRAPHICS DATA
	$client = new Google_Client();
	$client->setApplicationName("Hello Analytics Reporting");
	$client->setAuthConfig($KEY_FILE_LOCATION);
	$client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
	$analytics = new Google_Service_Analytics($client);
	
	$date = date("Y-m-d");
	$startdate = date("Y-m-01");
	$maxdate = date ("Y-m-d", strtotime("-2 months", strtotime($startdate)));
	
	//get historical visitor overview data
	while($startdate >= $maxdate){
    	$enddate = date ("Y-m-d", strtotime("+1 month", strtotime($startdate)));
    	$enddate = date ("Y-m-d", strtotime("-1 day", strtotime($enddate)));
    	//echo $startdate." - ".$enddate."<br/>";
    	$params = array('dimensions' => 'ga:userAgeBracket,ga:userGender');
		$results =  $analytics->data_ga->get(
			'ga:' . $apikey1,
			$startdate,
		  	$enddate,
		  	'ga:sessions,ga:bouncerate,ga:avgSessionDuration,ga:pageviewsPerSession',
		  	$params
		);
			
		if (count($results->getRows()) > 0) {
			//delete existing record if there is one
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$deletedemographic = mysql_query("delete from $database.websiteanalyticsgoogletrackdemographic
			where createddate = '$startdate' and websiteanalyticswebsitetrackingid = '$websiteanalyticswebsitetrackingid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Delete audience demographic data: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			$rows = $results->getRows();
			//echo json_encode($rows)."<br/><br/>";	
			//echo "<table style='border:1px solid #A0A0A0;'>"; 
			$updatevisitoroverview = "insert into $database.websiteanalyticsgoogletrackdemographic (
			createddate, agerange, sex, agerangesex, noofsessions, bouncerate, averagesessionduration, pageviewspersession, 
			websiteanalyticswebsitetrackingid, disabled, datecreated, masteronly) values ";			
			for($i=0; $i<count($results->getRows()); $i++){
			 	$agerange = $rows[$i][0];
			 	$sex = $rows[$i][1];
			 	$sex = ucfirst($sex);
				$agerangesex = $agerange." - ".$sex;				 	
			 	$sessions = $rows[$i][2];
			 	$bouncerate = $rows[$i][3];
			 	$avgsessionduration = $rows[$i][4];
			 	$pageviewspersession = $rows[$i][5];
			 	//update database with values
				$updatevisitoroverview = $updatevisitoroverview."('$startdate', '$agerange', '$sex', '$agerangesex', 
				'$sessions', '$bouncerate', '$avgsessionduration', '$pageviewspersession', 
				'$websiteanalyticswebsitetrackingid', '0', '$date', '0'),";  			
			 	//echo "<tr><td>".$agerange."</td><td>".$sex."</td><td>".$sessions."</td><td>".$bouncerate."</td><td>".$avgsessionduration."</td><td>".$pageviewspersession."</td></tr>";
		 	}
		 	
		 	$updatevisitoroverview = rtrim($updatevisitoroverview, ",");
		 	$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updatevisitoroverview = mysql_query($updatevisitoroverview);
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Update audience demographic data: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;  
		 	//echo "</table><br/><br/>";
		 	
		}
		else {
		 	print "No results found.\n";
		}
		$startdate = date ("Y-m-01", strtotime("-1 month", strtotime($startdate)));
	}


	//GET CATEGORY INTEREST OTHER DATA
	$client = new Google_Client();
	$client->setApplicationName("Hello Analytics Reporting");
	$client->setAuthConfig($KEY_FILE_LOCATION);
	$client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
	$analytics = new Google_Service_Analytics($client);
	
	$date = date("Y-m-d");
	$startdate = date("Y-m-01");
	$maxdate = date ("Y-m-d", strtotime("-2 months", strtotime($startdate)));
	
	//get historical visitor overview data
	while($startdate >= $maxdate){
    	$enddate = date ("Y-m-d", strtotime("+1 month", strtotime($startdate)));
    	$enddate = date ("Y-m-d", strtotime("-1 day", strtotime($enddate)));
    	$params = array('dimensions' => 'ga:interestOtherCategory');
    	$results =  $analytics->data_ga->get(
			'ga:' . $apikey1,
			$startdate,
		  	$enddate,
		  	'ga:sessions,ga:pageviewsPerSession',
		  	$params
		);
			
		if (count($results->getRows()) > 0) {
			//delete existing record if there is one
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$deletedemographic = mysql_query("delete from $database.websiteanalyticsgoogletrackinterestcategory
			where createddate = '$startdate' and categorytype = 'Other Category'
			and websiteanalyticswebsitetrackingid = '$websiteanalyticswebsitetrackingid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Delete visitor overview data: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			$rows = $results->getRows();
			//echo json_encode($rows)."<br/><br/>";	
			//echo "<table style='border:1px solid #A0A0A0;'>"; 
			$updatevisitoroverview = "insert into $database.websiteanalyticsgoogletrackinterestcategory (
			createddate, categorytype, categoryname, noofsessions, pageviewspersession, 
			websiteanalyticswebsitetrackingid, disabled, datecreated, masteronly) values ";
			for($i=0; $i<count($results->getRows()); $i++){
			 	$category = $rows[$i][0];
			 	$sessions = $rows[$i][1];
			 	$pageviewspersession = $rows[$i][2];
			 	//update database with values
				$updatevisitoroverview = $updatevisitoroverview."('$startdate', 'Other Category', '$category', '$sessions', '$pageviewspersession',
				'$websiteanalyticswebsitetrackingid', '0', '$date', '0'),";    
				//echo "<tr><td>".$category."</td><td>".$sessions."</td><td>".$pageviewspersession."</td></tr>";
		 	}
		 	
		 	$updatevisitoroverview = rtrim($updatevisitoroverview, ",");
		 	$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updatevisitoroverview = mysql_query($updatevisitoroverview);
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Update category interest data: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;  
		 	//echo "</table><br/><br/>";			 	
		}
		else {
		 	print "No results found.\n";
		}
		$startdate = date ("Y-m-01", strtotime("-1 month", strtotime($startdate)));
	}



	//GET CATEGORY INTEREST AFFINITY DATA
	$client = new Google_Client();
	$client->setApplicationName("Hello Analytics Reporting");
	$client->setAuthConfig($KEY_FILE_LOCATION);
	$client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
	$analytics = new Google_Service_Analytics($client);
	
	$date = date("Y-m-d");
	$startdate = date("Y-m-01");
	$maxdate = date ("Y-m-d", strtotime("-2 months", strtotime($startdate)));
	
	//get historical visitor overview data
	while($startdate >= $maxdate){
    	$enddate = date ("Y-m-d", strtotime("+1 month", strtotime($startdate)));
    	$enddate = date ("Y-m-d", strtotime("-1 day", strtotime($enddate)));
    	$params = array('dimensions' => 'ga:interestAffinityCategory');
    	$results =  $analytics->data_ga->get(
			'ga:' . $apikey1,
			$startdate,
		  	$enddate,
		  	'ga:sessions,ga:pageviewsPerSession',
		  	$params
		);
			
		if (count($results->getRows()) > 0) {
			//delete existing record if there is one
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$deletedemographic = mysql_query("delete from $database.websiteanalyticsgoogletrackinterestcategory
			where createddate = '$startdate' and categorytype = 'Affinity Category'
			and websiteanalyticswebsitetrackingid = '$websiteanalyticswebsitetrackingid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Delete interest infinity data: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			$rows = $results->getRows();
			//echo json_encode($rows)."<br/><br/>";	
			//echo "<table style='border:1px solid #A0A0A0;'>"; 
			$updatevisitoroverview = "insert into $database.websiteanalyticsgoogletrackinterestcategory (
			createddate, categorytype, categoryname, noofsessions, pageviewspersession, 
			websiteanalyticswebsitetrackingid, disabled, datecreated, masteronly) values ";
			for($i=0; $i<count($results->getRows()); $i++){
			 	$category = $rows[$i][0];
			 	$sessions = $rows[$i][1];
			 	$pageviewspersession = $rows[$i][2];
			 	//update database with values
				$updatevisitoroverview = $updatevisitoroverview."('$startdate', 'Affinity Category', '$category', '$sessions', '$pageviewspersession',
				'$websiteanalyticswebsitetrackingid', '0', '$date', '0'),"; 
				
			 	//echo "<tr><td>".$category."</td><td>".$sessions."</td><td>".$pageviewspersession."</td></tr>";
		 	}
		 	$updatevisitoroverview = rtrim($updatevisitoroverview, ",");
		 	$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updatevisitoroverview = mysql_query($updatevisitoroverview);
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Update interest affinity data: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;  
		 	//echo "</table><br/><br/>";			 	
		}
		else {
		 	print "No results found.\n";
		}
		$startdate = date ("Y-m-01", strtotime("-1 month", strtotime($startdate)));
	}



	//GET CATEGORY INTEREST INMARKET DATA
	$client = new Google_Client();
	$client->setApplicationName("Hello Analytics Reporting");
	$client->setAuthConfig($KEY_FILE_LOCATION);
	$client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
	$analytics = new Google_Service_Analytics($client);
	
	$date = date("Y-m-d");
	$startdate = date("Y-m-01");
	$maxdate = date ("Y-m-d", strtotime("-2 months", strtotime($startdate)));
	
	//get historical visitor overview data
	while($startdate >= $maxdate){
    	$enddate = date ("Y-m-d", strtotime("+1 month", strtotime($startdate)));
    	$enddate = date ("Y-m-d", strtotime("-1 day", strtotime($enddate)));
    	$params = array('dimensions' => 'ga:interestInMarketCategory');
		$results =  $analytics->data_ga->get(
			'ga:' . $apikey1,
			$startdate,
		  	$enddate,
		  	'ga:sessions,ga:pageviewsPerSession',
		  	$params
		);
			
		if (count($results->getRows()) > 0) {
			//delete existing record if there is one
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$deletedemographic = mysql_query("delete from $database.websiteanalyticsgoogletrackinterestcategory
			where createddate = '$startdate' and categorytype = 'InMarket Category'
			and websiteanalyticswebsitetrackingid = '$websiteanalyticswebsitetrackingid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Delete category interest inmarket data: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			$rows = $results->getRows();
			//echo json_encode($rows)."<br/><br/>";	
			//echo "<table style='border:1px solid #A0A0A0;'>"; 
			$updatevisitoroverview = "insert into $database.websiteanalyticsgoogletrackinterestcategory (
			createddate, categorytype, categoryname, noofsessions, pageviewspersession, 
			websiteanalyticswebsitetrackingid, disabled, datecreated, masteronly) values ";
			for($i=0; $i<count($results->getRows()); $i++){
			 	$category = $rows[$i][0];
			 	$sessions = $rows[$i][1];
			 	$pageviewspersession = $rows[$i][2];
			 	//update database with values
				$updatevisitoroverview = $updatevisitoroverview."('$startdate', 'InMarket Category', '$category', '$sessions', '$pageviewspersession',
				'$websiteanalyticswebsitetrackingid', '0', '$date', '0'),";    
				
			 	//echo "<tr><td>".$category."</td><td>".$sessions."</td><td>".$pageviewspersession."</td></tr>";
		 	}
		 	$updatevisitoroverview = rtrim($updatevisitoroverview, ",");
		 	$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updatevisitoroverview = mysql_query($updatevisitoroverview);
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Update category interest inmarket data: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;  
		 	//echo "</table><br/><br/>";			 	
		}
		else {
		 	print "No results found.\n";
		}
		$startdate = date ("Y-m-01", strtotime("-1 month", strtotime($startdate)));
	}



	//GET DEVICE DATA
	$client = new Google_Client();
	$client->setApplicationName("Hello Analytics Reporting");
	$client->setAuthConfig($KEY_FILE_LOCATION);
	$client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
	$analytics = new Google_Service_Analytics($client);
	
	$date = date("Y-m-d");
	$startdate = date("Y-m-01");
	$maxdate = date ("Y-m-d", strtotime("-2 months", strtotime($startdate)));
	
	//get historical visitor overview data
	while($startdate >= $maxdate){
    	$enddate = date ("Y-m-d", strtotime("+1 month", strtotime($startdate)));
    	$enddate = date ("Y-m-d", strtotime("-1 day", strtotime($enddate)));
    	$params = array('dimensions' => 'ga:deviceCategory');
		$results =  $analytics->data_ga->get(
			'ga:' . $apikey1,
			$startdate,
		  	$enddate,
		  	'ga:sessions',
		  	$params
		);
			
		if (count($results->getRows()) > 0) {
			//delete existing record if there is one
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$deletedevice = mysql_query("delete from $database.websiteanalyticsgoogletrackdevicecategory
			where createddate = '$startdate' and websiteanalyticswebsitetrackingid = '$websiteanalyticswebsitetrackingid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Delete device data: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			$rows = $results->getRows();
			//echo json_encode($rows)."<br/><br/>";	
			//echo "<table style='border:1px solid #A0A0A0;'>"; 
			$updatevisitoroverview = "insert into $database.websiteanalyticsgoogletrackdevicecategory (
			createddate, websiteanalyticsgoogletrackdevicecategoryname, noofsessions, 
			websiteanalyticswebsitetrackingid, disabled, datecreated, masteronly) values ";
			for($i=0; $i<count($results->getRows()); $i++){
			 	$devicecategory = $rows[$i][0];
			 	$sessions = $rows[$i][1];
			 	$devicecategory = ucfirst($devicecategory);
			 	//update database with values
				$updatevisitoroverview = $updatevisitoroverview."('$startdate', '$devicecategory', '$sessions', 
				'$websiteanalyticswebsitetrackingid', '0', '$date', '0'),";    
				
			 	//echo "<tr><td>".$devicecategory."</td><td>".$sessions."</td></tr>";
		 	}
		 	$updatevisitoroverview = rtrim($updatevisitoroverview, ",");
		 	$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updatevisitoroverview = mysql_query($updatevisitoroverview);
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Update device data: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;  
		 	//echo "</table><br/><br/>";			 	
		}
		else {
		 	print "No results found.\n";
		}
		$startdate = date ("Y-m-01", strtotime("-1 month", strtotime($startdate)));
	}



	//GET DESKTOP DEVICE DATA
	$client = new Google_Client();
	$client->setApplicationName("Hello Analytics Reporting");
	$client->setAuthConfig($KEY_FILE_LOCATION);
	$client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
	$analytics = new Google_Service_Analytics($client);
	
	$date = date("Y-m-d");
	$startdate = date("Y-m-01");
	$maxdate = date ("Y-m-d", strtotime("-2 months", strtotime($startdate)));
	
	//get historical visitor overview data
	while($startdate >= $maxdate){
    	$enddate = date ("Y-m-d", strtotime("+1 month", strtotime($startdate)));
    	$enddate = date ("Y-m-d", strtotime("-1 day", strtotime($enddate)));
    	$params = array('dimensions' => 'ga:browser,ga:browserVersion,ga:operatingSystem,ga:screenResolution');
		$results =  $analytics->data_ga->get(
			'ga:' . $apikey1,
			$startdate,
		  	$enddate,
		  	'ga:sessions',
		  	$params
		);
			
		if (count($results->getRows()) > 0) {
			//delete existing record if there is one
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$deletedesktopdevice = mysql_query("delete from $database.websiteanalyticsgoogletrackdesktopdevice
			where createddate = '$startdate' and websiteanalyticswebsitetrackingid = '$websiteanalyticswebsitetrackingid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Delete desktop device data: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			$rows = $results->getRows();
			//echo json_encode($rows)."<br/><br/>";	
			//echo "<table style='border:1px solid #A0A0A0;'>"; 
			$updatedesktopdevice = "insert into $database.websiteanalyticsgoogletrackdesktopdevice (
			createddate, browser, browserversion, operatingsystem, screenresolution, noofsessions, browserbrowserversion, 
			browserscreenresolution, websiteanalyticswebsitetrackingid, disabled, datecreated, masteronly) values ";
			for($i=0; $i<count($results->getRows()); $i++){
			 	$browser = $rows[$i][0];
			 	$browserversion = $rows[$i][1];
			 	$operatingsystem = $rows[$i][2];
			 	$screenresolution = $rows[$i][3];
			 	$sessions = $rows[$i][4];
			 	$browserbrowserversion = $browser." - ".$browserversion;
			 	$browserscreenresolution = $browser." - ".$screenresolution;
			 	//update database with values
				$updatedesktopdevice = $updatedesktopdevice."('$startdate', '$browser', '$browserversion', 
				'$operatingsystem', '$screenresolution', '$sessions', '$browserbrowserversion', 
				'$browserscreenresolution', '$websiteanalyticswebsitetrackingid', '0', '$date', '0'),";    
				
			 	//echo "<tr><td>".$browser."</td><td>".$browserversion."</td><td>".$operatingsystem."</td><td>".$screenresolution."</td><td>".$sessions."</td></tr>";
		 	}
		 	$updatedesktopdevice = rtrim($updatedesktopdevice, ",");
		 	$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updatedesktopdevice = mysql_query($updatedesktopdevice);
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Update desktop device data: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;  
		 //	echo "</table><br/><br/>";			 	
		}
		else {
		 	print "No results found.\n";
		}
		$startdate = date ("Y-m-01", strtotime("-1 month", strtotime($startdate)));
	}



	//GET MOBILE DEVICE DATA
	$client = new Google_Client();
	$client->setApplicationName("Hello Analytics Reporting");
	$client->setAuthConfig($KEY_FILE_LOCATION);
	$client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
	$analytics = new Google_Service_Analytics($client);
	
	$date = date("Y-m-d");
	$startdate = date("Y-m-01");
	$maxdate = date ("Y-m-d", strtotime("-2 months", strtotime($startdate)));
	
	//get historical visitor overview data
	while($startdate >= $maxdate){
    	$enddate = date ("Y-m-d", strtotime("+1 month", strtotime($startdate)));
    	$enddate = date ("Y-m-d", strtotime("-1 day", strtotime($enddate)));
    	$params = array('dimensions' => 'ga:mobileDeviceBranding,ga:mobileDeviceModel,ga:mobileDeviceInfo');
		$results =  $analytics->data_ga->get(
			'ga:' . $apikey1,
			$startdate,
		  	$enddate,
		  	'ga:sessions',
		  	$params
		);
			
		if (count($results->getRows()) > 0) {
			//delete existing record if there is one
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$deletemobiledevice = mysql_query("delete from $database.websiteanalyticsgoogletrackmobiledevice
			where createddate = '$startdate' and websiteanalyticswebsitetrackingid = '$websiteanalyticswebsitetrackingid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Delete mobile device data: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			$rows = $results->getRows();
			//echo json_encode($rows)."<br/><br/>";	
			//echo "<table style='border:1px solid #A0A0A0;'>"; 
			$updatedesktopdevice = "insert into $database.websiteanalyticsgoogletrackmobiledevice (
			createddate, mobiledevicebranding, mobiledevicemodel, mobiledeviceinfo, noofsessions, 
			websiteanalyticswebsitetrackingid, disabled, datecreated, masteronly) values ";
			for($i=0; $i<count($results->getRows()); $i++){
			 	$mobiledevicebranding = $rows[$i][0];
			 	$mobiledevicemodel = $rows[$i][1];
			 	$mobiledeviceinfo = $rows[$i][2];
			 	$sessions = $rows[$i][3];
			 	//update database with values
			 	if($mobiledevicebranding <> "(not set)"){
					$updatedesktopdevice = $updatedesktopdevice."('$startdate', '$mobiledevicebranding', 
					'$mobiledevicemodel', '$mobiledeviceinfo', '$sessions',
					'$websiteanalyticswebsitetrackingid', '0', '$date', '0'),";    

				 	//echo "<tr><td>".$mobiledevicebranding."</td><td>".$mobiledevicemodel."</td><td>".$mobiledeviceinfo."</td><td>".$sessions."</td></tr>";
		 		}
		 	}
		 	$updatedesktopdevice = rtrim($updatedesktopdevice, ",");
		 	$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updatedesktopdevice = mysql_query($updatedesktopdevice);
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Update mobile device data: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;  
		 //echo "</table><br/><br/>";			 	
		}
		else {
		 	print "No results found.\n";
		}
		$startdate = date ("Y-m-01", strtotime("-1 month", strtotime($startdate)));
	}


	//GET SESSION LOCATION DATA
	$client = new Google_Client();
	$client->setApplicationName("Hello Analytics Reporting");
	$client->setAuthConfig($KEY_FILE_LOCATION);
	$client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
	$analytics = new Google_Service_Analytics($client);
	
	$date = date("Y-m-d");
	$startdate = date("Y-m-01");
	$maxdate = date ("Y-m-d", strtotime("-2 months", strtotime($startdate)));
	
	//get historical visitor overview data
	while($startdate >= $maxdate){
    	$enddate = date ("Y-m-d", strtotime("+1 month", strtotime($startdate)));
    	$enddate = date ("Y-m-d", strtotime("-1 day", strtotime($enddate)));
    	$params = array('dimensions' => 'ga:continent,ga:country,ga:region,ga:metro,ga:city,ga:latitude,ga:longitude');
		$results =  $analytics->data_ga->get(
			'ga:' . $apikey1,
			$startdate,
		  	$enddate,
		  	'ga:sessions,ga:avgSessionDuration,ga:pageViews,ga:pageviewsPerSession',
		  	$params
		);
			
		if (count($results->getRows()) > 0) {
			//delete existing record if there is one
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$deletelocation = mysql_query("delete from $database.websiteanalyticsgoogletracklocation
			where createddate = '$startdate' and websiteanalyticswebsitetrackingid = '$websiteanalyticswebsitetrackingid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Delete session location data: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			$rows = $results->getRows();
			//echo json_encode($rows)."<br/><br/>";	
			//echo "<table style='border:1px solid #A0A0A0;'>"; 
			$updatelocation = "insert into $database.websiteanalyticsgoogletracklocation (
			createddate, continent, country, region, metro, city, latitude, longitude, noofsessions, avgsessionduration, pageviews,
			pageviewspersession, websiteanalyticswebsitetrackingid, disabled, datecreated, masteronly) values ";
			for($i=0; $i<count($results->getRows()); $i++){
			 	$continent = $rows[$i][0];
			 	$country = $rows[$i][1];
			 	$region = $rows[$i][2];
			 	$metro = $rows[$i][3];
			 	$city = $rows[$i][4];
			 	$latitude = $rows[$i][5];
			 	$longitude = $rows[$i][6];
			 	$sessions = $rows[$i][7];
			 	$avgsessionduration = $rows[$i][8];
			 	$pageviews = $rows[$i][9];
			 	$pageviewspersession = $rows[$i][10];
			 	$continent = str_replace("(not set)", "Unknown", $continent);
			 	$country = str_replace("(not set)", "Unknown", $country);
			 	$region = str_replace("(not set)", "Unknown", $region);
			 	$metro = str_replace("(not set)", "Unknown", $metro);
			 	$city = str_replace("(not set)", "Unknown", $city);
			 	//update database with values
			 	$updatelocation = $updatelocation."('$startdate', '$continent', '$country', '$region', '$metro', 
				'$city', '$latitude', '$longitude', '$sessions', '$avgsessionduration', 
				'$pageviews', '$pageviewspersession', '$websiteanalyticswebsitetrackingid', '0', '$date', '0'),";    
				
			 	//echo "<tr><td>".$continent."</td><td>".$country."</td><td>".$region."</td><td>".$metro."</td><td>".$city."</td><td>".$latitude."</td><td>".$longitude."</td><td>".$sessions."</td><td>".$avgsessionduration."</td><td>".$pageviews."</td><td>".$pageviewspersession."</td></tr>";			 		
		 	}
		 	$updatelocation = rtrim($updatelocation, ",");
		 	$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updatelocation = mysql_query($updatelocation);
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Update session location data: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;  
		 	//echo "</table><br/><br/>";			 	
		}
		else {
		 	print "No results found.\n";
		}
		$startdate = date ("Y-m-01", strtotime("-1 month", strtotime($startdate)));
	}



	//GET SESSION SOURCE DATA
	$client = new Google_Client();
	$client->setApplicationName("Hello Analytics Reporting");
	$client->setAuthConfig($KEY_FILE_LOCATION);
	$client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
	$analytics = new Google_Service_Analytics($client);
	
	$date = date("Y-m-d");
	$startdate = date("Y-m-01");
	$maxdate = date ("Y-m-d", strtotime("-2 months", strtotime($startdate)));
	
	//get historical visitor overview data
	while($startdate >= $maxdate){
    	$enddate = date ("Y-m-d", strtotime("+1 month", strtotime($startdate)));
    	$enddate = date ("Y-m-d", strtotime("-1 day", strtotime($enddate)));
    	$params = array('dimensions' => 'ga:source,ga:medium');
		$results =  $analytics->data_ga->get(
			'ga:' . $apikey1,
			$startdate,
		  	$enddate,
		  	'ga:sessions',
		  	$params
		);
			
		if (count($results->getRows()) > 0) {
			//delete existing record if there is one
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$deletelocation = mysql_query("delete from $database.websiteanalyticsgoogletracktrafficsource
			where createddate = '$startdate' and websiteanalyticswebsitetrackingid = '$websiteanalyticswebsitetrackingid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Delete session source data: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			$rows = $results->getRows();
			//echo json_encode($rows)."<br/><br/>";	
			echo "<table style='border:1px solid #A0A0A0;'>"; 
			$updatelocation = "insert into $database.websiteanalyticsgoogletracktrafficsource (
			createddate, source, medium, noofsessions, websiteanalyticswebsitetrackingid, disabled, datecreated, masteronly) values ";
			for($i=0; $i<count($results->getRows()); $i++){
			 	$source = $rows[$i][0];
			 	$medium = $rows[$i][1];
			 	$sessions = $rows[$i][2];
			 	$source = str_replace("(direct)", "Direct", $source);
			 	$medium = str_replace("(none)", "Direct", $medium);
			 	$medium = ucfirst($medium);
			 	//update database with values
			 	$updatelocation = $updatelocation."('$startdate', '$source', '$medium', 
				'$sessions', '$websiteanalyticswebsitetrackingid', '0', '$date', '0'),";    
			 	echo "<tr><td>".$source."</td><td>".$medium."</td><td>".$sessions."</td></tr>";			 		
		 	}
		 	$updatelocation = rtrim($updatelocation, ",");
		 	$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updatelocation = mysql_query($updatelocation);
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Update session source data: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;  
		 echo "</table><br/><br/>";			 	
		}
		else {
		 	print "No results found.\n";
		}
		$startdate = date ("Y-m-01", strtotime("-1 month", strtotime($startdate)));
	}
			
	
	
	//delete json file
	unlink($url);
}

	
?>


