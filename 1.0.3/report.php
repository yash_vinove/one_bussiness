<?php session_start(); ?>
<?php include('config.php'); ?>
<?php include('sessionconfig.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<link href="./fontAwesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="screen" title="stylesheet" />
	<title>OneBusiness</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
 	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">  
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel='stylesheet' type='text/css' href='style.php' />
</head>
  
<body class='body'>

<?php include_once ('headerthree.php');	?>
	

<?php
//LANGUAGE COLLECTION SECTION
$mysqli = new mysqli($server, $user_name, $password, $database);
$languageid = mysqli_real_escape_string($mysqli, $_SESSION['languageid']);
if($stmt = $mysqli->prepare("SELECT languagerecordid, languagerecordtextname FROM $masterdatabase.languagerecordtext 
where languageid = ? and languagerecordid in (5,8,9,10,11)")){
	$stmt->bind_param('i', $languageid);
	$stmt->execute();
	$result = $stmt->get_result();
	while ($row = $result->fetch_assoc()){
		$langid = $row['languagerecordid'];
		${"langval$langid"} = $row['languagerecordtextname'];
		//echo "<br/>".$langid." -".${"langval$langid"};
	}
}
	
$reportsectionid = isset($_GET['reportsectionid']) ? $_GET['reportsectionid'] : '';
$reportsectionid = mysql_real_escape_string($reportsectionid);

$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
if(!isset($_POST['submit'])) {
	$mysqli = new mysqli($server, $user_name, $password, $database);	$reportsectionid = mysqli_real_escape_string($mysqli, $reportsectionid);	if($stmt = $mysqli->prepare("select * from reportsection 
	where reportsection.reportsectionid = ?")){	   $stmt->bind_param('i', $reportsectionid);	   $stmt->execute();	   $result = $stmt->get_result();	   if($result->num_rows > 0){	    	while($row77 = $result->fetch_assoc()){	     		$startdate = $row77['defaultstartdate'];
				$enddate = $row77['defaultenddate'];	    	}	   }	}	$mysqli->close();
	$time = date("Y-m-d"); 
} 	
else {
	$startdate = isset($_POST['startdate']) ? $_POST['startdate'] : '';
	$enddate = isset($_POST['enddate']) ? $_POST['enddate'] : '';		
	$dashboardadditionalfilter = '';
	for($i = 1; $i <= 100; $i++) {
  		${"structurefieldname$i"} = '';
  	}	   		
	for($i = 1; $i <= 100; $i++) {
  		${"data$i"} =  '';
	}	   		
	$number10 = 1;
	$reportadditionalfilter = '';
 	foreach ($_POST as $key => $value) {
 		if($value <> "Submit" && $key <> "startdate" && $key <> "enddate") {
        	${"structurefieldname$number10"} = $key;
        	${"data$number10"} = $value;
        	
        	$mysqli = new mysqli($server, $user_name, $password, $database);			$reportsectionid = mysqli_real_escape_string($mysqli, $reportsectionid);			${"structurefieldname$number10"} = mysqli_real_escape_string($mysqli, ${"structurefieldname$number10"});			if($stmt = $mysqli->prepare("select * from $database.reportsectionfilter
			inner join $database.structurefield on reportsectionfilter.fieldname = structurefield.structurefieldid
			inner join $masterdatabase.structurefieldtype on structurefieldtype.structurefieldtypeid = structurefield.structurefieldtypeid
			inner join $database.structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
			where reportsectionfilter.reportsection = ? and structurefieldname = ?")){			   $stmt->bind_param('is', $reportsectionid, ${"structurefieldname$number10"});			   $stmt->execute();			   $result = $stmt->get_result();			   if($result->num_rows > 0){			    	while($getdatarow = $result->fetch_assoc()){			     		$fieldtypename = $getdatarow['fieldtypename'];
			        	$tablename = $getdatarow['tablename'];
			        	$structurefieldname = $getdatarow['structurefieldname'];			    	}			   }			}			$mysqli->close();

        	if($value <> ''){
	        	if($fieldtypename == 'Text' || $fieldtypename == 'Status'){
					$reportadditionalfilter = $reportadditionalfilter." and ".$tablename.".".$structurefieldname." like '%".${"data$number10"}."%'";
				}
				if($fieldtypename == 'Table Connect' || $fieldtypename == 'Unique TableID' || ($fieldtypename == 'Checkbox' && $datasetfiltername == 1) || $fieldtypename == 'Date Select'){
					$reportadditionalfilter = $reportadditionalfilter." and ".$tablename.".".$structurefieldname." = '".${"data$number10"}."'";						
				}
			}
			$number10 = $number10+1;
    	}
 	}
	//echo $reportadditionalfilter;		
}
?>	
<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding:10px;">
	<!--header bar for desktop-->
	<div class='bodyheader'>
		<h3><?php echo $langval11 ?></h3>
	</div>
	<div class='bodycontent'>
	
	<form class="form-horizontal" action='report.php?reportsectionid=<?php echo $reportsectionid ?>' method="post">
		<div class="form-group">
			<label for="Start Date" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label"><?php echo $langval9 ?></label>
	    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
	      	<input type="date" class="form-control" name="startdate" value="<?php echo $startdate ?>">
	    	</div>
	  		<label for="End Date" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label"><?php echo $langval10 ?></label>
	    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
	      	<input type="date" class="form-control" name="enddate" value="<?php echo $enddate ?>">
	    	</div>
	    	<?php 
			
			//get list of filters
			$allowsearch = 0;
			$mysqli = new mysqli($server, $user_name, $password, $database);			$reportsectionid = mysqli_real_escape_string($mysqli, $reportsectionid);			if($stmt = $mysqli->prepare("select * from reportsectionfilter
			where reportsectionfilter.reportsection = ?")){			   $stmt->bind_param('i', $reportsectionid);			   $stmt->execute();			   $result = $stmt->get_result();
			   $allowsearch = $result->num_rows;			   			}			$mysqli->close();
			//echo $allowsearch;
			if($allowsearch >= 1){
				echo "<br/><br/>";
				$countfilter = 1;
				$querywhere = "";
				$mysqli9 = new mysqli($server, $user_name, $password, $database);				$reportsectionid = mysqli_real_escape_string($mysqli9, $reportsectionid);				if($stmt9 = $mysqli9->prepare("select * from $database.reportsectionfilter
				inner join $database.structurefield on reportsectionfilter.fieldname = structurefield.structurefieldid
				inner join $masterdatabase.structurefieldtype on structurefieldtype.structurefieldtypeid = structurefield.structurefieldtypeid
				inner join $database.structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
				where reportsectionfilter.reportsection = ?")){
					$stmt9->bind_param('i', $reportsectionid);				   $stmt9->execute();				   $result9= $stmt9->get_result();				   if($result9->num_rows > 0){				    	while($rowfilter = $result9->fetch_assoc()){				     		${'tablename'.$countfilter}	= $rowfilter['tablename'];
							${'logiccode'.$countfilter}	= 'AND';
							${'displayname'.$countfilter}	= $rowfilter['reportsectionfiltername'];
							${'tableconnect'.$countfilter}	= $rowfilter['tableconnect'];
							${'tablefieldconnect'.$countfilter}	= $rowfilter['tablefieldconnect'];
							${'tablefieldconnectparent'.$countfilter}	= $rowfilter['tablefieldconnectparent'];
							${'fieldtypename'.$countfilter}= $rowfilter['fieldtypename'];
							${'structurefieldname'.$countfilter}= $rowfilter['structurefieldname'];
							$label2 = ${'displayname'.$countfilter};
							
							$mysqli = new mysqli($server, $user_name, $password, $database);							${'structurefieldname'.$countfilter} = mysqli_real_escape_string($mysqli, ${'structurefieldname'.$countfilter});							$languageid = mysqli_real_escape_string($mysqli, $languageid);							if($stmt = $mysqli->prepare("select * from structurefieldlanguagerecordtext 
							where structurefieldname = ? and languageid = ?")){							   $stmt->bind_param('ii', ${'structurefieldname'.$countfilter}, $languageid);							   $stmt->execute();							   $result = $stmt->get_result();							   if($result->num_rows > 0){							    	while($gettranslationrow = $result->fetch_assoc()){							     		$fieldnametranslated = $gettranslationrow['structurefieldlanguagerecordtextname'];
										$label = $fieldnametranslated;											    	}							   }							}							$mysqli->close();

							if(${'fieldtypename'.$countfilter} == 'Text' || ${'fieldtypename'.$countfilter} == 'Value' ) {								
							?>
								
								<label for="<?php echo $label2 ?>" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label"><?php echo $label2 ?></label>
							    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-bottom:10px;">
							      		<input type="text" class="form-control" name="<?php echo ${'structurefieldname'.$countfilter} ?>" value="<?php echo ${'data'.$countfilter} ?>">
							    	</div>
							  	<?php	
							}
							if(${'fieldtypename'.$countfilter} == 'Checkbox' ) {								
							?>
								
								<label for="<?php echo $label2 ?>" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label"><?php echo $label2 ?></label>
							    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-bottom:10px;">
							    	<input type='hidden' value='0' name='<?php echo ${'structurefieldname'.$countfilter} ?>'>
							    	<input type='checkbox' name='<?php echo ${'structurefieldname'.$countfilter} ?>' value='1' <?php if (${'data'.$countfilter}=='1') { ?> checked='checked' <?php ;} ?> />
							      	</div>
								  	
							<?php	
							}
							if(${'fieldtypename'.$countfilter} == 'Date Select') {								
							?>
							
								<label for="<?php echo $label2 ?>" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label"><?php echo $label2 ?></label>
						    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-bottom:10px;">
						      	<input type="date" class="form-control" name="<?php echo ${'structurefieldname'.$countfilter} ?>" value="<?php echo ${'data'.$countfilter} ?>">
								</div>
			  	  				
							<?php	
							}
							if(${'fieldtypename'.$countfilter} == 'Table Connect' ) {								
							?>
							<label for="<?php echo $label2 ?>" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label"><?php echo $label2 ?></label>
						    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-bottom:10px;">
					    		  	<?php
					    		  	$tableconnect2 = strstr(${'tableconnect'.$countfilter}, '.');
									$tableconnect2 = str_replace(".", "", $tableconnect2);
									${'tableconnect'.$countfilter} = $tableconnect2;
									$idfield = $tableconnect2."id";
					    		  	$namefield = ${'tablefieldconnect'.$countfilter};
					    		  
					    		  	if(${'tablefieldconnectparent'.$countfilter}==""){
					    		  		$querytype="select * from ".${'tableconnect'.$countfilter}." where masteronly = 0 ";
					    		  		if($idfield == 'businessunitid' && $_SESSION["superadmin"] == 0){
											$querytype = $querytype."and businessunitid in (".$_SESSION["userbusinessunit"].") ";
										}
										$querytype = $querytype."order by ".${'tablefieldconnect'.$countfilter}." asc";
					    		  	}
					    		  	else {							    		  		
					    		  		$querytype="select * from ".${'tableconnect'.$countfilter}." inner join ".${'tablefieldconnectparent'.$countfilter}." where masteronly = 0 ";
					    		  		if($idfield == 'businessunitid' && $_SESSION["superadmin"] == 0){
											$querytype = $querytype."and businessunitid in (".$_SESSION["userbusinessunit"].") ";
										}
										$querytype = $querytype."order by ".${'tablefieldconnect'.$countfilter}." asc";											
									}
									
									$resulttype = mysql_query ($querytype);
						        	echo "<select class='form-control' name='".${'structurefieldname'.$countfilter}."'>";
						        	echo "<option value=''>".$langval19."</option>";
					         		while($nttype=mysql_fetch_array($resulttype)){
					             	$parenttablefieldname = explode(' ',trim(${'tablefieldconnectparent'.$countfilter}))[0];
					         			if($parenttablefieldname <> ""){
					         				$parenttablefieldname = $parenttablefieldname."name";
					         				$parenttablefieldname = $nttype[$parenttablefieldname].": ";
					         			}
					         			if (${"data$countfilter"}==$nttype[$idfield]) {
					               		echo "<option value=".$nttype[$idfield]." selected='true'>".$parenttablefieldname.$nttype[$namefield]."</option>";
					             	}
					             	else
					               		echo "<option value=".$nttype[$idfield]." >".$parenttablefieldname.$nttype[$namefield]."</option>";
					             	}
					     				echo "</select>";
					     			echo "</div>";									
								
								}
								$countfilter = $countfilter+1;
							}				    	}				   }				}			?>
	  	  	<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
	    		<div style="text-align:center;">
	      		<button type='submit' name='submit' value='Submit' class="button-primary"><?php echo $langval5 ?></button>																									
	    		</div>
	    	</div>		
		</div>
 	</form>
		
		
		<table>
		<?php
		$savedreportsectionname = "";
		$mysqli5 = new mysqli($server, $user_name, $password, $database);		$reportsectionid = mysqli_real_escape_string($mysqli5, $reportsectionid);		if($stmt5 = $mysqli5->prepare("select * from report 
		inner join reportsection on report.reportsectionid = reportsection.reportsectionid		
		where reportsection.reportsectionid = ? and report.disabled = '0' order by sortorder asc")){		   $stmt5->bind_param('i', $reportsectionid);		   $stmt5->execute();		   $result5 = $stmt5->get_result();		   if($result5->num_rows > 0){		    	while($queryrow = $result5->fetch_assoc()){		     		$reportname = $queryrow['reportname'];
					$reportsectionname = $queryrow['reportsectionname'];
					$reportsectionid = mysql_real_escape_string($queryrow['reportsectionid']);
					$reportid = mysql_real_escape_string($queryrow['reportid']);
					
					//check permissions to section
					$mysqli = new mysqli($server, $user_name, $password, $database);					$userid = mysqli_real_escape_string($mysqli, $userid);					$reportsectionid = mysqli_real_escape_string($mysqli, $reportsectionid);					if($stmt = $mysqli->prepare("select * from userrole 
					inner join permissionrolereportsection on permissionrolereportsection.permissionroleid = userrole.permissionroleid
					where permissionrolereportsection.disabled = '0' and userid = ? and reportsectionid = ?")){					   $stmt->bind_param('ii', $userid, $reportsectionid);					   $stmt->execute();					   $result = $stmt->get_result();
					   $getperm = $result->num_rows;					}					$mysqli->close();

					if($getperm>0) {
						//translate content		
						$mysqli = new mysqli($server, $user_name, $password, $database);						$reportid = mysqli_real_escape_string($mysqli, $reportid);						$languageid = mysqli_real_escape_string($mysqli, $languageid);						if($stmt = $mysqli->prepare("select * from reporttranslation 
						where reportid = ? and languageid = ?")){						   $stmt->bind_param('ii', $reportid, $languageid);						   $stmt->execute();						   $result = $stmt->get_result();						   if($result->num_rows > 0){						    	while($gettranslationrow = $result->fetch_assoc()){						     		$reportname = $gettranslationrow['reporttranslationname'];						    	}						   }						}						$mysqli->close();
	
						echo "<tr><td>".$reportname."</td><td style='padding:5px;'>";
						
						//build dataset
						$datasetrecordid = $reportid;
						$datasetrecordfield = "reportid";
						$datasetrecordtable = "report";
						include('dataset.php');
						//echo $query;
						
						//show spreadsheet button
						?>
						<form name="export" action="export.php?pagetype=<?php echo "Report: ".$reportsectionname." - ".$reportname ?>" method="post">
						  	<input class="button-primary" type="submit" value="<?php echo $langval8 ?>">
						   	<input type="hidden" value="<?php echo $query; ?>" name='query'>
						</form>
						<?php
						echo "</td></tr>";
					}		    	}		   }		}		$mysqli5->close();

		
	
		
		?>
		</table>
	</div>
	
		<br/><br/><br/>
</div>
	
	<?php include_once ('footer.php'); ?>
</body>
