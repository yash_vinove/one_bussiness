<?php

$includeurl = "fpdf181/fpdf.php";
require($includeurl);

class PDF extends FPDF
{
	// Page header
	function Header()
	{
	    // Arial bold 15
	    $this->SetFont('Arial','B',13);
	}	
	// Page footer
	function Footer()
	{
	    // Position at 1.5 cm from bottom
	    $this->SetY(-15);
	    // Arial italic 8
	    $this->SetFont('Arial','I',8);
	    // Page number
	    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	}
}

$getrecord = mysql_query("select * from $database.websitesearchkeyword where websitesearchkeywordid = '$recordid'");
$getrecordrow = mysql_fetch_array($getrecord);
$websitesearchanalysisid = $getrecordrow['websitesearchanalysisid'];
$chosenkeyword1 = strtolower($getrecordrow['chosenkeyword1']);
$chosenkeyword2 = strtolower($getrecordrow['chosenkeyword2']);
$chosenkeyword3 = strtolower($getrecordrow['chosenkeyword3']);
$chosenkeyword4 = strtolower($getrecordrow['chosenkeyword4']);
$chosenkeyword5 = strtolower($getrecordrow['chosenkeyword5']);
$chosenkeyword6 = strtolower($getrecordrow['chosenkeyword6']);
$chosenkeyword7 = strtolower($getrecordrow['chosenkeyword7']);
$chosenkeyword8 = strtolower($getrecordrow['chosenkeyword8']);
$chosenkeyword9 = strtolower($getrecordrow['chosenkeyword9']);
$chosenkeyword10 = strtolower($getrecordrow['chosenkeyword10']);

if($chosenkeyword1 <> ''){
	//scan website
	$getwebsite = mysql_query("select * from $database.websitesearchanalysis 
	inner join $database.website on website.websiteid = websitesearchanalysis.onewebsiteid
	where websitesearchanalysisid = '$websitesearchanalysisid'");
	$getwebsiterow = mysql_fetch_array($getwebsite);
	$topurl = $getwebsiterow['websiteurl'];
	if (!preg_match("~^(?:f|ht)tps?://~i", $topurl)) {
     $topurl = "http://" . $topurl;
 	}
    
	//get sitemap record
	$check3 = mysql_query("select * from $database.websitesearchsitemap where websitesearchanalysisid = '$websitesearchanalysisid'");
	$check3row = mysql_fetch_array($check3);
	$websitesearchsitemapid = $check3row['websitesearchsitemapid'];	
	
	//run the sitemap creation + keyword density checker
	//scan website
	require('onewebsitesearchanalysisdetailactionscankeyword.php');
	
	$date = date('Y-m-d');
	
	//create document record in structurefunctiondocument
	$recordname = $websitesearchsitemapid." - Sitemap.xml";
	$docurl = $database."/sf/websitesearchsitemap/".$websitesearchsitemapid." - Sitemap.xml";
	$insertdocrecord = mysql_query("insert into $database.structurefunctiondocument (structurefunctiondocumentname, datecreated, 
	documenturl, doctypename, structurefunctionid, rowid) values ('$recordname', '$date', '$docurl', 
	'Sitemap', '225', '$websitesearchsitemapid')");
		
	
	//query the keyword density checker to get values for my chosen keywords
	foreach ($keywordsSortedArray[0] as $keyword => $keywordData){	
		if($keyword == $chosenkeyword1){
			$keywordoccurance1 = $keywordData[0];
			$keyworddensitypercentage1 = $keywordData[1];
		}
		if($keyword == $chosenkeyword2){
			$keywordoccurance2 = $keywordData[0];
			$keyworddensitypercentage2 = $keywordData[1];
		}
		if($keyword == $chosenkeyword3){
			$keywordoccurance3 = $keywordData[0];
			$keyworddensitypercentage3 = $keywordData[1];
		}
		if($keyword == $chosenkeyword4){
			$keywordoccurance4 = $keywordData[0];
			$keyworddensitypercentage4 = $keywordData[1];
		}
		if($keyword == $chosenkeyword5){
			$keywordoccurance5 = $keywordData[0];
			$keyworddensitypercentage5 = $keywordData[1];
		}
		if($keyword == $chosenkeyword6){
			$keywordoccurance6 = $keywordData[0];
			$keyworddensitypercentage6 = $keywordData[1];
		}
		if($keyword == $chosenkeyword7){
			$keywordoccurance7 = $keywordData[0];
			$keyworddensitypercentage7 = $keywordData[1];
		}
		if($keyword == $chosenkeyword8){
			$keywordoccurance8 = $keywordData[0];
			$keyworddensitypercentage8 = $keywordData[1];
		}
		if($keyword == $chosenkeyword9){
			$keywordoccurance9 = $keywordData[0];
			$keyworddensitypercentage9 = $keywordData[1];
		}
		if($keyword == $chosenkeyword10){
			$keywordoccurance10 = $keywordData[0];
			$keyworddensitypercentage10 = $keywordData[1];
		}
	}
	
	if(!isset($keywordoccurance1)){
		$keywordoccurance1 = 0;	
		$keyworddensitypercentage1 = 0;	
	}
	if(!isset($keywordoccurance2)){
		$keywordoccurance2 = 0;	
		$keyworddensitypercentage2 = 0;	
	}
	if(!isset($keywordoccurance3)){
		$keywordoccurance3 = 0;	
		$keyworddensitypercentage3 = 0;	
	}
	if(!isset($keywordoccurance4)){
		$keywordoccurance4 = 0;	
		$keyworddensitypercentage4 = 0;	
	}
	if(!isset($keywordoccurance5)){
		$keywordoccurance5 = 0;	
		$keyworddensitypercentage5 = 0;	
	}
	if(!isset($keywordoccurance6)){
		$keywordoccurance6 = 0;	
		$keyworddensitypercentage6 = 0;	
	}
	if(!isset($keywordoccurance7)){
		$keywordoccurance7 = 0;	
		$keyworddensitypercentage7 = 0;	
	}
	if(!isset($keywordoccurance8)){
		$keywordoccurance8 = 0;	
		$keyworddensitypercentage8 = 0;	
	}
	if(!isset($keywordoccurance9)){
		$keywordoccurance9 = 0;	
		$keyworddensitypercentage9 = 0;	
	}
	if(!isset($keywordoccurance10)){
		$keywordoccurance10 = 0;	
		$keyworddensitypercentage10 = 0;	
	}		
	
	//update the values in the websitesearchkeyword record
	$savekeywords = "update $database.websitesearchkeyword set 
	chosenkeyworddensitypercentage1 = '$keyworddensitypercentage1', 
	chosenkeyworddensitypercentage2 = '$keyworddensitypercentage2', 
	chosenkeyworddensitypercentage3 = '$keyworddensitypercentage3', 
	chosenkeyworddensitypercentage4 = '$keyworddensitypercentage4', 
	chosenkeyworddensitypercentage5 = '$keyworddensitypercentage5', 
	chosenkeyworddensitypercentage6 = '$keyworddensitypercentage6', 
	chosenkeyworddensitypercentage7 = '$keyworddensitypercentage7', 
	chosenkeyworddensitypercentage8 = '$keyworddensitypercentage8', 
	chosenkeyworddensitypercentage9 = '$keyworddensitypercentage9', 
	chosenkeyworddensitypercentage10 = '$keyworddensitypercentage10', 	chosenkeywordoccurance1 = '$keywordoccurance1', chosenkeywordoccurance2 = '$keywordoccurance2', 
	chosenkeywordoccurance3 = '$keywordoccurance3', chosenkeywordoccurance4 = '$keywordoccurance4', 
	chosenkeywordoccurance5 = '$keywordoccurance5', chosenkeywordoccurance6 = '$keywordoccurance6', 
	chosenkeywordoccurance7 = '$keywordoccurance7', chosenkeywordoccurance8 = '$keywordoccurance8', 
	chosenkeywordoccurance9 = '$keywordoccurance9', chosenkeywordoccurance10 = '$keywordoccurance10'
	where websitesearchanalysisid = '$websitesearchanalysisid'";
	//echo $savekeywords;
	$savekeywords = mysql_query($savekeywords);
	
	//regenerate report
	include('onewebsitesearchanalysisdetailactionkeywordsreport.php');
	$rowid = $websitesearchanalysisid;
}


?>