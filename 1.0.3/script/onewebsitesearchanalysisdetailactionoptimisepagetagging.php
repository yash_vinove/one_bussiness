<?php 

//LANGUAGE COLLECTION SECTION
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,
357,358,359,360,361,362,363,364,365,366,367,368,369,370,371)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}


//create websitesearchsitemap record
$check3 = mysql_query("select * from $database.websitesearchsitemap where websitesearchanalysisid = '$rowid'");
if(mysql_num_rows($check3) == 0){
	$createkeywordrecord = mysql_query("insert into $database.websitesearchsitemap (websitesearchsitemapname, websitesearchanalysisid,
	datecreated) values ('Sitemap', '$rowid', '$date')");	
	$websitesearchsitemapid = mysql_insert_id();
}
else {
	$check3row = mysql_fetch_array($check3);
	$websitesearchsitemapid = $check3row['websitesearchsitemapid'];
}
		
//scan website
$getwebsite = mysql_query("select * from $database.websitesearchanalysis 
inner join $database.website on website.websiteid = websitesearchanalysis.onewebsiteid
where websitesearchanalysisid = '$rowid'");
$getwebsiterow = mysql_fetch_array($getwebsite);
$topurl = $getwebsiterow['websiteurl'];
$excludedurls = $getwebsiterow['excludedwebaddresses'];
$slash = substr($topurl, -1);
if($slash <> "/"){
	$topurl = $getwebsiterow['websiteurl']."/";
}
if (!preg_match("~^(?:f|ht)tps?://~i", $topurl)) {
  $topurl = "http://" . $topurl;
}
 
//create websitesearchsitemap record
	$check3 = mysql_query("select * from $database.websitesearchsitemap where websitesearchanalysisid = '$rowid'");
	if(mysql_num_rows($check3) == 0){
		$createkeywordrecord = mysql_query("insert into $database.websitesearchsitemap (websitesearchsitemapname, websitesearchanalysisid,
		datecreated) values ('Sitemap', '$rowid', '$date')");	
		$websitesearchsitemapid = mysql_insert_id();
	}
	else {
		$check3row = mysql_fetch_array($check3);
		$websitesearchsitemapid = $check3row['websitesearchsitemapid'];
	}
	
//check if onewebsite storage folder exists, if not, then create
$filedir = '../documents/'. $database."/sf/websitesearchsitemap/";
if(file_exists($filedir)){
}
else {
	$file_directory = "../documents/".$database."/sf/websitesearchsitemap/";
	mkdir($file_directory);
}

$file="../documents/".$database."/sf/websitesearchsitemap/".$websitesearchsitemapid." - Sitemap.xml";

   	
//create sitemap file
$urlarray = array();
if (!file_exists($file)) {
	//create sitemap file
	include ('onewebsitesearch/xmlsitemapgenerator/sitemap-generator.php');    
} 
else {
	$date = date("y-m-d");
	$datetwo = filemtime($file);
	$datediff = ($date - $datetwo) / 60 / 60;
	if($datediff <= 24){
	   	//read sitemap
	   	$scanned = array();  
		$DomDocument = new DOMDocument();
		$DomDocument->preserveWhiteSpace = false;
		$DomDocument->load($file);
		$DomNodeList = $DomDocument->getElementsByTagName('loc');
		foreach($DomNodeList as $url) {
	    	$scanned[] = $url->nodeValue;
		}
		//echo json_encode($scanned);
	}
	else {
		//create sitemap file
		include ('onewebsitesearch/xmlsitemapgenerator/sitemap-generator.php'); 
	}
}

$urlarray = $scanned;
//echo "<br/>".json_encode($urlarray)."<br/>";

//get list of pages
include('onewebsitesearch/xmlsitemapgenerator/settings.php');
include('onewebsitesearch/htmlscraper/simple_html_dom.php');
//$html = file_get_html2($topurl);
$wordString = '';
//echo "<table class='table table-bordered'>";
//echo "<thead><td>url</td><td>titletag</td><td>descriptiontag</td><td>keywordtag</td><td>authortag</td><td>copyrighttag</td></thead>";
$titletag = '';
foreach ($urlarray as $url2) {
	
	$scan = 1;
	if($topurl == $url2){
		$scan = 1;
	}
	else {
		if(strpos($excludedurls,$url2) !== false) {
  			//do not scan this url as it is excluded from scan    
  			$scan = 0;
  			//echo "<br/>not scanned: ".$url2;
		} 			
	}
	if($scan == 1){

		//get page data
		$descriptiontag = '';
		$keywordtag = '';
		$authortag = '';
		$copyrighttag = '';
	   	//check page attributes
		$html = file_get_html2($url2);
		$pageurl = $url2;
		
		//get word count
		foreach($html->find('body') as $element) {
	       $bodywords = $element->plaintext;
	       //echo $bodywords;
	  	}	
	  	foreach($html->find('head') as $element) {
	       $headwords = $element->plaintext;
	       //echo $headwords;
	  	}	
	  	$totalwords = str_word_count($bodywords) + str_word_count($headwords);
	  	
	  	//get specific meta tags for pages
		foreach($html->find('title') as $element) {
			$titletag = $element->plaintext;
	  	}	
	  	foreach($html->find('meta') as $element) {
	       $metaname = $element->name;
	       if($metaname == 'description'){
	       	$descriptiontag = $element->content;
	     	}
	     	if($metaname == 'keywords'){
	       	$keywordtag = $element->content;
	     	}
	     	if($metaname == 'author'){
	       	$authortag = $element->content;
	     	}
	  	}
	  	//echo "<tr><td>".$pageurl."</td><td>".$titletag."</td><td>".$descriptiontag."</td>";
	  	//echo "<td>".$keywordtag."</td><td>".$authortag."</td><td>".$copyrighttag."</td></tr>";
	  	
	  	//validate titletag
	  	$validationerrortitletag = '';
	  	if(strlen($titletag) > 70){
			$validationerrortitletag = $validationerrortitletag."{####} ".$langval337;  	
	  	}
	  	if(strlen($titletag) < 40){
			$validationerrortitletag = $validationerrortitletag."{####} ".$langval338;  	
	  	}
	  	if($titletag == ""){
			$validationerrortitletag = $validationerrortitletag."{####} ".$langval339;  	
	  	}
	  	
	 	//validate descriptiontag
		$validationerrordescriptiontag = '';
	  	if(strlen($descriptiontag) > 155){
			$validationerrordescriptiontag = $validationerrordescriptiontag."{####} ".$langval340;  	
	  	}
	  	if(strlen($descriptiontag) < 100){
			$validationerrordescriptiontag = $validationerrordescriptiontag."{####} ".$langval341;  	
	  	}
	  	if($descriptiontag == ""){
			$validationerrordescriptiontag = $validationerrordescriptiontag."{####} ".$langval342;  	
	  	}
	  	$getlocaltargeting = mysql_query("select * from $database.websitesearchprofile 
	  	where websitesearchanalysisid = '$rowid'");
	  	$getlocaltargetingrow = mysql_fetch_array($getlocaltargeting);
	  	$localitytargeting1 = $getlocaltargetingrow['localitytargeting1'];
	  	$localitytargeting2 = $getlocaltargetingrow['localitytargeting2'];
	  	$localitytargeting3 = $getlocaltargetingrow['localitytargeting3'];
	  	$pass = 0;
	  	if($localitytargeting1 <> ""){
	  		if (stripos($descriptiontag, $localitytargeting1) !== false) {
			}
			else{
				$pass = $pass +1;	
			}
		}
		if($localitytargeting2 <> ""){
			if (stripos($descriptiontag, $localitytargeting2) !== false) {
			}
			else{
				$pass = $pass +1;	
			}
		}
		if($localitytargeting3 <> ""){
			if (stripos($descriptiontag, $localitytargeting3) !== false) {
			}
			else{
				$pass = $pass +1;	
			}
		}
		if($pass >= 1 and $localitytargeting1 <> ""){
			$validationerrordescriptiontag = $validationerrordescriptiontag."{####} ".$langval343;  	
		}
	  	
	 	//validate keywordtag
		$validationerrorkeywordtag = '';
		$keywordcount = str_word_count($keywordtag);	
		$perc = ($keywordcount / $totalwords) / 100;
		if($keywordtag == ""){
			$validationerrorkeywordtag = $validationerrorkeywordtag."{####} ".$langval344;  	
	  	}
	  	if($perc < 2){
			$validationerrorkeywordtag = $validationerrorkeywordtag."{####} ".$langval345;  	
	  	}
	  	if($perc >= 5){
			$validationerrorkeywordtag = $validationerrorkeywordtag."{####} ".$langval346;  	
	  	}
	  	$pass = 0;
	  	if($localitytargeting1 <> ""){
		  	if (stripos($keywordtag, $localitytargeting1) !== false) {
			}
			else{
				$pass = $pass +1;	
			}
		}
		if($localitytargeting2 <> ""){
			if (stripos($keywordtag, $localitytargeting2) !== false) {
			}
			else{
				$pass = $pass +1;	
			}
		}
		if($localitytargeting3 <> ""){
			if (stripos($keywordtag, $localitytargeting3) !== false) {
			}
			else{
				$pass = $pass +1;	
			}
		}
		if($pass >=1 and $localitytargeting1 <> ""){
			$validationerrorkeywordtag = $validationerrorkeywordtag."{####} ".$langval347;  	
		}
		
	   //validate authortag
		$validationerrorauthortag = '';
		if($authortag == ""){
			$validationerrorauthortag = $validationerrorauthortag."{####} ".$langval348;  	
	  	}
	  	
	  	
	   //save the record
	  	$datecreated = date('Y-m-d');
	  	if($titletag == ""){
	  		$websitesearchtagcheckpagename = $pageurl;
	  	}
	  	else {
	  		$websitesearchtagcheckpagename = $titletag." - ".$pageurl;
	  	}
	  	$deleteexisting = mysql_query("delete from $database.websitesearchtagcheckpage 
	  	where websitesearchanalysisid = '$rowid' and pageurl = '$pageurl'");
	  	$createnew = mysql_query("insert into $database.websitesearchtagcheckpage (websitesearchtagcheckpagename, websitesearchanalysisid, 
	  	pageurl, titletag, descriptiontag, keywordtag, authortag, copyrighttag, validationerrortitletag, validationerrordescriptiontag, 
	  	validationerrorkeywordtag, validationerrorauthortag, validationerrorcopyrighttag, datecreated) 
	  	values ('$websitesearchtagcheckpagename', '$rowid', '$pageurl', '$titletag', '$descriptiontag', '$keywordtag', '$authortag', 
	  	'', '$validationerrortitletag', '$validationerrordescriptiontag', '$validationerrorkeywordtag', 
	  	'$validationerrorauthortag', '', '$datecreated')");
  	}
}

//get image content
$deleteexisting = mysql_query("delete from $database.websitesearchtagcheckpageimage
where websitesearchanalysisid = '$rowid'");
//echo "<thead><td>url</td><td>alttag</td><td>imageurl</td><td>title</td></thead>";
  
foreach ($urlarray as $url2) {
	$scan = 1;
	if($topurl == $url2){
		$scan = 1;
	}
	else {
		if(strpos($excludedurls,$url2) !== false) {
  			//do not scan this url as it is excluded from scan    
  			$scan = 0;
  			//echo "<br/>not scanned: ".$url2;
		} 			
	}
	if($scan == 1){

	  	$html = file_get_html2($url2);
		$pageurl = $url2;
		
	  	foreach($html->find('img') as $element) {
	     	$alttag = $element->alt;
	     	$imageurl = $element->src;
	     	$title = $element->title;
	     	$validationerrortitle = '';
	     	$validationerroralttag = '';
	     	$validationerrorimageurl = '';
	     	
			//validate image title
			$validationerrortitle = '';
			if($title == ""){
				$validationerrortitle = $validationerrortitle."{####} ".$langval350;  	
	  		}
	  		$titlelength = strlen($title);
	  		$titleexcludingnumbers = preg_replace('/[0-9]+/', '', $title);
			$titleexcludingnumberslength = strlen($titleexcludingnumbers);
			$perc2 = 0;
			if($titlelength <> $titleexcludingnumberslength){
	  			$perc2 = (($titlelength - $titleexcludingnumberslength) / $titlelength) * 100;
	  		}
	  		if($perc2 >50){
			  	$validationerrortitle = $validationerrortitle."{####} ".$langval351;	
	  		}  		
	  		if(strlen($title) >= 100){
				$validationerrortitle = $validationerrortitle."{####} ".$langval352;  	
		  	}
		  	if(strlen($title) <= 20 && $title <> ''){
				$validationerrortitle = $validationerrortitle."{####} ".$langval353;  	
		  	}
		  	 	
	     	//validate image alt
			$validationerroralttag = '';
			if($alttag == ""){
				$validationerroralttag = $validationerroralttag."{####} ".$langval354;  	
	  		}
	  		$alttaglength = strlen($alttag);
	  		$alttagexcludingnumbers = preg_replace('/[0-9]+/', '', $alttag);
			$alttagexcludingnumberslength = strlen($alttagexcludingnumbers);
	  		$perc3 = 0;
			if($alttaglength <> $alttagexcludingnumberslength){
	  			$perc3 = (($alttaglength - $alttagexcludingnumberslength) / $alttaglength) * 100;
	  		}  		
	  		if($perc3 > 50){
			  	$validationerroralttag = $validationerroralttag."{####} ".$langval355;	
	  		}  		
	  		if(strlen($alttag) >= 100){
				$validationerroralttag = $validationerroralttag."{####} ".$langval356;  	
		  	}
		  	if(strlen($alttag) <= 20 && $alttag <> ''){
				$validationerroralttag = $validationerroralttag."{####} ".$langval357;  	
		  	}
		  	
	      //validate image src
			$validationerrorimageurl = '';
			$imagebaseurl = basename($imageurl);
			$imageurllength = strlen($imagebaseurl);
	  		$imageurlexcludingnumbers = preg_replace('/[0-9]+/', '', $imagebaseurl);
			$imageurlexcludingnumberslength = strlen($imageurlexcludingnumbers);
	  		$perc4 = 0;
			if($imageurllength <> $imageurlexcludingnumberslength){
	  			$perc4 = (($imageurllength - $imageurlexcludingnumberslength) / $imageurllength) * 100;
	  		}
	  		if($perc4 > 50){
			  	$validationerrorimageurl = $validationerrorimageurl."{####} ".$langval358;	
	  		}  		
	  		if(strlen($imagebaseurl) >= 100){
				$validationerrorimageurl = $validationerrorimageurl."{####} ".$langval359;  	
		  	}
		  	if(strlen($imagebaseurl) <= 20 && $imageurl <> ''){
				$validationerrorimageurl = $validationerrorimageurl."{####} ".$langval360;  	
		  	}
		  	
	     	//insert record
	     	if($title <> ""){
				$websitesearchtagcheckpageimagename = $title;     	
	     	}
	     	else {
	     		if($imageurl <> ""){
					$websitesearchtagcheckpageimagename = $imageurl;	
	     		}
	     		else {
					$websitesearchtagcheckpageimagename = $alttag;     		
	     		}
	     	}
	     	//echo "<tr><td>".$pageurl."</td><td>".$alttag."</td><td>".$imageurl."</td><td>".$title."</td></tr>";	
	     	$getpage = mysql_query("select * from $database.websitesearchtagcheckpage 
	     	where websitesearchanalysisid = '$rowid' and pageurl = '$pageurl'");
	     	$getpagerow = mysql_fetch_array($getpage);
	     	$websitesearchtagcheckpageid = $getpagerow['websitesearchtagcheckpageid'];
	     	$createnewimage = mysql_query("insert into $database.websitesearchtagcheckpageimage (websitesearchtagcheckpageimagename, 
	     	websitesearchanalysisid, websitesearchtagcheckpageid, imageurl, title, alttag, validationerrortitle, validationerroralttag, 
	     	validationerrorimageurl, datecreated) 
	     	values ('$websitesearchtagcheckpageimagename', '$rowid', '$websitesearchtagcheckpageid', '$imageurl', '$title', '$alttag', 
	     	'$validationerrortitle', '$validationerroralttag', '$validationerrorimageurl', '$datecreated')");    	
  		}
  	}
}

//get video content
$deleteexisting = mysql_query("delete from $database.websitesearchtagcheckpagevideo
where websitesearchanalysisid = '$rowid'");
//echo "<thead><td>url</td><td>htag</td><td>title</td><td>src</td></thead>";

foreach ($urlarray as $url2) { 	
  	$scan = 1;
	if($topurl == $url2){
		$scan = 1;
	}
	else {
		if(strpos($excludedurls,$url2) !== false) {
  			//do not scan this url as it is excluded from scan    
  			$scan = 0;
  			//echo "<br/>not scanned: ".$url2;
		} 			
	}
	if($scan == 1){

		$html = file_get_html2($url2);
		$pageurl = $url2;
		
		foreach($html->find('video') as $element) {
			$htag = $element;
			$htag = substr($htag, 0, strpos($htag, "</video>"));	
			$htag = strip_tags($htag);
			$htag = trim($htag);
	  		$title = $element->title;	
	  		$src = $element;
	  		$src = substr($src, strpos($src, "src=") + 5);
	  		$src = strtok($src, '.');
	  		//echo "<tr><td>".$pageurl."</td><td>".$htag."</td><td>".$title."</td><td>".$src."</td></tr>";	
	  		
	  		//validate video title
			$validationerrortitle = '';
			if($title == ""){
				$validationerrortitle = $validationerrortitle."{####} ".$langval361;  	
	  		}
	  		$titlelength = strlen($title);
	  		$titleexcludingnumbers = preg_replace('/[0-9]+/', '', $title);
			$titleexcludingnumberslength = strlen($titleexcludingnumbers);
			$perc6 = 0;
			if($titlelength <> $titleexcludingnumberslength){
	  			$perc6 = (($titlelength - $titleexcludingnumberslength) / $titlelength) * 100;
	  		}
	  		if($perc6 >50){
			  	$validationerrortitle = $validationerrortitle."{####} ".$langval362;	
	  		}  		
	  		if(strlen($title) >= 100){
				$validationerrortitle = $validationerrortitle."{####} ".$langval363;  	
		  	}
		  	if(strlen($title) <= 20 && $title <> ''){
				$validationerrortitle = $validationerrortitle."{####} ".$langval364;  	
		  	}
		  	
			//validate video htag
			$validationerrorhtag = '';
			if($htag == ""){
				$validationerrorhtag = $validationerrorhtag."{####} ".$langval365;  	
	  		}
	  		$htaglength = strlen($htag);
	  		$htagexcludingnumbers = preg_replace('/[0-9]+/', '', $htag);
			$htagexcludingnumberslength = strlen($htagexcludingnumbers);
			$perc7 = 0;
			if($htaglength <> $htagexcludingnumberslength){
	  			$perc7 = (($htaglength - $htagexcludingnumberslength) / $htaglength) * 100;
	  		}
	  		if($perc7 >50){
			  	$validationerrorhtag = $validationerrorhtag."{####} ".$langval366;	
	  		}  		
	  		if(strlen($htag) >= 100){
				$validationerrorhtag = $validationerrorhtag."{####} ".$langval367;  	
		  	}
		  	if(strlen($htag) <= 20 && $htag <> ''){
				$validationerrorhtag = $validationerrorhtag."{####} ".$langval368;  	
		  	}
		  	
	  		//validate video src
			$validationerrorsrc = '';
			$videobaseurl = basename($src);
			$videourllength = strlen($videobaseurl);
	  		$videourlexcludingnumbers = preg_replace('/[0-9]+/', '', $videobaseurl);
			$videourlexcludingnumberslength = strlen($videourlexcludingnumbers);
	  		$perc5 = 0;
			if($videourllength <> $videourlexcludingnumberslength){
	  			$perc5 = (($videourllength - $videourlexcludingnumberslength) / $videourllength) * 100;
	  		}
	  		if($perc5 > 50){
			  	$validationerrorsrc = $validationerrorsrc."{####} ".$langval369;	
	  		}  		
	  		if(strlen($videobaseurl) >= 100){
				$validationerrorsrc = $validationerrorsrc."{####} ".$langval370;  	
		  	}
		  	if(strlen($videobaseurl) <= 20 && $imageurl <> ''){
				$validationerrorsrc = $validationerrorsrc."{####} ".$langval371;  	
		  	}
		  	
	  		//insert record
	     	if($title <> ""){
				$websitesearchtagcheckpagevideoname = $title;     	
	     	}
	     	else {
	     		$websitesearchtagcheckpagevideoname = $src;	
	     	}
	  		$getpage = mysql_query("select * from $database.websitesearchtagcheckpage 
	     	where websitesearchanalysisid = '$rowid' and pageurl = '$pageurl'");
	     	$getpagerow = mysql_fetch_array($getpage);
	     	$websitesearchtagcheckpageid = $getpagerow['websitesearchtagcheckpageid'];
	     	$createnewvideo = "insert into $database.websitesearchtagcheckpagevideo (websitesearchtagcheckpagevideoname, 
	     	websitesearchanalysisid, websitesearchtagcheckpageid, src, title, htag, validationerrorsrc, validationerrortitle, validationerrorhtag,
	     	datecreated) 
	     	values ('$websitesearchtagcheckpagevideoname', '$rowid', '$websitesearchtagcheckpageid', '$src', '$title', '$htag', 
	     	'$validationerrorsrc', '$validationerrortitle', '$validationerrorhtag', '$datecreated')";    
	     	$createnewvideo = mysql_query($createnewvideo);	    	
  		}  	
  	}  	
}
//echo "</table>";

?>