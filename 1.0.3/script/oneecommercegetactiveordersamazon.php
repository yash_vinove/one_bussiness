<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

echo "<br/><br/><b>PROCESS AMAZON ORDERS</b><br/><br/>";

$date = date('Y-m-d');
global $xml;

$sellerskudeletelist = "";
$list=getAmazonOrders();
if ($list) {
  	
  	 /**
     * Returns the full set of data for the order.
     * 
     * This method will return <b>FALSE</b> if the order data has not yet been filled.
     * The array returned will have the following fields:
     * <ul>
     * <li><b>AmazonOrderId</b> - unique ID for the order, which you sent in the first place</li>
     * <li><b>SellerOrderId</b> (optional) - your unique ID for the order</li>
     * <li><b>PurchaseDate</b> - time in ISO8601 date format</li>
     * <li><b>LastUpdateDate</b> - time in ISO8601 date format</li>
     * <li><b>OrderStatus</b> - the current status of the order, see <i>getOrderStatus</i> for more details</li>
     * <li><b>MarketplaceId</b> - the marketplace in which the order was placed</li>
     * <li><b>FulfillmentChannel</b> (optional) - "AFN" or "MFN"</li>
     * <li><b>SalesChannel</b> (optional) - sales channel for the first item in the order</li>
     * <li><b>OrderChannel</b> (optional) - order channel for the first item in the order</li>
     * <li><b>ShipServiceLevel</b> (optional) - shipment service level of the order</li>
     * <li><b>ShippingAddress</b> (optional) - array, see <i>getShippingAddress</i> for more details</li>
     * <li><b>OrderTotal</b> (optional) - array with the fields <b>Amount</b> and <b>CurrencyCode</b></li>
     * <li><b>NumberOfItemsShipped</b> (optional) - number of items shipped</li>
     * <li><b>NumberOfItemsUnshipped</b> (optional) - number of items not shipped</li>
     * <li><b>PaymentExecutionDetail</b> (optional) - multi-dimensional array, see <i>getPaymentExecutionDetail</i> for more details</li>
     * <li><b>PaymentMethod</b> (optional) - "COD", "CVS", or "Other"</li>
     * <li><b>PaymentMethodDetails</b> (optional) - array of payment detail strings</li>
     * <li><b>IsReplacementOrder</b> (optional) - "true" or "false"</li>
     * <li><b>ReplacedOrderId</b> (optional) - Amazon Order ID, only given if <i>IsReplacementOrder</i> is true</li>
     * <li><b>MarketplaceId</b> (optional) - marketplace for the order</li>
     * <li><b>BuyerName</b> (optional) - name of the buyer</li>
     * <li><b>BuyerEmail</b> (optional) - Amazon-generated email for the buyer</li>
     * <li><b>BuyerCounty</b> (optional) - county for the buyer</li>
     * <li><b>BuyerTaxInfo</b> (optional) - tax information about the buyer, see <i>getBuyerTaxInfo</i> for more details</li>
     * <li><b>ShipmentServiceLevelCategory</b> (optional) - "Expedited", "FreeEconomy", "NextDay",
     * "SameDay", "SecondDay", "Scheduled", or "Standard"</li>
     * <li><b>ShippedByAmazonTFM</b> (optional) - "true" or "false"</li>
     * <li><b>TFMShipmentStatus</b> (optional) - the status of the TFM shipment, see <i>getTfmShipmentStatus</i> for more details</li>
     * <li><b>CbaDisplayableShippingLabel</b> (optional) - customized Checkout by Amazon label of the order</li>
     * <li><b>OrderType</b> (optional) - "StandardOrder" or "Preorder"</li>
     * <li><b>EarliestShipDate</b> (optional) - time in ISO8601 date format</li>
     * <li><b>LatestShipDate</b> (optional) - time in ISO8601 date format</li>
     * <li><b>EarliestDeliveryDate</b> (optional) - time in ISO8601 date format</li>
     * <li><b>LatestDeliveryDate</b> (optional) - time in ISO8601 date format</li>
     * <li><b>IsBusinessOrder</b> (optional) - "true" or "false"</li>
     * <li><b>PurchaseOrderNumber</b> (optional) - the Purchase Order number entered by the buyer</li>
     * <li><b>IsPrime</b> (optional) - "true" or "false"</li>
     * <li><b>IsPremiumOrder</b> (optional) - "true" or "false"</li>
     * </ul>
     * @return array|boolean array of data, or <b>FALSE</b> if data not filled yet
     */
    
   	$orderarray = array();
   	foreach ($list as $order) {
   		//these are AmazonOrder objects
   		$orderid = $order->getAmazonOrderId();
   		$customeremail = $order->getBuyerEmail();
   		$customername = $order->getBuyerName();
   		$orderid = $order->getAmazonOrderId();
   		$status = $order->getOrderStatus();
   		
   		echo "<br/><br/><b>ORDER ID: ".$orderid."</b><br/>";
		echo "status: ".$status."<br/>";	
		if($status <> "Canceled"){
	 		echo "customeremail: ".$customeremail."<br/>";
			
	      	$shippingaddress = $order->getShippingAddress(); //address is an array
	     	//echo '<br><b>City:</b> '.$address['City'];
			//echo "shippingaddress: ".json_encode($shippingaddress)."<br/>";  
			if(isset($shippingaddress['Name'])){  	
	     		$customershippingname = $shippingaddress['Name'];
	     	}
	     	else {
	     		$customershippingname = "";
	     	}
	      
			$results = array();
			preg_match('#^(\w+\.)?\s*([\'\’\w]+)\s+([\'\’\w]+)\s*(\w+\.?)?$#', $customername, $results);
			echo "namearray: ".json_encode($results)."<br/>";
			if(isset($results[2])){
				$customerfirstname = $results[2];
			}
			else {
				$customerfirstname = "";		
			}
			if(isset($results[3])){
				$customerlastname = $results[3];
				if(isset($results[4])){
					$customerlastname = $customerlastname." ".$results[4];
				}
			}
			else {
				$customerlastname = "";		
			}
			if($customerfirstname == "" && $customerlastname == ""){
				$customerlastname = $customername;			
			}
			if($customershippingname <> $customername){
				$customername = $customershippingname;		
			}
			echo "customername: ".$customername."<br/>";
			echo "customerfirstname: ".$customerfirstname."<br/>";
			echo "customerlastname: ".$customerlastname."<br/>";
				
			
	     	//customer shipping details
			if(isset($shippingaddress['AddressLine1'])){  	
	     		$customershippingaddress1 = $shippingaddress['AddressLine1'];
	     	}
	     	else {
	     		$customershippingaddress1 = "";
	     	}
			if(isset($shippingaddress['AddressLine2'])){  	
	     		$customershippingaddress2 = $shippingaddress['AddressLine2'];
	     	}
	     	else {
	     		$customershippingaddress2 = "";
	     	}
			if(isset($shippingaddress['AddressLine3'])){  	
	     		$customershippingaddress3 = $shippingaddress['AddressLine3'];
	     		if($customershippingaddress2 <> "" && $customershippingaddress3 <> ""){
	     			$customershippingaddress2 = $customershippingaddress2.", ".$customershippingaddress3;
	     		}
	     		if($customershippingaddress2 == "" && $customershippingaddress3 <> ""){
	     			$customershippingaddress2 = $customershippingaddress3;
	     		}
	     	}
	     	else {
	     		$customershippingaddress3 = "";
	     	}
	     	
			if(isset($shippingaddress['City'])){  	
	     		$customershippingcity = $shippingaddress['City'];
	     	}
	     	else {
	     		$customershippingcity = "";
	     	}
	     	if(isset($shippingaddress['District'])){  	
	     		$customershippingdistrict = $shippingaddress['District'];
	     	}
	     	else {
	     		$customershippingdistrict = "";
	     	}
	     	if(isset($shippingaddress['StateOrRegion'])){  	
	     		$customershippingstate = $shippingaddress['StateOrRegion'];
	     	}
	     	else {
	     		$customershippingstate = "";
	     	}
			
			if($customershippingdistrict <> ""){
				$customershippingcity = $customershippingcity.", ".$customershippingdistrict;
			}
			
			if(isset($shippingaddress['PostalCode'])){  	
	     		$customershippingzip = $shippingaddress['PostalCode'];
	     	}
	     	else {
	     		$customershippingzip = "";
	     	}
			if(isset($shippingaddress['CountryCode'])){  	
	     		$customershippingcountrycode = $shippingaddress['CountryCode'];
	     	}
	     	else {
	     		$customershippingcountrycode = "";
	     	}
			if(isset($shippingaddress['Phone'])){  	
	     		$customerphone = $shippingaddress['Phone'];
	     	}
	     	else {
	     		$customerphone = "";
	     	}
					
			$customershippinglatitude = "";
			$customershippinglongitude = "";
			if($customershippinglatitude == ''){
				//get latitude and longitude from google maps api
				$address = $customershippingaddress1.", ".$customershippingaddress2.", ".$customershippingaddress2.", ".$customershippingcity.", ".$customershippingstate.", ".$customershippingzip.", ".$customershippingcountrycode;
				$prepAddr = str_replace(' ','+',$address);
				$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
				//echo $geocode;
				$output= json_decode($geocode);
				if(isset($output->results[0]->geometry->location->lat)){
					$customershippinglatitude = $output->results[0]->geometry->location->lat;
					$customershippinglongitude = $output->results[0]->geometry->location->lng;						
				}
			}
			//get countryidshipping
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$getcountry = mysql_query("select * from $database.country where countryshortcode = '$customershippingcountrycode'");	
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Get country using shippingcountrycode: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			if(mysql_num_rows($getcountry)>=1){
				//get countryid of existing record
				$getcountryrow = mysql_fetch_array($getcountry);
				$countryidshipping = $getcountryrow['countryid'];
				$customershippingcountry = $getcountryrow['countryname'];
			}
			else {
				//add a new country to the system
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$addcountry = mysql_query("insert into $database.country set countryname = '$customershippingcountrycode', 
				countryshortcode = '$customershippingcountrycode', 
				dateformat = 'd-m-y', numberformatdecimal = '2', numberformatdecimalpoint = ',', numberformatseparator = '.', 
				disabled = '0', datecreated = '$date'");
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Add country if it does not exist: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
				$countryidshipping = mysql_insert_id();
				$customershippingcountry = $getcountryrow['countryname'];
			}
			
			echo "customerphone: ".$customerphone."<br/>";
			echo "customershippingname: ".$customershippingname."<br/>";
			echo "customershippingaddress1: ".$customershippingaddress1."<br/>";
			echo "customershippingaddress2: ".$customershippingaddress2."<br/>";
			echo "customershippingcity: ".$customershippingcity."<br/>";
			echo "customershippingstate: ".$customershippingstate."<br/>";
			echo "customershippingzip: ".$customershippingzip."<br/>";
			echo "customershippingcountrycode: ".$customershippingcountrycode."<br/>";
			echo "customershippingcountry: ".$customershippingcountry."<br/>";
			echo "customershippinglatitude: ".$customershippinglatitude."<br/>";
			echo "customershippinglongitude: ".$customershippinglongitude."<br/>";
			echo "countryidshipping: ".$countryidshipping."<br/>";	
			
			$customershippingaddress1 = mysql_real_escape_string($customershippingaddress1);		
			$customershippingaddress2 = mysql_real_escape_string($customershippingaddress2);		
			$customershippingcity = mysql_real_escape_string($customershippingcity);		
			$customershippingstate = mysql_real_escape_string($customershippingstate);		
			$customershippingzip = mysql_real_escape_string($customershippingzip);		
			
			//check if customer exists in database
			$salt = "h3f8s9en20vj3";
			$customeremailencrypt = openssl_encrypt($customeremail,"AES-128-ECB",$salt);
			$customernameencrypt = openssl_encrypt($customername,"AES-128-ECB",$salt);
			$customerfirstnameencrypt = openssl_encrypt($customerfirstname,"AES-128-ECB",$salt);
			$customerlastnameencrypt = openssl_encrypt($customerlastname,"AES-128-ECB",$salt);
			$customerbillingaddress1encrypt = openssl_encrypt($customerbillingaddress1,"AES-128-ECB",$salt);
			$customerbillingaddress2encrypt = openssl_encrypt($customerbillingaddress2,"AES-128-ECB",$salt);
			$customerbillingcityencrypt = openssl_encrypt($customerbillingcity,"AES-128-ECB",$salt);
			$customerbillingstateencrypt = openssl_encrypt($customerbillingstate,"AES-128-ECB",$salt);
			$customerbillingzipencrypt = openssl_encrypt($customerbillingzip,"AES-128-ECB",$salt);
			$customerphoneencrypt = openssl_encrypt($customerphone,"AES-128-ECB",$salt);
			echo "customeremailencrypted: ".$customeremailencrypt."<br/>";			
			
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$getcustomer = mysql_query("select * from $database.customer where emailaddress = '$customeremailencrypt'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Check if customer exists, using email address to match: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			if(mysql_num_rows($getcustomer)>=1){
				//update existing customer
				echo "update customer record<br/>";
				$rowcust = mysql_fetch_array($getcustomer);
				$customerid = $rowcust['customerid'];	
				$customername = str_replace("'", "", $customername);
				$customernameencrypt = str_replace("'", "", $customernameencrypt);
				$customerlastname = str_replace("'", "", $customerlastname);
				$customerlastnameencrypt = str_replace("'", "", $customerlastnameencrypt);
				$customerfirstname = str_replace("'", "", $customerfirstname);
				$customerfirstnameencrypt = str_replace("'", "", $customerfirstnameencrypt);
				$updatecustomer = "update $database.customer set customername = '$customernameencrypt', firstname = '$customerfirstnameencrypt',
				lastname = '$customerlastnameencrypt', customertypeid = '$customertypeid', salesstageid = '$salesstageid', 
				address1 = '$customershippingaddress1encrypt', address2 = '$customershippingaddress2encrypt', address3 = '', towncity = '$customershippingcityencrypt', 
				stateregion = '$customershippingstateencrypt', postzipcode = '$customershippingzipencrypt', 
				longitude = '$customershippinglongitude', latitude = '$customershippinglatitude', countryid = '$countryidshipping', 
				phonenumber = '$customerphoneencrypt', emailunsubscribe = '0', disabled = '0'
				where customerid = '$customerid'";
				echo "<br/>updatecustomer: ".$updatecustomer;
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$updatecustomer = mysql_query($updatecustomer);
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Update the existing customer record: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			}
			else {
				//create the customer record
				echo "create new customer record<br/>";
				$customername = str_replace("'", "", $customername);
				$customernameencrypt = str_replace("'", "", $customernameencrypt);
				$customerlastname = str_replace("'", "", $customerlastname);
				$customerlastnameencrypt = str_replace("'", "", $customerlastnameencrypt);
				$customerfirstname = str_replace("'", "", $customerfirstname);
				$customerfirstnameencrypt = str_replace("'", "", $customerfirstnameencrypt);
				$createcustomer = "insert into $database.customer (customername, firstname, lastname, customertypeid, salesstageid, address1, 
				address2, address3, towncity, stateregion, postzipcode, longitude, latitude, countryid, phonenumber, emailunsubscribe, disabled,
				datecreated, businessunitid, emailaddress) 
				values ('$customernameencrypt', '$customerfirstnameencrypt', '$customerlastnameencrypt', '$customertypeid', '$salesstageid', 
				'$customershippingaddress1encrypt', '$customershippingaddress2encrypt', '', '$customershippingcityencrypt', '$customershippingstateencrypt',
				'$customershippingzipencrypt', '$customershippinglongitude', '$customershippinglatitude', '$countryidshipping', '$customerphoneencrypt', 
				'0', '0', '$date', '$businessunitid', '$customeremailencrypt')";
				echo "<br/>createcustomer: ".$createcustomer;
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$createcustomer = mysql_query($createcustomer);
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Add a customer record if does not exist: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
				$customerid = mysql_insert_id();
			}
			echo "customerid: ".$customerid."<br/>";	
			
			
			
	     	$sellerorderid = $order->getSellerOrderId();
	     	$purchasedate = $order->getPurchaseDate();
	     	$lastupdatedate = $order->getLastUpdateDate();
	     	$marketplaceid = $order->getMarketplaceId();
	     	$saleschannel = $order->getSalesChannel();
	     	$orderchannel = $order->getOrderChannel();
	     	$shipservicelevel = $order->getShipServiceLevel();
	     	$ordertotal = json_encode($order->getOrderTotal());
	     	$numberofitemsshipped = $order->getNumberOfItemsShipped();
	     	$numberofitemsunshipped = $order->getNumberOfItemsUnshipped();
	     	$paymentexecutiondetail = $order->getPaymentExecutionDetail();
	     	$paymentmethod = $order->getPaymentMethod();
	     	$paymentmethoddetails = json_encode($order->getPaymentMethodDetails());
	     	$shipmentservicelevelcategory = $order->getShipmentServiceLevelCategory();
	     	$ordertype = $order->getOrderType();
	     	$earliestshipdate = $order->getEarliestShipDate();
	     	$latestshipdate = $order->getLatestShipDate();
	     	$earliestdeliverydate = $order->getEarliestDeliveryDate();
	     	$latestdeliverydate = $order->getLatestDeliveryDate();
	     	
	     	
	     	echo "sellerorderid: ".$sellerorderid."<br/>";	
	     	echo "purchasedate: ".$purchasedate."<br/>";	
	     	echo "lastupdatedate: ".$lastupdatedate."<br/>";	
	     	echo "marketplaceid: ".$marketplaceid."<br/>";	
	     	echo "saleschannel: ".$saleschannel."<br/>";	
	     	echo "orderchannel: ".$orderchannel."<br/>";	
	     	echo "shipservicelevel: ".$shipservicelevel."<br/>";	
	     	echo "ordertotal: ".$ordertotal."<br/>";	
	     	echo "numberofitemsshipped: ".$numberofitemsshipped."<br/>";	
	     	echo "numberofitemsunshipped: ".$numberofitemsunshipped."<br/>";	
	     	echo "paymentexecutiondetail: ".$paymentexecutiondetail."<br/>";	
	     	echo "paymentmethod: ".$paymentmethod."<br/>";	
	     	echo "paymentmethoddetails: ".$paymentmethoddetails."<br/>";	
	     	echo "shipmentservicelevelcategory: ".$shipmentservicelevelcategory."<br/>";	
	     	echo "ordertype: ".$ordertype."<br/>";	
	     	echo "earliestshipdate: ".$earliestshipdate."<br/>";	
	     	echo "latestshipdate: ".$latestshipdate."<br/>";	
	     	echo "earliestdeliverydate: ".$earliestdeliverydate."<br/>";	
	     	echo "latestdeliverydate: ".$latestdeliverydate."<br/>";	
	    
	    	$sqlstarttime = microtime(true);
	    	echo "<br/>time: ".$sqlstarttime;
	    	
    	 	getAmazonOrderItemList($orderid);
    	 	
    	 	$sqlstarttime = microtime(true);
	    	echo "<br/>time: ".$sqlstarttime;
	    	
	      echo "<br/><br/>".json_encode($xml)."<br/>";
	        	
	      	foreach ($xml->OrderItems->OrderItem as $orderitem){
	      		$sellersku = $orderitem->SellerSKU;
	      		$integrationproductid = $sellersku;
	      		$title = $orderitem->Title;
	      		$quantityordered = $orderitem->QuantityOrdered;
	      		settype($quantityordered, "int");
	      		$quantityshipped = $orderitem->QuantityShipped;
	      		settype($quantityshipped, "int");
	      		$shippingpricecurrencycode = $orderitem->ShippingPrice->CurrencyCode;
	      		$shippingpriceamount = $orderitem->ShippingPrice->Amount;
	      		settype($shippingpriceamount, "double");
	      		$itempricecurrencycode = $orderitem->ItemPrice->CurrencyCode;
	      		$itempriceamount = $orderitem->ItemPrice->Amount;
	      		settype($itempriceamount, "double");
	      		$itemtaxcurrencycode = $orderitem->ItemTax->CurrencyCode;
	      		$itemtaxamount = $orderitem->ItemTax->Amount;
	      		settype($itemtaxamount, "double");
	      		$shippingtaxcurrencycode = $orderitem->ShippingTax->CurrencyCode;
	      		$shippingtaxamount = $orderitem->ShippingTax->Amount;
	      		settype($shippingtaxamount, "double");
	      		$orderitemid = $orderitem->OrderItemId;
	      		echo "sellersku: ".$sellersku."<br/>";
	      		echo "title: ".$title."<br/>";
	      		echo "quantityordered: ".$quantityordered."<br/>";
	      		echo "quantityshipped: ".$quantityshipped."<br/>";
	      		echo "shippingpricecurrencycode: ".$shippingpricecurrencycode."<br/>";
	      		echo "shippingpriceamount: ".$shippingpriceamount."<br/>";
	      		echo "itempricecurrencycode: ".$itempricecurrencycode."<br/>";
	      		echo "itempriceamount: ".$itempriceamount."<br/>";
	      		echo "itemtaxcurrencycode: ".$itemtaxcurrencycode."<br/>";
	      		echo "itemtaxamount: ".$itemtaxamount."<br/>";
	      		echo "shippingtaxcurrencycode: ".$shippingtaxcurrencycode."<br/>";
	      		echo "shippingtaxamount: ".$shippingtaxamount."<br/>";
	      		echo "orderitemid: ".$orderitemid."<br/>";
	      		
	      		//get currencycode	
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$getcurrency = mysql_query("select * from $database.currency where currencycode = '$itempricecurrencycode'");	
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Get currency record matching on price currency code: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
				if(mysql_num_rows($getcurrency)>=1){
					//get countryid of existing record
					$getcurrencyrow = mysql_fetch_array($getcurrency);
					$currencyid = $getcurrencyrow['currencyid'];
				}
				else {
					//add a new country to the system
					$nosqlqueries = $nosqlqueries + 1;
					$sqlstarttime = microtime(true);
					$addcurrency = mysql_query("insert into $database.currency set currencyname = '$itempricecurrencycode', 
					currencycode = '$itempricecurrencycode', 
					currencysymbol = '', disabled = '0', datecreated = '$date'");
					$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
					$differencemilliseconds = microtime(true) - $sqlstarttime;
					echo "<br/>Add currency if does not exist: ";
					echo "<br/>differencemilliseconds: ".$differencemilliseconds;
					echo "<br/>sqlqueriestime: ".$sqlqueriestime;
					$currencyid = mysql_insert_id();
				}
				echo "currencyid: ".$currencyid."<br/>";	
				
				$integrationorderid = $orderid."///".$orderitemid;
				echo "integrationorderid: ".$integrationorderid."<br/>";	
				
				array_push($orderarray, array("integrationorderid"=>$integrationorderid,"orderid"=>$orderid,
				"customeremail"=>$customeremail,"customername"=>$customername,"customershippingname"=>$customershippingname,
				"customerfirstname"=>$customerfirstname,"customerlastname"=>$customerlastname,
				"customershippingaddress1"=>$customershippingaddress1,"customershippingaddress2"=>$customershippingaddress2,
				"customershippingaddress3"=>$customershippingaddress3,"customershippingcity"=>$customershippingcity,
				"customershippingdistrict"=>$customershippingdistrict,"customershippingstate"=>$customershippingstate,
				"customershippingzip"=>$customershippingzip,"customershippingcountrycode"=>$customershippingcountrycode,
				"customershippinglatitude"=>$customershippinglatitude,
				"customershippinglongitude"=>$customershippinglongitude,"countryidshipping"=>$countryidshipping,
				"customershippingcountry"=>$customershippingcountry,"customerphone"=>$customerphone,
				"customerid"=>$customerid,"sellerorderid"=>$sellerorderid,
				"purchasedate"=>$purchasedate,"status"=>$status,
				"lastupdatedate"=>$lastupdatedate,"marketplaceid"=>$marketplaceid,
				"saleschannel"=>$saleschannel,"orderchannel"=>$orderchannel,
				"numberofitemsshipped"=>$numberofitemsshipped,"numberofitemsunshipped"=>$numberofitemsunshipped,
				"paymentexecutiondetail"=>$paymentexecutiondetail,"paymentmethod"=>$paymentmethod,
				"paymentmethoddetails"=>$paymentmethoddetails,"shipmentservicelevelcategory"=>$shipmentservicelevelcategory,
				"ordertype"=>$ordertype,"earliestshipdate"=>$earliestshipdate,
				"latestshipdate"=>$latestshipdate,"earliestdeliverydate"=>$earliestdeliverydate,
				"latestdeliverydate"=>$latestdeliverydate,"sellersku"=>$sellersku,
				"integrationproductid"=>$integrationproductid,"title"=>$title,
				"quantityordered"=>$quantityordered,"quantityshipped"=>$quantityshipped,
				"shippingpricecurrencycode"=>$shippingpricecurrencycode,"shippingpriceamount"=>$shippingpriceamount,
				"itempricecurrencycode"=>$itempricecurrencycode,"itempriceamount"=>$itempriceamount,
				"itemtaxcurrencycode"=>$itemtaxcurrencycode,"itemtaxamount"=>$itemtaxamount,
				"shippingtaxcurrencycode"=>$shippingtaxcurrencycode,"shippingtaxamount"=>$shippingtaxamount,
				"orderitemid"=>$orderitemid,"currencyid"=>$currencyid));
				
			}
		}
	}
		
	echo "<br/><br/>orderarray: ".json_encode($orderarray);
	
	echo "<br/><br/><b>CREATION/STATUS CHECK</b>";
	$integrationorderids = "";
	foreach($orderarray as $item){	
		$status = $item['status'];
		$integrationorderid = $item['integrationorderid'];
		echo "<br/>integrationorderid: ".$integrationorderid." status: ".$status;
		$integrationorderids = $integrationorderids."'".$integrationorderid."',";	
	}		
	if($integrationorderids <> ""){
		$integrationorderids = rtrim($integrationorderids, ",");
		$getrecord = "select ecommercelistinglocation.ecommercelistingid, ecommercelistinglocationid, 
		integrationorderid, productstockitemid from $database.ecommercelistinglocation 
		where ecommercelistinglocation.integrationorderid in ($integrationorderids) and ecommercesiteconfigid = $ecommercesiteconfigid";
		echo "<br/><br/>".$getrecord;
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$getrecord = mysql_query($getrecord);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Get the records from ecommercelistinglocation table if it has already been matched to an order: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		$alreadycreated = array();
		//adding one dummy record because array search doesnt search the first record normally
		array_push($alreadycreated, array("integrationorderid"=>0,"ecommercelistingid"=>0,
		"ecommercelistinglocationid"=>0,"productstockitemid"=>0,
		"status"=>'dummy',"quantityshipped"=>0,"quantityordered"=>0));
		while($getrecordrow = mysql_fetch_array($getrecord)){
			$integrationorderid = $getrecordrow['integrationorderid'];
			$ecommercelistingid = $getrecordrow['ecommercelistingid'];
			$ecommercelistinglocationid = $getrecordrow['ecommercelistinglocationid'];
			$productstockitemid = $getrecordrow['productstockitemid'];
			$key = array_search($integrationorderid, array_column($orderarray, 'integrationorderid'));
			$status = $orderarray[$key]['status'];
			$quantityshipped = $orderarray[$key]['quantityshipped'];
			$quantityordered = $orderarray[$key]['quantityordered'];
			
			//save the orderid to the list of processed id's so that if the orderid reappears in another ecommercesiteconfig, it will be ignored
			array_push($orderidprocessedlist, $integrationorderid);
			
			//echo "<br/>integrationorderid: ".$integrationorderid." - ".$ecommercelistinglocationid." - ".$status;
			array_push($alreadycreated, array("integrationorderid"=>$integrationorderid,"ecommercelistingid"=>$ecommercelistingid,
			"ecommercelistinglocationid"=>$ecommercelistinglocationid,"productstockitemid"=>$productstockitemid,
			"status"=>$status,"quantityshipped"=>$quantityshipped,"quantityordered"=>$quantityordered));
		}	
		echo "<br/><br/>alreadycreated: ".json_encode($alreadycreated)."<br/>";
		
		$needtobecreated = array();
		foreach($orderarray as $item2){
			$integrationorderid = $item2['integrationorderid'];
			$status = $item2['status'];
			$key = array_search($integrationorderid, array_column($alreadycreated, 'integrationorderid'));
			//echo "<br/>not created yet - integrationorderid: ".$integrationorderid." - ".$status." key:".$key;	
			if($key == "" && $status <> 'Canceled'){
				echo "<br/>not created yet - integrationorderid: ".$integrationorderid." - ".$status;	
				array_push($needtobecreated, array("integrationorderid"=>$integrationorderid));	
			}
		}
		echo "<br/><br/>needtobecreated: ".json_encode($needtobecreated)."<br/>";
		
		
		echo "<br/><br/><b>UPDATE ORDER STATUSES ON EXISTING ORDERS</b>";	
		foreach($alreadycreated as $item3){	
			$status = $item3['status'];
			$integrationorderid = $item3['integrationorderid'];
			$ecommercelistingid = $item3['ecommercelistingid'];
			$ecommercelistinglocationid = $item3['ecommercelistinglocationid'];
			$productstockitemid = $item3['productstockitemid'];
			$quantityordered = $item3['quantityordered'];
			$quantityshipped = $item3['quantityshipped'];
			echo "<br/>integrationorderid: ".$integrationorderid;
			//if in an active status
			if($status <> 'Canceled'){
				if($quantityshipped == $quantityordered){
					$ecommercelistingstatusid = 2;				
				}
				else {
					$ecommercelistingstatusid = 3;				
				}
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$updatestatus = mysql_query("update $database.ecommercelisting 
				set ecommercelistingstatusid = '$ecommercelistingstatusid'
				where ecommercelistingid = '$ecommercelistingid' and ecommercelistingstatusid in (1,4,3)");
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Update the ecommercelistingstatusid for the record: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			}

		}
		
		
		echo "<br/><br/><b>ADD NEW ORDERS</b>";	
		foreach($needtobecreated as $item4){
			$integrationorderid = $item4['integrationorderid'];
			echo "<br/><br/>add order: integrationorderid: ".$integrationorderid;
			$key = array_search($integrationorderid, array_column($orderarray, 'integrationorderid'));
			$status = $orderarray[$key]['status'];
			$orderid = $orderarray[$key]['orderid'];
			$customeremail = $orderarray[$key]['customeremail'];
			$customername = $orderarray[$key]['customername'];
			$customershippingname = $orderarray[$key]['customershippingname'];
			$customerfirstname = $orderarray[$key]['customerfirstname'];
			$customerlastname = $orderarray[$key]['customerlastname'];
			$customershippingaddress1 = $orderarray[$key]['customershippingaddress1'];
			$customershippingaddress2 = $orderarray[$key]['customershippingaddress2'];
			$customershippingaddress3 = $orderarray[$key]['customershippingaddress3'];
			$customershippingcity = $orderarray[$key]['customershippingcity'];
			$customershippingdistrict = $orderarray[$key]['customershippingdistrict'];
			$customershippingstate = $orderarray[$key]['customershippingstate'];
			$customershippingzip = $orderarray[$key]['customershippingzip'];
			$customershippingcountrycode = $orderarray[$key]['customershippingcountrycode'];
			$customershippinglatitude = $orderarray[$key]['customershippinglatitude'];
			$customershippinglongitude = $orderarray[$key]['customershippinglongitude'];
			$countryidshipping = $orderarray[$key]['countryidshipping'];
			$customershippingcountry = $orderarray[$key]['customershippingcountry'];
			$customerphone = $orderarray[$key]['customerphone'];
			$customerid = $orderarray[$key]['customerid'];
			$sellerorderid = $orderarray[$key]['sellerorderid'];
			$purchasedate = $orderarray[$key]['purchasedate'];
			$lastupdatedate = $orderarray[$key]['lastupdatedate'];
			$marketplaceid = $orderarray[$key]['marketplaceid'];
			$saleschannel = $orderarray[$key]['saleschannel'];
			$orderchannel = $orderarray[$key]['orderchannel'];
			$numberofitemsshipped = $orderarray[$key]['numberofitemsshipped'];
			$numberofitemsunshipped = $orderarray[$key]['numberofitemsunshipped'];
			$paymentexecutiondetail = $orderarray[$key]['paymentexecutiondetail'];
			$paymentmethod = $orderarray[$key]['paymentmethod'];
			$paymentmethoddetails = $orderarray[$key]['paymentmethoddetails'];
			$shipmentservicelevelcategory = $orderarray[$key]['shipmentservicelevelcategory'];
			$ordertype = $orderarray[$key]['ordertype'];
			$earliestshipdate = $orderarray[$key]['earliestshipdate'];
			$latestshipdate = $orderarray[$key]['latestshipdate'];
			$latestdeliverydate = $orderarray[$key]['latestdeliverydate'];
			$earliestdeliverydate = $orderarray[$key]['earliestdeliverydate'];
			$sellersku = $orderarray[$key]['sellersku'];
			$integrationproductid = $orderarray[$key]['integrationproductid'];
			$title = $orderarray[$key]['title'];
			$quantityordered = $orderarray[$key]['quantityordered'];
			$quantityshipped = $orderarray[$key]['quantityshipped'];
			$shippingpricecurrencycode = $orderarray[$key]['shippingpricecurrencycode'];
			$shippingpriceamount = $orderarray[$key]['shippingpriceamount'];
			$itempricecurrencycode = $orderarray[$key]['itempricecurrencycode'];
			$itempriceamount = $orderarray[$key]['itempriceamount'];
			$itemtaxcurrencycode = $orderarray[$key]['itemtaxcurrencycode'];
			$itemtaxamount = $orderarray[$key]['itemtaxamount'];
			$shippingtaxcurrencycode = $orderarray[$key]['shippingtaxcurrencycode'];
			$shippingtaxamount = $orderarray[$key]['shippingtaxamount'];
			$orderitemid = $orderarray[$key]['orderitemid'];
			$currencyid = $orderarray[$key]['currencyid'];
			
			//add the order to the system
			//work out ecommercelistinglocation records
			$getlocationlisting = "select * from $database.ecommercelistinglocation 
			inner join ecommercelisting on ecommercelisting.ecommercelistingid = ecommercelistinglocation.ecommercelistingid
			inner join productstockitem on productstockitem.productstockitemid = ecommercelisting.productstockitemid
			where ecommercelistinglocation.businessunitid = '$businessunitid' 
			and integrationproductid = '$integrationproductid' and 
			ecommercesiteconfigid = '$ecommercesiteconfigid' and ecommercelistinglocationstatusid in (2,3,4,8)
			and integrationorderid = '' and showasnotstock = 0
			order by ecommercelistinglocationid asc";
			//echo $getlocationlisting;
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$getlocationlisting = mysql_query($getlocationlisting);
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>If has not been matched to orderid yet, then find the matching ecommercelistinglocation record: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			$countlocationlisting = mysql_num_rows($getlocationlisting);
			echo "number of potential records: ".$countlocationlisting."<br/>";		
			$outofstock = 0;
			$ecommercelistinglocationid = 0;
			if($countlocationlisting >= 1){
				//get the record id
				$getrecord = mysql_fetch_array($getlocationlisting);
				$ecommercelistinglocationid = $getrecord['ecommercelistinglocationid'];		
			}
			else {
				//item must already be sold - mark it as out of stock
				$outofstock = 1;
			}
			echo "outofstock: ".$outofstock."<br/>";		
			echo "ecommercelistinglocationid: ".$ecommercelistinglocationid."<br/><br/>";		
			
			//if out of stock then run this code
			if($outofstock == 1){
				//check record doesn't already exist
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$getrecord = mysql_query("select * from ecommerceoutofstock where integrationorderid = '$integrationorderid'");
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Item is out of stock, so check if it has been added to the out of stock table: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
				if(mysql_num_rows($getrecord)< 1){
					//calculate refundamount 
					$totalrefundamount = $itempriceamount +$itemtaxamount + $shippingpriceamount + $shippingtaxamount;
					echo "totalrefundamount: ".$totalrefundamount."<br/>";
						
					//get productid	
					if (strpos($sellersku, 'PSI') !== false) {
					  	$sku = str_replace("PSI-", "", $sellersku);
					  	$nosqlqueries = $nosqlqueries + 1;
						$sqlstarttime = microtime(true);
						$getsku = mysql_query("select * from productstockitem where productstockitemid = '$sku'");		
						$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
						$differencemilliseconds = microtime(true) - $sqlstarttime;
						echo "<br/>Get the matching productstockitem: ";
						echo "<br/>differencemilliseconds: ".$differencemilliseconds;
						echo "<br/>sqlqueriestime: ".$sqlqueriestime;
						$getskurow = mysql_fetch_array($getsku);
						$productid = $getskurow['productid'];
					}
					else {
						$sku = str_replace("P-", "", $sellersku);						
					}
					
					
					//add record to ecommerceoutofstock table
					$nosqlqueries = $nosqlqueries + 1;
					$sqlstarttime = microtime(true);
					$insertoutofstock = mysql_query("insert into ecommerceoutofstock (ecommerceoutofstockname, disabled, datecreated,
					masteronly, customerid, productid, integrationorderid, ecommercesiteconfigid, requestdate, ecommerceoutofstockstatusid, 
					businessunitid, totalrefundamount, refundcurrencyid) 
					values ('$title', '0', '$date', '0', '$customerid', '$sku', '$integrationorderid', '$ecommercesiteconfigid', '$date',
					'1', '$businessunitid', '$totalrefundamount', '$currencyid')");		
					$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
					$differencemilliseconds = microtime(true) - $sqlstarttime;
					echo "<br/>Add the record to the out of stock table: ";
					echo "<br/>differencemilliseconds: ".$differencemilliseconds;
					echo "<br/>sqlqueriestime: ".$sqlqueriestime;
				}			
			}
			
			//if found record then run this code
			if($ecommercelistinglocationid >= 1 && $outofstock == 0){
				//update this ecommercelistinglocation record as sold
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$update = mysql_query("update $database.ecommercelistinglocation set ecommercelistinglocationstatusid = '5',
				integrationorderid = '$integrationorderid', integrationstatusname = 'Sold'
				where ecommercelistinglocationid = '$ecommercelistinglocationid'");
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>If ecommercelistinglocation record found, update it as sold: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
				
				//save the orderid to the list of processed id's so that if the orderid reappears in another ecommercesiteconfig, it will be ignored
				array_push($orderidprocessedlist, $integrationorderid);
				
				//update all other ecommercelistinglocation records as not sold
				$ecommercelistingid = $getrecord['ecommercelistingid'];	
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$update = mysql_query("update $database.ecommercelistinglocation set ecommercelistinglocationstatusid = '6', 
				integrationstatusname = '', amazonproductxml = ''
				where ecommercelistinglocationid <> '$ecommercelistinglocationid' and ecommercelistingid = '$ecommercelistingid'");
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Update all of the other ecommercelistinglocation records as not sold: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
				
				//update ecommercelisting record as sold
				$siteconf = $getrecord['ecommercesiteconfigid'];	
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$update = mysql_query("update $database.ecommercelisting 
				set soldecommercesiteconfigid = '$siteconf', selldate = '$date', 
				sellcustomerid = '$customerid', ecommercelistingstatusid = '3', 
				billingaddress1 = '$customershippingaddress1', billingaddress2 = '$customershippingaddress2', 
				billingaddresstowncity = '$customershippingcity', billingaddressstatecounty = '$customershippingstate', 
				billingaddresspostzipcode = '$customershippingzip', billingaddresscountryid = '$countryidshipping', 
				fulfillmentaddress1 = '$customershippingaddress1', fulfillmentaddress2 = '$customershippingaddress2', 
				fulfillmentaddresstowncity = '$customershippingcity', fulfillmentaddressstatecounty = '$customershippingstate', 
				fulfillmentaddresspostzipcode = '$customershippingzip', fulfillmentaddresscountryid = '$countryidshipping', 
				shippingoptionname = '$shipservicelevel', shippingpricecurrencyid = '$currencyid', 				shippingprice = '$shippingpriceamount', shippingtax = '$shippingtaxamount'
				where ecommercelistingid = '$ecommercelistingid'");
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Update the ecommercelisting record with a whole load of data: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
				
				//update productstockitem record as sold 
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$getproductstock = mysql_query("select * from ecommercelisting where ecommercelistingid = '$ecommercelistingid'");
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Get the ecommercelisting record for this ecommercelistinglocation record: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
				$getproductstockrow = mysql_fetch_array($getproductstock);
				$productstockitemid = $getproductstockrow['productstockitemid'];
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$taxpercentage = $itemtaxamount / $itempriceamount * 100;
				$priceincludingtax = $itemtaxamount + $itempriceamount;
				echo "priceincludingtax: ".$priceincludingtax."<br/>";
				$update = mysql_query("update $database.productstockitem
				set productstockitemstatusid = '2', sellprice = '$itempriceamount', saleschannelid = '$saleschannelid', selldate = '$date', 
				customerid = '$customerid', sellcurrencyid = '$currencyid', selltaxrate = '$taxpercentage', 
				selltaxprice = '$itemtaxamount', sellpriceincludingtax = '$priceincludingtax'
				where productstockitemid = '$productstockitemid'");	
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Update the productstockitem record as sold: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;	
				
				//delete the sellersku from amazon if it starts with PSI-
				if (strpos($sellersku, 'PSI-') !== false) {
					$sellerskudeletelist = $sellerskudeletelist.$sellersku.",";
				}
		
				//create customerpurchase table record
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$getproduct = mysql_query("select * from productstockitem where productstockitemid = '$productstockitemid'");
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Get the productstockitem record for this ecommercelisting record: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
				$getproductrow = mysql_fetch_array($getproduct);
				$productid = $getproductrow['productid'];
				$currencyid = $getproductrow['currencyid'];
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$create = mysql_query("insert into customerpurchase (customerid, productid, currencyid, purchasevalue, 
				datepurchased, businessunitid, disabled, datecreated) values ('$customerid', '$productid', '$currencyid', '$itempriceamount', 
				'$date', '$businessunitid', '0', '$date')");
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Insert a customerpurchase record for this purchase: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
				
				// new update
				$ordertableentry = mysql_query("INSERT INTO ecommerceorder (ecommerceordername, 
				datecreated, disabled, customerid, orderdatetime, ecommercesiteconfigid) VALUES 
				('customer order', '$date', '0', '$customerid', '$date', '$ecommercesiteconfigid')");
				$ordertableid = mysql_insert_id($ordertableentry);

				$orderitemtableentry = mysql_query("INSERT INTO ecommerceorderitem (ecommerceorderitemname, 
				disabled, datecreated, ecommerceorderid, ecommercelistinglocationid, ecommercelistingid, 
				productstockitemid) VALUES ('customer order item', '0', '$date', '$ordertableid', '$ecommercelistinglocationid', 
				'$ecommercelistingid', '$productstockitemid')");
				// new update
			}					
		}
	}
	
}

echo "<br/><br/>orderidprocessedlist: ".json_encode($orderidprocessedlist);


//process deletions of any sku's needing to be deleted
$sellerskudeletelist = rtrim($sellerskudeletelist, ",");
if($sellerskudeletelist <> ''){
	//run an inventory update to set to 0
	$amazonfileid = rand(1000,9999);
	$feedinventory = '<?xml version="1.0" encoding="iso-8859-1"?>
	<AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	    xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
	  <Header>
	    <DocumentVersion>1.01</DocumentVersion>
	    <MerchantIdentifier>'.$amazonfileid.'</MerchantIdentifier>
	  </Header>
	  <MessageType>Product</MessageType>';
	$i = 1;
	
	$sellerskudeletelistarray = explode(',', $sellerskudeletelist);
	foreach($sellerskudeletelistarray as $integrationproductid){
		echo "<br/>".$integrationproductid;
		$feedinventory = $feedinventory.'
		<Message>
			<MessageID>'.$i.'</MessageID>
			<OperationType>Delete</OperationType>
			<Product>
				<SKU>'.$integrationproductid.'</SKU>
			</Product>
		</Message>';
	  	$i = $i + 1;		
	}
	$feedinventory = $feedinventory.'</AmazonEnvelope>';
	echo "<br/><br/>".nl2br(htmlentities($feedinventory));
	echo "<br/><br/>Submit the inventory feed: ";
	sendDeleteFeed($feedinventory);
	echo "<br/>final feed sub id: ".$feedsubid;
						
			
}




?>