<?php
set_time_limit(500);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$hour = date('H');
$date = date('Y-m-d');
$date30 = date('Y-m-d', strtotime("- 30 days"));
$date365 = date('Y-m-d', strtotime("- 365 days"));
echo "<br/>hour: ".$hour."<br/>";


$insertarray = array();


echo "<br/><br/><b>Volume of Sales Per Site</b>";
if($hour == 0){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from ecommercedashboardstat where ecommercestatdate >= '$date30' 
	and ecommercestattype = 'Volume of Sales Per Site'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select ecommercesiteconfig.ecommercesiteconfigname as 'Name', selldate as 'Dates',
	COUNT(ecommercelisting.ecommercelistingid) as 'Value' 
	from ecommercesiteconfig 
	inner join ecommercelisting on ecommercelisting.soldecommercesiteconfigid = ecommercesiteconfig.ecommercesiteconfigid 
	where ecommercelisting.ecommercelistingstatusid = '2' and ecommercelisting.selldate>='$date30' 
	group by ecommercesiteconfig.ecommercesiteconfigname, selldate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Volume of Sales Per Site';
		$statdate = $queryrow['Dates'];
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Value of Sales Per Site</b>";
if($hour == 1){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from ecommercedashboardstat where ecommercestatdate >= '$date30' 
	and ecommercestattype = 'Value of Sales Per Site'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select ecommercesiteconfig.ecommercesiteconfigname as 'Name', ecommercelisting.selldate as 'Dates',
	round(sum(if(usebudgetfxrates = 1, (productstockitem.sellprice / currencytohomecurrencybudgetfxrate), 
	(productstockitem.sellprice / currencytohqcurrencycurrentfxrate))), 2) as 'Value' 
	from ecommercesiteconfig 
	inner join ecommercelisting on ecommercelisting.soldecommercesiteconfigid = ecommercesiteconfig.ecommercesiteconfigid 
	inner join productstockitem on productstockitem.productstockitemid = ecommercelisting.productstockitemid 
	left join financialbudgetperiod on financialbudgetperiod.startdate <= productstockitem.buydate and 
	financialbudgetperiod.enddate >= productstockitem.buydate 
	left join financialbudgetperiodcurrencyfxrate on financialbudgetperiodcurrencyfxrate.currencyid = productstockitem.currencyid 
	and financialbudgetperiod.financialbudgetperiodid = financialbudgetperiodcurrencyfxrate.financialbudgetperiodid 
	left join currencyfxrate on currencyfxrate.currencyid = productstockitem.currencyid 
	left join onefinanceconfiguration on onefinanceconfiguration.disabled = productstockitem.disabled 
	where ecommercelisting.ecommercelistingstatusid = '2' and ecommercelisting.selldate>='$date30' 
	group by ecommercesiteconfig.ecommercesiteconfigname, ecommercelisting.selldate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Value of Sales Per Site';
		$statdate = $queryrow['Dates'];
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Sales Profitability Per Site</b>";
if($hour == 2){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from ecommercedashboardstat where ecommercestatdate >= '$date30' 
	and ecommercestattype = 'Sales Profitability Per Site'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select ecommercesiteconfig.ecommercesiteconfigname as 'Name', 
	round(sum(if(usebudgetfxrates = 1, ((productstockitem.sellprice / currencytohomecurrencybudgetfxrate) / 
	(productstockitem.buyprice / currencytohomecurrencybudgetfxrate)) * 100, ((productstockitem.sellprice / 
	currencytohqcurrencycurrentfxrate) / (productstockitem.buyprice / currencytohqcurrencycurrentfxrate)) * 100)), 2) as 'Value' 
	from ecommercesiteconfig 
	inner join ecommercelisting on ecommercelisting.soldecommercesiteconfigid = ecommercesiteconfig.ecommercesiteconfigid 
	inner join productstockitem on productstockitem.productstockitemid = ecommercelisting.productstockitemid 
	left join financialbudgetperiod on financialbudgetperiod.startdate <= productstockitem.buydate 
	and financialbudgetperiod.enddate >= productstockitem.buydate 
	left join financialbudgetperiodcurrencyfxrate on financialbudgetperiodcurrencyfxrate.currencyid = productstockitem.currencyid 
	and financialbudgetperiod.financialbudgetperiodid = financialbudgetperiodcurrencyfxrate.financialbudgetperiodid 
	left join currencyfxrate on currencyfxrate.currencyid = productstockitem.currencyid 
	left join onefinanceconfiguration on onefinanceconfiguration.disabled = productstockitem.disabled 
	where ecommercelisting.ecommercelistingstatusid = '2' and ecommercelisting.selldate>='$date365' 
	group by ecommercesiteconfig.ecommercesiteconfigname");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Sales Profitability Per Site';
		$statdate = '';
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Volume of Sales Per Business Unit</b>";
if($hour == 3){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from ecommercedashboardstat where ecommercestatdate >= '$date30' 
	and ecommercestattype = 'Volume of Sales Per Business Unit'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select businessunit.businessunitname as 'Name', selldate as 'Dates',
	COUNT(ecommercelisting.ecommercelistingid) as 'Value' 
	from businessunit 
	inner join ecommercelisting on businessunit.businessunitid = ecommercelisting.businessunitid 
	where ecommercelisting.ecommercelistingstatusid = '2' and ecommercelisting.selldate>='$date30' 
	group by businessunit.businessunitname, ecommercelisting.selldate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Volume of Sales Per Business Unit';
		$statdate = $queryrow['Dates'];
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Value of Sales Per Business Unit</b>";
if($hour == 4){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from ecommercedashboardstat where ecommercestatdate >= '$date30' 
	and ecommercestattype = 'Value of Sales Per Business Unit'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select businessunit.businessunitname as 'Name', ecommercelisting.selldate as 'Dates',
	round(sum(if(usebudgetfxrates = 1, (productstockitem.sellprice / currencytohomecurrencybudgetfxrate), 
	(productstockitem.sellprice / currencytohqcurrencycurrentfxrate))), 2) as 'Value' 
	from businessunit 
	inner join ecommercelisting on ecommercelisting.businessunitid = businessunit.businessunitid 
	inner join productstockitem on ecommercelisting.productstockitemid = productstockitem.productstockitemid 
	left join financialbudgetperiod on financialbudgetperiod.startdate <= productstockitem.buydate 
	and financialbudgetperiod.enddate >= productstockitem.buydate 
	left join financialbudgetperiodcurrencyfxrate on financialbudgetperiodcurrencyfxrate.currencyid = productstockitem.currencyid 
	and financialbudgetperiod.financialbudgetperiodid = financialbudgetperiodcurrencyfxrate.financialbudgetperiodid 
	left join currencyfxrate on currencyfxrate.currencyid = productstockitem.currencyid 
	left join onefinanceconfiguration on onefinanceconfiguration.disabled = productstockitem.disabled 
	where ecommercelisting.ecommercelistingstatusid = '2' and ecommercelisting.selldate>='$date30' 
	group by businessunit.businessunitname, ecommercelisting.selldate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Value of Sales Per Business Unit';
		$statdate = $queryrow['Dates'];
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Sales Profitability Per Business Unit</b>";
if($hour == 5){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from ecommercedashboardstat where ecommercestatdate >= '$date30' 
	and ecommercestattype = 'Sales Profitability Per Business Unit'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select businessunit.businessunitname as 'Name', 
	round(sum(if(usebudgetfxrates = 1, ((productstockitem.sellprice / currencytohomecurrencybudgetfxrate) / 
	(productstockitem.buyprice / currencytohomecurrencybudgetfxrate)) * 100, ((productstockitem.sellprice / 
	currencytohqcurrencycurrentfxrate) / (productstockitem.buyprice / currencytohqcurrencycurrentfxrate)) * 100)), 2) as 'Value' 
	from businessunit 
	inner join ecommercelisting on ecommercelisting.businessunitid = businessunit.businessunitid 
	inner join productstockitem on ecommercelisting.productstockitemid = productstockitem.productstockitemid 
	left join financialbudgetperiod on financialbudgetperiod.startdate <= productstockitem.buydate 
	and financialbudgetperiod.enddate >= productstockitem.buydate 
	left join financialbudgetperiodcurrencyfxrate on financialbudgetperiodcurrencyfxrate.currencyid = productstockitem.currencyid 
	and financialbudgetperiod.financialbudgetperiodid = financialbudgetperiodcurrencyfxrate.financialbudgetperiodid 
	left join currencyfxrate on currencyfxrate.currencyid = productstockitem.currencyid 
	left join onefinanceconfiguration on onefinanceconfiguration.disabled = productstockitem.disabled 
	where ecommercelisting.ecommercelistingstatusid = '2' and ecommercelisting.selldate>='$date365' 
	group by businessunit.businessunitname");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Sales Profitability Per Business Unit';
		$statdate = '';
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Volume of Returns Per Business Unit</b>";
if($hour == 6){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from ecommercedashboardstat where ecommercestatdate >= '$date30' 
	and ecommercestattype = 'Volume of Returns Per Business Unit'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select businessunit.businessunitname as 'Name', ecommercelisting.returndate as 'Dates',
	COUNT(ecommercelisting.ecommercelistingid) as 'Value' 
	from businessunit 
	inner join ecommercelisting on businessunit.businessunitid = ecommercelisting.businessunitid 
	where ecommercelisting.ecommercelistingstatusid = '6' and ecommercelisting.returndate>='$date30' 
	group by businessunit.businessunitname, ecommercelisting.returndate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Volume of Returns Per Business Unit';
		$statdate = $queryrow['Dates'];
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Volume of Out of Stock Per Business Unit</b>";
if($hour == 7){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from ecommercedashboardstat where ecommercestatdate >= '$date30' 
	and ecommercestattype = 'Volume of Out of Stock Per Business Unit'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select businessunit.businessunitname as 'Name', ecommerceoutofstock.requestdate as 'Dates',
	COUNT(ecommerceoutofstock.ecommerceoutofstockid) as 'Value' 
	from businessunit 
	inner join ecommerceoutofstock on businessunit.businessunitid = ecommerceoutofstock.businessunitid 
	where ecommerceoutofstock.requestdate>='$date30' 
	group by businessunit.businessunitname, ecommerceoutofstock.requestdate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Volume of Out of Stock Per Business Unit';
		$statdate = $queryrow['Dates'];
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Volume of Sales Per Day</b>";
if($hour == 8){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from ecommercedashboardstat where ecommercestatdate >= '$date30' 
	and ecommercestattype = 'Volume of Sales Per Day'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select ecommercelisting.selldate as 'Dates', 
	COUNT(ecommercelisting.ecommercelistingid) as 'Value' 
	from ecommercelisting 
	where (ecommercelisting.ecommercelistingstatusid = '2' or 
	ecommercelisting.ecommercelistingstatusid = '3' or ecommercelisting.ecommercelistingstatusid = '4')
	and ecommercelisting.selldate>='$date30' 
	group by ecommercelisting.selldate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Volume of Sales Per Day';
		$statdate = $queryrow['Dates'];
		$statname = '';
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Volume of Sales Per Month</b>";
if($hour == 9){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from ecommercedashboardstat where ecommercestatdate >= '$date30' 
	and ecommercestattype = 'Volume of Sales Per Month'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select ecommercelisting.selldate as 'Dates', 
	COUNT(ecommercelisting.ecommercelistingid) as 'Value' 
	from ecommercelisting 
	where (ecommercelisting.ecommercelistingstatusid = '2' or 
	ecommercelisting.ecommercelistingstatusid = '3' or ecommercelisting.ecommercelistingstatusid = '4')
	and ecommercelisting.selldate>='$date30' 
	group by ecommercelisting.selldate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Volume of Sales Per Month';
		$statdate = $queryrow['Dates'];
		$statname = '';
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Volume of Unprocessed Returns Per Day</b>";
if($hour == 10){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from ecommercedashboardstat where ecommercestatdate >= '$date30' 
	and ecommercestattype = 'Volume of Unprocessed Returns Per Day'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select ecommercelisting.selldate as 'Dates', 
	COUNT(ecommercelisting.ecommercelistingid) as 'Value' 
	from ecommercelisting 
	where ecommercelisting.ecommercelistingstatusid = '5' 
	group by ecommercelisting.selldate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Volume of Unprocessed Returns Per Day';
		$statdate = $queryrow['Dates'];
		$statname = '';
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Volume of Unprocessed Despatches Per Day</b>";
if($hour == 11){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from ecommercedashboardstat where ecommercestatdate >= '$date30' 
	and ecommercestattype = 'Volume of Unprocessed Despatches Per Day'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select ecommercelisting.selldate as 'Dates', 
	COUNT(ecommercelisting.ecommercelistingid) as 'Value' 
	from ecommercelisting 
	where ecommercelisting.ecommercelistingstatusid = '3' 
	group by ecommercelisting.selldate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Volume of Unprocessed Despatches Per Day';
		$statdate = $queryrow['Dates'];
		$statname = '';
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Volume of Unprocessed Payments Per Day</b>";
if($hour == 12){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from ecommercedashboardstat where ecommercestatdate >= '$date30' 
	and ecommercestattype = 'Volume of Unprocessed Payments Per Day'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select ecommercelisting.selldate as 'Dates', 
	COUNT(ecommercelisting.ecommercelistingid) as 'Value' 
	from ecommercelisting 
	where ecommercelisting.ecommercelistingstatusid = '4' 
	group by ecommercelisting.selldate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Volume of Unprocessed Payments Per Day';
		$statdate = $queryrow['Dates'];
		$statname = '';
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Volume of Unprocessed Out Of Stock Per Day</b>";
if($hour == 13){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from ecommercedashboardstat where ecommercestatdate >= '$date30' 
	and ecommercestattype = 'Volume of Unprocessed Out Of Stock Per Day'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select ecommerceoutofstock.requestdate as 'Dates', 
	COUNT(ecommerceoutofstock.ecommerceoutofstockid) as 'Value' 
	from ecommerceoutofstock 
	where ecommerceoutofstock.ecommerceoutofstockstatusid = '1' 
	group by ecommerceoutofstock.requestdate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Volume of Unprocessed Out Of Stock Per Day';
		$statdate = $queryrow['Dates'];
		$statname = '';
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}


//INSERT ALL THE NEW DATA
echo "<br/><br/>insertarray: ".json_encode($insertarray);
$i = 1;
foreach($insertarray as $insertrow){
	$stattype = $insertrow['stattype'];
	$statdate = $insertrow['statdate'];
	$statname = $insertrow['statname'];
	$statname = str_replace("'", "", $statname);
	$statname = str_replace('"', "", $statname);
	$statname = mysql_real_escape_string($statname);
	$statvalue = $insertrow['statvalue'];
	if($i == 1){
		$insert = "insert into ecommercedashboardstat (ecommercestattype, ecommercestatdate, ecommercestatname, ecommercestatvalue, 
		disabled, datecreated, masteronly) values ";	
	}
	$insert = $insert."('$stattype', '$statdate', '$statname', '$statvalue', '0', '$date', '0'), ";
	if($i == 1000){
		$insert = rtrim($insert, ", ");
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$insert = mysql_query($insert);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Insert 1000 rows: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;		
		$i = 1;
	}
	else {
		$i = $i + 1;
	}
}
if($i <> 1){
	$insert = rtrim($insert, ", ");
	echo "<br/><br/>insert: ".$insert;
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$insert = mysql_query($insert);
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Insert remaining rows: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;	
}



echo "<br/><br/>";
?>