<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include('1000003 - config.php');
include('googleconfig.php');

$date = date('Y-m-d');
$datetime = date('Y-m-d H:i:s');
$date2 = date('Y-m-d', strtotime("- 180 days"));
//echo "<br/>Date 2 : ".$date2;
$limitcount = 0;

$limitcheck = "SELECT count(zzbusinessid) AS count_value FROM zzbusiness WHERE MONTH(datecreated) = MONTH(CURDATE()) AND YEAR(datecreated) = YEAR(CURDATE()) AND googleplaceid <> '' ";
$limitcheck = mysql_query($limitcheck);
while($getcount = mysql_fetch_array($limitcheck))
{
    $limitcount = $getcount['count_value'];
}

$enhancementdataarray = array();
$enhancementdata = mysql_query("SELECT subcategoryid, includewords, excludewords FROM zzbusinessimportmetadata GROUP BY subcategoryid, includewords, excludewords");
while($enhancementdatarow = mysql_fetch_array($enhancementdata))
{
    array_push($enhancementdataarray, array(
        'id' => $enhancementdatarow['subcategoryid'],
        'inc_words' => $enhancementdatarow['includewords'],
        'exc_words' => $enhancementdatarow['excludewords']
    ));
}

if($limitcount < 10000)
{
    $getrecords = "SELECT zzbusinessimportmetadataid, locationname, subcategoryname, subcategoryid, latitude, longitude, radius, includewords, excludewords FROM zzbusinessimportmetadata 
    WHERE lastscanneddate <= '$date2' 
    limit 30";
    echo "<br/>Records : ".$getrecords;
    $getrecords = mysql_query($getrecords);
    while($getrecordsrow = mysql_fetch_array($getrecords))
    {
        $zzbusinessimportmetadataid = $getrecordsrow['zzbusinessimportmetadataid'];
        $zzbusinesssubcategoryid = $getrecordsrow['subcategoryid'];
        
        $latitude = $getrecordsrow['latitude'];
        $longitude = $getrecordsrow['longitude'];
        $radius = $getrecordsrow['radius'];

        $includewords = $getrecordsrow['includewords'];
        $includewords = array_filter(explode(",", $includewords));
        $excludewords = $getrecordsrow['excludewords'];
        $excludewords = array_filter(explode(",", $excludewords));

        $searchquery = $getrecordsrow['subcategoryname']." in ".$getrecordsrow['locationname'];
        $searchquery = urlencode($searchquery);

        $url = $googlePlaceSearchUrl;
        $url = str_replace("parameters", "query=".$searchquery, $url);
        
        //Adding lat & lng with radius
        $url = $url."&location=".$latitude.",".$longitude."&radius=".(1609*$radius);
        
        $url = $url."&key=".$googleApiKey;
        echo "<br/>URL : ".$url;

        $response = getDataFromGooglePlacesSearchApi($url, $googlePlaceSearchUrl, $googleApiKey);

        echo "<br/><br/>Data: ".json_encode($response);

        $count = 0;
        foreach($response as $data)
        {
            $googleplaceid = $data->place_id;
            $businessname = $data->name;
            $googleLatitude = $data->geometry->location->lat;
            $googleLongitude = $data->geometry->location->lng;
            echo "<br/>googleplaceid: ".$googleplaceid;
            echo "<br/>businessname: ".$businessname;
            
            $funcresult = calculateDistance($latitude, $longitude, $radius, $googleLatitude, $googleLongitude);
            echo "<br/>funcres: ".$funcresult;
            if($funcresult)
            {
                $finalresult = checkForIncludeExcludeWords($businessname, $includewords, $excludewords);

                if($finalresult)
                {
                    $businessname = str_replace("'","\'",$businessname);
            
                    $getbusinessrecords = "SELECT * FROM zzbusiness WHERE googleplaceid = '$googleplaceid' AND zzbusinessname = '$businessname' ";
                    echo "<br/>".$getbusinessrecords;
                    $getbusinessrecords = mysql_query($getbusinessrecords);  
                                     
                    if(mysql_num_rows($getbusinessrecords) == 0)
                    {
                    	  $check2 = 0;	
                    	  foreach($includewords as $w)
					        {
					            if(stripos($businessname, $w) !== false)
					            {
					                $check2 = 1;
					                echo "<br/>check2: ".$check2;
					            }
					        }
					        
                        $insertrecord = "INSERT INTO zzbusiness (zzbusinessname, datecreated, businessimportmetadataid, googleplaceid, 
                        subcategoryid, subcategoryconfirmed) 
                        VALUES ('$businessname', '$date', $zzbusinessimportmetadataid , '$googleplaceid', '$zzbusinesssubcategoryid', 
                        '$check2')";
                        
                        echo "<br/>".$insertrecord;
                        
                        $insertrecord = mysql_query($insertrecord);
                        $lastid = mysql_insert_id();

                        echo "<br/>last inserted id : ".$lastid;
                        checkForCorrectSubcategory($lastid, $zzbusinesssubcategoryid, $businessname, $enhancementdataarray);
                    }
                    $count++;
                }
            }
        }
        echo "<br/><br/>Total Results : ".$count;
        $updatesql = "UPDATE zzbusinessimportmetadata SET lastscanneddate = '$datetime' WHERE zzbusinessimportmetadataid = $zzbusinessimportmetadataid ";
        $updatesql = mysql_query($updatesql);
    }
}

function getDataFromGooglePlacesSearchApi($url, $googlePlaceSearchUrl, $googleApiKey)
{
    $result = array();

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    // echo "<br/><br/>response: ".$response;
    $response = json_decode($response);
    //echo "<br/><br/>response status: ".$response->status;
    if($response->status == 'OK')
    {
        foreach($response->results as $data)
        {
            array_push($result, $data);
        }

        if(isset($response->next_page_token) && ($response->next_page_token <> '' || $response->next_page_token <> null))
        {
            $pagetoken = $response->next_page_token;
            $url = $googlePlaceSearchUrl;
            $url = str_replace("parameters", "pagetoken=".$pagetoken, $url);
            $url = $url."&key=".$googleApiKey;
            //echo "<br/><br/>URL: ".$url;
            sleep(2);
            $nextPageResults = getDataFromGooglePlacesSearchApi($url, $googlePlaceSearchUrl, $googleApiKey);
            //echo "<br/><br/>nextpagedata ".json_encode($nextPageResults);
            if($nextPageResults <> null)
            $result = array_merge($result, $nextPageResults);
        }
        return $result;
    }
}

function calculateDistance($latitude, $longitude, $radius, $googleLatitude, $googleLongitude)
{
    $theta = $longitude - $googleLongitude; 
    $distance = (sin(deg2rad($latitude)) * sin(deg2rad($googleLatitude))) + (cos(deg2rad($latitude)) * cos(deg2rad($googleLatitude)) * cos(deg2rad($theta))); 
    $distance = acos($distance); 
    $distance = rad2deg($distance); 
    $distance = $distance * 60 * 1.1515; 
    
    $distance = round($distance,2);
    echo "<br/>distance : ".$distance;
    
    if($distance <= $radius)
    return true;
    else
    return false;
}

function checkForIncludeExcludeWords($businessname, $includewords, $excludewords)
{
    $rankPositive = $rankNegative = 0;
    $businessname = strtolower($businessname);
    if(count($includewords) > 0)
    foreach($includewords as $word)
    {
        $word = strtolower($word);
        if(strpos($businessname, $word) !== false)
        {
            $rankPositive++;
        }
    }
    
    if(count($excludewords) > 0)
    foreach($excludewords as $word)
    {
        $word = strtolower($word);
        if(strpos($businessname, $word) !== false)
        {
            $rankNegative++;
        }
    }

    if(($rankPositive == 0 && $rankNegative == 0) || $rankPositive > $rankNegative)
    return true;
    else
    return false;
}

function checkForCorrectSubcategory($lastid, $zzbusinesssubcategoryid, $businessname, $enhancementdataarray)
{
    $final = array();
    $final['id'] = 0;
    $final['count'] = 0;
        
    foreach($enhancementdataarray as $val) 
    {
        $count = 0;
        $incWords = array_filter(explode(",", $val['inc_words']));
        $excWords = array_filter(explode(",", $val['exc_words']));
        $id = $val['id'];
        
        if(count($incWords) > 0)
        foreach($incWords as $w)
        {
            if(stripos($businessname, $w) !== false)
            {
                $count++;
            }
        }
        
        if(count($excWords) > 0)
        foreach($excWords as $w)
        {
            if(stripos($businessname, $w) !== false)
            {
                $count--;
            }
        }
        
        if($count > 0 && $final['count'] < $count)
        {
            $final['id'] = $id;
            $final['count'] = $count;
        }
    }
    
    if($final['count'] > 0 && $zzbusinesssubcategoryid != $final['id'])
    {
        $updatedsubcategory = $final['id'];
        mysql_query("UPDATE zzbusiness SET subcategoryid = $updatedsubcategory, subcategoryconfirmed = 1 WHERE zzbusinessid = $lastid");
    }
}

echo "<br/><br/>END";
?>