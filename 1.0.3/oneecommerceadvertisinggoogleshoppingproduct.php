<?php

// include your composer dependencies
require_once '../1.0.3/google-api-php-contentapi/vendor/autoload.php';

$date20 = date('Y-m-d', strtotime("- 20 days"));
$date = date('Y-m-d');
		

//GET CREDENTIALS FOR API CONNECTION
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$getaccount = mysql_query("select ecommercewebsiteadvertisingconfigname, ecommercewebsiteadvertisingconfigid, 
ecommercewebsiteadvertisingconfig.ecommercesiteconfigid, ecommercewebsiteadvertisingconfig.apikey1, 
ecommercewebsiteadvertisingconfig.apikey2, ecommercewebsiteadvertisingconfig.apikey3, titleconfiguration,
ecommercewebsiteadvertisingconfig.apikey4, ecommercesiteconfig.productindividual from ecommercewebsiteadvertisingconfig
inner join ecommercesiteconfig on ecommercesiteconfig.ecommercesiteconfigid = ecommercewebsiteadvertisingconfig.ecommercesiteconfigid 
where ecommercewebsiteadvertisingconfig.disabled = 0");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get accounts: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
while($getaccountrow = mysql_fetch_array($getaccount)){
	$ecommercewebsiteadvertisingconfigid = $getaccountrow['ecommercewebsiteadvertisingconfigid'];
	$ecommercewebsiteadvertisingconfigname = $getaccountrow['ecommercewebsiteadvertisingconfigname'];
	$ecommercesiteconfigid = $getaccountrow['ecommercesiteconfigid'];
	$titleconfiguration = $getaccountrow['titleconfiguration'];
	$productindividual = $getaccountrow['productindividual'];
	$merchantid = $getaccountrow['apikey1'];
	$email = $getaccountrow['apikey2'];
	$privatekey = $getaccountrow['apikey3'];
	$idstart = $getaccountrow['apikey4'];
	$idstartvals = explode(":", $idstart);
	$location = $idstartvals[0];
	$language = $idstartvals[1];
	$country = $idstartvals[2];
	echo "<br/><br/><b>PROCESS MERCHANTID: ".$merchantid."</b>";
	//echo "<br/>email: ".$email;
	$privatekey = str_replace("\\n", "
", $privatekey);
	//echo "<br/>privatekey: ".$privatekey;
	
	//GET ACCESS TOKEN THAT LASTS 1 HOUR
	$header = json_encode(['alg' => 'RS256', 'typ' => 'JWT']);
	$base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));
	$iat = strtotime("now");
	$exp = strtotime("+1 hour");
	$currenttime = date("H:i:s");
	$claimset = json_encode(['iss' => $email, 
	'scope' => 'https://www.googleapis.com/auth/content', 
	'aud' => 'https://www.googleapis.com/oauth2/v4/token', 
	'exp' => $exp, 
	'iat' => $iat]);
	$base64UrlClaimSet = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($claimset));
	$binary_signature = "";
	$algo = "SHA256";
	openssl_sign($base64UrlHeader.".".$base64UrlClaimSet, $binary_signature, $privatekey, $algo);
	$jwtSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($binary_signature));
	$jwt = $base64UrlHeader . "." . $base64UrlClaimSet . "." . $jwtSignature;
	//echo "<br/><br/>jwt: ".$jwt;
	$url = "https://www.googleapis.com/oauth2/v4/token";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer&assertion=".$jwt);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$data = curl_exec($ch);
	//echo "<br/><br/>data: ".$data;
	$accesstoken = substr($data, strpos($data, 'access_token') + 16);
	$arr = explode('"',trim($accesstoken));
	$accesstoken = $arr[0]; 
	//echo "<br/><br/>accesstoken: ".$accesstoken;


	//google shopping requires that all products are updated every 30 days, or else they get deleted - this ensures all products are updated every 20 days
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getprocessnewexisting = mysql_query("update ecommercelistinglocation 
	set updategoogleshoppingproductdata = 1
	where updategoogleshoppinglastupdateddate <= '$date20' and ecommercesiteconfigid = $ecommercesiteconfigid
	and websiteurl <> ''");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get existing/new records to process: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;

	//check if any previous removals can now be reversed and reslisted
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$checkremoved = mysql_query("update ecommercelistinglocation 
	set updategoogleshoppingstatus = '', updategoogleshoppinglastupdateddate = '0000-00-00', updategoogleshoppingproductdata = 1
	where ecommercelistinglocationstatusid = 2 and ecommercesiteconfigid = $ecommercesiteconfigid
	and websiteurl <> '' and updategoogleshoppingstatus = 'Removed'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Check if any previous removals can now be reversed and reslisted: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;


	//CHECK IF THERE ARE ANY REMOVALS TO PROCESS
	echo "<br/><br/><b>CHECK IF THERE ARE ANY REMOVALS TO PROCESS</b>";
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	if($productindividual == 0){
		$getprocessremovals = mysql_query("select ecommercelistinglocationid, productid, productstockitemid 
		from ecommercelistinglocation 
		where ecommercelistinglocationstatusid <> 2 and updategoogleshoppingstatus = 'Active' 
		and ecommercesiteconfigid = $ecommercesiteconfigid and productid not in (select productid 
		from ecommercelistinglocation where ecommercesiteconfigid = $ecommercesiteconfigid 
		and ecommercelistinglocationstatusid = 2)
		limit 100");
	}
	else {
		$getprocessremovals = mysql_query("select ecommercelistinglocationid, productid, productstockitemid 
		from ecommercelistinglocation
		where ecommercelistinglocationstatusid <> 2 and updategoogleshoppingstatus = 'Active' 
		and ecommercesiteconfigid = $ecommercesiteconfigid
		limit 100");
	}
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get removal records to process: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;	
	$productremovalarray = array();
	$s = 1;
	$productidlist = "";
	$productstockitemidlist = "";
	while($getprocessremovalsrow = mysql_fetch_array($getprocessremovals)){
		$ecommercelistinglocationid = $getprocessremovalsrow['ecommercelistinglocationid'];
		$productid = $getprocessremovalsrow['productid'];
		$productstockitemid = $getprocessremovalsrow['productstockitemid'];
		$productidlist = $productidlist.$productid.",";
		$productstockitemidlist = $productstockitemidlist.$productstockitemid.",";
		if($productindividual == 0){
			$uniqueid = $idstart."P-".$productid;
			$uniqueid1 = "P-".$productid;
		}
		else {
			$uniqueid = $idstart."PSI-".$productstockitemid;		
			$uniqueid1 = "PSI-".$productstockitemid;		
		}
		
		echo "<br/>uniqueid: ".$uniqueid;
		array_push($productremovalarray, array("batchId"=> $s,"merchantId"=> $merchantid,
      "method"=> "delete", "productId"=> $uniqueid));
      
      $s = $s + 1;
	}
	$productstockitemidlist = rtrim($productstockitemidlist, ",");
	$productidlist = rtrim($productidlist, ",");
	
	if(count($productremovalarray) >= 1){
		$productremovalarray = array("entries" => $productremovalarray);
		echo "<br/><br/>productremovalarray: ".json_encode($productremovalarray);
		
		//INSERT LIST OF ALL PRODUCTS (updates products it finds to be already existing)
		$url = "https://www.googleapis.com/content/v2/products/batch";
		$header = array(
		    'Authorization: Bearer '.$accesstoken,
		    'Content-Type: application/json'
		);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($productremovalarray));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$data = curl_exec($ch);
		
		echo "<br/><br/>data: ".$data;
		$data = json_decode($data, true);	
		
		if(count($data['entries']) >= 1){
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			if($productindividual = 0){
				$updateremoved = "update ecommercelistinglocation 
				set updategoogleshoppingstatus = 'Removed' 
				where productid in ($productidlist)";
				echo "<br/>updateremoved: ".$updateremoved;
				if($productidlist <> ''){
					$updateremoved = mysql_query($updateremoved);
				}
			}
			else {
				if($productstockitemidlist <> ''){
					$updateremoved = mysql_query("update ecommercelistinglocation 
					set updategoogleshoppingstatus = 'Removed', updategoogleshoppinglastupdateddate = '$date' 
					where productstockitemid in ($productstockitemidlist)");
				}
			}
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Update to removed: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		}
		else {	
			echo "<br/>None Found";	
		}
	}
	
	
	//CHECK IF THERE ARE ANY EXISTING/NEW RECORDS TO PROCESS
	echo "<br/><br/><b>CHECK IF THERE ARE ANY EXISTING/NEW RECORDS TO PROCESS</b>";
	
	//setup batching		
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$setupbatching = mysql_query("select min(ecommercelistinglocationid) as 'minval', max(ecommercelistinglocationid) as 'maxval' 
	from ecommercelistinglocation");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get batching stats: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($row10 = mysql_fetch_array($setupbatching)){
		$minid = $row10['minval'];
		$maxid = $row10['maxval'];
	}	
	
	$hour = date("H");
	echo "<br/>hour: ".$hour;
	/*
	if($hour == 0 || $hour == 6 || $hour == 12 || $hour == 18){
		$randno = 1;		
	}
	if($hour == 1 || $hour == 7 || $hour == 13 || $hour == 19){
		$randno = 2;		
	}
	if($hour == 2 || $hour == 8 || $hour == 14 || $hour == 20){
		$randno = 3;		
	}
	if($hour == 3 || $hour == 9 || $hour == 15 || $hour == 21){
		$randno = 4;		
	}
	if($hour == 4 || $hour == 10 || $hour == 16 || $hour == 22){
		$randno = 5;		
	}
	if($hour == 5 || $hour == 11 || $hour == 17 || $hour == 23){
		$randno = 6;		
	}
	*/
	$randno = rand(1,6);
	$total = $maxid - $minid;
	$range = $total / 6;
	//$randno = 1;
	if($randno == 1){
		$startid = $minid;
		$stopid = $minid + $range;
	}
	if($randno == 2){
		$startid = $minid + $range;
		$stopid = $maxid - ($range * 4);
	}
	if($randno == 3){
		$startid = $minid + ($range * 2);
		$stopid = $maxid - ($range * 3);
	}
	if($randno == 4){
		$startid = $minid + ($range * 3);
		$stopid = $maxid - ($range * 2);
	}
	if($randno == 5){
		$startid = $minid + ($range * 4);
		$stopid = $maxid - $range;
	}
	if($randno == 6){
		$startid = $maxid - $range;
		$stopid = $maxid;
	}
	
	
	$startid = round($startid);
	$stopid = round($stopid);
	echo "<br/><br/>minid: ".$minid;
	echo "<br/>maxid: ".$maxid;
	echo "<br/>total: ".$total;
	echo "<br/>range: ".$range;
	echo "<br/>randno: ".$randno;
	echo "<br/>startid: ".$startid;
	echo "<br/>stopid: ".$stopid;
	
	$convert = array(
        '¼'=>'',
        'Â'=>'',
        'Ã'=>'',
        'ä'=>'a',
        'Ä'=>'A',
        'á'=>'a',
        'Á'=>'A',
        'à'=>'a',
        'À'=>'A',
        'ã'=>'a',
        'Ã'=>'A',
        'â'=>'a',
        'Â'=>'A',
        'č'=>'c',
        'Č'=>'C',
        'ć'=>'c',
        'Ć'=>'C',
        'ď'=>'d',
        'Ď'=>'D',
        'ě'=>'e',
        'Ě'=>'E',
        'é'=>'e',
        'É'=>'E',
        'ë'=>'e',
    	);
    	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getprocessnewexisting = "select ecommercelistinglocation.productid, ecommercelistinglocationid, 
	ecommercelistingid, ecommercelistinglocation.productstockitemid, productindividual, productname, productlongdescription, 
	productproducer, productuniqueid, listingprice, ecommercesiteconfig.currencyid, ecommercelistinglocation.websiteurl, productitemdescription, 
	kgweight, if(structurefunctionid = 74, documenturl, '') as documenturl, currencycode, productimage1
	from ecommercelistinglocation 
	inner join ecommercesiteconfig on ecommercesiteconfig.ecommercesiteconfigid = ecommercelistinglocation.ecommercesiteconfigid
	inner join product on product.productid = ecommercelistinglocation.productid
	left join structurefunctiondocument on structurefunctiondocument.rowid = product.productid
	inner join productstockitem on ecommercelistinglocation.productstockitemid = productstockitem.productstockitemid
	inner join currency on currency.currencyid = ecommercesiteconfig.currencyid
	where ecommercelistinglocationid >= $startid and ecommercelistinglocationid < $stopid and
	ecommercelistinglocation.ecommercesiteconfigid = $ecommercesiteconfigid and updategoogleshoppingproductdata = 1 
	and ecommercelistinglocationstatusid = 2 and (productitemdescription = 'New' or productitemdescription = '') 
	and ((doctypename = 'Primary Image' 
	and structurefunctionid = 74) or (productimage1 <> '' and (structurefunctionid <> 74 or structurefunctionid is null))) 
	and productlongdescription <> '' and listingprice > 0 and productproducer <> '' 
	and updategoogleshoppingerrormessage = '' and ecommercelistinglocation.websiteurl <> ''
	limit 50";
	echo "<br/>getprocessnewexisting: ".$getprocessnewexisting;
	$getprocessnewexisting = mysql_query($getprocessnewexisting);
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get existing/new records to process: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;	
	$productarray = array();
	$insertproductarray = array();
	$i = 1;
	while($getprocessnewexistingrow = mysql_fetch_array($getprocessnewexisting)){
		$ecommercelistinglocationid = $getprocessnewexistingrow['ecommercelistinglocationid'];
		$productid = $getprocessnewexistingrow['productid'];
		$productstockitemid = $getprocessnewexistingrow['productstockitemid'];
		$productindividual = $getprocessnewexistingrow['productindividual'];
		$productname = $getprocessnewexistingrow['productname'];
		$productname = strtr($productname , $convert);
		$productname = utf8_encode($productname);
		$productlongdescription = $getprocessnewexistingrow['productlongdescription'];
		$productproducer = $getprocessnewexistingrow['productproducer'];
		$productproducer = str_replace("¼", "", $productproducer);
		$productproducer = strtr($productproducer , $convert);
		$productproducer = utf8_encode($productproducer);
		$productuniqueid = $getprocessnewexistingrow['productuniqueid'];
		$listingprice = $getprocessnewexistingrow['listingprice'];
		$currencyid = $getprocessnewexistingrow['currencyid'];
		$currencycode = $getprocessnewexistingrow['currencycode'];
		$websiteurl = $getprocessnewexistingrow['websiteurl'];
		echo "<br/>websiteurl: ".$websiteurl;
		$productitemdescription = $getprocessnewexistingrow['productitemdescription'];
		$kgweight = $getprocessnewexistingrow['kgweight'];
		$documenturl = $getprocessnewexistingrow['documenturl'];
		$productimage1 = $getprocessnewexistingrow['productimage1'];
		if($documenturl <> ''){
			$documenturl = "https://www.onebusiness-liveserver.com/onebusiness/documents/".$getprocessnewexistingrow['documenturl'];
		}
		else {
			$documenturl = $productimage1;		
		}
		if($productindividual == 0){
			$uniqueid = $idstart."P-".$productid;
			$uniqueid1 = "P-".$productid;
		}
		else {
			$uniqueid = $idstart."PSI-".$productstockitemid;		
			$uniqueid1 = "PSI-".$productstockitemid;		
		}
		
		$productnamealtered = '';
		if($titleconfiguration <> ""){
			
			$titleconfiguration = str_replace("{", "", $titleconfiguration);		
			$titleconfiguration = str_replace("}", "", $titleconfiguration);	
			$explode6 = explode(",", $titleconfiguration);
			foreach($explode6 as $f){
				$explode7 = explode(".", $f);
				$tablename = $explode7[0];			
				$fieldname = $explode7[1];		
				
				if($tablename == "product"){
					if($fieldname == "productname"){
						$productnamealtered = $productnamealtered." - ".$productname;				
					}
					if($fieldname == "productproducer"){
						$productnamealtered = $productnamealtered." - ".$productproducer;		
					}				
				}
				else {
					$nosqlqueries = $nosqlqueries + 1;
					$sqlstarttime = microtime(true);
					$getdata = "select $fieldname from $tablename 
					where productid = '$productid'";
					//echo "<br/>getprocessnewexisting: ".$getprocessnewexisting;
					$getdata = mysql_query($getdata);
					$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
					$differencemilliseconds = microtime(true) - $sqlstarttime;
					echo "<br/>Get record: ";
					echo "<br/>differencemilliseconds: ".$differencemilliseconds;
					echo "<br/>sqlqueriestime: ".$sqlqueriestime;				
					$getdatarow = mysql_fetch_array($getdata);
					$fieldresult = $getdatarow[$fieldname];
					$productnamealtered = $productnamealtered." - ".$fieldresult;		
				}					
			}	
		}
		if($productnamealtered == ""){
			$productnamealtered = $productname;		
		}
		if(strlen($productnamealtered) >= 150){
			$productnamealtered = substr($productnamealtered, 0, 149);		
		}
		
		$productnamealtered = strtr($productnamealtered , $convert);
		$productnamealtered = utf8_encode($productnamealtered);
		echo "<br/>".$ecommercelistinglocationid." - ".$uniqueid." - ".$productnamealtered;
		
		array_push($productarray, array("batchid" => $i, "ecommercelistinglocationid" => $ecommercelistinglocationid, 
		"productid" => $productid, "productstockitemid" => $productstockitemid, 
		"productindividual" => $productindividual, "uniqueid" => $uniqueid, 
		"productname" => $productnamealtered, "productlongdescription" => $productlongdescription, 
		"productproducer" => $productproducer, "productuniqueid" => $productuniqueid, 
		"listingprice" => $listingprice, "currencyid" => $currencyid, 
		"websiteurl" => $websiteurl, "productitemdescription" => $productitemdescription, 
		"kgweight" => $kgweight, "documenturl" => $documenturl));
		
		array_push($insertproductarray, array("batchId"=>$i, "merchantId"=>(int)$merchantid, "method"=>"insert",
		"product"=>array("kind" => "content#product", "offerId" => $uniqueid1, "source" => "api", "title" => $productnamealtered,
     	"description" => $productlongdescription, "link" => $websiteurl, "imageLink" => $documenturl, "contentLanguage" => $language,
     	"targetCountry" => $country, "channel" => $location, "availability" => "in stock",  
     	"brand" => $productproducer, "condition" => "new", 
     	"gtin" => $productuniqueid, "price" => array("value" => $listingprice, 
     	"currency" => $currencycode), "shippingWeight" => array("unit"=>"kg", "value"=>$kgweight))));
		$i = $i + 1;
	
	}
	echo "<br/><br/>productarray: ".json_encode($productarray);
	
	if(count($productarray) >= 1){
		$insertproductarray = array("entries" => $insertproductarray);
		echo "<br/><br/>insertproductarray: ".json_encode($insertproductarray);
			
		
		//INSERT LIST OF ALL PRODUCTS (updates products it finds to be already existing)
		$url = "https://www.googleapis.com/content/v2/products/batch";
		$header = array(
		    'Authorization: Bearer '.$accesstoken,
		    'Content-Type: application/json'
		);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($insertproductarray));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$data = curl_exec($ch);
		
		echo "<br/><br/>data: ".$data;
		$data = json_decode($data, true);
		
		$foundarray = array();
		$errorarray = array();
		if(count($data['entries']) >= 1){
			foreach($data['entries'] as $founditem){
				if(isset($founditem['product']['offerId'])){
					$uniqueid = $founditem['product']['offerId'];
					array_push($foundarray, $uniqueid);		
				}
				else {
					$batchid = $founditem['batchId'];
					$key = array_search($batchid, array_column($productarray, 'batchid'));
					$ecommercelistinglocationid = htmlspecialchars($productarray[$key]['ecommercelistinglocationid']);	
					$errormessage = $founditem['errors']['errors'][0]['message'];
					array_push($errorarray, array("ecommercelistinglocationid" => $ecommercelistinglocationid, 
					"batchid" => $batchid, "errormessage" => $errormessage));		
				}
			} 
		}
		else {	
			echo "<br/>None Found";	
		}
		echo "<br/><br/>foundarray: ".json_encode($foundarray);
		echo "<br/><br/>errorarray: ".json_encode($errorarray);
		
		if(count($foundarray) >= 1){
			//update ecommercelistinglocation to show processed successfully
			$ecommercelistinglocationidsuccesslistpsi = "";
			$ecommercelistinglocationidsuccesslistp = "";
			foreach($foundarray as $ell){
				if($productindividual == 1){
					$ell = str_replace("PSI-", "", $ell);		
					$ecommercelistinglocationidsuccesslistpsi = $ecommercelistinglocationidsuccesslistpsi.$ell.",";					
				}
				else {
					$ell = str_replace("P-", "", $ell);	
					$ecommercelistinglocationidsuccesslistp = $ecommercelistinglocationidsuccesslistp.$ell.",";					
				}				
			}
			$ecommercelistinglocationidsuccesslistpsi = rtrim($ecommercelistinglocationidsuccesslistpsi, ",");
			$ecommercelistinglocationidsuccesslistp = rtrim($ecommercelistinglocationidsuccesslistp, ",");
			if($ecommercelistinglocationidsuccesslistpsi <> ''){
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$updatesuccess = "update ecommercelistinglocation 
				set updategoogleshoppingerrormessage = '', updategoogleshoppingproductdata = 0, 
				updategoogleshoppingstatus = 'Active', updategoogleshoppinglastupdateddate = '$date'
				where produtstockitemid in ($ecommercelistinglocationidsuccesslistpsi)";
				echo "<br/><br/>updatesuccess: ".$updatesuccess;
				$updatesuccess = mysql_query($updatesuccess);
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>update ecommercelistinglocation to show processed successfully: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;	
			}
			if($ecommercelistinglocationidsuccesslistp <> ''){
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$updatesuccess = "update ecommercelistinglocation 
				set updategoogleshoppingerrormessage = '', updategoogleshoppingproductdata = 0, 
				updategoogleshoppingstatus = 'Active', updategoogleshoppinglastupdateddate = '$date'
				where productid in ($ecommercelistinglocationidsuccesslistp)";
				echo "<br/><br/>updatesuccess: ".$updatesuccess;
				$updatesuccess = mysql_query($updatesuccess);
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>update ecommercelistinglocation to show processed successfully: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;	
			}	
		}
		
		if(count($errorarray) >= 1){
			//update ecommercelistinglocation to show processed successfully
			$updateerror = "update ecommercelistinglocation set updategoogleshoppinglastupdateddate = '$date', 
			updategoogleshoppingerrormessage = 
			case ecommercelistinglocationid ";
			$ecommercelistinglocationerrorlist = "";
			foreach($errorarray as $item){
				$ecommercelistinglocationid = $item['ecommercelistinglocationid'];	
				$errormessage = mysql_real_escape_string($item['errormessage']);	
				$updateerror = $updateerror." when ".$ecommercelistinglocationid.' then "'.$errormessage.'"'; 
				$ecommercelistinglocationerrorlist = $ecommercelistinglocationerrorlist.$ecommercelistinglocationid.",";	
			}
			$ecommercelistinglocationerrorlist = rtrim($ecommercelistinglocationerrorlist, ",");
			$updateerror = $updateerror." end where ecommercelistinglocationid in (".$ecommercelistinglocationerrorlist.")";
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			echo "<br/><br/>updateerror: ".$updateerror;
			$updateerror = mysql_query($updateerror);
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>update ecommercelistinglocation to show processed successfully: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;		
		}
	}
	
	else {
		if($i >= 20){
			die;		
		}	
	}
	
}

echo "<br/><br/>result: ".json_encode($result);
?>
