<?php
$query = "Select ";
$queryjoins = " ";
$querygroupby = "group by";
//construct select clause
$getdatasetcolumn = "select datasetcolumnname, format, aggregatecode, if(structurefield.structurefieldname is null, structurefieldmaster.structurefieldname, structurefield.structurefieldname) as 'structurefieldname',  ";
$getdatasetcolumn = $getdatasetcolumn."if(structurefunction.tablename is null, structurefunctionmaster.tablename, structurefunction.tablename) as 'tablename', ";
$getdatasetcolumn = $getdatasetcolumn."if(structurefunction.primarykeyfield is null, structurefunctionmaster.primarykeyfield, structurefunction.primarykeyfield) as 'primarykeyfield', ";
$getdatasetcolumn = $getdatasetcolumn."if(structurefunction.tablename is null, 1, 2) as 'fieldtype', ";

//Added isFinancial here...
$getdatasetcolumn = $getdatasetcolumn."connectedtablename, hidecolumn, isfinancial, calculationlogic from ".$datasetrecordtable;

if($datasetrecordtable <> 'dataset'){
	$getdatasetcolumn = $getdatasetcolumn." inner join dataset on dataset.datasetid = ".$datasetrecordtable.".datasetid";
}
$getdatasetcolumn = $getdatasetcolumn." inner join datasetcolumn on dataset.datasetid = datasetcolumn.datasetid";
$getdatasetcolumn = $getdatasetcolumn." left join structurefield on structurefield.structurefieldid = datasetcolumn.structurefieldid";
$getdatasetcolumn = $getdatasetcolumn." left join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid";
$getdatasetcolumn = $getdatasetcolumn." left join ".$masterdatabase.".structurefield as structurefieldmaster on structurefieldmaster.structurefieldid = datasetcolumn.structurefieldid ";
$getdatasetcolumn = $getdatasetcolumn." left join ".$masterdatabase.".structurefunction as structurefunctionmaster on structurefunctionmaster.structurefunctionid = structurefieldmaster.structurefunctionid ";
$getdatasetcolumn = $getdatasetcolumn." left join ".$masterdatabase.".datasetaggregate on datasetaggregate.datasetaggregateid = datasetcolumn.datasetaggregateid";
$getdatasetcolumn = $getdatasetcolumn." where ".$datasetrecordtable.".".$datasetrecordfield." = '$datasetrecordid' order by datasetcolumn.sortorder";
$counttable = 1;
//echo $getdatasetcolumn;

$mysqli = new mysqli($server, $user_name, $password, $database);if($stmt = $mysqli->prepare($getdatasetcolumn)){   $stmt->execute();   $result = $stmt->get_result();
   $numfields = $result->num_rows;   if($result->num_rows > 0){
   		while($rowcolumn = $result->fetch_assoc()){     		$structurefieldname	= $rowcolumn['structurefieldname'];
			//echo "<br/>".$structurefieldname;
			$tablename = $rowcolumn['tablename'];
			//echo "<br/>".$tablename;
			//check table exists
			$mysqli2 = new mysqli($server, $user_name, $password, $database);			if($stmt2 = $mysqli2->prepare("show tables like '$tablename'")){			   $stmt2->execute();			   $result2 = $stmt2->get_result();
			   $count10 = $result2->num_rows;
			}			$mysqli2->close();
			if($count10 == 0){
		   		$tablename2 = "onebusinessmaster.".$tablename;	
		   }
		   else {			   		
		   		$tablename2 = $tablename;		
		   	}

			$datasetcolumnname	= $rowcolumn['datasetcolumnname'];
			$primarykeyfield	= $rowcolumn['primarykeyfield'];
			$connectedtable	= $rowcolumn['connectedtablename'];
			$calculationlogic	= $rowcolumn['calculationlogic'];
			$aggregatecode	= $rowcolumn['aggregatecode'];
			$hidecolumn	= $rowcolumn['hidecolumn'];
			$isfinancial	= $rowcolumn['isfinancial'];
			$fieldtype	= $rowcolumn['fieldtype'];
			$format	= $rowcolumn['format'];
			if($hidecolumn==0){
				if($calculationlogic <> ""){
					$aggregatecode = "Calculated Logic";
				}
				if($aggregatecode == "Calculated Logic"){
					$query = $query.$calculationlogic." as '".$datasetcolumnname."', ";
				}
				if($aggregatecode == "" || $aggregatecode == "GROUP BY"){
					$query = $query.$tablename.".".$structurefieldname." as '".$datasetcolumnname."', ";
				}
				/*if($aggregatecode == "SUM" || $aggregatecode == "COUNT"){
					$query = $query.$aggregatecode."(".$tablename.".".$structurefieldname.") as '".$datasetcolumnname."', ";
				}*/
				if ($aggregatecode == "SUM") {
				    /*$query = $query ."ROUND(". $aggregatecode . "(" . $tablename . "." . $structurefieldname . "), 2) as '" . $datasetcolumnname . "', ";*/
				    if ($isfinancial == 1) {
				        $query = $query ."ROUND(". $aggregatecode . "(" . $tablename . "." . $structurefieldname . "), 2) as '" . $datasetcolumnname  . " (".$_SESSION['currencysymbol'].")', ";
				    }
				    else {
				        $query = $query ."ROUND(". $aggregatecode . "(" . $tablename . "." . $structurefieldname . "), 2) as '" . $datasetcolumnname . "', ";
				    }
				}
				if ($aggregatecode == "COUNT") {
					$query = $query . $aggregatecode . "(" . $tablename . "." . $structurefieldname . ") as '" . $datasetcolumnname . "', ";
				}
				if($aggregatecode == "GROUP BY MONTH"){
					if($format <> ""){
						$query = $query."DATE_FORMAT(".$tablename.".".$structurefieldname.", '".$format."') as '".$datasetcolumnname."', ";
					}
					else {
						$query = $query.$tablename.".".$structurefieldname." as '".$datasetcolumnname."', ";
					}					
				}	
				if($aggregatecode == "GROUP BY"){
					$querygroupby = $querygroupby." ".$tablename.".".$structurefieldname.", ";	
				}
				if($aggregatecode == "GROUP BY MONTH"){
					$querygroupby = $querygroupby." DATE_FORMAT(".$tablename.".".$structurefieldname.", '".$format."'), ";	
				}
			}
			if($counttable == 1){
				$queryjoins = $queryjoins."from ".$tablename2;							
				$counttable = 2;		
			}
			else {
				if($connectedtable<>""){
					$queryjoins = $queryjoins." ".$connectedtable;
				}
			}
			//check if businessunitid field exists and user is not admin user
			$buwhere = "";
			if($fieldtype == 2){
				$mysqli3 = new mysqli($server, $user_name, $password, $database);				if($stmt3 = $mysqli3->prepare("SELECT * FROM $tablename LIMIT 1")){				   $stmt3->execute();				   $result3 = $stmt3->get_result();				   if($result3->num_rows > 0){				    	while($mycol = $result3->fetch_assoc()){				     		if(isset($mycol['businessunitid'])) {
								$businessunitexist = 1;				
							} else {
								$businessunitexist = 0;					
							}
						  	if($businessunitexist == 1 && $_SESSION["superadmin"] == 0){
						  		$buwhere = $buwhere." and ".$tablename.".businessunitid in (".$_SESSION["userbusinessunit"].")";
							}				    	}				   }				}				$mysqli3->close();
			  	
				
			}    	}   }}$mysqli->close();

	

$query = substr($query,0,-2);
$querygroupby = substr($querygroupby,0,-2);
//echo $querygroupby;
if($querygroupby=="group "){
	$querygroupby = "";				
}
//construct the filtering options
if(!isset($viewextra)){
	$viewextra = 0;
}
//used for map search fields
if(isset($_POST['submitsearch'])){
	$getdatasetfilters = "select * from ".$filtertable ;
   	$getdatasetfilters = $getdatasetfilters." inner join datasetcolumn on ".$filtertable.".datasetcolumnid = datasetcolumn.datasetcolumnid";
  	$getdatasetfilters = $getdatasetfilters." inner join ".$masterdatabase.".datasetfilterlogic on datasetfilterlogic.datasetfilterlogicid = viewmapfilter.datasetfilterlogicid";
	$getdatasetfilters = $getdatasetfilters." inner join structurefield on structurefield.structurefieldid = datasetcolumn.structurefieldid";
	$getdatasetfilters = $getdatasetfilters." inner join ".$masterdatabase.".structurefieldtype on structurefieldtype.structurefieldtypeid = structurefield.structurefieldtypeid";
   	$getdatasetfilters = $getdatasetfilters." inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid";
	$getdatasetfilters = $getdatasetfilters." where ".$filterfield." = '".$filterfieldid."'";
}
//used for view datasets
if($viewextra == 1){
	$getdatasetfilters = "select structurefieldname, tablename, fieldtypename, logiccode, datasetfiltername from dataset";
   	$getdatasetfilters = $getdatasetfilters." inner join datasetcolumn on dataset.datasetid = datasetcolumn.datasetid";
   	$getdatasetfilters = $getdatasetfilters." inner join datasetfilter on datasetfilter.datasetcolumnid = datasetcolumn.datasetcolumnid";
  	$getdatasetfilters = $getdatasetfilters." inner join ".$masterdatabase.".datasetfilterlogic on datasetfilterlogic.datasetfilterlogicid = datasetfilter.datasetfilterlogicid";
	$getdatasetfilters = $getdatasetfilters." inner join structurefield on structurefield.structurefieldid = datasetcolumn.structurefieldid";
	$getdatasetfilters = $getdatasetfilters." inner join ".$masterdatabase.".structurefieldtype on structurefieldtype.structurefieldtypeid = structurefield.structurefieldtypeid";
   	$getdatasetfilters = $getdatasetfilters." inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid";
	$getdatasetfilters = $getdatasetfilters." where dataset.datasetid = '".$filterfieldid."'";
}	
//used for dashboards and reports

//for Dataset Operator (TL-128)...
if(!isset($_POST['submitsearch']) && $viewextra <> 1) {
	$getdatasetfilters = "select structurefieldname, fieldtypename, tablename, logiccode, datasetfiltername, operator";
	$getdatasetfilters .= " from ".$datasetrecordtable;
	if($datasetrecordtable <> 'dataset'){
		$getdatasetfilters .= " inner join dataset on dataset.datasetid = ".$datasetrecordtable.".datasetid";
	}
	$getdatasetfilters .= " inner join datasetcolumn on dataset.datasetid = datasetcolumn.datasetid";
	$getdatasetfilters .= " inner join datasetfilter on datasetcolumn.datasetcolumnid = datasetfilter.datasetcolumnid";
	$getdatasetfilters .= " inner join ".$masterdatabase.".datasetfilterlogic on datasetfilter.datasetfilterlogicid = datasetfilterlogic.datasetfilterlogicid";
	
	//for Dataset Operator (TL-128)...
	$getdatasetfilters .= " left join ".$masterdatabase.".datasetfilteroperator on datasetfilter.operatorid = datasetfilteroperator.datasetfilteroperatorid";
	
	
	$getdatasetfilters .= " inner join structurefield on structurefield.structurefieldid = datasetcolumn.structurefieldid";
	$getdatasetfilters .= " inner join ".$masterdatabase.".structurefieldtype on structurefieldtype.structurefieldtypeid = structurefield.structurefieldtypeid";
	$getdatasetfilters .= " inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid";
	$getdatasetfilters .= " left join ".$masterdatabase.".datasetaggregate on datasetaggregate.datasetaggregateid = datasetcolumn.datasetaggregateid";
	$getdatasetfilters .= " where ".$datasetrecordtable.".".$datasetrecordfield." = '".$datasetrecordid."' order by datasetcolumn.sortorder";
}

$countfilter = 1;
$querywhere = "";

$mysqli = new mysqli($server, $user_name, $password, $database);if($stmt = $mysqli->prepare($getdatasetfilters)){   
    $stmt->execute();   
    $result = $stmt->get_result();   
    if($result->num_rows > 0){    	
        while($rowfilter = $result->fetch_assoc()){     		$structurefieldname	= $rowfilter['structurefieldname'];
			$tablename	= $rowfilter['tablename'];
			$fieldtypename	= $rowfilter['fieldtypename'];
			$logiccode	= $rowfilter['logiccode'];
			
			//For Dataset Operator (TL-128)...
			$operator	= $rowfilter['operator'];
			if(isset($_POST['submitsearch'])){	
				$counter56 = 1;
				$datasetfiltername = '';
				while($counter56<=$numfields){
					if(${"structurefieldname$counter56"} == $structurefieldname){
						$datasetfiltername = ${"data$counter56"};
					}
					$counter56 = $counter56+1;
				}
			}
			else{
				$datasetfiltername = $rowfilter['datasetfiltername'];
			}
			if($logiccode <> ''){
				if($logiccode == "AND NOT"){
				    //For Dataset Operator (TL-128)...
				    if($operator <> ''){
				        $querywhere = $querywhere." and ".$tablename.".".$structurefieldname." ".$operator. " ".$datasetfiltername;
				    } else {
				       $querywhere = $querywhere." and ".$tablename.".".$structurefieldname." <> '".$datasetfiltername."'";
				    }
				}
				if(($fieldtypename == 'Checkbox' && $datasetfiltername == 1) || $fieldtypename == 'Date Select'){
					if(strpos($datasetfiltername, '#date') !== false){
						$date2 = "'".date('Y-m-d')."'";
						$datasetfiltername = str_replace("#date", $date2, $datasetfiltername);		
						if($logiccode == "AND"){
							$querywhere = $querywhere." and ".$tablename.".".$structurefieldname."  ".$datasetfiltername;
						}
						if($logiccode == "OR"){
							$querywhere = $querywhere." or ".$tablename.".".$structurefieldname."  ".$datasetfiltername;
						}			
					}
					else {
						if($logiccode == "AND"){
						    //For Dataset Operator (TL-128)...
						    if($operator <> ''){
        				        $querywhere = $querywhere." and ".$tablename.".".$structurefieldname." ".$operator. " '".$datasetfiltername."'";
        				    } else {
        				       $querywhere = $querywhere." and ".$tablename.".".$structurefieldname." = '".$datasetfiltername."'";
        				    }
						}
						if($logiccode == "OR"){
						    //For Dataset Operator (TL-128)...
						    if($operator <> ''){
        				        $querywhere = $querywhere." or ".$tablename.".".$structurefieldname." ".$operator. " '".$datasetfiltername."'";
        				    } else {
        				       $querywhere = $querywhere." or ".$tablename.".".$structurefieldname." = '".$datasetfiltername."'";
        				    }
						}
					}
				}
				
				//For Dataset Operator (TL-128)...
				if($fieldtypename == 'Text' /*|| $fieldtypename == 'Status'*/){
					if($logiccode == "AND"){
					    if($operator <> ''){
					        if ($operator == '%LIKE%') {
					            $querywhere = $querywhere." and ".$tablename.".".$structurefieldname." like '%".$datasetfiltername."%'";
					        } else {
					            $querywhere = $querywhere." and ".$tablename.".".$structurefieldname." ".$operator. " '".$datasetfiltername."'";
					        }
					    } else {
					       $querywhere = $querywhere." and ".$tablename.".".$structurefieldname." like '%".$datasetfiltername."%'"; 
					    }
					}
					if($logiccode == "OR"){
					    if($operator <> ''){
					        if ($operator == '%LIKE%') {
					            $querywhere = $querywhere." or ".$tablename.".".$structurefieldname." like '%".$datasetfiltername."%'";
					        } else {
					            $querywhere = $querywhere." or ".$tablename.".".$structurefieldname." ".$operator. " '".$datasetfiltername."'";
					        }
					    } else {
					       $querywhere = $querywhere." or ".$tablename.".".$structurefieldname." like '%".$datasetfiltername."%'";
					    }
					}
				}
				
				//For Dataset Operator (TL-128)...
				if($fieldtypename == 'Status') {
				    if($logiccode == "AND"){
					    if($operator <> ''){
					        if ($operator == '%LIKE%') {
					            $querywhere = $querywhere." and ".$tablename.".".$structurefieldname." like '%".$datasetfiltername."%'";
					        } else {
					            $querywhere = $querywhere." and ".$tablename.".".$structurefieldname." ".$operator. " ".$datasetfiltername;
					        }
					    } else {
					       $querywhere = $querywhere." and ".$tablename.".".$structurefieldname." = ".$datasetfiltername; 
					    }
					}
					if($logiccode == "OR"){
					    if($operator <> ''){
					        if ($operator == '%LIKE%') {
					            $querywhere = $querywhere." or ".$tablename.".".$structurefieldname." like '%".$datasetfiltername."%'";
					        } else {
					            $querywhere = $querywhere." or ".$tablename.".".$structurefieldname." ".$operator. " ".$datasetfiltername;
					        }
					    } else {
					       $querywhere = $querywhere." or ".$tablename.".".$structurefieldname." = ".$datasetfiltername;
					    }
					}
				}
				
				if($fieldtypename == 'Table Connect' || $fieldtypename == 'Unique TableID'){
					if($logiccode == "AND"){
						if(strpos($datasetfiltername, '$_') !== false){	
					        $datasetfiltername = substr($datasetfiltername, 11);
					        $datasetfiltername = substr($datasetfiltername, 0, -2);
					        $datasetfiltername = $_SESSION[$datasetfiltername];
				        }						
				        else {							
				            if(strpos($datasetfiltername, '$') !== false){
				                $datasetfiltername = substr($datasetfiltername, 1);	
				                $datasetfiltername = ${$datasetfiltername};
			                }  
				        }
				        
				        //For Dataset Operator (TL-128)...
				        if($operator <> ''){
    				        $querywhere = $querywhere." and ".$tablename.".".$structurefieldname." ".$operator. " '".$datasetfiltername."'";
    				    } else {
    				       $querywhere = $querywhere." and ".$tablename.".".$structurefieldname." = '".$datasetfiltername."'";
    				    }
					}
					if($logiccode == "OR"){
					    if($operator <> ''){
    				        $querywhere = $querywhere." or ".$tablename.".".$structurefieldname." ".$operator. " '".$datasetfiltername."'";
    				    } else {
    				       $querywhere = $querywhere." or ".$tablename.".".$structurefieldname." = '".$datasetfiltername."'";
    				    }
					}		
				}
			}    	
            
        }   
        
    }
    
}
$mysqli->close();

//get the datefilterfield/sortorder
$getdatefilterfield = "select dataset.datefilterfield, dataset.sortbyfield from ".$datasetrecordtable;
if($datasetrecordtable <> 'dataset'){
	$getdatefilterfield = $getdatefilterfield." inner join dataset	on ".$datasetrecordtable.".datasetid = dataset.datasetid";		
}
$getdatefilterfield = $getdatefilterfield." where ".$datasetrecordfield." = '".$datasetrecordid."'";

$mysqli = new mysqli($server, $user_name, $password, $database);if($stmt = $mysqli->prepare($getdatefilterfield)){   $stmt->execute();   $result = $stmt->get_result();   if($result->num_rows > 0){    	while($getdatefilterfieldrow = $result->fetch_assoc()){     		$datefilterfield = $getdatefilterfieldrow['datefilterfield']; 
			$querysortorder = $getdatefilterfieldrow['sortbyfield'];     	}   }}$mysqli->close();


//construct where date values
$querywheredate = "";
//Added if condition FOR TL-143...
if ($datefilterfield <> ''){
    if($startdate == ''){
    	$startdate = '0000-00-00';			
    }
    if($enddate == ''){
    	$enddate = '0000-00-00';			
    }
    if($startdate <> '0000-00-00' && $enddate <> '0000-00-00') {
    	$querywheredate = $querywheredate." and ".$datefilterfield.">='".$startdate."' and ".$datefilterfield."<='".$enddate."'";	
    }
    if($startdate <>'0000-00-00' && $enddate =='0000-00-00') {
    	$querywheredate = $querywheredate." and ".$datefilterfield.">='".$startdate."'";	
    }
    if($startdate =='0000-00-00' && $enddate <>'0000-00-00') {
    	$querywheredate = $querywheredate." and ".$datefilterfield."<='".$enddate."'";	
    }

}



//build the query
if($queryjoins <> ""){				
	$query = $query." ".$queryjoins;
}
if($querywhere == "" && $querywheredate == ""){
}
else{
	if($querywhere <> ''){
		if (preg_match('/^ AND /', $querywhere) !== false){
			$querywhere = substr($querywhere, 4);
			$query = $query." where ".$querywhere.$querywheredate;
			if($buwhere <> ""){
				$query = $query.$buwhere;
			}
		}
	}
	else {
		if (preg_match('/^ AND /', $querywheredate) !== false){
			$querywheredate = substr($querywheredate, 5);
			$query = $query." where ".$querywheredate;
			if($buwhere <> ""){
				$query = $query.$buwhere;
			}
		}
	}
}
if(isset($dashboardadditionalfilter)){
	if ($dashboardadditionalfilter <> '') {
		$dashboardadditionalfilter2 = substr($dashboardadditionalfilter, 4);
		$dashboardadditionalfilter2 = "where".$dashboardadditionalfilter2;
		$query = $query." ".$dashboardadditionalfilter2;
	}		
}
if(isset($reportadditionalfilter)){
	if ($reportadditionalfilter <> '') {
		$reportadditionalfilter2 = substr($reportadditionalfilter, 4);
		$reportadditionalfilter2 = "where".$reportadditionalfilter2;
		$query = $query." ".$reportadditionalfilter2;
	}		
}
if($querygroupby <> ""){				
	$query = $query." ".$querygroupby;
}
if($querysortorder <> ""){
	$query = $query." order by ".$querysortorder;
}



if($masterdatabase == $defaultmasterdb){
	$val = " ".$defaultmasterdb.".";
	$query = str_replace(" onebusinessmaster.", $val, $query);					
}	
			   
//echo $query."<br/><br/>";
?>