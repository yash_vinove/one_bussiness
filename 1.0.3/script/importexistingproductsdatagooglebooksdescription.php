<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include("config.php");
$date = date('Y-m-d');
$date365 = date('Y-m-d', strtotime("- 365 days"));

$googleapikey = 'AIzaSyAjEgx07h33rQ0sg8IMegwewmHhqZPhdNI';
//$googleapikey = 'AIzaSyA5x6EeHkBaY0PlKlxB0J8KXLx0H_kUCx4';

$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$getproduct = mysql_query("select zzproductdataid, product.productid, isbn from zzproductdata 
inner join product on product.productid = zzproductdata.productid
where datelastupdatedgoogle <= '$date365' and googleproductid = '' and productlongdescription = '' 
limit 20");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get product data without googleproductid: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
$zzproductdataidlist = "";
while($getproductrow = mysql_fetch_array($getproduct)){
	$zzproductdataid = $getproductrow['zzproductdataid'];	
	$productid = $getproductrow['productid'];	
	$isbn = $getproductrow['isbn'];	
	$zzproductdataidlist = $zzproductdataidlist.$zzproductdataid.",";
	echo "<br/><b>".$zzproductdataid." - ".$productid."</b>";
	
	$maxresults = 40;

	$url = "https://www.googleapis.com/books/v1/volumes?q=isbn:".$isbn."&key=".$googleapikey."&maxResults=".$maxresults."&startIndex=0";
	echo "<br/><b>".$url."</b><br/>";
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
	//comment out returntransfer line below to see the json response for debugging purposes
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
	$result = curl_exec($ch);
	curl_close($ch);
	echo $result;
	
	$result = json_decode($result, true); 
	
	$i = 0; 
	if(isset($result['error']['errors'])){
		$error = $result['error']['errors'][0]['reason'];
		echo "<br/>".$result['error']['errors'][0]['reason'];
		if($error == "dailyLimitExceeded"){
			include('recordjobstatistics.php');
			die;
		}
	}
	
	
	if(isset($result['totalItems']) && $result['totalItems'] <> 0){	
		$addbook = 1;
		$isbn1 = '';
		$isbn2 = '';
		$publisheddate = '';
		$title = '';
		$subtitle = '';
		$publisher = '';
		$author = '';
		$description = '';
		$imagelink = '';
		$pagecount = 0;
		$averagerating = 0;
		$ratingscount = 0;
		
		if(isset($result['items'][0]['id'])){
			$googleid = $result['items'][0]['id'];
		}	
		else {
			$googleid = "";	
		}
	
		if(isset($result['items'][0]['volumeInfo']['industryIdentifiers'][0]['identifier'])){
			$isbn1 = $result['items'][0]['volumeInfo']['industryIdentifiers'][0]['identifier'];
		} 
		if(isset($result['items'][0]['volumeInfo']['industryIdentifiers'][1]['identifier'])){
			$isbn2 = $result['items'][0]['volumeInfo']['industryIdentifiers'][1]['identifier'];
		} 
		if(isset($result['items'][0]['volumeInfo']['title'])){
			$title = $result['items'][0]['volumeInfo']['title'];
		}
		if(isset($result['items'][0]['volumeInfo']['subtitle'])){
			$subtitle = $result['items'][0]['volumeInfo']['subtitle'];
		}
		if(isset($result['items'][0]['volumeInfo']['publisher'])){
			$publisher = $result['items'][0]['volumeInfo']['publisher'];
		}
		if(isset($result['items'][0]['volumeInfo']['publishedDate'])){
			$publisheddate = $result['items'][0]['volumeInfo']['publishedDate'];
		}
		if(isset($result['items'][0]['volumeInfo']['authors'][0])){
			$author = $result['items'][0]['volumeInfo']['authors'][0];
		}
		if(isset($result['items'][0]['volumeInfo']['description'])){
			$description = $result['items'][0]['volumeInfo']['description'];
		}
		if(isset($result['items'][0]['volumeInfo']['imageLinks']['thumbnail'])){
			$imagelink = $result['items'][0]['volumeInfo']['imageLinks']['thumbnail'];
		}
		if(isset($result['items'][0]['volumeInfo']['pageCount'])){
			$pagecount = $result['items'][0]['volumeInfo']['pageCount'];
		}
		if(isset($result['items'][0]['volumeInfo']['averageRating'])){
			$averagerating = $result['items'][0]['volumeInfo']['averageRating'];
		}
		if(isset($result['items'][0]['volumeInfo']['ratingsCount'])){
			$ratingscount = $result['items'][0]['volumeInfo']['ratingsCount'];
		}
		if(isset($result['items'][0]['volumeInfo']['maturityRating'])){
			$maturityrating = $result['items'][0]['volumeInfo']['maturityRating'];
			if($maturityrating <> "NOT_MATURE"){
				$addbook = 0;			
			}
		}
		else {
			$maturityrating = "";					
		}
		if(isset($result['items'][0]['volumeInfo']['language'])){
			$language = $result['items'][0]['volumeInfo']['language'];
			if($language <> "en"){
				$addbook = 0;			
			}
		}
		else {
			$language = "";		
		}
		if(strlen($publisheddate) == 4){
			$publisheddate = $publisheddate."-01-01";						
		}
		if(strlen($publisheddate) == 7 || strlen($publisheddate) == 6){
			$publisheddate = $publisheddate."-01";						
		}
		if($title == ''){
			$addbook = 0;						
		}
		if($isbn1 == '' && $isbn2 == ''){
			$addbook = 0;					
		}
		$isebook = 0;
		if(isset($result['items'][0]['volumeInfo']['salesInfo']['isEbook'])){
			$isebook = $result['items'][0]['volumeInfo']['salesInfo']['isEbook'];
		}
		if($isebook == 1){
			$productstreamid = 1000002;					
		}
		else {
			$productstreamid = 1000001;		
		}
		
		echo "<br/>isbn1 - ".$isbn1;
		echo "<br/>isbn2 - ".$isbn2;
		echo "<br/>title - ".$title;
		echo "<br/>subtitle - ".$subtitle;
		echo "<br/>publisher - ".$publisher;
		echo "<br/>publisheddate - ".$publisheddate;
		echo "<br/>author - ".$author;
		echo "<br/>pagecount - ".$pagecount;
		echo "<br/>averagerating - ".$averagerating;
		echo "<br/>ratingscount - ".$ratingscount;
		echo "<br/>description - ".$description;
		echo "<br/>maturityrating - ".$maturityrating;
		echo "<br/>language - ".$language;
		echo "<br/>imagelink - ".$imagelink;
		echo "<br/>addbook - ".$addbook;
							
		$title = mysql_real_escape_string($title);
		$author = mysql_real_escape_string($author);
		$description = mysql_real_escape_string($description);
		$subtitle = mysql_real_escape_string($subtitle);
		$publisher = mysql_real_escape_string($publisher);
		$imageurl = mysql_real_escape_string($imagelink);
		
		//update product
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$updateproduct = mysql_query("update product 
		set productlongdescription = '$description', productshortdescription = '$subtitle' 
		where productid = '$productid'");	
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Update the existing product record, if something has changed: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;							
		
		//update ecommercelistinglocation	
		//$nosqlqueries = $nosqlqueries + 1;
		//$sqlstarttime = microtime(true);
		//$updateecommercelistinglocation = mysql_query("update ecommercelistinglocation 
		//inner join ecommercelisting on ecommercelisting.ecommercelistingid = ecommercelistinglocation.ecommercelistingid
		//inner join productstockitem on productstockitem.productstockitemid = ecommercelisting.productstockitemid
		//set productdatachanged = 1 where productid = '$productid'");
		//$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		//$differencemilliseconds = microtime(true) - $sqlstarttime;
		//echo "<br/>Update ecommercelistinglocation if something in the product data has changed: ";
		//echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		//echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			
		//update existing productdata row 	
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$updateproductdatarow = "update zzproductdata 
		set googleproductid = '$googleid', imagefull = '$imageurl', datelastupdatedgoogle = '$date' 
		where zzproductdataid = '$zzproductdataid'";	
		echo "<br/><br/>updateproductdatarow: ".$updateproductdatarow;
		$updateproductdatarow = mysql_query($updateproductdatarow);		
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Update the product data row, if it exists: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;					
		
		
	}	
	
	echo "<br/><br/>";
}

$zzproductdataidlist = rtrim($zzproductdataidlist, ",");
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$updateproductdatarow = mysql_query("update zzproductdata 
set datelastupdatedgoogle = '$date' 
where zzproductdataid in ($zzproductdataidlist)");			
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Update the product data row as checked: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;					

include('recordjobstatistics.php');
?>