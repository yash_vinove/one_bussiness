<?php
//echo "userid: ".$userid;
$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
if ($submit) {
	//delete existing import for this user, if there is one
	$mysqli = new mysqli($server, $user_name, $password, $database);
	$userid = mysqli_real_escape_string($mysqli, $userid);
	if ($stmt = $mysqli->prepare("delete from datauploadtemp where userid = ?")) {
		$stmt->bind_param('i', $userid);
		$stmt->execute();
	}
	$mysqli->close();

	$validation = "";

	if ($_FILES['csv']['size'] > 0) {
		//get the csv file 
		$file = $_FILES['csv']['tmp_name'];
		ini_set('auto_detect_line_endings', TRUE);
		$handle = fopen($file, "r");
		$counter56 = 1;
		for ($i = 0; $i <= 100; $i++) {
			${"content$i"} = "";
		}

		$data = array('', '', '');
		$numcols = 0;
		//loop through the csv file and insert into database
		do {
			if ($data[0] || $data[1] || $data[2]) {
				//check for no apostrophies and html tags in all fields
				if ($counter56 == 1) {
					$counter56 = $counter56 + 1;
					for ($i = 0; $i <= 100; $i++) {
						if (!isset($data[$i])) {
							$data[$i] = '';
						}
						${'content' . $i} = str_replace("'", "", $data[$i]);
						${'content' . $i} = str_replace('"', "", $data[$i]);
						//echo "<br/>content: ".${'content'.$i};
						if (${'content' . $i} == '') {
							${'content' . $i} = "";
							$numcols = $i;
							break;
						}
					}
					$numcols = $numcols - 1;
				}
				if ($counter56 >= 1) {
					for ($i = 0; $i <= $numcols; $i++) {
						${'content' . $i} = str_replace("'", "", $data[$i]);
						${'content' . $i} = str_replace('"', "", $data[$i]);
					}
				}
				//add record to database
				$querybuilder = "INSERT INTO datauploadtemp (userid, ";
				for ($i = 1; $i <= 100; $i++) {
					$querybuilder = $querybuilder . "field" . $i . ", ";
				}
				$querybuilder = rtrim($querybuilder, ', ');
				$querybuilder = $querybuilder . ") VALUES (";
				$querybuilder = $querybuilder . " '" . $userid . "',";
				for ($i = 0; $i <= 99; $i++) {
					$querybuilder = $querybuilder . " '" . addslashes(${'content' . $i}) . "', ";
				}
				$querybuilder = rtrim($querybuilder, ', ');
				$querybuilder = $querybuilder . ")";
				$query = mysql_query($querybuilder);
				//echo "<br/>querybuilder: ".$querybuilder;
			}
		} while ($data = fgetcsv($handle, 0));
	}

	//check column headers are all correct
	$getheaders = mysql_query("select * from datauploadtemp 
	where userid = $userid 
	limit 1");
	$i = 1;
	$headerarray = array();
	$header2array = array();
	$lastfieldnum = 0;
	while ($getheaderrow = mysql_fetch_array($getheaders)) {
		while ($i <= 100) {
			${"field" . $i} = $getheaderrow['field' . $i];
			//echo "<br/>field".$i." - ".${"field".$i};
			if ($i >= 2 && ${"field" . $i} <> "") {
				$lastfieldnum = $i;
				array_push($headerarray, ${"field" . $i});
				array_push($header2array, array("fieldid" => $i, "field" => ${"field" . $i}));
			}
			$i = $i + 1;
		}
	}

	//echo "<br/><br/>";
	$getfields = mysql_query("select datasetcolumnid, datasetcolumnname, sortorder, structurefieldname, structurefunctionid from datasetcolumn 
	inner join structurefield on structurefield.structurefieldid = datasetcolumn.structurefieldid
	where datasetid = 1000001
	order by sortorder asc");
	$fieldarray = array();
	$field2array = array();
	while ($getfieldrows = mysql_fetch_array($getfields)) {
		$sortorder = $getfieldrows['sortorder'];
		$datasetcolumnname = $getfieldrows['datasetcolumnname'];
		$structurefunctionid = $getfieldrows['structurefunctionid'];
		$structurefieldname = $getfieldrows['structurefieldname'];
		//echo "<br/>".$sortorder." - ".$datasetcolumnname;
		array_push($fieldarray, $datasetcolumnname);
		array_push($field2array, array(
			"datasetcolumnname" => $datasetcolumnname, "structurefunctionid" => $structurefunctionid,
			"structurefieldname" => $structurefieldname
		));
	}
	if ($field1 <> 'Type') {
		$validation = $validation . "Type is a required field. Please place this in the first column and ensure it is filled in on every line 
		with either 'new' or 'updated'. ";
	}

	//echo "<br/><br/>headerarray: ".json_encode($headerarray);
	//echo "<br/><br/>header2array: ".json_encode($header2array);
	//echo "<br/><br/>fieldarray: ".json_encode($fieldarray);
	//echo "<br/><br/>field2array: ".json_encode($field2array);

	$p = 0;
	$val = "";
	foreach ($fieldarray as $field) {
		$header = $headerarray[$p];
		if ($header <> $field) {
			$val = $val . "Field " . $header . " should be " . $field . ". ";
		}
		$p = $p + 1;
	}

	//echo "<br/><br/>val: ".$val;
	$validation = $validation . $val;

	if ($validation == "") {
		//echo "<br/><br/>Processing Import";	

		//GET PRODUCT RIGHTS OWNER DATA
		$getproductrightsowner = mysql_query("select * from productrightsowner 
		where disabled = 0");
		$productrightsownerarray = array();
		while ($getproductrightsownerrow = mysql_fetch_array($getproductrightsowner)) {
			$productrightsownerid = $getproductrightsownerrow['productrightsownerid'];
			$productrightsownername = $getproductrightsownerrow['productrightsownername'];
			array_push($productrightsownerarray, array(
				"productrightsownerid" => $productrightsownerid,
				"productrightsownername" => $productrightsownername
			));
		}
		//echo "<br/><br/>productrightsownerarray: ".json_encode($productrightsownerarray);


		//GET PRODUCT STREAM DATA
		$getproductstream = mysql_query("select * from productstream 
		where disabled = 0");
		$productstreamarray = array();
		while ($getproductstreamrow = mysql_fetch_array($getproductstream)) {
			$productstreamid = $getproductstreamrow['productstreamid'];
			$productstreamname = $getproductstreamrow['productstreamname'];
			array_push($productstreamarray, array(
				"productstreamid" => $productstreamid,
				"productstreamname" => $productstreamname
			));
		}
		//echo "<br/><br/>productstreamarray: ".json_encode($productstreamarray);


		//GET PRODUCT SUPPLIER DATA
		$getproductsupplier = mysql_query("select * from productsupplier 
		where disabled = 0");
		$productsupplierarray = array();
		while ($getproductsupplierrow = mysql_fetch_array($getproductsupplier)) {
			$productsupplierid = $getproductsupplierrow['productsupplierid'];
			$productsuppliername = $getproductsupplierrow['productsuppliername'];
			array_push($productsupplierarray, array(
				"productsupplierid" => $productsupplierid,
				"productsuppliername" => $productsuppliername
			));
		}
		//echo "<br/><br/>productsupplierarray: ".json_encode($productsupplierarray);


		//UPDATE UPDATED RECORDS
		$getupdates = mysql_query("select * from datauploadtemp 
		where field1 = 'update' or field1 = 'Update'");
		while ($getupdaterow = mysql_fetch_array($getupdates)) {
			$l = 1;
			//echo "<br/><br/><br/>";
			while ($l <= $lastfieldnum) {
				${'field' . $l} = $getupdaterow['field' . $l];
				$l = $l + 1;
				//echo " - ".${'field'.$l};
			}

			//update product record
			$updatequery = "update product set ";
			foreach ($field2array as $column) {
				$datasetcolumnname = $column['datasetcolumnname'];
				$structurefunctionid = $column['structurefunctionid'];
				$structurefieldname = $column['structurefieldname'];
				if (($structurefunctionid == 74 || $structurefunctionid == 311 || $structurefunctionid == 112 || $structurefunctionid == 73)
					&& $structurefieldname <> 'productuniqueid'
				) {
					$key = array_search($datasetcolumnname, array_column($header2array, 'field'));
					$key1 = $header2array[$key]['fieldid'];

					//deal with disabled field
					if ($structurefieldname == "productshortdescription") {
						if (${'field' . $key1} == 'Active') {
							$updatequery = $updatequery . " disabled = 0, ";
						} else {
							$updatequery = $updatequery . " disabled = 1, ";
						}
					}

					//echo "<br/><br/>field".$key1.": ".${'field'.$key1};

					//deal with productrightsowner field
					if ($structurefieldname == "productrightsownername") {
						$key = array_search(${'field' . $key1}, array_column($productrightsownerarray, 'productrightsownername'));
						$key2 = $productrightsownerarray[$key]['productrightsownerid'];
						if ($key2 >= 1) {
							$updatequery = $updatequery . " productrightsownerid = " . $key2 . ", ";
						}
					}

					//deal with productsupplier field
					if ($structurefieldname == "productsuppliername") {
						$key = array_search(${'field' . $key1}, array_column($productsupplierarray, 'productsuppliername'));
						$key2 = $productsupplierarray[$key]['productsupplierid'];
						if ($key2 >= 1) {
							$updatequery = $updatequery . " productsupplierid = " . $key2 . ", ";
						}
					}

					//deal with productstream field
					if ($structurefieldname == "productstreamname") {
						$key = array_search(${'field' . $key1}, array_column($productstreamarray, 'productstreamname'));
						$key2 = $productstreamarray[$key]['productstreamid'];
						if ($key2 >= 1) {
							$updatequery = $updatequery . " productstreamid = " . $key2 . ", ";
						}
					}

					//deal with all other fields other than disabled/productrightsowner/productsupplier
					if (
						$structurefieldname <> "productshortdescription" && $structurefieldname <> "productrightsownername"
						&& $structurefieldname <> "productsuppliername" && $structurefieldname <> "productstreamname"
					) {
						${'field' . $key1} = mysql_real_escape_string(${'field' . $key1});
						$updatequery = $updatequery . " " . $structurefieldname . ' = "' . ${'field' . $key1} . '", ';
					}

					$key1 = "";
				}
			}
			$updatequery = rtrim($updatequery, ", ");
			$updatequery = $updatequery . " where productuniqueid = '" . $field2 . "'";
			//echo "<br/><br/>updatequery: ".$updatequery;	
			$updatequery = mysql_query($updatequery);


			//update zzproductdata
			$getzzproductdata = mysql_query("select * from product where productuniqueid = '$field2'");
			$getzzproductdatarow = mysql_fetch_array($getzzproductdata);
			$productid = $getzzproductdatarow['productid'];
			//echo "<br/>productid: ".$productid;
			if ($productid <> '' && $productid <> 0) {
				$updatezzproductdata = "update zzproductdata set ";

				//zzdropshipproduct
				$getzzdropshipproduct = mysql_query("SELECT * FROM zzdropshipproduct WHERE productid = $productid");
				if(mysql_num_rows($getzzdropshipproduct) > 0)
				{
					$updatezzdropshipproduct = "UPDATE zzdropshipproduct SET ";
				}
				else {
					$insertzzdropshipproduct = "INSERT INTO zzdropshipproduct (datecreated, productid, price, url, dropshipproductid) VALUES ";
				}
				

				foreach ($field2array as $column) {
					$datasetcolumnname = $column['datasetcolumnname'];
					$structurefunctionid = $column['structurefunctionid'];
					$structurefieldname = $column['structurefieldname'];
					if ($structurefunctionid == 1000001) {
						$key = array_search($datasetcolumnname, array_column($header2array, 'field'));
						$key1 = $header2array[$key]['fieldid'];

						//deal with ismasterproduct
						if ($structurefieldname == "ismasterproduct") {
							if (strtolower(${'field' . $key1}) == 'parent') {
								$updatezzproductdata = $updatezzproductdata . " ismasterproduct = 1, ";
							} else {
								$updatezzproductdata = $updatezzproductdata . " ismasterproduct = 0, ";
							}
						}

						//deal with zzproductdataname
						if ($structurefieldname == "zzproductdataname") {
							$key9 = array_search('parent_sku', array_column($header2array, 'field'));
							$key3 = $header2array[$key9]['fieldid'];
							if ($key3 >= 0) {
								if (${'field' . $key3} <> '') {
									$getproductid = mysql_query("select * from product where productuniqueid = '${'field' .$key3}'");
									$getproductidrow = mysql_fetch_array($getproductid);
									$prodid = $getproductidrow['productid'];
									if($prodid) {
										$updatezzproductdata = $updatezzproductdata . " masterproductid = $prodid, ";
									}
									else {
										$updatezzproductdata = $updatezzproductdata . " masterproductid = 0, ";
									}
								} else {
									$updatezzproductdata = $updatezzproductdata . " masterproductid = 0, ";
								}
							}

							//add zzproductdataname
							$key10 = array_search('item_name', array_column($header2array, 'field'));
							$key3 = $header2array[$key10]['fieldid'];
							${'field' . $key3} = mysql_real_escape_string(${'field' . $key3});
							$updatezzproductdata = $updatezzproductdata . ' zzproductdataname = "' . ${'field' . $key3} . '", ';
						}

						//deal with id fields
						if ($structurefieldname == 'gtin') {
							$key12 = array_search('external_product_id_type', array_column($header2array, 'field'));
							$key5 = $header2array[$key12]['fieldid'];
							//echo "<br/><br/>key5: ".${'field'.$key5};
							$key14 = array_search('external_product_id', array_column($header2array, 'field'));
							$key7 = $header2array[$key14]['fieldid'];
							//echo "<br/><br/>key7: ".${'field'.$key7};
							if (${'field' . $key5} == "EAN") {
								$updatezzproductdata = $updatezzproductdata . ' ean = "' . ${'field' . $key7} . '", ';
							}
							if (${'field' . $key5} == "UPC") {
								$updatezzproductdata = $updatezzproductdata . ' upc = "' . ${'field' . $key7} . '", ';
							}
							if (${'field' . $key5} == "GTIN") {
								$updatezzproductdata = $updatezzproductdata . ' gtin = "' . ${'field' . $key7} . '", ';
							}
							if (${'field' . $key5} == "ASIN") {
								$updatezzproductdata = $updatezzproductdata . ' asin1 = "' . ${'field' . $key7} . '", ';
							}
							if (${'field' . $key5} == "GCID") {
								$updatezzproductdata = $updatezzproductdata . ' gcid = "' . ${'field' . $key7} . '", ';
							}
							if (${'field' . $key5} == "ISBN") {
								$updatezzproductdata = $updatezzproductdata . ' isbn = "' . ${'field' . $key7} . '", ';
							}
						}

						if ($structurefieldname == 'asin1') {
							//do nothing, dealt with in gtin above
						}

						//deal with all other fields
						if (
							$structurefieldname <> "ismasterproduct" && $structurefieldname <> "zzproductdataname" &&
							$structurefieldname <> "gtin" && $structurefieldname <> "asin1"
						) {
							${'field' . $key1} = str_replace("\r", "", ${'field' . $key1});
							${'field' . $key1} = mysql_real_escape_string(${'field' . $key1});
							$updatezzproductdata = $updatezzproductdata . " " . $structurefieldname . ' = "' . ${'field' . $key1} . '", ';
						}

						//deal with zzdropshipproduct table fields
						$dropshipPriceColumn = array_search('dropship_price', array_column($header2array, 'field'));
						$dropshipPriceId = $header2array[$dropshipPriceColumn]['fieldid'];
						$dropshipPriceValue = ${'field' . $dropshipPriceId};

						if(mysql_num_rows($getzzdropshipproduct) > 0) {
							$updatezzdropshipproduct = $updatezzdropshipproduct." price = '$dropshipPriceValue',";
						}

						$dropshipItemNumberColumn = array_search('dropship_item_number', array_column($header2array, 'field'));
						$dropshipItemNumberId = $header2array[$dropshipItemNumberColumn]['fieldid'];
						$dropshipItemNumberValue = ${'field' . $dropshipItemNumberId};

						if(mysql_num_rows($getzzdropshipproduct) > 0) {
							$updatezzdropshipproduct = $updatezzdropshipproduct." dropshipproductid = '$dropshipItemNumberValue',";
						}

						$dropshipLinkColumn = array_search('dropship_link', array_column($header2array, 'field'));
						$dropshipLinkId = $header2array[$dropshipLinkColumn]['fieldid'];
						$dropshipLinkValue = ${'field' . $dropshipLinkId};

						if(mysql_num_rows($getzzdropshipproduct) > 0) {
							$updatezzdropshipproduct = $updatezzdropshipproduct." url = '$dropshipLinkValue'";
						}
					}
				}

				$updatezzproductdata = rtrim($updatezzproductdata, ", ");
				$updatezzproductdata = $updatezzproductdata . " where productid = $productid";
				//echo "<br/><br/>updatezzproductdata: ".$updatezzproductdata;	
				$updatezzproductdata = mysql_query($updatezzproductdata);
				
				if(mysql_num_rows($getzzdropshipproduct) > 0) {
					$updatezzdropshipproduct = $updatezzdropshipproduct." WHERE productid = $productid";
					$updatezzdropshipproduct = mysql_query($updatezzdropshipproduct);
				}
				else {
					$datecreated = date('Y-m-d');
					$insertzzdropshipproduct = $insertzzdropshipproduct." ('$datecreated', '$productid', '$dropshipPriceValue', '$dropshipLinkValue', '$dropshipItemNumberValue')";
					$insertzzdropshipproduct = mysql_query($insertzzdropshipproduct);
				}
				
				// NEW CODE FOR IMAGE -- start
				/*
				$database2 = str_replace("andrewnorth_", "", $database);

				//check if directory exists for this structurefunction
				$structurefunctionname = "product";
				$filedir = '../documents/' . $database2 . "/sf/" . $structurefunctionname . "/";

				if (file_exists($filedir)) {
					//get current max directory
					$files = scandir($filedir);
					$results = array();
					foreach ($files as $key => $value) {
						$path = realpath($filedir . DIRECTORY_SEPARATOR . $value);
						if (is_dir($path)) {
							if (strpos($path, 'files') !== false) {
								array_push($results, $path);
							}
						}
					}
					//echo json_encode($results);
					if (count($results) == 0) {
						$file_directory = "../documents/" . $database2 . "/sf/" . $structurefunctionname . "/files1/";
						$folderno = 1;
						if (!file_exists($file_directory)) {
							mkdir($file_directory);
						}
					}
					if (!isset($file_directory)) {
						$numbers = array();
						foreach ($results as $folder) {
							$folderno = substr($folder, strpos($folder, "files") + 5);
							echo "<br/>folderno: " . $folderno;
							array_push($numbers, $folderno);
						}
						$maxfolder = max($numbers);
						$folderno = $maxfolder;
						echo "<br/>maxfolder: " . $maxfolder;

						//check size of folder
						$fd = "../documents/" . $database2 . "/sf/" . $structurefunctionname . "/files" . $maxfolder . "/";
						$output = exec('du -sk ' . $fd);
						$repositorysize = trim(str_replace($fd, '', $output));
						$numberfiles = scandir($fd);
						$numberfiles = count($numberfiles) - 1;
						echo "<br/>repositorysize - " . $repositorysize . "<br/>";
						echo "<br/>numberfiles - " . $numberfiles . "<br/>";
						if ($repositorysize >= 300000000 || $numberfiles >= 5000) {
							$folderno = $maxfolder + 1;
							$file_directory = "../documents/" . $database2 . "/sf/" . $structurefunctionname . "/files" . $folderno . "/";
							if (!file_exists($file_directory)) {
								mkdir($file_directory);
							}
						} else {
							$file_directory = $fd;
						}
					}
				} else {
					$file_directory = "../documents/" . $database2 . "/sf/" . $structurefunctionname . "/";
					mkdir($file_directory);
				}
				//echo "<br/>file_directory: ".$file_directory;

				$imageColumn = array_search('main_image_url', array_column($header2array, 'field'));
				$imageUrlId = $header2array[$imageColumn]['fieldid'];
				$imageUrl = ${'field' . $imageUrlId};

				// Need for downloading
				$imageUrl = str_replace("dl=0", "dl=1", $imageUrl);

				//var_dump($imageUrl);
				$newfilename = $productid . " - Primary Image.jpg";
				//echo "<br>".$newfilename."<br>";
				$urlstart = $database2 . "/sf/product/files" . $folderno . "/" . $newfilename;

				$url2 = "../documents/" . $urlstart;
				$success = file_put_contents($url2, file_get_contents($imageUrl));

				print $success ? $url2 : 'Unable to save the file.';

				$getoldrecord = mysql_query("select count(structurefunctiondocumentid) from structurefunctiondocument
				where structurefunctionid = 74 and rowid = $productid and doctypename = 'Primary Image'");
				while ($getrow = mysql_fetch_array($getoldrecord)) {
					//printf("<br/>".$getrow[0]."<br/>".$getrow[1]."<br/>");
					if (!($getrow[0] >= 1)) {
						$insertrecord = mysql_query("insert into structurefunctiondocument (structurefunctiondocumentname, datecreated,
						documenturl, doctypename, structurefunctionid, rowid) values ('$newfilename', '$date', '$urlstart', 'Primary Image', '74', '$productid')");
					}
				}
				*/
				// NEW CODE FOR IMAGE -- end

				// JIRA PACK-45 Update start
				$getrecords2 = mysql_query("select * from  ecommercelistinglocation
				inner join zzproductdata on zzproductdata.productid = ecommercelistinglocation.productid
				where zzproductdata.productid = '$productid' and ecommercelistinglocationstatusid in (2,3,7,8,9,10,11,12,14,15,16,17)");
				while ($row56 = mysql_fetch_array($getrecords2)) {
					$ecommercelistinglocationid = $row56['ecommercelistinglocationid'];
					$update2 = mysql_query("update ecommercelistinglocation set ecommercelistinglocationstatusid = 1, 
					amazonproductxml = '', integrationstatusname = ''
					where ecommercelistinglocationid = '$ecommercelistinglocationid'");
				}

				// echo "<br/>Product ID : ".$productid;
				// echo "<br/>EcommerceListingLocationID : ".$ecommercelistinglocationid;
				// JIRA PACK-45 Update end

				// Update zzdropshipproduct

			}
		}



		//ADD NEW RECORDS
		$getnew = mysql_query("select * from datauploadtemp 
		where field1 = 'new' or field1 = 'New'");
		while ($getnewrow = mysql_fetch_array($getnew)) {
			$l = 1;
			//echo "<br/><br/><br/>";
			while ($l <= $lastfieldnum) {
				${'field' . $l} = $getnewrow['field' . $l];
				$l = $l + 1;
				//echo " - ".${'field'.$l};
			}

			//add product record
			$addproduct = "insert into product (productname, disabled, productstreamid, productlongdescription, productsupplierid, productuniqueid, 
			retailprice, currencyid, productrightsownerid, productalternativename1, productalternativename2, productalternativename3, productvariantdata) values (";

			$key10 = array_search('item_name', array_column($header2array, 'field'));
			$key11 = $header2array[$key10]['fieldid'];
			$productname = mysql_real_escape_string(${'field' . $key11});
			$addproduct = $addproduct . '"' . $productname . '", ';

			$key10 = array_search('Status', array_column($header2array, 'field'));
			$key11 = $header2array[$key10]['fieldid'];
			$disabled = ${'field' . $key11};
			if ($disabled == 'Active') {
				$addproduct = $addproduct . '0, ';
			} else {
				$addproduct = $addproduct . '1, ';
			}

			$key10 = array_search('Product Stream', array_column($header2array, 'field'));
			$key11 = $header2array[$key10]['fieldid'];
			$productstreamname = ${'field' . $key11};
			$key = array_search(${'field' . $key11}, array_column($productstreamarray, 'productstreamname'));
			$key2 = $productstreamarray[$key]['productstreamid'];
			if ($key2 >= 1) {
				$addproduct = $addproduct . $key2 . ', ';
			}

			$key10 = array_search('product_description', array_column($header2array, 'field'));
			$key11 = $header2array[$key10]['fieldid'];
			$productlongdescription = mysql_real_escape_string(${'field' . $key11});
			$addproduct = $addproduct . '"' . $productlongdescription . '", ';

			$key10 = array_search('Product Supplier', array_column($header2array, 'field'));
			$key11 = $header2array[$key10]['fieldid'];
			$productsuppliername = ${'field' . $key11};
			$key = array_search(${'field' . $key11}, array_column($productsupplierarray, 'productsuppliername'));
			$key2 = $productsupplierarray[$key]['productsupplierid'];
			if ($key2 >= 1) {
				$addproduct = $addproduct . $key2 . ', ';
			}

			$key10 = array_search('item_sku', array_column($header2array, 'field'));
			$key11 = $header2array[$key10]['fieldid'];
			$itemsku = mysql_real_escape_string(${'field' . $key11});
			$addproduct = $addproduct . '"' . $itemsku . '", ';

			$key10 = array_search('standard_price', array_column($header2array, 'field'));
			$key11 = $header2array[$key10]['fieldid'];
			$retailprice = ${'field' . $key11};
			$addproduct = $addproduct . $retailprice . ', ';
			$addproduct = $addproduct . '1000001, ';

			$key10 = array_search('brand_name', array_column($header2array, 'field'));
			$key11 = $header2array[$key10]['fieldid'];
			$productrightsownername = ${'field' . $key11};
			$key = array_search(${'field' . $key11}, array_column($productrightsownerarray, 'productrightsownername'));
			$key2 = $productrightsownerarray[$key]['productrightsownerid'];
			if ($key2 >= 1) {
				$addproduct = $addproduct . $key2 . ', ';
			}

			$key10 = array_search('product_alernative_name_1', array_column($header2array, 'field'));
			$key11 = $header2array[$key10]['fieldid'];
			$product_alernative_name_1 = mysql_real_escape_string(${'field' . $key11});
			$addproduct = $addproduct . '"' . $product_alernative_name_1 . '", ';

			$key10 = array_search('product_alternative_name_2', array_column($header2array, 'field'));
			$key11 = $header2array[$key10]['fieldid'];
			$product_alternative_name_2 = mysql_real_escape_string(${'field' . $key11});
			$addproduct = $addproduct . '"' . $product_alternative_name_2 . '", ';

			$key10 = array_search('product_alternative_name_3', array_column($header2array, 'field'));
			$key11 = $header2array[$key10]['fieldid'];
			$product_alternative_name_3 = mysql_real_escape_string(${'field' . $key11});
			$addproduct = $addproduct . '"' . $product_alternative_name_3 . '", ';

			$key10 = array_search('product_variant_data', array_column($header2array, 'field'));
			$key11 = $header2array[$key10]['fieldid'];
			$product_variant_data = mysql_real_escape_string(${'field' . $key11});
			$addproduct = $addproduct . '"' . $product_variant_data . '", ';

			$addproduct = rtrim($addproduct, ", ");
			$addproduct = $addproduct . ")";
			//echo "<br/><br/>addproduct: ".$addproduct;	
			$addproduct = mysql_query($addproduct);


			//get added productid 
			$productid = mysql_insert_id();
			//$productid = 1;

			if ($productid >= 1) {
				//add zzproductdata row 
				$addzzproductdatafields = "insert into zzproductdata (zzproductdataname, disabled, productid, ismasterproduct, masterproductid, ";
				$addzzproductdata = ") values (";

				$insertzzdropshipproduct = "INSERT INTO zzdropshipproduct (datecreated, productid, price, url, dropshipproductid) VALUES ";

				$key10 = array_search('item_name', array_column($header2array, 'field'));
				$key11 = $header2array[$key10]['fieldid'];
				$productname = mysql_real_escape_string(${'field' . $key11});
				$addzzproductdata = $addzzproductdata . '"' . $productname . '", ';

				$key10 = array_search('Status', array_column($header2array, 'field'));
				$key11 = $header2array[$key10]['fieldid'];
				$disabled = ${'field' . $key11};
				if ($disabled == 'Active') {
					$addzzproductdata = $addzzproductdata . '0, ';
				} else {
					$addzzproductdata = $addzzproductdata . '1, ';
				}

				$addzzproductdata = $addzzproductdata . $productid . ", ";

				$key10 = array_search('parent_child', array_column($header2array, 'field'));
				$key11 = $header2array[$key10]['fieldid'];
				$master = strtolower(${'field' . $key11});
				if ($master == 'parent') {
					$master = 1;
				} else {
					$master = 0;
				}
				$addzzproductdata = $addzzproductdata . $master . ', ';

				$key10 = array_search('parent_sku', array_column($header2array, 'field'));
				$key11 = $header2array[$key10]['fieldid'];
				$mastersku = ${'field' . $key11};
				if ($mastersku <> '') {
					$getmasterproduct = mysql_query("select * from product where productuniqueid = '$mastersku'");
					$getmasterproductrow = mysql_fetch_array($getmasterproduct);
					$masterproductid = $getmasterproductrow['productid'];
					if($masterproductid)
					{
						$addzzproductdata = $addzzproductdata . $masterproductid . ", ";
					}
					else {
						$addzzproductdata = $addzzproductdata . " '', ";
					}
					
				} else {
					$addzzproductdata = $addzzproductdata . "0, ";
				}

				foreach ($field2array as $column) {
					$datasetcolumnname = $column['datasetcolumnname'];
					$structurefunctionid = $column['structurefunctionid'];
					$structurefieldname = $column['structurefieldname'];
					if ($structurefunctionid == 1000001) {
						$key = array_search($datasetcolumnname, array_column($header2array, 'field'));
						$key1 = $header2array[$key]['fieldid'];
						$foundfield = str_replace("\r", "", ${'field' . $key1});
						$foundfield = mysql_real_escape_string($foundfield);

						//deal with id fields
						if ($structurefieldname == 'gtin') {
							$key12 = array_search('external_product_id_type', array_column($header2array, 'field'));
							$key5 = $header2array[$key12]['fieldid'];
							//echo "<br/><br/>key5: ".${'field'.$key5};
							$key14 = array_search('external_product_id', array_column($header2array, 'field'));
							$key7 = $header2array[$key14]['fieldid'];
							//echo "<br/><br/>key7: ".${'field'.$key7};
							if (${'field' . $key5} == "EAN") {
								$addzzproductdatafields = $addzzproductdatafields . ' ean, ';
								$addzzproductdata = $addzzproductdata . '"' . ${'field' . $key7} . '", ';
							}
							if (${'field' . $key5} == "UPC") {
								$addzzproductdatafields = $addzzproductdatafields . ' upc, ';
								$addzzproductdata = $addzzproductdata . '"' . ${'field' . $key7} . '", ';
							}
							if (${'field' . $key5} == "GTIN") {
								$addzzproductdatafields = $addzzproductdatafields . ' gtin, ';
								$addzzproductdata = $addzzproductdata . '"' . ${'field' . $key7} . '", ';
							}
							if (${'field' . $key5} == "ASIN") {
								$addzzproductdatafields = $addzzproductdatafields . ' asin1, ';
								$addzzproductdata = $addzzproductdata . '"' . ${'field' . $key7} . '", ';
							}
							if (${'field' . $key5} == "GCID") {
								$addzzproductdatafields = $addzzproductdatafields . ' gcid, ';
								$addzzproductdata = $addzzproductdata . '"' . ${'field' . $key7} . '", ';
							}
							if (${'field' . $key5} == "ISBN") {
								$addzzproductdatafields = $addzzproductdatafields . ' isbn, ';
								$addzzproductdata = $addzzproductdata . '"' . ${'field' . $key7} . '", ';
							}
						}

						//deal with all other fields
						if (
							$structurefieldname <> 'ismasterproduct' && $structurefieldname <> 'zzproductdataname' &&
							$structurefieldname <> "gtin" && $structurefieldname <> "asin1"
						) {
							$addzzproductdatafields = $addzzproductdatafields . $structurefieldname . ", ";

							$addzzproductdata = $addzzproductdata . '"' . $foundfield . '", ';
						}

						//deal with zzdropshipproduct table fields
						$dropshipPriceColumn = array_search('dropship_price', array_column($header2array, 'field'));
						$dropshipPriceId = $header2array[$dropshipPriceColumn]['fieldid'];
						$dropshipPriceValue = ${'field' . $dropshipPriceId};

						$dropshipItemNumberColumn = array_search('dropship_item_number', array_column($header2array, 'field'));
						$dropshipItemNumberId = $header2array[$dropshipItemNumberColumn]['fieldid'];
						$dropshipItemNumberValue = ${'field' . $dropshipItemNumberId};

						$dropshipLinkColumn = array_search('dropship_link', array_column($header2array, 'field'));
						$dropshipLinkId = $header2array[$dropshipLinkColumn]['fieldid'];
						$dropshipLinkValue = ${'field' . $dropshipLinkId};

					}
				}

				$addzzproductdatafields = rtrim($addzzproductdatafields, ", ");
				$addzzproductdata = rtrim($addzzproductdata, ", ");
				$addzzproductdata = $addzzproductdatafields . $addzzproductdata . ")";
				//echo "<br/><br/>addzzproductdata: ".$addzzproductdata;
				$addzzproductdata = mysql_query($addzzproductdata);

				// zzdropshipproduct
				$datecreated = date('Y-m-d');
				$insertzzdropshipproduct = $insertzzdropshipproduct." ('$datecreated', '$productid', '$dropshipPriceValue', '$dropshipLinkValue', '$dropshipItemNumberValue')";
				$insertzzdropshipproduct = mysql_query($insertzzdropshipproduct);

				// NEW CODE FOR IMAGE -- start
/*
				$database2 = str_replace("andrewnorth_", "", $database);

				//check if directory exists for this structurefunction
				$structurefunctionname = "product";
				$filedir = '../documents/' . $database2 . "/sf/" . $structurefunctionname . "/";

				if (file_exists($filedir)) {
					//get current max directory
					$files = scandir($filedir);
					$results = array();
					foreach ($files as $key => $value) {
						$path = realpath($filedir . DIRECTORY_SEPARATOR . $value);
						if (is_dir($path)) {
							if (strpos($path, 'files') !== false) {
								array_push($results, $path);
							}
						}
					}
					//echo json_encode($results);
					if (count($results) == 0) {
						$file_directory = "../documents/" . $database2 . "/sf/" . $structurefunctionname . "/files1/";
						$folderno = 1;
						if (!file_exists($file_directory)) {
							mkdir($file_directory);
						}
					}
					if (!isset($file_directory)) {
						$numbers = array();
						foreach ($results as $folder) {
							$folderno = substr($folder, strpos($folder, "files") + 5);
							echo "<br/>folderno: " . $folderno;
							array_push($numbers, $folderno);
						}
						$maxfolder = max($numbers);
						$folderno = $maxfolder;
						echo "<br/>maxfolder: " . $maxfolder;

						//check size of folder
						$fd = "../documents/" . $database2 . "/sf/" . $structurefunctionname . "/files" . $maxfolder . "/";
						$output = exec('du -sk ' . $fd);
						$repositorysize = trim(str_replace($fd, '', $output));
						$numberfiles = scandir($fd);
						$numberfiles = count($numberfiles) - 1;
						echo "<br/>repositorysize - " . $repositorysize . "<br/>";
						echo "<br/>numberfiles - " . $numberfiles . "<br/>";
						if ($repositorysize >= 300000000 || $numberfiles >= 5000) {
							$folderno = $maxfolder + 1;
							$file_directory = "../documents/" . $database2 . "/sf/" . $structurefunctionname . "/files" . $folderno . "/";
							if (!file_exists($file_directory)) {
								mkdir($file_directory);
							}
						} else {
							$file_directory = $fd;
						}
					}
				} else {
					$file_directory = "../documents/" . $database2 . "/sf/" . $structurefunctionname . "/";
					mkdir($file_directory);
				}
				//echo "<br/>file_directory: ".$file_directory;

				$imageColumn = array_search('main_image_url', array_column($header2array, 'field'));
				$imageUrlId = $header2array[$imageColumn]['fieldid'];
				$imageUrl = ${'field' . $imageUrlId};

				// Need for downloading
				$imageUrl = str_replace("dl=0", "dl=1", $imageUrl);

				//var_dump($imageUrl);
				$newfilename = $productid . " - Primary Image.jpg";
				//echo "<br>".$newfilename."<br>";
				$urlstart = $database2 . "/sf/product/files" . $folderno . "/" . $newfilename;

				$url2 = "../documents/" . $urlstart;
				$success = file_put_contents($url2, file_get_contents($imageUrl));

				print $success ? $url2 : 'Unable to save the file.';

				$insertrecord = mysql_query("insert into structurefunctiondocument (structurefunctiondocumentname, datecreated,
				documenturl, doctypename, structurefunctionid, rowid) values ('$newfilename', '$date', '$urlstart', 'Primary Image', '74', '$productid')");
				//$zzproductdataids2 = $zzproductdataids2 . $zzproductdataid;
				//var_dump($insertrecord);
				*/
				// NEW CODE FOR IMAGE -- end

				// JIRA PACK-45 Update start
				$getrecords2 = mysql_query("select * from  ecommercelistinglocation
				inner join zzproductdata on zzproductdata.productid = ecommercelistinglocation.productid
				where zzproductdata.productid = '$productid' and ecommercelistinglocationstatusid in (2,3,7,8,9,10,11,12,14,15,16,17)");
				while ($row56 = mysql_fetch_array($getrecords2)) {
					$ecommercelistinglocationid = $row56['ecommercelistinglocationid'];
					$update2 = mysql_query("update ecommercelistinglocation set ecommercelistinglocationstatusid = 1, 
					amazonproductxml = '', integrationstatusname = ''
					where ecommercelistinglocationid = '$ecommercelistinglocationid'");
				}

				// echo "<br/>Product ID : ".$productid;
				// echo "<br/>EcommerceListingLocationID : ".$ecommercelistinglocationid;
				// JIRA PACK-45 Update end
			}
		}

		$success = "Import completed";
	}
}


if (isset($validation)) {
	if ($validation <> "") {
		echo "<br/><p class='background-warning'>" . $validation . "</p>";
	}
}
if (isset($success)) {
	if ($success <> "") {
		echo "<br/><p class='background-success'>" . $success . "</p>";
	}
}
?>

<form action="" method="post" enctype="multipart/form-data" name="form1" id="form1">
	<b>Upload File</b><br /><br />
	<input name="csv" type="file" id="csv" />
	<br />
	<input class="button-primary" type="submit" name="submit" value="Submit" />
</form>