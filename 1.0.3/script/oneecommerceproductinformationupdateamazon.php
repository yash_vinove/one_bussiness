<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$date = date('Y-m-d');
$hour = date('H');
$minute = date('i');
echo "<br/><br/>hour: ".$hour;
echo "<br/>minute: ".$minute;
include('../1.0.3/phpAmazonMWS/includes/classes.php');

//update price
echo "<br/><br/><b>CHECK IF ANY RECORDS NEED PRICE UPDATING - PRICE CHECK</b>";
//check if there are any files already processing
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$checkexistingfiles = mysql_query("select ecommerceprocessamazonfilename, ecommercesiteconfig.ecommercesiteconfigid, 
apikey1, apikey2, apikey3, apikey4, ecommerceprocessamazonfileid, ecommercelistinglocationidlist
 from $database.ecommerceprocessamazonfile
 inner join $database.ecommercesiteconfig on ecommercesiteconfig.ecommercesiteconfigid = ecommerceprocessamazonfile.ecommercesiteconfigid
where status = 'Update Pricing Changes' and filetype = '_POST_PRODUCT_PRICING_DATA_ - UPDATE'");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get list of amazon config records: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
if(mysql_num_rows($checkexistingfiles) >= 1){
	//finish processing existing file
	$response = "";
	while($existingfile = mysql_fetch_array($checkexistingfiles)){
		$ecommerceprocessamazonfileid = $existingfile['ecommerceprocessamazonfileid'];
		$ecommercesiteconfigid = $existingfile['ecommercesiteconfigid'];
		$feedsubid = $existingfile['ecommerceprocessamazonfilename'];
		$ecommercelistinglocationidlist = $existingfile['ecommercelistinglocationidlist'];
		
		if($feedsubid == "" || $feedsubid == 0){
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updatefile = mysql_query("update ecommerceprocessamazonfile 
			set status = 'Failed' where ecommerceprocessamazonfileid = '$ecommerceprocessamazonfileid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Update ecommerceprocessamazonfile as failed: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;	
			break;	
		}
		$apikey1 = $existingfile['apikey1'];
		$apikey2 = $existingfile['apikey2'];
		$apikey3 = $existingfile['apikey3'];
		$apikey4 = $existingfile['apikey4'];
		
		$errorupdate = "update ecommercelistinglocation 
		set integrationerrorretrynumber = 1, ecommercelistinglocationstatusid = 12, integrationerrormessage = case integrationproductid ";
		$errorupdatewhere = "";
		
		echo "<br/><br/>Get the status of the feed: ".$ecommercesiteconfigid." - ".$feedsubid;
		getFeedResult($feedsubid);	
		if (strpos(json_encode($response), 'Feed Submission Result is not ready for Feed ') !== false) {
			//list not uploaded yet
    		echo '<br/><br/>No Results yet';
		}
		else {
			//process the result
			$body = $response['body'];
			$xml = new SimpleXMLElement($body);
			$items =  $xml->xpath('/AmazonEnvelope/Message/ProcessingReport/Result');
			//echo json_encode($items);
			echo "<br/><br/>";
			foreach($items as $result){
				$sku = $result->AdditionalInfo->SKU;
				$resultdescription = $result->ResultDescription;
				$result = $result->ResultCode;
				echo "<br/>".$sku." - ".$result." - ".$resultdescription;		
				if($result == "Error" && $sku <> ""){
					$resultdescription = str_replace('"', " ", $resultdescription);
					$resultdescription = str_replace("'", " ", $resultdescription);
					$errorupdate = $errorupdate.' when "'.$sku.'" then "'.$resultdescription.'" ';
					$errorupdatewhere = $errorupdatewhere.'"'.$sku.'",';
				}
			}
			
			if($errorupdatewhere <> ""){
				$errorupdatewhere = rtrim($errorupdatewhere, ",");
				$errorupdate = $errorupdate."else '' end where integrationproductid in (".$errorupdatewhere.") 
				and productdatachanged = 1";
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				echo "<br/><br/>".$errorupdate;
				$errorupdate = mysql_query($errorupdate);
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Update all the errors into ecommercelistinglocation: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			}
			
			//mark the ecommerceprocessamazonfile processing as complete
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updatefile = mysql_query("update ecommerceprocessamazonfile 
			set status = 'Processed' where ecommerceprocessamazonfileid = '$ecommerceprocessamazonfileid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Update ecommerceprocessamazonfile as completed: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			
			//mark all the non-erroring ecommercelistinglocation records as complete - listed
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updateecommercelistinglocation = mysql_query("update ecommercelistinglocation 
			set productdatachanged = 0, integrationstatusname = 'Listed'
			where integrationproductid <> '' and integrationerrormessage = '' and ecommercesiteconfigid = '$ecommercesiteconfigid'
			and ecommercelistinglocationid in ($ecommercelistinglocationidlist)");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Update all successful records as listed: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;		
		}	
	}
}


echo "<br/><br/><b>PROCESS UPDATING PRICE INFORMATION</b>";
//update price information for products that have been added to Amazon
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$getsiteconfig = mysql_query("select * from $database.ecommercesiteconfig
inner join currency on currency.currencyid = ecommercesiteconfig.currencyid
where ecommercesiteid = 1
order by rand()
limit 1");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get list of amazon config records: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
while($siteconfig = mysql_fetch_array($getsiteconfig)){
	$ecommercesiteconfigid = $siteconfig['ecommercesiteconfigid'];
	$apikey1 = $siteconfig['apikey1'];
	$apikey2 = $siteconfig['apikey2'];
	$apikey3 = $siteconfig['apikey3'];
	$apikey4 = $siteconfig['apikey4'];
	$currencycode = $siteconfig['currencycode'];

		//setup batching		
		/*
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$setupbatching = mysql_query("select min(ecommercelistinglocationid) as 'minval', max(ecommercelistinglocationid) as 'maxval' 
		from ecommercelistinglocation");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Get batching stats: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		while($row10 = mysql_fetch_array($setupbatching)){
			$minid = $row10['minval'];
			$maxid = $row10['maxval'];
		}	
		$minute = date('i');
		$randno = 1;
		if($minute <= 20){
			$randno = 2;		
		}
		if($minute >= 40){
			$randno = 3;		
		}
		$total = $maxid - $minid;
		$range = $total / 3;
		if($randno == 1){
			$startid = $minid;
			$stopid = $minid + $range;
		}
		if($randno == 2){
			$startid = $minid + $range;
			$stopid = $maxid - $range;
		}
		if($randno == 3){
			$startid = $maxid - $range;
			$stopid = $maxid;
		}
		$startid = round($startid);
		$stopid = round($stopid);
		echo "<br/><br/>minid: ".$minid;
		echo "<br/>maxid: ".$maxid;
		echo "<br/>total: ".$total;
		echo "<br/>range: ".$range;
		echo "<br/>randno: ".$randno;
		echo "<br/>startid: ".$startid;
		echo "<br/>stopid: ".$stopid;
		*/
		
		//create products that have listing locations
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		//$getlistinglocations = mysql_query("select ecommercelistinglocationid, integrationproductid, listingprice
		//from ecommercelistinglocation
		//where ecommercelistinglocationid >= $startid and ecommercelistinglocationid <= $stopid
		//and productdatachanged = 1 and integrationproductid <> '' and ecommercelistinglocationstatusid in (2,12) 
		//and ecommercelistinglocation.ecommercesiteconfigid = '$ecommercesiteconfigid'
		//limit 5000");
		$getlistinglocations = mysql_query("select ecommercelistinglocationid, integrationproductid, listingprice
		from ecommercelistinglocation
		where productdatachanged = 1 and integrationproductid <> '' and ecommercelistinglocationstatusid in (2) 
		and integrationstatusname = 'Listed'
		and ecommercelistinglocation.ecommercesiteconfigid = '$ecommercesiteconfigid'
		limit 10000");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Get list of ecommercelistinglocation records that require pricing submitted: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		if(mysql_num_rows($getlistinglocations) >= 1){
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$createamazonfile = mysql_query("insert into ecommerceprocessamazonfile (status, filetype, ecommercesiteconfigid,
			disabled, datecreated, masteronly) values ('Update Pricing Changes', 
			'_POST_PRODUCT_PRICING_DATA_ - UPDATE', '$ecommercesiteconfigid', '0', '$date', '0')");
			$amazonfileid = mysql_insert_id();
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Insert a record into ecommerceprocessamazonfile: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			
			$feedprice = '<?xml version="1.0" encoding="iso-8859-1"?>
			<AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			    xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
			  <Header>
			    <DocumentVersion>1.01</DocumentVersion>
			    <MerchantIdentifier>'.$amazonfileid.'</MerchantIdentifier>
			  </Header>
			  <MessageType>Product</MessageType>';
			$i = 1;  
			
			
			$ecommercelistinglocationids = "";
			while($row92=mysql_fetch_array($getlistinglocations)){
				$ecommercelistinglocationid = $row92['ecommercelistinglocationid'];
				$integrationproductid = $row92['integrationproductid'];			
				$listingprice = $row92['listingprice'];			
				$feedprice = $feedprice.'
				<Message>
				<MessageID>'.$i.'</MessageID>
				<Price>
					<SKU>'.$integrationproductid.'</SKU>
					<StandardPrice currency="'.$currencycode.'">'.$listingprice.'</StandardPrice>
				</Price>
			</Message>';
			  	
		  		$ecommercelistinglocationids = $ecommercelistinglocationids.$ecommercelistinglocationid.',';
			  	$i = $i + 1;
			}
			$ecommercelistinglocationids = rtrim($ecommercelistinglocationids, ",");
			$feedprice = $feedprice.'</AmazonEnvelope>';
			$kb = strlen($feedprice) / 1024;
			echo "<br/><br/>kb size: ".$kb;
			//echo "<br/><br/>".nl2br(htmlentities($feedprice));
			echo "<br/><br/>Submit the pricing feed: ";
			sendPricingFeed($feedprice);
			echo "<br/>final feed sub id: ".$feedsubid;
			
			$feedprice = mysql_real_escape_string($feedprice);
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updateamazonfileid = mysql_query("update ecommerceprocessamazonfile set ecommerceprocessamazonfilename = '$feedsubid',
			ingoingxml = '$feedprice', ecommercelistinglocationidlist = '$ecommercelistinglocationids'
			where ecommerceprocessamazonfileid = '$amazonfileid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Insert the amazon id of the file being processed: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			
			$updateecommercelistinglocation = "update ecommercelistinglocation set integrationstatusname = 'Updating Price Data' 
			where ecommercelistinglocationid in (".$ecommercelistinglocationids.")";
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			echo "<br/><br/>".$updateecommercelistinglocation;	
			$updateamazonfileid = mysql_query($updateecommercelistinglocation);
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Insert the amazon id of the file being processed: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			
		
	}
	
	//update any records that failed recently, back to Listed so they can try again
	if($hour == 23 && $minute <= 4){
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$updatependinglisting = mysql_query("update ecommercelistinglocation 
		set integrationstatusname = 'Listed', productdatachanged = 1
		where ecommercelistinglocationstatusid in (2) and integrationstatusname = 'Updating Price Data' and 
		ecommercesiteconfigid = '$ecommercesiteconfigid'");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Update any records that failed recently, back to Listed so they can try updating price again: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;		
	}
}		


function getFeedResult($feedId) {
    try {
        $amz=new AmazonFeedResult("Default", $feedId); //feed ID can be quickly set by passing it to the constructor
        $amz->setFeedId($feedId); //otherwise, it must be set this way
        $amz->fetchFeedResult();
        return $amz->getRawFeed();
    } catch (Exception $ex) {
        echo '<br/>There was a problem with the Amazon library. Error: '.$ex->getMessage();
    }
}

function sendPricingFeed($feed) {
    try {
        $amz=new AmazonFeed(); //if there is only one store in config, it can be omitted
        $amz->setFeedType("_POST_PRODUCT_PRICING_DATA_"); //feed types listed in documentation
        $amz->setFeedContent($feed); //can be either XML or CSV data; a file upload method is available as well
        $amz->submitFeed(); //this is what actually sends the request
        return $amz->getResponse();
    } catch (Exception $ex) {
        echo '<br/>There was a problem with the Amazon library. Error: '.$ex->getMessage();
    }
}

?>