<?php

$date = date('Y-m-d');

$getloyaltypoint = mysql_query("select * from $database.loyaltypoint 
inner join $masterdatabase.loyaltycheckfrequency on loyaltycheckfrequency.loyaltycheckfrequencyid = loyaltypoint.loyaltycheckfrequencyid
where loyaltypoint.disabled = '0'");
while($row99 = mysql_fetch_array($getloyaltypoint)){
	$loyaltypointid = $row99['loyaltypointid'];
	$loyaltypointname = $row99['loyaltypointname'];
	$triggerquery = $row99['triggerquery'];
	$numberofpoints = $row99['numberofpoints'];
	$maxtimes = $row99['maxtimes'];
	$noofseconds = $row99['noofseconds'];
	$loyaltycheckfrequencyname = $row99['loyaltycheckfrequencyname'];
	$lastruntimedate = $row99['lastruntimedate'];
	
	//echo "<b>".$loyaltypointname."</b><br/><br/>";
	
	//check if we need to run the query - check if it is due to be rerun
	$currentdatetime = date('Y-m-d H:i:s');	
	$runcheck = 0;
	if($noofseconds > 0){
		$diffseconds = strtotime($currentdatetime) - strtotime($lastruntimedate);
		if($diffseconds >= $noofseconds){
			$runcheck = 1;		
		}
	}
	else {
		//if 1st Day of Each Month
		if($loyaltycheckfrequencyname == "1st Day of Each Month"){
			$currentmonth = date('m');
			$previousmonth = date('m', strtotime($lastruntimedate));
			if($currentmonth > $previousmonth){
				$runcheck = 1;			
			}
		}
		
		//1st Day of Each Week
		if($loyaltycheckfrequencyname == "1st Day of Each Week"){
			$previousdate = date('Y-m-d', strtotime($lastruntimedate));
			if(date('w') === '1'){
				if($previousdate <> $date){
					$runcheck = 1;		
				}
			}
		}
		
		//15th Day of Each Month
		if($loyaltycheckfrequencyname == "15th Day of Each Month"){
			$currentday = date('d');
			if($currentday == 15){
				$previousdate = date('Y-m-d', strtotime($lastruntimedate));
				if($previousdate <> $date){	
					$runcheck = 1;		
				}		
			}
		}
		
		//1st Day of Each Quarter
		if($loyaltycheckfrequencyname == "1st Day of Each Quarter"){
			$currentday = date('d');
			$currentmonth = date('m');
			if(($currentmonth == '01' || $currentmonth == '04' || $currentmonth == '07' || $currentmonth == '10') && $currentday == '01'){
				$previousdate = date('Y-m-d', strtotime($lastruntimedate));
				if($previousdate <> $date){	
					$runcheck = 1;		
				}			
			}
		}
		
		//1st Day of Each Year
		if($loyaltycheckfrequencyname == "1st Day of Each Year"){
			$currentday = date('d');
			$currentmonth = date('m');
			if($currentmonth == '01' && $currentday == '01'){
				$previousdate = date('Y-m-d', strtotime($lastruntimedate));
				if($previousdate <> $date){	
					$runcheck = 1;		
				}	
			}
		}		
	}	
	if($lastruntimedate == ""){
		$runcheck == 1;	
	}
	
	
	//RUN THE CHECK, IF NEEDED
	if($runcheck == 1){
		$triggerquery = str_replace("##date##", $date, $triggerquery);
		$triggerquery = str_replace("##database##", $database, $triggerquery);
		//echo $triggerquery;
		$checkquery = mysql_query($triggerquery);
		while($row67 = mysql_fetch_array($checkquery)){
			$customerid = $row67['customerid'];	
			if(isset($row67['processdate'])){	
				$processdate = $row67['processdate'];	
			}	
			else {
				$processdate = $date;			
			}
			if(isset($row67['uniquevalue'])){	
				$uniquevalue = $row67['uniquevalue'];		
			}	
			else {
				$uniquevalue = 0;				
			}
			//echo "<br/>value: ".$uniquevalue." date: ".$processdate;	
			
			//check if record already exists
			$existing = 0;
			if($uniquevalue > 0){
				$checkexisting = mysql_query("select * from $database.loyaltypointcustomer 
				where uniquevalue = '$uniquevalue' and loyaltypointid = '$loyaltypointid'");	
				if(mysql_num_rows($checkexisting) >= 1){
					$existing = 1;
				}		
			}
			
			//check if user already awarded the maximum number of times
			$stopmaxtimes = 0;
			if($maxtimes <> 0){
				$getnumberoftimes = mysql_query("select * from $database.loyaltypointcustomer
				where loyaltypointid = '$loyaltypointid' and customerid = '$customerid'");
				if(mysql_num_rows($getnumberoftimes) >= $maxtimes){
					$stopmaxtimes = 1;				
				}	
			}
	
			//create record
			if($existing == 0 && $stopmaxtimes == 0){
				$createrecord = mysql_query("insert into $database.loyaltypointcustomer (loyaltypointcustomername, customerid, 
				loyaltypointid, dateawarded, pointsawarded, uniquevalue, disabled, datecreated, masteronly) 
				values ('$customerid', '$customerid', '$loyaltypointid', '$processdate', '$numberofpoints', '$uniquevalue', '0', '$date', '0')");			
			}
		}
		
		//update lastruntimedate
		$lastruntimedate = date('Y-m-d H:i:s');	
		$updateruntime = mysql_query("update $database.loyaltypoint set lastruntimedate = '$lastruntimedate' 
		where loyaltypointid = '$loyaltypointid'");
	}	
	
	//echo "<br/><br/>";	
}


//CALCULATE LOYALTY LEVEL
//delete any records that are now disabled
$getdisabled = mysql_query("select loyaltylevelid from $database.loyaltylevel where disabled = 1");
while ($row55 = mysql_fetch_array($getdisabled)){
	$loyaltylevelid = $row55['loyaltylevelid'];
	$deletedisabled = mysql_query("delete from $database.loyaltylevelcustomer 
	where loyaltylevelid = '$loyaltylevelid'");
}

//check customer level
$getcustomer = mysql_query("select loyaltypointcustomer.customerid, sum(pointsawarded) as 'totalpoints', loyaltylevelid from loyaltypointcustomer
left join loyaltylevelcustomer on loyaltylevelcustomer.customerid = loyaltypointcustomer.customerid
where loyaltypointcustomer.disabled = 0 group by loyaltypointcustomer.customerid");
while($row44 = mysql_fetch_array($getcustomer)){
	$customerid = $row44['customerid'];
	$totalpoints = $row44['totalpoints'];
	$existinglevel = $row44['loyaltylevelid'];
	//echo "<br/>customer: ".$customerid." points: ".$totalpoints." existinglevel: ".$existinglevel;	
	
	//get new level 
	$getlevel = mysql_query("select * from $database.loyaltylevel 
	where minloyaltypoints <= '$totalpoints' and maxloyaltypoints >= '$totalpoints' and disabled = 0");	
	$getlevelrow = mysql_fetch_array($getlevel);
	$loyaltylevelid = $getlevelrow['loyaltylevelid'];
	//echo " newlevel: ".$loyaltylevelid;
	
	//replace record	
	if($loyaltylevelid <> $existinglevel && $loyaltylevelid > 0){
		$deleterecord = mysql_query("delete from loyaltylevelcustomer where customerid = '$customerid'");
		$addrecord = mysql_query("insert into loyaltylevelcustomer (loyaltylevelcustomername, customerid, loyaltylevelid, dateawarded, 		disabled, datecreated, masteronly) values ('$customerid', '$customerid', '$loyaltylevelid', '$date', '0', '$date', '0')");
	}
	
}

?>