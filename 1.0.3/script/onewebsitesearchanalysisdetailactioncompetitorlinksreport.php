<?php

//LANGUAGE COLLECTION SECTION
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (209,210,211,212,213,214)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}

//update status as complete
$updatestatus = mysql_query("update $database.websitesearchanalysistask set completeddate = '$date', completedby = '$uid',
websitesearchtaskstatusid = '4' where websitesearchanalysisid = '$rowid' and websitesearchtaskid = '18'");

//get LinkHistory
class TrafficHistory {
    protected static $ServiceHost      = 'awis.amazonaws.com';
    public function TrafficHistory($accessKeyId, $secretAccessKey, $site, $offset) {
        $this->accessKeyId = $accessKeyId;
        $this->secretAccessKey = $secretAccessKey;
        $this->site = $site;
        $this->offset = $offset;
    }
    public function getTrafficHistory() {
        $queryParams = $this->buildQueryParams();
        $sig = $this->generateSignature($queryParams);
        $url = 'https://awis.amazonaws.com/?' . $queryParams . 
            '&Signature=' . $sig;
        $ret = self::makeRequest($url);
        //echo "\nResults for " . $this->site .":\n\n";
        $site = $this->site;
        self::parseResponse($ret,$site);
    }
	protected static function getTimestamp() {
        return gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time()); 
    }
    protected function buildQueryParams() {
    		$date = date ("Y-m-d");
    		$date = date ("Ymd", strtotime("-20 days", strtotime($date)));
    		$params = array(
            'Action'            => 'SitesLinkingIn',
            'ResponseGroup'     => 'SitesLinkingIn',
            'AWSAccessKeyId'    => $this->accessKeyId,
            'Timestamp'         => self::getTimestamp(),
            'Count'             => '20',
            'Start'             => $this->offset,
            'SignatureVersion'  => '2',
            'SignatureMethod'   => 'HmacSHA256',
            'Url'               => $this->site
        );
        ksort($params);
        $keyvalue = array();
        foreach($params as $k => $v) {
            $keyvalue[] = $k . '=' . rawurlencode($v);
        }
        return implode('&',$keyvalue);
    }
    protected static function makeRequest($url) {
        //echo "$url\n\n";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 6);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
  		  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
        if (strpos($result, 'Fatal error') !== false) {
    			echo 'text';
			}
    }
    public static function parseResponse($response,$site) {
    	global $offset;
    	 	try {
	    		$xml = new SimpleXMLElement($response,null,false,
	          'http://awis.amazonaws.com/doc/2005-07-11'); 
				 if($xml->count() && $xml->Response->SitesLinkingInResult->Alexa->SitesLinkingIn->Site->count()) {
	            $info = $xml->Response->SitesLinkingInResult->Alexa->SitesLinkingIn;
	            $nice_array = array(
	            );
	            $numrec = $xml->Response->SitesLinkingInResult->Alexa->SitesLinkingIn->Site->count();
	            //echo $numrec;
	            //echo "<br/>".$offset;
	            if($numrec < 20){
						$offset = 10000;            
	            }
	            	for ($i=0; $i<$numrec; $i++){
	            		array_push($nice_array, $info->Site[$i]->Title);
	            		array_push($nice_array, $info->Site[$i]->Url);     	
	            	}
	            	
	            //echo "<br/><br/>";
		        //echo "<table><thead><td>Title</td><td>Url</td></thead>";
		        $z = 1;
		        global $database;
		        global $rowid;
		        global $site;
		        $date = date ("Y-m-d");
		        $shorturl = '';     
		        $longurl = '';     
		        foreach($nice_array as $k => $v) {
		        		if($z == 1){
		            		$shorturl = $v;
		            	}
		            	if($z == 2){
		            		$longurl = $v;
		        			$z = $z +1;
		        		} 
		        		if($z == 3){
		            		//check link is already in library
							$checklink = mysql_query("select * from $database.websitesearchlink 
							where websitesearchanalysisid = '$rowid' and competitorurl = '$site' and linkurl = '$shorturl'");        			
		        			if(mysql_num_rows($checklink) == 0){
		        				//add link, if not in library
		        				if($longurl <> ""){
		        					$shorturl1 = $shorturl;
		        					$shorturl2 = "http://www.".$shorturl;
		        					$shorturl3 = "https://www.".$shorturl;
		        					$shorturl4 = "http://".$shorturl;
		        					$shorturl5 = "https://".$shorturl;
		        					$getlibraryurl = mysql_query("select * from $database.websitesearchlinklibrary
		        					where linkurl = '$shorturl1' or linkurl = '$shorturl2' or linkurl = '$shorturl3' or linkurl = '$shorturl4' or linkurl = '$shorturl5'");
			        				$getlibraryurlrow = mysql_fetch_array($getlibraryurl);
			        				$websitesearchlinklibraryid = $getlibraryurlrow['websitesearchlinklibraryid'];
			        				$query = mysql_query("insert into $database.websitesearchlink (websitesearchlinkname, websitesearchanalysisid, linkurl, 
			       				competitorurl, websitesearchlinklibraryid, websitesearchlinkstatusid, datecreated) values ('$longurl', '$rowid', '$shorturl', 
			       				'$site', '$websitesearchlinklibraryid', '1', '$date')");
		       				}
		        			}
		        			$z = 0;
		        		}
		        		$z = $z+1;
		        }
	        		unset($nice_array);
	        }
        
        } catch (Exception $e) {
			    echo 'Caught exception: ',  $e->getMessage(), "<br/>";    
			}
        
        //echo "</table>";
        //echo "<br/><br/>";
    }
    protected function generateSignature($url) {
        $sign = "GET\n" . strtolower(self::$ServiceHost) . "\n/\n". $url;
        //echo "String to sign: \n" . $sign . "\n";
        $sig = base64_encode(hash_hmac('sha256', $sign, $this->secretAccessKey, true));
        //echo "\nSignature: " . $sig ."\n";
        return rawurlencode($sig);
    }
}

$accessKeyId = "AKIAJXEU5GQLLAUGN25Q";
$secretAccessKey = "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";
$getwebsitecomp = mysql_query("select * from $database.websitesearchcompetitorreview 
where websitesearchanalysisid = '$rowid'");
$getwebsitecomprow = mysql_fetch_array($getwebsitecomp);
$competitorurl1 = $getwebsitecomprow['competitorurl1'];
$competitorurl2 = $getwebsitecomprow['competitorurl2'];
$competitorurl3 = $getwebsitecomprow['competitorurl3'];
$competitorurl4 = $getwebsitecomprow['competitorurl4'];
$competitorurl5 = $getwebsitecomprow['competitorurl5'];
$competitorurl6 = $getwebsitecomprow['competitorurl6'];
$competitorurl7 = $getwebsitecomprow['competitorurl7'];
$competitorurl8 = $getwebsitecomprow['competitorurl8'];
$competitorurl9 = $getwebsitecomprow['competitorurl9'];
$competitorurl10 = $getwebsitecomprow['competitorurl10'];
$w = 1;
while($w <= 10){
	//echo ${'competitorurl'.$w}."<br/>";
	if(${'competitorurl'.$w} <> ""){
		$slash = substr(${'competitorurl'.$w}, -1);
		if($slash == "/"){
			$site = substr(trim(${'competitorurl'.$w}), 0, -1);
		}
		else {
			$site = ${'competitorurl'.$w};
		}
		$site = str_replace('https://', '', $site);	
		$site = str_replace('http://', '', $site);	
		$site = str_replace('www.', '', $site);
		//echo "<br/>".$site."<br/>";	
		$offset = 0;
		while($offset <= 500){
			$traffichistory = new TrafficHistory($accessKeyId, $secretAccessKey, $site, $offset);
			$traffichistory->getTrafficHistory();
			$offset = $offset + 20;
		}
	}
	$w = $w + 1;
}

//GENERATE DOCUMENT
$getrecord = mysql_query("select * from $database.websitesearchanalysis 
inner join $database.website on website.websiteid = websitesearchanalysis.onewebsiteid
where websitesearchanalysis.websitesearchanalysisid = '$rowid'");
$getrecordrow = mysql_fetch_array($getrecord);
$websitesearchanalysisid = $getrecordrow['websitesearchanalysisid'];
$websitename = $getrecordrow['websitename'];
$websiteurl = $getrecordrow['websiteurl'];


//date created
$date = date("d-M-Y");
$date2 = date("Y-m-d");

$checklink = mysql_query("select min(websitesearchlinkid) as 'minlink' from $database.websitesearchlink 
where websitesearchanalysisid = '$rowid' and competitorurl <> '' and websitesearchlinkstatusid = '1'
order by websitesearchlinkid asc");  
$checklinkrow = mysql_fetch_array($checklink);
$websitesearchlinkid = $checklinkrow['minlink'];
if(mysql_num_rows($checklink) >= 1){
	$pdf = new PDF();
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->SetFont('Arial','B',13);
	$pdf->Cell(75);
	$pdf->Cell(40,10,$langval209.$websitename,0,0,'C');
	$pdf->Ln(15);
	$pdf->SetFont('Arial','',10);
	$pdf->MultiCell(0,5,$langval210,0,1);
	$pdf->Ln(5);
	$pdf->SetFont('Arial','',10);
	$pdf->MultiCell(0,5,$langval211.$date,0,1);
	$pdf->MultiCell(0,5,$langval212.$websiteurl,0,1);
	$pdf->Ln(6);

}	
	include('onewebsitesearchanalysisdetailactioncompetitorlinksreportsection.php');
	//uncomment this, if you want to see an example in the browser for testing. However, comment it out once you are finished otherwise the whilst loop will not run.
	//$pdf->Output();

if(mysql_num_rows($checklink) >= 1){
	
	//check if onewebsite storage folder exists, if not, then create
	$filedir = '../documents/'. $database."/sf/websitesearchlink/";
	if(file_exists($filedir)){
	}
	else {
		$file_directory = "../documents/".$database."/sf/websitesearchlink/";
		mkdir($file_directory);
	}
	
	$filename="../documents/".$database."/sf/websitesearchlink/".$websitesearchlinkid." - Website Search Competitor Back Links Report.pdf";
	$pdf->Output($filename,'F');
	
	
	//create document record in structurefunctiondocument
	$recordname = $websitesearchlinkid." - Website Search Competitor Back Links Report.pdf";
	$docurl = "../documents/".$database."/sf/websitesearchlink/".$recordname;
	$insertdocrecord = mysql_query("insert into $database.structurefunctiondocument (structurefunctiondocumentname, datecreated, 
	documenturl, doctypename, structurefunctionid, rowid) values ('$recordname', '$date2', '$docurl', 
	'Competitor Links Report', '235', '$websitesearchlinkid')");
}


	
?>
