<?php
//test defaults
//$emailcontentid = 1;
//$subject1 = 'Fred Bloggs';
//$content1 = 'Fred Bloggs';
//$content2 = 'Andy North';
//$content3 = 'Business Unit 111';
//$fromemail = 'test@test.com';
//$fromname= 'Test Name';

if(!isset($emailto)){
	$emailto = '';
}

//get emailcontent record
$mysqli = new mysqli($server, $user_name, $password, $database);$emailcontentid = mysqli_real_escape_string($mysqli, $emailcontentid);if($stmt = $mysqli->prepare("select * from emailcontent
inner join emaillayout on emaillayout.emaillayoutid = emailcontent.emaillayoutid
where emailcontentid = ?")){   $stmt->bind_param('i', $emailcontentid);   $stmt->execute();   $result = $stmt->get_result();   if($result->num_rows > 0){    	while($getemailcontentrow = $result->fetch_assoc()){     		$subject = $getemailcontentrow['subject'];
			$content = $getemailcontentrow['content'];
			$layoutcontent = $getemailcontentrow['htmlcontent'];    	}   	}}else {	//echo $mysqli->error;}$stmt->close();

//construct body
$body = $layoutcontent;
$body = str_replace("##maincontent##",$content,$body);
$i = 1;
while($i <= 50){
	if(isset(${"content$i"})){
		$body = str_replace("##content".$i."##",${"content$i"},$body);
	}
	$i = $i+1;
}
$body = str_replace("/n/n", "<br>", $body);
$body = str_replace("/r/n", "<br>", $body);
$body = str_replace("<br/>", "<br>", $body);
//echo $body;

//construct subject
$i = 1;
while($i <= 50){
	if(isset(${"subject$i"})){
		$subject = str_replace("##subject".$i."##",${"subject$i"},$subject);
	}
	$i = $i+1;
}
//echo $subject;

//construct From
if(!isset($fromemail)){
	$fromemail = 'alerts@onebusiness.com';
}
//echo $fromemail;

//construct FromName
if(!isset($fromname)){
	$fromname = 'OneBusiness';
}
//echo $fromname;

//construct Send To Addresses
$mysqli = new mysqli($server, $user_name, $password, $database);
$emailcontentid = mysqli_real_escape_string($mysqli, $emailcontentid);
if($stmt = $mysqli->prepare("select * from emailcontentrecipient 
inner join user on user.userid = emailcontentrecipient.userid
where emailcontentrecipient.emailcontentid = ? and user.email <> ''"))
{   $stmt->bind_param('i', $emailcontentid);   
	$stmt->execute();   $result = $stmt->get_result();   
	if($result->num_rows > 0){    	if($emailto == ''){
			$emailto = $emailto.'';	
		}
		else {
			$emailto = $emailto.', ';	
		}
		while($rowemailrecipient = $result->fetch_assoc()){     		$emailadd = $rowemailrecipient['email'];
			$emailto = $emailto.$emailadd.", ";    	}   	}}else {	//echo $mysqli->error;}$stmt->close();
$emailto = rtrim($emailto, ', ');
//echo "<br/>".$emailto;

//send email
require_once('PHPMailer/class.phpmailer.php');
$array = explode(',', $emailto); 
foreach($array as $value) {
	$value = trim($value);
	$email = new PHPMailer();
	$email->IsHTML(TRUE);
	$email->From      = $fromemail;
	$email->FromName  = $fromname;
	$email->Subject   = $subject;
	$email->Body      = $body;
	$email->AddAddress( $value );
	if(isset($attachmentpath)){
		$email->addAttachment($attachmentpath);
	}
	if(isset($attachmentpath2)){
		$email->addAttachment($attachmentpath2);
	}
	if(isset($attachmentpath3)){
		$email->addAttachment($attachmentpath3);
	}
	if(isset($attachmentpath4)){
		$email->addAttachment($attachmentpath4);
	}
	if(isset($attachmentpath5)){
		$email->addAttachment($attachmentpath5);
	}
	$email->Send();
}
//echo "send email: ".$emailcontentid;
?>