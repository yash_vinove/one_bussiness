<?php

//get application record
$getapplication = mysql_query("select * from $masterdatabase.appsetupapplication where appsetupapplicationid = '$appsetupapplicationid'");
$getapplicationrow = mysql_fetch_array($getapplication);
$databasetables = $getapplicationrow['databasetables'];
$businessgridpages = $getapplicationrow['businessgridpages'];
$businessnavigationsections = $getapplicationrow['businessnavigationsections'];
$businesspermissionroles = $getapplicationrow['businesspermissionroles'];
$businessdashboardsections = $getapplicationrow['businessdashboardsections'];
$businessreportsections = $getapplicationrow['businessreportsections'];
$businessviews = $getapplicationrow['businessviews'];
$businessscripts = $getapplicationrow['businessscripts'];
$businessemailcontents = $getapplicationrow['businessemailcontents'];
$businessrequestmanagerconfigurations = $getapplicationrow['businessrequestmanagerconfigurations'];

//insert databasetables
$array = explode(',', $databasetables); //split string into array seperated by ', '
foreach($array as $value) //loop over values
{
    //echo $value."<br/>";
    //check if table exists
   	$checktable = mysql_query("SELECT * FROM information_schema.tables
	WHERE table_schema = '$databasename' AND table_name = '$value'");
   if(mysql_num_rows($checktable) == 0){
	    $query2 = mysql_query("create table ".$databasename.".".$value." like ".$masterdatabase.".".$value);
	    //automatically set the minimum id to 100,000 so that clients records don't override onebusiness records
	    $insertdummy = "insert into ".$databasename.".".$value." (".$value."id) values (1000000)";
	    //echo $insertdummy."<br/>";
	    $insertdummy = mysql_query($insertdummy);
	    $deletedummy = mysql_query("delete from ".$databasename.".".$value." where ".$value."id = 1000000");	
	}
}

//insert grid pages and associated databasetables
$array = explode(',', $businessgridpages); //split string into array seperated by ', '
foreach($array as $value) //loop over values
{
   	$getstructurefunction = mysql_query("select * from $masterdatabase.structurefunction where structurefunctionid = '$value'");
  	$getstructurefunctionrow = mysql_fetch_array($getstructurefunction);
  	$tablename = $getstructurefunctionrow['tablename'];
	echo $value."-".$tablename."<br/>";
    
    //create database table
    $query2 = mysql_query("create table ".$databasename.".".$tablename." like ".$masterdatabase.".".$tablename);
    //automatically set the minimum id to 100,000 so that clients records don't override onebusiness records
     $insertdummy = "insert into ".$databasename.".".$tablename." (".$tablename."id) values (1000000)";
    //echo $insertdummy."<br/>";
    $insertdummy = mysql_query($insertdummy);
    $deletedummy = mysql_query("delete from ".$databasename.".".$tablename." where ".$tablename."id = 1000000");
    
    //create structurefunction table content
   	$insertstructurefieldtypes = mysql_query("INSERT INTO $databasename.structurefunction (structurefunctionid, 
   	structurefunctionname, tablename, 
	primarykeyfield, gridorderfield, gridorderdirection, datefilterfield, uploaderactive, runscriptadd, runscriptedit, 
	runscriptdisable, runscriptenable, runscriptextra1name, runscriptextra1page, runscriptextra2name, runscriptextra2page, 
	runscriptextra3name, runscriptextra3page, 
	runscriptextra4name, runscriptextra4page, runscriptextra5name, runscriptextra5page, addhelptext, edithelptext, 
	gridhelptext, mastercontrol, tenantcontrol, businesscontrol, disabled, datecreated, masteronly, allowdocupload, doctype, 
	runscriptdocupload, runscriptdocdelete, allowspreadsheetdownload)
	SELECT structurefunctionid, structurefunctionname, tablename, 
	primarykeyfield, gridorderfield, gridorderdirection, datefilterfield, uploaderactive, runscriptadd, runscriptedit, 
	runscriptdisable, runscriptenable, 
	runscriptextra1name, runscriptextra1page, runscriptextra2name, runscriptextra2page, runscriptextra3name, runscriptextra3page, 
	runscriptextra4name, runscriptextra4page, runscriptextra5name, runscriptextra5page,
	addhelptext, edithelptext, gridhelptext, mastercontrol, tenantcontrol, businesscontrol, disabled, datecreated, masteronly, 
	allowdocupload, doctype, runscriptdocupload, runscriptdocdelete, allowspreadsheetdownload
	FROM $masterdatabase.structurefunction
	where structurefunctionid = '$value'");

	//create structurefield table content
	$insertstructurefieldtypes = mysql_query("INSERT INTO $databasename.structurefield (structurefieldid, structurefunctionid, 
	structurefieldtypeid, structurefieldname, displayname, fieldposition, showingrid, gridposition, 
	requiredfield, textlimit, tableconnect, tablefieldconnect, lowvalue, highvalue, helptext, keywordsearchable, uniqueval, 
	noedit, mastercontrol, tenantcontrol, businesscontrol, disabled, datecreated, masteronly)
	SELECT structurefieldid, structurefunctionid, structurefieldtypeid, structurefieldname, displayname, fieldposition, 
	showingrid, gridposition, 
	requiredfield, textlimit, tableconnect, tablefieldconnect, lowvalue, highvalue, helptext, keywordsearchable, uniqueval, 
	noedit, mastercontrol, tenantcontrol, businesscontrol, disabled, datecreated, masteronly
	FROM $masterdatabase.structurefield
	where structurefunctionid = '$value'");

	//create structurefunctionpage table content
	$insertstructurefunctionpage = mysql_query("INSERT INTO $databasename.structurefunctionpage (structurefunctionpageid, 
	structurefunctionpagename, disabled, datecreated, navigationposition, structurefunctionsectionid, viewid, structurefunctionpagetypeid,
	structurefunctionid, documentationpagedescription, masteronly)
	SELECT structurefunctionpageid, structurefunctionpagename, disabled, datecreated, 
	navigationposition, structurefunctionsectionid, viewid, structurefunctionpagetypeid, structurefunctionid, documentationpagedescription, 
	masteronly
	FROM $masterdatabase.structurefunctionpage
	where structurefunctionid = '$value'");
	
	//create structurefieldlanguagerecordtext table content
	$getfieldnames = mysql_query("select * from $databasename.structurefield where structurefunctionid = '$value'");
	while($row45 = mysql_fetch_array($getfieldnames)){
		$structurefieldid = $row45['structurefieldid'];
		$insertstructurefieldlanguagerecordtext = mysql_query("INSERT INTO $databasename.structurefieldlanguagerecordtext 
		(structurefieldlanguagerecordtextid, structurefieldlanguagerecordtextname, disabled, datecreated, mastercontrol,
		tenantcontrol, businesscontrol, languageid, structurefieldname, helptext, masteronly)
		SELECT structurefieldlanguagerecordtextid, structurefieldlanguagerecordtextname, disabled, datecreated, mastercontrol,
		tenantcontrol, businesscontrol, languageid, structurefieldname, helptext, masteronly
		FROM $masterdatabase.structurefieldlanguagerecordtext
		where structurefieldid = '$structurefieldid'");		
	}
	
	//create structurefunctionpagelanguagerecordtext table content
	$insertstructurefunctionpagelanguagerecordtext = mysql_query("INSERT INTO 
	$databasename.structurefunctionpagelanguagerecordtext (structurefunctionpagelanguagerecordid, structurefunctionpagelanguagerecordtext, 
	disabled, datecreated, languageid, structurefunctionid, addhelptext, edithelptext, gridhelptext, 
	mastercontrol, tenantcontrol, businesscontrol, masteronly)
	SELECT structurefunctionpagelanguagerecordid, structurefunctionpagelanguagerecordtext, 
	disabled, datecreated, languageid, structurefunctionid, addhelptext, edithelptext, gridhelptext, 
	mastercontrol, tenantcontrol, businesscontrol, masteronly
	FROM $masterdatabase.structurefunctionpagelanguagerecordtext
	where structurefunctionid = '$value'");
	
	//create structurefunctionsectionlanguagerecordtext table content
   $insertstructurefunctionpagelanguagerecordtext = mysql_query("INSERT INTO 
	$databasename.structurefunctionsectionlanguagerecordtext (structurefunctionsectionlanguagerecordid, structurefunctionsectionlanguagerecordtext, 
	disabled, datecreated, languageid, structurefunctionid, addhelptext, edithelptext, gridhelptext, 
	mastercontrol, tenantcontrol, businesscontrol, masteronly)
	SELECT structurefunctionsectionlanguagerecordtext, structurefunctionpagelanguagerecordtext, 
	disabled, datecreated, languageid, structurefunctionid, addhelptext, edithelptext, gridhelptext, 
	mastercontrol, tenantcontrol, businesscontrol, masteronly
	FROM $masterdatabase.structurefunctionsectionlanguagerecordtext
	where structurefunctionid = '$value'"); 
}

//insert businessemailcontents
if($businessemailcontents <> ''){
	$array = explode(',', $businessemailcontents); //split string into array seperated by ', '
	foreach($array as $value) //loop over values
	{
	   	//create emailcontent table content
	   	$insertemailcontents = mysql_query("INSERT INTO $databasename.emailcontent (emailcontentid, emailcontentname, disabled,
	   	datecreated, masteronly, subject, content, emaillayoutid)
		SELECT emailcontentid, emailcontentname, disabled, datecreated, masteronly, subject, content, emaillayoutid
		FROM $masterdatabase.emailcontent
		where emailcontentid = '$value'");
	}
	//get all added emailcontents 
	$getemailcontent = mysql_query("select emaillayoutid from $databasename.emailcontent group by emaillayoutid");
	while($row2=mysql_fetch_array($getemailcontent)){
		//create emaillayout table content
		$value99 = $row2['emaillayoutid'];
	   	$insertemaillayouts = mysql_query("INSERT INTO $databasename.emaillayout (emaillayoutid, emaillayoutname, disabled, 
	   	datecreated, masteronly, htmlcontent)
		SELECT emaillayoutid, emaillayoutname, disabled, datecreated, masteronly, htmlcontent
		FROM $masterdatabase.emaillayout
		where emaillayoutid = '$value99'");	
	}
}	
	
//insert requestconfiguration	
$array = explode(',', $businessrequestmanagerconfigurations); //split string into array seperated by ', '
foreach($array as $value) //loop over values
{
   	//create requestconfiguration table content
   	$insertrequestconfiguration = mysql_query("INSERT INTO $databasename.requestconfiguration (requestconfigurationid,
   	requestconfigurationname, disabled, datecreated,requesttable, requestidfield, requestdisplayfield, masteronly, 
   	emailcontentrequestid, emailcontentdeclinedid, emailcontentapprovedid, permissiontable)
	SELECT requestconfigurationid,
   	requestconfigurationname, disabled, datecreated,requesttable, requestidfield, requestdisplayfield, masteronly, 
   	emailcontentrequestid, emailcontentdeclinedid, emailcontentapprovedid, permissiontable
	FROM $masterdatabase.requestconfiguration
	where requestconfigurationid = '$value'");
}
	
//structurefunctionsections
$array = explode(',', $businessnavigationsections); //split string into array seperated by ', '
foreach($array as $value2) //loop over values
{
    $getstructurefunctionsection = mysql_query("select * from structurefunctionsection
    where structurefunctionsectionid = '$value2'");
    $getstructurefunctionsectionrow = mysql_fetch_array($getstructurefunctionsection);
    $structurefunctionsectionname = $getstructurefunctionsectionrow['structurefunctionsectionname'];
    

	//create structurefunctionsection table content
	$insertstructurefunctionsection = mysql_query("INSERT INTO 
	$databasename.structurefunctionsection (structurefunctionsectionid, structurefunctionsectionname, sortorder, 
	mastercontrol, tenantcontrol, businesscontrol, disabled, datecreated, masteronly)
	SELECT structurefunctionsectionid, structurefunctionsectionname, sortorder, 
	mastercontrol, tenantcontrol, businesscontrol, disabled, datecreated, masteronly
	FROM $masterdatabase.structurefunctionsection
	where structurefunctionsectionid = '$value2'"); 
	
	//create structurefunctionsectionlanguagerecordtext table content
	$insertstructurefunctionsectionlanguagerecordtext = mysql_query("INSERT INTO 
	$databasename.structurefunctionsectionlanguagerecordtext (structurefunctionsectionlanguagerecordtextid, structurefunctionsectionlanguagerecordtextname, 
	structurefunctionsectionid, languageid, mastercontrol, tenantcontrol, businesscontrol, disabled, datecreated, masteronly)
	SELECT structurefunctionsectionlanguagerecordtextid, structurefunctionsectionlanguagerecordtextname, 
	structurefunctionsectionid, languageid, mastercontrol, tenantcontrol, businesscontrol, disabled, datecreated, masteronly
	FROM $masterdatabase.structurefunctionsectionlanguagerecordtext
	where structurefunctionsectionid = '$value2'"); 
	
}	
		
//businesspermissionroles
$array = explode(',', $businesspermissionroles); //split string into array seperated by ', '
foreach($array as $value3) //loop over values
{
    $getpermissionrole = mysql_query("select * from permissionrole
    where permissionroleid = '$value3'");
    $getpermissionrolerow = mysql_fetch_array($getpermissionrole);
    $permissionrolename = $getpermissionrolerow['permissionrolename'];
    
    //create permissionrole table content
	$insertpermissionrole = mysql_query("INSERT INTO 
	$databasename.permissionrole (permissionroleid, permissionrolename, disabled, datecreated, helpdocumentation, masteronly)
	SELECT permissionroleid, permissionrolename, disabled, datecreated, helpdocumentation, masteronly
	FROM $masterdatabase.permissionrole
	where permissionroleid = '$value3'"); 
	
	$date = date("Y-m-d");	
	
	if($appsetupapplicationid <= 2){
		//insert admin role into userrole table
		$insertuserrole = mysql_query("insert into $databasename.userrole (userid, permissionroleid, disabled, datecreated) 
		values ('1000001', '$value3', '0', '$date')");
	}
	else{
		$insertuserrole = mysql_query("insert into userrole (userid, permissionroleid, disabled, datecreated) 
		values ('$userid', '$value3', '0', '$date')");
	}
	
	//create permissionrolestructurefunctionpage table content
	$insertpermissionrolestructurefunctionpage = mysql_query("INSERT INTO 
	$databasename.permissionrolestructurefunctionpage (permissionrolestructurefunctionpageid, 
	permissionroleid, structurefunctionpageid, disabled, datecreated, masteronly)
	SELECT permissionrolestructurefunctionpageid, 
	permissionroleid, structurefunctionpageid, disabled, datecreated, masteronly
	FROM $masterdatabase.permissionrolestructurefunctionpage
	where permissionroleid = '$value3'"); 
	
	//create permissionrolestructurefunctionsection table content
	$insertpermissionrolestructurefunctionsection = mysql_query("INSERT INTO 
	$databasename.permissionrolestructurefunctionsection (permissionrolestructurefunctionsectionid, 
	permissionroleid, structurefunctionsectionid, disabled, datecreated, masteronly)
	SELECT permissionrolestructurefunctionsectionid, 
	permissionroleid, structurefunctionsectionid, disabled, datecreated, masteronly
	FROM $masterdatabase.permissionrolestructurefunctionsection
	where permissionroleid = '$value3'"); 
	
	//create permissionroledashboardsection table content
	$insertpermissionroledashboardsection = mysql_query("INSERT INTO 
	$databasename.permissionroledashboardsection (permissionroledashboardsectionid, 
	permissionroleid, chartsectionid, disabled, datecreated, masteronly)
	SELECT permissionroledashboardsectionid, 
	permissionroleid, chartsectionid, disabled, datecreated, masteronly
	FROM $masterdatabase.permissionroledashboardsection
	where permissionroleid = '$value3'"); 
	
	//create permissionrolereportsection table content
	$insertpermissionrolereportsection = mysql_query("INSERT INTO 
	$databasename.permissionrolereportsection (permissionrolereportsectionid, 
	permissionroleid, reportsectionid, disabled, datecreated, masteronly)
	SELECT permissionrolereportsectionid, 
	permissionroleid, reportsectionid, disabled, datecreated, masteronly
	FROM $masterdatabase.permissionrolereportsection
	where permissionroleid = '$value3'"); 
}


//dashboards
$array = explode(',', $businessdashboardsections); //split string into array seperated by ', '
foreach($array as $value4) //loop over values
{
	//create chartsection table content
	$insertchartsection = mysql_query("INSERT INTO 
	$databasename.chartsection (chartsectionid, chartsectionname, disabled, datecreated, sortorder, masteronly)
	SELECT chartsectionid, chartsectionname, disabled, datecreated, sortorder, masteronly
	FROM $masterdatabase.chartsection
	where chartsectionid = '$value4'"); 
	
	//create dashboardsectionfilter table content
	$insertchartsection = mysql_query("INSERT INTO 
	$databasename.dashboardsectionfilter (dashboardsectionfilterid, dashboardsectionfiltername, structurefieldid, chartsectionid, 
	disabled, datecreated, masteronly)
	SELECT dashboardsectionfilterid, dashboardsectionfiltername, structurefieldid, chartsectionid, 
	disabled, datecreated, masteronly
	FROM $masterdatabase.dashboardsectionfilter
	where chartsectionid = '$value4'"); 
	
	//create chartsectiontranslation table content
	$insertchartsection = mysql_query("INSERT INTO 
	$databasename.chartsectiontranslation (chartsectiontranslationid, chartsectiontranslationname, 
	disabled, datecreated, chartsectionid, languageid, masteronly)
	SELECT chartsectiontranslationid, chartsectiontranslationname, 
	disabled, datecreated, chartsectionid, languageid, masteronly
	FROM $masterdatabase.chartsectiontranslation
	where chartsectionid = '$value4'"); 
	
	//create chart table content
	$insertchart = mysql_query("INSERT INTO 
	$databasename.chart (chartid, chartname, disabled, datecreated, chartsectionid, datasetid, charttypeid, chartdescription, masteronly, sqlquery)
	SELECT chartid, chartname, disabled, datecreated, chartsectionid, datasetid, charttypeid, chartdescription, masteronly, sqlquery
	FROM $masterdatabase.chart
	where chartsectionid = '$value4'");
	
	//create dataset table content
	$getdatasets = mysql_query("select * from $databasename.chart where chartsectionid = '$value4'");
	while($row46 = mysql_fetch_array($getdatasets)){
		$datasetid = $row46['datasetid'];
		$chartid = $row46['chartid'];
		$insertdataset = mysql_query("INSERT INTO 
		$databasename.dataset (datasetid, datasetname, disabled, datecreated, sortbyfield, datefilterfield, invert, masteronly)
		SELECT datasetid, datasetname, disabled, datecreated, sortbyfield, datefilterfield, invert, masteronly
		FROM $masterdatabase.dataset
		where datasetid = '$datasetid'"); 
		
		//create charttranslation table content		
		$insertcharttranslation = mysql_query("INSERT INTO 
		$databasename.charttranslation (charttranslationid, charttranslationname, disabled, datecreated, chartid, languageid, masteronly)
		SELECT charttranslationid, charttranslationname, disabled, datecreated, chartid, languageid, masteronly
		FROM $masterdatabase.charttranslation
		where chartid = '$chartid'"); 
		
		//create datasetcolumn table content
		$insertdatasetcolumns = mysql_query("INSERT INTO 
		$databasename.datasetcolumn (datasetcolumnid, datasetcolumnname, disabled, datecreated, datasetid, 
		structurefieldid, datasetaggregateid, format, sortorder, connectedtablename, hidecolumn, masteronly, calculationlogic)
		SELECT datasetcolumnid, datasetcolumnname, disabled, datecreated, datasetid, 
		structurefieldid, datasetaggregateid, format, sortorder, connectedtablename, hidecolumn, masteronly, calculationlogic
		FROM $masterdatabase.datasetcolumn
		where datasetid = '$datasetid'"); 
		
		//create datasetfilter table content
		$getdatasetcolumns = mysql_query("select * from $databasename.datasetcolumn where datasetid = '$datasetid'");
		while($row47 = mysql_fetch_array($getdatasetcolumns)){
			$datasetcolumnid = $row47['datasetcolumnid'];
			$insertdataset = mysql_query("INSERT INTO 
			$databasename.datasetfilter (datasetfilterid, datasetfiltername, disabled, datecreated, datasetcolumnid, datasetfilterlogicid, masteronly)
			SELECT datasetfilterid, datasetfiltername, disabled, datecreated, datasetcolumnid, datasetfilterlogicid, masteronly
			FROM $masterdatabase.datasetfilter
			where datasetcolumnid = '$datasetcolumnid'"); 
		}			
	}
}



//reports
$array = explode(',', $businessreportsections); //split string into array seperated by ', '
foreach($array as $value5) //loop over values
{
	//create reportsection table content
	$insertreportsection = mysql_query("INSERT INTO 
	$databasename.reportsection (reportsectionid, reportsectionname, disabled, datecreated, sortorder, masteronly, defaultstartdate, defaultenddate)
	SELECT reportsectionid, reportsectionname, disabled, datecreated, sortorder, masteronly, defaultstartdate, defaultenddate
	FROM $masterdatabase.reportsection
	where reportsectionid = '$value5'"); 
	
	//create reportsectiontranslation table content
	$insertreportsection = mysql_query("INSERT INTO 
	$databasename.reportsectiontranslation (reportsectiontranslationid, reportsectiontranslationname, 
	disabled, datecreated, reportsectionid, languageid, masteronly)
	SELECT reportsectiontranslationid, reportsectiontranslationname, 
	disabled, datecreated, reportsectionid, languageid, masteronly
	FROM $masterdatabase.reportsectiontranslation
	where reportsectionid = '$value5'"); 
	
	//create report table content
	$insertreport = mysql_query("INSERT INTO 
	$databasename.report (reportid, reportname, disabled, datecreated, reportsectionid, datasetid, reportdescription, masteronly)
	SELECT reportid, reportname, disabled, datecreated, reportsectionid, datasetid, reportdescription, masteronly
	FROM $masterdatabase.report
	where reportsectionid = '$value5'");
	
	//create dataset table content
	$getdatasets = mysql_query("select * from $databasename.report where reportsectionid = '$value5'");
	while($row46 = mysql_fetch_array($getdatasets)){
		$datasetid = $row46['datasetid'];
		$reportid = $row46['reportid'];
		$insertdataset = mysql_query("INSERT INTO 
		$databasename.dataset (datasetid, datasetname, disabled, datecreated, sortbyfield, datefilterfield, invert, masteronly)
		SELECT datasetid, datasetname, disabled, datecreated, sortbyfield, datefilterfield, invert, masteronly
		FROM $masterdatabase.dataset
		where datasetid = '$datasetid'"); 
		
		//create reporttranslation table content		
		$insertreporttranslation = mysql_query("INSERT INTO 
		$databasename.reporttranslation (reporttranslationid, reporttranslationname, disabled, datecreated, reportid, languageid, masteronly)
		SELECT reporttranslationid, reporttranslationname, disabled, datecreated, reportid, languageid, masteronly
		FROM $masterdatabase.reporttranslation
		where reportid = '$reportid'"); 
		
		//create datasetcolumn table content
		$insertdatasetcolumns = mysql_query("INSERT INTO 
		$databasename.datasetcolumn (datasetcolumnid, datasetcolumnname, disabled, datecreated, datasetid, 
		structurefieldid, datasetaggregateid, format, sortorder, connectedtablename, hidecolumn, masteronly, calculationlogic)
		SELECT datasetcolumnid, datasetcolumnname, disabled, datecreated, datasetid, 
		structurefieldid, datasetaggregateid, format, sortorder, connectedtablename, hidecolumn, masteronly, calculationlogic
		FROM $masterdatabase.datasetcolumn
		where datasetid = '$datasetid'"); 
		
		//create datasetfilter table content
		$getdatasetcolumns = mysql_query("select * from $databasename.datasetcolumn where datasetid = '$datasetid'");
		while($row47 = mysql_fetch_array($getdatasetcolumns)){
			$datasetcolumnid = $row47['datasetcolumnid'];
			$insertdataset = mysql_query("INSERT INTO 
			$databasename.datasetfilter (datasetfilterid, datasetfiltername, disabled, datecreated, datasetcolumnid, datasetfilterlogicid, masteronly)
			SELECT datasetfilterid, datasetfiltername, disabled, datecreated, datasetcolumnid, datasetfilterlogicid, masteronly
			FROM $masterdatabase.datasetfilter
			where datasetcolumnid = '$datasetcolumnid'"); 
		}			
	}
}


//views
$array = explode(',', $businessviews); //split string into array seperated by ', '
foreach($array as $value6) //loop over values
{
	//create view table content
	$insertview = mysql_query("INSERT INTO 
	$databasename.view (viewid, viewname, disabled, datecreated, masteronly)
	SELECT viewid, viewname, disabled, datecreated, masteronly
	FROM $masterdatabase.view
	where viewid = '$value6'"); 

	//create structurefunctionpage table content
	$insertview = mysql_query("INSERT INTO 
	$databasename.structurefunctionpage (structurefunctionpageid, structurefunctionpagename, disabled, datecreated,
	navigationposition, structurefunctionsectionid, viewid, structurefunctionpagetypeid, structurefunctionid, documentationpagedescription, 
	masteronly)
	SELECT structurefunctionpageid, structurefunctionpagename, disabled, datecreated,
	navigationposition, structurefunctionsectionid, viewid, structurefunctionpagetypeid, structurefunctionid, documentationpagedescription, 
	masteronly
	FROM $masterdatabase.structurefunctionpage
	where viewid = '$value6'"); 
	
	//create viewsection table content
	$insertviewsection = mysql_query("INSERT INTO 
	$databasename.viewsection (viewsectionid, viewsectionname, disabled, datecreated, viewid, position, viewtextid,
	viewdatasetid, viewmapid, viewcalendarid, viewspecialcodeurl, documentationhelp, masteronly)
	SELECT viewsectionid, viewsectionname, disabled, datecreated, viewid, position, viewtextid,
	viewdatasetid, viewmapid, viewcalendarid, viewspecialcodeurl, documentationhelp, masteronly
	FROM $masterdatabase.viewsection
	where viewid = '$value6'"); 
	
	//get added viewsections so you can add the rest of the components
	$getviewsections = mysql_query("select * from $databasename.viewsection where viewid = '$value6'");
	while($row89 = mysql_fetch_array($getviewsections)){
		$viewsectionid = $row89['viewsectionid'];
		$viewtextid = $row89['viewtextid'];
		$viewdatasetid = $row89['viewdatasetid'];
		$viewmapid = $row89['viewmapid'];
		$viewcalendarid = $row89['viewcalendarid'];
		
		//create viewtext table content
		if($viewtextid >= 1){
			$insertviewtext = mysql_query("INSERT INTO 
			$databasename.viewtext (viewtextid, viewtextname, disabled, datecreated, text, styleheadertag, masteronly)
			SELECT viewtextid, viewtextname, disabled, datecreated, text, styleheadertag, masteronly
			FROM $masterdatabase.viewtext
			where viewtextid = '$viewtextid'"); 	
		}	
		
		//create viewdataset table content
		if($viewdatasetid >= 1){
			$insertviewdataset = mysql_query("INSERT INTO 
			$databasename.viewdataset (viewdatasetid, viewdatasetname, disabled, datecreated, datasetid, showheadings, 
			gridtextdescription, tablewidth, masteronly, allowedituniquefieldid, requestconfigurationid, pagelink, pagelinkname,
			structurefieldiddisable, structurefieldidenable, addnew)
			SELECT viewdatasetid, viewdatasetname, disabled, datecreated, datasetid, showheadings, 
			gridtextdescription, tablewidth, masteronly, allowedituniquefieldid, requestconfigurationid, pagelink, pagelinkname,
			structurefieldiddisable, structurefieldidenable, addnew
			FROM $masterdatabase.viewdataset
			where viewdatasetid = '$viewdatasetid'"); 	
			
			//create viewdataset dataset table content
			$getdatasets = mysql_query("select * from $databasename.viewdataset where viewdatasetid = '$viewdatasetid'");
			while($row46 = mysql_fetch_array($getdatasets)){
				$datasetid = $row46['datasetid'];
				$insertdataset = mysql_query("INSERT INTO 
				$databasename.dataset (datasetid, datasetname, disabled, datecreated, sortbyfield, datefilterfield, invert, masteronly)
				SELECT datasetid, datasetname, disabled, datecreated, sortbyfield, datefilterfield, invert, masteronly
				FROM $masterdatabase.dataset
				where datasetid = '$datasetid'"); 
				
				//create datasetcolumn table content
				$insertdatasetcolumns = mysql_query("INSERT INTO 
				$databasename.datasetcolumn (datasetcolumnid, datasetcolumnname, disabled, datecreated, datasetid, 
				structurefieldid, datasetaggregateid, format, sortorder, connectedtablename, hidecolumn, masteronly, calculationlogic)
				SELECT datasetcolumnid, datasetcolumnname, disabled, datecreated, datasetid, 
				structurefieldid, datasetaggregateid, format, sortorder, connectedtablename, hidecolumn, masteronly, calculationlogic
				FROM $masterdatabase.datasetcolumn
				where datasetid = '$datasetid'"); 
				
				//create datasetfilter table content
				$getdatasetcolumns = mysql_query("select * from $databasename.datasetcolumn where datasetid = '$datasetid'");
				while($row47 = mysql_fetch_array($getdatasetcolumns)){
					$datasetcolumnid = $row47['datasetcolumnid'];
					$insertdataset = mysql_query("INSERT INTO 
					$databasename.datasetfilter (datasetfilterid, datasetfiltername, disabled, datecreated, datasetcolumnid, datasetfilterlogicid, masteronly)
					SELECT datasetfilterid, datasetfiltername, disabled, datecreated, datasetcolumnid, datasetfilterlogicid, masteronly
					FROM $masterdatabase.datasetfilter
					where datasetcolumnid = '$datasetcolumnid'"); 
				}			
			}
		}
		
		//create viewmap table content
		if($viewmapid >= 1){
			$insertviewmap = mysql_query("INSERT INTO 
			$databasename.viewmap (viewmapid, viewmapname, disabled, datecreated, datasetid, viewheat, viewlocationpins, 
			allowsearch, datasetcolumnid, masteronly)
			SELECT viewmapid, viewmapname, disabled, datecreated, datasetid, viewheat, viewlocationpins, 
			allowsearch, datasetcolumnid, masteronly
			FROM $masterdatabase.viewmap
			where viewmapid = '$viewmapid'"); 
			
			//create viewmapfilter table content
			$insertviewmapfilter = mysql_query("INSERT INTO 
			$databasename.viewmapfilter (viewmapfilterid, viewmapfiltername, disabled, datecreated, viewmapid, 
			datasetcolumnid, datasetfilterlogicid, masteronly)
			SELECT viewmapfilterid, viewmapfiltername, disabled, datecreated, viewmapid, 
			datasetcolumnid, datasetfilterlogicid, masteronly
			FROM $masterdatabase.viewmapfilter
			where viewmapid = '$viewmapid'"); 
				
			//create viewmap dataset table content
			$getdatasets = mysql_query("select * from $databasename.viewmap where viewmapid = '$viewmapid'");
			while($row46 = mysql_fetch_array($getdatasets)){
				$datasetid = $row46['datasetid'];
				$insertdataset = mysql_query("INSERT INTO 
				$databasename.dataset (datasetid, datasetname, disabled, datecreated, sortbyfield, datefilterfield, invert, masteronly)
				SELECT datasetid, datasetname, disabled, datecreated, sortbyfield, datefilterfield, invert, masteronly
				FROM $masterdatabase.dataset
				where datasetid = '$datasetid'"); 
				
				//create datasetcolumn table content
				$insertdatasetcolumns = mysql_query("INSERT INTO 
				$databasename.datasetcolumn (datasetcolumnid, datasetcolumnname, disabled, datecreated, datasetid, 
				structurefieldid, datasetaggregateid, format, sortorder, connectedtablename, hidecolumn, masteronly, calculationlogic)
				SELECT datasetcolumnid, datasetcolumnname, disabled, datecreated, datasetid, 
				structurefieldid, datasetaggregateid, format, sortorder, connectedtablename, hidecolumn, masteronly, calculationlogic
				FROM $masterdatabase.datasetcolumn
				where datasetid = '$datasetid'"); 
				
				//create datasetfilter table content
				$getdatasetcolumns = mysql_query("select * from $databasename.datasetcolumn where datasetid = '$datasetid'");
				while($row47 = mysql_fetch_array($getdatasetcolumns)){
					$datasetcolumnid = $row47['datasetcolumnid'];
					$insertdataset = mysql_query("INSERT INTO 
					$databasename.datasetfilter (datasetfilterid, datasetfiltername, disabled, datecreated, datasetcolumnid, datasetfilterlogicid, masteronly)
					SELECT datasetfilterid, datasetfiltername, disabled, datecreated, datasetcolumnid, datasetfilterlogicid, masteronly
					FROM $masterdatabase.datasetfilter
					where datasetcolumnid = '$datasetcolumnid'"); 
				}			
			}
		}
		
		//create viewcalendar table content
		if($viewcalendarid >= 1){
			$insertviewcalendar = mysql_query("INSERT INTO 
			$databasename.viewcalendar (viewcalendarid, viewcalendarname, disabled, datecreated, datasetid, masteronly)
			SELECT viewcalendarid, viewcalendarname, disabled, datecreated, datasetid, masteronly
			FROM $masterdatabase.viewcalendar
			where viewcalendarid = '$viewcalendarid'"); 	
			
			//create viewcalendar dataset table content
			$getdatasets = mysql_query("select * from $databasename.viewcalendar where viewcalendarid = '$viewcalendarid'");
			while($row46 = mysql_fetch_array($getdatasets)){
				$datasetid = $row46['datasetid'];
				$insertdataset = mysql_query("INSERT INTO 
				$databasename.dataset (datasetid, datasetname, disabled, datecreated, sortbyfield, datefilterfield, invert, masteronly)
				SELECT datasetid, datasetname, disabled, datecreated, sortbyfield, datefilterfield, invert, masteronly
				FROM $masterdatabase.dataset
				where datasetid = '$datasetid'"); 
				
				//create datasetcolumn table content
				$insertdatasetcolumns = mysql_query("INSERT INTO 
				$databasename.datasetcolumn (datasetcolumnid, datasetcolumnname, disabled, datecreated, datasetid, 
				structurefieldid, datasetaggregateid, format, sortorder, connectedtablename, hidecolumn, masteronly, calculationlogic)
				SELECT datasetcolumnid, datasetcolumnname, disabled, datecreated, datasetid, 
				structurefieldid, datasetaggregateid, format, sortorder, connectedtablename, hidecolumn, masteronly, calculationlogic
				FROM $masterdatabase.datasetcolumn
				where datasetid = '$datasetid'"); 
				
				//create datasetfilter table content
				$getdatasetcolumns = mysql_query("select * from $databasename.datasetcolumn where datasetid = '$datasetid'");
				while($row47 = mysql_fetch_array($getdatasetcolumns)){
					$datasetcolumnid = $row47['datasetcolumnid'];
					$insertdataset = mysql_query("INSERT INTO 
					$databasename.datasetfilter (datasetfilterid, datasetfiltername, disabled, datecreated, datasetcolumnid, datasetfilterlogicid, masteronly)
					SELECT datasetfilterid, datasetfiltername, disabled, datecreated, datasetcolumnid, datasetfilterlogicid, masteronly
					FROM $masterdatabase.datasetfilter
					where datasetcolumnid = '$datasetcolumnid'"); 
				}			
			}		
		}
		
			
	}
}

//scripts
$array = explode(',', $businessscripts); //split string into array seperated by ', '
foreach($array as $value8) //loop over values
{
    //create scheduledjob table content
	$insertscheduledjob = mysql_query("INSERT INTO 
	$databasename.scheduledjob (scheduledjobid, scheduledjobname, disabled, datecreated, scheduledjobscriptid, min,
	hour, daymonth, month, dayweek, lastrundate, masteronly, allowrun)
	SELECT scheduledjobid, scheduledjobname, disabled, datecreated, scheduledjobscriptid, min,
	hour, daymonth, month, dayweek, lastrundate, masteronly, allowrun
	FROM $masterdatabase.scheduledjob
	where scheduledjobscriptid = '$value8'");
	
	//create scheduledjobscript table content
	$insertscheduledjobscript = mysql_query("INSERT INTO 
	$databasename.scheduledjobscript (scheduledjobscriptid, scheduledjobscriptname, disabled, datecreated, 
	description, scripturl, masteronly)
	SELECT scheduledjobscriptid, scheduledjobscriptname, disabled, datecreated, 
	description, scripturl, masteronly
	FROM $masterdatabase.scheduledjobscript
	where scheduledjobscriptid = '$value8'"); 
}	

?>