<?php 
	$lifetime = strtotime('+3000 minutes', 0);
	session_set_cookie_params($lifetime);
?>
<?php session_start(); ?>
<?php
if(!isset($_COOKIE['database'])) {
    //echo "Cookie named 'database' is not set!";
} else {
    //echo "Cookie 'database' is set!<br>";
    //echo "Value is: " . $_COOKIE['database'];
}

/*$lifetime = strtotime('+3000 minutes', 0);
session_set_cookie_params($lifetime);*/
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
<?php //include('config.php'); ?>
<?php include('sessionconfig.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<link href="./fontAwesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="screen" title="stylesheet" />
	<title>OneBusiness</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel='stylesheet' type='text/css' href='style.php' />
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>	
	
	<script>
 	jQuery(function(){
	        jQuery('.showSingle').click(function(){
	              jQuery('.targetDiv').hide();
	              jQuery('#div'+$(this).attr('target')).show();
	        });
	});
	</script>
													
</head>
  
<body class='body'>

<?php include_once ('headerthree.php');	?>
	
<?php
//LANGUAGE COLLECTION SECTION
$mysqli = new mysqli($server, $user_name, $password, $database);
$languageid = mysqli_real_escape_string($mysqli, $_SESSION['languageid']);
if($stmt = $mysqli->prepare("SELECT languagerecordid, languagerecordtextname FROM $masterdatabase.languagerecordtext 
where languageid = ? and languagerecordid in (4,5,9,10,19,848,849,850,851,852,853,854,855,856,857,858,859,860,861,862,863,
864,865,866,867,868)")){
	$stmt->bind_param('i', $languageid);
	$stmt->execute();
	$result = $stmt->get_result();
	while ($row = $result->fetch_assoc()){
		$langid = $row['languagerecordid'];
		${"langval$langid"} = $row['languagerecordtextname'];
		//echo "<br/>".$langid." -".${"langval$langid"};
	}
}

$dashboardsectionid = isset($_GET['dashboardsectionid']) ? $_GET['dashboardsectionid'] : '';
$dashboardsectionid = mysql_real_escape_string($dashboardsectionid);
		

		
		$number = 1;
		
		for($i = 0; $i <= 100; $i++) {
  			${"jsontable$i"} = "";
  			${"getdata$i"} = "";
  			${"data$i"} = "";
  			${"string$i"} = "";
  			${"csv_hdr$i"} = "";
  			${"csv_output$i"} = "";
		}
		
		$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
	   if(!isset($_POST['submit'])) {
	   	
	   		$mysqli = new mysqli($server, $user_name, $password, $database);			$dashboardsectionid = mysqli_real_escape_string($mysqli, $dashboardsectionid);			if($stmt = $mysqli->prepare("select * from chartsection 
			where chartsection.chartsectionid = ?")){			   $stmt->bind_param('i', $dashboardsectionid);			   $stmt->execute();			   $result = $stmt->get_result();			   if($result->num_rows > 0){			    	while($row77 = $result->fetch_assoc()){			     		$chartsectionname = $row77['chartsectionname'];
			     		$startdate = $row77['defaultstartdate'];
	   					$enddate = $row77['defaultenddate'];			    	}			   }			}			$mysqli->close();
			
	   		$time = date("Y-m-d"); 
		} 	
		else {
			$startdate = isset($_POST['startdate']) ? $_POST['startdate'] : '';
			$enddate = isset($_POST['enddate']) ? $_POST['enddate'] : '';		
			$dashboardadditionalfilter = '';
			for($i = 1; $i <= 100; $i++) {
  				${"structurefieldname$i"} = '';
  			}	   		
   			for($i = 1; $i <= 100; $i++) {
  				${"data$i"} =  '';
			}	   		
			$number10 = 1;
			$dashboardadditionalfilter = '';
	    	foreach ($_POST as $key => $value) {
	    		if($value <> "Submit" && $key <> "startdate" && $key <> "enddate") {
		        	${"structurefieldname$number10"} = $key;
		        	${"data$number10"} = $value;
		        	
		        	$mysqli = new mysqli($server, $user_name, $password, $database);					$dashboardsectionid = mysqli_real_escape_string($mysqli, $dashboardsectionid);					${"structurefieldname$number10"} = mysqli_real_escape_string($mysqli, ${"structurefieldname$number10"});					if($stmt = $mysqli->prepare("select * from $database.dashboardsectionfilter
					inner join $database.structurefield on dashboardsectionfilter.structurefieldid = structurefield.structurefieldid
					inner join $masterdatabase.structurefieldtype on structurefieldtype.structurefieldtypeid = structurefield.structurefieldtypeid
					inner join $database.structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
					where dashboardsectionfilter.chartsectionid = ? and structurefieldname = ?")){					   $stmt->bind_param('is', $dashboardsectionid, ${"structurefieldname$number10"});					   $stmt->execute();					   $result = $stmt->get_result();					   if($result->num_rows > 0){					    	while($getdatarow = $result->fetch_assoc()){					     		$fieldtypename = $getdatarow['fieldtypename'];
					        	$tablename = $getdatarow['tablename'];
					        	$structurefieldname = $getdatarow['structurefieldname'];					    	}					   }					}					$mysqli->close();

		        	if($value <> ''){
			        	if($fieldtypename == 'Text' || $fieldtypename == 'Status'){
							$dashboardadditionalfilter = $dashboardadditionalfilter." and ".$tablename.".".$structurefieldname." like '%".${"data$number10"}."%'";
						}
						if($fieldtypename == 'Table Connect' || $fieldtypename == 'Unique TableID' || ($fieldtypename == 'Checkbox' && $datasetfiltername == 1) || $fieldtypename == 'Date Select'){
							$dashboardadditionalfilter = $dashboardadditionalfilter." and ".$tablename.".".$structurefieldname." = '".${"data$number10"}."'";						
						}
					}
					$number10 = $number10+1;
		    	}
	    	}
	   		//echo $dashboardadditionalfilter;
		}
	
		if(!isset($chartsectionname)){
			$chartsectionname = "None";		
		}
?>

<!--For TL-142 Start...-->
<?php
    //Changing Charts Order Start...
    if(isset($_POST['changeOrder'])) {
        $chart_id = $_POST['chartid'];
        $chart_order = $_POST['chartorder'];
        $type = $_POST['type'];
        $chartsectionid = $_POST['chartsectionid'];
        
        $chartDataQuery = mysql_query("SELECT chartid FROM chart WHERE chartsectionid = '$chartsectionid' AND disabled = '0' ORDER BY chartorder ASC");
        
        $chartIdsArr = array();
        $counterVal = 1;
        $oldOrder = 0;
        $newOrder = 0;
        while($getRow = mysql_fetch_assoc($chartDataQuery)) {
            $c_id = $getRow['chartid'];
            if($chart_id == $c_id) {
                $oldOrder = $counterVal;
                if ($type == 'increment'){
                    $newOrder = $counterVal + 1;
                } else {
                    $newOrder = $counterVal - 1;
                } 
            }
            $updateChartOrder = mysql_query("UPDATE chart SET chartorder = '$counterVal' WHERE chartid = $c_id");
            $counterVal++;
        }
        /*$qq = "SELECT chartid FROM chart WHERE chartorder = '$newOrder' AND disabled = '0' LIMIT 1";
        echo "<pre>";
        print_r($qq);*/
        $newOrderChart = mysql_query("SELECT chartid FROM chart WHERE chartorder = '$newOrder' AND disabled = '0' LIMIT 1");
        
        
        
        while($getChart = mysql_fetch_array($newOrderChart)){
            $newOrderChartId = $getChart[0];
        }
        
        $updateNewChart = mysql_query("UPDATE chart SET chartorder = '$oldOrder' WHERE chartid = '$newOrderChartId'");
        
        $updateOldChart = mysql_query("UPDATE chart SET chartorder = '$newOrder' WHERE chartid = '$chart_id'");
    }

    //Changing Charts Order End...
?>
<!--For TL-142 END...-->


<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10" style="padding:10px;">
	<!--header bar for desktop-->
	<div class='bodyheader'>
		<h3><?php echo $langval4." - ".$chartsectionname ?></h3>
	</div>
	<div class='bodycontent'>
			
			
		<form class="form-horizontal" action='dashboard.php?dashboardsectionid=<?php echo $dashboardsectionid ?>' method="post">
			<div class="form-group">
				<label for="Start Date" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label"><?php echo $langval9 ?></label>
		    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		      	<input type="date" class="form-control" name="startdate" value="<?php echo $startdate ?>">
		    	</div>
		  		<label for="End Date" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label"><?php echo $langval10 ?></label>
		    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
		      	<input type="date" class="form-control" name="enddate" value="<?php echo $enddate ?>">
		    	</div>
		  	  	
			<?php 
			
			//get list of filters
			$allowsearch = 0;
			$mysqli = new mysqli($server, $user_name, $password, $database);			$dashboardsectionid = mysqli_real_escape_string($mysqli, $dashboardsectionid);			if($stmt = $mysqli->prepare("select * from dashboardsectionfilter
			where dashboardsectionfilter.chartsectionid = ?")){			   $stmt->bind_param('i', $dashboardsectionid);			   $stmt->execute();			   $result = $stmt->get_result();
			   $allowsearch = $result->num_rows;			   			}			$mysqli->close();
			if($allowsearch >= 1){
				echo "<br/><br/>";
				$countfilter = 1;
				$querywhere = "";
				$mysqli9 = new mysqli($server, $user_name, $password, $database);				$dashboardsectionid = mysqli_real_escape_string($mysqli9, $dashboardsectionid);				if($stmt9 = $mysqli9->prepare("select * from $database.dashboardsectionfilter
				inner join $database.structurefield on dashboardsectionfilter.structurefieldid = structurefield.structurefieldid
				inner join $masterdatabase.structurefieldtype on structurefieldtype.structurefieldtypeid = structurefield.structurefieldtypeid
				inner join $database.structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
				where dashboardsectionfilter.chartsectionid = ?")){				   $stmt9->bind_param('i', $dashboardsectionid);				   $stmt9->execute();				   $result9= $stmt9->get_result();				   if($result9->num_rows > 0){				    	while($rowfilter = $result9->fetch_assoc()){				     		${'tablename'.$countfilter}	= $rowfilter['tablename'];
							${'logiccode'.$countfilter}	= 'AND';
							${'displayname'.$countfilter}	= $rowfilter['dashboardsectionfiltername'];
							${'tableconnect'.$countfilter}	= $rowfilter['tableconnect'];
							${'tablefieldconnect'.$countfilter}	= $rowfilter['tablefieldconnect'];
							${'tablefieldconnectparent'.$countfilter}	= $rowfilter['tablefieldconnectparent'];
							${'fieldtypename'.$countfilter}= $rowfilter['fieldtypename'];
							${'structurefieldname'.$countfilter}= $rowfilter['structurefieldname'];
							$label = ${'displayname'.$countfilter};
							
							$mysqli = new mysqli($server, $user_name, $password, $database);							${'structurefieldname'.$countfilter} = mysqli_real_escape_string($mysqli, ${'structurefieldname'.$countfilter});							$languageid = mysqli_real_escape_string($mysqli, $languageid);							if($stmt = $mysqli->prepare("select * from structurefieldlanguagerecordtext 
							where structurefieldname = ? and languageid = ?")){							   $stmt->bind_param('ii', ${'structurefieldname'.$countfilter}, $languageid);							   $stmt->execute();							   $result = $stmt->get_result();							   if($result->num_rows > 0){							    	while($gettranslationrow = $result->fetch_assoc()){							     		$fieldnametranslated = $gettranslationrow['structurefieldlanguagerecordtextname'];
										$label = $fieldnametranslated;											    	}							   }							}							$mysqli->close();

							if(${'fieldtypename'.$countfilter} == 'Text' || ${'fieldtypename'.$countfilter} == 'Value' ) {								
							?>
								
								<label for="<?php echo $label ?>" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label"><?php echo $label ?></label>
							    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-bottom:10px;">
							      		<input type="text" class="form-control" name="<?php echo ${'structurefieldname'.$countfilter} ?>" value="<?php echo ${'data'.$countfilter} ?>">
							    	</div>
							  	<?php	
							}
							if(${'fieldtypename'.$countfilter} == 'Checkbox' ) {								
							?>
								
								<label for="<?php echo $label ?>" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label"><?php echo $label ?></label>
							    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-bottom:10px;">
							    	<input type='hidden' value='0' name='<?php echo ${'structurefieldname'.$countfilter} ?>'>
							    	<input type='checkbox' name='<?php echo ${'structurefieldname'.$countfilter} ?>' value='1' <?php if (${'data'.$countfilter}=='1') { ?> checked='checked' <?php ;} ?> />
							      	</div>
								  	
							<?php	
							}
							if(${'fieldtypename'.$countfilter} == 'Date Select') {								
							?>
							
								<label for="<?php echo $label ?>" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label"><?php echo $label ?></label>
						    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-bottom:10px;">
						      	<input type="date" class="form-control" name="<?php echo ${'structurefieldname'.$countfilter} ?>" value="<?php echo ${'data'.$countfilter} ?>">
								</div>
			  	  				
							<?php	
							}
							if(${'fieldtypename'.$countfilter} == 'Table Connect' ) {								
							?>
							<label for="<?php echo $label ?>" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label"><?php echo $label ?></label>
						    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="padding-bottom:10px;">
					    		  	<?php
					    		  	$tableconnect2 = strstr(${'tableconnect'.$countfilter}, '.');
									$tableconnect2 = str_replace(".", "", $tableconnect2);
									${'tableconnect'.$countfilter} = $tableconnect2;
									$idfield = $tableconnect2."id";
					    		  	$namefield = ${'tablefieldconnect'.$countfilter};
					    		  
					    		  	if(${'tablefieldconnectparent'.$countfilter}==""){
					    		  		$querytype="select * from ".${'tableconnect'.$countfilter}." where masteronly = 0 ";
					    		  		if($idfield == 'businessunitid' && $_SESSION["superadmin"] == 0){
											$querytype = $querytype."and businessunitid in (".$_SESSION["userbusinessunit"].") ";
										}
										$querytype = $querytype."order by ".${'tablefieldconnect'.$countfilter}." asc";
					    		  	}
					    		  	else {							    		  		
					    		  		$querytype="select * from ".${'tableconnect'.$countfilter}." inner join ".${'tablefieldconnectparent'.$countfilter}." where masteronly = 0 ";
					    		  		if($idfield == 'businessunitid' && $_SESSION["superadmin"] == 0){
											$querytype = $querytype."and businessunitid in (".$_SESSION["userbusinessunit"].") ";
										}
										$querytype = $querytype."order by ".${'tablefieldconnect'.$countfilter}." asc";											
									}
									
									$resulttype = mysql_query ($querytype);
						        	echo "<select class='form-control' name='".${'structurefieldname'.$countfilter}."'>";
						        	echo "<option value=''>".$langval19."</option>";
					         		while($nttype=mysql_fetch_array($resulttype)){
					             	$parenttablefieldname = explode(' ',trim(${'tablefieldconnectparent'.$countfilter}))[0];
					         			if($parenttablefieldname <> ""){
					         				$parenttablefieldname = $parenttablefieldname."name";
					         				$parenttablefieldname = $nttype[$parenttablefieldname].": ";
					         			}
					         			if (${"data$countfilter"}==$nttype[$idfield]) {
					               		echo "<option value=".$nttype[$idfield]." selected='true'>".$parenttablefieldname.$nttype[$namefield]."</option>";
					             	}
					             	else
					               		echo "<option value=".$nttype[$idfield]." >".$parenttablefieldname.$nttype[$namefield]."</option>";
					             	}
					     				echo "</select>";
					     			echo "</div>";									
								
								}
								$countfilter = $countfilter+1;
							}				    	}				   }				}			?>
			<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
	    		<div style="text-align:center;">
	      		<button type='submit' name='submit' value='Submit' class="button-primary"><?php echo $langval5 ?></button>																									
	    		</div>
	    	</div>	
	    </form>
		<br/><br/><br/>			
		<?php 
			$savedchartsectionname = "";	
			//Added chartorder in query for TL-142...
			$mysqli10 = new mysqli($server, $user_name, $password, $database);		$dashboardsectionid = mysqli_real_escape_string($mysqli10, $dashboardsectionid);		if($stmt10 = $mysqli10->prepare("select * from chart 
			inner join chartsection on chart.chartsectionid = chartsection.chartsectionid
			where chartsection.chartsectionid = ? and chart.disabled = 0 
			order by chartorder asc")){		   
				$stmt10->bind_param('i', $dashboardsectionid);		   
				$stmt10->execute();		   
				$result10 = $stmt10->get_result();
		    
		    
		    //Added $totalCharts and $chartCounter for TL-142...
		    $totalCharts = $result10->num_rows;
	        $chartCounter = 0;
	        
		    if($result10->num_rows > 0){		    	
		        while($getchartrow = $result10->fetch_assoc()){	     			$chartname = $getchartrow['chartname'];
					$chartsectionname = $getchartrow['chartsectionname'];
					$chartid = $getchartrow['chartid'];
					$sqlquery = $getchartrow['sqlquery'];
					
					//check permissions to section
					$numrows6 = 0;
					$mysqli = new mysqli($server, $user_name, $password, $database);					$userid = mysqli_real_escape_string($mysqli, $userid);					$dashboardsectionid = mysqli_real_escape_string($mysqli, $dashboardsectionid);					if($stmt = $mysqli->prepare("select * from userrole 
					inner join permissionroledashboardsection on permissionroledashboardsection.permissionroleid = userrole.permissionroleid
					where permissionroledashboardsection.disabled = '0' and userid = ? and chartsectionid = ?")){					   $stmt->bind_param('ii', $userid, $dashboardsectionid);					   $stmt->execute();					   $result = $stmt->get_result();
					   $numrows6 = $result->num_rows;				  					}					$mysqli->close();
					
					if($numrows6>0) {
						$mysqli = new mysqli($server, $user_name, $password, $database);						$chartid = mysqli_real_escape_string($mysqli, $chartid);						$languageid = mysqli_real_escape_string($mysqli, $languageid);						if($stmt = $mysqli->prepare("select * from charttranslation 
						where chartid = ? and languageid = ?")){						   $stmt->bind_param('ii', $chartid, $languageid);						   $stmt->execute();						   $result = $stmt->get_result();						   if($result->num_rows > 0){						    	while($gettranslationrow = $result->fetch_assoc()){						     		$chartname = $gettranslationrow['charttranslationname'];						    	}						   }						}						$mysqli->close();

						$datasetrecordid = $chartid;
						$datasetrecordfield = "chartid";
						$datasetrecordtable = "chart";
						
						
						//override chart content to use sqlquery field instead if it is inserted
						if($sqlquery <> ""){
							$query = $sqlquery;
							if($startdate == '0000-00-00' || $startdate == ''){
								$startdate = "1000-01-01";							
							}
							if($enddate == '0000-00-00' || $enddate == ''){
								$enddate = "9999-12-30";							
							}
							$query = str_replace("[startdate]", $startdate, $query);						
							$query = str_replace("[enddate]", $enddate, $query);						
						}
						else {
							include('dataset.php');
							$query = $query." limit 250";
						}
						//echo $query;
						
						${'getdata'.$number} = mysql_query($query);
						
						$num = mysql_num_rows(${'getdata'.$number});
						$numfields = mysql_num_fields(${'getdata'.$number});
						
						$mysqli = new mysqli($server, $user_name, $password, $database);						$chartid = mysqli_real_escape_string($mysqli, $chartid);						if($stmt = $mysqli->prepare("select charttype.charttypename
		 				from chart
						left join dataset	on chart.datasetid = dataset.datasetid		
						inner join $masterdatabase.charttype on chart.charttypeid = charttype.charttypeid	
						where chartid = ?")){						   $stmt->bind_param('i', $chartid);						   $stmt->execute();						   $result = $stmt->get_result();						   if($result->num_rows > 0){						    	while($getcharttyperow = $result->fetch_assoc()){						     		$charttype = $getcharttyperow['charttypename']; 						    	}						   }						}						$mysqli->close();

						
						$string1 = "";
						$number10 = 0;
						$number11 = 1;
				    	while ($numfields>$number10) {
				    		$value = mysql_field_name(${'getdata'.$number}, $number10);
				    		${'string'.$number11} = $value;
					      //	echo ${'string'.$number11}.":";
					      	$number10 = $number10+1;
					      	$number11 = $number11+1;
				    	}		
				    	
				    	if($charttype == "Pie" || $charttype == 'Geo Map') {
							$sth = mysql_query($query);
							
							$rows = array();
							//flag is not needed
							$flag = true;
							$table = array();
							$table['cols'] = array(
								// Labels for your chart, these represent the column titles
						    	// Note that one column is in "string" format and another one is in "number" format as pie chart only required "numbers" for calculating percentage and string will be used for column title
						    	array('label' => $string1, 'type' => 'string'),
						    	array('label' => $string2, 'type' => 'number')
							);
							
							//echo json_encode($table['cols']);
							
							$rows = array();
							while($r = mysql_fetch_assoc($sth)) {
							    $temp = array();
							    // the following line will be used to slice the Pie chart
							    if($charttype == 'Geo Map' && $r[$string1] == "UK"){
									$r[$string1] = "GB";					    
							    }
							    $temp[] = array('v' => (string) $r[$string1]); 
							
							    // Values of each slice
							    $temp[] = array('v' => (int) $r[$string2]); 
							    $rows[] = array('c' => $temp);
							}
							$table['rows'] = $rows;
							$data = $table;
						}
					    
					    if(($num+1)>21 && $charttype == "Bar"){
								$num2 = 21;				
							}	
							else {
								$num2=$num+1;				
							}
						
							if($number11 == 3){				
							 	//construct headings
						    	$num44 = 1;
						    	$data[0] = array();
						    	while($num44 < $number11){
						    		array_push($data[0], ${'string'.$num44});
						    		$num44 = $num44 + 1;
						    	}
						    	//echo json_encode($data[0]);
						    	$salt = "h3f8s9en20vj3";
						    	//construct contents
								for ($i=1; $i<$num2; $i++){
							   		/*$data[$i] = array(mysql_result(${'getdata'.$number}, $i-1, $string1),
									(int) mysql_result(${'getdata'.$number}, $i-1, $string2));*/
									
									if ($data[0][0] == 'Customer') {
    							        $customer_name = mysql_result(${'getdata'.$number}, $i-1, $string1);
                                        $customer_name = openssl_decrypt($customer_name,"AES-128-ECB",$salt);
        						        $data[$i] = array($customer_name,
    								    (int) mysql_result(${'getdata'.$number}, $i-1, $string2));
            						} else {
            						    $data[$i] = array(mysql_result(${'getdata'.$number}, $i-1, $string1),
    								    (int) mysql_result(${'getdata'.$number}, $i-1, $string2));
            						}
									$p = 3;
									if($number11 >= 4) {
										$z = 4;
										while($z<=$number11){
									     	array_push($data[$i], (int) mysql_result(${'getdata'.$number}, $i-1, ${'string'.$p}));
									     	$z = $z + 1;
									     	$p = $p + 1;
								     	}
					    			}						
								}	
							}
							
							if($number11 == 4){
								//construct headings
								$data[0] = array();
								array_push($data[0], ${'string1'});
								$col1 = ${'string2'};
								$t = 1;
								while($row23 = mysql_fetch_array(${'getdata'.$number})){
									$col2 = $row23[$col1];
									$key = array_search($col2, $data[0]);
									if($key == 0 && $t<=19){
										array_push($data[0], $col2);
										$t = $t + 1;			
									}	
								}
								
								//construct contents
						    	$col1 = ${'string1'};
						    	$col2 = ${'string2'};
						    	$col3 = ${'string3'};
						    	${'getdata2'.$number} = mysql_query($query);
						    	$i = 1;
						    	for($q = 0; $q <= 20; $q++) {
			  						${"c$q"} = 0;
			  					}	   		
			   					$savecol10 = "";
						    	$q = 1;
						    	$data[$i] = array();
										
						    	while($row24 = mysql_fetch_array(${'getdata2'.$number})){
									$col10 = $row24[$col1];
									$col11 = $row24[$col2];
									$col12 = $row24[$col3];
									settype($col12, 'int');
									if($savecol10 == ""){
										$savecol10 = $col10;
									}
									if($savecol10 <> $col10){
										if($t == 2){
											array_push($data[$i], $savecol10, $c1);
										}									
										if($t == 3){
											array_push($data[$i], $savecol10, $c1, $c2);
										}									
										if($t == 4){
											array_push($data[$i], $savecol10, $c1, $c2, $c3);
										}									
										if($t == 5){
											array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4);
										}									
										if($t == 6){
											array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5);
										}									
										if($t == 7){
											array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6);
										}									
										if($t == 8){
											array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7);
										}									
										if($t == 9){
											array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8);
										}									
										if($t == 10){
											array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9);
										}									
										if($t == 11){
											array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $c10);
										}									
										if($t == 12){
											array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $c10, $c11);
										}									
										if($t == 13){
											array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $c10, $c11, $c12);
										}									
										if($t == 14){
											array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $c10, $c11, $c12, $c13);
										}									
										if($t == 15){
											array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $c10, $c11, $c12, $c13, $c14);
										}									
										if($t == 16){
											array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $c10, $c11, $c12, $c13, $c14, $c15);
										}									
										if($t == 17){
											array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $c10, $c11, $c12, $c13, $c14, $c15, $c16);
										}									
										if($t == 18){
											array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $c10, $c11, $c12, $c13, $c14, $c15, $c16, $c17);
										}									
										if($t == 19){
											array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $c10, $c11, $c12, $c13, $c14, $c15, $c16, $c17, $c18);
										}									
										if($t >= 20){
											array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $c10, $c11, $c12, $c13, $c14, $c15, $c16, $c17, $c18, $c19);
										}	
										$savecol10 = $col10;			
										$i = $i + 1;
										for($key = 0; $key <= 20; $key++) {
			  								${"c$key"} = 0;
			  							}	   		
			   							$data[$i] = array();
									
									}
									${'getdata3'.$number} = mysql_query($query);
					    			while($m = mysql_fetch_array(${'getdata3'.$number})){
									    if($m[$col1] == $col10 && $m[$col2] == $col11){ 
									     	$key = array_search($col11, $data[0]);
											${'c'.$key} = $m[$col3];
									     	settype(${"c$key"}, 'double');				
									     
									   }
									}								
								}
								if($t == 2){
									array_push($data[$i], $savecol10, $c1);
								}									
								if($t == 3){
									array_push($data[$i], $savecol10, $c1, $c2);
								}									
								if($t == 4){
									array_push($data[$i], $savecol10, $c1, $c2, $c3);
								}									
								if($t == 5){
									array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4);
								}									
								if($t == 6){
									array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5);
								}									
								if($t == 7){
									array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6);
								}									
								if($t == 8){
									array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7);
								}									
								if($t == 9){
									array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8);
								}									
								if($t == 10){
									array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9);
								}									
								if($t == 11){
									array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $c10);
								}									
								if($t == 12){
									array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $c10, $c11);
								}									
								if($t == 13){
									array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $c10, $c11, $c12);
								}									
								if($t == 14){
									array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $c10, $c11, $c12, $c13);
								}									
								if($t == 15){
									array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $c10, $c11, $c12, $c13, $c14);
								}									
								if($t == 16){
									array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $c10, $c11, $c12, $c13, $c14, $c15);
								}									
								if($t == 17){
									array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $c10, $c11, $c12, $c13, $c14, $c15, $c16);
								}									
								if($t == 18){
									array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $c10, $c11, $c12, $c13, $c14, $c15, $c16, $c17);
								}									
								if($t == 19){
									array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $c10, $c11, $c12, $c13, $c14, $c15, $c16, $c17, $c18);
								}									
								if($t >= 20){
									array_push($data[$i], $savecol10, $c1, $c2, $c3, $c4, $c5, $c6, $c7, $c8, $c9, $c10, $c11, $c12, $c13, $c14, $c15, $c16, $c17, $c18, $c19);
								}									
																											
							}
							
							${'jsonTable'.$number} = json_encode($data);
						   //echo "<br/><br/>".${'jsonTable'.$number};
						   	
						   	
						 	?>
								<div class='col-xs-12 col-sm-6 col-md-6 col-lg-4'>
									<div style='display:inline-block;width:100%;margin-top:0px;margin-left:0px;margin-right:0px;border-top:1px solid #A0A0A0;border-left:1px solid #A0A0A0;border-right:1px solid #A0A0A0;border-bottom:1px solid #A0A0A0;'>
										<div style='width:100%;min-height:4px;background-color:#5d9ef4;'>
										</div>
										<table style='width:100%;min-height:260px;'>
											<tr>
												<td style="padding:10px;vertical-align:top;max-width:100%;">
													
													
													<?php
												   	
													
												   	if($charttype == "Line") {
												   		//echo ${'jsonTable'.$number};
															?>						   	
												   		<script type="text/javascript">
													      	google.load("visualization", "1", { packages: ["corechart"] });
													      	google.setOnLoadCallback(drawChart);
													      	function drawChart() {
													         	var jsonData = $.ajax({
													            	url: "dashboard.php",
													            	dataType: "json",
													            	async: true
													        	}).responseText;
													
													         	var data = new google.visualization.arrayToDataTable(<?=${'jsonTable'.$number}?>);
													         	var options = {chartArea:{left:40,top:20,width:"100%",height:"70%"},legend:{position:'bottom'}};
													         	var chart = new google.visualization.LineChart(
													         	document.getElementById('chart_div<?php echo $number ?>'));
													         	chart.draw(data, options);
													     		}
													     	</script>
												    		<?php
												    	}
												    
												    	if($charttype == "Bar"){
															?>		
															<script type="text/javascript">
														      	google.load("visualization", "1", { packages: ["corechart"] });
														      	google.setOnLoadCallback(drawChart);
														      	function drawChart() {
														         	var jsonData = $.ajax({
														            	url: "dashboard.php",
														            	dataType: "json",
														            	async: true
														        	}).responseText;
																	var data = new google.visualization.arrayToDataTable(<?=${'jsonTable'.$number}?>);
																	
										        var columnRange = data.getColumnRange(1);
														         	var options = {chartArea:{left:60,top:20,width:"100%",height:"70%"},legend:{position:'bottom'},isStacked: true,
														         	    hAxis: {
																				viewWindow: {
																				min: columnRange.min
																				}
																			}
														         	};
														         	var chart = new google.visualization.BarChart(
														         	document.getElementById('chart_div<?php echo $number ?>'));
														         	chart.draw(data, options);
														      	}
														     </script>		
														     					    	
												    		<?php
												    	}
														
												    								    
												    	if($charttype == "Pie"){
															?>		
															<script type="text/javascript">
																google.load("visualization", "1", { packages :["corechart"] });
																google.setOnLoadCallback(drawChart);
																function drawChart() {
																	var jsonData = $.ajax({
														            	url: "dashboard.php",
														            	dataType: "json",
														            	async: true
														        	}).responseText;
																	var data = new google.visualization.DataTable(<?=${'jsonTable'.$number}?>);
																	var options = {'chartArea':{left:20,top:10,width:"100%",height:"90%"},
																								 'titleTextStyle':{fontSize:16,color:'#5e5e5e'},											 
																								 'width':300,'legend':{position: 'bottom'},
														                       'height':200};
																	var chart = new google.visualization.PieChart(document.getElementById('chart_div<?php echo $number ?>'));
														        chart.draw(data, options);
																}
													    	</script>						    	
												    		<?php
												    	}		
												    	
												    	
												    	if($charttype == "Geo Map"){
															?>		
															<script type="text/javascript">
															google.load("visualization", "1", { packages :["corechart"] });
																google.setOnLoadCallback(drawChart);
																function drawChart() {
																	var jsonData = $.ajax({
														            	url: "dashboard.php",
														            	dataType: "json",
														            	async: true
														        	}).responseText;
																	var data = new google.visualization.DataTable(<?=${'jsonTable'.$number}?>);
																	var options = {'chartArea':{left:20,top:10,width:"100%",height:"90%"},
																								 'titleTextStyle':{fontSize:16,color:'#5e5e5e'},											 
																								 'width':300,'legend':{position: 'bottom'},
														                       'height':200};
																	var chart = new google.visualization.GeoChart(document.getElementById('chart_div<?php echo $number ?>'));
														        chart.draw(data, options);
																}
													    	</script>						    	
												    		<?php
												    	}
												    	
												    	
												    	$submiteditsql = isset($_POST['submiteditsql']) ? $_POST['submiteditsql'] : '';
												    	$editname = isset($_POST['editname']) ? $_POST['editname'] : '';
												    	$edittypeid = isset($_POST['edittypeid']) ? $_POST['edittypeid'] : '';
												    	$editsql = isset($_POST['editsql']) ? $_POST['editsql'] : '';
												    	$edit = isset($_GET['edit']) ? $_GET['edit'] : '';
	   													$edittype = isset($_GET['edittype']) ? strip_tags($_GET['edittype']) : '';	
	   													
	   													if($edit >= 1 && $chartid == $edit){
	   														if($edittype == 'sql'){
	   															
																if($submiteditsql){
																	//save the new version of the data
																	$validateedit = "";
																	if($editname == ''){
																		$validateedit = $validateedit.$langval848.". ";								
																	}	
																	if($editsql == ''){
																		$validateedit = $validateedit.$langval849.". ";								
																	}	
																	if($edittypeid == '' || $edittypeid == 0){
																		$validateedit = $validateedit.$langval850.". ";								
																	}
																	
																	if($validateedit == ""){
																		$date = date("Y-m-d");
																		$mysqli = new mysqli($server, $user_name, $password, $database);																		$editname = mysqli_real_escape_string($mysqli, $editname);																		if($stmt = $mysqli->prepare("update chart set charttypeid = ?, chartname = ?, sqlquery = ?
																		where chartid = ?")){																		   $stmt->bind_param('issi', $edittypeid,$editname, $editsql, $chartid);																		   $stmt->execute();																		   $result = $stmt->get_result();		   																		}																		$mysqli->close();	
																		
																		$url = 'dashboard.php?dashboardsectionid='.$dashboardsectionid;
											    						echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">'; 									
																	}					
																}
																else {
																	//get the existing field contents
																	$mysqli = new mysqli($server, $user_name, $password, $database);																	if($stmt = $mysqli->prepare("select * from chart
																	where chartid = ?")){																	   $stmt->bind_param('i', $chartid);																	   $stmt->execute();																	   $result = $stmt->get_result();		
																	   while ($row = $result->fetch_assoc()){
																			$chartid = $row['chartid'];
																			$editname = $row['chartname'];
																			$edittypeid = $row['charttypeid'];
																			$editsql = $row['sqlquery'];
																		}   																	}																	$mysqli->close();
																}
																
																//display editing chart using sql
	   															echo "<div style='margin-bottom:10px;font-size:12px;'><b>$langval851 $chartname </b>";
																echo "<a href='dashboard.php?dashboardsectionid=$dashboardsectionid'><input type='button' value='-' /></a><br/><br/>";
																if(isset($validateedit) && $validateedit <> "") {
																	echo "<p class='background-warning'>".$validateedit."</p>";
																}
																																	 
																?>
																
																<form class="form-horizontal" action='dashboard.php?dashboardsectionid=<?php echo $dashboardsectionid ?>&edit=<?php echo $chartid ?>&edittype=<?php echo $edittype ?>' method="post">
																	<div class="form-group">
																		<label for="Chart Name" class="col-xs-12 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval852 ?></label>
																    	<div class="col-xs-12 col-sm-8 col-md-10 col-lg-10" style="padding-bottom:10px;">
																      		<input type="text" class="form-control" name="editname" value="<?php echo $editname ?>">
																    	</div>
																  		<label for="Chart Type" class="col-xs-6 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval853 ?></label>
																    	<div class="col-xs-6 col-sm-8 col-md-10 col-lg-10">
															    			<?php
															    			$resulttype = mysql_query("select charttypeid, charttypename 
															    			from $masterdatabase.charttype 
															    			where disabled = 0");
																			echo "<select class='form-control' name='edittypeid'>";
																	      	echo "<option value=''>Select Type</option>";
															         		while($charttyperow=mysql_fetch_array($resulttype)){
															         			if ($edittypeid == $charttyperow['charttypeid']) {
															               		echo "<option value=".$charttyperow['charttypeid']." selected='true'>".$charttyperow['charttypename']."</option>";
															          		}
															          		else {
															            			echo "<option value=".$charttyperow['charttypeid']." >".$charttyperow['charttypename']."</option>";
															          		}
															             }
															  				echo "</select>";
															  				?>
																    	</div>
																    	<label for="sql" class="col-xs-12 col-sm-4 col-md-2 col-lg-2 control-label" style="margin-top:10px;"><?php echo $langval854 ?></label>
																    	<div class="col-xs-12 col-sm-8 col-md-10 col-lg-10" style="padding-top:10px;">
																      		<textarea rows="2" cols="50" class="form-control" name="editsql"><?php echo $editsql ?></textarea>
																    	</div>
																  	</div>
																  	<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
															    		<div style="text-align:center;">
															      			<button type='submit' name='submiteditsql' value='Submit' class="button-primary"><?php echo $langval5 ?></button>																									
															    		</div>
															    	</div>	
															    </form>
															    <?php
	   														}
	   														else {
	   															
	   															/*
	   															echo "<table>";
																    foreach ($_POST as $key => $value) {
																        echo "<tr>";
																        echo "<td>";
																        echo $key;
																        echo "</td>";
																        echo "<td>";
																        echo $value;
																        echo "</td>";
																        echo "</tr>";
																    }
																echo "</table>";
																*/
																

	   															$submiteditfield = isset($_POST['submiteditfield']) ? $_POST['submiteditfield'] : '';
												    			$editname = isset($_POST['editname']) ? strip_tags($_POST['editname']) : '';
										   						$edittypeid = isset($_POST['edittypeid']) ? strip_tags($_POST['edittypeid']) : '';
										   						$editsegment = isset($_POST['editsegment']) ? strip_tags($_POST['editsegment']) : '';
										   						$editsegmenttwo = isset($_POST['editsegmenttwo']) ? strip_tags($_POST['editsegmenttwo']) : '';
										   						$editvalue = isset($_POST['editvalue']) ? strip_tags($_POST['editvalue']) : '';
										   						$editgroupby = isset($_POST['editgroupby']) ? strip_tags($_POST['editgroupby']) : '';
										   						$editsortbyfield = isset($_POST['editsortbyfield']) ? strip_tags($_POST['editsortbyfield']) : '';
										   						$editsortorder = isset($_POST['editsortorder']) ? strip_tags($_POST['editsortorder']) : '';
										   						$editdatefilterfield = isset($_POST['editdatefilterfield']) ? strip_tags($_POST['editdatefilterfield']) : '';
										   						$savededitname = isset($_POST['savededitname']) ? strip_tags($_POST['savededitname']) : '';
										   						$savededittypeid = isset($_POST['savededittypeid']) ? strip_tags($_POST['savededittypeid']) : '';
										   						$savededitsegment = isset($_POST['savededitsegment']) ? strip_tags($_POST['savededitsegment']) : '';
										   						$savededitsegmenttwo = isset($_POST['savededitsegmenttwo']) ? strip_tags($_POST['savededitsegmenttwo']) : '';
										   						$savededitvalue = isset($_POST['savededitvalue']) ? strip_tags($_POST['savededitvalue']) : '';
										   						$savededitgroupby = isset($_POST['savededitgroupby']) ? strip_tags($_POST['savededitgroupby']) : '';
										   						$savededitsortbyfield = isset($_POST['savededitsortbyfield']) ? strip_tags($_POST['savededitsortbyfield']) : '';
										   						$savededitsortorder = isset($_POST['savededitsortorder']) ? strip_tags($_POST['savededitsortorder']) : '';
										   						$savededitdatefilterfield = isset($_POST['savededitdatefilterfield']) ? strip_tags($_POST['savededitdatefilterfield']) : '';
										   						
										   						if($submiteditfield){
										   							
										   							//process the editing details
																	$validateedit = "";
																	if($editname == ''){
																		$validateedit = $validateedit.$langval848.". ";								
																	}	
																	if($editsegment == ''){
																		$validateedit = $validateedit.$langval855.". ";								
																	}	
																	if($editvalue == '' && $edittypeid <> 4){
																		$validateedit = $validateedit.$langval856.". ";								
																	}	
																	if($editvalue == '' && $edittypeid <> 4){
																		$validateedit = $validateedit.$langval857.". ";								
																	}	
																	if($editsortbyfield <> '' && $editsortorder == ''){
																		$validateedit = $validateedit.$langval858." 'asc' or 'desc'. ";							
																	}	
																	if($editsortorder <> '' && ($editsortorder <> 'asc')){
																		if($editsortorder <> 'desc'){
																			$validateedit = $validateedit.$langval858." 'asc' or 'desc'. ";				
																		}				
																	}	
																	if($edittypeid == '' || $edittypeid == 0){
																		$validateedit = $validateedit.$langval850.". ";								
																	}
																	
																	if($validateedit == ""){
																		$date = date("Y-m-d");
	   																	
	   																	//process updating chart details
	   																	if($editname <> $savededitname || $edittypeid <> $savededittypeid){
	   																		$mysqli = new mysqli($server, $user_name, $password, $database);																			if($stmt = $mysqli->prepare("update chart set chartname = ?, charttypeid = ? 
																			where chartid = ?")){																			   $stmt->bind_param('sii', $editname, $edittypeid, $chartid);																			   $stmt->execute();																			   $result = $stmt->get_result();	  																			}																			$mysqli->close();
	   																	}
	   																	
	   																	//process updating dataset details
	   																	if($savededitsortbyfield <> $editsortbyfield || $savededitsortorder <> $editsortorder 
	   																	|| $savededitdatefilterfield <> $editdatefilterfield){
	   																		$mysqli = new mysqli($server, $user_name, $password, $database);																			if($stmt = $mysqli->prepare("select * from chart
																			where chartid = ?")){																			   $stmt->bind_param('i', $chartid);																			   $stmt->execute();																			   $result = $stmt->get_result();		
																			   while ($row = $result->fetch_assoc()){
																					$editdatasetid = $row['datasetid'];																																					
																				}   																			}																			$mysqli->close();
																			
																			//get fieldname of sortbyfield
																			$mysqli = new mysqli($server, $user_name, $password, $database);																			if($stmt = $mysqli->prepare("select * from structurefield
																			inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
																			where structurefieldid = ?")){																			   $stmt->bind_param('i', $editsortbyfield);																			   $stmt->execute();																			   $result = $stmt->get_result();		
																			   while ($row = $result->fetch_assoc()){
																					$editsortby2 = $row['structurefieldname'];
																					$editsortby2 = $row['tablename'].".".$editsortby2;
																				}   																			}																			$mysqli->close();	
																			
																			//add grouping to sortbynew, if sortbynew and valuenew are the same
																			if($editgroupby <> '' && $editsortbyfield == $editvalue){
																				if($editgroupby == 1){
																					$editgroupbyname = "sum";
																				}
																				else{
																					$editgroupbyname = "count";
																				}
																				$editsortby2 = $editgroupbyname."(".$editsortby2.")";
																			}
																			$editsortby2 = $editsortby2." ".$editsortorder;
																			//echo "<br/><br/>editsortby2: ".$editsortby2;
																			
																			//get fieldname of datefilterfield
																			$mysqli = new mysqli($server, $user_name, $password, $database);																			if($stmt = $mysqli->prepare("select * from structurefield
																			inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
																			where structurefieldid = ?")){																			   $stmt->bind_param('i', $editdatefilterfield);																			   $stmt->execute();																			   $result = $stmt->get_result();		
																			   while ($row = $result->fetch_assoc()){
																					$editdatefilter2 = $row['tablename'].".".$row['structurefieldname'];
																				}   																			}																			$mysqli->close();	
																			//echo "<br/><br/>editdatefilter2: ".$editdatefilter2;
																			
																			$mysqli = new mysqli($server, $user_name, $password, $database);																			if($stmt = $mysqli->prepare("update dataset set sortbyfield = ?, datefilterfield = ? 
																			where datasetid = ?")){																			   $stmt->bind_param('ssi', $editsortby2, $editdatefilter2, $editdatasetid);																			   $stmt->execute();																			   $result = $stmt->get_result();	  																			}																			$mysqli->close();
																		}
																		
																		
																		$mysqli = new mysqli($server, $user_name, $password, $database);																		if($stmt = $mysqli->prepare("select * from chart
																		where chartid = ?")){																		   $stmt->bind_param('i', $chartid);																		   $stmt->execute();																		   $result = $stmt->get_result();		
																		   while ($row = $result->fetch_assoc()){
																				$editdatasetid = $row['datasetid'];																																					
																			}   																		}																		$mysqli->close();
																			
																		//update segment 1 details
																		if($savededitsegment <> $editsegment){
																			
																			//get fieldtype of segmentnew
																			$mysqli = new mysqli($server, $user_name, $password, $database);																			if($stmt = $mysqli->prepare("select * from structurefield
																			inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
																			where structurefieldid = ?")){																			   $stmt->bind_param('i', $editsegment);																			   $stmt->execute();																			   $result = $stmt->get_result();		
																			   while ($row = $result->fetch_assoc()){
																					$editsegment1name = $row['structurefieldid'];
																					$editsegment1name = $row['structurefieldname'];
																					$editsegment1displayname = $row['displayname'];
																					$editsegment1type = $row['structurefieldtypeid'];
																					$editsegment1tablename = $row['tablename'];
																				}   																			}																			$mysqli->close();	
																			if($editsegment1type == 3 || $editsegment1type == 8){
																				$datasetaggregateid1 = 5;			
																				$format1 = "%m-%y";					
																			}
																			else {
																				$datasetaggregateid1 = 3;			
																				$format1 = "";		
																			}
																			
																			$mysqli = new mysqli($server, $user_name, $password, $database);																			if($stmt = $mysqli->prepare("update datasetcolumn 
																			set datasetcolumnname = ?, structurefieldid = ?, datasetaggregateid = ?, format = ?
																			where datasetid = ? and sortorder = 1")){																			   $stmt->bind_param('siisi', $editsegment1displayname, $editsegment, $datasetaggregateid1, 
																			   $format1, $editdatasetid);																			   $stmt->execute();																			   $result = $stmt->get_result();	  																			}																			$mysqli->close();
																			
								
																		}
																		
																		//update segment 2 details
																		if($savededitsegmenttwo <> $editsegmenttwo){
																			
																			//delete existing record if the user has removed a selection for segment 2
																			if($savededitsegmenttwo <> '' && $editsegmenttwo == ''){
																				$mysqli = new mysqli($server, $user_name, $password, $database);																				if($stmt = $mysqli->prepare("delete from datasetcolumn 
																				where datasetid = ? and sortorder = 2")){																				   $stmt->bind_param('i', $editdatasetid);																				   $stmt->execute();																				   $result = $stmt->get_result();	  																				}																				$mysqli->close();
																				
																				$mysqli = new mysqli($server, $user_name, $password, $database);																				if($stmt = $mysqli->prepare("update datasetcolumn 
																				set sortorder = 2 
																				where datasetid = ? and sortorder = 3")){																				   $stmt->bind_param('i', $editdatasetid);																				   $stmt->execute();																				   $result = $stmt->get_result();	  																				}																				$mysqli->close();
																			}
																			
																			//edit the existing record to update the segment 2 selection
																			if($savededitsegmenttwo <> '' && $editsegmenttwo <> ''){
																				//get fieldtype of segmentnew
																				$mysqli = new mysqli($server, $user_name, $password, $database);																				if($stmt = $mysqli->prepare("select * from structurefield
																				inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
																				where structurefieldid = ?")){																				   $stmt->bind_param('i', $editsegmenttwo);																				   $stmt->execute();																				   $result = $stmt->get_result();		
																				   while ($row = $result->fetch_assoc()){
																						$editsegment2name = $row['structurefieldid'];
																						$editsegment2name = $row['structurefieldname'];
																						$editsegment2displayname = $row['displayname'];
																						$editsegment2type = $row['structurefieldtypeid'];
																						$editsegment2tablename = $row['tablename'];
																					}   																				}																				$mysqli->close();	
																				if($editsegment2type == 3 || $editsegment2type == 8){
																					$datasetaggregateid2 = 5;			
																					$format2 = "%m-%y";					
																				}
																				else {
																					$datasetaggregateid2 = 3;			
																					$format2 = "";		
																				}
																				
																				//work out connectedtablename2
																				$mysqli = new mysqli($server, $user_name, $password, $database);																				if($stmt = $mysqli->prepare("select * from structurefield
																				inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
																				where structurefieldid = ?")){																				   $stmt->bind_param('i', $editsegment);																				   $stmt->execute();																				   $result = $stmt->get_result();		
																				   while ($row = $result->fetch_assoc()){
																						$editsegment1name = $row['structurefieldid'];
																						$editsegment1name = $row['structurefieldname'];
																						$editsegment1displayname = $row['displayname'];
																						$editsegment1type = $row['structurefieldtypeid'];
																						$editsegment1tablename = $row['tablename'];
																					}   																				}																				$mysqli->close();	
																				if($editsegment2tablename == $editsegment1tablename){
																					$connectedtablename2 = "";
																				}
																				else {
																					$primaryfield1 = $editsegment1tablename."id";
																					$connectedtablename2 = "inner join $editsegment1tablename on $segmentnew1tablename.$primaryfield1 = 
																					$segmentnew2tablename.$primaryfield1";					
																				}
																				
																				$mysqli = new mysqli($server, $user_name, $password, $database);																				if($stmt = $mysqli->prepare("update datasetcolumn 
																				set datasetcolumnname = ?, structurefieldid = ?, datasetaggregateid = ?, format = ?, 
																				connectedtablename = ?
																				where datasetid = ? and sortorder = 2")){																				   $stmt->bind_param('siissi', $editsegment2displayname, $editsegmenttwo, $datasetaggregateid2, 
																				   $format2, $connectedtablename2, $editdatasetid);																				   $stmt->execute();																				   $result = $stmt->get_result();	  																				}																				$mysqli->close();
																			}
																			
																			//add a new sortorder 2 field and update existing sortorder 2 field to be sortorder 3
																			if($savededitsegmenttwo == 0 && $editsegmenttwo <> ''){
																				$mysqli = new mysqli($server, $user_name, $password, $database);																				if($stmt = $mysqli->prepare("update datasetcolumn 
																				set sortorder = 3
																				where datasetid = ? and sortorder = 2")){																				   $stmt->bind_param('i', $editdatasetid);																				   $stmt->execute();																				   $result = $stmt->get_result();	  																				}																				$mysqli->close();
																				
																				//get fieldtype of segmentnew
																				$mysqli = new mysqli($server, $user_name, $password, $database);																				if($stmt = $mysqli->prepare("select * from structurefield
																				inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
																				where structurefieldid = ?")){																				   $stmt->bind_param('i', $editsegmenttwo);																				   $stmt->execute();																				   $result = $stmt->get_result();		
																				   while ($row = $result->fetch_assoc()){
																						$editsegment2name = $row['structurefieldname'];
																						$editsegment2displayname = $row['displayname'];
																						$editsegment2type = $row['structurefieldtypeid'];
																						$editsegment2tablename = $row['tablename'];
																					}   																				}																				$mysqli->close();	
																				if($editsegment2type == 3 || $editsegment2type == 8){
																					$datasetaggregateid2 = 5;			
																					$format2 = "%m-%y";					
																				}
																				else {
																					$datasetaggregateid2 = 3;			
																					$format2 = "";		
																				}
																				
																				//work out connectedtablename2
																				$mysqli = new mysqli($server, $user_name, $password, $database);																				if($stmt = $mysqli->prepare("select * from structurefield
																				inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
																				where structurefieldid = ?")){																				   $stmt->bind_param('i', $editsegment);																				   $stmt->execute();																				   $result = $stmt->get_result();		
																				   while ($row = $result->fetch_assoc()){
																						$editsegment1name = $row['structurefieldid'];
																						$editsegment1name = $row['structurefieldname'];
																						$editsegment1displayname = $row['displayname'];
																						$editsegment1type = $row['structurefieldtypeid'];
																						$editsegment1tablename = $row['tablename'];
																					}   																				}																				$mysqli->close();	
																				if($editsegment2tablename == $editsegment1tablename){
																					$connectedtablename2 = "";
																				}
																				else {
																					$primaryfield1 = $editsegment1tablename."id";
																					$connectedtablename2 = "inner join $editsegment1tablename on $segmentnew1tablename.$primaryfield1 = 
																					$segmentnew2tablename.$primaryfield1";					
																				}
																			
																				//run record addition
																				$mysqli = new mysqli($server, $user_name, $password, $database);																				if($stmt = $mysqli->prepare("insert into datasetcolumn 
																				(datasetcolumnname, disabled, datecreated, masteronly, datasetid, structurefieldid, datasetaggregateid, format, sortorder, 
																				connectedtablename) 
																				values (?, 0, '$date', 0, ?, ?, ?, ?, 2, ?)")){																				   $stmt->bind_param('siiiss', $editsegment2displayname, $editdatasetid, $editsegmenttwo, $datasetaggregateid2, 
																				   $format2, $connectedtablename2);																				   $stmt->execute();																				   $result = $stmt->get_result();		   																				}																				$mysqli->close();	
																			}
																		}
																		
																		//update value/groupby details
																		if($savededitvalue <> $editvalue || $savededitgroupby <> $editgroupby){
																			$p = 2;
																			if($editsegmenttwo <> 0){
																				$p = 3;																			
																			}
																			
																			//get fieldtype of value
																			$mysqli = new mysqli($server, $user_name, $password, $database);																			if($stmt = $mysqli->prepare("select * from structurefield
																			inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
																			where structurefieldid = ?")){																			   $stmt->bind_param('i', $editvalue);																			   $stmt->execute();																			   $result = $stmt->get_result();		
																			   while ($row = $result->fetch_assoc()){
																					$editsegment1name = $row['structurefieldid'];
																					$editsegment1name = $row['structurefieldname'];
																					$editsegment1displayname = $row['displayname'];
																					$editsegment1type = $row['structurefieldtypeid'];
																					$editsegment1tablename = $row['tablename'];
																				}   																			}																			$mysqli->close();	
																			
																			//get fieldtype of valuenew
																			$mysqli = new mysqli($server, $user_name, $password, $database);																			if($stmt = $mysqli->prepare("select * from structurefield
																			inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
																			where structurefieldid = ?")){																			   $stmt->bind_param('i', $editvalue);																			   $stmt->execute();																			   $result = $stmt->get_result();		
																			   while ($row = $result->fetch_assoc()){
																					$editvaluename = $row['structurefieldname'];
																					$editvaluedisplayname = $row['displayname'];
																					$editvaluetype = $row['structurefieldtypeid'];
																					$editvaluetablename = $row['tablename'];
																				}   																			}																			$mysqli->close();	
																			
																			//work out connectedtablename2
																			if($editvaluetablename == $editsegment1tablename){
																				$connectedtablename3 = "";
																			}
																			else {
																				$primaryfield1 = $editsegment1tablename."id";
																				$connectedtablename3 = "inner join $editsegment1tablename on $editsegment1tablename.$primaryfield1 = 
																				$editvaluetablename.$primaryfield1";					
																			}
								
																			$mysqli = new mysqli($server, $user_name, $password, $database);																			if($stmt = $mysqli->prepare("update datasetcolumn 
																			set datasetcolumnname = ?, structurefieldid = ?, datasetaggregateid = ?, 
																			connectedtablename = ?
																			where datasetid = ? and sortorder = ?")){																			   $stmt->bind_param('siisii', $editvaluedisplayname, $editvalue, $editgroupby, 
																			   $connectedtablename3, $editdatasetid, $p);																			   $stmt->execute();																			   $result = $stmt->get_result();	  																			}																			$mysqli->close();
																			
																		
																		}
	   																	
	   																	$url = 'dashboard.php?dashboardsectionid='.$dashboardsectionid;
	    																echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">'; 	
	   																}
	   															}
	   															else {	
																
																	//get the existing field contents
																	$mysqli = new mysqli($server, $user_name, $password, $database);																	if($stmt = $mysqli->prepare("select * from chart
																	where chartid = ?")){																	   $stmt->bind_param('i', $chartid);																	   $stmt->execute();																	   $result = $stmt->get_result();		
																	   while ($row = $result->fetch_assoc()){
																			$chartid = $row['chartid'];
																			$editname = $row['chartname'];
																			$edittypeid = $row['charttypeid'];
																			$datasetid = $row['datasetid'];
																		}   																	}																	$mysqli->close();
																	
																	$editsortbyfield = 0;		
																	$editdatefilterfield = 0;		
																	$editsegment = 0;		
																	$editsegmenttwo = 0;		
																	$editvalue = 0;		
																	$editgroupby = 0;		
																	$editsortorder = '';		
																	
																	if($datasetid <> 0){
																		//get sort by field and date filter field and convert to field id's
																		$mysqli = new mysqli($server, $user_name, $password, $database);																		if($stmt = $mysqli->prepare("select * from dataset
																		where datasetid = ?")){																		   $stmt->bind_param('i', $datasetid);																		   $stmt->execute();																		   $result = $stmt->get_result();		
																		   while ($row = $result->fetch_assoc()){
																				$editsortbyfield = $row['sortbyfield'];
																				$editdatefilterfield = $row['datefilterfield'];
																				if(strpos($editsortbyfield, " asc") !== false) {
																				    $editsortorder = 'asc';
																				}
																				if(strpos($editsortbyfield, " desc") !== false) {
																				    $editsortorder = 'desc';
																				}
																				$editsortbyfield = str_replace("count(", "", $editsortbyfield);
																				$editsortbyfield = str_replace(")", "", $editsortbyfield);
																				$editsortbyfield = str_replace("sum(", "", $editsortbyfield);
																				$editsortbyfield = str_replace("asc", "", $editsortbyfield);
																				$editsortbyfield = str_replace("desc", "", $editsortbyfield);
																				//echo "<br/>editsortbyfield: ".$editsortbyfield;
																				//echo "<br/>editdatefilterfield: ".$editdatefilterfield;																			
																			}   																		}																		$mysqli->close();
																		
																		if($editsortbyfield <> ''){
																			$tablename10 = '';
																			$fieldname10 = '';
																			if (strpos($editsortbyfield, '.') !== false) {
																				$explode = explode(".", $editsortbyfield);
																				$tablename10 = $explode[0];
																				$fieldname10 = $explode[1];
																			}
																			if($tablename10 <> '' && $fieldname10 <> ''){
																				$mysqli = new mysqli($server, $user_name, $password, $database);																				if($stmt = $mysqli->prepare("select * from structurefield 
																				inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
																				where tablename = ? and structurefieldname = ?")){																				   $stmt->bind_param('ss', $tablename10, $fieldname10);																				   $stmt->execute();																				   $result = $stmt->get_result();		
																				   while ($row = $result->fetch_assoc()){
																						$editsortbyfield = $row['structurefieldid'];
																						//echo "<br/>editsortbyfield: ".$editsortbyfield;																	
																					}   																				}																																					$mysqli->close();
																			}
																		}	
																		
																		if($editdatefilterfield <> ''){
																			$explode = explode(".", $editdatefilterfield);
																			$tablename10 = $explode[0];
																			$fieldname10 = $explode[1];
																			if($tablename10 <> '' && $fieldname10 <> ''){
																				$mysqli = new mysqli($server, $user_name, $password, $database);																				if($stmt = $mysqli->prepare("select * from structurefield 
																				inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
																				where tablename = ? and structurefieldname = ?")){																				   $stmt->bind_param('ss', $tablename10, $fieldname10);																				   $stmt->execute();																				   $result = $stmt->get_result();		
																				   while ($row = $result->fetch_assoc()){
																						$editdatefilterfield = $row['structurefieldid'];
																						//echo "<br/>editdatefilterfield: ".$editdatefilterfield;																	
																					}   																				}																																					$mysqli->close();
																			}
																		}
																		
																		$numfields = 0;																	
																		$mysqli = new mysqli($server, $user_name, $password, $database);																		if($stmt = $mysqli->prepare("select count(datasetcolumnid) as 'numfields' from datasetcolumn 
																		where datasetid = ? and hidecolumn = 0 and disabled = 0 ")){																		   $stmt->bind_param('i', $datasetid);																		   $stmt->execute();																		   $result = $stmt->get_result();		
																		   while ($row = $result->fetch_assoc()){
																				$numfields = $row['numfields'];																																		
																			}   																		}																																			$mysqli->close();	
																		
																		if($numfields == 1 || $numfields == 2 || $numfields == 3){
																			$mysqli = new mysqli($server, $user_name, $password, $database);																			if($stmt = $mysqli->prepare("select * from datasetcolumn 
																			where datasetid = ? and sortorder = 1")){																			   $stmt->bind_param('i', $datasetid);																			   $stmt->execute();																			   $result = $stmt->get_result();		
																			   while ($row = $result->fetch_assoc()){
																					$editsegment = $row['structurefieldid'];										
																				}   																			}																																				$mysqli->close();	
																		}
																		
																		if($numfields == 2){
																			$mysqli = new mysqli($server, $user_name, $password, $database);																			if($stmt = $mysqli->prepare("select * from datasetcolumn 
																			where datasetid = ? and sortorder = 2")){																			   $stmt->bind_param('i', $datasetid);																			   $stmt->execute();																			   $result = $stmt->get_result();		
																			   while ($row = $result->fetch_assoc()){
																					$editvalue = $row['structurefieldid'];
																					$editgroupby = $row['datasetaggregateid'];													
																				}   																			}																																				$mysqli->close();	
																		}
																		
																		if($numfields == 3){
																			$mysqli = new mysqli($server, $user_name, $password, $database);																			if($stmt = $mysqli->prepare("select * from datasetcolumn 
																			where datasetid = ? and sortorder = 2")){																			   $stmt->bind_param('i', $datasetid);																			   $stmt->execute();																			   $result = $stmt->get_result();		
																			   while ($row = $result->fetch_assoc()){
																					$editsegmenttwo = $row['structurefieldid'];													
																				}   																			}																																				$mysqli->close();	
																			$mysqli = new mysqli($server, $user_name, $password, $database);																			if($stmt = $mysqli->prepare("select * from datasetcolumn 
																			where datasetid = ? and sortorder = 3")){																			   $stmt->bind_param('i', $datasetid);																			   $stmt->execute();																			   $result = $stmt->get_result();		
																			   while ($row = $result->fetch_assoc()){
																					$editvalue = $row['structurefieldid'];
																					$editgroupby = $row['datasetaggregateid'];													
																				}   																			}																																				$mysqli->close();	
																		}
							   						
																	}
																}
																
																$savededitname = $editname;
										   						$savededittypeid = $edittypeid;
										   						$savededitsegment = $editsegment;
										   						$savededitsegmenttwo = $editsegmenttwo;
										   						$savededitvalue = $editvalue;
										   						$savededitgroupby = $editgroupby;
										   						$savededitsortbyfield = $editsortbyfield;
										   						$savededitsortorder = $editsortorder;
										   						$savededitdatefilterfield = $editdatefilterfield;
												   						
																//display editing chart using fields	 
																echo "<div style='margin-bottom:10px;font-size:12px;'><b>$langval851 $chartname </b>";
																echo "<a href='dashboard.php?dashboardsectionid=$dashboardsectionid'><input type='button' value='-' /></a><br/><br/>";
																if(isset($validateedit) && $validateedit <> "") {
																	echo "<p class='background-warning'>".$validateedit."</p>";
																}
																
																?>
																
																<form class="form-horizontal" action='dashboard.php?dashboardsectionid=<?php echo $dashboardsectionid ?>&edit=<?php echo $chartid ?>&edittype=fields' method="post">
																	<div class="form-group">
																		<label for="Chart Name" class="col-xs-12 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval852 ?></label>
																    	<div class="col-xs-12 col-sm-8 col-md-10 col-lg-10" style="padding-bottom:10px;">
																      		<input type="text" class="form-control" name="editname" value="<?php echo $editname ?>">
																    	</div>
																  		<label for="Chart Type" class="col-xs-6 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval853 ?></label>
																    	<div class="col-xs-6 col-sm-8 col-md-10 col-lg-10" style="padding-bottom:10px;">
															    			<?php
															    			$resulttype = mysql_query("select charttypeid, charttypename 
															    			from $masterdatabase.charttype 
															    			where disabled = 0");
																			echo "<select class='form-control' name='edittypeid'>";
																	      	echo "<option value=''>Select Type</option>";
															         		while($charttyperow=mysql_fetch_array($resulttype)){
															         			if ($edittypeid == $charttyperow['charttypeid']) {
															               		echo "<option value=".$charttyperow['charttypeid']." selected='true'>".$charttyperow['charttypename']."</option>";
															          		}
															          		else {
															            			echo "<option value=".$charttyperow['charttypeid']." >".$charttyperow['charttypename']."</option>";
															          		}
															             }
															  				echo "</select>";
															  				?>
																    	</div>
																    	
																    	<label for="Segment" class="col-xs-6 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval859 ?></label>
																    	<div class="col-xs-6 col-sm-8 col-md-10 col-lg-10" style="padding-bottom:10px;">
															    			<?php
															    			$resulttype1 = mysql_query("select structurefieldid, structurefieldname, tablename from structurefield
															    			inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
															    			where structurefield.disabled = 0 
															    			order by structurefunctionname asc, structurefieldname asc");
																			echo "<select class='form-control' name='editsegment'>";
																	      	echo "<option value=''>Select Field</option>";
															         		while($segmentnewrow=mysql_fetch_array($resulttype1)){
															         			if ($editsegment == $segmentnewrow['structurefieldid']) {
															               		echo "<option value=".$segmentnewrow['structurefieldid']." selected='true'>".$segmentnewrow['tablename'].".".$segmentnewrow['structurefieldname']."</option>";
															          		}
															          		else {
															            			echo "<option value=".$segmentnewrow['structurefieldid']." >".$segmentnewrow['tablename'].".".$segmentnewrow['structurefieldname']."</option>";
															          		}
															             }
															  				echo "</select>";
															  				?>
																    	</div>
																    	
																    	<label for="Segment 2" class="col-xs-6 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval859 ?> 2</label>
																    	<div class="col-xs-6 col-sm-8 col-md-10 col-lg-10" style="padding-bottom:10px;">
															    			<?php
															    			$resulttype2 = mysql_query("select structurefieldid, structurefieldname, tablename from structurefield
															    			inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
															    			where structurefield.disabled = 0 
															    			order by structurefunctionname asc, structurefieldname asc");
																			echo "<select class='form-control' name='editsegmenttwo'>";
																	      	echo "<option value=''>Select Field</option>";
															         		while($segmenttwonewrow=mysql_fetch_array($resulttype2)){
															         			if ($editsegmenttwo == $segmenttwonewrow['structurefieldid']) {
															               		echo "<option value=".$segmenttwonewrow['structurefieldid']." selected='true'>".$segmenttwonewrow['tablename'].".".$segmenttwonewrow['structurefieldname']."</option>";
															          		}
															          		else {
															            			echo "<option value=".$segmenttwonewrow['structurefieldid']." >".$segmenttwonewrow['tablename'].".".$segmenttwonewrow['structurefieldname']."</option>";
															          		}
															             }
															  				echo "</select>";
															  				?>
																    	</div>
																    	
																    	<label for="Value" class="col-xs-6 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval860 ?></label>
																    	<div class="col-xs-6 col-sm-8 col-md-10 col-lg-10" style="padding-bottom:10px;">
															    			<?php
															    			$resulttype3 = mysql_query("select structurefieldid, structurefieldname, tablename from structurefield
															    			inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
															    			where structurefield.disabled = 0 
															    			order by structurefunctionname asc, structurefieldname asc");
																			echo "<select class='form-control' name='editvalue'>";
																	      	echo "<option value=''>Select Field</option>";
															         		while($valuenewrow=mysql_fetch_array($resulttype3)){
															         			if ($editvalue == $valuenewrow['structurefieldid']) {
															               		echo "<option value=".$valuenewrow['structurefieldid']." selected='true'>".$valuenewrow['tablename'].".".$valuenewrow['structurefieldname']."</option>";
															          		}
															          		else {
															            			echo "<option value=".$valuenewrow['structurefieldid']." >".$valuenewrow['tablename'].".".$valuenewrow['structurefieldname']."</option>";
															          		}
															             }
															  				echo "</select>";
															  				?>
																    	</div>
																    	
																    	<label for="Group By" class="col-xs-6 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval861 ?></label>
																    	<div class="col-xs-6 col-sm-8 col-md-10 col-lg-10" style="padding-bottom:10px;">
															    			<?php
															    			$resulttype4 = mysql_query("select datasetaggregateid, datasetaggregatename from $masterdatabase.datasetaggregate 
															    			where disabled = 0 and datasetaggregatename in ('Sum', 'Count')");
																			echo "<select class='form-control' name='editgroupby'>";
																	      	echo "<option value=''>Select Field</option>";
															         		while($groupbynewrow=mysql_fetch_array($resulttype4)){
															         			if ($editgroupby == $groupbynewrow['datasetaggregateid']) {
															               		echo "<option value=".$groupbynewrow['datasetaggregateid']." selected='true'>".$groupbynewrow['datasetaggregatename']."</option>";
															          		}
															          		else {
															            			echo "<option value=".$groupbynewrow['datasetaggregateid']." >".$groupbynewrow['datasetaggregatename']."</option>";
															          		}
															             }
															  				echo "</select>";
															  				?>
																    	</div>
																    	
																    	<label for="Sort By" class="col-xs-6 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval862 ?></label>
																    	<div class="col-xs-6 col-sm-8 col-md-10 col-lg-10" style="padding-bottom:10px;">
															    			<?php
															    			$resulttype4 = mysql_query("select structurefieldid, structurefieldname, tablename from structurefield
															    			inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
															    			where structurefield.disabled = 0 
															    			order by structurefunctionname asc, structurefieldname asc");
																			echo "<select class='form-control' name='editsortbyfield'>";
																	      	echo "<option value=''>Select Field</option>";
															         		while($sortbynewrow=mysql_fetch_array($resulttype4)){
															         			if ($editsortbyfield == $sortbynewrow['structurefieldid']) {
															               		echo "<option value=".$sortbynewrow['structurefieldid']." selected='true'>".$sortbynewrow['tablename'].".".$sortbynewrow['structurefieldname']."</option>";
															          		}
															          		else {
															            			echo "<option value=".$sortbynewrow['structurefieldid']." >".$sortbynewrow['tablename'].".".$sortbynewrow['structurefieldname']."</option>";
															          		}
															             }
															  				echo "</select>";
															  				?>
																    	</div>
																    	
																    	<label for="Sort By Order" class="col-xs-12 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval863 ?></label>
																    	<div class="col-xs-12 col-sm-8 col-md-10 col-lg-10" style="padding-bottom:10px;">
																      		<input type="text" class="form-control" name="editsortorder" value="<?php echo $editsortorder ?>">
																    	</div>
																  		
																    	<label for="Date Filter" class="col-xs-6 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval868 ?></label>
																    	<div class="col-xs-6 col-sm-8 col-md-10 col-lg-10" style="padding-bottom:10px;">
															    			<?php
															    			$resulttype4 = mysql_query("select structurefieldid, structurefieldname, tablename from structurefield
															    			inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
															    			where structurefield.disabled = 0 
															    			order by structurefunctionname asc, structurefieldname asc");
																			echo "<select class='form-control' name='editdatefilterfield'>";
																	      	echo "<option value=''>Select Field</option>";
															         		while($datefilternewrow=mysql_fetch_array($resulttype4)){
															         			if ($editdatefilterfield == $datefilternewrow['structurefieldid']) {
															               		echo "<option value=".$datefilternewrow['structurefieldid']." selected='true'>".$datefilternewrow['tablename'].".".$datefilternewrow['structurefieldname']."</option>";
															          		}
															          		else {
															            			echo "<option value=".$datefilternewrow['structurefieldid']." >".$datefilternewrow['tablename'].".".$datefilternewrow['structurefieldname']."</option>";
															          		}
															             }
															  				echo "</select>";
															  				?>
																    	</div>
																    	
																  	</div>
																  	<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
															    		<div style="text-align:center;">
																      		<input type="hidden" value="<?php echo $editname ?>" name="savededitname" id="savededitname" />
																      		<input type="hidden" value="<?php echo $edittypeid ?>" name="savededittypeid" />
																      		<input type="hidden" value="<?php echo $editsegment ?>" name="savededitsegment" />
																      		<input type="hidden" value="<?php echo $editsegmenttwo ?>" name="savededitsegmenttwo" />
																      		<input type="hidden" value="<?php echo $editvalue ?>" name="savededitvalue" />
																      		<input type="hidden" value="<?php echo $editgroupby ?>" name="savededitgroupby" />
																      		<input type="hidden" value="<?php echo $editsortbyfield ?>" name="savededitsortbyfield" />
																      		<input type="hidden" value="<?php echo $editsortorder ?>" name="savededitsortorder" />
																      		<input type="hidden" value="<?php echo $editdatefilterfield ?>" name="savededitdatefilterfield" />									   																					      		
															      			<button type='submit' name='submiteditfield' value='Submit' class="button-primary"><?php echo $langval5 ?></button>																									
															    		</div>
															    	</div>	
															    </form>
																<?php
																echo "</div>";
															}
	   													
	   													}			
	   													else {  
	   														//display the chart 																    		
													    	?>
													    	<div style='margin-bottom:10px;font-size:12px;'><b><?php echo $chartname; ?></b></div>
													    	<div style='margin-bottom:10px;font-size:12px;'>
														    	<form name="export" action="export.php?pagetype=<?php echo "Dashboard: ".$chartsectionname ?>" method="post" style="display:inline;">
																	
																	  	<input type="submit" value="=">
											  	
											  							<!--For TL-143 Start...-->
																	  	<?php if($_SESSION['accesslevel'] == "Master" || (($_SESSION['accesslevel'] == "Tenant" || $_SESSION['accesslevel'] == "Business") && $chartid >= 1000000)){ ?>
											  							<!--For TL-143 End...-->
																		  	<input type="button" value="?" class="showSingle" target="<?php echo $chartid ?>" />
																		  	<a href="dashboard.php?dashboardsectionid=<?php echo $dashboardsectionid ?>&edit=<?php echo $chartid ?>&edittype=sql"  style="color:black;">
																		  		<input type="button" value="<?php echo $langval864 ?>" />
																		  	</a>
																		  	<a href="dashboard.php?dashboardsectionid=<?php echo $dashboardsectionid ?>&edit=<?php echo $chartid ?>&edittype=fields"  style="color:black;">
																		  		<input type="button" value="<?php echo $langval865 ?>" />
																		  	</a>
												  	
								 											<div id="div<?php echo $chartid ?>" class="targetDiv" style="display:none;"><?php echo $query ?></div>
							 											<?php }?>
																		<input type="hidden" value="<?php echo $query; ?>" name='query'>
																	
																</form>
																<!--For TL-142 Start...-->
																<?php if($_SESSION['accesslevel'] == "Master" || (($_SESSION['accesslevel'] == "Tenant" || $_SESSION['accesslevel'] == "Business") && $dashboardsectionid >= 1000000)){ ?>
																	<form method="POST" style="display:inline">
																	    <?php 
																  	
							    									  	    if ($chartCounter != 0) {
							    								  	    ?>
								    								  	    <input type="hidden" name="chartorder" value="<?php echo $chartorder; ?>" />
								    								  	    
								    								  	    <input type="hidden" name="chartid" value="<?php echo $chartid; ?>" />
								    								  	    
								    								  	    <input type="hidden" name="chartsectionid" value="<?php echo $dashboardsectionid; ?>" />
								    								  	    
								    								  	    <input type="hidden" name="type" value="decrement" />
								    								  	    
								    									  	<a href="#" style="color:black;">
								    									  		<input type="submit" name="changeOrder" value="<"/>
								    									  	</a>
							    									  	<?php
							    									  	    } 
							    								  	    ?>
															  	    </form>  
								  	    
															  	    <form method="POST" style="display:inline">
							    								  	    <?php 
							    									  	
							    									  	    if($chartCounter != $totalCharts- 1) {
							    								  	    ?>
								    								  	    <input type="hidden" name="chartorder" value="<?php echo $chartorder; ?>" />
								    								  	    
								    								  	    <input type="hidden" name="chartid" value="<?php echo $chartid; ?>" />
								    								  	    
								    								  	    <input type="hidden" name="chartsectionid" value="<?php echo $dashboardsectionid; ?>" />
								    								  	    
								    								  	    <input type="hidden" name="type" value="increment" />
								    								  	    
								    									  	<a href="#" style="color:black;">
								    									  		<input type="submit" name="changeOrder" value=">"/>
								    									  	</a>
							    									  	<?php
							    									  	    } 
							    								  	    ?>
																	</form>
																<?php }?>
																<!--For TL-142 END...-->
															</div>
													
														<div id="chart_div<?php echo $number ?>">
															<?php
													   		if($charttype == "Data Grid"){
																echo "<table style='width:100px;'>";
																$rowquery = mysql_query($query);
																$hdr[$i] = "";
														      	$countoffields = 0;
														      	echo "<thead>";
																for($p = 0; $p < mysql_num_fields($rowquery); $p++) {
																   	$field_info = mysql_fetch_field($rowquery, $p);
																   	$hdr[$i] = $field_info->name;
																   	echo "<td style='font-weight:bold;font-size:10px;border:1px solid #A0A0A0;padding-left:5px;padding-right:5px;padding-top:2px;padding-bottom:2px;width:30px;'>".$hdr[$i]."</td>";
																   	$countoffields = $countoffields+1;
																}
																echo "</thead>";
																// Print the data
																$output[$i] = "";
																$maxrows = 1;
																while($row = mysql_fetch_row($rowquery)) {
																	if($maxrows <= 10){
																	    echo "<tr>";
																		$counter3 = 1;
																	    foreach($row as $_column) {
																	      	$output[$i] = $_column;
																	      	if($counter3 == 1){
																	      		echo "<td style='font-size:10px;border:1px solid #A0A0A0;padding-left:5px;padding-right:5px;padding-top:1px;padding-bottom:1px;max-width:130px;white-space:nowrap;overflow:hidden;'>".$output[$i]."</td>";
																	      	}
																	     	else {
																	      		echo "<td style='font-size:10px;border:1px solid #A0A0A0;padding-left:5px;padding-right:5px;padding-top:1px;padding-bottom:1px;min-width:60px;white-space:nowrap;overflow:hidden;'>".$output[$i]."</td>";
																	    	}
																	    	$counter3 = $counter3 +1;	 
																	    }	
																	    echo "</tr>";			
																	    $maxrows = $maxrows + 1;		
																    }															    
																}
																echo "</table>";								     
													    	}
													   ?>
													   	</div>
													<?php
													}
													?>

												</td>
											</tr>
										</table>
										
									</div>
								</div>
							<div class='xs-12 hidden-sm hidden-md hidden-lg' style = "clear:both;"></div>
							<?php
					
						//reset variables		
						$number = $number+1;
						$data = "";
						//$jsonTable = json_decode($data);
						//unset($jsonTable);
					}
					
		            $chartCounter++;
					    	}
					    	
					}		}
		    	
		    	//add an extra box to add a new chart, if relevant

				/**** For TL-143 Start ****/
				$pagename = $_SERVER['REQUEST_URI'];
				if($_SESSION['accesslevel'] == "Master" || (($_SESSION['accesslevel'] == "Tenant" || $_SESSION['accesslevel'] == "Business"))){
				/**** For TL-143 End ****/	
 					$addnew = isset($_GET['addnew']) ? $_GET['addnew'] : '';
 					echo "<div class='xs-12 hidden-sm hidden-md hidden-lg' style = 'clear:both;'></div>";   				
 					echo "<div class='col-xs-12 col-sm-6 col-md-6 col-lg-4'>";
					echo "<div style='display:inline-block;width:100%;margin-top:0px;margin-left:0px;margin-right:0px;border-top:1px solid #A0A0A0;border-left:1px solid #A0A0A0;border-right:1px solid #A0A0A0;border-bottom:1px solid #A0A0A0;'>";
					echo "<div style='width:100%;min-height:4px;background-color:#5d9ef4;'>";
					echo "</div>";
					echo "<table style='width:100%;min-height:260px;'>";
					echo "<tr>";
					echo "<td style='padding:10px;vertical-align:top;max-width:100%;'>";
					
					$pagename2 = str_replace("&addnew=1", "", $pagename);					
					$pagename2 = str_replace("&addnew=2", "", $pagename2);			
					
					//option to add new chart
					if($addnew == '') {
						echo "<div class = 'col-xs-2 col-sm-2 col-md-2 col-lg-2'>";	
						echo "</div>";											
						echo "<div class = 'col-xs-9 col-sm-9 col-md-9 col-lg-9'>";											
						echo "<br/><br/><br/><br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;<a href = '$pagename&addnew=1'>$langval866</a>";											
						echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;<a href = '$pagename&addnew=2'>$langval867</a>";											
						echo "</div>";											
					}
					
					//add new chart using sql
					if($addnew == 1){
						
						$submitnew = isset($_POST['submitnew']) ? $_POST['submitnew'] : '';
   						$newname = isset($_POST['newname']) ? strip_tags($_POST['newname']) : '';
   						$newtypeid = isset($_POST['newtypeid']) ? strip_tags($_POST['newtypeid']) : '';
   						$newsql = isset($_POST['newsql']) ? strip_tags($_POST['newsql']) : '';
   						
   						if($submitnew){
							$validatenew = "";
							if($newname == ''){
								$validatenew = $validatenew.$langval848.". ";								
							}	
							if($newsql == ''){
								$validatenew = $validatenew.$langval849.". ";								
							}	
							if($newtypeid == '' || $newtypeid == 0){
								$validatenew = $validatenew.$langval850.". ";								
							}
							
							if($validatenew == ""){
								$date = date("Y-m-d");
								$mysqli = new mysqli($server, $user_name, $password, $database);								$newname = mysqli_real_escape_string($mysqli, $newname);								if($stmt = $mysqli->prepare("insert into chart 
								(chartname, disabled, datecreated, masteronly, chartsectionid, datasetid, charttypeid, chartdescription, sqlquery) 
								values (?, 0, '$date', 0, ?, 0, ?, '', ?)")){								   $stmt->bind_param('siis', $newname, $dashboardsectionid, $newtypeid, $newsql);								   $stmt->execute();								   $result = $stmt->get_result();		   								}								$mysqli->close();	
								
								$url = 'dashboard.php?dashboardsectionid='.$dashboardsectionid;
	    						echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">'; 									
							}								
							
   						}
  
						echo "<div style='margin-bottom:10px;font-size:12px;'><b>$langval866 </b>";
						echo "<a href='$pagename2'><input type='button' value='-' /></a><br/><br/>";
						if(isset($validatenew) && $validatenew <> "") {
							echo "<p class='background-warning'>".$validatenew."</p>";
						} 
						?>
						
						<form class="form-horizontal" action='dashboard.php?dashboardsectionid=<?php echo $dashboardsectionid ?>&addnew=1' method="post">
							<div class="form-group">
								<label for="Chart Name" class="col-xs-12 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval852 ?></label>
						    	<div class="col-xs-12 col-sm-8 col-md-10 col-lg-10" style="padding-bottom:10px;">
						      		<input type="text" class="form-control" name="newname" value="<?php echo $newname ?>">
						    	</div>
						  		<label for="Chart Type" class="col-xs-6 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval853 ?></label>
						    	<div class="col-xs-6 col-sm-8 col-md-10 col-lg-10">
					    			<?php
					    			$resulttype = mysql_query("select charttypeid, charttypename 
					    			from $masterdatabase.charttype 
					    			where disabled = 0");
									echo "<select class='form-control' name='newtypeid'>";
							      	echo "<option value=''>Select Type</option>";
					         		while($charttyperow=mysql_fetch_array($resulttype)){
					         			if ($newtypeid == $charttyperow['charttypeid']) {
					               		echo "<option value=".$charttyperow['charttypeid']." selected='true'>".$charttyperow['charttypename']."</option>";
					          		}
					          		else {
					            			echo "<option value=".$charttyperow['charttypeid']." >".$charttyperow['charttypename']."</option>";
					          		}
					             }
					  				echo "</select>";
					  				?>
						    	</div>
						    	<label for="sql" class="col-xs-12 col-sm-4 col-md-2 col-lg-2 control-label" style="margin-top:10px;"><?php echo $langval854 ?></label>
						    	<div class="col-xs-12 col-sm-8 col-md-10 col-lg-10" style="padding-top:10px;">
						      		<textarea rows="2" cols="50" class="form-control" name="newsql"><?php echo $newsql ?></textarea>
						    	</div>
						  	</div>
						  	<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
					    		<div style="text-align:center;">
					      			<button type='submit' name='submitnew' value='Submit' class="button-primary"><?php echo $langval5 ?></button>																									
					    		</div>
					    	</div>	
					    </form>
						<?php
						echo "</div>";
					}
					
					//add new chart using fields
					if($addnew == 2){
						
						$submitnew = isset($_POST['submitnew']) ? $_POST['submitnew'] : '';
   						$newname = isset($_POST['newname']) ? strip_tags($_POST['newname']) : '';
   						$newtypeid = isset($_POST['newtypeid']) ? strip_tags($_POST['newtypeid']) : '';
   						$segmentnew = isset($_POST['segmentnew']) ? strip_tags($_POST['segmentnew']) : '';
   						$segmenttwonew = isset($_POST['segmenttwonew']) ? strip_tags($_POST['segmenttwonew']) : '';
   						$valuenew = isset($_POST['valuenew']) ? strip_tags($_POST['valuenew']) : '';
   						$groupbynew = isset($_POST['groupbynew']) ? strip_tags($_POST['groupbynew']) : '';
   						$sortbynew = isset($_POST['sortbynew']) ? strip_tags($_POST['sortbynew']) : '';
   						$sortbyordernew = isset($_POST['sortbyordernew']) ? strip_tags($_POST['sortbyordernew']) : '';
   						$datefilternew = isset($_POST['datefilternew']) ? strip_tags($_POST['datefilternew']) : '';
   						
   						if($submitnew){
							$validatenew = "";
							if($newname == ''){
								$validatenew = $validatenew.$langval848.". ";								
							}	
							if($segmentnew == ''){
								$validatenew = $validatenew.$langval855.". ";								
							}	
							if($valuenew == '' && $newtypeid <> 4){
								$validatenew = $validatenew.$langval856.". ";								
							}	
							if($valuenew == '' && $newtypeid <> 4){
								$validatenew = $validatenew.$langval857.". ";								
							}	
							if($sortbynew <> '' && $sortbyordernew == ''){
								$validatenew = $validatenew.$langval858." 'asc' or 'desc'. ";							
							}	
							if($sortbyordernew <> '' && ($sortbyordernew <> 'asc')){
								if($sortbyordernew <> 'desc'){
									$validatenew = $validatenew.$langval858." 'asc' or 'desc'. ";				
								}				
							}	
							if($newtypeid == '' || $newtypeid == 0){
								$validatenew = $validatenew.$langval850.". ";								
							}
							
							if($validatenew == ""){
								$date = date("Y-m-d");
								
								//get fieldname of sortbyfield
								$mysqli = new mysqli($server, $user_name, $password, $database);								$newname = mysqli_real_escape_string($mysqli, $newname);								if($stmt = $mysqli->prepare("select * from structurefield
								inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
								where structurefieldid = ?")){								   $stmt->bind_param('i', $sortbynew);								   $stmt->execute();								   $result = $stmt->get_result();		
								   while ($row = $result->fetch_assoc()){
										$sortbynew2 = $row['structurefieldname'];
										$sortbynew2 = $row['tablename'].".".$sortbynew2;
									}   								}								$mysqli->close();	
								
								//add grouping to sortbynew, if sortbynew and valuenew are the same
								if($groupbynew <> '' && $sortbynew == $valuenew){
									if($groupbynew == 1){
										$groupbynewname = "sum";
									}
									else{
										$groupbynewname = "count";
									}
									$sortbynew2 = $groupbynewname."(".$sortbynew2.")";
								}
								
								//get fieldname of datefilterfield
								//FOR TL-143...
								$datefilternew2 = '';
								$mysqli = new mysqli($server, $user_name, $password, $database);								
								$newname = mysqli_real_escape_string($mysqli, $newname);								
								if($stmt = $mysqli->prepare("select * from structurefield
								inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
								where structurefieldid = ?")){								   
									$stmt->bind_param('i', $datefilternew);								   
									$stmt->execute();								   
									$result = $stmt->get_result();		
								   	while ($row = $result->fetch_assoc()){
										$datefilternew2 = $row['tablename'].".".$row['structurefieldname'];
									}   								
								}								
								$mysqli->close();	
								
								
								//add dataset
								$sortbynew2 = $sortbynew2." ".$sortbyordernew;
								$name2 = $chartsectionname." - ".$newname;
								$mysqli = new mysqli($server, $user_name, $password, $database);								
								$newname = mysqli_real_escape_string($mysqli, $newname);								
								if($stmt = $mysqli->prepare("insert into dataset 
								(datasetname, disabled, datecreated, masteronly, sortbyfield, datefilterfield) 
								values (?, 0, '$date', 0, ?, ?)")){								   
									$stmt->bind_param('sss', $name2, $sortbynew2, $datefilternew2);								   
									$stmt->execute();								   
									$datasetid = $stmt->insert_id; 								
								}								
								$mysqli->close();	
								
								//get fieldtype of segmentnew
								$mysqli = new mysqli($server, $user_name, $password, $database);								
								$newname = mysqli_real_escape_string($mysqli, $newname);								
								if($stmt = $mysqli->prepare("select * from structurefield
								inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
								where structurefieldid = ?")){								   
									$stmt->bind_param('i', $segmentnew);								   
									$stmt->execute();								   
									$result = $stmt->get_result();		
								   	while ($row = $result->fetch_assoc()){
										$segmentnew1name = $row['structurefieldname'];
										$segmentnew1displayname = $row['displayname'];
										$segmentnew1type = $row['structurefieldtypeid'];
										$segmentnew1tablename = $row['tablename'];
									}   								
								}								
								$mysqli->close();	

								if($segmentnew1type == 3 || $segmentnew1type == 8){
									$datasetaggregateid1 = 5;			
									$format1 = "%m-%y";					
								}
								else {
									$datasetaggregateid1 = 3;			
									$format1 = "";		
								}
								
								//add datasetcolumn1
								$segmentnew2tablename = "";
								$mysqli = new mysqli($server, $user_name, $password, $database);								
								$newname = mysqli_real_escape_string($mysqli, $newname);								
								if($stmt = $mysqli->prepare("insert into datasetcolumn 
								(datasetcolumnname, disabled, datecreated, masteronly, datasetid, structurefieldid, datasetaggregateid, format, sortorder, 
								connectedtablename) 
								values (?, 0, '$date', 0, ?, ?, ?, ?, 1, '')")){								   
									$stmt->bind_param('siiis', $segmentnew1displayname, $datasetid, $segmentnew, $datasetaggregateid1, $format1);								   
									$stmt->execute();								   
									$result = $stmt->get_result();		   								
								}								
								$mysqli->close();	
								
								//add datasetcolumn2
								$p = 2;
								if($segmenttwonew <> '' && $segmenttwonew <> 0){
									
									//get fieldtype of segmentnew
									$mysqli = new mysqli($server, $user_name, $password, $database);									
									$newname = mysqli_real_escape_string($mysqli, $newname);									
									if($stmt = $mysqli->prepare("select * from structurefield
									inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
									where structurefieldid = ?")){									   
										$stmt->bind_param('i', $segmenttwonew);									   
										$stmt->execute();									   
										$result = $stmt->get_result();		
									   	while ($row = $result->fetch_assoc()){
											$segmentnew2name = $row['structurefieldname'];
											$segmentnew2displayname = $row['displayname'];
											$segmentnew2type = $row['structurefieldtypeid'];
											$segmentnew2tablename = $row['tablename'];
										}   									
									}									
									$mysqli->close();	
									if($segmentnew2type == 3 || $segmentnew2type == 8){
										$datasetaggregateid2 = 5;			
										$format2 = "%m-%y";					
									}
									else {
										$datasetaggregateid2 = 3;			
										$format2 = "";		
									}
									
									//work out connectedtablename2
									if($segmentnew2tablename == $segmentnew1tablename){
										$connectedtablename2 = "";
									}
									else {
										$primaryfield1 = $segmentnew1tablename."id";
										$connectedtablename2 = "inner join $segmentnew2tablename on $segmentnew2tablename.$primaryfield1 = $segmentnew1tablename.$primaryfield1";					
									}
								
									//run record addition
									$mysqli = new mysqli($server, $user_name, $password, $database);									
									$newname = mysqli_real_escape_string($mysqli, $newname);									
									if($stmt = $mysqli->prepare("insert into datasetcolumn 
									(datasetcolumnname, disabled, datecreated, masteronly, datasetid, structurefieldid, datasetaggregateid, format, sortorder, 
									connectedtablename) 
									values (?, 0, '$date', 0, ?, ?, ?, ?, 2, ?)")){									   
										$stmt->bind_param('siiiss', $segmentnew2displayname, $datasetid, $segmenttwonew, $datasetaggregateid2, $format2, $connectedtablename2);									   
										$stmt->execute();									   
										$result = $stmt->get_result();		   									
									}									
									$mysqli->close();	
									$p = 3;
								}
								
								//get fieldtype of valuenew
								$mysqli = new mysqli($server, $user_name, $password, $database);								
								$newname = mysqli_real_escape_string($mysqli, $newname);								
								if($stmt = $mysqli->prepare("select * from structurefield
								inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
								where structurefieldid = ?")){								   
									$stmt->bind_param('i', $valuenew);								   
									$stmt->execute();								  
								 	$result = $stmt->get_result();		
								   	while ($row = $result->fetch_assoc()){
										$valuenewname = $row['structurefieldname'];
										$valuenewdisplayname = $row['displayname'];
										$valuenewtype = $row['structurefieldtypeid'];
										$valuenewtablename = $row['tablename'];
									}   								
								}								
								$mysqli->close();	
								
								//work out connectedtablename2
								if($valuenewtablename == $segmentnew1tablename){
									$connectedtablename3 = "";
								}
								else {
									$primaryfield1 = $segmentnew1tablename."id";
									//FOR TL-143...
									/*$connectedtablename3 = "inner join $segmentnew1tablename on $segmentnew1tablename.$primaryfield1 = 
									$valuenewtablename.$primaryfield1";	*/	
									$connectedtablename3 = "inner join $valuenewtablename on $valuenewtablename.$primaryfield1 = $segmentnew1tablename.$primaryfield1
									";			
								}
									
								//add datasetcolumn3
								$mysqli = new mysqli($server, $user_name, $password, $database);								
								$newname = mysqli_real_escape_string($mysqli, $newname);								
								if($stmt = $mysqli->prepare("insert into datasetcolumn 
								(datasetcolumnname, disabled, datecreated, masteronly, datasetid, structurefieldid, datasetaggregateid, format, sortorder, 
								connectedtablename) 
								values (?, 0, '$date', 0, ?, ?, ?, '', ?, ?)")){								   
									$stmt->bind_param('siiiis', $valuenewdisplayname, $datasetid, $valuenew, $groupbynew, 
								   $p, $connectedtablename3);								   
									$stmt->execute();								   
									$result = $stmt->get_result();		   								
								}								
								$mysqli->close();	
								
							
										
								//add chart
								$mysqli = new mysqli($server, $user_name, $password, $database);								$newname = mysqli_real_escape_string($mysqli, $newname);								if($stmt = $mysqli->prepare("insert into chart 
								(chartname, disabled, datecreated, masteronly, chartsectionid, datasetid, charttypeid, chartdescription, sqlquery) 
								values (?, 0, '$date', 0, ?, ?, ?, '', '')")){								   $stmt->bind_param('siii', $newname, $dashboardsectionid, $datasetid, $newtypeid);								   $stmt->execute();								   $result = $stmt->get_result();		   								}								$mysqli->close();		
								
								$url = 'dashboard.php?dashboardsectionid='.$dashboardsectionid;
	    						echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">'; 
								
								
							}								
							
   						}
  
						echo "<div style='margin-bottom:10px;font-size:12px;'><b>Add New Chart Using Fields </b>";
						echo "<a href='$pagename2'><input type='button' value='-' /></a><br/><br/>";
						if(isset($validatenew) && $validatenew <> "") {
							echo "<p class='background-warning'>".$validatenew."</p>";
						} 
						?>
						
						<form class="form-horizontal" action='dashboard.php?dashboardsectionid=<?php echo $dashboardsectionid ?>&addnew=2' method="post">
							<div class="form-group">
								<label for="Chart Name" class="col-xs-12 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval852 ?></label>
						    	<div class="col-xs-12 col-sm-8 col-md-10 col-lg-10" style="padding-bottom:10px;">
						      		<input type="text" class="form-control" name="newname" value="<?php echo $newname ?>">
						    	</div>
						  		<label for="Chart Type" class="col-xs-6 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval853 ?></label>
						    	<div class="col-xs-6 col-sm-8 col-md-10 col-lg-10" style="padding-bottom:10px;">
					    			<?php
					    			$resulttype = mysql_query("select charttypeid, charttypename 
					    			from $masterdatabase.charttype 
					    			where disabled = 0");
									echo "<select class='form-control' name='newtypeid'>";
							      	echo "<option value=''>Select Type</option>";
					         		while($charttyperow=mysql_fetch_array($resulttype)){
					         			if ($newtypeid == $charttyperow['charttypeid']) {
					               		echo "<option value=".$charttyperow['charttypeid']." selected='true'>".$charttyperow['charttypename']."</option>";
					          		}
					          		else {
					            			echo "<option value=".$charttyperow['charttypeid']." >".$charttyperow['charttypename']."</option>";
					          		}
					             }
					  				echo "</select>";
					  				?>
						    	</div>
						    	
						    	<label for="Segment" class="col-xs-6 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval859 ?></label>
						    	<div class="col-xs-6 col-sm-8 col-md-10 col-lg-10" style="padding-bottom:10px;">
					    			<?php
					    			$resulttype1 = mysql_query("select structurefieldid, structurefieldname, tablename from structurefield
					    			inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
					    			where structurefield.disabled = 0 
					    			order by structurefunctionname asc, structurefieldname asc");
									echo "<select class='form-control' name='segmentnew'>";
							      	echo "<option value=''>Select Field</option>";
					         		while($segmentnewrow=mysql_fetch_array($resulttype1)){
					         			if ($segmentnew == $segmentnewrow['structurefieldid']) {
					               		echo "<option value=".$segmentnewrow['structurefieldid']." selected='true'>".$segmentnewrow['tablename'].".".$segmentnewrow['structurefieldname']."</option>";
					          		}
					          		else {
					            			echo "<option value=".$segmentnewrow['structurefieldid']." >".$segmentnewrow['tablename'].".".$segmentnewrow['structurefieldname']."</option>";
					          		}
					             }
					  				echo "</select>";
					  				?>
						    	</div>
						    	
						    	<label for="Segment 2" class="col-xs-6 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval859 ?> 2</label>
						    	<div class="col-xs-6 col-sm-8 col-md-10 col-lg-10" style="padding-bottom:10px;">
					    			<?php
					    			$resulttype2 = mysql_query("select structurefieldid, structurefieldname, tablename from structurefield
					    			inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
					    			where structurefield.disabled = 0 
					    			order by structurefunctionname asc, structurefieldname asc");
									echo "<select class='form-control' name='segmenttwonew'>";
							      	echo "<option value=''>Select Field</option>";
					         		while($segmenttwonewrow=mysql_fetch_array($resulttype2)){
					         			if ($segmenttwonew == $segmenttwonewrow['structurefieldid']) {
					               		echo "<option value=".$segmenttwonewrow['structurefieldid']." selected='true'>".$segmenttwonewrow['tablename'].".".$segmenttwonewrow['structurefieldname']."</option>";
					          		}
					          		else {
					            			echo "<option value=".$segmenttwonewrow['structurefieldid']." >".$segmenttwonewrow['tablename'].".".$segmenttwonewrow['structurefieldname']."</option>";
					          		}
					             }
					  				echo "</select>";
					  				?>
						    	</div>
						    	
						    	<label for="Value" class="col-xs-6 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval860 ?></label>
						    	<div class="col-xs-6 col-sm-8 col-md-10 col-lg-10" style="padding-bottom:10px;">
					    			<?php
					    			$resulttype3 = mysql_query("select structurefieldid, structurefieldname, tablename from structurefield
					    			inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
					    			where structurefield.disabled = 0 
					    			order by structurefunctionname asc, structurefieldname asc");
									echo "<select class='form-control' name='valuenew'>";
							      	echo "<option value=''>Select Field</option>";
					         		while($valuenewrow=mysql_fetch_array($resulttype3)){
					         			if ($valuenew == $valuenewrow['structurefieldid']) {
					               		echo "<option value=".$valuenewrow['structurefieldid']." selected='true'>".$valuenewrow['tablename'].".".$valuenewrow['structurefieldname']."</option>";
					          		}
					          		else {
					            			echo "<option value=".$valuenewrow['structurefieldid']." >".$valuenewrow['tablename'].".".$valuenewrow['structurefieldname']."</option>";
					          		}
					             }
					  				echo "</select>";
					  				?>
						    	</div>
						    	
						    	<label for="Group By" class="col-xs-6 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval861 ?></label>
						    	<div class="col-xs-6 col-sm-8 col-md-10 col-lg-10" style="padding-bottom:10px;">
					    			<?php
					    			$resulttype4 = mysql_query("select datasetaggregateid, datasetaggregatename from $masterdatabase.datasetaggregate 
					    			where disabled = 0 and datasetaggregatename in ('Sum', 'Count')");
									echo "<select class='form-control' name='groupbynew'>";
							      	echo "<option value=''>Select Field</option>";
					         		while($groupbynewrow=mysql_fetch_array($resulttype4)){
					         			if ($groupbynew == $groupbynewrow['datasetaggregateid']) {
					               		echo "<option value=".$groupbynewrow['datasetaggregateid']." selected='true'>".$groupbynewrow['datasetaggregatename']."</option>";
					          		}
					          		else {
					            			echo "<option value=".$groupbynewrow['datasetaggregateid']." >".$groupbynewrow['datasetaggregatename']."</option>";
					          		}
					             }
					  				echo "</select>";
					  				?>
						    	</div>
						    	
						    	<label for="Sort By" class="col-xs-6 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval862 ?></label>
						    	<div class="col-xs-6 col-sm-8 col-md-10 col-lg-10" style="padding-bottom:10px;">
					    			<?php
					    			$resulttype4 = mysql_query("select structurefieldid, structurefieldname, tablename from structurefield
					    			inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
					    			where structurefield.disabled = 0 
					    			order by structurefunctionname asc, structurefieldname asc");
									echo "<select class='form-control' name='sortbynew'>";
							      	echo "<option value=''>Select Field</option>";
					         		while($sortbynewrow=mysql_fetch_array($resulttype4)){
					         			if ($sortbynew == $sortbynewrow['structurefieldid']) {
					               		echo "<option value=".$sortbynewrow['structurefieldid']." selected='true'>".$sortbynewrow['tablename'].".".$sortbynewrow['structurefieldname']."</option>";
					          		}
					          		else {
					            			echo "<option value=".$sortbynewrow['structurefieldid']." >".$sortbynewrow['tablename'].".".$sortbynewrow['structurefieldname']."</option>";
					          		}
					             }
					  				echo "</select>";
					  				?>
						    	</div>
						    	
						    	<label for="Sort By Order" class="col-xs-12 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval863 ?></label>
						    	<div class="col-xs-12 col-sm-8 col-md-10 col-lg-10" style="padding-bottom:10px;">
						      		<input type="text" class="form-control" name="sortbyordernew" value="<?php echo $sortbyordernew ?>">
						    	</div>
						  		
						    	<label for="Date Filter" class="col-xs-6 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval868 ?></label>
						    	<div class="col-xs-6 col-sm-8 col-md-10 col-lg-10" style="padding-bottom:10px;">
					    			<?php
					    			$resulttype4 = mysql_query("select structurefieldid, structurefieldname, tablename from structurefield
					    			inner join structurefunction on structurefunction.structurefunctionid = structurefield.structurefunctionid
					    			where structurefield.disabled = 0 
					    			order by structurefunctionname asc, structurefieldname asc");
									echo "<select class='form-control' name='datefilternew'>";
							      	echo "<option value=''>Select Field</option>";
					         		while($datefilternewrow=mysql_fetch_array($resulttype4)){
					         			if ($datefilternew == $datefilternewrow['structurefieldid']) {
					               		echo "<option value=".$datefilternewrow['structurefieldid']." selected='true'>".$datefilternewrow['tablename'].".".$datefilternewrow['structurefieldname']."</option>";
					          		}
					          		else {
					            			echo "<option value=".$datefilternewrow['structurefieldid']." >".$datefilternewrow['tablename'].".".$datefilternewrow['structurefieldname']."</option>";
					          		}
					             }
					  				echo "</select>";
					  				?>
						    	</div>
						    	
						  	</div>
						  	<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
					    		<div style="text-align:center;">
					      			<button type='submit' name='submitnew' value='Submit' class="button-primary"><?php echo $langval5 ?></button>																									
					    		</div>
					    	</div>	
					    </form>
						<?php
						echo "</div>";
					}
					
					echo "</td>";
					echo "</tr>";
					echo "</table>";	
					echo "</div>";
					echo "</div>";
	
 				}		   		$mysqli10->close();
		
		
			
		
		
		?>
		
		
	</div>
	</div>
	
		<br/><br/><br/>
</div>
	
	<?php include_once ('footer.php'); ?>
</body>
