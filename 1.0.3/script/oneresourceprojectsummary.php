<?php

//LANGUAGE COLLECTION SECTION
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (4,5,9,10,19,582,583,584,585,586,587,588,589,590,591,592,593,594,595,596,597,598,599,600,601,602,603,
604,605,606,607,608,610,677,678,679,680,681,682,683,684,685,686,687,688,689,670,671,672,673,674,675,676,677,678,679,680,
681,682,683,684,685,686,687,688,689,690,691,692,693,694,695,696,697,698,699,700,701,702,703,704,705,706,707,708,709,710,
711,712,713,714,805,806,807,821,829,830,831,832,833,834,835,836,837,838,839,840,841,842,843,844,845,846,847,848,849,850)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}

//FILTERS
$pagename = $_SERVER['REQUEST_URI'];
$pagename = str_replace("&", "xxxxxxxxxx", $pagename);								

$displaytype = isset($_GET['displaytype']) ? $_GET['displaytype'] : '';
$projectid = isset($_POST['projectid']) ? $_POST['projectid'] : '';
$projid = isset($_GET['projid']) ? $_GET['projid'] : '';
$startdate = isset($_POST['startdate']) ? $_POST['startdate'] : '';
$start = isset($_GET['start']) ? $_GET['start'] : '';
$enddate = isset($_POST['enddate']) ? $_POST['enddate'] : '';
$end = isset($_GET['end']) ? $_GET['end'] : '';
//echo "<br/>resourceid: ".$resourceid;
//echo "<br/>startdate: ".$startdate;
//echo "<br/>enddate: ".$enddate;

if($projid <> ""){
	$projectid = $projid;
}
if($start <> ""){
	$startdate = $start;
}
if($end <> ""){
	$enddate = $end;
}
?>
<form class="form-horizontal" action='view.php?viewid=51' method="post" enctype="multipart/form-data">
	<div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
		<div class="form-group">
		 	<label for="Project" class="col-xs-6 col-sm-2 col-md-1 col-lg-1 control-label"><?php echo $langval829 ?></label>
	    	<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
    			<?php
    			$resulttype = mysql_query("select distinct project.projectid, projectname from project
				inner join resourceallocation on project.projectid = resourceallocation.projectid    			
    			where project.disabled = 0");
				echo "<select class='form-control' name='projectid'>";
		      	echo "<option value=''>$langval830</option>";
         		while($projectrow=mysql_fetch_array($resulttype)){
         			if ($projectid == $projectrow['projectid']) {
               		echo "<option value=".$projectrow['projectid']." selected='true'>".$projectrow['projectname']."</option>";
          		}
          		else {
            			echo "<option value=".$projectrow['projectid']." >".$projectrow['projectname']."</option>";
          		}
             }
  				echo "</select>";
  				?>
	    	</div>
	  		<label for="Start Date" class="col-xs-6 col-sm-1 col-md-1 col-lg-1 control-label"><?php echo $langval805 ?></label>
	    	<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
	      	<input type="date" class="form-control" name="startdate" value="<?php echo $startdate ?>">
	    	</div>
	  		<label for="End Date" class="col-xs-6 col-sm-1 col-md-1 col-lg-1 control-label"><?php echo $langval806 ?></label>
	    	<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
	      	<input type="date" class="form-control" name="enddate" value="<?php echo $enddate ?>">
	    	</div>
	    	<div class="col-md-3 col-lg-3 col-sm-2 col-xs-12">
    		<div style="text-align:center;">
      			<button type='submit' name='submit' value='Submit' class="button-primary"><?php echo $langval807 ?></button>		
 			</div>
    	</div>
	  	</div>
  	</div>
</form>	

<?php 
echo "<br/><br/>";

if($projectid <> ''){
	
	//DISPLAY PROJECT ALLOCATIONS
	$getallocation = "select resourceallocationid, resourceallocationname, resourceallocation.resourceid, resourcename, projectname, 
	startdatetime, enddatetime, resourceallocation.progresspercent, resourceallocationstatusname from resourceallocation 
	inner join resourceallocationstatus on resourceallocationstatus.resourceallocationstatusid = resourceallocation.resourceallocationstatusid
	inner join resource on resource.resourceid = resourceallocation.resourceid
	inner join project on project.projectid = resourceallocation.projectid
	where resourceallocation.projectid = $projectid";
	$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
	if($startdate <> '' || $enddate <> '') {
		//add dates to query
		if($startdate <> "" && $enddate <> ""){
			$getallocation = $getallocation." and enddatetime <= '".$enddate."' and startdatetime >= '".$startdate."'";
		}	
		if($startdate <> "" && $enddate == ""){
			$getallocation = $getallocation." and startdatetime >= '".$startdate."'";
		}	
		if($startdate == "" && $enddate <> ""){
			$getallocation = $getallocation." and enddatetime <= '".$enddate."'";
		}	
	}
	$getallocation = $getallocation." order by startdatetime asc limit 500";
	//echo $getallocation;
	$getallocation = mysql_query($getallocation);
	
	
	echo "<h2>$langval831</h2>";
	echo "<a class='button-primary' href='pageadd.php?pagetype=resourceallocation&pagename=".$pagename."'>$langval832</a>&nbsp;&nbsp;&nbsp;&nbsp;";
	echo "<b>".$langval587."</b><a href='view.php?viewid=51&projid=".$projectid."&start=".$startdate."&end=".$enddate."&displaytype=calendar'>$langval821</a>";
	echo " | <a href='view.php?viewid=51&projid=".$projectid."&start=".$startdate."&end=".$enddate."&displaytype=tasklist'>".$langval589."</a>";
	echo "<br/><br/>";
			
	if($displaytype == "tasklist" || $displaytype == ""){
		if(mysql_num_rows($getallocation) >= 1){		
			echo "<table class='table table-bordered'>";
			echo "<thead><tr>";
			echo "<th>$langval833</th>";
			echo "<th>$langval834</th>";
			echo "<th>$langval835</th>";
			echo "<th>$langval836</th>";
			echo "<th>$langval837</th>";
			echo "<th>$langval838</th>";
			echo "<th>$langval839</th>";
			echo "<th>$langval840</th>";
			echo "</tr></thead>";
		}
		while($row23 = mysql_fetch_array($getallocation)){
			$resourceallocationid = $row23['resourceallocationid'];
			$resourceallocationname = $row23['resourceallocationname'];
			$resourcename = $row23['resourcename'];
			$progresspercent = $row23['progresspercent'];
			$status = $row23['resourceallocationstatusname'];
			$startdate1 = $row23['startdatetime'];
			$enddate1 = $row23['enddatetime'];
			echo "<tr>";	
			echo "<td>".$resourceallocationid."</td>";
			echo "<td>".$resourceallocationname."</td>";
			echo "<td>".$resourcename."</td>";
			echo "<td>".$progresspercent."</td>";
			echo "<td>".$status."</td>";
			echo "<td>".$startdate1."</td>";
			echo "<td>".$enddate1."</td>";
			echo "<td><a href='pageedit.php?pagetype=resourceallocation&rowid=".$resourceallocationid."&pagename=".$pagename."'>".$langval599."</a></td>";			
			echo "</tr>";	
		}
		if(mysql_num_rows($getallocation) >= 1){
			echo "</table>";
		}
	}
	
	if($displaytype == "calendar"){
		//include('fullcalendar-3.2.0/demos/default.html');
		?>
		<script type="text/javascript" >
		
			$(document).ready(function() {
		
				$('#calendar2').fullCalendar({
					defaultDate: '<?php echo $date ?>',
					editable: true,
					eventLimit: true, // allow "more" link when too many events
					events: [ 
					<?php
					$events = "";
					$title = '';
					$start = '';
					$end = '';
					$url = '';
					$colour = '';
					while ($row5 = mysql_fetch_assoc($getallocation)){
						$title1 = $row5['resourceallocationname'].' - '.$row5['resourcename'];
						$start1 = $row5['startdatetime'];
						$end1 = $row5['enddatetime'];
						$colour1 = 'blue';
						$url1 = "pageedit.php?pagetype=resourceallocation&rowid=".$row5['resourceallocationid'];
						$tip1 = $row5['resourceallocationname'].' - '.$row5['resourcename'];
						$events = $events."{";
						$events = $events."title: '".$title1."',";
						$events = $events."start: '".$start1."',";
						$events = $events."end: '".$end1."',";
						$events = $events."tip: '".$tip1."',";
						$events = $events."color: '".$colour1."',";
						$events = $events."url: '".$url1."'";
						$events = $events."},";
					}
					$events = substr(trim($events), 0, -1);
				 	echo $events;
					?>
						
					],
					eventRender: function(event, element) {
			            element.attr('title', event.tip);
			        },
					eventClick: function(event) {
		        	if (event.url) {
		            	window.open(event.url);
		            	return false;
		        	}
		    	}
		    	});
				
			});
		
		</script>
		<div id='calendar2' style="width:100%;height:auto;overflow:hidden;z-index:-1"></div>
		<link href='fullcalendar-3.2.0/fullcalendar.min.css' rel='stylesheet' />
		<link href='fullcalendar-3.2.0/fullcalendar.print.min.css' rel='stylesheet' media='print' />
		<script src='fullcalendar-3.2.0/lib/jquery.min.js'></script>
		<script src='fullcalendar-3.2.0/lib/moment.min.js'></script>
		<script src='fullcalendar-3.2.0/fullcalendar.min.js'></script>
		<br/><br/>
		<?php
	}		
	
	
	//DISPLAY TASK ALLOCATIONS
	$getallocation = "select resourceallocationid, resourceallocationname, resourceallocation.resourceid, resourcename, taskname, 
	startdatetime, enddatetime, resourceallocation.progresspercent, resourceallocationstatusname from resourceallocation 
	inner join resourceallocationstatus on resourceallocationstatus.resourceallocationstatusid = resourceallocation.resourceallocationstatusid
	inner join resource on resource.resourceid = resourceallocation.resourceid
	inner join task on task.taskid = resourceallocation.taskid
	inner join project on task.oneprojectid = project.projectid
	where project.projectid = $projectid";
	$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
	if($startdate <> '' || $enddate <> '') {
		//add dates to query
		if($startdate <> "" && $enddate <> ""){
			$getallocation = $getallocation." and enddatetime <= '".$enddate."' and startdatetime >= '".$startdate."'";
		}	
		if($startdate <> "" && $enddate == ""){
			$getallocation = $getallocation." and startdatetime >= '".$startdate."'";
		}	
		if($startdate == "" && $enddate <> ""){
			$getallocation = $getallocation." and enddatetime <= '".$enddate."'";
		}	
	}
	$getallocation = $getallocation." order by startdatetime asc limit 500";
	//echo $getallocation;
	$getallocation = mysql_query($getallocation);
	
	
	echo "<h2>$langval841</h2>";
	echo "<a class='button-primary' href='pageadd.php?pagetype=resourceallocation&pagename=".$pagename."'>$langval832</a>&nbsp;&nbsp;&nbsp;&nbsp;";
	echo "<b>".$langval587."</b><a href='view.php?viewid=51&projid=".$projectid."&start=".$startdate."&end=".$enddate."&displaytype=calendar'>$langval821</a>";
	echo " | <a href='view.php?viewid=51&projid=".$projectid."&start=".$startdate."&end=".$enddate."&displaytype=tasklist'>".$langval589."</a>";
	echo "<br/><br/>";
			
	if($displaytype == "tasklist" || $displaytype == ""){
		if(mysql_num_rows($getallocation) >= 1){		
			echo "<table class='table table-bordered'>";
			echo "<thead><tr>";
			echo "<th>$langval833</th>";
			echo "<th>$langval834</th>";
			echo "<th>$langval842</th>";
			echo "<th>$langval835</th>";
			echo "<th>$langval836</th>";
			echo "<th>$langval837</th>";
			echo "<th>$langval838</th>";
			echo "<th>$langval839</th>";
			echo "<th>$langval840</th>";
			echo "</tr></thead>";
		}
		while($row23 = mysql_fetch_array($getallocation)){
			$resourceallocationid = $row23['resourceallocationid'];
			$resourceallocationname = $row23['resourceallocationname'];
			$taskname = $row23['taskname'];
			$resourcename = $row23['resourcename'];
			$progresspercent = $row23['progresspercent'];
			$status = $row23['resourceallocationstatusname'];
			$startdate1 = $row23['startdatetime'];
			$enddate1 = $row23['enddatetime'];
			echo "<tr>";	
			echo "<td>".$resourceallocationid."</td>";
			echo "<td>".$resourceallocationname."</td>";
			echo "<td>".$taskname."</td>";
			echo "<td>".$resourcename."</td>";
			echo "<td>".$progresspercent."</td>";
			echo "<td>".$status."</td>";
			echo "<td>".$startdate1."</td>";
			echo "<td>".$enddate1."</td>";
			echo "<td><a href='pageedit.php?pagetype=resourceallocation&rowid=".$resourceallocationid."&pagename=".$pagename."'>".$langval599."</a></td>";			
			echo "</tr>";	
		}
		if(mysql_num_rows($getallocation) >= 1){
			echo "</table>";
		}
	}
	
	if($displaytype == "calendar"){
		//include('fullcalendar-3.2.0/demos/default.html');
		?>
		<script type="text/javascript" >
		
			$(document).ready(function() {
		
				$('#calendar1').fullCalendar({
					defaultDate: '<?php echo $date ?>',
					editable: true,
					eventLimit: true, // allow "more" link when too many events
					events: [ 
					<?php
					$events = "";
					$title = '';
					$start = '';
					$end = '';
					$url = '';
					$colour = '';
					while ($row5 = mysql_fetch_assoc($getallocation)){
						$title1 = $row5['taskname'].' - '.$row5['resourcename'];
						$start1 = $row5['startdatetime'];
						$end1 = $row5['enddatetime'];
						$colour1 = 'blue';
						$url1 = "pageedit.php?pagetype=resourceallocation&rowid=".$row5['resourceallocationid'];
						$tip1 = $row5['taskname'].' - '.$row5['resourcename'];
						$events = $events."{";
						$events = $events."title: '".$title1."',";
						$events = $events."start: '".$start1."',";
						$events = $events."end: '".$end1."',";
						$events = $events."tip: '".$tip1."',";
						$events = $events."color: '".$colour1."',";
						$events = $events."url: '".$url1."'";
						$events = $events."},";
					}
					$events = substr(trim($events), 0, -1);
				 	echo $events;
					?>
						
					],
					eventRender: function(event, element) {
			            element.attr('title', event.tip);
			        },
					eventClick: function(event) {
		        	if (event.url) {
		            	window.open(event.url);
		            	return false;
		        	}
		    	}
		    	});
				
			});
		
		</script>
		<div id='calendar1' style="width:100%;height:auto;overflow:hidden;z-index:-1"></div>
		<link href='fullcalendar-3.2.0/fullcalendar.min.css' rel='stylesheet' />
		<link href='fullcalendar-3.2.0/fullcalendar.print.min.css' rel='stylesheet' media='print' />
		<script src='fullcalendar-3.2.0/lib/jquery.min.js'></script>
		<script src='fullcalendar-3.2.0/lib/moment.min.js'></script>
		<script src='fullcalendar-3.2.0/fullcalendar.min.js'></script>
		<br/><br/>
		<?php
	}
	
		
	
}

?>