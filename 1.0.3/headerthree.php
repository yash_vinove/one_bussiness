<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
/* Style the navigation menu */
.topnav {
  overflow: hidden;
  background-color: #d3d3d3;
  position: relative;
}

/* Hide the links inside the navigation menu (except for logo/home) */
.topnav #myLinks {
  display: none;
}

/* Style navigation menu links */
.topnav a {
  color: black;
  padding: 5px 5px;
  text-decoration: none;
  font-size: 17px;
  display: block;
}

/* Style the hamburger menu */
.topnav a.icon {
  background: #d3d3d3;
  display: block;
  position: absolute;
  right: 0;
  top: 0;
}

/* Style the active link (or home/logo) */
.active {
  background-color: #4CAF50;
  color: white;
}
</style>
<script>
/* Toggle between showing and hiding the navigation menu links when the user clicks on the hamburger menu / bar icon */
function myFunction() {
  var x = document.getElementById("myLinks");
  if (x.style.display === "block") {
    x.style.display = "none";
  } else {
    x.style.display = "block";
  }
}
</script>



<!-- Desktop Navigation -->
<div class="hidden-xs col-sm-2 col-md-2 col-lg-2" style="padding:10px;">

<?php
$mysqli = new mysqli($server, $user_name, $password, $database);
$languageid = mysqli_real_escape_string($mysqli, $_SESSION['languageid']);
if ($stmt = $mysqli->prepare("SELECT languagerecordid, languagerecordtextname FROM $masterdatabase.languagerecordtext
where languageid = ? and languagerecordid in (1,2,3,130,131,847)")) {
    $stmt->bind_param('i', $languageid);
    $stmt->execute();
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()) {
        $langid = $row['languagerecordid'];
        ${"langval$langid"} = $row['languagerecordtextname'];
        //echo "<br/>".$langid." -".${"langval$langid"};
    }
}
$stmt->close();

$topnavigation = "";
$topnavigationmobile = "";
$topnavigation1 = "";
if ($_SESSION["savedbusinessname"] != "") {
    $topnavigation1 = $topnavigation1 . "<a href=''><div class='topnavigationbusinessname'>" . $_SESSION["savedbusinessname"] . "</div></a>";
} else {
    $topnavigation1 = $topnavigation1 . "<a href=''><div class='topnavigationbusinessname'>OneBusiness</div></a>";
}

//print dashboards
$mysqli = new mysqli($server, $user_name, $password, $database);if ($stmt = $mysqli->prepare("select chartsectionid, chartsectionname from chartsection
where chartsection.disabled = 0 order by sortorder asc")) {$stmt->execute();
    $result = $stmt->get_result();if ($result->num_rows > 0) {$topnavigation = $topnavigation . "<a href='#dashboard' data-toggle='collapse'><div class='navigationlista'>" . strtoupper($langval1) . "<b class='caret'></b></div></a>";
        $topnavigationmobile = $topnavigationmobile . "<a href='#dashboardmobile' data-toggle='collapse'><div class='navigationlista'>" . strtoupper($langval1) . "<b class='caret'></b></div></a>";
        $topnavigation = $topnavigation . "<div id='dashboard' class='collapse'><ul style='list-style:none;padding-left:8px;'>";
        $topnavigationmobile = $topnavigationmobile . "<div id='dashboardmobile' class='collapse'><ul style='list-style:none;padding-left:8px;'>";
        if ($_SESSION['superadmin'] == 1) {
            $pagename = $_SERVER['REQUEST_URI'];
            $pagename = substr($pagename, 0, strrpos($pagename, '/'));
            $pagename = $pagename . "/";
            $topnavigation = $topnavigation . "<li><a href='pageadd.php?pagetype=chartsection&pagename=" . $pagename . "dashboard.php?dashboardsectionid=[recordid]'><div class='navigationlista'>+ $langval847</div></a></li>";
            $topnavigationmobile = $topnavigationmobile . "<li><a href='pageadd.php?pagetype=chartsection&pagename=" . $pagename . "dashboard.php?dashboardsectionid=[recordid]'><div class='navigationlista'>+ $langval847</div></a></li>";
        }
        while ($row96 = $result->fetch_assoc()) {$sectionname = $row96['chartsectionname'];
            $sectionid = $row96['chartsectionid'];
            $getpermission = mysql_query("select * from userrole
			inner join permissionroledashboardsection on permissionroledashboardsection.permissionroleid = userrole.permissionroleid
			where permissionroledashboardsection.disabled = '0' and userid = '$userid' and chartsectionid = '$sectionid'");
            $gettranslation = mysql_query("select * from chartsectiontranslation
			where chartsectionid = '$sectionid' and languageid = '$_SESSION[languageid]'");
            if (mysql_num_rows($gettranslation) >= 1) {
                $gettranslationrow = mysql_fetch_array($gettranslation);
                $sectionname = $gettranslationrow['chartsectiontranslationname'];
            }
            if (mysql_num_rows($getpermission) > 0) {
                $topnavigation = $topnavigation . "<li><div class='navigationlista'><a href='dashboard.php?dashboardsectionid=" . $sectionid . "' style='color:black;'>" . $sectionname . "</a>";
                $topnavigationmobile = $topnavigationmobile . "<li><div class='navigationlista'><a href='dashboard.php?dashboardsectionid=" . $sectionid . "'>" . $sectionname . "</a>";
                if ($_SESSION['accesslevel'] == "Master" ||
                    (($_SESSION['accesslevel'] == "Tenant" || $_SESSION['accesslevel'] == "Business") && $sectionid >= 1000000)) {
                    $topnavigation = $topnavigation . " <a href='pageedit.php?pagetype=chartsection&rowid=" . $sectionid . "&pagename=" . $pagename . "dashboard.php?dashboardsectionid=[recordid]'><i class='fa fa-pencil'></i></a>";
                    $topnavigationmobile = $topnavigationmobile . " <a href='pageedit.php?pagetype=chartsection&rowid=" . $sectionid . "&pagename=" . $pagename . "dashboard.php?dashboardsectionid=[recordid]'><i class='fa fa-pencil'></i></a>";
                }
                $topnavigation = $topnavigation . "</div>";
                $topnavigationmobile = $topnavigationmobile . "</div>";

                $topnavigation = $topnavigation . "</li>";
                $topnavigationmobile = $topnavigationmobile . "</li>";
            }}
        $topnavigation = $topnavigation . "</ul></div>";
        $topnavigationmobile = $topnavigationmobile . "</ul></div>";}}
$mysqli->close();

$mysqli = new mysqli($server, $user_name, $password, $database);
if ($stmt = $mysqli->prepare("select reportsectionid, reportsectionname from reportsection
where reportsection.disabled = 0 order by sortorder asc")) {$stmt->execute();
    $result = $stmt->get_result();if ($result->num_rows > 0) {$topnavigation = $topnavigation . "<a href='#report' data-toggle='collapse'><div class='navigationlista'>" . strtoupper($langval2) . "<b class='caret'></b></div></a>";
        $topnavigationmobile = $topnavigationmobile . "<a href='#reportmobile' data-toggle='collapse'><div class='navigationlista'>" . strtoupper($langval2) . "<b class='caret'></b></div></a>";
        $topnavigation = $topnavigation . "<div id='report' class='collapse'><ul style='list-style:none;padding-left:8px;'>";
        $topnavigationmobile = $topnavigationmobile . "<div id='reportmobile' class='collapse'><ul style='list-style:none;padding-left:8px;'>";
        while ($row96 = $result->fetch_assoc()) {$sectionname = $row96['reportsectionname'];
            $sectionid = $row96['reportsectionid'];
            $getpermission = mysql_query("select * from userrole
			inner join permissionrolereportsection on permissionrolereportsection.permissionroleid = userrole.permissionroleid
			where permissionrolereportsection.disabled = '0' and userid = '$userid' and reportsectionid = '$sectionid'");
            $gettranslation = mysql_query("select * from reportsectiontranslation
			where reportsectionid = '$sectionid' and languageid = '$_SESSION[languageid]'");
            if (mysql_num_rows($gettranslation) >= 1) {
                $gettranslationrow = mysql_fetch_array($gettranslation);
                $sectionname = $gettranslationrow['reportsectiontranslationname'];
            }
            if (mysql_num_rows($getpermission) > 0) {
                $topnavigation = $topnavigation . "<li><a href='report.php?reportsectionid=" . $sectionid . "'><div class='navigationlista'>" . $sectionname . "</div></a></li>";
                $topnavigationmobile = $topnavigationmobile . "<li><a href='report.php?reportsectionid=" . $sectionid . "'><div class='navigationlista'>" . $sectionname . "</div></a></li>";
            }}
        $topnavigation = $topnavigation . "</ul></div>";
        $topnavigationmobile = $topnavigationmobile . "</ul></div>";}}
$mysqli->close();

$databaseamended = str_replace('onebusinesstenant', '', $database);
$_SESSION["tenantid"] = 0;
if (is_numeric($databaseamended)) {
    $_SESSION["tenantid"] = $databaseamended;
    $topnavigation = $topnavigation . "<ul style='list-style:none;padding-left:8px;'><li><a href='tenantbilling.php'><div class='navigationlista'>BILLING</a></div></li></ul>";
    $topnavigationmobile = $topnavigationmobile . "<ul style='list-style:none;padding-left:8px;'><li><a href='tenantbilling.php'><div class='navigationlista'>BILLING</a></div></li></ul>";
}

$structurefunctionsectionnamesaved = '';
$mysqli = new mysqli($server, $user_name, $password, $database);
if ($stmt = $mysqli->prepare("select * from structurefunctionpage
inner join structurefunctionsection on structurefunctionsection.structurefunctionsectionid = structurefunctionpage.structurefunctionsectionid
left join structurefunction on structurefunction.structurefunctionid = structurefunctionpage.structurefunctionid
left join view on view.viewid = structurefunctionpage.viewid
left join $masterdatabase.structurefunctionpagetype on structurefunctionpage.structurefunctionpagetypeid = structurefunctionpagetype.structurefunctionpagetypeid
where structurefunctionpage.disabled = '0' order by structurefunctionsection.sortorder,
structurefunctionpage.structurefunctionpagetypeid, structurefunctionpage.navigationposition asc")) {$stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows > 0) {while ($getfunctionrow = $result->fetch_assoc()) {$functionname = $getfunctionrow['structurefunctionpagename'];
        $structurefunctionsectionid = $getfunctionrow['structurefunctionsectionid'];
        $structurefunctionid = $getfunctionrow['structurefunctionid'];
        $viewid = $getfunctionrow['viewid'];
        $structurefunctionpagetypename = $getfunctionrow['structurefunctionpagetypename'];
        //echo "<br/>".$functionname." - ".$structurefunctionsectionid;
        //check permissions to section
        $mysqli5 = new mysqli($server, $user_name, $password, $database);
        $userid = mysqli_real_escape_string($mysqli5, $userid);
        $structurefunctionsectionid = mysqli_real_escape_string($mysqli5, $structurefunctionsectionid);if ($stmt5 = $mysqli5->prepare("select * from userrole
			inner join permissionrolestructurefunctionsection on permissionrolestructurefunctionsection.permissionroleid = userrole.permissionroleid
			where permissionrolestructurefunctionsection.disabled = '0' and userid = ? and structurefunctionsectionid = ?")) {$stmt5->bind_param('ii', $userid, $structurefunctionsectionid);
            $stmt5->execute();
            $result5 = $stmt5->get_result();
            $checkperm1 = $result5->num_rows;}
        $mysqli5->close();

        // var_dump($structurefunctionsectionid);
        // die();

        //check permissions to pages inside section
        if ($structurefunctionid != 0) {
            $mysqli4 = new mysqli($server, $user_name, $password, $database);
            $userid = mysqli_real_escape_string($mysqli4, $userid);
            $structurefunctionid = mysqli_real_escape_string($mysqli4, $structurefunctionid);
            if ($stmt4 = $mysqli4->prepare("select * from userrole
				inner join permissionrolestructurefunctionpage on
				permissionrolestructurefunctionpage.permissionroleid = userrole.permissionroleid
				inner join structurefunctionpage on
				structurefunctionpage.structurefunctionpageid = permissionrolestructurefunctionpage.structurefunctionpageid
				where permissionrolestructurefunctionpage.disabled = '0' and userid = ? and structurefunctionid = ?")) {$stmt4->bind_param('ii', $userid, $structurefunctionid);
                $stmt4->execute();
                $result4 = $stmt4->get_result();
                $checkperm2 = $result4->num_rows;}
            $mysqli4->close();
        }
        if ($viewid != 0) {
            $mysqli3 = new mysqli($server, $user_name, $password, $database);
            $userid = mysqli_real_escape_string($mysqli3, $userid);
            $viewid = mysqli_real_escape_string($mysqli3, $viewid);if ($stmt3 = $mysqli3->prepare("select * from userrole
				inner join permissionrolestructurefunctionpage on
				permissionrolestructurefunctionpage.permissionroleid = userrole.permissionroleid
				inner join structurefunctionpage on
				structurefunctionpage.structurefunctionpageid = permissionrolestructurefunctionpage.structurefunctionpageid
				where permissionrolestructurefunctionpage.disabled = '0' and userid = ? and viewid = ?")) {$stmt3->bind_param('ii', $userid, $viewid);
                $stmt3->execute();
                $result3 = $stmt3->get_result();
                $checkperm2 = $result3->num_rows;}
            $mysqli3->close();
        }

        if ($checkperm1 > 0 || $checkperm2 > 0) {
            $structurefunctionsectionname = $getfunctionrow['structurefunctionsectionname'];
            $structurefunctionsectionid = $getfunctionrow['structurefunctionsectionid'];
            $tablename = $getfunctionrow['tablename'];
            //echo "<br/>".$structurefunctionsectionid." - ".$structurefunctionsectionname;
            if ($structurefunctionsectionnamesaved != $structurefunctionsectionname) {
                $count19 = 0;
                if ($structurefunctionsectionnamesaved != '') {
                    $topnavigation = $topnavigation . "</ul></div>";
                    $topnavigationmobile = $topnavigationmobile . "</ul></div>";
                }

                $mysqli2 = new mysqli($server, $user_name, $password, $database);
                $structurefunctionsectionid = mysqli_real_escape_string($mysqli2, $structurefunctionsectionid);
                $_SESSION['languageid'] = mysqli_real_escape_string($mysqli2, $_SESSION['languageid']);if ($stmt2 = $mysqli2->prepare("select * from structurefunctionsectionlanguagerecordtext
					where structurefunctionsectionid = ? and languageid = ?")) {$stmt2->bind_param('ii', $structurefunctionsectionid, $_SESSION['languageid']);
                    $stmt2->execute();
                    $result2 = $stmt2->get_result();if ($result2->num_rows > 0) {while ($gettranslationrow = $result2->fetch_assoc()) {$structurefunctionsectionnameupdated = $gettranslationrow['structurefunctionsectionlanguagerecordtextname'];
                        $topnavigation = $topnavigation . "<a data-toggle='collapse' href='#" . $structurefunctionsectionid . "'><div class='navigationlista'>" . strtoupper($structurefunctionsectionnameupdated) . "<b class='caret'></b></div></a><div id='" . $structurefunctionsectionid . "' class='collapse'>";
                        $topnavigationmobile = $topnavigationmobile . "<a data-toggle='collapse' href='#" . $structurefunctionsectionid . "mobile'><div class='navigationlista'>" . strtoupper($structurefunctionsectionnameupdated) . "<b class='caret'></b></div></a><div id='" . $structurefunctionsectionid . "mobile' class='collapse'>";
                    }} else {
                        $topnavigation = $topnavigation . "<a data-toggle='collapse' href='#" . $structurefunctionsectionid . "'><div class='navigationlista'>" . strtoupper($structurefunctionsectionname) . "<b class='caret'></b></div></a><div id='" . $structurefunctionsectionid . "' class='collapse'>";
                        $topnavigationmobile = $topnavigationmobile . "<a data-toggle='collapse' href='#" . $structurefunctionsectionid . "mobile'><div class='navigationlista'>" . strtoupper($structurefunctionsectionname) . "<b class='caret'></b></div></a><div id='" . $structurefunctionsectionid . "mobile' class='collapse'>";
                    }
                    $topnavigation = $topnavigation . "<ul style='list-style:none;padding-left:8px;'>";
                    $topnavigationmobile = $topnavigationmobile . "<ul style='list-style:none;padding-left:8px;'>";
                    $structurefunctionsectionnamesaved = $structurefunctionsectionname;}
                $mysqli2->close();
            }
            if ($checkperm1 > 0 || $checkperm2 > 0) {
                if ($count19 == 0 || $structurefunctionpagetypename != $savedstructurefunctionpagetypename) {
                    if ($structurefunctionpagetypename == "Tools") {
                        $topnavigation = $topnavigation . "<li><b><a href=''><div class='navigationlista'>" . strtoupper($langval130) . "</div></a></b></li>";
                        $topnavigationmobile = $topnavigationmobile . "<li><b><a href=''><div class='navigationlista'>" . strtoupper($langval130) . "</div></a></b></li>";
                    }
                    if ($structurefunctionpagetypename == "Settings") {
                        $topnavigation = $topnavigation . "<li><b><a href=''><div class='navigationlista'>" . strtoupper($langval131) . "</div></a></b></li>";
                        $topnavigationmobile = $topnavigationmobile . "<li><b><a href=''><div class='navigationlista'>" . strtoupper($langval131) . "</div></a></b></li>";
                    }
                    $savedstructurefunctionpagetypename = $structurefunctionpagetypename;
                    $count19 = 1;
                }
                $gettranslation = mysql_query("select * from structurefunctionpagelanguagerecordtext
					where structurefunctionid = '$structurefunctionid' and languageid = '$_SESSION[languageid]'");
                if (mysql_num_rows($gettranslation) >= 1) {
                    $gettranslationrow = mysql_fetch_array($gettranslation);
                    $structurefunctionupdated = $gettranslationrow['structurefunctionpagelanguagerecordtextname'];
                    if ($viewid != 0) {
                        $topnavigation = $topnavigation . "<li><a href='view.php?viewid=" . $viewid . "'><div class='navigationlista'>> " . $structurefunctionupdated . "</div></a></li>";
                        $topnavigationmobile = $topnavigationmobile . "<li><a href='view.php?viewid=" . $viewid . "'><div class='navigationlista'>> " . $structurefunctionupdated . "</div></a></li>";
                    } else {
                        $topnavigation = $topnavigation . "<li><a href='pagegrid.php?pagetype=" . $tablename . "'><div class='navigationlista'>> " . $structurefunctionupdated . "</div></a></li>";
                        $topnavigationmobile = $topnavigationmobile . "<li><a href='pagegrid.php?pagetype=" . $tablename . "'><div class='navigationlista'>> " . $structurefunctionupdated . "</div></a></li>";
                    }
                } else {
                    if ($viewid != 0) {
                        $topnavigation = $topnavigation . "<li><a href='view.php?viewid=" . $viewid . "'><div class='navigationlista'>> " . $functionname . "</div></a></li>";
                        $topnavigationmobile = $topnavigationmobile . "<li><a href='view.php?viewid=" . $viewid . "'><div class='navigationlista'>> " . $functionname . "</div></a></li>";
                    } else {
                        $topnavigation = $topnavigation . "<li><a href='pagegrid.php?pagetype=" . $tablename . "'><div class='navigationlista'>> " . $functionname . "</div></a></li>";
                        $topnavigationmobile = $topnavigationmobile . "<li><a href='pagegrid.php?pagetype=" . $tablename . "'><div class='navigationlista'>> " . $functionname . "</div></a></li>";
                    }
                }

            }

        }}}}
$mysqli->close();

$topnavigation = $topnavigation . "</ul></div>";
$topnavigationmobile = $topnavigationmobile . "</ul></div>";
$url = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
if (strpos($url, 'script/') !== false) {
    $topnavigation = $topnavigation . "<ul style='list-style:none;padding-left:0px;'><li><a href='../sonhlab-social-auth-v2-20141002/auth/logout.php'><div class='navigationlista'>" . strtoupper($langval3) . "</div></a></li></ul>";
    $topnavigationmobile = $topnavigationmobile . "<ul style='list-style:none;padding-left:0px;'><li><a href='../sonhlab-social-auth-v2-20141002/auth/logout.php'><div class='navigationlista'>" . strtoupper($langval3) . "</div></a></li></ul>";
} else {
    $topnavigation = $topnavigation . "<ul style='list-style:none;padding-left:0px;'><li><a href='./sonhlab-social-auth-v2-20141002/auth/logout.php'><div class='navigationlista'>" . strtoupper($langval3) . "</div></a></li></ul>";
    $topnavigationmobile = $topnavigationmobile . "<ul style='list-style:none;padding-left:0px;'><li><a href='./sonhlab-social-auth-v2-20141002/auth/logout.php'><div class='navigationlista'>" . strtoupper($langval3) . "</div></a></li></ul>";
}
$topnavigation = $topnavigation . "<br/><br/><br/>";
$topnavigationmobile = $topnavigationmobile . "<br/><br/><br/>";
//$topnavigation = $topnavigation."<br/><br/><br/></div>";
echo $topnavigation1;
echo $topnavigation;
?>
</div>

<!-- Mobile Navigation -->
<div class="col-xs-12 hidden-sm hidden-md hidden-lg">
	<div class="topnav">
	  <?php
echo $topnavigation1;
?>
	  <!-- Navigation links (hidden by default) -->
	  <div id="myLinks">
	    <?php
echo $topnavigationmobile;
?>
	  </div>
	  <!-- "Hamburger menu" / "Bar icon" to toggle the navigation links -->
	  <a href="javascript:void(0);" class="icon" onclick="myFunction()">
	    Menu
	  </a>
	</div>
</div>