<?php session_start(); ?>
<?php include('config.php'); ?>
<?php include('sessionconfig.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<title>OneBusiness</title>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel='stylesheet' type='text/css' href='style.php' />
</head>
  
<body class='body'>
	<?php 
	include_once ('headerthree.php');
	$mysqli = new mysqli($server, $user_name, $password, $database);
	$languageid = mysqli_real_escape_string($mysqli, $_SESSION['languageid']);
	if($stmt = $mysqli->prepare("SELECT languagerecordid, languagerecordtextname FROM $masterdatabase.languagerecordtext 
	where languageid = ? and languagerecordid in (5,19,88,89,90)")){
		$stmt->bind_param('i', $languageid);
		$stmt->execute();
		$result = $stmt->get_result();
		while ($row = $result->fetch_assoc()){
			$langid = $row['languagerecordid'];
			${"langval$langid"} = $row['languagerecordtextname'];
			//echo "<br/>".$langid." -".${"langval$langid"};
		}
	}
	else {
		//echo $mysqli->error;
	}
	$mysqli->close();
	
	$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
	$requestconfigurationid = isset($_GET['requestconfigurationid']) ? strip_tags($_GET['requestconfigurationid']) : '';
	 ?>
	 
	<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding:10px;">
		<!--search results header bar for desktop-->
		<div class='bodyheader'>
			<h3><?php echo $langval89 ?></h3>
		</div>
			<?php 
			if(isset($_POST['submit'])) {
				//get post values
				$requestfield = isset($_POST['requestfield']) ? strip_tags($_POST['requestfield']) : '';
				$pagename = isset($_POST['pagename']) ? strip_tags($_POST['pagename']) : '';
				
				//echo $requestfield;
				//validate fields
				$validate = "";
				if($requestfield == "") {
					$validate .=$langval90; 	
				}
				
				if($validate == "") {
					$datecreated = date("Y-m-d");
					
					//check if there is a record for this already
					$mysqli = new mysqli($server, $user_name, $password, $database);					$requestconfigurationid = mysqli_real_escape_string($mysqli, $requestconfigurationid);					if($stmt = $mysqli->prepare("select * from requestconfiguration 
					where requestconfigurationid = ?")){					   $stmt->bind_param('i', $requestconfigurationid);					   $stmt->execute();					   $result = $stmt->get_result();					   if($result->num_rows > 0){					    	while($resultconfig = $result->fetch_assoc()){					     		$requesttable = $resultconfig['requesttable'];
				    		  	$requestidfield = $resultconfig['requestidfield'];
				    		  	$requestdisplayfield = $resultconfig['requestdisplayfield'];
				    		  	$emailcontentrequestid = $resultconfig['emailcontentrequestid'];					    	}					   	}					}					else {						//echo $mysqli->error;					}					$mysqli->close();					
					
	    		  	
	    		  	//delete existing, if exists
	    		  	$mysqli = new mysqli($server, $user_name, $password, $database);					$userid = mysqli_real_escape_string($mysqli, $_SESSION['userid']);					$requestidfield = mysqli_real_escape_string($mysqli, $requestidfield);					$requestfield = mysqli_real_escape_string($mysqli, $requestfield);					if($stmt = $mysqli->prepare("delete from request 
					where userid = ? and ? = ?")){					   $stmt->bind_param('iss', $userid, $requestidfield, $requestfield);					   $stmt->execute();	
					   $stmt->close();				   					}					else {						//echo $mysqli->error;					}					$mysqli->close();		
					
					//add new record
					$name = $_SESSION['userid']." - ".$requestfield;
					$mysqli = new mysqli($server, $user_name, $password, $database);
					if($requestidfield == 'businessunitid'){
						$businessunitid = mysqli_real_escape_string($mysqli, $requestfield);	
					}
					else {
						$businessunitid = 0;	
					}						if($requestidfield == 'permissionroleid'){
						$permissionroleid = mysqli_real_escape_string($mysqli, $requestfield);	
					}
					else {
						$permissionroleid = 0;	
					}						$requestidfield = mysqli_real_escape_string($mysqli, $requestidfield);
					$requestdisplayfield = mysqli_real_escape_string($mysqli, $requestdisplayfield);
					$requesttable = mysqli_real_escape_string($mysqli, $requesttable);
					$userid9 = mysqli_real_escape_string($mysqli, $_SESSION['userid']);
					$name = mysqli_real_escape_string($mysqli, $name);
					$requestfield = mysqli_real_escape_string($mysqli, $requestfield);
					$datecreated = mysqli_real_escape_string($mysqli, $datecreated);
					$requestconfigurationid = mysqli_real_escape_string($mysqli, $requestconfigurationid);
					//echo $requestidfield.", ".$name.", ".$userid9.", ".$requestfield.", ".$datecreated.", ".$requestconfigurationid;
					if($stmt = $mysqli->prepare("insert into request (requestname, userid, permissionroleid, businessunitid, masteronly, 
					disabled, datecreated, requeststatusid, requestconfigurationid) 
					values (?, ?, ?, ?, '0', '0', ?, '1', ?)")){					   $stmt->bind_param('siiisi', $name, $userid9, $permissionroleid, $businessunitid, $datecreated, $requestconfigurationid);					   $stmt->execute();	
					   $stmt->close();			   					}					else {						//echo $mysqli->error;										}					$mysqli->close();		
					
					//get businessunit details
					$mysqli = new mysqli($server, $user_name, $password, $database);					if($stmt = $mysqli->prepare("select $requestidfield, $requestdisplayfield from $requesttable where disabled = 0")){					   $stmt->execute();					   $result = $stmt->get_result();					   if($result->num_rows > 0){					    	while($querydrop = $result->fetch_assoc()){					     		$requestfieldname = $querydrop[$requestdisplayfield];					    	}					   	}					}					else {						//echo $mysqli->error;					}					$mysqli->close();

					//send request email
					$subject1 = $_SESSION["firstname"]." ".$_SESSION["lastname"];
					//echo "subject1 - ".$subject1."<br/>";
					$content1 = "Administrator";
					//echo "content1 - ".$content1."<br/>";
					$content2 = $_SESSION["firstname"];
					//echo "content2 - ".$content2."<br/>";
					$content3 = $requestfieldname;
					//echo "content3 - ".$content3."<br/>";
					$emailurl = "sendemail.php";	
					$emailcontentid = $emailcontentrequestid;
					include($emailurl);	
					
					//redirect user back to originating page
					$url = $pagename."&updated=1";
					echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">';   				
				}				
			}
			else {
				$pagename = isset($_GET['pagename']) ? strip_tags($_GET['pagename']) : '';
				$requestfield = '';
					
			} 
			?>
			
			<div class='bodycontent'>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<?php 
	  				if(isset($validate) && $validate <> "") {
						echo "<p class='background-warning'>".$validate."</p>";
					} 
					?>
				<?php
				echo "<p>".$langval88."<br/><br/></p>";
				?>
		
			</div>
				
					<form class="form-horizontal" action='accessrequest.php?requestconfigurationid=<?php echo $requestconfigurationid?>&pagename=<?php echo $pagename ?>' method="post">
						<div class="col-xs-12 col-md-7 col-sm-8 col-lg-6">
							<div class="form-group">
							 	<label for="<?php echo $langval19 ?>" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label"><?php echo $langval19?></label>
						    	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
					    		  	<?php
					    		  	$mysqli = new mysqli($server, $user_name, $password, $database);									$requestconfigurationid = mysqli_real_escape_string($mysqli, $requestconfigurationid);									if($stmt = $mysqli->prepare("select * from requestconfiguration where requestconfigurationid = ?")){									   $stmt->bind_param('i', $requestconfigurationid);									   $stmt->execute();									   $result = $stmt->get_result();									   if($result->num_rows > 0){									    	while($resultconfig = $result->fetch_assoc()){									     		$requesttable = $resultconfig['requesttable'];
								    		  	$requestidfield = $resultconfig['requestidfield'];
								    		  	$requestdisplayfield = $resultconfig['requestdisplayfield'];									    	}									   	}									}									else {										//echo $mysqli->error;									}									$mysqli->close();
									
									echo "<select class='form-control' name='requestfield'>";
						        	echo "<option value=''>".$langval19."</option>";
					         		$mysqli = new mysqli($server, $user_name, $password, $database);									$requestidfield = mysqli_real_escape_string($mysqli, $requestidfield);									$requestdisplayfield = mysqli_real_escape_string($mysqli, $requestdisplayfield);									$requesttable = mysqli_real_escape_string($mysqli, $requesttable);									if($stmt = $mysqli->prepare("select ".$requestidfield.", ".$requestdisplayfield." 
									from ".$requesttable." 
									where ".$requesttable.".disabled = 0")){									   $stmt->execute();									   $result = $stmt->get_result();									   if($result->num_rows > 0){									    	while($row = $result->fetch_assoc()){									     		if($requestfield==$row[$requestidfield]) {
							               		echo "<option value=".$row[$requestidfield]." selected='true'>".$row[$requestdisplayfield]."</option>";
							             	}
							             	else {
							               		echo "<option value=".$row[$requestidfield]." >".$row[$requestdisplayfield]."</option>";
							             	}						     													    	}								    										   	}									}									else {										//echo $mysqli->error;									}									$mysqli->close();
									echo "</select>";
						      		?>
						    	</div>
						  	</div> 
						</div>
						<div class="form-group">
					    	<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					    		<div style="text-align:center;">
					      		<button type='submit' name='submit' value='Submit' class="button-primary"><?php echo $langval5 ?></button>	
					      		<input type="hidden" value='<?php echo $pagename; ?>' name='pagename'>																								
					    		</div>
					    	</div>
					  	</div>
					</form>	
					
					<br/><br/><br/>
				</div>
	  		</div>
		</div>
			
	</div>	
	<?php include_once ('footer.php'); ?>
</body>
