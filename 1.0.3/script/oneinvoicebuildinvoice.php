<?php

$getinvoice = mysql_query("select invoice.businessunitid, businessunitname, customer.customerid, dateissued, customername, 
firstname, lastname, customer.address1, customer.address2, customer.address3, customer.towncity, customer.stateregion, 
customer.postzipcode, countryname, customer.emailaddress, invoiceintroductiontext, invoiceclosingtext, invoicepaymentterms
from invoice 
inner join customer on customer.customerid = invoice.customerid
inner join businessunit on businessunit.businessunitid = invoice.businessunitid
inner join country on country.countryid = customer.countryid
inner join invoiceconfiguration on invoice.businessunitid = invoiceconfiguration.businessunitid
where invoiceid = '$invoiceid'");
if(mysql_num_rows($getinvoice) >= 1){
	$getinvoicerow = mysql_fetch_array($getinvoice);
	$businessuntid = $getinvoicerow['businessunitid'];
	$businessunitname = $getinvoicerow['businessunitname'];
	$customerid = $getinvoicerow['customerid'];
	$dateissued = $getinvoicerow['dateissued'];
	$customername = $getinvoicerow['customername'];
	$firstname = $getinvoicerow['firstname'];
	$lastname = $getinvoicerow['lastname'];
	$address1 = $getinvoicerow['address1'];
	$address2 = $getinvoicerow['address2'];
	$address3 = $getinvoicerow['address3'];
	$towncity = $getinvoicerow['towncity'];
	$stateregion = $getinvoicerow['stateregion'];
	$postzipcode = $getinvoicerow['postzipcode'];
	$countryname = $getinvoicerow['countryname'];
	$emailaddress = $getinvoicerow['emailaddress'];
	$invoiceintroductiontext = $getinvoicerow['invoiceintroductiontext'];
	$invoiceclosingtext = $getinvoicerow['invoiceclosingtext'];
	$invoicepaymentterms = $getinvoicerow['invoicepaymentterms'];
	
	$date = date("d-M-Y");
	$dateissued = date("d-M-Y", strtotime($dateissued));
	
	// Instantiation of inherited class
	$pdf = new PDF();
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->SetFont('Arial','B',13);
	$pdf->Cell(75);
   	$pdf->Cell(40,10,$businessunitname,0,0,'C');
   	$pdf->Ln(15);
	$pdf->Cell(75);
   	$pdf->Cell(40,10,'Invoice - '.$customername." - ".$invoiceid,0,0,'C');
   	$pdf->Ln(8);
	$pdf->Cell(75);
	$pdf->SetFont('Arial','',10);
   	$pdf->Cell(40,10,$dateissued,0,0,'C');
   	
   	//customer details section
  	$pdf->Ln(15);
 	$pdf->SetFont('Arial','',10);
   	if($address1 <> ''){
		$pdf->MultiCell(0,5,$address1,0,1);
	}
	if($address2 <> ''){
		$pdf->MultiCell(0,5,$address2,0,1);
	}
	if($address3 <> ''){
		$pdf->MultiCell(0,5,$address3,0,1);
	}
	if($towncity <> ''){
		$pdf->MultiCell(0,5,$towncity,0,1);
	}
	if($stateregion <> ''){
		$pdf->MultiCell(0,5,$stateregion,0,1);
	}
	if($postzipcode <> ''){
		$pdf->MultiCell(0,5,$postzipcode,0,1);
	}
	if($countryname <> ''){
		$pdf->MultiCell(0,5,$countryname,0,1);
	}
	if($emailaddress <> ''){
		$pdf->Ln(4);
		$pdf->MultiCell(0,5,$emailaddress,0,1);
	}
	if($firstname <> ''){
		$pdf->Ln(4);
		$pdf->MultiCell(0,5,'FAO - '.$firstname.' '.$lastname,0,1);
	}
	
	 
	if($invoiceintroductiontext <> ""){ 	
	   	$pdf->Ln(5);
		$pdf->SetFont('Arial','',10);
		$pdf->MultiCell(0,5,$invoiceintroductiontext,0,1);
	}
	
	
	//iterate through the paymentitem positive values
	$overallamount = 0;
	$overalltaxamount = 0;
	$overalltotalamount = 0;
	$querybuilder2 = mysql_query("select * from invoiceitem 
	inner join currency on currency.currencyid = invoiceitem.currencyid
	where invoiceid = '$invoiceid'");
	if(mysql_num_rows($querybuilder2) >= 1){
		$pdf->SetFont('Arial','B',10);
		$pdf->Ln(5);
		$y = $pdf->GetY();
		$x = $pdf->GetX();
		$width = 80;
		$pdf->MultiCell($width, 6, 'Invoice Item', 1, 'L', FALSE);
		$pdf->SetXY($x + $width, $y);
		$pdf->Cell(20,6, 'Currency', 1, 0, "l");	
		$q = $x + $width + 20;
		$pdf->SetXY($q, $y);
		$pdf->Cell(30,6, 'Amount', 1, 0, "l");	
		$q = $x + $width + 50;
		$pdf->SetXY($q, $y);
		$pdf->Cell(30,6, 'Tax Amount', 1, 0, "l");	
		$q = $x + $width + 80;
		$pdf->SetXY($q, $y);
		$pdf->Cell(30,6, 'Total Amount', 1, 0, "l");	
		$pdf->Ln(6);
		while($rowinvoiceitem = mysql_fetch_array($querybuilder2)){
			//get needed variables
			$invoiceitemname = $rowinvoiceitem['invoiceitemname'];
			$currencycode = $rowinvoiceitem['currencycode'];
			$amount = $rowinvoiceitem['amount'];
			$totalamount = $rowinvoiceitem['amountincludingtax'];
			$taxamount = $totalamount - $amount;
			$pdf->SetFont('Arial','',10);
			$y = $pdf->GetY();
			$x = $pdf->GetX();
			$width = 80;
			$pdf->MultiCell($width, 6, $invoiceitemname, 1, 'L', FALSE);
			$pdf->SetXY($x + $width, $y);
			$pdf->Cell(20,6, $currencycode, 1, 0, "l");	
			$q = $x + $width + 20;
			$pdf->SetXY($q, $y);
			$pdf->Cell(30,6, number_format($amount, 2), 1, 0, "l");	
			$q = $x + $width + 50;
			$pdf->SetXY($q, $y);
			$pdf->Cell(30,6, number_format($taxamount, 2), 1, 0, "l");	
			$q = $x + $width + 80;
			$pdf->SetXY($q, $y);
			$pdf->Cell(30,6, number_format($totalamount, 2), 1, 0, "l");	
			$pdf->Ln(6);
			$overallamount = $overallamount + $amount;
			$overalltaxamount = $overalltaxamount + $taxamount;
			$overalltotalamount = $overalltotalamount + $totalamount;
		}
	}
	
	if($overallamount <> 0){
		$pdf->SetFont('Arial','B',10);
		$y = $pdf->GetY();
		$x = $pdf->GetX();
		$width = 80;
		$pdf->SetXY($x + $width, $y);
		$pdf->Cell(20,6, 'TOTAL', 1, 0, "l");	
		$q = $x + $width + 20;
		$pdf->SetXY($q, $y);
		$pdf->Cell(30,6, number_format($overallamount, 2), 1, 0, "l");	
		$q = $x + $width + 50;
		$pdf->SetXY($q, $y);
		$pdf->Cell(30,6, number_format($overalltaxamount, 2), 1, 0, "l");	
		$q = $x + $width + 80;
		$pdf->SetXY($q, $y);
		$pdf->Cell(30,6, number_format($overalltotalamount, 2), 1, 0, "l");	
		$pdf->Ln(6);			
	}
	
	
	
	if($invoicepaymentterms <> ""){ 	
	   	$pdf->Ln(5);
		$pdf->SetFont('Arial','',10);
		$pdf->MultiCell(0,5,$invoicepaymentterms,0,1);
	}
	if($invoiceclosingtext <> ""){ 	
	   	$pdf->Ln(5);
		$pdf->SetFont('Arial','',10);
		$pdf->MultiCell(0,5,$invoiceclosingtext,0,1);
	}
	
	
}
			


?>