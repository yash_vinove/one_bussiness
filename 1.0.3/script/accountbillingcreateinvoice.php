<?php

require __DIR__ . '../../twilio-php-master/Twilio/autoload.php';
use Twilio\Rest\Client;

$masterdatabase = 'andrewnorth_onebusinessmaster';

//get all tenants
$gettenant = mysql_query("select tenantid, tenantname, accountbillingdiscountpercentage, accountbillingcurrencyid, accountbillingcountryid 
from andrewnorth_onebusinessmaster.tenant where tenant.disabled='0' and (tenanttypeid ='1' or tenanttypeid = '2')");

while($tenantrow=mysql_fetch_array($gettenant)){
	//generate databasename for tenant
	$tenantid = $tenantrow['tenantid'];
	$tenantname = $tenantrow['tenantname'];
	$accountbillingcurrencyid = $tenantrow['accountbillingcurrencyid'];
	$accountbillingcountryid = $tenantrow['accountbillingcountryid'];
	$accountbillingdiscountpercentage = $tenantrow['accountbillingdiscountpercentage'];
	$tenantdatabase = "andrewnorth_onebusinesstenant".$tenantid;
	echo "<br/><br/>tenantdatabase - ".$tenantdatabase."<br/>";
	
	//gettodaysdate
	$date = date("Y-m-d");
	
	//generate an invoice name
	$accountbillinginvoicename = $tenantid."-".$tenantname."-".$date;
	
	//create an invoice record for the tenant and set status to generating cost breakdown (5)	
	$createinvoice = mysql_query("insert into andrewnorth_onebusinessmaster.accountbillinginvoice (accountbillinginvoicename, tenantid, invoicedate,
	accountbillingcurrencyid, accountbillinginvoicestatusid, disabled, datecreated) 
	values ('$accountbillinginvoicename', '$tenantid', '$date', '$accountbillingcurrencyid', '5', '0', '$date')");
	$accountbillinginvoiceid = mysql_insert_id();
	
	//get businesses within the tenant
	$querybuilder = "select businessid, businessname from ".$tenantdatabase.".business 
	where business.disabled='0' and business.businessid<>'1'";
	//echo $querybuilder;
	$getbusiness = mysql_query($querybuilder);
	
	while($businessrow=mysql_fetch_array($getbusiness)){
		//generate databasename for business
		$businessid = $businessrow['businessid'];
		$businessname = $businessrow['businessname'];
		$businessdatabase = "andrewnorth_onebusinesstenant".$tenantid."business".$businessid;
		echo "<br/><br/>businessdatabase - ".$businessdatabase."<br/>";
	
		$checktable = mysql_query("SELECT * FROM information_schema.tables
		WHERE table_schema = '$businessdatabase' AND table_name = 'user'");
	   if(mysql_num_rows($checktable) >= 1){
			//calculate how many admin users there are
			$querybuilder = "select count(user.userid) as adminusers from ".$businessdatabase.".user 
			inner join ".$businessdatabase.".userrole on userrole.userid = user.userid 
			where userrole.permissionroleid = '1' and user.disabled = '0'";
			//echo "<br/><br/>".$querybuilder;
			$getadminusers = mysql_query($querybuilder);
			$adminusersrow = mysql_fetch_array($getadminusers);
			$adminusers = $adminusersrow['adminusers'];
			echo "adminusers - ".$adminusers."<br/>";
			
			//calculate how many normal users there are
			$querybuilder = "select count(user.userid) as normalusers from ".$businessdatabase.".user where user.disabled = '0'";
			$getnormalusers = mysql_query($querybuilder);
			$normalusersrow = mysql_fetch_array($getnormalusers);
			$normalusers = $normalusersrow['normalusers'] - $adminusers;
			echo "normalusers - ".$normalusers."<br/>";
		}
		else {
			$adminusers = 0;
			$normalusers = 0;
			echo "adminusers - ".$adminusers."<br/>";
			echo "normalusers - ".$normalusers."<br/>";
			
		}
		
		//calculate the size of the database and file repository
		mysql_select_db($businessdatabase);  
		$q = mysql_query("SHOW TABLE STATUS");  
		$size = 0;  
		while($row = mysql_fetch_array($q)) {  
		    $size += $row["Data_length"] + $row["Index_length"];  
		}
		$databasesize = number_format($size/(1024*1024),2);
		echo "databasesize - ".$databasesize."<br/>";
		
		$file_directory = '../../documents/'.$businessdatabase;
		$output = exec('du -sk ' . $file_directory);
		$repositorysize = trim(str_replace($file_directory, '', $output)) * 1024 / 1000000;
		echo "repositorysize - ".$repositorysize."<br/>";
		
		$totalsize = $repositorysize+$databasesize;
		echo "totalsize - ".$totalsize."<br/>";
		
		
		//create an invoicebreakdown record for the business
		$accountbillinginvoicebreakdownname = $businessid."-".$businessname."-".$date;
		$querybuilder = "insert into ".$masterdatabase.".accountbillinginvoicebreakdown (accountbillinginvoicebreakdownname, 
		accountbillinginvoiceid, businessname, adminuseramount, normaluseramount, databasesizeamount, disabled, datecreated)
		values ('$accountbillinginvoicebreakdownname','$accountbillinginvoiceid','$businessname','$adminusers',
		'$normalusers','$totalsize','0','$date')";
		echo $querybuilder;
		$insertinvoicebreakdown = mysql_query($querybuilder); 
		$accountbillinginvoicebreakdownid = mysql_insert_id();
		
		//check if they are using OneCustomer Twilio Integration and get costs
		include ('accountbillingcreateinvoicetwilio.php');
		
		//check if they are using OneCustomer Mailchimp Integration and get costs		
		include ('accountbillingcreateinvoicemailchimp.php');
			
	
	}
	
	
	//calculate the total number of admin users/normal users/database size from the invoicebreakdown table
	$querybuilder = "select sum(accountbillinginvoicebreakdown.adminuseramount) as adminusertotal, 
	sum(accountbillinginvoicebreakdown.normaluseramount) as normalusertotal, 
	sum(accountbillinginvoicebreakdown.databasesizeamount) as databasetotal,
	sum(accountbillinginvoicebreakdown.twiliototalcost) as twiliototal, 
	sum(accountbillinginvoicebreakdown.mailchimptotalcost) as mailchimptotal
	from ".$masterdatabase.".accountbillinginvoice 
	inner join ".$masterdatabase.".accountbillinginvoicebreakdown on accountbillinginvoicebreakdown.accountbillinginvoiceid = 
	accountbillinginvoice.accountbillinginvoiceid where accountbillinginvoicebreakdown.accountbillinginvoiceid = '$accountbillinginvoiceid'";
	$getinvoice = mysql_query($querybuilder);
	$rowinvoice = mysql_fetch_array($getinvoice);
	$adminusertotal = $rowinvoice['adminusertotal'];
	$normalusertotal = $rowinvoice['normalusertotal'];
	$databasetotal = $rowinvoice['databasetotal'];
	$twiliototal = $rowinvoice['twiliototal'];
	$mailchimptotal = $rowinvoice['mailchimptotal'];
	$databasetotal = round($databasetotal, 2);
	echo "adminusertotal - ".$adminusertotal."<br/>";
	echo "normalusertotal - ".$normalusertotal."<br/>";
	echo "databasetotal - ".$databasetotal."<br/>";
	
	//calculate the cost of the admin users
	$querybuilder = "select accountbillingadminuserrateid, rate from ".$masterdatabase.".accountbillingadminuserrate where disabled='0' and 
	accountbillingcurrencyid='$accountbillingcurrencyid' and lowvalue <= '$adminusertotal' and highvalue >= '$adminusertotal'";
	//echo $querybuilder;
	$getrate = mysql_query($querybuilder);
	$rowrate = mysql_fetch_array($getrate);
	$rateadminusers = $rowrate['rate'];
	if($rateadminusers <=0){
		$rateadminusers	= 0;
	}
	if($accountbillingdiscountpercentage <= 0){
		$accountbillingdiscountpercentage = 0;
	}
	else {
		$rateadminusers = round(($rateadminusers*((100-$accountbillingdiscountpercentage)/100)), 2);
	}
	echo "rateadminusers - ".$rateadminusers."<br/>";
	
	//calculate the cost of the normal users
	$querybuilder = "select accountbillingnormaluserrateid, rate from ".$masterdatabase.".accountbillingnormaluserrate where disabled='0' and 
	accountbillingcurrencyid='$accountbillingcurrencyid' and lowvalue <= '$normalusertotal' and highvalue >= '$normalusertotal'";
	//echo $querybuilder;
	$getrate = mysql_query($querybuilder);
	$rowrate = mysql_fetch_array($getrate);
	$ratenormalusers = $rowrate['rate'];
	if($ratenormalusers <=0){
		$ratenormalusers	= 0;
	}
	if($accountbillingdiscountpercentage <= 0){
		$accountbillingdiscountpercentage = 0;
	}
	else {
		$ratenormalusers = round(($ratenormalusers*((100-$accountbillingdiscountpercentage)/100)), 2);
	}
	echo "ratenormalusers - ".$ratenormalusers."<br/>";
	
	
	//calculate the cost of the database and file repository
	$querybuilder = "select accountbillingdatabasesizerateid, rate from ".$masterdatabase.".accountbillingdatabasesizerate where disabled='0' and 
	accountbillingcurrencyid='$accountbillingcurrencyid' and lowvalue <= '$databasetotal' and highvalue >= '$databasetotal'";
	//echo $querybuilder;
	$getrate = mysql_query($querybuilder);
	$rowrate = mysql_fetch_array($getrate);
	$ratedatabasesize = $rowrate['rate'];
	if($ratedatabasesize <=0){
		$ratedatabasesize	= 0;
	}
	if($accountbillingdiscountpercentage <= 0){
		$accountbillingdiscountpercentage = 0;
	}
	else {
		$ratedatabasesize = round(($ratedatabasesize*((100-$accountbillingdiscountpercentage)/100)), 2);
	}
	echo "ratedatabasesize - ".$ratedatabasesize."<br/>";
	
	
	//get remaining variables
	$totalrate = $ratedatabasesize+$rateadminusers+$ratenormalusers+$twiliototal+$mailchimptotal;	
	$gettaxrate = mysql_query("select taxrate from andrewnorth_onebusinessmaster.accountbillingcountry where accountbillingcountryid = '$accountbillingcountryid'");
	$gettaxraterow = mysql_fetch_array($gettaxrate);
	$taxpercentage = $gettaxraterow['taxrate'];
	$totalrateincludingtax = round(($totalrate * ($taxpercentage/100)) + $totalrate, 2);
	if($totalrate==0){
		$accountbillinginvoicestatusid = 7;	
	} 
	else {
		$accountbillinginvoicestatusid = 6;
	}
	
	//update the invoice record with the final cost values and set status to generating invoice (6)
	$querybuilder = "update ".$masterdatabase.".accountbillinginvoice set adminuserrate = '$rateadminusers', normaluserrate = 
	'$ratenormalusers', databasesizerate = '$ratedatabasesize', accountbillingcurrencyid = '$accountbillingcurrencyid', 
	adminuseramount = '$adminusertotal', normaluseramount = '$normalusertotal', databasesizeamount = '$databasetotal', 
	twiliototalcost = '$twiliototal', mailchimptotalcost = '$mailchimptotal',
	totalrate = '$totalrate', taxpercentage = '$taxpercentage', totalrateincludingtax = '$totalrateincludingtax', 
	accountbillinginvoicestatusid = '$accountbillinginvoicestatusid'
	where accountbillinginvoiceid = '$accountbillinginvoiceid'";	
	echo $querybuilder;
	$updateinvoice = mysql_query($querybuilder);
}

//include('accountbillingpdfinvoicegeneration.php');
?>