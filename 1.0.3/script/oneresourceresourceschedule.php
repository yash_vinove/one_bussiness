<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<?php

//LANGUAGE COLLECTION SECTION
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (4,5,9,10,19,582,583,584,585,586,587,588,589,590,591,592,593,594,595,596,597,598,599,600,601,602,603,
604,605,606,607,608,610,677,678,679,680,681,682,683,684,685,686,687,688,689,670,671,672,673,674,675,676,677,678,679,680,
681,682,683,684,685,686,687,688,689,690,691,692,693,694,695,696,697,698,699,700,701,702,703,704,705,706,707,708,709,710,
711,712,713,714,715,716,717,718,719,805,806,807,843,844,845,846)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}


//FILTERS
$pagename = $_SERVER['REQUEST_URI'];
$pagename = str_replace("&", "xxxxxxxxxx", $pagename);								

$displaytype = isset($_GET['displaytype']) ? $_GET['displaytype'] : '';
$resourceid = isset($_POST['resourceid']) ? $_POST['resourceid'] : '';
$resourcegroupid = isset($_POST['resourcegroupid']) ? $_POST['resourcegroupid'] : '';
$startdate = isset($_POST['startdate']) ? $_POST['startdate'] : '';
$enddate = isset($_POST['enddate']) ? $_POST['enddate'] : '';
if($resourcegroupid <> '' && $resourceid <> ''){
	$resourceid = '';
}
//echo "<br/>resourceid: ".$resourceid;
//echo "<br/>resourcegroupid: ".$resourcegroupid;
//echo "<br/>startdate: ".$startdate;
//echo "<br/>enddate: ".$enddate;


?>
<form class="form-horizontal" action='view.php?viewid=52' method="post" enctype="multipart/form-data">
	<div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
		<div class="form-group">
		 	<label for="Resource Group" class="col-xs-6 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval843 ?></label>
	    	<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
    			<?php
    			$resulttype = mysql_query("select resourcegroupid, resourcegroupname from resourcegroup where disabled = 0");
				echo "<select class='form-control' name='resourcegroupid'>";
		      	echo "<option value=''>Select Resource Group</option>";
         		while($resourcegrouprow=mysql_fetch_array($resulttype)){
         			if ($resourcegroupid == $resourcegrouprow['resourcegroupid']) {
               		echo "<option value=".$resourcegrouprow['resourcegroupid']." selected='true'>".$resourcegrouprow['resourcegroupname']."</option>";
          		}
          		else {
            			echo "<option value=".$resourcegrouprow['resourcegroupid']." >".$resourcegrouprow['resourcegroupname']."</option>";
          		}
             }
  				echo "</select>";
  				?>
	    	</div>
	  		<label for="Resource Group" class="col-xs-6 col-sm-4 col-md-2 col-lg-2 control-label"><?php echo $langval844 ?></label>
	    	<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
    			<?php
    			$resulttype = mysql_query("select resourceid, resourcename from resource where disabled = 0");
				echo "<select class='form-control' name='resourceid'>";
		      	echo "<option value=''>Select Resource</option>";
         		while($resourcerow=mysql_fetch_array($resulttype)){
         			if ($resourceid == $resourcerow['resourceid']) {
               		echo "<option value=".$resourcerow['resourceid']." selected='true'>".$resourcerow['resourcename']."</option>";
          		}
          		else {
            			echo "<option value=".$resourcerow['resourceid']." >".$resourcerow['resourcename']."</option>";
          		}
             }
  				echo "</select>";
  				?>
	    	</div>
	  	</div>
	  	<div class="form-group">
			<label for="Start Date" class="col-xs-6 col-sm-2 col-md-2 col-lg-2 control-label"><?php echo $langval805 ?></label>
	    	<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
	      	<input type="date" class="form-control" name="startdate" value="<?php echo $startdate ?>">
	    	</div>
	  		<label for="End Date" class="col-xs-6 col-sm-2 col-md-2 col-lg-2 control-label"><?php echo $langval806 ?></label>
	    	<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
	      	<input type="date" class="form-control" name="enddate" value="<?php echo $enddate ?>">
	    	</div>
	    	<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
	    		<div style="text-align:center;">
	      			<button type='submit' name='submit' value='Submit' class="button-primary"><?php echo $langval807 ?></button>		
	 			</div>
	    	</div>
	  	</div>
  	</div>
</form>	

<?php 
echo "<br/><br/>";

echo "<h2>$langval845</h2>";


$hobject = "select resourceallocationid as 'objectid', resourceallocationname as 'description', resource.resourceid, 
resourcename as 'objectname', progresspercent as 'progresspercentage',
startdatetime as 'startdate', enddatetime as 'enddate', resourcetypename as 'Type', projectname, taskname
from resource
inner join resourceallocation on resourceallocation.resourceid = resource.resourceid
inner join resourcetype on resourcetype.resourcetypeid = resource.resourcetypeid
left join project on resourceallocation.projectid = project.projectid
left join task on resourceallocation.taskid = task.taskid";
if($resourceid <> ''){
	$hobject = $hobject." where resource.resourceid = $resourceid";
}
if($resourcegroupid <> ''){
	$hobject = $hobject." inner join resourcegroupresource on resourcegroupresource.resourceid = resource.resourceid
	where resourcegroupresource.resourcegroupid = $resourcegroupid";
}
if($resourceid == '' && $resourcegroupid == ''){
	$hobject = $hobject." where resource.resourceid >= 0";
}
if($startdate <> '' || $enddate <> '') {
	//add dates to query
	if($startdate <> "" && $enddate <> ""){
		$hobject = $hobject." and enddatetime <= '".$enddate."' and startdatetime >= '".$startdate."'";
	}	
	if($startdate <> "" && $enddate == ""){
		$hobject = $hobject." and startdatetime >= '".$startdate."'";
	}	
	if($startdate == "" && $enddate <> ""){
		$hobject = $hobject." and enddatetime <= '".$enddate."'";
	}	
}
$hobject = $hobject." order by resource.resourceid, startdatetime limit 2000";
//echo "<br/>".$hobject;

//get the minimum and maximum dates for the columns
$mindate = date("Y-m-d");
$maxdate = date("Y-m-d");
$getobject = mysql_query($hobject);
$i = 1;
while ($dates = mysql_fetch_array($getobject)){
	$startdate = $dates['startdate'];
	$enddate = $dates['enddate'];
	if($i == 1){
		$mindate = $startdate;
	}
	else {
		if($startdate <= $mindate){
			$mindate = $startdate;		
		}	
	}
	if($i == 1){
		$maxdate = $enddate;
	}
	else {
		if($enddate >= $maxdate){
			$maxdate = $enddate;		
		}	
	}
	$i = $i + 1;
}
$maxd = strtotime($maxdate);
$mind = strtotime($mindate);
$dateDiff = $maxd - $mind;
$numdays = floor($dateDiff/(60*60*24));
$headingdates = '';
if($numdays <= 12){
	$thisdate = date('d-M', $mind);
	for($p = 0; $p <= $numdays; $p++) {
		$headingdates = $headingdates."<td style='border: 1px solid #D3D3D3;padding:5px;'><b>".$thisdate."</b></td>";
		$thisdate = date('d-M',date(strtotime("+1 day", strtotime($thisdate))));
	}	
}
if($numdays > 12 && $numdays <= 84){
	$thisdate = date('d-M', $mind);
	$num2 = $numdays / 7;
	for($p = 0; $p <= $num2; $p++) {
		$headingdates = $headingdates."<td colspan='7' style='border: 1px solid #D3D3D3;padding:5px;'><b>".$thisdate."</b></td>";
		$thisdate = date('d-M',date(strtotime("+7 day", strtotime($thisdate))));
	}	
}
if($numdays > 84 && $numdays <= 360){
	$thisdate = date('d-M-y', $mind);
	$num2 = ($numdays / 30) + 1;
	for($p = 0; $p <= $num2; $p++) {
		$colspan = date('t', strtotime($thisdate));
		$thisdate2 = date('M-y', strtotime($thisdate));
		$headingdates = $headingdates."<td colspan='".$colspan."' style='border: 1px solid #D3D3D3;padding:5px;'><b>".$thisdate2."</b></td>";
		$thisdate = date('d-M-y',date(strtotime("+1 month", strtotime($thisdate))));
	}	
}
if($numdays > 360){
	$thisdate = date('d-M-Y', $mind);
	$num2 = ($numdays / 365) +1;
	for($p = 0; $p <= $num2; $p++) {
		$colspan = date('z', strtotime($thisdate));
		$thisdate2 = date('Y', strtotime($thisdate));
		$headingdates = $headingdates."<td colspan='".$colspan."' style='border: 1px solid #D3D3D3;padding:5px;'><b>".$thisdate2."</b></td>";
		$thisdate = date('d-M-Y',date(strtotime("+1 year", strtotime($thisdate))));
	}	
}

//echo $mindate." - ".$maxdate." - ".$numdays."<br/>";
$pagename = $_SERVER['REQUEST_URI'];
$pagename = str_replace("&", "xxxxxxxxxx", $pagename);								
		
//create table of timeline
$getobject = mysql_query($hobject);
if(mysql_num_rows($getobject)>= 1){
	echo "<table style='border: 1px solid #D3D3D3;width:100%;overflow:scroll;'>";
	echo "<thead style='border: 1px solid #D3D3D3;'><td style='padding:5px;'><b>Resource Name</b></td>".$headingdates."</thead>";
	echo "<tr>";
	$numdays = $numdays + 1;
	for($s = 0; $s <= $numdays; $s++) {
		echo "<td style='color:white;font-size:1px;border: 0px solid #D3D3D3;''>I</td>";
	}	
	echo "</tr>";
	while ($object = mysql_fetch_array($getobject)){
		$objectid = $object['objectid'];
		$objectname = $object['objectname'];
		$startdate = $object['startdate'];
		$enddate = $object['enddate'];
		$projectname = $object['projectname'];
		$taskname = $object['taskname'];
		$description = $object['description'];
		$ppercentage = $object['progresspercentage'];
		$type = $object['Type'];
		//echo "<br/>".$type." - ".$objectname." - ".$startdate." - ".$enddate." - ".$description." - ".$ppercentage;
		echo "<tr><td style='width:20%;padding:5px;border-right: 1px solid #D3D3D3;''><b>".$objectname."</b> ";
		if($projectname <> ''){
			$name = $projectname;
			echo " - ".$projectname;
		}
		if($taskname <> ''){
			$name = $taskname;
			echo " - ".$taskname;
		}
		echo " <a href='pageedit.php?pagetype=resourceallocation&rowid=".$objectid."&pagename=".$pagename."'><i class='fa fa-pencil'></i></a>";
		echo "</td>";		
		$d = date("Y-m-d", strtotime($mindate));
		$rows = '';
		$startdate = date("Y-m-d", strtotime($startdate));
		$enddate = date("Y-m-d", strtotime($enddate));
		//echo "<br/>1: ".strtotime($d);
		//echo "<br/>2: ".strtotime($maxdate);
		//echo "<br/>2: ".$d;
		//echo "<br/>2: ".$startdate;
		while (strtotime($d) <= strtotime($maxdate)) {
			if($d == $startdate){
				$startd = strtotime($startdate);
				$endd = strtotime($enddate);
				$dateDiff2 = $endd - $startd;
				$nd = floor($dateDiff2/(60*60*24));
				if($nd == 0){
					$nd = 1;			
				}
				$title = $objectname."&#13;&#10;".$description."&#13;&#10;".$name."&#13;&#10;".$startdate." - ".$enddate."&#13;&#10;".$ppercentage."% ".$langval716;
				if($ppercentage==0){ $color = '#FF0000'; }
				if($ppercentage>=1 && $ppercentage <=10){ $color = '#FF0000'; }
				if($ppercentage>=11 && $ppercentage <=20){ $color = '#FF3300'; }
				if($ppercentage>=21 && $ppercentage <=30){ $color = '#ff6600'; }
				if($ppercentage>=31 && $ppercentage <=40){ $color = '#FFCC00'; }
				if($ppercentage>=41 && $ppercentage <=50){ $color = '#FFFF00'; }
				if($ppercentage>=51 && $ppercentage <=60){ $color = '#ccff00'; }
				if($ppercentage>=61 && $ppercentage <=70){ $color = '#99ff00'; }
				if($ppercentage>=71 && $ppercentage <=80){ $color = '#66ff00'; }
				if($ppercentage>=81 && $ppercentage <=90){ $color = '#33ff00'; }
				if($ppercentage>=91 && $ppercentage <=100){ $color = '#00FF00'; }
				$rows = $rows."<td title='".$title."' colspan='".$nd."' style='background-color:".$color.";color:".$color.";'></td>";				
			}      
			else {
				
				$rows = $rows."<td></td>";	
			} 	
       	$d = date ("Y-m-d", strtotime("+1 day", strtotime($d)));
       	//echo "<br/>d: ".$d;
		}
		echo $rows;
		echo "</tr>";
	}
	echo "</table>";
	echo "<table class='table table-bordered'>";
	echo "<tr><td>";
	echo "<a class='button-primary' href='pageadd.php?pagetype=resourceallocation&pagename=".$pagename."'>$langval846</a>";
	echo "</td></tr>";
	echo "</table>";
	
}
?>