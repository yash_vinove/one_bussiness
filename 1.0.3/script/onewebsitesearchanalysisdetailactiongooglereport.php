<?php

//LANGUAGE COLLECTION SECTION
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (211,212,219,220,221,222,223,224,225,226,227,228,229)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}

//generate json file temporarily
$database10 = str_replace("andrewnorth_", '', $database);
$url = "../documents/".$database10."/service-account-credentials.json";
$fp = fopen($url,"wb");
fwrite($fp,$apikey2);
fclose($fp);
$KEY_FILE_LOCATION = '../documents/'.$database10.'/service-account-credentials.json';
require_once __DIR__ .'../../googleanalytics/google-api-php-client-2.2.0/vendor/autoload.php';
	
//connect to google analytics
$client = new Google_Client();
$client->setApplicationName("Hello Analytics Reporting");
$client->setAuthConfig($KEY_FILE_LOCATION);
$client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
$analytics = new Google_Service_Analytics($client);

$date = date("Y-m-d");
$startdate = date("Y-m-01");
$maxdate = date ("Y-m-d", strtotime("-6 months", strtotime($startdate)));

//get historical visitor overview data
while($startdate >= $maxdate){
 	$enddate = date ("Y-m-d", strtotime("+1 month", strtotime($startdate)));
 	$enddate = date ("Y-m-d", strtotime("-1 day", strtotime($enddate)));
 	$params = array('dimensions' => 'ga:source,ga:medium,ga:keyword');
	$results =  $analytics->data_ga->get(
		'ga:' . $apikey1,
		$startdate,
	  	$enddate,
	  	'ga:sessions',
	  	$params
	);
		
	if (count($results->getRows()) > 0) {
		//delete existing record if there is one
		$deleterecord = mysql_query("delete from $database.websitesearchgoogleorganicsources
		where createddate = '$startdate' and websitesearchgoogleid = '$websitesearchgoogleid'");
		$rows = $results->getRows();
		//echo json_encode($rows)."<br/><br/>";	
		//echo "<table style='border:1px solid #A0A0A0;'>"; 
		for($i=0; $i<count($results->getRows()); $i++){
		 	$source = $rows[$i][0];
		 	$medium = $rows[$i][1];
		 	$keyword = $rows[$i][2];
		 	$sessions = $rows[$i][3];
		 	$source = ucfirst($source);
		 	if($keyword = "(not provided)"){
				$keyword = 'Not Provided';		 	
		 	}
		 	if($medium == 'organic'){
			 	//update database with values
			 	$updatelocation = mysql_query("insert into $database.websitesearchgoogleorganicsources (
			 	websitesearchgoogleid, keywordused, source, createddate, noofsessions) values 
				('$websitesearchgoogleid', '$keyword', '$source', '$startdate', '$sessions')");    
			 	//echo "<tr><td>".$startdate."</td><td>".$enddate."</td><td>".$source."</td><td>".$medium."</td><td>".$sessions."</td><td>".$keyword."</td></tr>";
		 	}			 		
	 	}
	 //echo "</table><br/><br/>";			 	
	}
	else {
	 	//print "No results found.\n";
	}
	$startdate = date ("Y-m-01", strtotime("-1 month", strtotime($startdate)));
}

//delete json file
unlink($url);

//generate report
$includeurl = "fpdf181/fpdf.php";
require($includeurl);

class PDF extends FPDF
{
	// Page header
	function Header()
	{
	    // Arial bold 15
	    $this->SetFont('Arial','B',13);
	}	
	// Page footer
	function Footer()
	{
	    // Position at 1.5 cm from bottom
	    $this->SetY(-15);
	    // Arial italic 8
	    $this->SetFont('Arial','I',8);
	    // Page number
	    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	}
}

//date created
$date = date("d-M-Y");
$date2 = date("Y-m-d");
$mindate = date ("Y-m-d", strtotime("-6 months", strtotime($date2)));

$getrecord = mysql_query("select * from $database.websitesearchanalysis 
inner join $database.website on website.websiteid = websitesearchanalysis.onewebsiteid
where websitesearchanalysisid = '$rowid'");
$getrecordrow = mysql_fetch_array($getrecord);
$websitesearchanalysisid = $getrecordrow['websitesearchanalysisid'];
$websitename = $getrecordrow['websitename'];
$websiteurl = $getrecordrow['websiteurl'];

//Instantiation of inherited class
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','B',13);
$pdf->Cell(75);
$pdf->Cell(40,10,$langval219.$websitename,0,0,'C');
$pdf->Ln(15);
$pdf->SetFont('Arial','',10);
$pdf->MultiCell(0,5,$langval220,0,1);
$pdf->Ln(5);
$pdf->SetFont('Arial','',10);
$pdf->MultiCell(0,5,$langval211.$date,0,1);
$pdf->MultiCell(0,5,$langval212.$websiteurl,0,1);

$pdf->Ln(5);
$pdf->SetFont('Arial','U',12);
$pdf->Cell(0,10,$langval221,0,1);
$pdf->SetFont('Arial','',10);
$pdf->MultiCell(0,5,$langval222,0,1);
$pdf->Ln(6);
$y = $pdf->GetY();
$x = $pdf->GetX();
$pdf->SetFont('Arial','B',10);
$pdf->Cell(60, 6, $langval223, 1, 'L', FALSE);
$y = $pdf->GetY();
$x = $pdf->GetX();
$pdf->SetXY($x, $y);
$pdf->Cell(60,6, $langval224, 1, 0, "l");	
$pdf->Ln(6);
$getrecord3 = mysql_query("select createddate, sum(noofsessions) as numsessions from $database.websitesearchgoogleorganicsources 
where websitesearchgoogleid = '$websitesearchgoogleid'  and createddate >= '$mindate' 
group by createddate order by createddate desc");
while($getrecord3row = mysql_fetch_array($getrecord3)){
	$createddate = $getrecord3row['createddate'];
	$createddate = date("M-Y", strtotime($createddate));
	$noofsessions = $getrecord3row['numsessions'];
	$pdf->SetFont('Arial','',10);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$width = 60;
	$pdf->Cell($width, 6, $createddate, 1, 'L', FALSE);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->SetXY($x, $y);
	$pdf->Cell(60,6, $noofsessions, 1, 0, "l");	
	$pdf->Ln(6);
}

$pdf->Ln(5);
$pdf->SetFont('Arial','U',12);
$pdf->Cell(0,10,$langval225,0,1);
$pdf->SetFont('Arial','',10);
$pdf->MultiCell(0,5,$langval226,0,1);
$pdf->Ln(6);
$y = $pdf->GetY();
$x = $pdf->GetX();
$pdf->SetFont('Arial','B',10);
$pdf->Cell(60, 6, $langval227, 1, 'L', FALSE);
$datearray = array();
$mindate3 = date ("Y-m-01", strtotime("-5 months", strtotime($date2)));
$mindate2 = $mindate3;
while ($mindate2 <= $date2){
	array_push($datearray, $mindate2);
	$mindate2 = date ("Y-m-01", strtotime("+1 month", strtotime($mindate2)));
}
foreach ($datearray as $key => $value) {
	$finaldate = date("M-y", strtotime($value));
   	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->SetXY($x, $y);
	$pdf->Cell(20,6, $finaldate, 1, 0, "l");	
}
$pdf->SetFont('Arial','',10);
$savedkeyword = '';
$getrecord4 = mysql_query("select keywordused, sum(noofsessions) as numsessions
from $database.websitesearchgoogleorganicsources 
where websitesearchgoogleid = '$websitesearchgoogleid'  and createddate >= '$mindate3' 
group by keywordused order by keywordused asc");
while($getrecord4row = mysql_fetch_array($getrecord4)){
	$noofsessions = $getrecord4row['numsessions'];
	$keywordused = $getrecord4row['keywordused'];
	$pdf->Ln(6);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->Cell(60, 6, $keywordused, 1, 'L', FALSE);	
	foreach ($datearray as $key => $value) {
		$getrecord9 = mysql_query("select sum(noofsessions) as numsessions, createddate
		from $database.websitesearchgoogleorganicsources 
		where websitesearchgoogleid = '$websitesearchgoogleid'  and createddate = '$value'
		and keywordused = '$keywordused' group by createddate");
		if(mysql_num_rows($getrecord9) >= 1){
			$getrecord9row = mysql_fetch_array($getrecord9);
			$val = $getrecord9row['numsessions'];
		}
		else {
			$val = 0;
		}
		$y = $pdf->GetY();
		$x = $pdf->GetX();
		$pdf->SetXY($x, $y);
		$pdf->Cell(20,6, $val, 1, 0, "l");	
	}
}

$pdf->Ln(11);
$pdf->SetFont('Arial','U',12);
$pdf->Cell(0,10,$langval228,0,1);
$pdf->SetFont('Arial','',10);
$pdf->MultiCell(0,5,$langval229,0,1);
$pdf->Ln(6);
$y = $pdf->GetY();
$x = $pdf->GetX();
$pdf->SetFont('Arial','B',10);
$pdf->Cell(60, 6, $langval227, 1, 'L', FALSE);
$y = $pdf->GetY();
$x = $pdf->GetX();
$pdf->SetXY($x, $y);
$pdf->Cell(60,6, $langval224, 1, 0, "l");	
$pdf->Ln(6);
$getrecord7 = mysql_query("select sum(noofsessions) as numsessions, keywordused 
from $database.websitesearchgoogleorganicsources 
where websitesearchgoogleid = '$websitesearchgoogleid'  and createddate >= '$mindate' 
group by keywordused order by numsessions desc");
while($getrecord7row = mysql_fetch_array($getrecord7)){
	$noofsessions = $getrecord7row['numsessions'];
	$keywordused = $getrecord7row['keywordused'];
	$pdf->SetFont('Arial','',10);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$width = 60;
	$pdf->Cell($width, 6, $keywordused, 1, 'L', FALSE);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->SetXY($x, $y);
	$pdf->Cell(60,6, $noofsessions, 1, 0, "l");	
	$pdf->Ln(6);
}

//uncomment this, if you want to see an example in the browser for testing. However, comment it out once you are finished otherwise the whilst loop will not run.
//$pdf->Output();



//check if onewebsite storage folder exists, if not, then create
$filedir = '../documents/'. $database."/sf/websitesearchgoogle/";
if(file_exists($filedir)){
}
else {
	$file_directory = "../documents/".$database."/sf/websitesearchgoogle/";
	mkdir($file_directory);
}

$filename="../documents/".$database."/sf/websitesearchgoogle/".$websitesearchgoogleid." - Website Google Keywords Report.pdf";
$pdf->Output($filename,'F');

//create document record in structurefunctiondocument
$recordname = $websitesearchgoogleid." - Website Google Keywords Report.pdf";
$docurl = "../documents/".$database."/sf/websitesearchgoogle/".$recordname;
$insertdocrecord = mysql_query("insert into $database.structurefunctiondocument (structurefunctiondocumentname, datecreated, 
documenturl, doctypename, structurefunctionid, rowid) values ('$recordname', '$date2', '$docurl', 
'Google Analytics', '227', '$websitesearchgoogleid')");

//update status as in progress
$checkstatus = mysql_query("select * from $database.websitesearchanalysistask 
where websitesearchanalysisid = '$websitesearchanalysisid' and websitesearchtaskid = '6'");
$checkstatusrow = mysql_fetch_array($checkstatus);
$websitesearchtaskstatusid = $checkstatusrow['websitesearchtaskstatusid'];
$updatestatus = mysql_query("update $database.websitesearchanalysistask set websitesearchtaskstatusid = '2', 
completedby = 0, completeddate = '0000-00-00'  
where websitesearchanalysisid = '$websitesearchanalysisid' and websitesearchtaskid = '$websitesearchtaskid'");

?>