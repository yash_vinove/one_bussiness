<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$date = date('Y-m-d');
$datetime = date('Y-m-d H:i:s');	

const API_BASE_URL = 'https://ship.amazon.co.uk/api/v2/sandbox/{accountId}'; // update account id
const GRANT_TYPE = 'client_credentials';
const AMZ_SHIP_CLIENT_SECRET = '44f8c14a3debcd63b586c7d9add399a8ea0c44370dbe52ccdf575f298e3d4002';
const AMZ_SHIP_CLIENT_ID = 'amzn1.application-oa2-client.0e6bff35a2f44dfb843342ebf94b5377';
const SCOPE = 'ship::sandbox:api';
const ACCOUNT_ID = '9711673717';

$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);

$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
//echo "<br/>Get batching stats: ";
//echo "<br/>differencemilliseconds: ".$differencemilliseconds;
//echo "<br/>sqlqueriestime: ".$sqlqueriestime;

    $getorders = "SELECT ecommerceorder.ecommerceorderid, customer.customerid, ecommercelisting.ecommercelistingid, ecommercelistinglocation.ecommercelistinglocationid, GROUP_CONCAT(ecommerceorderitem.ecommerceorderitemid SEPARATOR ', ') AS ecomorderitemids, ecommercelistinglocation.integrationorderid, fulfillmentaddress1, fulfillmentaddress2, fulfillmentaddresstowncity, fulfillmentaddressstatecounty, fulfillmentaddresspostzipcode, shippingoptionname, customername, phonenumber, emailaddress, GROUP_CONCAT(product.productid SEPARATOR ', ') AS productids, SUM(product.fulfillmentpackageweight) AS packageweight, SUM(product.fulfillmentpackagewidth) AS packagewidth, SUM(product.fulfillmentpackageheight) AS packageheight, SUM(product.fulfillmentpackagelength) AS packagelength FROM ecommerceorder 
	INNER JOIN ecommerceorderitem ON ecommerceorderitem.ecommerceorderid = ecommerceorder.ecommerceorderid
	INNER JOIN ecommercelistinglocation ON ecommercelistinglocation.ecommercelistinglocationid = ecommerceorderitem.ecommercelistinglocationid
	INNER JOIN ecommercelisting ON ecommercelisting.ecommercelistingid = ecommerceorderitem.ecommercelistingid
	INNER JOIN productstockitem ON productstockitem.productstockitemid = ecommerceorderitem.productstockitemid
	INNER JOIN product ON product.productid = productstockitem.productid
	INNER JOIN customer ON customer.customerid = ecommerceorder.customerid
	WHERE ecommerceorder.ecommercesiteconfigid = 1000001 AND ecommercelistinglocation.fulfillmentpartnerid = 1000002 AND ecommercelisting.despatchdate = '0000-00-00'";

	echo "<br/>getorders: ".$getorders;
	$getorders = mysql_query($getorders);
    while($getordersrow = mysql_fetch_array($getorders))
    {
        $token = fetchToken();
        echo "<br/><br/>".$token;
        die();
        // $accessToken = $token->access_token; // need checking
        $createShipment = createShipment($oken);

    }

echo "<br/><br/>";

function fetchToken()
{
    $url = 'https://api.amazon.com/auth/o2/token';
    $header = array(
        "Content-Type: application/json"
    );
    $postFields = [
        "grant_type" => GRANT_TYPE,
        "client_secret" => AMZ_SHIP_CLIENT_SECRET,
        "client_id" => AMZ_SHIP_CLIENT_ID,
        "scope" => SCOPE
    ];

    echo "<br/><br/>postfields: ".json_encode($postFields)."<br/>";
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($postFields),
        CURLOPT_HTTPHEADER => $header,
    ));
    
    $response = curl_exec($curl);
    $err = curl_error($curl);
    
    curl_close($curl);
    
    if ($err) {
        echo "<br/><br/>cURL Error #:" . $err;
        die();
    } else {
        return $response;
    }
}

function createShipment($token)
{
    $url = API_BASE_URL."/shipments";
    $header = array(
        "Content-Type: application/json",
        "Authorization: Bearer ".$token,
        "x-business-identifier: AmazonShipping_UK"
        // optional param x-transaction-identifier
    );

    // $finalobject = [
    //     "businessType" => "",
    //     "consignor" => [], // object -- optional
    //     "shipTo" => [], // object
    //     "containers" => [], // array
    //     "expectedServiceOffering" => [], // object -- optional
    //     "clientReferenceId" => "",
    //     "shipFrom" => [], // object
    //     "returnTo" => [], // object -- optional -- defaultTo shipFrom field
    //     "shippingPurpose" => ""
    // ];

    $finalobject = [
        "businessType" => "B2C",
        "consignor" => null, // object -- optional
        "shipTo" => [
            "name" => "A1",
            "addressLine1" => "Test",
            "addressLine2" => "Test", // optional
            "addressLine3" => "Test", // optional
            "postalCode" => "DN1 1QZ",
            "city" => "Doncaster",
            "countryCode" => "GB",
            "email" => "sample@email.com", // optional
            "phoneNumber" => "444-444-444" // optional
        ], // object
        "containers" => [
            [
                "containerType" => null,
                "identifiers" => [
                    "containerReferenceId" => "12",
                    "clientReferenceId" => "12212121121212121221"
                ],
                "items" => [
                    [
                        "qualifiers" => array(
                            [
                                "name" => "ORDER_SOURCE",
                                "value" => "AMAZON_UK"
                            ],
                            [
                                "name" => "ORDER_ID",
                                "value" => "789-4562457-6845712"
                            ],
                            [
                                "name" => "ASIN",
                                "value" => "B00CBU0J12"
                            ]
                        ),
                        "title" => "string",
                        "quantity" => 2,
                        "unitPrice" => [
                            "unit" => "GBP",
                            "value" => 14.99
                        ],
                        "unitWeight" => [
                            "unit" => "kg",
                            "value" => 4
                        ]
                    ]
                ],
                "dimensions" => [
                    "height" => [
                        "unit" => "CM",
                        "value" => 12
                    ],
                    "lenght" => [
                        "unit" => "CM",
                        "value" => 36
                    ],
                    "weight" => [
                        "unit" => "kg",
                        "value" => 4
                    ],
                    "width" => [
                        "unit" => "CM",
                        "value" => 31
                    ],
                ],
                "value" => [
                    "unit" => "GBP",
                    "value" => "29.98"
                ]
            ]
        ], // array
        // "expectedServiceOffering" => [], // object -- optional
        "clientReferenceId" => "DUMMY",
        "shipFrom" => [
            "name" => "A1",
            "addressLine1" => "Test",
            "addressLine2" => "Test",
            "addressLine3" => "Test",
            "postalCode" => "DN1 1QZ",
            "city" => "Doncaster",
            "countryCode" => "GB",
            "email" => "sample@email.com",
            "phoneNumber" => "444-444-444"
        ], // object
        // "returnTo" => [], // object -- optional -- defaultTo shipFrom field
        "shippingPurpose" => "SALE"
    ];

    echo "<br/><br/>".json_encode($finalobject);
}

function confirmShipment($shipmentId, $serviceOfferingToken)
{
    $url = API_BASE_URL."/shipments/".$shipmentId."/confirm";
    $header = array(
        "Content-Type: application/json",
        "Authorization: Bearer ".$token,
        "x-business-identifier: AmazonShipping_UK"
        // optional param x-transaction-identifier
    );

    $postFields = json_encode([
        "serviceOfferingToken" => $serviceOfferingToken
    ]);


}

function getShippingLabel($shipmentId, $trackingId)
{
    $url = API_BASE_URL."/shipments/".$shipmentId."/containers/".$trackingId."/label";
    $header = array(
        "Content-Type: application/json",
        "Authorization: Bearer ".$token,
        "x-business-identifier: AmazonShipping_UK"
        // optional param x-transaction-identifier
    );

}

?>