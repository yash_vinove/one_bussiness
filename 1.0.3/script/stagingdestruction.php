<?php session_start(); ?>
<?php include('../config.php'); ?>
<?php include('../sessionconfig.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	
 	<title>Staging Destruction</title>

 	<!-- Bootstrap -->
 	<link href="bootstrap-3.3.1/dist/css/bootstrap.css" rel="stylesheet">

 	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
 	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">   
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
	<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script type="text/javascript">
    function redirectMe (sel) {
        var url = sel[sel.selectedIndex].value;
        window.location = url;
    }
    </script>
    <link rel='stylesheet' type='text/css' href='../style.php' />
</head>
  
<body class='body'>
	<?php include_once ('../headerthree.php');	?>
	<?php
	//LANGUAGE COLLECTION SECTION
	
	$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
	and languagerecordid = '32'");
	$langrow = mysql_fetch_array($lang);
	$langval32 = $langrow['languagerecordtextname'];
	$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
	and languagerecordid = '33'");
	$langrow = mysql_fetch_array($lang);
	$langval33 = $langrow['languagerecordtextname'];
	$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
	and languagerecordid = '34'");
	$langrow = mysql_fetch_array($lang);
	$langval34 = $langrow['languagerecordtextname'];
	$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
	and languagerecordid = '35'");
	$langrow = mysql_fetch_array($lang);
	$langval35 = $langrow['languagerecordtextname'];
	
	?>
	
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:10px;">
		<!--search results header bar for desktop-->
		<div class='bodyheader'>
			<h3><?php echo $langval32 ?></h3>
		</div>
			<?php 
			$rowid = isset($_GET['rowid']) ? strip_tags($_GET['rowid']) : '';
			$submit = isset($_POST['submit']) ? $_POST['submit'] : ''; 
			if($submit) {
				
				//delete business from database
				$selectdatabasename = mysql_query("select * from business where businessid='$rowid'");
				$databasenamerow = mysql_fetch_array($selectdatabasename);
				$databasename = $databasenamerow['databasename'];
				$deletebusiness = mysql_query("delete from business where businessid = '$rowid'");		
				
				//delete database
				$sql = mysql_query('DROP DATABASE '.$databasename);
				
				$dir = '../../documents/'.$databasename;
				//echo $dir;
				//echo is_dir($dir);
			    if( is_dir($dir) ) { 
			    	//echo "here";
			    	$objects = array_diff( scandir($dir), array('..', '.') );
			      foreach ($objects as $object) { 
			        $objectPath = $dir."/".$object;
			        if( is_dir($objectPath) )
			          recursive_rmdir($objectPath);
			        else
			          unlink($objectPath); 
			      } 
			      rmdir($dir); 
			    } 
					
				//go to homepage
	     		$url = '../pagegrid.php?pagetype=business';
	      		echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">'; 
			}
			?>				
			
			<div class='bodycontent'>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<?php 
	  				if(isset($validate) && $validate <> "") {
						echo "<p class='background-warning'>".$validate."</p>";
					} 
					?>
				
			<p><?php echo $langval33 ?> <br/><br/><?php echo $langval33 ?> <br/><br/><?php echo $langval33 ?> <br/>
			<br/> <?php echo $langval34 ?> </p>
			<br/>
			</div>
			<form class="form-horizontal" action='stagingdestruction.php?rowid=<?php echo $rowid?>' method="post">
			  	<div class="form-group">
			    	<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
			    		<div style="text-align:center;">
			      		<button type='submit' name='submit' value='Submit' class="button-primary"><?php echo $langval35 ?></button>																									
			    		</div>
			    	</div>
			  	</div>
			</form> 
			<br/>
			
			
					<br/><br/><br/>
				</div>
	  		</div>
		</div>
			
	</div>	
	<?php include_once ('../footer.php'); ?>
</body>
