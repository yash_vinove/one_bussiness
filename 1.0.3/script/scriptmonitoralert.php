<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$date = date('Y-m-d');

//delete all stat records that are older than 11 days
$date11 = date('Y-m-d', strtotime("- 11 days"));
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$insert = mysql_query("delete from scheduledjobstat where startdatetime <= '$date11'");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Delete old records: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;		

//GET LIST OF PEOPLE TO SEND EMAIL TO 
echo "<br/><br/><b>LIST OF PEOPLE TO SEND EMAIL TO</b>";
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$getemail = mysql_query("select * from scheduledjobstatalert 
where disabled = 0");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get list of emails to send email to: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
$sendto = array();
while($getemailrow = mysql_fetch_array($getemail)){
	$fullname = $getemailrow['fullname'];
	$emailaddress = $getemailrow['emailaddress'];
	array_push($sendto, array("name"=>$fullname,"email"=>$emailaddress));
}
echo "<br/><br/>sendto: ".json_encode($sendto);

if(count($sendto) >= 1){
	//GET LIST OF CRON JOB FAILURES
	echo "<br/><br/><b>LIST OF CRON JOB FAILURES IN LAST 24 HOURS</b>";
	$date1 = date('Y-m-d H:i:s', strtotime("- 1 days"));
	$date2 = date('Y-m-d H:i:s', strtotime("- 5 minutes"));
	echo "<br/>after: ".$date1;
	echo "<br/>before: ".$date2;
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getcronjobfailures = mysql_query("select scheduledjobstatname, startdatetime from scheduledjobstat 
	where enddatetime = '0000-00-00 00:00:00' and startdatetime >= '$date1' and startdatetime <= '$date2'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get list of cron job failures: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	$cronjobfailures = array();
	while($getcronjobfailuresrow = mysql_fetch_array($getcronjobfailures)){
		$scheduledjobstatname = $getcronjobfailuresrow['scheduledjobstatname'];
		$parts  = explode('/', $scheduledjobstatname);
		$scheduledjobstatname = implode('/', array_slice($parts, 4, 10));
		$scheduledjobstatname = str_replace("script/", "", $scheduledjobstatname);
		$startdatetime = $getcronjobfailuresrow['startdatetime'];
		array_push($cronjobfailures, array("scheduledjobstatname"=>$scheduledjobstatname,"startdatetime"=>$startdatetime));
	}
	echo "<br/><br/>cronjobfailures: ".json_encode($cronjobfailures);
	
	//GET GRID OF CRON JOB STATS - 1 DAY
	echo "<br/><br/><b>LIST OF CRON JOB STATISTICS</b>";
	$date1 = date('Y-m-d H:i:s', strtotime("- 1 days"));
	echo "<br/>after: ".$date1;
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getcronjobstats = "select scripturl as 'Script', count(scheduledjobstatid) as 'No Runs', 
	round(avg(executiontime), 2) as 'Avg Execution Time', round(avg(nosqlqueries), 0) as 'Avg No SQL Queries', 
	round(avg(sqlqueriestime), 2) as 'Avg SQL Queries Time', 
	SUM(CASE WHEN enddatetime = '0000-00-00 00:00:00' THEN 1 ELSE 0 END) as 'No of Failures' 
	from scheduledjobstat
	where startdatetime >= '$date1'
	group by scripturl ASC";
	echo $getcronjobstats;
	$getcronjobstats = mysql_query($getcronjobstats);
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get list of cron job stats: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	$cronjobstatsone = array();
	while($getcronjobstatsrow = mysql_fetch_array($getcronjobstats)){
		$script = $getcronjobstatsrow['Script'];
		$parts  = explode('/', $script);
		$script = implode('/', array_slice($parts, 4, 10));
		$noruns = $getcronjobstatsrow['No Runs'];
		$avgexecutiontime = $getcronjobstatsrow['Avg Execution Time'];
		$avgnosqlqueries = $getcronjobstatsrow['Avg No SQL Queries'];
		$avgsqlqueriestime = $getcronjobstatsrow['Avg SQL Queries Time'];
		$nooffailures = $getcronjobstatsrow['No of Failures'];
		array_push($cronjobstatsone, array("script"=>$script,"noruns"=>$noruns,"avgexecutiontime"=>$avgexecutiontime,
		"avgnosqlqueries"=>$avgnosqlqueries,"avgsqlqueriestime"=>$avgsqlqueriestime,"nooffailures"=>$nooffailures));
	}
	echo "<br/><br/>cronjobstatsone: ".json_encode($cronjobstatsone);
	
	
	//GET GRID OF CRON JOB STATS - 10 DAYS
	echo "<br/><br/><b>LIST OF CRON JOB STATISTICS</b>";
	$date10 = date('Y-m-d H:i:s', strtotime("- 10 days"));
	echo "<br/>after: ".$date1;
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getcronjobstats = mysql_query("select scripturl as 'Script', count(scheduledjobstatid) as 'No Runs', 
	round(avg(executiontime), 2) as 'Avg Execution Time', round(avg(nosqlqueries), 0) as 'Avg No SQL Queries', 
	round(avg(sqlqueriestime), 2) as 'Avg SQL Queries Time', 
	SUM(CASE WHEN enddatetime = '0000-00-00 00:00:00' THEN 1 ELSE 0 END) as 'No of Failures' 
	from scheduledjobstat ".$sqlqueriestime;
	$cronjobstats = array();
	// Getting the cron job stats
	while($getcronjobstatsrow = mysql_fetch_array($getcronjobstats)){
		$script = $getcronjobstatsrow['Script'];
		$parts  = explode('/', $script);
		$script = implode('/', array_slice($parts, 4, 10));
		$noruns = $getcronjobstatsrow['No Runs'];
		$avgexecutiontime = $getcronjobstatsrow['Avg Execution Time'];
		$avgnosqlqueries = $getcronjobstatsrow['Avg No SQL Queries'];
		$avgsqlqueriestime = $getcronjobstatsrow['Avg SQL Queries Time'];
		$nooffailures = $getcronjobstatsrow['No of Failures'];
		array_push($cronjobstats, array("script"=>$script,"noruns"=>$noruns,"avgexecutiontime"=>$avgexecutiontime,
		"avgnosqlqueries"=>$avgnosqlqueries,"avgsqlqueriestime"=>$avgsqlqueriestime,"nooffailures"=>$nooffailures));
	}
	echo "<br/><br/>cronjobstats: ".json_encode($cronjobstats);
	
	//CONSTRUCT EMAIL
	foreach($sendto as $person){	
		$fullname = $person['name'];
		$emailaddress = $person['email'];
		//EMAIL TEXT
		echo "<br/><br/><b>EMAIL TEXT TO BE SENT</b>";
		$emailtext = "Hi ".$fullname." <br/><br/> Here are your daily stats for ".$database.".";
		$emailtext = $emailtext."<br/><br/><b>FAILED SCRIPTS IN LAST 24 HOURS</b><br/>";
		if(count($cronjobfailures) >= 1){
			foreach($cronjobfailures as $failure){
				$scheduledjobstatname = $failure['scheduledjobstatname'];		
				$startdatetime = $failure['startdatetime'];
				$emailtext = $emailtext."<br/>".$startdatetime." - ".$scheduledjobstatname;		
			}
		}
		else {
			$emailtext = $emailtext."No failures to report.";		
		}
		
		$emailtext = $emailtext."<br/><br/><b>OVERALL STATS FOR LAST 24 HOURS</b><br/>";		
		if(count($cronjobstats) >= 1){
			$emailtext = $emailtext."<table><tr><td>Script</td><td>No Runs</td><td>Avg Execution Time</td>";
			$emailtext = $emailtext."<td>Avg No SQL Queries</td><td>Avg SQL Queries Time</td><td>No Of Failures</td></tr>";
			foreach($cronjobstatsone as $stat){
				$script = $stat['script'];		
				$noruns = $stat['noruns'];		
				$avgexecutiontime = $stat['avgexecutiontime'];		
				$avgnosqlqueries = $stat['avgnosqlqueries'];		
				$avgsqlqueriestime = $stat['avgsqlqueriestime'];		
				$nooffailures = $stat['nooffailures'];		
				
				$emailtext = $emailtext."<tr><td>".$script."</td><td>".$noruns."</td><td>".$avgexecutiontime;		
				$emailtext = $emailtext."</td><td>".$avgnosqlqueries."</td><td>".$avgsqlqueriestime."</td><td>";
				$emailtext = $emailtext.$nooffailures."</td></tr>";		
			}
			$emailtext = $emailtext."</table>";
		}
		else {
			$emailtext = $emailtext."No scheduled jobs ran in last day.";		
		}
		
		$emailtext = $emailtext."<br/><br/><b>OVERALL STATS FOR LAST 10 DAYS</b><br/>";		
		if(count($cronjobstats) >= 1){
			$emailtext = $emailtext."<table><tr><td>Script</td><td>No Runs</td><td>Avg Execution Time</td>";
			$emailtext = $emailtext."<td>Avg No SQL Queries</td><td>Avg SQL Queries Time</td><td>No Of Failures</td></tr>";
			foreach($cronjobstats as $stat){
				$script = $stat['script'];		
				$noruns = $stat['noruns'];		
				$avgexecutiontime = $stat['avgexecutiontime'];		
				$avgnosqlqueries = $stat['avgnosqlqueries'];		
				$avgsqlqueriestime = $stat['avgsqlqueriestime'];		
				$nooffailures = $stat['nooffailures'];		
				
				$emailtext = $emailtext."<tr><td>".$script."</td><td>".$noruns."</td><td>".$avgexecutiontime;		
				$emailtext = $emailtext."</td><td>".$avgnosqlqueries."</td><td>".$avgsqlqueriestime."</td><td>";
				$emailtext = $emailtext.$nooffailures."</td></tr>";		
			}
			$emailtext = $emailtext."</table>";
		}
		else {
			$emailtext = $emailtext."No scheduled jobs ran in last 10 days.";		
		}
		$emailtext = $emailtext."<br/><br/>Regards, Administrator";
		echo "<br/><br/>".$emailtext;
		
		//SEND EMAIL
		/*
		echo "<br/><br/><b>SEND EMAIL</b>";
		//require_once($versionname.'/PHPMailer/class.phpmailer.php');
		require_once($versionname.'/PHPMailer/PHPMailerAutoload.php');
		$email5 = new PHPMailer();
		$email5->SMTPDebug = 2;       
	  	$email5->isSMTP();                           
	   	//$email5->Host = $smtphost;  
	   	$email5->Host = 'mail.mumblebooks.co.uk';  
	   	$email5->SMTPAuth = true; 
	   	$email5->Username = 'info@mumblebooks.co.uk';           
	   	$email5->Password = 'Mumblebooks2018';                         
	   	$email5->SMTPSecure = 'tls';    
	   	$email5->SMTPOptions = array(
		'ssl' => array(
		'verify_peer' => false,
		'verify_peer_name' => false,
		'allow_self_signed' => true
		)
		);                      
	   	$email5->Priority = 1;                          
 		$email5->Port = 587;                                    
		$email5->From      = 'info@mumblebooks.co.uk';
		$email5->FromName  = 'OneBusiness';
		$email5->AddReplyTo('info@mumblebooks.co.uk', 'OneBusiness');
		$email5->Subject   = 'Scheduled Job Stats - '.$database;
		$email5->Body      = $emailtext;
		$email5->IsHTML(TRUE);
		$email5->AddAddress($emailaddress);
		$email5->Send();
		echo "<br/>Email Sent to ".$fullname;
		*/
		
		echo "<br/><br/><b>SEND EMAIL</b>";
		$subject = 'Scheduled Job Stats - '.$database;
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: <info@mumblebooks.co.uk>' . "\r\n";
		mail($emailaddress,$subject,$emailtext,$headers);
	}
}

echo "<br/><br/>";
?>