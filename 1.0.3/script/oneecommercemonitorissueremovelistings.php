<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include('1000003 - config.php');

// $hour = date('H');
// $minute = date('i');
$date = date('Y-m-d');
// echo "<br/>minute: ".$minute;
// echo "<br/>hour: ".$hour;

// Get OneEcommerceMonitor Type Config ID
$sql1 = "SELECT ecommercemonitortypeconfigid FROM ecommercemonitortypeconfig WHERE 
ecommercemonitortypeconfigname = 'Listing Hijack in Amazon' ";

$sql1 = mysql_query($sql1);
$ecommercemonitortypeconfigid = NULL;
while($row1 = mysql_fetch_array($sql1))
{
    $ecommercemonitortypeconfigid = $row1['ecommercemonitortypeconfigid'];
}
echo "<br/><br/> ecommercemonitortypeconfigid : ".$ecommercemonitortypeconfigid;
if($ecommercemonitortypeconfigid != NULL)
{
    $deleteidsarray = array();
    $includearray = array(1000007, 1000010);

    // ecommercemonitorissues with productid
    $sql2 = "SELECT ecommercemonitorissueid, ecommercemonitorissue.productid AS product, productrightsownerid FROM ecommercemonitorissue
    INNER JOIN product ON product.productid = ecommercemonitorissue.productid
    WHERE ecommercemonitortypeconfigid = $ecommercemonitortypeconfigid";

    $sql2 = mysql_query($sql2);

    while($row2 = mysql_fetch_array($sql2))
    {
        if(!in_array($row2['productrightsownerid'], $includearray ))
        {
            array_push($deleteidsarray, $row2['ecommercemonitorissueid']);
        }
    }

    // ecommercemonitorissues with ecommercelistingid
    $sql3 = "SELECT ecommercemonitorissueid, ecommercemonitorissue.ecommercelistingid, ecommercelisting.productstockitemid, productstockitem.productid, product.productrightsownerid 
    FROM ecommercemonitorissue
    INNER JOIN ecommercelisting ON ecommercelisting.ecommercelistingid = ecommercemonitorissue.ecommercelistingid
    INNER JOIN productstockitem ON productstockitem.productstockitemid = ecommercelisting.productstockitemid
    INNER JOIN product ON product.productid = productstockitem.productid
    WHERE ecommercemonitortypeconfigid = $ecommercemonitortypeconfigid";

    $sql3 = mysql_query($sql3);

    while($row3 = mysql_fetch_array($sql3))
    {
        if(!in_array($row3['productrightsownerid'], $includearray ))
        {
            array_push($deleteidsarray, $row3['ecommercemonitorissueid']);
        }
    }

    // ecommercemonitorissues with ecommercelistinglocationid
    $sql4 = "SELECT ecommercemonitorissueid, ecommercemonitorissue.ecommercelistinglocationid, ecommercelistinglocation.productid, product.productrightsownerid 
    FROM ecommercemonitorissue
    INNER JOIN ecommercelistinglocation ON ecommercelistinglocation.ecommercelistinglocationid = ecommercemonitorissue.ecommercelistinglocationid
    INNER JOIN product ON product.productid = ecommercelistinglocation.productid
    WHERE ecommercemonitortypeconfigid = $ecommercemonitortypeconfigid";

    $sql4 = mysql_query($sql4);

    while($row4 = mysql_fetch_array($sql4))
    {
        if(!in_array($row4['productrightsownerid'], $includearray ))
        {
            array_push($deleteidsarray, $row4['ecommercemonitorissueid']);
        }
    }

    // ecommercemonitorissues with productstockitemid
    $sql5 = "SELECT ecommercemonitorissueid, ecommercemonitorissue.productstockitemid, productstockitem.productid, product.productrightsownerid 
    FROM ecommercemonitorissue
    INNER JOIN productstockitem ON productstockitem.productstockitemid = ecommercemonitorissue.productstockitemid
    INNER JOIN product ON product.productid = productstockitem.productid
    WHERE ecommercemonitortypeconfigid = $ecommercemonitortypeconfigid";

    $sql5 = mysql_query($sql5);

    while($row5 = mysql_fetch_array($sql5))
    {
        if(!in_array($row5['productrightsownerid'], $includearray ))
        {
            array_push($deleteidsarray, $row5['ecommercemonitorissueid']);
        }
    }

    echo "<br/><br/> deleteidsarray : ".json_encode($deleteidsarray);

    if(count($deleteidsarray) > 0)
    {
        $ids = implode(',', $deleteidsarray);
        $deletesql = "DELETE FROM ecommercemonitorissue WHERE ecommercemonitorissueid IN ($ids)";
        mysql_query($deletesql);
    }
}

// Remove/update listings from ecommercelistinglocationstat where numberoflistings > 1 & belong to same ASIN

$idsarray = array();

$sql6 = "SELECT ecommercelistinglocationstatid, ecommercelistinglocationstat.ecommercelistinglocationid, 
numberofsellersonlisting, ecommercelistinglocation.productid 
FROM ecommercelistinglocationstat 
INNER JOIN ecommercelistinglocation ON ecommercelistinglocation.ecommercelistinglocationid = ecommercelistinglocationstat.ecommercelistinglocationid
WHERE numberofsellersonlisting > 1";

$sql6 = mysql_query($sql6);

while($row6 = mysql_fetch_array($sql6))
{
    array_push($idsarray, array(
        'ecommercelistinglocationstat' => $row6['ecommercelistinglocationstatid'],
        'product' => $row6['productid']
    ));
    // Get productid
    // Check zzproductdata table & find asin1
    // Again search asin1 in zzproductdata and get productid
    // if the value is greater than 1, delete entries from ecommercemonitorissue table
}

$deletearray = array();

if(count($idsarray) > 0)
{
    $start = microtime(true);
    foreach($idsarray as $input)
    {
        echo "<br/> Input : ".$input['product'];

        $sql7 = "SELECT asin1 FROM zzproductdata WHERE productid = ".$input['product'];
        $sql7 = mysql_query($sql7);
        while($row7 = mysql_fetch_array($sql7))
        {
            $asin = $row7['asin1'];
            echo "<br/> ASIN : ".$asin;

            $sql8 = "SELECT productid FROM zzproductdata WHERE asin1 like '$asin' ";
            $sql8 = mysql_query($sql8);
            if(mysql_num_rows($sql8) > 1)
            {
                // while($row8 = mysql_fetch_array($sql8))
                // {
                //     echo "<br/> * ".$row8['productid']." *";
                // }
                // array_push($updatearray, $input['ecommercelistinglocationstat']);
                array_push($deletearray, $input['product']);

            }
        }
    }
    $end = microtime(true);
    echo "<br/>Time taken : ".($end-$start);

    if(count($deletearray) > 0)
    {
        $ids = implode(',', $deletearray);
        $deletesql1 = "DELETE FROM ecommercemonitorissue WHERE productid IN ($ids)";
        mysql_query($deletesql1);
    }
}

echo "<br/><br/>Data : ".json_encode($idsarray);

echo "<br/><br/>";
include('1000005 - recordjobstatistics.php');

?>