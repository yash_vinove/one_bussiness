<?php

echo "here - ebay";

//set api credentials
$devid = $apikey1;
$appid = $apikey2;
$certid = $apikey3;
$serverurl = $datacentre;
$usertoken = $apikey4;
$siteid = $apikey5;

//echo $devid." - ".$appid." - ".$certid." - ".$serverurl." - ".$usertoken." - ".$siteid."<br/><br/>";

$endpoint = 'http://open.api.ebay.com/Shopping';  // URL to call
$responseEncoding = 'XML';   // Format of the response
$apicall = "$endpoint?callname=GetCategoryInfo"
     . "&appid=$appid"
     . "&siteid=$siteid"
     . "&CategoryID=-1"
     . "&version=677"
     . "&IncludeSelector=ChildCategories";

// Load the call and capture the document returned by the GetCategoryInfo API
$xml = simplexml_load_file($apicall);
echo $apicall;

$errors = $xml->Errors;

if($errors->count() > 0)
{
    echo '<p><b>eBay returned the following error(s):</b></p>';
    //display each error
    //Get error code, ShortMesaage and LongMessage
    $code = $errors->ErrorCode;
    $shortMsg = $errors->ShortMessage;
    $longMsg = $errors->LongMessage;
    //Display code and shortmessage
    echo '<p>', $code, ' : ', str_replace(">", "&gt;", str_replace("<", "&lt;", $shortMsg));
    //if there is a long message (ie ErrorLevel=1), display it
    if(count($longMsg) > 0)
        echo '<br>', str_replace(">", "&gt;", str_replace("<", "&lt;", $longMsg));
}
else //no errors
{
	echo "<ul>";
	foreach($xml->CategoryArray->Category as $cat){
		if($cat->CategoryLevel!=0):
			echo '<li><"'.$cat->CategoryID.'">'.$cat->CategoryName.'</li>';
		endif;
	}
	echo "</ul>";
}

?>

