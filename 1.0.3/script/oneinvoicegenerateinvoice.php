<?php session_start(); ?>
<?php
include('../config.php');
include('../sessionconfig.php');
$date = date("Y-m-d");
$rowid = isset($_GET['rowid']) ? strip_tags($_GET['rowid']) : '';
require('../fpdf181/fpdf.php');
	
	class PDF extends FPDF
	{
		
	// Page header
	function Header()
	{
	    // Arial bold 15
	    $this->SetFont('Arial','B',13);
	}
	
	
	// Page footer
	function Footer()
	{
	    // Position at 1.5 cm from bottom
	    $this->SetY(-15);
	    // Arial italic 8
	    $this->SetFont('Arial','I',8);
	    // Page number
	    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	}
	
	}

//check if oneinvoice installed
$checkinstall = mysql_query("select table_name from information_schema.TABLES where table_name like 'invoice' 
and table_schema like '$database'");
if(mysql_num_rows($checkinstall) >= 1){
	
	//check if invoice already exists
	$checkinvoiceitem = mysql_query("select * from invoiceitem 
	where financialtransactionid = $rowid");
	if(mysql_num_rows($checkinvoiceitem) >= 1){
		$checkinvoiceitemrow = mysql_fetch_array($checkinvoiceitem);
		$invoiceid = $checkinvoiceitemrow['invoiceid'];	
	}
	else {
		$invoiceid = 0;	
	}
	
	//echo "<br/>invoiceid: ".$invoiceid;

	//get the financialtransaction details 
	$financialtransaction = mysql_query("select * from financialtransaction where financialtransactionid = $rowid");
	$financialtransactionrow = mysql_fetch_array($financialtransaction);
	$financialtransactionname = mysql_real_escape_string($financialtransactionrow['financialtransactionname']);
	$businessunitid = $financialtransactionrow['businessunitid'];
	$customerid = $financialtransactionrow['customerid'];
	$currencyid = $financialtransactionrow['currencyid'];
	$amount = $financialtransactionrow['amount'];
	$salestaxid = $financialtransactionrow['salestaxid'];
	$amountincludingtax = $financialtransactionrow['amountincludingtax'];
	
	//if invoice does not exist then create the records in oneinvoice
	if($invoiceid == 0){
		//create invoice record
		$createinvoice = mysql_query("insert into invoice (invoicename, dateissued, customerid, businessunitid, disabled, datecreated, masteronly) 
		values ('Invoice - $financialtransactionname', '$date', '$customerid', '$businessunitid', '0', '$date', '0')");
		$invoiceid = mysql_insert_id();

		//create invoiceitem record
		$createinvoiceitem = mysql_query("insert into invoiceitem (invoiceitemname, financialtransactionid, invoiceid, customerid, 
		businessunitid, currencyid, amount, salestaxid, amountincludingtax, disabled, datecreated, masteronly) 
		values ('$financialtransactionname', '$rowid', '$invoiceid', '$customerid', '$businessunitid', '$currencyid', '$amount', '$salestaxid', 
		'$amountincludingtax', '0', '$date', '0')");	
	}
	else {
		//update financialtransaction item value
		$updateinvoiceitem = mysql_query("update invoiceitem set currencyid = '$currencyid', amount = '$amount', 
		salestaxid = '$salestaxid', amountincludingtax = '$amountincludingtax', invoiceitemname = '$financialtransactionname', 
		customerid = '$customerid', businessunitid = '$businessunitid' 
		where financialtransactionid = '$rowid'");	
		
	}
	
	include('oneinvoicebuildinvoice.php');
	$pdf->Output();
}
else {
	echo "<br/><br/>Please install OneInvoice, in order to automatically create invoices in OneFinance.";
}


?>