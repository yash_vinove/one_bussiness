<?php

//LANGUAGE COLLECTION SECTION
$languageid = mysql_real_escape_string($_SESSION['languageid']);
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $languageid
and languagerecordid in (7,19,20,27,28,29,30,31,64)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}

$success = isset($_GET['success']) ? $_GET['success'] : '';
$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
$currentpassword = isset($_POST['currentpassword']) ? strip_tags($_POST['currentpassword']) : '';
$newpassword = isset($_POST['newpassword']) ? strip_tags($_POST['newpassword']) : '';
$repeatnewpassword = isset($_POST['repeatnewpassword']) ? strip_tags($_POST['repeatnewpassword']) : '';
if($submit){
	echo "<br/>currentpassword: ".$currentpassword; 	
	echo "<br/>newpassword: ".$newpassword; 	
	echo "<br/>repeatnewpassword: ".$repeatnewpassword; 	
	
	$validate = "";
	
	//check if newpassword and repeatnewpassword match
	if($newpassword <> $repeatnewpassword){
		$validate = $validate."Your New Password does not match your Repeat New Password. ";	
	}
	
	//check if currentpassword matches password in database
	$currentpassword = md5($currentpassword);
	$userid = mysql_real_escape_string($_SESSION['userid']);
	$getpassword = mysql_query("select password, firstname, lastname, username from user where userid = '$userid'");
	$passwordrow = mysql_fetch_array($getpassword);
	$password = $passwordrow['password'];
	$firstname = $passwordrow['firstname'];
	$lastname = $passwordrow['lastname'];
	$username = $passwordrow['username'];
	//echo "<br/>password: ".$password;
	if($password <> $currentpassword){
		$validate = $validate."The Current Password you entered does not match your current password you logged in with. ";		
	}
	
	//check if newpassword is the same as a password used in last 6 months
	$newpasswordmd5 = md5($newpassword);
	$userid = mysql_real_escape_string($_SESSION['userid']);
	$getusedpassword = mysql_query("select * from userlogin where userid = '$userid' and passwordused = '$newpasswordmd5'");
	if(mysql_num_rows($getusedpassword) >= 1){
		$validate = $validate."Your New Password has been used in the last 6 months - please pick another password. ";		
	}
	
	//check if newpassword is more than 8 characters
	if(strlen($newpassword) < 8){
		$validate = $validate."Your New Password has to be at least 8 characters. ";		
	}
	
	//check if newpassword contains one number
	if (strcspn($newpassword, '0123456789') == strlen($newpassword)){
		$validate = $validate."Your New Password must contain at least one number. ";		
	}
	
	//check if newpassword contains one letter
	if (strcspn($newpassword, 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') == strlen($newpassword)){
		$validate = $validate."Your New Password must contain at least one letter. ";		
	}
		
	//check if newpassword contains one special character
	if (strcspn($newpassword, ';:,.$%£#@!()-_|') == strlen($newpassword)){
		$validate = $validate."Your New Password must contain at least one of these characters - ;:,.$%£#@!()-_| ";		
	}
	
	//check if newpassword contains users first or last name
	if(stripos($newpassword, $firstname) !== false){
		$validate = $validate."Your New Password cannot contain your firstname, lastname, or username ";		
	}
	if(stripos($newpassword, $lastname) !== false){
		$validate = $validate."Your New Password cannot contain your firstname, lastname, or username ";		
	}
	if(stripos($newpassword, $username) !== false){
		$validate = $validate."Your New Password cannot contain your firstname, lastname, or username ";		
	}

	if($validate == ""){
		//update the password into the database
		$newpasswordmd5 = mysql_real_escape_string(md5($newpassword));
		$userid = mysql_real_escape_string($_SESSION['userid']);
		$updatepassword = mysql_query("update user set password = '$newpasswordmd5' where userid = '$userid'");	
		
		$url = 'view.php?viewid=45&success=1';
	   echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">'; 
	}
}

if(isset($validate) && $validate <> "") {
	echo "<p class='background-warning'>".$validate."</p>";
} 
if($success == 1) {
	echo "<br/><p class='background-success'>Your password has been changed.</p>";
} 
?>
<p>Please input your current password and your new password. Please note the following requirements: </p>
<ul>
	<li>Your Current Password must be the password you are logged in with.</li>
	<li>Your New Password cannot be the same as a password used in the last 6 months.</li>
	<li>Your New Password must be at least 8 characters.</li>
	<li>Your New Password must match the Repeat New Password.</li>
	<li>Your New Password must contain at least one number, one letter, and one of these characters - ;:,.$%£#@!()-_|</li>
	<li>Your New Password cannot contain your firstname, lastname, or username.</li>
</ul>


<form class="form-horizontal" action='view.php?viewid=45' method="post">
	<div class="col-xs-12 col-md-7 col-sm-8 col-lg-6">
		<div class="form-group">
			<label for="Current Password" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Current Password *</label>
		   	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
		      	<input type="password" class="form-control" name="currentpassword" value="">
		  	</div>
	  	</div>
	  	<div class="form-group">
			<label for="New Password" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">New Password *</label>
	    	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
	      		<input type="password" class="form-control" name="newpassword" value="">
	      	</div>
	  	</div>
	  	<div class="form-group">
			<label for="Repeat New Password" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Repeat New Password *</label>
	    	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
	      		<input type="password" class="form-control" name="repeatnewpassword" value="">
	      	</div>
	  	</div>	  	
		<div class="form-group">
	    	<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
	    		<div style="text-align:center;">
		      		<button type='submit' name='submit' value='Submit' class="button-primary"><?php echo $langval20 ?></button>																									
	 			</div>
	    	</div>
	  	</div>
  	</div>
</form>	

<br/><br/><br/><br/><br/><br/><br/>
<br/><br/><br/><br/><br/><br/><br/>
<br/><br/><br/><br/><br/><br/><br/>
<br/><br/><br/>