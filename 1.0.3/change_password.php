<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link href="./fontAwesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="screen" title="stylesheet" />
      <title>OneBusiness</title>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel='stylesheet' type='text/css' href='css/sidebar.css' />
      <link rel='stylesheet' type='text/css' href='css/content.css' />
      <link rel='stylesheet' type='text/css' href='css/header.css' />
      <script type="text/javascript" src="https://www.google.com/jsapi"></script>	
   </head>
   <body class="content">
      <!----------------------------------------- sidebar -------------------------------- -->
      <div class="sidebar">
         <nav class="main-menu">
            <ul >
               <li><i class="fa fa-long-arrow-left fa-2x arrow icon_click"></i></li>
            </ul>
            <ul class="navbar-nav flex-column w-100 mt-5">
               <li class="nav-item ">
                  <img src="img/icons/my-apps.png" class="icon_click dropdown">
                  <a class="nav-link" href="my_app.php"  >My Apps</a>
               </li>
               <div class="dropdown-divider"></div>
               <!-- Second Li Start-->
               <li class="nav-item ">
                  <img src="img/icons/dashboard-icon.png" class="icon_click dropdown">
                  <a class="nav-link" href="#"  >
                  Dashboard
                  </a>
               </li>
               <!-- Second Li Close-->
               <div class="dropdown-divider"></div>
               <li class="nav-item active">
                  <img src="img/icons/reports-icon.png" class="icon_click dropdown">
                  <a class="nav-link" href="reports.php"  >
                  Reports</a>
               </li>
               <div class="dropdown-divider"></div>
               <li class="nav-item ">
                  <img src="img/icons/my-details-icon.png" class="icon_click dropdown">
                  <a class="nav-link" href="#"  >
                  My Details 
                  </a>
               </li>
               <div class="dropdown-divider"></div>
               <li class="nav-item ">
                  <img src="img/icons/users-icon.png" class="icon_click dropdown">
                  <a class="nav-link" href="#"  >
                  Users  
                  </a>
               </li>
               <div class="dropdown-divider"></div>
               <li class="nav-item ">
                  <img src="img/icons/site-builder-icon.png" class="icon_click dropdown">
                  <a class="nav-link" href="#"  >
                  Site Billing  
                  </a>
               </li>
               <div class="dropdown-divider"></div>
               <li class="nav-item ">
                  <img src="img/icons/translation-icon.png" class="icon_click dropdown">
                  <a class="nav-link" href="#"  >Translation </a>
               </li>
               <div class="dropdown-divider"></div>
               <li class="nav-item ">
                  <img src="img/icons/security-icon.png" class="icon_click dropdown">
                  <a class="nav-link" href="#"  >
                  Security
                  </a>
               </li>
               <div class="dropdown-divider"></div>
               <li class="nav-item ">
                  <img src="img/icons/fave-icon-selected.png" class="icon_click dropdown">
                  <a class="nav-link" href="#" data-toggle="collapse" data-target="#navDashboard7" aria-expanded="false" aria-controls="navDashboard7" >
                     Favourites 
                     <span class="ml-auto">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down">
                           <polyline points="6 9 12 15 18 9"></polyline>
                        </svg>
                     </span>
                  </a>
                  <div id="navDashboard7" class="collapse" data-parent="#sidebarNav">
                     <ul class="nav flex-column nav_1">
                        <li class="nav-item ">
                           <a class="nav-link" href="#">Stock</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" href="#">Manage</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" href="#">Finance</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" href="#">Product</a>
                        </li>
                        <li class="link">
                           <p>Manage Qick Link <img src="img/icons/add-icon.png" width="20px"></p>
                        </li>
                     </ul>
                  </div>
               </li>
            </ul>
         </nav>
      </div>
      <!----------------------------------------- sidebar -------------------------------- -->
      <!----------------------------------------- content -------------------------------- -->
      <section class="content reports">
         <div class='overlay'></div>
         <div class="container">
            <div class="row mt-5 header">
               <div class="col-sm-6">
                  <img src="img/Apps.png" width="250px">
               </div>
               <div class="col-sm-6">
                  <p class="text-right txt_1">21st August 2020</p>
               </div>
            </div>
            <div class="row mt-5 dekstop">
               <div class="col-xs-3">
                  <img src="img/icons/burger-menu.png" class="img-responsive icon_click">
               </div>
               <div class="col-xs-6">
                  <img src="img/Apps.png" class="img-responsive">
               </div>
               <div class="col-xs-3">
                  <div class="header_profile ">
                     <img src="img/profile.png" class="img-responsive left_click ">
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12">
                  <div class="line"></div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12">
                 <h1 class="head_title">Add User Roles Permissions</h1>
                 
               </div>
            </div>
         <div class="back_board h-20">
            <div class="row">    
               <div class="col-sm-6">
                  <p>Please input your current password and new password. Please note the following requirement.</p>
                  <ul>
                     <li>Your Current Password must be the password you are logged in with.</li>
                     <li>Your New Password connot be the same as a password used in the last 6 months.</li>
                     <li>Your New Password must be at least 8 characters. </li>
                     <li>Your New Password must match the Repeat new Password. </li>
                     <li>Your New Password must contain at least one number, one letter, and one of these characters -;:,.$%£#@,()_I </li>
                     <li>Your New Password cannot contain your firstname, lastname, or usemame </li>
                  </ul>
               </div>
               <div class="col-sm-6">
                  <div class="form_background">
                        <form>
                           <div class="form-group">
                              <label >Current Password</label>
                              <input type="text" class="form-control"  >
                           </div>
                           <div class="form-group">
                              <label >New Password</label>
                              <input type="text" class="form-control" >
                           </div>
                           <div class="form-group">
                              <label >Repeat New Password</label>
                              <input type="text" class="form-control" >
                           </div>
                        </form>
                  </div>
                        
               </div>
            </div>
         </div>
      </section>
      <!----------------------------------------- content -------------------------------- -->
      <!----------------------------------------- left sidebar -------------------------------- -->
      <div class="left-sidebar">
         <nav class="left-menu">
            <ul class="">
               <li>
                  <a href="#">
                     <div class="noti left_click mt-5">
                        <div class="profile ">
                           <img src="img/profile.png" class="img-responsive">
                        </div>
                        <span class="notification">5</span>
                     </div>
                  </a>
               </li>
            </ul>
            <ul class="logout">
               <li class="remove_click">
                  <a href="#">
                  <i class="fa fa-long-arrow-right fa-2x"></i>
                  </a>
               </li>
               <div class="main_logout">
                  <p>Logout</p>
                  <div class="profile ">
                     <img src="img/profile.png" class="img-responsive">
                  </div>
                  <div class="txt_3">
                     <h3>Andrew Smith</h3>
                     <p>Director</p>
                  </div>
               </div>
               <li style="display:flex; margin-top:20px"> <img src="img/icons/edit-details.png" class="pl">
                  <a  href="#" > Edit My Details  </a>
               </li>
               <!-- <div class="dropdown-divider"></div> -->
               <li style="display:flex; margin-top:20px"> <img src="img/icons/change-password.png" class="pl">
                  <a href="#" >Change Password</a>
               </li>
            </ul>
            <ul>
            </ul>
         </nav>
      </div>
      <!----------------------------------------- left sidebar -------------------------------- -->
      <script src="js/sidebar.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
      <script src="js/calender.js"></script>
   </body>
</html>