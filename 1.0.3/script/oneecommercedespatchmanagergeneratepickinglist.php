<?php session_start(); ?>
<?php include('../config.php'); ?>
<?php include('../sessionconfig.php'); ?>
<?php

$selldate = isset($_GET['selldate']) ? $_GET['selldate'] : ''; 
$businessunitid = isset($_GET['businessunitid']) ? $_GET['businessunitid'] : ''; 
$storagelocationname = isset($_GET['storagelocationname']) ? $_GET['storagelocationname'] : ''; 
$storagelocationsectionname = isset($_GET['storagelocationsectionname']) ? $_GET['storagelocationsectionname'] : ''; 
$orderdatetimehour = isset($_GET['orderdatetimehour']) ? $_GET['orderdatetimehour'] : ''; 
$orderid = isset($_GET['orderid']) ? $_GET['orderid'] : ''; 

$includeurl = "../fpdf181/fpdf.php";
require($includeurl);

class PDF extends FPDF{}

//check if businessunitid field exists and user is not admin user	
$querybuilder = "select ecommerceorder.ecommerceorderid, orderdatetime, sellcustomerid, businessunitname, customername, firstname, 
lastname, fulfillmentaddress1, fulfillmentaddress2, fulfillmentaddresstowncity, fulfillmentaddressstatecounty, 
fulfillmentaddresspostzipcode, tb1.countryname as countrynamefulfillment, billingaddress1, billingaddress2, 
billingaddresstowncity, billingaddressstatecounty, billingaddresspostzipcode, sellcurrencyid, 
productname, count(ecommercelistinglocation.ecommercelistinglocationid) as 'numproducts', storagelocationsectionname, recipientname
from ecommerceorder 
inner join ecommerceorderitem on ecommerceorderitem.ecommerceorderid = ecommerceorder.ecommerceorderid
inner join ecommercelisting on ecommercelisting.ecommercelistingid = ecommerceorderitem.ecommercelistingid 
inner join ecommercelistinglocation on ecommercelistinglocation.ecommercelistingid = ecommercelisting.ecommercelistingid
inner join businessunit on businessunit.businessunitid = ecommercelisting.businessunitid
inner join customer on customer.customerid = ecommercelisting.sellcustomerid
inner join country as tb1 on tb1.countryid = ecommercelisting.fulfillmentaddresscountryid
inner join productstockitem on productstockitem.productstockitemid = ecommercelisting.productstockitemid
inner join storagelocationsection on storagelocationsection.storagelocationsectionid = productstockitem.storagelocationsectionid
inner join storagelocation on storagelocation.storagelocationid = storagelocationsection.storagelocationid
inner join product on productstockitem.productid = product.productid
where ecommercelistingstatusid in (2,3) and ecommercelistinglocationstatusid in (5,13)";
if($selldate <> '') {
	$querybuilder = $querybuilder." and ecommercelisting.selldate = '$selldate'";
}
if($businessunitid <> '') {
	$querybuilder = $querybuilder." and ecommercelisting.businessunitid = $businessunitid";
}
if($_SESSION["userbusinessunit"] <> '' && $_SESSION["superadmin"] == 0){
  	$querybuilder = $querybuilder." and ecommercelisting.businessunitid in (".$_SESSION["userbusinessunit"].")";
}
if($storagelocationname <> '') {
	$querybuilder = $querybuilder." and storagelocationname = '$storagelocationname'";
}
if($storagelocationsectionname <> '') {
	$querybuilder = $querybuilder." and storagelocationsectionname = '$storagelocationsectionname'";
}
if($orderdatetimehour <> '') {
	$querybuilder = $querybuilder." and HOUR(orderdatetime) = '$orderdatetimehour'";
}
if($orderid <> '') {
	$querybuilder = $querybuilder." and ecommerceorder.ecommerceorderid = $orderid";
}
$querybuilder = $querybuilder." group by sellcustomerid, businessunitname, sellcurrencyid, product.productid, fulfillmentaddress1, 
fulfillmentaddresspostzipcode 
order by sellcustomerid";
//echo $querybuilder;
$querybuilder = mysql_query($querybuilder);

$pdf = new PDF();

// Instantiation of inherited class
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','B',13);
$pdf->Cell(75);
$pdf->Cell(40,10,'Picking List - '.$selldate,0,0,'C');
$pdf->Ln(15);
$pdf->SetFont('Arial','',10);
	
while($row22=mysql_fetch_array($querybuilder)){
	$customerid = $row22['sellcustomerid'];
	$businessunit = $row22['businessunitname'];
	$customername = $row22['customername'];
	$salt = "h3f8s9en20vj3";
	$customername = openssl_decrypt($customername,"AES-128-ECB",$salt);
	$recipientname = $row22['recipientname'];
	if($recipientname <> ""){
		$customername = $recipientname;	
	}
	$firstname = $row22['firstname'];
	$lastname = $row22['lastname'];
	$fulfillmentaddress1 = $row22['fulfillmentaddress1'];
	$fulfillmentaddress2 = $row22['fulfillmentaddress2'];
	$fulfillmentaddresstowncity = $row22['fulfillmentaddresstowncity'];
	$fulfillmentaddressstatecounty = $row22['fulfillmentaddressstatecounty'];
	$fulfillmentaddresspostzipcode = $row22['fulfillmentaddresspostzipcode'];
	$fulfillmentaddresscountryname = $row22['countrynamefulfillment'];
	$numproducts = $row22['numproducts'];
	$storagelocationsectionname = $row22['storagelocationsectionname'];
	$productname = $row22['productname'];
	$address = $fulfillmentaddress1.", ".$fulfillmentaddress2.", ".$fulfillmentaddresstowncity.", ".$fulfillmentaddressstatecounty.", ".$fulfillmentaddresspostzipcode.", ".$fulfillmentaddresscountryname;
	
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->MultiCell(190, 6, $customername.', '.$address."\n".$storagelocationsectionname.' - '.$productname.' - Number Ordered: '.$numproducts, 1, 'L', FALSE);
	$pdf->Ln(6);
	
}

//uncomment this, if you want to see an example in the browser for testing. However, comment it out once you are finished otherwise the whilst loop will not run.
$pdf->Output();

?>				
