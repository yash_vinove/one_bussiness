<?php 
//get structure detail
$getstructurefunction = mysql_query("select * from structurefunction where structurefunctionid = '$recordid'");
$getstructurefunctionrow = mysql_fetch_array($getstructurefunction);
$tablename = $getstructurefunctionrow['tablename'];
$namefield = $getstructurefunctionrow['primarykeyfield'];
$structurefunctionname = $getstructurefunctionrow['structurefunctionname'];
$gridorderdirection = $getstructurefunctionrow['gridorderdirection'];
$gridorderfield = $getstructurefunctionrow['gridorderfield'];
$datefilterfield = $getstructurefunctionrow['datefilterfield'];
$tablename = str_replace(' ', '', $tablename);
if($_SESSION['accesslevel'] == 'Business'){
	$check1 = $tablename."id";
	$check2 = $tablename."name";
	if($gridorderfield == $check1){
		$gridorderfield = "zz".$check1;	
	}
	if($gridorderfield == $check2){
		$gridorderfield = "zz".$check2;	
	}
	$tablename = "zz".$tablename;
}
$idname = $tablename."id";

//gridorderfield - if input is not asc/desc then set to asc
if($gridorderdirection == 'asc' || $gridorderdirection == 'desc'){
}
else {
	$gridorderdirection = 'asc';
}

//datefilterfield - default to datecreated if nothing input
if($datefilterfield == ""){
	$datefilterfield = 'datecreated';
}

//tablename - remove spaces and decapitalise
//primarykeyfield - update with tablename.id
$updatename = mysql_query("update structurefunction set tablename = '$tablename', datefilterfield = '$datefilterfield',
structurefunctionname = '$structurefunctionname', gridorderdirection = '$gridorderdirection', 
uploaderactive = '1', primarykeyfield = '$idname', gridorderfield = '$gridorderfield' where structurefunctionid = '$recordid'");


//create table
$querybuilder = "create table ".$database.".".$tablename." ( ".$idname." int not null auto_increment, ".$tablename."name text not null, 
disabled tinyint not null, datecreated date not null, masteronly tinyint not null, primary key (".$idname."))";
//echo $querybuilder;
$addtable = mysql_query($querybuilder);


//auto add structurefield records for disabled, id, and name field
$name = $tablename."name";
$structurefunctionid = $structurefunctionname." ID";
$structurefunctionname = $structurefunctionname." Name";
$insertfielddisabled = mysql_query("insert into structurefield (structurefunctionid, structurefieldtypeid, structurefieldname, displayname, 
fieldposition, showingrid, gridposition, requiredfield, textlimit, keywordsearchable, mastercontrol, datecreated) 
values ('$recordid', '5', 'disabled', 'Disabled', '0', '1', '2', '0', '1000', '0', '1', '$createddate')");
$insertfieldid = mysql_query("insert into structurefield (structurefunctionid, structurefieldtypeid, structurefieldname, displayname, 
fieldposition, showingrid, gridposition, requiredfield, textlimit, keywordsearchable, mastercontrol, datecreated) 
values ('$recordid', '6', '$idname', '$structurefunctionid', '1', '1', '1', '0', '0', '0', '1', '$createddate')");
$insertfieldname = mysql_query("insert into structurefield (structurefunctionid, structurefieldtypeid, structurefieldname, displayname, 
fieldposition, showingrid, gridposition, requiredfield, textlimit, keywordsearchable, mastercontrol, datecreated) 
values ('$recordid', '1', '$name', '$structurefunctionname', '2', '1', '2', '1', '250', '1', '1', '$createddate')");
if($database = $masterdatabase){
	$insertfieldname = mysql_query("insert into structurefield (structurefunctionid, structurefieldtypeid, structurefieldname, displayname, 
	fieldposition, showingrid, gridposition, requiredfield, textlimit, keywordsearchable, mastercontrol, datecreated, masteronly) 
	values ('$recordid', '7', 'masteronly', 'Master only', '1000', '1', '1000', '0', '0', '0', '1', '$createddate', 1)");
}
else {
	$insertfieldname = mysql_query("insert into structurefield (structurefunctionid, structurefieldtypeid, structurefieldname, displayname, 
	fieldposition, showingrid, gridposition, requiredfield, textlimit, keywordsearchable, mastercontrol, datecreated, masteronly) 
	values ('$recordid', '7', 'masteronly', 'Master only', '1000', '1', '1000', '0', '0', '0', '1', '$createddate', 0)");
}


//set mastercontrol settings
$getprimarykey = mysql_query("select * from structurefunction where tablename = '$pagetype'");
$getrow = mysql_fetch_array($getprimarykey);
$primarykeyfield = $getrow['primarykeyfield'];
						
						
if($_SESSION["accesslevel"]=='Master'){
	$updatemasterflag = mysql_query("update $pagetype set mastercontrol = '1' where $primarykeyfield = '$recordid'");
}
if($_SESSION["accesslevel"]=='Tenant'){
	$updatemasterflag = mysql_query("update $pagetype set mastercontrol = '1', tenantcontrol = '1' where $primarykeyfield = '$recordid'");
}
if($_SESSION["accesslevel"]=='Business'){
	$updatemasterflag = mysql_query("update $pagetype set mastercontrol = '1', tenantcontrol = '1' , businesscontrol = '1' where $primarykeyfield = '$recordid'");
}
?>