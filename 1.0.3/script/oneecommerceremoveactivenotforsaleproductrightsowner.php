<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include('../1.0.3/phpAmazonMWS/includes/classes.php');
$timestamp= gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time());
$date = date("Y-m-d");

$minute = date('i');
$hour = date('H');
echo "<br/>hour: ".$hour;
echo "<br/>minute: ".$minute;

//setup batching		
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$setupbatching = mysql_query("select min(ecommercelistingid) as 'minval', max(ecommercelistingid) as 'maxval' 
from ecommercelisting");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get batching stats: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
while($row10 = mysql_fetch_array($setupbatching)){
	$minid = $row10['minval'];
	$maxid = $row10['maxval'];
}	
if($minute <= 10){
	$randno = 1;		
}
if($minute > 10 && $minute <= 20){
	$randno = 2;		
}
if($minute > 20 && $minute <= 30){
	$randno = 3;		
}
if($minute > 30 && $minute <= 40){
	$randno = 4;		
}
if($minute > 40 && $minute <= 50){
	$randno = 5;		
}
if($minute >= 50){
	$randno = 6;		
}
$total = $maxid - $minid;
$range = $total / 6;
if($randno == 1){
	$startid = $minid;
	$stopid = $minid + $range;
}
if($randno == 2){
	$startid = $minid + $range;
	$stopid = $maxid - ($range * 4);
}
if($randno == 3){
	$startid = $minid + ($range * 2);
	$stopid = $maxid - ($range * 3);
}
if($randno == 4){
	$startid = $minid + ($range * 3);
	$stopid = $maxid - ($range * 2);
}
if($randno == 5){
	$startid = $minid + ($range * 4);
	$stopid = $maxid - $range;
}
if($randno == 6){
	$startid = $maxid - $range;
	$stopid = $maxid;
}
$startid = round($startid);
$stopid = round($stopid);
echo "<br/><br/>minid: ".$minid;
echo "<br/>maxid: ".$maxid;
echo "<br/>total: ".$total;
echo "<br/>range: ".$range;
echo "<br/>randno: ".$randno;
echo "<br/>startid: ".$startid;
echo "<br/>stopid: ".$stopid;

$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$getproductstockitem = mysql_query("select productstockitem.productid, productstockitem.productstockitemid
from ecommercelisting 
inner join productstockitem on ecommercelisting.productstockitemid = productstockitem.productstockitemid
inner join product on product.productid = productstockitem.productid
inner join productrightsowner on productrightsowner.productrightsownerid = product.productrightsownerid
where ecommercelistingid >= $startid and ecommercelistingid <= $stopid and 
notforsale = 1 and productstockitemstatusid = 1 
limit 100");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get ProductStockItem records that need to be deleted: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
echo "<br/><br/><br/>";
$productidlist = "";
$productstockitemidlist = "";
while($productstockitemrow = mysql_fetch_array($getproductstockitem)){
	$productid = $productstockitemrow['productid'];
	$productstockitemid = $productstockitemrow['productstockitemid'];
	$productidlist = $productidlist.$productid.",";
	$productstockitemidlist = $productstockitemidlist.$productstockitemid.",";
	//echo "<br/>notforsale: ".$productid." - ".$productstockitemid." - ".$notforsale;
}
echo "<br/><br/>";

//add some product exclusions
if($minute <= 10 && $hour <= 0){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getproductstockitem = mysql_query("select ecommerceproductexclusion.productid, productstockitemid from ecommerceproductexclusion
	inner join productstockitem on productstockitem.productid = ecommerceproductexclusion.productid
	where productstockitemstatusid = 1");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get ProductStockItem records that need to be deleted for product exclusions: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	echo "<br/><br/><br/>";
	while($productstockitemrow = mysql_fetch_array($getproductstockitem)){
		$productid = $productstockitemrow['productid'];
		$productstockitemid = $productstockitemrow['productstockitemid'];
		$productidlist = $productidlist.$productid.",";
		$productstockitemidlist = $productstockitemidlist.$productstockitemid.",";
		//echo "<br/>notforsale: ".$productid." - ".$productstockitemid." - ".$notforsale;
	}
	echo "<br/><br/>";
}


if($productidlist <> ""){
	$productidlist = rtrim($productidlist, ",");
	$productstockitemidlist = rtrim($productstockitemidlist, ",");
	echo "<br/><br/>productidlist: ".$productidlist;
	echo "<br/><br/>productstockitemidlist: ".$productstockitemidlist;
	
	//get list of ecommercelisting records 
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getecommercelisting = mysql_query("select ecommercelistingid, productstockitemid from ecommercelisting 
	where productstockitemid in ($productstockitemidlist)");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get ecommercelisting records that need to be deleted: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	$ecommercelistingidlist = "";
	while($ecommercelistingrow = mysql_fetch_array($getecommercelisting)){
		$ecommercelistingid = $ecommercelistingrow['ecommercelistingid'];
		$productstockitemid = $ecommercelistingrow['productstockitemid'];
		$ecommercelistingidlist = $ecommercelistingidlist.$ecommercelistingid.",";
	
	}
	$ecommercelistingidlist = rtrim($ecommercelistingidlist, ",");
	echo "<br/><br/>ecommercelistingidlist: ".$ecommercelistingidlist;
	
	if($ecommercelistingidlist <> ""){
		//get list of ecommercelistinglocation records that can be deleted
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$getecommercelistinglocation = mysql_query("select ecommercelistinglocationid, ecommercelistingid
		from ecommercelistinglocation 
		where ecommercelistingid in ($ecommercelistingidlist) and integrationproductid = ''
		and ecommercelistinglocationstatusid not in (5,13,18)");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Get ecommercelistinglocation records that need to be deleted: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		$ecommercelistinglocationidlist = "";
		while($ecommercelistinglocationrow = mysql_fetch_array($getecommercelistinglocation)){
			$ecommercelistinglocationid = $ecommercelistinglocationrow['ecommercelistinglocationid'];
			$ecommercelistingid = $ecommercelistinglocationrow['ecommercelistingid'];
			$ecommercelistinglocationidlist = $ecommercelistinglocationidlist.$ecommercelistinglocationid.",";
		
		}
		$ecommercelistinglocationidlist = rtrim($ecommercelistinglocationidlist, ",");
		echo "<br/><br/>ecommercelistinglocationidlist: ".$ecommercelistinglocationidlist;
		
		
		//get list of ecommercelistinglocation records that need some processing via Amazon 
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$getecommercelistinglocation = mysql_query("select ecommercelistinglocationid, ecommercelistingid, integrationproductid, 
		ecommercesiteconfig.ecommercesiteconfigid 
		from ecommercelistinglocation 
		inner join ecommercesiteconfig on ecommercesiteconfig.ecommercesiteconfigid = ecommercelistinglocation.ecommercesiteconfigid
		where ecommercelistingid in ($ecommercelistingidlist) and integrationproductid <> ''
		and ecommercelistinglocationstatusid not in (5,13,18) and ecommercesiteid = 1
		order by ecommercelistinglocation.ecommercesiteconfigid asc");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Get ecommercelistinglocation records that need to be deleted: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		$ecommercelistinglocationidlistamazon = "";
		$ecommercelistinglocationidlistamazonarray = array();
		while($ecommercelistinglocationrow = mysql_fetch_array($getecommercelistinglocation)){
			$ecommercelistinglocationid = $ecommercelistinglocationrow['ecommercelistinglocationid'];
			$ecommercelistingid = $ecommercelistinglocationrow['ecommercelistingid'];
			$integrationproductid = $ecommercelistinglocationrow['integrationproductid'];
			$ecommercesiteconfigid = $ecommercelistinglocationrow['ecommercesiteconfigid'];
			$ecommercelistinglocationidlistamazon = $ecommercelistinglocationidlistamazon.$ecommercelistinglocationid.",";
			array_push($ecommercelistinglocationidlistamazonarray, array("ecommercelistinglocationid"=>$ecommercelistinglocationid,
			"integrationproductid"=>$integrationproductid,"ecommercesiteconfigid"=>$ecommercesiteconfigid));
		}
		$ecommercelistinglocationidlistamazon = rtrim($ecommercelistinglocationidlistamazon, ",");
		echo "<br/><br/>ecommercelistinglocationidlistamazon: ".$ecommercelistinglocationidlistamazon;
		
		
		//get list of ecommercelistinglocation records that need some processing via Shopify
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$getecommercelistinglocation = mysql_query("select ecommercelistinglocationid, ecommercelistingid, 
		ecommercesiteconfig.ecommercesiteconfigid, integrationproductid, apikey1, apikey2, apikey3
		 from ecommercelistinglocation 
		inner join ecommercesiteconfig on ecommercesiteconfig.ecommercesiteconfigid = ecommercelistinglocation.ecommercesiteconfigid
		where ecommercelistingid in ($ecommercelistingidlist) and integrationproductid <> ''
		and ecommercelistinglocationstatusid not in (5,13,18) and ecommercesiteid = 3
		order by ecommercelistinglocation.ecommercesiteconfigid asc");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Get ecommercelistinglocation records that need to be deleted: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		$ecommercelistinglocationidlistshopify = "";
		$ecommercelistinglocationidlistshopifyarray = array();
		while($ecommercelistinglocationrow = mysql_fetch_array($getecommercelistinglocation)){
			$ecommercelistinglocationid = $ecommercelistinglocationrow['ecommercelistinglocationid'];
			$ecommercelistingid = $ecommercelistinglocationrow['ecommercelistingid'];
			$ecommercesiteconfigid = $ecommercelistinglocationrow['ecommercesiteconfigid'];
			$integrationproductid = $ecommercelistinglocationrow['integrationproductid'];
			$shopdomain = $ecommercelistinglocationrow['apikey1'];
			$apikey = $ecommercelistinglocationrow['apikey2'];
			$secret = $ecommercelistinglocationrow['apikey3'];
			$ecommercelistinglocationidlistshopify = $ecommercelistinglocationidlistshopify.$ecommercelistinglocationid.",";
			array_push($ecommercelistinglocationidlistshopifyarray, array("ecommercelistinglocationid"=>$ecommercelistinglocationid,
			"integrationproductid"=>$integrationproductid,"ecommercesiteconfigid"=>$ecommercesiteconfigid, 
			"shopdomain"=>$shopdomain,"apikey"=>$apikey,
			"secret"=>$secret));
		}
		$ecommercelistinglocationidlistshopify = rtrim($ecommercelistinglocationidlistshopify, ",");
		echo "<br/><br/>ecommercelistinglocationidlistshopify: ".$ecommercelistinglocationidlistshopify;
		echo "<br/><br/>ecommercelistinglocationidlistshopifyarray: ".json_encode($ecommercelistinglocationidlistshopifyarray);
		
		
		//get list of ecommercelistinglocation records that cannot be deleted because sold 
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$getecommercelistinglocation = mysql_query("select ecommercelistinglocationid, ecommercelistingid 
		from ecommercelistinglocation 
		inner join ecommercesiteconfig on ecommercesiteconfig.ecommercesiteconfigid = ecommercelistinglocation.ecommercesiteconfigid
		where ecommercelistingid in ($ecommercelistingidlist) and ecommercelistinglocationstatusid in (5,13,18)");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Get ecommercelistinglocation records that need to be deleted: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		$ecommercelistinglocationidlistdonotdelete = "";
		while($ecommercelistinglocationrow = mysql_fetch_array($getecommercelistinglocation)){
			$ecommercelistinglocationid = $ecommercelistinglocationrow['ecommercelistinglocationid'];
			$ecommercelistingid = $ecommercelistinglocationrow['ecommercelistingid'];
			$ecommercelistinglocationidlistdonotdelete = $ecommercelistinglocationidlistdonotdelete.$ecommercelistinglocationid.",";
		
		}
		$ecommercelistinglocationidlistdonotdelete = rtrim($ecommercelistinglocationidlistdonotdelete, ",");
		echo "<br/><br/>ecommercelistinglocationidlistdonotdelete: ".$ecommercelistinglocationidlistdonotdelete;
			
		
		//compile full list of ecommercelistinglocation records
		$fullecommercelistinglocationidlist = "";
		if($ecommercelistinglocationidlist <> ""){
			$fullecommercelistinglocationidlist = $fullecommercelistinglocationidlist.$ecommercelistinglocationidlist.",";
		}
		if($ecommercelistinglocationidlistamazon <> ""){
			$fullecommercelistinglocationidlist = $fullecommercelistinglocationidlist.$ecommercelistinglocationidlistamazon.",";
		}
		if($ecommercelistinglocationidlistshopify <> ""){
			$fullecommercelistinglocationidlist = $fullecommercelistinglocationidlist.$ecommercelistinglocationidlistshopify.",";
		}
		if($fullecommercelistinglocationidlist <> ""){
			$fullecommercelistinglocationidlist = rtrim($fullecommercelistinglocationidlist, ",");
			$fullecommercelistinglocationidlist = str_replace(",,", ",", $fullecommercelistinglocationidlist);
		}
		echo "<br/><br/>fullecommercelistinglocationidlist: ".$fullecommercelistinglocationidlist;
		
		//get list of ecommercelistinglocationstat records
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$getecommercelistinglocationstat = mysql_query("select ecommercelistinglocationstatid, 
		ecommercelistinglocationid from ecommercelistinglocationstat 
		where ecommercelistinglocationid in ($fullecommercelistinglocationidlist)");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Get ecommercelistinglocation records that need to be deleted: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		$ecommercelistinglocationstatlist = "";
		while($getecommercelistinglocationstatrow = mysql_fetch_array($getecommercelistinglocationstat)){
			$ecommercelistinglocationstatid = $getecommercelistinglocationstatrow['ecommercelistinglocationstatid'];
			$ecommercelistinglocationid = $getecommercelistinglocationstatrow['ecommercelistinglocationid'];
			$ecommercelistinglocationstatlist = $ecommercelistinglocationstatlist.$ecommercelistinglocationstatid.",";
		
		}
		$ecommercelistinglocationstatlist = rtrim($ecommercelistinglocationstatlist, ",");
		echo "<br/><br/>ecommercelistinglocationstatlist: ".$ecommercelistinglocationstatlist;
		
		
		//submit deletions of amazon records
		if($ecommercelistinglocationidlistamazon <> ''){
			$p = 1;
			$i = 1;
			$savedecommercesiteconfigid = "";
			foreach($ecommercelistinglocationidlistamazonarray as $row33){
				$ecommercelistinglocationid = $row33['ecommercelistinglocationid'];
				$integrationproductid = $row33['integrationproductid'];
				$ecommercesiteconfigid = $row33['ecommercesiteconfigid'];
				echo "<br/>".$ecommercelistinglocationid." - ".$integrationproductid;
				
				//run update, because the ecommercesiteconfigid has changed
				if($savedecommercesiteconfigid <> $ecommercesiteconfigid && $savedecommercesiteconfigid <> ""){
					$nosqlqueries = $nosqlqueries + 1;
					$sqlstarttime = microtime(true);
					$getconfig = mysql_query("select * from ecommercesiteconfig 
					where ecommercesiteconfigid = '$savedecommercesiteconfigid'");
					$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
					$differencemilliseconds = microtime(true) - $sqlstarttime;
					echo "<br/>Get amazon config details: ";
					echo "<br/>differencemilliseconds: ".$differencemilliseconds;
					echo "<br/>sqlqueriestime: ".$sqlqueriestime;
					while($configrow = mysql_fetch_array($getconfig)){
						$ecommercesiteconfigid = $configrow['ecommercesiteconfigid'];
						$ecommercesiteconfigname = $configrow['ecommercesiteconfigname'];
						$apikey1 = $configrow['apikey1'];
						$apikey2 = $configrow['apikey2'];
						$apikey3 = $configrow['apikey3'];
						$apikey4 = $configrow['apikey4'];
						echo "<br/><br/><b>".$ecommercesiteconfigname."</b><br/><br/>";
					}
					
					$feedinventory = $feedinventory.'</AmazonEnvelope>';
					//echo "<br/><br/>".nl2br(htmlentities($feedinventory));
					
					echo "<br/><br/>Submit the amazon cancel feed: ";
					sendDeleteFeed($feedinventory);
					echo "<br/>final feed sub id: ".$feedsubid;
					$p = 0;				
				}
				
				//first iteration of ecommercesiteconfig, so add the feedinventory header
				if($p == 1){
					$fileid = substr(str_shuffle("0123456789"), 0, 5);
					$feedinventory = '<?xml version="1.0" encoding="iso-8859-1"?>
					<AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
					    xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
					  <Header>
					    <DocumentVersion>1.01</DocumentVersion>
					    <MerchantIdentifier>'.$fileid.'</MerchantIdentifier>
					  </Header>
					  <MessageType>Product</MessageType>';
					$i = 1;
				}
				
				$feedinventory = $feedinventory.'
				<Message>
					<MessageID>'.$i.'</MessageID>
					<OperationType>Delete</OperationType>
					<Product>
						<SKU>'.$integrationproductid.'</SKU>
					</Product>
				</Message>';
				$i = $i + 1;		
				
				$p = $p + 1;
				$savedecommercesiteconfigid = $ecommercesiteconfigid;
			}
			//run update, because the ecommercesiteconfigid has changed
			if($i >= 1){
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$getconfig = mysql_query("select * from ecommercesiteconfig 
				where ecommercesiteconfigid = '$savedecommercesiteconfigid'");
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Get amazon config details: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
				while($configrow = mysql_fetch_array($getconfig)){
					$ecommercesiteconfigid = $configrow['ecommercesiteconfigid'];
					$ecommercesiteconfigname = $configrow['ecommercesiteconfigname'];
					$apikey1 = $configrow['apikey1'];
					$apikey2 = $configrow['apikey2'];
					$apikey3 = $configrow['apikey3'];
					$apikey4 = $configrow['apikey4'];
					echo "<br/><br/><b>".$ecommercesiteconfigname."</b><br/><br/>";
				}
				$feedinventory = $feedinventory.'</AmazonEnvelope>';
				//echo "<br/><br/>".nl2br(htmlentities($feedinventory));
				
				echo "<br/><br/>Submit the amazon cancel feed: ";
				sendDeleteFeed($feedinventory);
				echo "<br/>final feed sub id: ".$feedsubid;
				$p = 0;				
			}
		}
	
		
		//submit deletions of shopify records
		if($ecommercelistinglocationidlistshopify <> ''){
			foreach ($ecommercelistinglocationidlistshopifyarray as $item){
				$ecommercelistinglocationid = $item['ecommercelistinglocationid'];
				$integrationproductid = $item['integrationproductid'];
				$apikey = $item['apikey'];
				$secret = $item['secret'];
				$shopdomain = $item['shopdomain'];
				if($integrationproductid <> ''){
					$pieces = explode("//", $integrationproductid);
					$integrationproductid = $pieces[0];
				}
				
				//remove the product
				$url = "https://".$apikey.":".$secret."@".$shopdomain."/admin/products/".$integrationproductid.".json";
				echo "<br/>".$url;
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
				curl_setopt($ch, CURLOPT_POST, true);
				//comment out returntransfer line below to see the json response for debugging purposes
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				
				$result = curl_exec($ch);
				curl_close($ch);
				echo "<br/>".$result;
			}
		}
		
		if($ecommercelistinglocationstatlist <> ""){
			//delete all ecommercelistinglocationstat records
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$deleterecord = mysql_query("delete from ecommercelistinglocationstat 
			where ecommercelistinglocationstatid in ($ecommercelistinglocationstatlist)");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Delete ecommercelistinglocationstat: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		}	
		
		if($fullecommercelistinglocationidlist <> ""){	
			//delete all ecommercelistinglocation records
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$deleterecord = mysql_query("delete from ecommercelistinglocation 
			where ecommercelistinglocationid in ($fullecommercelistinglocationidlist)");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Delete ecommercelistinglocation: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		}	
		
		if($ecommercelistingidlist <> ""){	
			//delete all ecommercelisting records
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$deleterecord = mysql_query("delete from ecommercelisting 
			where ecommercelistingid in ($ecommercelistingidlist)");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Delete ecommercelisting: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		}
	}
	
}

function sendDeleteFeed($feed) {
    try {
        $amz=new AmazonFeed(); //if there is only one store in config, it can be omitted
        $amz->setFeedType("_POST_PRODUCT_DATA_"); //feed types listed in documentation
        $amz->setFeedContent($feed); //can be either XML or CSV data; a file upload method is available as well
        $amz->submitFeed(); //this is what actually sends the request
        return $amz->getResponse();
    } catch (Exception $ex) {
        echo '<br/>There was a problem with the Amazon library. Error: '.$ex->getMessage();
    }
}

echo "<br/><br/>";
?>









