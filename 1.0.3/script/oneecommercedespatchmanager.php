<?php

$businessunitid = isset($_POST['businessunitid']) ? $_POST['businessunitid'] : '';
$submit = isset($_POST['submit']) ? $_POST['submit'] : '';

if($businessunitid >= 1){

	$getdespatchconfig = "SELECT * FROM despatchmanagerconfig LIMIT 1";
	$getdespatchconfig = mysql_query($getdespatchconfig);
	while($fetchdata = mysql_fetch_array($getdespatchconfig))
	{
		$includegeneratepickinglist = $fetchdata['includegeneratepickinglist'];
		$includegenerateinvoicefiles = $fetchdata['includegenerateinvoicefiles'];
		$includegeneratedespatchlabels = $fetchdata['includegeneratedespatchlabels'];
		$includegenerateindividualorderlist = $fetchdata['includegenerateindividualorderlist'];
		$individualorderlistscriptlocation = $fetchdata['individualorderlistscriptlocation'];
		$splitbystoragelocation = $fetchdata['splitbystoragelocation'];
		$splitbystoragelocationsection = $fetchdata['splitbystoragelocationsection'];
		$splitbyhourly = $fetchdata['splitbyhourly'];
		$splitinbatchesof = $fetchdata['splitinbatchesof'];
	}

	$updated = isset($_GET['updated']) ? $_GET['updated'] : '';
	if(isset($updated) && $updated == 100) {
		echo "<br/><p class='background-success'>The records have been marked as despatched.</p>";
	} 		
	echo "<p>This section allows you to manage your despatch processes. It provides you with an overview of your daily despatches, how many are left to be processed, an export of invoices, and an export of despatch labels. Once processed, you can then update the records to despatched in bulk.</p>";
	echo "<p>You can access excel reports within Reports -> OneECommerce - Fulfillment/Despatch Management. 'Warehouse Picking List' allows you to collect your items from your warehouse storage locations, and 'Despatch List' allows your packers to package and label your parcels for despatch.</p>";
	echo "<p>Once all despatches are completed, the record will disappear from this page.</p>";
	echo "<p>Before you use the Invoice/Labels for the first time, ensure you configure the settings for the invoices. Click here: <a href='pagegrid.php?pagetype=ecommerceinvoicegenerationconfig'>Invoice Generation Config</a></p>";
	echo "<br/>";
	echo "<h2>Outstanding Despatch Log</h2>";
	echo "<table id='' class='table table-bordered'>";
	echo '<thead><tr>';
	echo "<th style='width:90px;'>Batch</th>";
	echo '<th>Number Pending Despatch</th>';
	echo '<th>Number Despatched</th>';
	
	if(isset($includegeneratepickinglist) && $includegeneratepickinglist == 1)
	{
		echo '<th>Generate Picking List</th>';
	}
	if(isset($includegenerateinvoicefiles) && $includegenerateinvoicefiles == 1)
	{
		echo '<th>Generate Invoice File For Remaining Despatches</th>';
	}
	if(isset($includegeneratedespatchlabels) && $includegeneratedespatchlabels == 1)
	{
		echo '<th>Generate Despatch Labels File For Remaining Despatches</th>';
	}
	if(isset($includegenerateindividualorderlist) && $includegenerateindividualorderlist == 1)
	{
		echo '<th>Generate Order List</th>';
	}
	
	echo '<th>Mark All As Despatched</th>';
	echo "</tr></thead>";
	echo "<tbody>";

	$splitbyfieldsarray = array();

	$getdespatch = "select CONCAT(HOUR(orderdatetime), ':00 - ', HOUR(orderdatetime)+1, ':00') AS Hours, HOUR(orderdatetime) as hourintime, storagelocationsectionname, storagelocationname, count(ecommerceorderitemid) as 'despatchcount', ecommercelisting.selldate
	from $database.ecommerceorder inner join $database.ecommerceorderitem on ecommerceorderitem.ecommerceorderid = ecommerceorder.ecommerceorderid 
	inner join $database.ecommercelisting on ecommercelisting.ecommercelistingid = ecommerceorderitem.ecommercelistingid 
	inner join $database.productstockitem on productstockitem.productstockitemid = ecommercelisting.productstockitemid
	inner join $database.storagelocationsection on storagelocationsection.storagelocationsectionid = productstockitem.storagelocationsectionid
	inner join $database.storagelocation on storagelocation.storagelocationid = storagelocationsection.storagelocationid where ecommercelistingstatusid in (2,3) and ecommercelisting.businessunitid = $businessunitid";
	if($_SESSION["userbusinessunit"] <> '' && $_SESSION["superadmin"] == 0){
		$getdespatch = $getdespatch." and ecommercelisting.businessunitid in (".$_SESSION["userbusinessunit"].")";
	}
	
	if(isset($splitbystoragelocation) && $splitbystoragelocation == 1) {
		array_push($splitbyfieldsarray, 'storagelocationname');
	}
	if(isset($splitbystoragelocationsection) && $splitbystoragelocationsection == 1) {
		array_push($splitbyfieldsarray, 'storagelocationsectionname');
	}
	if(isset($splitbyhourly) && $splitbyhourly == 1) {
		array_push($splitbyfieldsarray, 'HOUR(orderdatetime)');
	}
	if(isset($splitinbatchesof) && $splitinbatchesof <> 0) {
		// todo
	}

	if(count($splitbyfieldsarray) > 0)
	{
		$splitbyfieldsarray = implode(",", $splitbyfieldsarray);
		$getdespatch = $getdespatch." group by ".$splitbyfieldsarray." order by ".$splitbyfieldsarray." desc limit 10";
	}
	else
	{
		$getdespatch = $getdespatch." limit 10";
	}
		
	//echo $getdespatch;
	$getdespatch = mysql_query($getdespatch);
	while ($row22=mysql_fetch_array($getdespatch)){
		$selldate = $row22['selldate'];

		$storagelocation = $row22['storagelocationname'];
		$storagelocationsection = $row22['storagelocationsectionname'];
		$orderdatetime = $row22['hourintime'];

		$batch = array();
		$stringforurl = array();
		echo "<tr>";

		$getdespatched = "select ecommercelisting.selldate from $database.ecommerceorder
		inner join $database.ecommerceorderitem on ecommerceorderitem.ecommerceorderid = ecommerceorder.ecommerceorderid
		inner join $database.ecommercelisting on ecommercelisting.ecommercelistingid = ecommerceorderitem.ecommercelistingid 
		inner join $database.productstockitem on productstockitem.productstockitemid = ecommercelisting.productstockitemid
		inner join $database.storagelocationsection on storagelocationsection.storagelocationsectionid = productstockitem.storagelocationsectionid
		inner join $database.storagelocation on storagelocation.storagelocationid = storagelocationsection.storagelocationid"; 
		$getdespatched = $getdespatched." where ecommercelistingstatusid = 2 and ecommercelisting.businessunitid = $businessunitid";
		if($_SESSION["userbusinessunit"] <> '' && $_SESSION["superadmin"] == 0){
			$getdespatched = $getdespatched." and ecommercelisting.businessunitid in (".$_SESSION["userbusinessunit"].")";
		}
		if(isset($splitbystoragelocation) && $splitbystoragelocation == 1) {
			$getdespatched = $getdespatched." and storagelocationname = '$storagelocation'  ";
			array_push($batch, $storagelocation);
			array_push($stringforurl, "storagelocationname='$storagelocation'");
		}
		if(isset($splitbystoragelocationsection) && $splitbystoragelocationsection == 1) {
			$getdespatched = $getdespatched." and storagelocationsectionname = '$storagelocationsection'  ";
			array_push($batch, $storagelocationsection);
			array_push($stringforurl, "storagelocationsectionname='$storagelocationsection'");
		}
		if(isset($splitbyhourly) && $splitbyhourly == 1) {
			$getdespatched = $getdespatched." and HOUR(orderdatetime) = '$orderdatetime'  ";
			array_push($batch, $row22['Hours']);
			array_push($stringforurl, "orderdatetimehour='$orderdatetime'");
		}
		$getdespatched = mysql_query($getdespatched);

		$stringforurl = implode("&", $stringforurl);
		$stringforurl = str_replace("'","",$stringforurl);
		//echo $stringforurl;
		echo "<td>".implode(",", $batch)."</td>";
		echo "<td>".$row22['despatchcount']."</td>";
		echo "<td>".mysql_num_rows($getdespatched)."</td>";

		if(isset($includegeneratepickinglist) && $includegeneratepickinglist == 1)
		{
			echo "<td><a href='script/oneecommercedespatchmanagergeneratepickinglist.php?$stringforurl&selldate=".$selldate."&businessunitid=".$businessunitid."' target='_blank'>Generate Picking List</a><p>The file will download once it has generated. Please note it can take a few minutes.</p></td>";
		}
		if(isset($includegenerateinvoicefiles) && $includegenerateinvoicefiles == 1)
		{
			echo "<td><a href='script/oneecommercedespatchmanagergenerateinvoice.php?$stringforurl&selldate=".$selldate."&businessunitid=".$businessunitid."' target='_blank'>Generate Invoice File</a><p>The file will download once it has generated. Please note it can take a few minutes.</p></td>";
		}
		if(isset($includegeneratedespatchlabels) && $includegeneratedespatchlabels == 1)
		{
			echo "<td><a href='script/oneecommercedespatchmanagergeneratelabels.php?$stringforurl&selldate=".$selldate."&businessunitid=".$businessunitid."' target='_blank'>Generate Despatch Labels</a><p>The file will download once it has generated. Please note it can take a few minutes.</p></td>";
		}
		if(isset($includegenerateindividualorderlist) && $includegenerateindividualorderlist == 1)
		{
			echo "<td><a href='script/individualorderlistscriptlocation.php' target='_blank'>Generate Order List</a></td>";
		}
		
		echo "<td><a href='script/oneecommercedespatchmanagermarkalldespatched.php?$stringforurl&selldate=".$selldate."&businessunitid=".$businessunitid."'>Mark All As Despatched</a><p>Please note, this will instantly mark all records as despatched. Do not click this unless you are sure ALL items have been despatched.</p></td>";		
		
		echo "</tr>";
	}
	echo "</tbody>";			        	
	echo "</table>";
}

if($businessunitid == 0){
	//show the first page - selecting supplier and businessunit	
	?>
	<h2>Despatch Manager</h2>
	<p>Select your Business Unit.</p>
	<form class="form-horizontal" action='view.php?viewid=11' method="post" enctype="multipart/form-data">
		<div class="form-group">
			<label class="col-xs-12 col-sm-3 col-md-3 col-lg-2 control-label">Business Unit</label>			
			<div class="col-xs-12 col-md-5 col-sm-5 col-lg-5">
				<select class='form-control' name='businessunitid' id='action'>
				<?php
				$querybu = "select businessunitid, businessunitname from businessunit where disabled = 0";
				if($bulist <> ""){
					$querybu = $querybu." and businessunitid in ($bulist)"; 
				}  	
				echo $querybu;	  	
				$resultbu = mysql_query($querybu);
				echo "<option value=''>Select Item</option>";
				while($ntbu=mysql_fetch_array($resultbu)){
					if($businessunitid == $ntbu['businessunitid']){
						echo "<option value='".$ntbu['businessunitid']."' selected='true'>".$ntbu['businessunitname']."</option>";	
					}
					else{
						echo "<option value='".$ntbu['businessunitid']."'>".$ntbu['businessunitname']."</option>";	
					}
				}
				?>
				</select>
			</div>
		</div>
		<div class="form-group">
		 	<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
		 		<label class="col-xs-12 col-sm-3 col-md-3 col-lg-2 control-label"></label>
		 		<button type='submit' name='submit' value='Submit' class="button-primary">Submit</button>																									
		 	</div>
		</div>
	</form>
<?php
}
?>