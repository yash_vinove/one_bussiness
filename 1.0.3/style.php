<?php session_start(); ?>
<?php include('config.php'); ?>
<?php include('sessionconfig.php'); ?>
<?php
    header("Content-type: text/css; charset: UTF-8");
?>


/* FONTS */	
.body {
  	margin: 0;
  	background-color:<?php if (empty($_SESSION['backgroundbackgroundcolour'])){echo "#D3D3D3";} else {echo $_SESSION['backgroundbackgroundcolour'];} ?>; /* backgroundbackgroundcolour */
  	font-family: <?php if (empty($_SESSION['fontfontfamily'])){echo "Arial, Helvetica, sans-serif";} else {echo $_SESSION['fontfontfamily'];} ?>; /* fontfontfamily */
  
  	/* Supported font examples
  	"Times New Roman", Times, serif	Georgia, serif	"Palatino Linotype", "Book Antiqua", Palatino, serif	Arial, Helvetica, sans-serif	"Arial Black", Gadget, sans-serif	"Comic Sans MS", cursive, sans-serif	Impact, Charcoal, sans-serif	"Lucida Sans Unicode", "Lucida Grande", sans-serif	Tahoma, Geneva, sans-serif	"Trebuchet MS", Helvetica, sans-serif	Verdana, Geneva, sans-serif	"Courier New", Courier, monospace	"Lucida Console", Monaco, monospace
  	*/
}


/* BACKGROUND */	
.body .bodyheader {
	padding:10px;
	background-color:<?php if (empty($_SESSION['backgroundheadingbackgroundcolour'])){echo "#e9e9e9";} else {echo $_SESSION['backgroundheadingbackgroundcolour'];} ?>; /* backgroundheadingbackgroundcolour */
	border-bottom:1px solid #A0A0A0;
	color:<?php if (empty($_SESSION['backgroundheadingtextcolour'])){echo "#000000";} else {echo $_SESSION['backgroundheadingtextcolour'];} ?>; /* backgroundheadingtextcolour */
}

.body .bodycontent {
	padding:10px;
	background-color:<?php if (empty($_SESSION['backgroundcontainerbackgroundcolour'])){echo "#ffffff";} else {echo $_SESSION['backgroundcontainerbackgroundcolour'];} ?>; /* backgroundcontainerbackgroundcolour */
	border-bottom:1px solid #A0A0A0;
	color:<?php if (empty($_SESSION['backgroundcontainertextcolour'])){echo "#000000";} else {echo $_SESSION['backgroundcontainertextcolour'];} ?>; /* backgroundcontainertextcolour */
}


/* TEXT */	
.body p { 
    font-size: <?php if (empty($_SESSION['textparagraphsize'])){echo "14";} else {echo $_SESSION['textparagraphsize'];} ?>px; /* textparagraphsize */
}

.body h1 { 
    font-size: <?php if (empty($_SESSION['texth1size'])){echo "24";} else {echo $_SESSION['texth1size'];} ?>px; /* texth1size */
    font-weight: bold;
    padding: 0px;
    margin-top: 5px;
    margin-bottom: 10px;
}

.body h2 { 
    font-size: <?php if (empty($_SESSION['texth2size'])){echo "18";} else {echo $_SESSION['texth2size'];} ?>px; /* texth2size */
    font-weight: bold;
    padding: 0px;
    margin-top: 5px;
    margin-bottom: 10px;
}

.body h3 { 
    font-size: <?php if (empty($_SESSION['texth3size'])){echo "14";} else {echo $_SESSION['texth3size'];} ?>px; /* texth3size */
    font-weight: bold;
    padding: 0px;
    margin: 0px;
}

.body h4 { 
    font-size: <?php if (empty($_SESSION['texth4size'])){echo "12";} else {echo $_SESSION['texth4size'];} ?>px; /* texth4size */
    padding: 0px;
    margin: 0px;
}

.body h5 { 
    font-size: <?php if (empty($_SESSION['texth5size'])){echo "10";} else {echo $_SESSION['texth5size'];} ?>px; /* texth5size */
    padding: 0px;
    margin: 0px;
}


/* COLOURS */	
.background-success {
  padding: 10px 10px 10px 10px;
  background-color: <?php if (empty($_SESSION['coloursuccesspositive'])){echo "#21f900";} else {echo $_SESSION['coloursuccesspositive'];} ?>; /* coloursuccesspositive */
}

.background-warning {
  padding: 10px 10px 10px 10px;
  background-color: <?php if (empty($_SESSION['colourwarningnegative'])){echo "#f90d00";} else {echo $_SESSION['colourwarningnegative'];} ?>; /* colourwarningnegative */
}

.background-colour-primary {
	padding: 10px 10px 10px 10px;
  background-color: <?php if (empty($_SESSION['colourprimary'])){echo "#ffffff";} else {echo $_SESSION['colourprimary'];} ?>; /* colourprimary */
}

.background-colour-secondary {
	padding: 10px 10px 10px 10px;
  background-color: <?php if (empty($_SESSION['coloursecondary'])){echo "#ffffff";} else {echo $_SESSION['coloursecondary'];} ?>; /* coloursecondary */
}

.background-colour-tertiary {
	padding: 10px 10px 10px 10px;
  background-color: <?php if (empty($_SESSION['colourtertiary'])){echo "#ffffff";} else {echo $_SESSION['colourtertiary'];} ?>; /* colourtertiary */
}

.background-colour-backup1 {
	padding: 10px 10px 10px 10px;
  background-color: <?php if (empty($_SESSION['colourbackup1'])){echo "#ffffff";} else {echo $_SESSION['colourbackup1'];} ?>; /* colourbackup1 */
}

.background-colour-backup2 {
	padding: 10px 10px 10px 10px;
  background-color: <?php if (empty($_SESSION['colourbackup2'])){echo "#ffffff";} else {echo $_SESSION['colourbackup2'];} ?>; /* colourbackup2 */
}

.background-colour-backup3 {
	padding: 10px 10px 10px 10px;
  background-color: <?php if (empty($_SESSION['colourbackup3'])){echo "#ffffff";} else {echo $_SESSION['colourbackup3'];} ?>; /* colourbackup3 */
}

.background-colour-backup4 {
	padding: 10px 10px 10px 10px;
  background-color: <?php if (empty($_SESSION['colourbackup4'])){echo "#ffffff";} else {echo $_SESSION['colourbackup4'];} ?>; /* colourbackup4 */
}

.background-colour-backup5 {
	padding: 10px 10px 10px 10px;
  background-color: <?php if (empty($_SESSION['colourbackup5'])){echo "#ffffff";} else {echo $_SESSION['colourbackup5'];} ?>; /* colourbackup5 */
}

.background-colour-backup6 {
	padding: 10px 10px 10px 10px;
  background-color: <?php if (empty($_SESSION['colourbackup6'])){echo "#ffffff";} else {echo $_SESSION['colourbackup6'];} ?>; /* colourbackup6 */
}

.background-colour-backup7 {
	padding: 10px 10px 10px 10px;
  background-color: <?php if (empty($_SESSION['colourbackup7'])){echo "#ffffff";} else {echo $_SESSION['colourbackup7'];} ?>; /* colourbackup7 */
}

.background-colour-backup8 {
	padding: 10px 10px 10px 10px;
  background-color: <?php if (empty($_SESSION['colourbackup8'])){echo "#ffffff";} else {echo $_SESSION['colourbackup8'];} ?>; /* colourbackup8 */
}

.background-colour-backup9 {
	padding: 10px 10px 10px 10px;
  background-color: <?php if (empty($_SESSION['colourbackup9'])){echo "#ffffff";} else {echo $_SESSION['colourbackup9'];} ?>; /* colourbackup9 */
}

.background-colour-backup10 {
	padding: 10px 10px 10px 10px;
  	background-color: <?php if (empty($_SESSION['colourbackup10'])){echo "#ffffff";} else {echo $_SESSION['colourbackup10'];} ?>; /* colourbackup10 */
}


/* BUTTONS */	
.button-primary {
  color: <?php if (empty($_SESSION['buttonprimarytextcolour'])){echo "#ffffff";} else {echo $_SESSION['buttonprimarytextcolour'];} ?>; /* buttonprimarytextcolour */
  background-color: <?php if (empty($_SESSION['buttonprimarybackgroundcolour'])){echo "#3687f3";} else {echo $_SESSION['buttonprimarybackgroundcolour'];} ?>; /* buttonprimarybackgroundcolour */
  font-size: <?php if (empty($_SESSION['buttonprimarytextsize'])){echo "16";} else {echo $_SESSION['buttonprimarytextsize'];} ?>px; /* buttonprimarytextsize */
  border-color: <?php if (empty($_SESSION['buttonprimarybordercolour'])){echo "#2e6da4";} else {echo $_SESSION['buttonprimarybordercolour'];} ?>; /* buttonprimarybordercolour*/
  display: inline-block;
  padding: 6px 12px;
  margin-bottom: 0;
  font-size: 14px;
  font-weight: normal;
  line-height: 1.42857143;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  -ms-touch-action: manipulation;
      touch-action: manipulation;
  cursor: pointer;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
  background-image: none;
  border: 1px solid transparent;
  border-radius: 4px;
}
.button-primary:hover,
.button-primary:focus,
.button-primary.focus,
.button-primary:active,
.button-primary.active,
.open > .dropdown-toggle.button-primary {
  color: <?php if (empty($_SESSION['buttonprimarytextcolour'])){echo "#ffffff";} else {echo $_SESSION['buttonprimarytextcolour'];} ?>; /* buttonprimarytextcolour */
  background-color: <?php if (empty($_SESSION['buttonprimaryhovercolour'])){echo "#286090";} else {echo $_SESSION['buttonprimaryhovercolour'];} ?>; /* buttonprimaryhovercolour*/
  border-color: <?php if (empty($_SESSION['buttonprimarybordercolour'])){echo "#2e6da4";} else {echo $_SESSION['buttonprimarybordercolour'];} ?>; /* buttonprimarybordercolour*/
}

.button-secondary {
  color: <?php if (empty($_SESSION['buttonsecondarytextcolour'])){echo "#ffffff";} else {echo $_SESSION['buttonsecondarytextcolour'];} ?>; /* buttonsecondarytextcolour */
  background-color: <?php if (empty($_SESSION['buttonsecondarybackgroundcolour'])){echo "#3687f3";} else {echo $_SESSION['buttonsecondarybackgroundcolour'];} ?>; /* buttonsecondarybackgroundcolour */
  font-size: <?php if (empty($_SESSION['buttonsecondarytextsize'])){echo "16";} else {echo $_SESSION['buttonsecondarytextsize'];} ?>px; /* buttonsecondarytextsize */
  border-color: <?php if (empty($_SESSION['buttonsecondarybordercolour'])){echo "#2e6da4";} else {echo $_SESSION['buttonsecondarybordercolour'];} ?>; /* buttonsecondarybordercolour*/
 display: inline-block;
  padding: 6px 12px;
  margin-bottom: 0;
  font-size: 14px;
  font-weight: normal;
  line-height: 1.42857143;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  -ms-touch-action: manipulation;
      touch-action: manipulation;
  cursor: pointer;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
  background-image: none;
  border: 1px solid transparent;
  border-radius: 4px;
}
.button-secondary:hover,
.button-secondary:focus,
.button-secondary.focus,
.button-secondary:active,
.button-secondary.active,
.open > .dropdown-toggle.button-secondary {
  color: <?php if (empty($_SESSION['buttonsecondarytextcolour'])){echo "#ffffff";} else {echo $_SESSION['buttonsecondarytextcolour'];} ?>; /* buttonsecondarytextcolour */
  background-color: <?php if (empty($_SESSION['buttonsecondaryhovercolour'])){echo "#286090";} else {echo $_SESSION['buttonsecondaryhovercolour'];} ?>; /* buttonsecondaryhovercolour*/
  border-color: <?php if (empty($_SESSION['buttonsecondarybordercolour'])){echo "#2e6da4";} else {echo $_SESSION['buttonsecondarybordercolour'];} ?>; /* buttonsecondarybordercolour*/
}


/* BOTTOM NAVIGATION */	
#bottom {
	width: 100%;
	position: fixed;
	bottom: 40px;
	height:40px;
	background-color: <?php if (empty($_SESSION['bottomnavigationtopbackgroundcolour'])){echo "#3687f3";} else {echo $_SESSION['bottomnavigationtopbackgroundcolour'];} ?>; /* bottomnavigationtopbackgroundcolour*/
	border-bottom: 3px solid <?php if (empty($_SESSION['bottomnavigationbottombackgroundcolour'])){echo "#2e6da4";} else {echo $_SESSION['bottomnavigationbottombackgroundcolour'];} ?>; /* bottomnavigationbottombackgroundcolour*/
	-moz-border-radius-topleft: 7px;
	-moz-border-radius-topright: 7px;
	margin-bottom:-40px;
}



/* TOP NAVIGATION */	
.topnavigationbusinessname {
	font-size: <?php if (empty($_SESSION['topnavigationnavigationbusinessnamesize'])){echo "20";} else {echo $_SESSION['topnavigationnavigationbusinessnamesize'];} ?>px; /* topnavigationnavigationbusinessnamesize */	
	color: <?php if (empty($_SESSION['topnavigationnavigationbusinessnamecolour'])){echo "#3687f3";} else {echo $_SESSION['topnavigationnavigationbusinessnamecolour'];} ?>; /* topnavigationnavigationbusinessnamecolour */
   	font-weight: bold;
}

.sidenava {
    padding: 5px 5px 0px 10px;
    text-decoration: none;
    font-size: <?php if (empty($_SESSION['topnavigationnavigationheadingsize'])){echo "13";} else {echo $_SESSION['topnavigationnavigationheadingsize'];} ?>px; /* topnavigationnavigationheadingsize */
	 color: #000000;
    display: block;
}

.navigationlist:hover { 
    background-color: <?php if (empty($_SESSION['topnavigationnavigationbackgroundcolour'])){echo "#e9e9e9";} else {echo $_SESSION['topnavigationnavigationbackgroundcolour'];} ?>; /* topnavigationnavigationbackgroundcolour */
}

.open > .dropdown-menu {
 	display: block;
  background-color: <?php if (empty($_SESSION['topnavigationnavigationbackgroundcolour'])){echo "#e9e9e9";} else {echo $_SESSION['topnavigationnavigationbackgroundcolour'];} ?>; /* topnavigationnavigationbackgroundcolour */
}


.navigationlista {
	padding: 0px;
	margin: 0px;	
	text-decoration: none;
	 display: block;
    font-size: <?php if (empty($_SESSION['topnavigationnavigationheadingsize'])){echo "13";} else {echo $_SESSION['topnavigationnavigationheadingsize'];} ?>px; /* topnavigationnavigationheadingsize */
	color: <?php if (empty($_SESSION['topnavigationnavigationheadingcolour'])){echo "#000000";} else {echo $_SESSION['topnavigationnavigationheadingcolour'];} ?>; /* topnavigationnavigationheadingcolour */
	margin-color: <?php if (empty($_SESSION['topnavigationnavigationbackgroundcolour'])){echo "#e9e9e9";} else {echo $_SESSION['topnavigationnavigationbackgroundcolour'];} ?>; /* topnavigationnavigationbackgroundcolour */	
}

.container2 {
  padding-right: 0px;
  padding-left: 0px;
  margin-right: 0px;
  margin-left: 0px;
  background-color: <?php if (empty($_SESSION['topnavigationnavigationbackgroundcolour'])){echo "#e9e9e9";} else {echo $_SESSION['topnavigationnavigationbackgroundcolour'];} ?>; /* topnavigationnavigationbackgroundcolour */
}

.nav > li > a {
  padding: 15px 7px;
}