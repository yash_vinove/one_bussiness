<?php session_start(); ?>
<?php include('../config.php'); ?>
<?php include('../sessionconfig.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	
 	<title>Create Staging Environment</title>

 	<!-- Bootstrap -->
 	<link href="bootstrap-3.3.1/dist/css/bootstrap.css" rel="stylesheet">

 	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
 	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">   
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
	<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script type="text/javascript">
    function redirectMe (sel) {
        var url = sel[sel.selectedIndex].value;
        window.location = url;
    }
    </script>
    <link rel='stylesheet' type='text/css' href='../style.php' />
</head>
  
<body class='body'>
	<?php include_once ('../headerthree.php');	?>
	<?php
	//LANGUAGE COLLECTION SECTION
	
	$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
	and languagerecordid = '36'");
	$langrow = mysql_fetch_array($lang);
	$langval36 = $langrow['languagerecordtextname'];
	$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
	and languagerecordid = '37'");
	$langrow = mysql_fetch_array($lang);
	$langval37 = $langrow['languagerecordtextname'];
	$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
	and languagerecordid = '38'");
	$langrow = mysql_fetch_array($lang);
	$langval38 = $langrow['languagerecordtextname'];
	
	?>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:10px;">
		<!--search results header bar for desktop-->
		<div class='bodyheader'>
			<h3><?php echo $langval36 ?></h3>
		</div>
			<?php 
			$rowid = isset($_GET['rowid']) ? strip_tags($_GET['rowid']) : '';
			$submit = isset($_POST['submit']) ? $_POST['submit'] : ''; 
			if($submit) {
				//get business details
				$getbusiness = mysql_query("select * from business where businessid = '$rowid'");
				$getbusinessrow = mysql_fetch_array($getbusiness);
				$databasename = $getbusinessrow['databasename'];
				$businessname = $getbusinessrow['businessname'];
				$firstname = $getbusinessrow['firstname'];
				$lastname = $getbusinessrow['lastname'];
				$emailaddress = $getbusinessrow['emailaddress'];
				$phonenumber = $getbusinessrow['phonenumber'];
				$rootpassword = $getbusinessrow['rootpassword'];
				$versionid = $getbusinessrow['versionid'];
				$date = date("Y-m-d");		
				
				//update business details ready for insert
				$businessname = $businessname." - Staging";	
				$getmaxbusinessid = mysql_query("select max(businessid) as maxval from business");
				$getmaxbusinessidrow = mysql_fetch_array($getmaxbusinessid)	;
				$businessmax = $getmaxbusinessidrow['maxval'];
				$businessmaxid = $businessmax +1;
				$tenantid = trim($database, 'onebusinesstenant');
				$databasename = "onebusinesstenant".$tenantid."business".$businessmaxid;
				
				//create business
				$insertbusiness = mysql_query("insert into business (businessname, databasename, firstname, 
				lastname, emailaddress, phonenumber, rootpassword, versionid, disabled, datecreated) 
				values ('$businessname', '$databasename', '$firstname', '$lastname', '$emailaddress', '$phonenumber', 
				'$rootpassword', '$versionid', '0', '$date')");
				$businessid = mysql_insert_id();			
				
				
				$databasenew = "onebusinesstenant".$tenantid."business".$businessid;
				$databaseexisting = "onebusinesstenant".$tenantid."business".$rowid;
				$query = mysql_query("create database ".$databasenew);
				
				//update database name in business
				$updatebusiness = mysql_query("update $database.business set databasename = '$databasenew' where businessid = '$businessid'");

				
				$tables = mysql_query("SHOW TABLES FROM $databaseexisting");
				
				while ($line = mysql_fetch_row($tables)) {
				    $tab = $line[0];
				    mysql_query("DROP TABLE IF EXISTS $databasenew.$tab");
				    mysql_query("CREATE TABLE $databasenew.$tab LIKE $databaseexisting.$tab") or die(mysql_error());
				    mysql_query("INSERT INTO $databasenew.$tab SELECT * FROM $databaseexisting.$tab");
				    //echo "Table: <b>" . $line[0] . " </b>Done<br>";
				}
				
				//disable all users except admins
				$getusers = mysql_query("select * from $databasenew.user where disabled = '0'");
				while($row19 = mysql_fetch_array($getusers)){
					$saveduserid = $row19['userid'];
					$getuserrole = mysql_query("select * from $databasenew.userrole where userid = '$saveduserid' and permissionroleid='1'");
					if(mysql_num_rows($getuserrole)>=1) {
					}				
					else {
						$updateuser = mysql_query("update $databasenew.user set disabled='1' where userid = '$saveduserid'");					
					}
				}	
				
				$file_directory = "../../documents/".$databasenew;
				mkdir($file_directory);

				//go to homepage
	     		$url = '../pagegrid.php?pagetype=business';
	      		echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">'; 
			}
			?>				
			
			<div class='bodycontent'>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<?php 
	  				if(isset($validate) && $validate <> "") {
						echo "<p class='background-warning'>".$validate."</p>";
					} 
					?>
				
			<p><?php echo $langval37 ?></p>
			<br/>
			</div>
			<form class="form-horizontal" action='stagingcreation.php?rowid=<?php echo $rowid?>' method="post">
			  	<div class="form-group">
			    	<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
			    		<div style="text-align:center;">
			      		<button type='submit' name='submit' value='Submit' class="button-primary"><?php echo $langval38 ?></button>																									
			    		</div>
			    	</div>
			  	</div>
			</form> 
			<br/>
			<script>
			$(document).ready(function(){
			    $('#myTable4').dataTable({
			 });
			});
			</script>
			
			
					<br/><br/><br/>
				</div>
	  		</div>
		</div>
			
	</div>	
	<?php include_once ('../footer.php'); ?>
</body>
