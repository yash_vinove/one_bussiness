<?php session_start(); ?>
<?php include('config.php'); ?>
<?php include('sessionconfig.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	
 	<title>OneBusiness</title>

 	<!-- Bootstrap -->
 	<link href="bootstrap-3.3.1/dist/css/bootstrap.css" rel="stylesheet">

 	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
 	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">   
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
	<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script type="text/javascript">
    function redirectMe (sel) {
        var url = sel[sel.selectedIndex].value;
        window.location = url;
    }
    </script>
   	<script type='text/javascript'>
	$(document).ready(function() {
		var option = 'intro-tekst';
		var url = window.location.href;
		option = url.match(/option=(.*)/)[1];
	});
	function showLess(option) {
		$('.boxes').hide();
		$('#' + option).show();
	} 
	</script>	
</head>
  
<body class='greybody' style=''>
	<?php include_once ('headerthree.php');	?>
	<?php 
	//get structurefunction details
	$pagetype = isset($_GET['pagetype']) ? $_GET['pagetype'] : '';
   	$getpagedata = mysql_query("select * from structurefunction where structurefunction.tablename = '$pagetype'");
	$pagedatarow = mysql_fetch_array($getpagedata);
	$functionname = $pagedatarow['functionname'];	   	
	$structurefunctionid = $pagedatarow['structurefunctionid'];	  
	$valresult = "passed"; 	
	
	
	?>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:10px;">
		<!--search results header bar for desktop-->
		<div style="padding:10px;background-color:#e9e9e9;border-bottom:1px solid #A0A0A0;">
			<b><?php echo $functionname ?> Uploader</b>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:10px;background-color:#ffffff;border-bottom:1px solid #A0A0A0;">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			
		
		
			<?php
			//define variables
			for($i = 0; $i <= 100; $i++) {
	  			${"fieldname$i"} = "";
			}
		   for($i = 0; $i <= 100; $i++) {
	  			${"fieldtypename$i"} = "";
			}
		   	for($i = 0; $i <= 100; $i++) {
	  			${"displayname$i"} = "";
			}
			for($i = 0; $i <= 100; $i++) {
	  			${"structurefieldtypeid$i"} = "";
			}
			
			
			//get list of all fields in structurefunction
			$gridcontents = mysql_query("select * from structurefield 
			inner join structurefieldtype on structurefield.structurefieldtypeid = structurefieldtype.structurefieldtypeid			
			where structurefunctionid = '$structurefunctionid' and disabled=0
			order by fieldposition asc");
			$fieldid = 1;
			while($gridcontentsrow=mysql_fetch_array($gridcontents)){
				$fieldname = $gridcontentsrow['fieldname'];
				$displayname = $gridcontentsrow['displayname'];
				$structurefieldtypeid = $gridcontentsrow['structurefieldtypeid'];
				$structurefieldtypename = $gridcontentsrow['fieldtypename'];
				//echo $fieldid." - ".$fieldname."<br/>";
				${'fieldname'.$fieldid} = $fieldname;
				${'displayname'.$fieldid} = $displayname;
				${'fieldtypename'.$fieldid} = $structurefieldtypename;
				${'structurefieldtypeid'.$fieldid} = $structurefieldtypeid;
				$fieldid = $fieldid +1;
			}			
			$countoffields = $fieldid - 1;
					
			
			//write fields to the csv header
			$counter4 = 1;
	      	$csv_hdr = "";
     		while($counter4 <= $countoffields) {
     			$value = ${'displayname'.$counter4};
     			$csv_hdr .= $value;
         		if($counter4 <> $countoffields){ 
         			$csv_hdr .= ",";
         		}
         		$counter4 = $counter4 +1;	            		
     		}
			
			//provide csv download button
			?>
			<form name="export" action="export.php" method="post">
			    <input class="btn btn-primary" type="submit" value="Download Sample Spreadsheet">
			    <input type="hidden" value='<?php echo $csv_hdr; ?>' name='csv_hdr'>
			</form>
			<?php 
			
			//import data into temp table
			$submit = isset($_POST['submit']) ? $_POST['submit'] : ''; 
			if($submit) {
				//delete existing import for this user, if there is one
				$deleteimport = mysql_query("delete from datauploadtemp where userid = '$userid'");				
				
		   		if ($_FILES['csv']['size'] > 0) {
		   			//get the csv file 
				   	$file = $_FILES['csv']['tmp_name'];
				   	ini_set('auto_detect_line_endings',TRUE);
				   	$handle = fopen($file,"r"); 
				   	$counter56 = 1;
					for($i = 0; $i <= 100; $i++) {
			  			${"content$i"} = "";
					}
					
					$data = array('','','');
					$numcols = 0;
					//loop through the csv file and insert into database
				   	do { 
				   		if ($data[0] || $data[1] || $data[2]) {
				   			//check for no apostrophies and html tags in all fields
				   			if($counter56 == 1){
				   				$counter56 = $counter56+1;
					     		for($i = 0; $i <= 100; $i++) {
					     			if(!isset($data[$i])) {
										$data[$i] = '';					     			
					     			}
					     			${'content'.$i} = str_replace("'", "", $data[$i]);
					     			if(${'content'.$i} == ''){
					     				${'content'.$i} = "";
					     				$numcols = $i;
					     				break;				     			
					     			}
					     		}
					     	$numcols = $numcols-1;
					     	}
					     	if($counter56 >= 1){
					     		for($i = 0; $i <= $numcols; $i++) {
					     			${'content'.$i} = str_replace("'", "", $data[$i]);
					     		}
					     	}
				     		//add record to database
				     		$querybuilder = "INSERT INTO datauploadtemp (userid, ";
				     		for($i = 1; $i <= 100; $i++) {
			  					$querybuilder = $querybuilder."field".$i.", ";
							}
							$querybuilder = rtrim($querybuilder, ', ');
				     		$querybuilder = $querybuilder.") VALUES (";
				         	$querybuilder = $querybuilder." '".$userid."',";
				         	for($i = 0; $i <= 99; $i++) {
					         	$querybuilder = $querybuilder." '".addslashes(${'content'.$i})."', ";
				         	}
				         	$querybuilder = rtrim($querybuilder, ', ');
				         	$querybuilder = $querybuilder.")";
				     		$query = mysql_query($querybuilder); 
				        	//echo $querybuilder;
				      	} 
				   	} while ($data = fgetcsv($handle,1000,",","'")); 
				   
					//get all details of top row
					$querybuilder = "select min(datauploadtempid) as number, ";
					for($i = 1; $i <= 100; $i++) {
	  					$querybuilder = $querybuilder."field".$i.", ";
					}
					$querybuilder = rtrim($querybuilder, ', ');
		     		$querybuilder = $querybuilder." from datauploadtemp where userid = '".$userid."'";
		     		//echo $querybuilder;
					$gettop = mysql_query($querybuilder);
					$gettoprow = mysql_fetch_array($gettop);

					//check how many columns there are
					$numberofcols = 0;
					$fieldid = 1;
					for($i = 1; $i <= $numcols; $i++) {
						${'saveddisplayname'.$fieldid} = $gettoprow['field'.$i];
						//echo " ".${'displayname'.$fieldid}."=".${'saveddisplayname'.$fieldid}.", ";
						if(${'saveddisplayname'.$fieldid} == ""){
							if($numberofcols == 0) {
								$numberofcols = $i - 1;
							}
							
						}
						$fieldid = $fieldid +1;
					}					
					//echo $numberofcols;
					
				   	//redirect back to the page
					ini_set('auto_ detect_line_endings',FALSE); 
					
					//set validation values
					$validate = ""; 
					$validationresult = "passed";					
					
					//check that fieldnames are the same					
					$value = 0;
					$columnsvalidation = "";
					for($i = 1; $i <= $numberofcols; $i++) {
						if(${'displayname'.$i} == ${'saveddisplayname'.$fieldid}) {
			  				//echo "pass";
						}
						else {
							$value = $value+1;
			  				$columnsvalidation = $columnsvalidation."Column Name ".${'saveddisplayname'.$fieldid}." is incorrect - expecting ".${'displayname'.$i}.". ";
						}
					}
					if($value == 0) {
						$columns = '1';
						$validationresult = "passed";
					}
					else {
						$columns = '0';
						$validationresult = "failed";					
					}
					//echo $columns;
					//echo $validationresult;
					//echo $numberofcols;
					
					if($validationresult == "passed") {
						//remove top header
						$gettop = mysql_query("select min(datauploadtempid) as number from datauploadtemp 
						where userid = '$userid'");
						$gettoprow = mysql_fetch_array($gettop);
						$deleteid = $gettoprow['number'];
						$deleterow = mysql_query("delete from datauploadtemp where datauploadtempid = '$deleteid'");
						
						$numcols = $numcols +1;
						
						//check validation of each line
						$getdata = mysql_query("select * from datauploadtemp where userid = '$userid'");
						while($row5=mysql_fetch_array($getdata)) {
							$validate = "";
							
							for($i = 1; $i <= $numcols; $i++) {
								${'value'.$i} = $row5['field'.$i];
  								$datauploadtempid = $row5['datauploadtempid'];
  								
  								$getvalidationterms = mysql_query("select * from structurefield where fieldname = '${"fieldname$i"}' and structurefunctionid = '$structurefunctionid'");	
								$validationrow = mysql_fetch_array($getvalidationterms);
								$displayname = $validationrow['displayname'];
								$requiredfield = $validationrow['requiredfield'];
								$tableconnect = $validationrow['tableconnect'];
								$tablefieldconnect = $validationrow['tablefieldconnect'];
								$textlimit = $validationrow['textlimit'];
								$minvalue = $validationrow['lowvalue'];
								$maxvalue = $validationrow['highvalue'];
								//echo "i = ".$i."<br/>";
								//echo "value = ".${'value'.$i}."<br/>";
								//echo "displayname = ".$displayname."<br/>";
								//echo "requiredfield = ".$requiredfield."<br/>";
								//echo "textlimit = ".$textlimit."<br/>";
								//echo "fieldtypename = ".${'fieldtypename'.$i}."<br/>";
	  							
	  							
	  							//check for required fields - all field types
	  							if($requiredfield == 1 && ${'value'.$i} == "") {
									$validate = $validate.$displayname." is required. "; 
					 			}
					 		
					 			
					 			//check that text is below maximum limit - 1
					 			if(${'fieldtypename'.$i} == 'Text' && strlen(${'value'.$i}) > $textlimit){
									$validate = $validate.$displayname." is over maximum characters: ".$textlimit.". ";					 			
					 			}
					 			
					 			
					 			//check that unique tableid is not text - 2
					 			if(${'fieldtypename'.$i} == 'Unique TableID' && !is_numeric(${'value'.$i})){
					 				if (${'value'.$i} == ""){
					 				
					 				}
					 				else {
					 					$validate = $validate.$displayname." is not a number. ";		
					 				}			 			
					 			}
					 			
					 			
					 			//check that status is either active or disabled or blank - 3
					 			if(${'fieldtypename'.$i} == 'Status'){
					 				if(${'value'.$i} == '1' || ${'value'.$i} == '0'){
					 				}
					 				else {
					 					$validate = $validate.$displayname." must be either 1 or 0. ";	
					 				}
								}
					 			
					 			//check that table connects actually exist - 4 + update the value
					 			$value7 = 0;
					 			if(${'fieldtypename'.$i} == 'Table Connect'){
					 				$getrecord = "select ".$tableconnect."id, ".$tablefieldconnect." from ".$tableconnect." where disabled = 0";
					 				$getrecords = mysql_query($getrecord);
					 				//echo $getrecord;
					 				while($row10 = mysql_fetch_array($getrecords)) {
										$name = $row10[$tablefieldconnect];
										$id = $row10[$tableconnect.'id'];
										if(${'value'.$i} == $name) {
											$value7 = 1;	
											$update = "update datauploadtemp set field".$i." = ".$id." where datauploadtempid = ".$datauploadtempid;
											$updateid = mysql_query($update);
											//echo $update."<br/><br/>";									
										}					 				
					 				}
					 				if($value7 == 0) {
					 					$validate = $validate.$displayname." is not a valid entry from the list of entries in the ".$tableconnect." table. ";
					 				}
					 			}
					 			
					 			
					 			//check that value is between min and max - 5
					 			if(${'fieldtypename'.$i} == 'Value'){
					 				if(${'value'.$i} < $minvalue && $minvalue <>0){
					 					$validate = $validate.$displayname." must be greater than ".$minvalue.". ";
					 				}
					 				if(${'value'.$i} > $maxvalue && $maxvalue <>0){
					 					$validate = $validate.$displayname." must be less than ".$maxvalue.". ";
					 				}
					 			}
					 			
					 			//check that date formats are correct - 6
					 			if(${'fieldtypename'.$i} == 'Date Select'){
					 				${'value'.$i} = implode('-', array_reverse(explode('/', trim(${'value'.$i}))));
					 				$format = "Y-m-d";
									if(date($format, strtotime(${'value'.$i})) == date(${'value'.$i})) {
										$timestamp = strtotime(${'value'.$i});
						     			$timestamp = date('Y-m-d', $timestamp);
						     			$update = "update datauploadtemp set field".$i." = '".$timestamp."' where datauploadtempid = ".$datauploadtempid;
										$updateid = mysql_query($update);
										//echo $update;										
									} 
									else {
									    $validate = $validate.$displayname." must be a date in format Y-m-d. ";
									}
						 			
		  						}
						   }
							//echo "<b>".$validate."<br/><br/><br/></b>";
							if($validate == ""){
							}
							else {
								$valresult = "failed";							
							}
						   $updatefailed = mysql_query("update datauploadtemp set validationresult = '$validate' 
							where datauploadtempid = '$datauploadtempid'");						 		
						}
					
						
				 		//update the tables
						if($valresult == "passed") {
							//identify type of record and append type into table
							$getdata77 = mysql_query("select * from datauploadtemp where userid = '$userid'");
							while($row77=mysql_fetch_array($getdata77)) {
								//find out if update or add
								$check = $row77['field1'];
								$datauploadtempid = $row77['datauploadtempid'];
								if($check >= 1) {
									//update records that have an id
									//echo "update".$datauploadtempid;
									$queryupdate = "update ".$pagetype." set ";
									for($i = 2; $i <= $numcols; $i++) {
							  			${"content$i"} = $row77['field'.$i];
							  			$queryupdate = $queryupdate.${"fieldname$i"}." = '".${"content$i"}."', ";
									}
									$queryupdate = rtrim($queryupdate, ', '); 
									$queryupdate = $queryupdate." where ".$pagetype."id = ".$check;
									$queryrun = mysql_query($queryupdate);
									//echo $queryupdate."<br/><br/>";
								}
								else {
									//add in new records
									//echo "add".$datauploadtempid;
									$createddate = date("Y-m-d");
						
									$querybuilder = "insert into ".$pagetype." (";
									$number11 = 2;
									while($number11 <= $countoffields) {
										if($number11 == $countoffields) {
											$querybuilder .=${"fieldname$number11"};
										}
										else {
											$querybuilder .=${"fieldname$number11"}.", ";
										}
										$number11 = $number11 + 1;					
									}
									$querybuilder = $querybuilder.", datecreated) values (";
									$number11 = 2;
									
									while($number11 <= $countoffields) {
										${"content$number11"} = $row77['field'.$number11];
										if($number11 == $countoffields) {
											$querybuilder .="'".${"content$number11"}."'";
										}
										else {
											$querybuilder .="'".${"content$number11"}."', ";
										}
										$number11 = $number11 + 1;					
									}
									$querybuilder = $querybuilder.", '".$createddate."')";
									//echo "<br/>".$querybuilder."<br/><br/>";
								   //run the query
								   $insertrecord = mysql_query($querybuilder);
								}
							}	
														
						}
								
							}
							$url = 'pagegridupload.php?pagetype='.$pagetype.'&processed=1&validation='.$valresult.'&columns='.$columns.'&columnsvalidation='.$columnsvalidation;
							echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">';    												
						} 
					}
								

			?>
			<script>
				$(document).ready(function(){
				    $('#myTable').dataTable({
				    	"searching": false
				 });
				});
			</script>
			
			<br/>	
			<form action="" method="post" enctype="multipart/form-data" name="form1" id="form1"> 
			  <b>Upload import spreadsheet (csv format only): </b><br/><br/> 
			  <input name="csv" type="file" id="csv" /> 
			  <br/>
			  <input class="btn btn-primary" type="submit" name="submit" value="Submit" /> 
			</form> 	
			<br/>
			<?php
			//show error
			if (!empty($_GET['processed'])) { 
				//return validation results
				$valresult = isset($_GET['validation']) ? strip_tags($_GET['validation']) : '';
				$columnsvalidation = isset($_GET['columnsvalidation']) ? strip_tags($_GET['columnsvalidation']) : '';
				$columns = isset($_GET['columns']) ? strip_tags($_GET['columns']) : '';
				if($valresult == "failed") {
					if($columns == '1') {
						echo "<p>Validation has failed - please correct the following errors in your file and resubmit the entire file. None of your records have been inserted.</p>";
						$querybuilder = "select * from datauploadtemp where userid = '$userid' and validationresult <> ''";
					   	$querybuilder = $querybuilder." order by datauploadtempid";
				     	$query = $querybuilder;
				     	//echo $querybuilder;
				      	$result = mysql_query($query);
						if(mysql_num_rows($result)>0){
				         	echo "<div class='table-responsive'><table id='myTable' class='table table-bordered'>";
				         	echo "<thead><tr>";
				        	echo "<th>Field 1</th>";
				         	echo "<th>Field 2</th>";
				         	echo "<th>Field 3</th>";
				         	echo "<th>Validation Errors</th>";
				         	echo "</tr></thead>";
				         	while($row=mysql_fetch_array($result)){
				         		echo "<tr>";
			        			echo "<td>".$row['field1']."</td>";
			        			echo "<td>".$row['field2']."</td>";
			        			echo "<td>".$row['field3']."</td>";
			        			echo "<td>".$row['validationresult']."</td>";
			            		echo "</tr>";
		            		}
		      	    		echo "</table></div>";
		          	}
		          	else {
		            		echo "No results";
		          	}
		          }
		      		else {
						echo "<p>Validation has failed - columns have been deleted or are in the wrong order. Please redownload the sample file and ensure you have all of the columns. Ensure you have not changed anything in the top line - the top two line must exist, and the column names must not have been changed.</p>";		      		
		      			echo $columnsvalidation;
		      		}				
				}
				else {
					echo "<b>Your file has been imported successfully.</b><br><br>";
				}
			} 
		
			?> 
		
			</div>	
		</div>	
	</div>	
	
	<?php include_once ('footer.php'); ?>
</body>
