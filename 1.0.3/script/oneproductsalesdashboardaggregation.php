<?php
set_time_limit(500);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$hour = date('H');
$date = date('Y-m-d');
$date30 = date('Y-m-d', strtotime("- 30 days"));
//$all = 1;
echo "<br/>hour: ".$hour."<br/>";
$insertarray = array();

echo "<br/><br/><b>Product Sales Value By Month</b>";
if($hour == 0 || $all = 1){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from productdashboardstat where productstatdate >= '$date30' 
	and productstattype = 'Product Sales Value By Month'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("select productstockitem.selldate as 'Dates', 
	round(sum(if(usebudgetfxrates = 1, (productstockitem.sellprice / currencytohomecurrencybudgetfxrate), 
	(productstockitem.sellprice / currencytohqcurrencycurrentfxrate))), 2) as 'Value' 
	from productstockitem 
	left join financialbudgetperiod on financialbudgetperiod.startdate <= productstockitem.selldate 
	and financialbudgetperiod.enddate >= productstockitem.selldate 
	left join financialbudgetperiodcurrencyfxrate on financialbudgetperiodcurrencyfxrate.currencyid = productstockitem.currencyid 
	and financialbudgetperiod.financialbudgetperiodid = financialbudgetperiodcurrencyfxrate.financialbudgetperiodid 
	left join currencyfxrate on currencyfxrate.currencyid = productstockitem.currencyid 
	left join onefinanceconfiguration on onefinanceconfiguration.disabled = productstockitem.disabled 
	where sellprice > 0 and selldate >= '$date30'
	group by productstockitem.selldate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Product Sales Value By Month';
		$statdate = $queryrow['Dates'];
		$statname = "";
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Product Stream Sales Value</b>";
if($hour == 1 || $all = 1){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from productdashboardstat where productstatdate >= '$date30' 
	and productstattype = 'Product Stream Sales Value'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("select productstream.productstreamname as 'Name', selldate as 'Dates',
	round(sum(if(usebudgetfxrates = 1, (productstockitem.sellprice / currencytohomecurrencybudgetfxrate), 
	(productstockitem.sellprice / currencytohqcurrencycurrentfxrate))), 2) as 'Value' 
	from productstream 
	inner join product on product.productstreamid = productstream.productstreamid 
	inner join productstockitem on productstockitem.productid = product.productid 
	left join financialbudgetperiod on financialbudgetperiod.startdate <= productstockitem.selldate 
	and financialbudgetperiod.enddate >= productstockitem.selldate 
	left join financialbudgetperiodcurrencyfxrate on financialbudgetperiodcurrencyfxrate.currencyid = productstockitem.currencyid 
	and financialbudgetperiod.financialbudgetperiodid = financialbudgetperiodcurrencyfxrate.financialbudgetperiodid 
	left join currencyfxrate on currencyfxrate.currencyid = productstockitem.currencyid 
	left join onefinanceconfiguration on onefinanceconfiguration.disabled = productstockitem.disabled 
	where sellprice > 0 and selldate >= '$date30'
	group by productstream.productstreamname, selldate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Product Stream Sales Value';
		$statdate = $queryrow['Dates'];
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Product Line Sales Value</b>";
if($hour == 2 || $all = 1){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from productdashboardstat where productstatdate >= '$date30' 
	and productstattype = 'Product Line Sales Value'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select productline.productlinename as 'Name', selldate as 'Dates', 
	round(sum(if(usebudgetfxrates = 1, (productstockitem.sellprice / currencytohomecurrencybudgetfxrate), 
	(productstockitem.sellprice / currencytohqcurrencycurrentfxrate))), 2) as 'Value' 
	from productline 
	inner join productstream on productstream.productlineid = productline.productlineid 
	inner join product on product.productstreamid = productstream.productstreamid 
	inner join productstockitem on product.productid = productstockitem.productid 
	left join financialbudgetperiod on financialbudgetperiod.startdate <= productstockitem.selldate 
	and financialbudgetperiod.enddate >= productstockitem.selldate 
	left join financialbudgetperiodcurrencyfxrate on financialbudgetperiodcurrencyfxrate.currencyid = productstockitem.currencyid 
	and financialbudgetperiod.financialbudgetperiodid = financialbudgetperiodcurrencyfxrate.financialbudgetperiodid 
	left join currencyfxrate on currencyfxrate.currencyid = productstockitem.currencyid 
	left join onefinanceconfiguration on onefinanceconfiguration.disabled = productstockitem.disabled 
	where sellprice > 0 and selldate >= '$date30'
	group by productline.productlinename, selldate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Product Line Sales Value';
		$statdate = $queryrow['Dates'];
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Product Sales Volume by Month</b>";
if($hour == 3 || $all = 1){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from productdashboardstat where productstatdate >= '$date30' 
	and productstattype = 'Product Sales Volume by Month'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select productstockitem.selldate as 'Dates', 
	COUNT(productstockitem.productstockitemid) as 'Value' 
	from productstockitem 
	where sellprice > 0 and selldate >= '$date30'
	group by selldate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Product Sales Volume by Month';
		$statdate = $queryrow['Dates'];
		$statname = '';
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Total Sales by Sales Rep</b>";
if($hour == 4 || $all = 1){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from productdashboardstat where productstatdate >= '$date30' 
	and productstattype = 'Total Sales by Sales Rep'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select user.username as 'Name', selldate as 'Dates',
	round(sum(if(usebudgetfxrates = 1, (productstockitem.sellprice / currencytohomecurrencybudgetfxrate), 
	(productstockitem.sellprice / currencytohqcurrencycurrentfxrate))), 2) as 'Value' 
	from user inner join productstockitem on user.userid = productstockitem.salespersonuserid 
	left join financialbudgetperiod on financialbudgetperiod.startdate <= productstockitem.selldate 
	and financialbudgetperiod.enddate >= productstockitem.selldate 
	left join financialbudgetperiodcurrencyfxrate on financialbudgetperiodcurrencyfxrate.currencyid = productstockitem.currencyid 
	and financialbudgetperiod.financialbudgetperiodid = financialbudgetperiodcurrencyfxrate.financialbudgetperiodid 
	left join currencyfxrate on currencyfxrate.currencyid = productstockitem.currencyid 
	left join onefinanceconfiguration on onefinanceconfiguration.disabled = productstockitem.disabled 
	where sellprice > 0 and selldate >= '$date30'
	group by user.username, selldate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Total Sales by Sales Rep';
		$statdate = $queryrow['Dates'];
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Total Sales by Customer</b>";
if($hour == 5 || $all = 1){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from productdashboardstat where productstatdate >= '$date30' 
	and productstattype = 'Total Sales by Customer'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select customer.customername as 'Name', round(sum(if(usebudgetfxrates = 1, 
	(productstockitem.sellprice / currencytohomecurrencybudgetfxrate), (productstockitem.sellprice / currencytohqcurrencycurrentfxrate))), 2) 
	as 'Value', selldate as 'Dates'
	from customer 
	inner join productstockitem on customer.customerid = productstockitem.customerid 
	left join financialbudgetperiod on financialbudgetperiod.startdate <= productstockitem.selldate 
	and financialbudgetperiod.enddate >= productstockitem.selldate 
	left join financialbudgetperiodcurrencyfxrate on financialbudgetperiodcurrencyfxrate.currencyid = productstockitem.currencyid 
	and financialbudgetperiod.financialbudgetperiodid = financialbudgetperiodcurrencyfxrate.financialbudgetperiodid
	left join currencyfxrate on currencyfxrate.currencyid = productstockitem.currencyid 
	left join onefinanceconfiguration on onefinanceconfiguration.disabled = productstockitem.disabled 
	where sellprice > 0 and selldate >= '$date30'
	group by customer.customername, selldate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Total Sales by Customer';
		$statdate = $queryrow['Dates'];
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Total Sales by Sales Channel 1</b>";
if($hour == 6 || $all = 1){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from productdashboardstat where productstatdate >= '$date30' 
	and productstattype = 'Total Sales by Sales Channel 1'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select saleschannel.saleschannelname as 'Name', selldate as 'Dates',
	round(sum(if(usebudgetfxrates = 1, (productstockitem.sellprice / currencytohomecurrencybudgetfxrate), 
	(productstockitem.sellprice / currencytohqcurrencycurrentfxrate))), 2) as 'Value' 
	from saleschannel 
	inner join productstockitem on saleschannel.saleschannelid = productstockitem.saleschannelid 
	left join financialbudgetperiod on financialbudgetperiod.startdate <= productstockitem.selldate 
	and financialbudgetperiod.enddate >= productstockitem.selldate 
	left join financialbudgetperiodcurrencyfxrate on financialbudgetperiodcurrencyfxrate.currencyid = productstockitem.currencyid 
	and financialbudgetperiod.financialbudgetperiodid = financialbudgetperiodcurrencyfxrate.financialbudgetperiodid 
	left join currencyfxrate on currencyfxrate.currencyid = productstockitem.currencyid 
	left join onefinanceconfiguration on onefinanceconfiguration.disabled = productstockitem.disabled 
	where sellprice > 0 and selldate >= '$date30'
	group by saleschannel.saleschannelname, selldate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Total Sales by Sales Channel 1';
		$statdate = $queryrow['Dates'];
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Total Sales by Sales Channel Type</b>";
if($hour == 7 || $all = 1){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from productdashboardstat where productstatdate >= '$date30' 
	and productstattype = 'Total Sales by Sales Channel Type'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select saleschanneltype.saleschanneltypename as 'Name', selldate as 'Dates',
	round(sum(if(usebudgetfxrates = 1, (productstockitem.sellprice / currencytohomecurrencybudgetfxrate), 
	(productstockitem.sellprice / currencytohqcurrencycurrentfxrate))), 2) as 'Value' 
	from saleschanneltype 
	inner join saleschannel on saleschannel.saleschanneltypeid = saleschanneltype.saleschanneltypeid 
	inner join productstockitem on saleschannel.saleschannelid = productstockitem.saleschannelid 
	left join financialbudgetperiod on financialbudgetperiod.startdate <= productstockitem.selldate 
	and financialbudgetperiod.enddate >= productstockitem.selldate 
	left join financialbudgetperiodcurrencyfxrate on financialbudgetperiodcurrencyfxrate.currencyid = productstockitem.currencyid 
	and financialbudgetperiod.financialbudgetperiodid = financialbudgetperiodcurrencyfxrate.financialbudgetperiodid 
	left join currencyfxrate on currencyfxrate.currencyid = productstockitem.currencyid 
	left join onefinanceconfiguration on onefinanceconfiguration.disabled = productstockitem.disabled 
	where sellprice > 0 and selldate >= '$date30'
	group by saleschanneltype.saleschanneltypename, selldate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Total Sales by Sales Channel Type';
		$statdate = $queryrow['Dates'];
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Top Performing Products by Volume</b>";
if($hour == 8 || $all = 1){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from productdashboardstat where productstatdate >= '$date30' 
	and productstattype = 'Top Performing Products by Volume'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select product.productname as 'Name', selldate as 'Dates',
	COUNT(productstockitem.productstockitemid) as 'Value' 
	from product 
	inner join productstockitem on product.productid = productstockitem.productid 
	inner join productstockitemstatus on productstockitemstatus.productstockitemstatusid = productstockitem.productstockitemstatusid 
	where productstockitemstatus.productstockitemstatusname like '%Sold%' and selldate >= '$date30'
	group by product.productname, selldate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Top Performing Products by Volume';
		$statdate = $queryrow['Dates'];
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Top Performing Products by Value</b>";
if($hour == 9 || $all = 1){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from productdashboardstat where productstatdate >= '$date30' 
	and productstattype = 'Top Performing Products by Value'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select product.productname as 'Name', selldate as 'Dates',
	round(sum(if(usebudgetfxrates = 1, (productstockitem.sellprice / currencytohomecurrencybudgetfxrate), 
	(productstockitem.sellprice / currencytohqcurrencycurrentfxrate))), 2) as 'Value' 
	from product 
	inner join productstockitem on product.productid = productstockitem.productid 
	left join financialbudgetperiod on financialbudgetperiod.startdate <= productstockitem.selldate 
	and financialbudgetperiod.enddate >= productstockitem.selldate 
	left join financialbudgetperiodcurrencyfxrate on financialbudgetperiodcurrencyfxrate.currencyid = productstockitem.currencyid 
	and financialbudgetperiod.financialbudgetperiodid = financialbudgetperiodcurrencyfxrate.financialbudgetperiodid 
	left join currencyfxrate on currencyfxrate.currencyid = productstockitem.currencyid 
	left join onefinanceconfiguration on onefinanceconfiguration.disabled = productstockitem.disabled 
	where selldate >= '$date30'
	group by product.productname, selldate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Top Performing Products by Value';
		$statdate = $queryrow['Dates'];
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Volume of Products Sold by Month</b>";
if($hour == 10 || $all = 1){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from productdashboardstat where productstatdate >= '$date30' 
	and productstattype = 'Volume of Products Sold by Month'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select productstockitem.selldate as 'Dates', 
	COUNT(productstockitem.productstockitemid) as 'Value' 
	from productstockitem 
	inner join productstockitemstatus on productstockitemstatus.productstockitemstatusid = productstockitem.productstockitemstatusid 
	where productstockitemstatus.productstockitemstatusname like '%Sold%' and selldate >= '$date30'
	group by selldate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Volume of Products Sold by Month';
		$statdate = $queryrow['Dates'];
		$statname = '';
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Number of Products by Product Line</b>";
if($hour == 11 || $all = 1){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from productdashboardstat where productstatdate >= '$date30' 
	and productstattype = 'Number of Products by Product Line'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select productline.productlinename as 'Name', 
	COUNT(product.productid) as 'Value' 
	from productline 
	inner join productstream on productstream.productlineid = productline.productlineid 
	inner join product on product.productstreamid = productstream.productstreamid 
	where product.disabled like '%0%'
	group by productline.productlinename");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Number of Products by Product Line';
		$statdate = '';
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Number of Products by Product Stream</b>";
if($hour == 12 || $all = 1){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from productdashboardstat where productstatdate >= '$date30' 
	and productstattype = 'Number of Products by Product Stream'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select productstream.productstreamname as 'Name', 
	COUNT(product.productid) as 'Value' 
	from productstream 
	inner join product on productstream.productstreamid = product.productstreamid 
	where product.disabled like '%0%'
	group by productstream.productstreamname");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Number of Products by Product Stream';
		$statdate = '';
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Top Performing Products by Value</b>";
if($hour == 13 || $all = 1){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from productdashboardstat where productstatdate >= '$date30' 
	and productstattype = 'Top Performing Products by Value'");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select product.productname as 'Name', selldate as 'Dates',
	round(sum(if(usebudgetfxrates = 1, (productstockitem.sellprice / currencytohomecurrencybudgetfxrate), 
	(productstockitem.sellprice / currencytohqcurrencycurrentfxrate))), 2) as 'Value' 
	from product 
	inner join productstockitem on product.productid = productstockitem.productid 
	left join financialbudgetperiod on financialbudgetperiod.startdate <= productstockitem.selldate 
	and financialbudgetperiod.enddate >= productstockitem.selldate 
	left join financialbudgetperiodcurrencyfxrate on financialbudgetperiodcurrencyfxrate.currencyid = productstockitem.currencyid 
	and financialbudgetperiod.financialbudgetperiodid = financialbudgetperiodcurrencyfxrate.financialbudgetperiodid 
	left join currencyfxrate on currencyfxrate.currencyid = productstockitem.currencyid 
	left join onefinanceconfiguration on onefinanceconfiguration.disabled = productstockitem.disabled 
	where selldate >= '$date30'
	group by product.productname, selldate");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Top Performing Products by Value';
		$statdate = $queryrow['Dates'];
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Value of Current Stock by Product Line</b>";
//needs refactoring - query is taking over 100 seconds to run
/*
if($hour == 14) || $all = 1{
	
	//delete all existing data for last 30 days
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from productdashboardstat where 
	productstattype in ('Value of Current Stock by Product Line')");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select productline.productlinename as 'Name', 
	round(sum(if(usebudgetfxrates = 1, (productstockitem.buyprice / currencytohomecurrencybudgetfxrate), 
	(productstockitem.buyprice / currencytohqcurrencycurrentfxrate))), 2) as 'Value'
	from productline 
	inner join productstream on productstream.productlineid = productline.productlineid 
	inner join product on product.productstreamid = productstream.productstreamid 
	inner join productstockitem on productstockitem.productid = product.productid 
	left join financialbudgetperiod on financialbudgetperiod.startdate <= productstockitem.buydate 
	and financialbudgetperiod.enddate >= productstockitem.buydate 
	left join financialbudgetperiodcurrencyfxrate on financialbudgetperiodcurrencyfxrate.currencyid = productstockitem.currencyid 
	and financialbudgetperiod.financialbudgetperiodid = financialbudgetperiodcurrencyfxrate.financialbudgetperiodid 
	left join currencyfxrate on currencyfxrate.currencyid = productstockitem.currencyid 
	left join onefinanceconfiguration on onefinanceconfiguration.disabled = productstockitem.disabled 
	where productstockitemstatusid = 1
	group by productline.productlinename");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Value of Current Stock by Product Line';
		$statdate = '';
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}
*/

echo "<br/><br/><b>Value of Current Stock by Product Stream</b>";
if($hour == 15 || $all = 1){
	
	//delete all existing data for last 30 days
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from productdashboardstat where 
	productstattype in ('Value of Current Stock by Product Stream')");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select productstream.productstreamname as 'Name', 
	round(sum(if(usebudgetfxrates = 1, (productstockitem.buyprice / currencytohomecurrencybudgetfxrate), 
	(productstockitem.buyprice / currencytohqcurrencycurrentfxrate))), 2) as 'Value' 
	from productstream 
	inner join product on product.productstreamid = productstream.productstreamid 
	inner join productstockitem on productstockitem.productid = product.productid 
	left join financialbudgetperiod on financialbudgetperiod.startdate <= productstockitem.buydate 
	and financialbudgetperiod.enddate >= productstockitem.buydate 
	left join financialbudgetperiodcurrencyfxrate on financialbudgetperiodcurrencyfxrate.currencyid = productstockitem.currencyid 
	and financialbudgetperiod.financialbudgetperiodid = financialbudgetperiodcurrencyfxrate.financialbudgetperiodid 
	left join currencyfxrate on currencyfxrate.currencyid = productstockitem.currencyid 
	left join onefinanceconfiguration on onefinanceconfiguration.disabled = productstockitem.disabled 
	where productstockitemstatusid = 1
	group by productstream.productstreamname");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Value of Current Stock by Product Stream';
		$statdate = '';
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Volume of Stock by Product Line</b>";
if($hour == 16 || $all = 1){
	
	//delete all existing data for last 30 days
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from productdashboardstat where 
	productstattype in ('Volume of Stock by Product Line')");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select productline.productlinename as 'Name', 
	COUNT(productstockitem.productstockitemid) as 'Value' 
	from productline 
	inner join productstream on productstream.productlineid = productline.productlineid 
	inner join product on product.productstreamid = productstream.productstreamid 
	inner join productstockitem on product.productid = productstockitem.productid 
	where productstockitemstatusid = 1 
	group by productline.productlinename");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Volume of Stock by Product Line';
		$statdate = '';
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}

echo "<br/><br/><b>Volume of Stock by Product Stream</b>";
if($hour == 17 || $all = 1){
	
	//delete all existing data for last 30 days
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("delete from productdashboardstat where 
	productstattype in ('Volume of Stock by Product Stream')");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete existing: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;	
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$query = mysql_query("Select productstream.productstreamname as 'Name', 
	COUNT(productstockitem.productstockitemid) as 'Value' 
	from productstream 
	inner join product on product.productstreamid = productstream.productstreamid 
	inner join productstockitem on product.productid = productstockitem.productid 
	where productstockitemstatusid = 1 
	group by productstream.productstreamname ");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get data: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($queryrow = mysql_fetch_array($query)){
		$stattype = 'Volume of Stock by Product Stream';
		$statdate = '';
		$statname = $queryrow['Name'];
		$statvalue = $queryrow['Value'];
		array_push($insertarray, array("stattype"=>$stattype, "statdate"=>$statdate, "statname"=>$statname, "statvalue"=>$statvalue));
	}
}




//INSERT ALL THE NEW DATA
echo "<br/><br/>insertarray: ".json_encode($insertarray);
if(count($insertarray) >= 1){
	$i = 1;
	foreach($insertarray as $insertrow){
		$stattype = $insertrow['stattype'];
		$statdate = $insertrow['statdate'];
		$statname = $insertrow['statname'];
		$statname = str_replace("'", "", $statname);
		$statname = str_replace('"', "", $statname);
		$statname = mysql_real_escape_string($statname);
		$statvalue = $insertrow['statvalue'];
		if($i == 1){
			$insert = "insert into productdashboardstat (productstattype, productstatdate, productstatname, productstatvalue, 
			disabled, datecreated, masteronly) values ";	
		}
		$insert = $insert."('$stattype', '$statdate', '$statname', '$statvalue', '0', '$date', '0'), ";
		if($i == 1000){
			$insert = rtrim($insert, ", ");
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$insert = mysql_query($insert);
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Insert 1000 rows: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;		
			$i = 1;
		}
		else {
			$i = $i + 1;
		}
	}
	if($i <> 1){
		$insert = rtrim($insert, ", ");
		echo "<br/><br/>insert: ".$insert;
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$insert = mysql_query($insert);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Insert remaining rows: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;	
	}
}


echo "<br/><br/>";
?>