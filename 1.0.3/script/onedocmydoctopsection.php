<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<?php

//LANGUAGE COLLECTION SECTION
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (614,615,616,617,618,619,620,621)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}

$did = isset($_GET['did']) ? $_GET['did'] : '';
$pagename = $_SERVER['REQUEST_URI'];
$pagename = str_replace("&", "xxxxxxxxxx", $pagename);	
$uid = $_SESSION['userid'];				
if($_SESSION['userbusinessunit'] <> ""){
	$ubusinessunits = $_SESSION['userbusinessunit'];	
}		
//only used for testing - line below - comment out in normal use	
//$ubusinessunits = "1,2,3,4,5";

//view list of drives
if($did == ""){
	echo "<h2>".$langval614."</h2>";
	echo "<p>".$langval615."</p>";
	echo "<a class='button-primary' href='pageadd.php?pagetype=documentdrive&pagename=".$pagename."'>".$langval616."</a><br/><br/>";
	$driveids = array();
	
	//get drives owned by this user OR everyone is allowed access
	$getdata = mysql_query("select documentdrive.documentdriveid from $database.documentdrive 
	left join $database.documentpermission on documentpermission.documentdriveid = documentdrive.documentdriveid
	where documentdrive.disabled = '0' and (documentdrive.driveownerid = '$uid' or documentpermission.everyone = 1)");
	while($getdatarow = mysql_fetch_array($getdata)){
		$documentdriveid = $getdatarow['documentdriveid'];
		if (!in_array($documentdriveid, $driveids)) {
	    	array_push($driveids, $documentdriveid);
		}
	}
	
	//get drives where this user has a permission record
	$getdata = mysql_query("select documentdrive.documentdriveid from $database.documentdrive 
	inner join $database.documentpermission on documentpermission.documentdriveid = documentdrive.documentdriveid
	where documentdrive.disabled = '0' and documentpermission.userid = '$uid'");
	while($getdatarow = mysql_fetch_array($getdata)){
		$documentdriveid = $getdatarow['documentdriveid'];
		if (!in_array($documentdriveid, $driveids)) {
	    	array_push($driveids, $documentdriveid);
		}
	}
	
	//get drives where this user has a businessunit that meets criteria
	if(isset($ubusinessunits)){
		$getdata = mysql_query("select documentdrive.documentdriveid from $database.documentdrive 
		inner join $database.documentpermission on documentpermission.documentdriveid = documentdrive.documentdriveid
		where documentdrive.disabled = '0' and documentpermission.businessunitid in ($ubusinessunits)");
		while($getdatarow = mysql_fetch_array($getdata)){
			$documentdriveid = $getdatarow['documentdriveid'];
			if (!in_array($documentdriveid, $driveids)) {
	    		array_push($driveids, $documentdriveid);
			}
		}
		
		//get businessunits within businessunithierarchies
		$getbuhierarchy = mysql_query("select businessunithierarchyid from $database.businessunit
		where businessunitid in ($ubusinessunits)");
		while($getbuhierarchyrow = mysql_fetch_array($getbuhierarchy)){
			$businessunithierarchyid = $getbuhierarchyrow['businessunithierarchyid'];
			$getdata = mysql_query("select documentdrive.documentdriveid from $database.documentdrive 
			inner join $database.documentpermission on documentpermission.documentdriveid = documentdrive.documentdriveid
			where documentdrive.disabled = '0' and documentpermission.businessunithierarchyid = '$businessunithierarchyid'");
			while($getdatarow = mysql_fetch_array($getdata)){
				$documentdriveid = $getdatarow['documentdriveid'];
				if (!in_array($documentdriveid, $driveids)) {
	    			array_push($driveids, $documentdriveid);
				}
			}
		}
		
	}
	
	
	//get drives where I am owner of a folder in the drive OR everyone can see the folder
	$getdata = mysql_query("select documentdrive.documentdriveid from $database.documentdrive 
	left join $database.documentfolder on documentfolder.documentdriveid = documentdrive.documentdriveid
	left join $database.documentpermission on documentpermission.documentfolderid = documentfolder.documentfolderid
	where documentdrive.disabled = '0' and documentfolder.disabled = '0' 
	and (documentfolder.folderownerid = '$uid' or documentpermission.everyone = 1)");
	while($getdatarow = mysql_fetch_array($getdata)){
		$documentdriveid = $getdatarow['documentdriveid'];
		if (!in_array($documentdriveid, $driveids)) {
	    	array_push($driveids, $documentdriveid);
		}
	}
	
	//get drives where I am owner of a document in the drive OR everyone can see the document
	$getdata = mysql_query("select documentdrive.documentdriveid from $database.documentdrive 
	left join $database.documentfolder on documentfolder.documentdriveid = documentdrive.documentdriveid
	left join $database.document on document.documentfolderid = documentfolder.documentfolderid
	left join $database.documentpermission on documentpermission.documentid = document.documentid
	where documentdrive.disabled = '0' and documentfolder.disabled = '0'  and document.disabled = '0' 
	and (document.documentownerid = '$uid' or documentpermission.everyone = 1)");
	while($getdatarow = mysql_fetch_array($getdata)){
		$documentdriveid = $getdatarow['documentdriveid'];
		if (!in_array($documentdriveid, $driveids)) {
	    	array_push($driveids, $documentdriveid);
		}	
	}
	
	//get drives where this user has a permission record to a folder in the drive
	$getdata = mysql_query("select documentdrive.documentdriveid from $database.documentdrive 
	left join $database.documentfolder on documentfolder.documentdriveid = documentdrive.documentdriveid
	left join $database.documentpermission on documentpermission.documentfolderid = documentfolder.documentfolderid
	where documentdrive.disabled = '0'  and documentfolder.disabled = '0' 
	and documentpermission.userid = '$uid'");
	while($getdatarow = mysql_fetch_array($getdata)){
		$documentdriveid = $getdatarow['documentdriveid'];
		if (!in_array($documentdriveid, $driveids)) {
	    	array_push($driveids, $documentdriveid);
		}
	}
	
	//get drives where this user has a permission record to a document in the drive
	$getdata = mysql_query("select documentdrive.documentdriveid from $database.documentdrive 
	left join $database.documentfolder on documentfolder.documentdriveid = documentdrive.documentdriveid
	left join $database.document on document.documentfolderid = documentfolder.documentfolderid
	left join $database.documentpermission on documentpermission.documentid = document.documentid
	where documentdrive.disabled = '0' and documentfolder.disabled = '0'  and document.disabled = '0' 
	and documentpermission.userid = '$uid'");
	while($getdatarow = mysql_fetch_array($getdata)){
		$documentdriveid = $getdatarow['documentdriveid'];
		if (!in_array($documentdriveid, $driveids)) {
	    	array_push($driveids, $documentdriveid);
		}
	}
	
	//get drives where this user has a businessunit that has access to a folder
	if(isset($ubusinessunits)){
		$getdata = mysql_query("select documentdrive.documentdriveid from $database.documentdrive 
		left join $database.documentfolder on documentfolder.documentdriveid = documentdrive.documentdriveid
		left join $database.documentpermission on documentpermission.documentfolderid = documentfolder.documentfolderid
		where documentdrive.disabled = '0' and documentfolder.disabled = '0' 
		and documentpermission.businessunitid in ($ubusinessunits)");
		while($getdatarow = mysql_fetch_array($getdata)){
			$documentdriveid = $getdatarow['documentdriveid'];
			if (!in_array($documentdriveid, $driveids)) {
	    		array_push($driveids, $documentdriveid);
			}
		}
		
		//get businessunits within businessunithierarchies
		$getbuhierarchy = mysql_query("select businessunithierarchyid from $database.businessunit
		where businessunitid in ($ubusinessunits)");
		while($getbuhierarchyrow = mysql_fetch_array($getbuhierarchy)){
			$businessunithierarchyid = $getbuhierarchyrow['businessunithierarchyid'];
			$getdata = mysql_query("select documentdrive.documentdriveid from $database.documentdrive 
			left join $database.documentfolder on documentfolder.documentdriveid = documentdrive.documentdriveid
			left join $database.documentpermission on documentpermission.documentfolderid = documentfolder.documentfolderid
			where documentdrive.disabled = '0' and documentfolder.disabled = '0' 
			and documentpermission.businessunithierarchyid = '$businessunithierarchyid'");
			while($getdatarow = mysql_fetch_array($getdata)){
				$documentdriveid = $getdatarow['documentdriveid'];
				if (!in_array($documentdriveid, $driveids)) {
	    			array_push($driveids, $documentdriveid);
				}
			}
		}
		
	}
	
	//get drives where this user has a businessunit that has access to a document
	if(isset($ubusinessunits)){
		$getdata = mysql_query("select documentdrive.documentdriveid from $database.documentdrive 
		left join $database.documentfolder on documentfolder.documentdriveid = documentdrive.documentdriveid
		left join $database.document on document.documentfolderid = documentfolder.documentfolderid
		left join $database.documentpermission on documentpermission.documentid = document.documentid
		where documentdrive.disabled = '0' and documentfolder.disabled = '0'  and document.disabled = '0' 
		and documentpermission.businessunitid in ($ubusinessunits)");
		while($getdatarow = mysql_fetch_array($getdata)){
			$documentdriveid = $getdatarow['documentdriveid'];
			if (!in_array($documentdriveid, $driveids)) {
	    		array_push($driveids, $documentdriveid);
			}
		}
		
		//get businessunits within businessunithierarchies
		$getbuhierarchy = mysql_query("select businessunithierarchyid from $database.businessunit
		where businessunitid in ($ubusinessunits)");
		while($getbuhierarchyrow = mysql_fetch_array($getbuhierarchy)){
			$businessunithierarchyid = $getbuhierarchyrow['businessunithierarchyid'];
			$getdata = mysql_query("select documentdrive.documentdriveid from $database.documentdrive 
			left join $database.documentfolder on documentfolder.documentdriveid = documentdrive.documentdriveid
			left join $database.document on document.documentfolderid = documentfolder.documentfolderid
			left join $database.documentpermission on documentpermission.documentid = document.documentid
			where documentdrive.disabled = '0' and documentfolder.disabled = '0'  and document.disabled = '0' 
			and documentpermission.businessunithierarchyid = '$businessunithierarchyid'");
			while($getdatarow = mysql_fetch_array($getdata)){
				$documentdriveid = $getdatarow['documentdriveid'];
				if (!in_array($documentdriveid, $driveids)) {
	    			array_push($driveids, $documentdriveid);
				}
			}
		}
		
	}
	
	//sort drives into order
	$driveids = rtrim(implode(',', $driveids), ',');
	$getorder = mysql_query("select * from $database.documentdrive 
	where documentdriveid in ($driveids) 
	order by documentdrivename asc");
	$driveids = array();
	while($row99 = mysql_fetch_array($getorder)){
		$documentdriveid = $row99['documentdriveid'];
		array_push($driveids, $documentdriveid);
	}
	
	//iterate through and show drives
	if(count($driveids)>= 1){
		echo "<table class='table table-bordered' style='width:70%'>";
		echo "<thead>";
		echo "<td><b>".$langval617."</b></td>";
		echo "<td><b>".$langval618."</b></td>";
		echo "<td><b>".$langval619."</b></td>";
		echo "<td><b>".$langval620."</b></td>";
		echo "<td><b>".$langval621."</b></td>";
		echo "</thead>";
		foreach($driveids as $driveid){
			$getdata2 = mysql_query("select * from $database.documentdrive 
			inner join $database.user on user.userid = documentdrive.driveownerid
			where documentdriveid = '$driveid'");
			$datarow2 = mysql_fetch_array($getdata2);
			$documentdriveid = $datarow2['documentdriveid'];
			$drivename = $datarow2['documentdrivename'];
			$driveownerid = $datarow2['driveownerid'];
			$driveowner = $datarow2['firstname']." ".$datarow2['lastname'];
			
			//calculate if allowed to edit/permission the drive because they own the drive
			$allowedit = 0;
			if($driveownerid == $uid){
				$allowedit = 1;		
			}		
			
			//calculate if allowed to edit/permission the drive because they have been given editable access to the drive
			$geteditpermission = mysql_query("select sum(allowedit) as 'editable' from $database.documentpermission
			where documentdriveid = '$documentdriveid' and (userid = '$uid' or everyone = '1')
			group by documentdriveid");
			$geteditpermissionrow = mysql_fetch_array($geteditpermission);
			$editable = $geteditpermissionrow['editable'];
			if($editable >= 1){
				$allowedit = 1;	
			}
			if(isset($ubusinessunits)){
				$getdata = mysql_query("select sum(allowedit) as 'editable' from $database.documentpermission
				where documentdriveid = '$documentdriveid' and businessunitid in ($ubusinessunits)
				group by documentdriveid");
				while($getdatarow = mysql_fetch_array($getdata)){
					$editable = $getdatarow['editable'];
					if($editable >= 1){
						$allowedit = 1;	
					}	
				}
				
				//get businessunits within businessunithierarchies
				$getbuhierarchy = mysql_query("select businessunithierarchyid from $database.businessunit
				where businessunitid in ($ubusinessunits)");
				while($getbuhierarchyrow = mysql_fetch_array($getbuhierarchy)){
					$businessunithierarchyid = $getbuhierarchyrow['businessunithierarchyid'];
					$getdata = mysql_query("select sum(allowedit) as 'editable' from $database.documentpermission
					where documentdriveid = '$documentdriveid' and documentpermission.businessunithierarchyid = '$businessunithierarchyid'
					group by documentdriveid");
					while($getdatarow = mysql_fetch_array($getdata)){
						$editable = $getdatarow['editable'];
						if($editable >= 1){
							$allowedit = 1;	
						}	
					}
				}
				
			}
			
			//calculate drivesize
			$getdrivesize = mysql_query("select sum(filesize) as 'drivesize' from $database.document
			inner join $database.documentfolder on documentfolder.documentfolderid = document.documentfolderid
			where documentdriveid = '$documentdriveid'");
			$getdrivesizerow = mysql_fetch_array($getdrivesize);
			$drivesize = round($getdrivesizerow['drivesize'] / 1073741824, 2)." GB";
					
			//show drive rows
			echo "<tr>";
			echo "<td>".$drivename."</td>";
			echo "<td>".$drivesize."</td>";
			echo "<td>".$driveowner."</td>";
			echo "<td><a href='view.php?viewid=28&did=".$documentdriveid."'>".$langval620."</a></td>";
			if($allowedit == 1){
				echo "<td><a href='pageedit.php?pagetype=documentdrive&rowid=".$documentdriveid."&pagename=".$pagename."'>".$langval621."</a></td>";
			}
			else {
				echo "<td></td>";		
			}
			echo "</tr>";
		}
		echo "</table>";
	}
}

?>