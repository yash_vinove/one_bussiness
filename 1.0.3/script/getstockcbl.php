<?php

set_time_limit(10000000);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include('config.php');
$date = date('Y-m-d');


//record the fact that we are updating the inventory
$datetime2 = date("Y-m-d H:i:s");
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$create = mysql_query("insert into zzdropshipstockinventoryupdatelog (zzdropshipstockinventoryupdatelogname, 
datetimeupdated, completed, zzdropshipsupplierid, datecreated) values ('CBL Inventory Update', '$datetime2', '0', 
'1', '$date')");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Record this update in the inventory log: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
$logid = mysql_insert_id();
echo "<br/><br/>logid: ".$logid;


//delete table
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$delete = mysql_query("DROP TABLE zzcblinventorynew");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Drop table: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;

//save old table by renaming
/*
$datetime = date("YmdHis");
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$rename = mysql_query("RENAME TABLE zzcblinventorynew TO zzcblinventorynew$datetime");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Save table in different name: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
*/

//add table
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$rename = mysql_query("CREATE TABLE `zzcblinventorynew` (
  `isbn13` varchar(255) NOT NULL DEFAULT '',
  `qty` varchar(255) DEFAULT NULL,
  `supplier_code` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `ship_cost` varchar(255) DEFAULT NULL,
  `weight_gram` varchar(255) DEFAULT NULL,
  `rrp_inc` varchar(255) DEFAULT NULL,
  `rrp_ex` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Add table: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;

//add primary key
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$rename = mysql_query("ALTER TABLE `zzcblinventorynew`
  ADD PRIMARY KEY (`isbn13`);");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Add table primary key: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;

//load data into table - no longer using - this does it in batches, if you want to use it uncomment its file creation section in getsourcefilescbl2.php
/*
$h = 1;
while($h <= 20){
	$file = 'CBLFTP/cblfiles/cblinventory'.$h.".csv";
	if (file_exists($file)) { 
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$sql = "LOAD DATA LOCAL INFILE '".$file."' 
		INTO TABLE zzcblinventorynew FIELDS TERMINATED BY ',' 
		ENCLOSED BY '\"' LINES TERMINATED BY '\n'";
		if($h == 1){
			$sql = $sql.' IGNORE 1 LINES';		
		}
		$result = mysql_query($sql);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Run insert of records: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	} 
	else { 	
		break;	
	} 
	$h = $h + 1;
}
*/

//load data into table
$file = 'CBLFTP/cblfiles/ebay_export.csv';
if (file_exists($file)) { 
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$sql = "LOAD DATA LOCAL INFILE '".$file."' 
	REPLACE INTO TABLE zzcblinventorynew FIELDS TERMINATED BY ',' 
	ENCLOSED BY '\"' LINES TERMINATED BY '\n' IGNORE 1 LINES";
	$result = mysql_query($sql);
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Run insert of records: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
} 

//update records that are notforsale with stock of 0
echo "<br/><br/><b>UPDATE ALL RECORDS THAT ARE NOT FOR SALE WITH STOCK OF 0</b><br/><br/>";
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$getnotforsale = mysql_query("select * from zzcblnotforsale");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get all records that are not for sale: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
$isbnlist = "";
$p = 1;
while($row12 = mysql_fetch_array($getnotforsale)){
	$isbn = $row12['isbn'];
	$isbn = rtrim($isbn, ' ');
   		
	if($p<= 15000){
		$isbnlist = $isbnlist."'".$isbn."',";
		$p = $p + 1;	
	}
	else {
		$isbnlist = rtrim($isbnlist, ",");
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$deletenotforsale = "delete from zzcblinventorynew where isbn13 in ($isbnlist)";
		echo "<br/>".$deletenotforsale;
		$deletenotforsale = mysql_query($deletenotforsale);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Delete notforsale records: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		$isbnlist = "";	
		$p = 1;
	}
}
if($isbnlist <> ""){
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$deletenotforsale = "delete from zzcblinventorynew where isbn13 in ($isbnlist)";
	echo "<br/>".$deletenotforsale;
	$deletenotforsale = mysql_query($deletenotforsale);
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Delete notforsale records: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
}


//check for stock to be removed using zzdropshipstockremove table 
//these are records that have been purchased recently, and need to be removed temporarily from cbl's inventory list, so that they don't get 
//instantly relisted when there is no stock left
echo "<br/><br/><b>REMOVE ZZDROPSHIPSTOCKREMOVE RECORDS</b>";
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$getremovals = mysql_query("select isbn, count(zzdropshipstockremoveid) as 'number' from zzdropshipstockremove 
where disabled = 0 and zzdropshipsupplierid = 1
group by isbn");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get removal records: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
$isbnlist = "";
$isbnremovearray = array();
while($getremovalrow = mysql_fetch_array($getremovals)){
	$isbn = $getremovalrow['isbn'];
	$number = $getremovalrow['number'];
	$isbnlist = $isbnlist."'".$isbn."',";
	array_push($isbnremovearray, array("isbn"=>$isbn,"number"=>$number));
}
if($isbnlist <> ""){	
	$isbnlist = rtrim($isbnlist, ",");
	echo "<br/><br/>isbnlist: ".$isbnlist;
	echo "<br/><br/>isbnremovearray: ".json_encode($isbnremovearray);
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getexisting = mysql_query("select isbn13, qty from zzcblinventorynew 
	where isbn13 in ($isbnlist)");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get removal records: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	$existingarray = array();
	while($getexistingrow = mysql_fetch_array($getexisting)){
		$isbn13 = $getexistingrow['isbn13'];	
		$qty = $getexistingrow['qty'];
		array_push($existingarray, array("isbn13"=>$isbn13,"qty"=>$qty));
	}
	echo "<br/><br/>existingarray: ".json_encode($existingarray);
	$updatequery = "update zzcblinventorynew set qty = case isbn13 ";
	$isbnlist = "";
	foreach($isbnremovearray as $item){
		$isbn = $item['isbn'];	
		$number = $item['number'];	
		$key = array_search($isbn, array_column($existingarray, 'isbn13'));
		$qty = $existingarray[$key]['qty'];
		$newnumber = $qty - $number;
		if($newnumber < 0){
			$newnumber = 0;		
		}
		$isbnlist = $isbnlist."'".$isbn."',";
		echo "<br/>record: ".$isbn." - ".$number." - ".$qty." - ".$newnumber;
		$updatequery = $updatequery." when ".$isbn." then ".$newnumber;
	}
	$isbnlist = rtrim($isbnlist, ",");
	
	$updatequery = $updatequery." end where isbn13 in ($isbnlist)";
	echo "<br/><br/>updatequery: ".$updatequery;
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$updatequery = mysql_query($updatequery);
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Update new quantities: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
}


//UPDATE ALL RECORDS WITH STOCK OF 1 TO HAVE STOCK OF 0
//THIS ENSURES THAT WE HIT AMAZON'S 4% MINIMUM DESPATCH RATE
//No longer needed as we are managing via pricing algorithms instead
/*
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$updatenotforsale = mysql_query("UPDATE zzcblinventorynew set qty = 0 where qty = 1");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Delete notforsale records: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
*/

//record the completion of the script in the log table
$datetime3 = date("Y-m-d H:i:s");
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$update = mysql_query("update zzdropshipstockinventoryupdatelog set completed = 1, datetimecompleted = '$datetime3'
where zzdropshipstockinventoryupdatelogid = $logid");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Update completion time in log table: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;

//delete all log records that are older than 30 days
$date30 = date('Y-m-d', strtotime("- 30 days"));
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$delete = mysql_query("delete from zzdropshipstockinventoryupdatelog where datecreated <= '$date30'");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Delete old records from log table: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;

echo "<br/><br/>";
include('recordjobstatistics.php');

?>