<?php

$taxperiodid = isset($_GET['rowid']) ? $_GET['rowid'] : '';

$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (115,116,117,118,119,120,121,122,123,124,125,126,127)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}

echo "<h2>".$langval117."</h2>";
echo "<p>".$langval118."</p>";

$gettaxperiod = mysql_query("select startdate, enddate, currency.currencyid, currencycode, taxapproachname, allcountries,
taxpercentage, taxtype.countryid from taxperiod 
inner join taxtype on taxtype.taxtypeid = taxperiod.taxtypeid
inner join $masterdatabase.taxapproach on taxapproach.taxapproachid = taxtype.taxapproachid
inner join currency on currency.currencyid = taxtype.currencyid
where taxperiodid = '$taxperiodid'");
$gettaxperiodrow = mysql_fetch_array($gettaxperiod);
$taxapproachname = $gettaxperiodrow['taxapproachname'];
$currencycode = $gettaxperiodrow['currencycode'];
$taxperiodcurrencyid = $gettaxperiodrow['currencyid'];
$taxperiodcurrencycode = $gettaxperiodrow['currencycode'];
$allcountries = $gettaxperiodrow['allcountries'];
$startdate = $gettaxperiodrow['startdate'];
$enddate = $gettaxperiodrow['enddate'];
$taxpercentage = $gettaxperiodrow['taxpercentage'];
$countryid = $gettaxperiodrow['countryid'];

echo "<table class='table table-bordered' style='width:100%;'>";
		
if($taxapproachname == "Transaction Payment"){
	echo "<thead><td><b>".$langval119."</b></td><td><b>".$langval120."</b></td>";
	echo "<td><b>".$langval121."</b></td><td><b>".$langval122."</b></td><td><b>".$langval123."</b></td>";
	echo "<td><b>".$langval121." (".$taxperiodcurrencycode.")</b></td>";
	echo "<td><b>".$langval122." (".$taxperiodcurrencycode.")</b></td>";
	echo "</thead>";

	$totalprofit = 0;
	$totalincome = 0;
	$totalexpenditure = 0;
	
	$getcurrency = mysql_query("select * from currency where disabled = 0");
	while($row44 = mysql_fetch_array($getcurrency)){
		$currencycode = $row44['currencycode'];	
		$currencyid = $row44['currencyid'];	
		
		if($allcountries == 1){
			$getincome = mysql_query("select currencycode, sum(amount) as 'Income', sum(amountincludingtax) as 'Tax Amount' 
			from financialtransaction 
			inner join currency on currency.currencyid = financialtransaction.currencyid
			inner join financesubcategory on financesubcategory.financesubcategoryid = financialtransaction.financesubcategoryid
			where transactiondate >= '$startdate' and transactiondate <= '$enddate' 
			and financialtransaction.currencyid = '$currencyid' and financecategoryid = '1' and financialtransactionstatusid in (3,5)
			group by currencycode");
			$row55 = mysql_fetch_array($getincome);
			$income = $row55['Income'];	
			$incometax = $row55['Tax Amount'] - $income;	
			
			$getexpenditure = mysql_query("select currencycode, sum(amount) as 'Expenditure', sum(amountincludingtax) as 'Tax Amount'  
			from financialtransaction 
			inner join currency on currency.currencyid = financialtransaction.currencyid
			inner join financesubcategory on financesubcategory.financesubcategoryid = financialtransaction.financesubcategoryid
			where transactiondate >= '$startdate' and transactiondate <= '$enddate' 
			and financialtransaction.currencyid = '$currencyid' and financecategoryid = '2' and financialtransactionstatusid in (3,5)
			group by currencycode");
			$row55 = mysql_fetch_array($getexpenditure);
			$expenditure = $row55['Expenditure'];	
			$expendituretax = $row55['Tax Amount'] - $expenditure;	
		}
		else {
			$getincome = mysql_query("select currencycode, sum(amount) as 'Income', sum(amountincludingtax) as 'Tax Amount'  
			from financialtransaction 
			inner join currency on currency.currencyid = financialtransaction.currencyid
			inner join financesubcategory on financesubcategory.financesubcategoryid = financialtransaction.financesubcategoryid
			inner join businessunit on businessunit.businessunitid = financialtransaction.businessunitid
			where transactiondate >= '$startdate' and transactiondate <= '$enddate' and countryid = '$countryid'
			and financialtransaction.currencyid = '$currencyid' and financecategoryid = '1' and financialtransactionstatusid in (3,5)
			group by currencycode");
			$row55 = mysql_fetch_array($getincome);
			$income = $row55['Income'];	
			$incometax = $row55['Tax Amount'] - $income;	
			
			$getexpenditure = mysql_query("select currencycode, sum(amount) as 'Expenditure', sum(amountincludingtax) as 'Tax Amount'  
			from financialtransaction 
			inner join currency on currency.currencyid = financialtransaction.currencyid
			inner join financesubcategory on financesubcategory.financesubcategoryid = financialtransaction.financesubcategoryid
			inner join businessunit on businessunit.businessunitid = financialtransaction.businessunitid
			where transactiondate >= '$startdate' and transactiondate <= '$enddate'  and countryid = '$countryid'
			and financialtransaction.currencyid = '$currencyid' and financecategoryid = '2' and financialtransactionstatusid in (3,5)
			group by currencycode");
			$row55 = mysql_fetch_array($getexpenditure);
			$expenditure = $row55['Expenditure'];	
			$expendituretax = $row55['Tax Amount'] - $expenditure;	
		}
		
		$getexchange1 = mysql_query("select fxrate from taxcurrencyconversionrate 
		where taxperiodid = '$taxperiodid' and disabled = 0 and fromcurrencyid = '$currencyid' and tocurrencyid = '$taxperiodcurrencyid'");		
		$row59 = mysql_fetch_array($getexchange1);
		$exchangerate1 = $row59['fxrate'];	
		
		$getexchange2 = mysql_query("select currencytohqcurrencycurrentfxrate from currencyfxrate
		where currencyid = '$currencyid'");		
		$row60 = mysql_fetch_array($getexchange2);
		$exchangerate2 = $row60['currencytohqcurrencycurrentfxrate'];			
		
		if($exchangerate1 <> 0){
			$exchangerate = $exchangerate1;
		}
		else {
			$exchangerate = $exchangerate2;		
		}
		if($exchangerate == 0){
			$exchangerate = 1;		
		}
		
		$include = $income + $expenditure;
		$maincurrencyincometax = $incometax / $exchangerate;
		$maincurrencyexpendituretax = $expendituretax / $exchangerate;
		$totalincome = $totalincome + $maincurrencyincometax;
		$totalexpenditure = $totalexpenditure + $maincurrencyexpendituretax;
		if($include > 0){		
			echo "<tr><td>".$currencycode." ".number_format($income)."</td>";
			echo "<td>".$currencycode." ".number_format($expenditure)."</td>";
			echo "<td>".$currencycode." ".number_format($incometax)."</td>";
			echo "<td>".$currencycode." ".number_format($expendituretax)."</td>";
			echo "<td>".$exchangerate."</td>";
			echo "<td>".$taxperiodcurrencycode." ".number_format($maincurrencyincometax)."</td>";
			echo "<td>".$taxperiodcurrencycode." ".number_format($maincurrencyexpendituretax)."</td>";
		}
	}
	
	echo "<tr><td></td><td></td><td></td><td></td><td></td>";
	echo "<td><b>".$taxperiodcurrencycode." ".number_format($totalincome)."<b></td>";
	echo "<td><b>".$taxperiodcurrencycode." ".number_format($totalexpenditure)."<b></td></tr>";
	
	$taxpayable = $totalincome - $totalexpenditure;

}

if($taxapproachname == "Profit Tax"){
	echo "<thead><td><b>".$langval119."</b></td><td><b>".$langval120."</b></td>";
	echo "<td><b>".$langval124."</b></td><td><b>".$langval123."</b></td>";
	echo "<td><b>".$langval119." (".$taxperiodcurrencycode.")</b></td><td><b>".$langval120." (".$taxperiodcurrencycode.")</b></td>";
	echo "<td><b>".$langval124." (".$taxperiodcurrencycode.")</b></td>";
	echo "</thead>";

	$totalprofit = 0;
	$totalincome = 0;
	$totalexpenditure = 0;
	
	$getcurrency = mysql_query("select * from currency where disabled = 0");
	while($row44 = mysql_fetch_array($getcurrency)){
		$currencycode = $row44['currencycode'];	
		$currencyid = $row44['currencyid'];	
		
		if($allcountries == 1){
			$getincome = mysql_query("select currencycode, sum(amount) as 'Income' from financialtransaction 
			inner join currency on currency.currencyid = financialtransaction.currencyid
			inner join financesubcategory on financesubcategory.financesubcategoryid = financialtransaction.financesubcategoryid
			where transactiondate >= '$startdate' and transactiondate <= '$enddate' 
			and financialtransaction.currencyid = '$currencyid' and financecategoryid = '1' and financialtransactionstatusid in (3,5)
			group by currencycode");
			$row55 = mysql_fetch_array($getincome);
			$income = $row55['Income'];	
			
			$getexpenditure = mysql_query("select currencycode, sum(amount) as 'Expenditure' from financialtransaction 
			inner join currency on currency.currencyid = financialtransaction.currencyid
			inner join financesubcategory on financesubcategory.financesubcategoryid = financialtransaction.financesubcategoryid
			where transactiondate >= '$startdate' and transactiondate <= '$enddate' 
			and financialtransaction.currencyid = '$currencyid' and financecategoryid = '2' and financialtransactionstatusid in (3,5)
			group by currencycode");
			$row55 = mysql_fetch_array($getexpenditure);
			$expenditure = $row55['Expenditure'];	
		}
		else {
			$getincome = mysql_query("select currencycode, sum(amount) as 'Income' from financialtransaction 
			inner join currency on currency.currencyid = financialtransaction.currencyid
			inner join financesubcategory on financesubcategory.financesubcategoryid = financialtransaction.financesubcategoryid
			inner join businessunit on businessunit.businessunitid = financialtransaction.businessunitid
			where transactiondate >= '$startdate' and transactiondate <= '$enddate' and countryid = '$countryid'
			and financialtransaction.currencyid = '$currencyid' and financecategoryid = '1' and financialtransactionstatusid in (3,5)
			group by currencycode");
			$row55 = mysql_fetch_array($getincome);
			$income = $row55['Income'];	
			
			$getexpenditure = mysql_query("select currencycode, sum(amount) as 'Expenditure' from financialtransaction 
			inner join currency on currency.currencyid = financialtransaction.currencyid
			inner join financesubcategory on financesubcategory.financesubcategoryid = financialtransaction.financesubcategoryid
			inner join businessunit on businessunit.businessunitid = financialtransaction.businessunitid
			where transactiondate >= '$startdate' and transactiondate <= '$enddate'  and countryid = '$countryid'
			and financialtransaction.currencyid = '$currencyid' and financecategoryid = '2' and financialtransactionstatusid in (3,5)
			group by currencycode");
			$row55 = mysql_fetch_array($getexpenditure);
			$expenditure = $row55['Expenditure'];	
		}
		
		$getexchange1 = mysql_query("select fxrate from taxcurrencyconversionrate 
		where taxperiodid = '$taxperiodid' and disabled = 0 and fromcurrencyid = '$currencyid' and tocurrencyid = '$taxperiodcurrencyid'");		
		$row59 = mysql_fetch_array($getexchange1);
		$exchangerate1 = $row59['fxrate'];	
		
		$getexchange2 = mysql_query("select currencytohqcurrencycurrentfxrate from currencyfxrate
		where currencyid = '$currencyid'");		
		$row60 = mysql_fetch_array($getexchange2);
		$exchangerate2 = $row60['currencytohqcurrencycurrentfxrate'];			
		
		if($exchangerate1 <> 0){
			$exchangerate = $exchangerate1;
		}
		else {
			$exchangerate = $exchangerate2;		
		}
		if($exchangerate == 0){
			$exchangerate = 1;		
		}
		
		$include = $income + $expenditure;
		$profit = $income - $expenditure;
		$maincurrencyprofit = $profit / $exchangerate;
		$maincurrencyincome = $income / $exchangerate;
		$maincurrencyexpenditure = $expenditure / $exchangerate;
		$totalprofit = $totalprofit + $maincurrencyprofit;
		$totalincome = $totalincome + $maincurrencyincome;
		$totalexpenditure = $totalexpenditure + $maincurrencyexpenditure;
		if($include > 0){		
			echo "<tr><td>".$currencycode." ".number_format($income)."</td>";
			echo "<td>".$currencycode." ".number_format($expenditure)."</td>";
			echo "<td>".$currencycode." ".number_format($profit)."</td>";
			echo "<td>".$exchangerate."</td>";
			echo "<td>".$taxperiodcurrencycode." ".number_format($maincurrencyincome)."</td>";
			echo "<td>".$taxperiodcurrencycode." ".number_format($maincurrencyexpenditure)."</td>";
			echo "<td>".$taxperiodcurrencycode." ".number_format($maincurrencyprofit)."</td></tr>";
		}
	}
	
	echo "<tr><td></td><td></td><td></td><td></td>";
	echo "<td><b>".$taxperiodcurrencycode." ".number_format($totalincome)."<b></td>";
	echo "<td><b>".$taxperiodcurrencycode." ".number_format($totalexpenditure)."<b></td>";
	echo "<td><b>".$taxperiodcurrencycode." ".number_format($totalprofit)."<b></td></tr>";

	$taxpayable = $totalprofit * ($taxpercentage / 100);

}
echo "</table>";

echo "<h2>".$langval115."</h2>";
echo "<p>".$langval116."</p>";
echo "<table class='table table-bordered' style='width:40%;'>";
echo "<tr><td><b>".$langval125."</b></td><td>".$taxperiodcurrencycode." ".number_format($taxpayable)."</td></tr>";
$gettaxpaid = mysql_query("select sum(paymentamount) as 'totaltaxpaid' from taxpayment 
where taxperiodid = '$taxperiodid' and taxpaymentstatusid = '2' group by taxperiodid");
$gettaxpaidrow = mysql_fetch_array($gettaxpaid);
$totaltaxpaid = $gettaxpaidrow['totaltaxpaid'];
echo "<tr><td><b>".$langval126."</b></td><td>".$taxperiodcurrencycode." ".number_format($totaltaxpaid)."</td></tr>";
$taxremaining = $taxpayable - $totaltaxpaid;
echo "<tr><td><b>".$langval127."</b></td><td>".$taxperiodcurrencycode." ".number_format($taxremaining)."</td></tr>";
echo "</table>";
?>


