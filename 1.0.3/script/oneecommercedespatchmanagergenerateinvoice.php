<?php session_start(); ?>
<?php include('../config.php'); ?>
<?php include('../sessionconfig.php'); ?>
<?php

$selldate = isset($_GET['selldate']) ? $_GET['selldate'] : ''; 
$businessunitid = isset($_GET['businessunitid']) ? $_GET['businessunitid'] : ''; 
$storagelocationname = isset($_GET['storagelocationname']) ? $_GET['storagelocationname'] : ''; 
$storagelocationsectionname = isset($_GET['storagelocationsectionname']) ? $_GET['storagelocationsectionname'] : ''; 
$orderdatetimehour = isset($_GET['orderdatetimehour']) ? $_GET['orderdatetimehour'] : ''; 
$orderid = isset($_GET['orderid']) ? $_GET['orderid'] : ''; 

$includeurl = "../fpdf181/fpdf.php";
require($includeurl);

class PDF extends FPDF{}

//check if businessunitid field exists and user is not admin user	
$querybuilder = "select ecommerceorder.ecommerceorderid, orderdatetime, max(ecommercelisting.ecommercelistingid) as 'InvoiceNo', sellcustomerid, businessunitname, 
customername, firstname, 
lastname, fulfillmentaddress1, fulfillmentaddress2, fulfillmentaddresstowncity, fulfillmentaddressstatecounty, 
fulfillmentaddresspostzipcode, tb1.countryname as countrynamefulfillment, billingaddress1, billingaddress2, 
billingaddresstowncity, billingaddressstatecounty, billingaddresspostzipcode, tb2.countryname as countrynamebilling, sellcurrencyid, 
tb3.countryname as countrynamebu, businessunit.address1, businessunit.address2, businessunit.towncity, businessunit.countystate, 
businessunit.postzipcode
from $database.ecommerceorder 
inner join $database.ecommerceorderitem on ecommerceorderitem.ecommerceorderid = ecommerceorder.ecommerceorderid
inner join $database.ecommercelisting on ecommercelisting.ecommercelistingid = ecommerceorderitem.ecommercelistingid
inner join $database.businessunit on businessunit.businessunitid = ecommercelisting.businessunitid
inner join $database.customer on customer.customerid = ecommercelisting.sellcustomerid
inner join $database.country as tb1 on tb1.countryid = ecommercelisting.fulfillmentaddresscountryid
inner join $database.country as tb2 on tb2.countryid = ecommercelisting.billingaddresscountryid
inner join $database.country as tb3 on tb3.countryid = ecommercelisting.billingaddresscountryid
inner join $database.productstockitem on productstockitem.productstockitemid = ecommercelisting.productstockitemid
inner join $database.storagelocationsection on storagelocationsection.storagelocationsectionid = productstockitem.storagelocationsectionid
inner join $database.storagelocation on storagelocation.storagelocationid = storagelocationsection.storagelocationid
where ecommercelistingstatusid in (2,3)";
if($selldate <> '') {
	$querybuilder = $querybuilder." and ecommercelisting.selldate = '$selldate'";
}
if($businessunitid <> '') {
	$querybuilder = $querybuilder." and ecommercelisting.businessunitid = $businessunitid";
}
if($_SESSION["userbusinessunit"] <> '' && $_SESSION["superadmin"] == 0){
  	$querybuilder = $querybuilder." and ecommercelisting.businessunitid in (".$_SESSION["userbusinessunit"].")";
}
if($storagelocationname <> '') {
	$querybuilder = $querybuilder." and storagelocationname = '$storagelocationname'";
}
if($storagelocationsectionname <> '') {
	$querybuilder = $querybuilder." and storagelocationsectionname = '$storagelocationsectionname'";
}
if($orderdatetimehour <> '') {
	$querybuilder = $querybuilder." and HOUR(orderdatetime) = '$orderdatetimehour'";
}
if($orderid <> '') {
	$querybuilder = $querybuilder." and ecommerceorder.ecommerceorderid = $orderid";
}
$querybuilder = $querybuilder." group by sellcustomerid, 
businessunitname, sellcurrencyid order by sellcustomerid";
//echo $querybuilder;
$querybuilder = mysql_query($querybuilder);

$pdf = new PDF();
	
while($row22=mysql_fetch_array($querybuilder)){
	$customerid = $row22['sellcustomerid'];
	$invoicenumber = $row22['InvoiceNo'];
	$businessunit = $row22['businessunitname'];
	$customername = $row22['customername'];
	$salt = "h3f8s9en20vj3";
	$customername = openssl_decrypt($customername,"AES-128-ECB",$salt);
	$firstname = $row22['firstname'];
	$firstname = openssl_decrypt($firstname,"AES-128-ECB",$salt);
	$lastname = $row22['lastname'];
	$lastname = openssl_decrypt($lastname,"AES-128-ECB",$salt);
	
	//echo $customerid."<br/>";
	$invoicedate = date("d-M-Y");
	$getconfig = mysql_query("select * from $database.ecommerceinvoicegenerationconfig
	where disabled = 0 
	order by ecommerceinvoicegenerationconfigid desc");
	$getconfigrow = mysql_fetch_array($getconfig);
	$businessname = $getconfigrow['businessname'];
	if (strpos($businessname, '{businessunitname}') !== false) {
		$businessname = str_replace("{businessunitname}", $businessunit, $businessname);
	}
	$returnssentence = $getconfigrow['invoicereturnsaddress'];
	if($returnssentence <> ''){
		if(strpos($returnssentence, '{businessunitaddress}') !== false) {
			$address1 = $row22['address1'];
			$address2 = $row22['address2'];
			$towncity = $row22['towncity'];
			$countystate = $row22['countystate'];
			$postzipcode = $row22['postzipcode'];
			$countrynamebu = $row22['countrynamebu'];
			$businessunitaddress = '';
			if($address1 <> ''){
				$businessunitaddress = $businessunitaddress.", ".$address1;
			}
			if($address2 <> ''){
				$businessunitaddress = $businessunitaddress.", ".$address2;
			}
			if($towncity <> ''){
				$businessunitaddress = $businessunitaddress.", ".$towncity;
			}
			if($countystate <> ''){
				$businessunitaddress = $businessunitaddress.", ".$countystate;
			}
			if($postzipcode <> ''){
				$businessunitaddress = $businessunitaddress.", ".$postzipcode;
			}
			if($countrynamebu <> ''){
				$businessunitaddress = $businessunitaddress.", ".$countrynamebu;
			}
			if (0 === strpos($businessunitaddress, ', ')) {
			   $businessunitaddress = substr($businessunitaddress, 2);
			}
			$returnssentence = str_replace("{businessunitaddress}", $businessunitaddress, $returnssentence);	
		}
	}
	$invoiceincludebillingaddress = $getconfigrow['invoiceincludebillingaddress'];
	$invoiceincludefulfillmentaddress = $getconfigrow['invoiceincludefulfillmentaddress'];
	$invoiceopeningparagraph = $getconfigrow['invoiceopeningparagraph'];
	$invoiceclosingparagraph = $getconfigrow['invoiceclosingparagraph'];
	
	if($invoiceincludefulfillmentaddress == 1){
		$fulfillmentaddress1 = $row22['fulfillmentaddress1'];
		$fulfillmentaddress2 = $row22['fulfillmentaddress2'];
		$fulfillmentaddresstowncity = $row22['fulfillmentaddresstowncity'];
		$fulfillmentaddressstatecounty = $row22['fulfillmentaddressstatecounty'];
		$fulfillmentaddresspostzipcode = $row22['fulfillmentaddresspostzipcode'];
		$fulfillmentaddresscountryname = $row22['countrynamefulfillment'];
	}
	if($invoiceincludebillingaddress == 1){
		$billingaddress1 = $row22['billingaddress1'];
		$billingaddress2 = $row22['billingaddress2'];
		$billingaddresstowncity = $row22['billingaddresstowncity'];
		$billingaddressstatecounty = $row22['billingaddressstatecounty'];
		$billingaddresspostzipcode = $row22['billingaddresspostzipcode'];
		$billingaddresscountryname = $row22['countrynamebilling'];
	}
	
	// Instantiation of inherited class
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->SetFont('Arial','B',13);
	$pdf->Cell(75);
   	$pdf->Cell(40,10,'Invoice - '.$businessname,0,0,'C');
   	$pdf->Ln(15);
	$pdf->SetFont('Arial','',10);
	$pdf->MultiCell(0,5,'Invoice Number: '.$invoicenumber,0,1);
	$pdf->MultiCell(0,5,'Invoice Date: '.$invoicedate,0,1);
	$pdf->MultiCell(0,5,'',0,1);
	$pdf->MultiCell(0,5,$firstname.' '.$lastname,0,1);
	if($invoiceincludefulfillmentaddress == 1){
		if($fulfillmentaddress1 <> ''){
			$pdf->MultiCell(0,5,$fulfillmentaddress1,0,1);
		}
		if($fulfillmentaddress2 <> ''){
			$pdf->MultiCell(0,5,$fulfillmentaddress2,0,1);
		}
		if($fulfillmentaddresstowncity <> ''){
			$pdf->MultiCell(0,5,$fulfillmentaddresstowncity,0,1);
		}
		if($fulfillmentaddressstatecounty <> ''){
			$pdf->MultiCell(0,5,$fulfillmentaddressstatecounty,0,1);
		}
		if($fulfillmentaddresspostzipcode <> ''){
			$pdf->MultiCell(0,5,$fulfillmentaddresspostzipcode,0,1);
		}
		if($fulfillmentaddresscountryname <> ''){
			$pdf->MultiCell(0,5,$fulfillmentaddresscountryname,0,1);
		}
		$pdf->Ln(5);
	}
	if($invoiceincludebillingaddress == 1){
		$pdf->MultiCell(0,5,'Billing Address: ',0,1);
		if($billingaddress1 <> ''){
			$pdf->MultiCell(0,5,$billingaddress1,0,1);
		}
		if($billingaddress2 <> ''){
			$pdf->MultiCell(0,5,$billingaddress2,0,1);
		}
		if($billingaddresstowncity <> ''){
			$pdf->MultiCell(0,5,$billingaddresstowncity,0,1);
		}
		if($fulfillmentaddressstatecounty <> ''){
			$pdf->MultiCell(0,5,$fulfillmentaddressstatecounty,0,1);
		}
		if($billingaddresspostzipcode <> ''){
			$pdf->MultiCell(0,5,$billingaddresspostzipcode,0,1);
		}
		if($billingaddresscountryname <> ''){
			$pdf->MultiCell(0,5,$billingaddresscountryname,0,1);
		}
	}
	$pdf->Ln(5);
	$pdf->SetFont('Arial','',10);
	$pdf->MultiCell(0,5,$invoiceopeningparagraph,0,1);
	$pdf->Ln(5);
	$pdf->SetFont('Arial','U',12);
	$pdf->Cell(0,10,'Invoice Breakdown',0,1);
	$pdf->SetFont('Arial','B',10);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$width = 130;
	$pdf->MultiCell($width, 6, 'Item', 1, 'L', FALSE);
	$pdf->SetXY($x + $width, $y);
	$pdf->Cell(40,6, 'Cost', 1, 0, "l");	
	$pdf->Ln(6);
	
	$getproductsales = mysql_query("select * from $database.ecommercelisting 
	inner join $database.productstockitem on productstockitem.productstockitemid = ecommercelisting.productstockitemid
	inner join $database.product on product.productid = productstockitem.productid
	left join $database.currency on currency.currencyid = productstockitem.sellcurrencyid
	where ecommercelisting.selldate = '$selldate' and ecommercelisting.sellcustomerid = '$customerid' 
	and ecommercelisting.ecommercelistingstatusid in (2,3)");
	$totalcost = 0;
	$totalcosttax = 0;
	$totalpostagecost = 0;
	$totalpostagecosttax = 0;
	while($row66=mysql_fetch_array($getproductsales)){
		$currencycode = $row66['currencycode'];
		$productdetail = $row66['productname']." - ".$row66['productproducer'];
		$productcost = $row66['sellprice'];
		$productcosttax = $row66['selltaxprice'];
		$postagecost = $row66['shippingprice'];
		$postagecosttax = $row66['shippingtax'];
		$totalcost = $totalcost + $productcost;
		$totalcosttax = $totalcosttax + $productcosttax;
		$totalpostagecost = $totalpostagecost + $postagecost;
		$totalpostagecosttax = $totalpostagecosttax + $postagecosttax;
		$pdf->SetFont('Arial','',10);
		$y = $pdf->GetY();
		$x = $pdf->GetX();
		$width = 130;
		$pdf->MultiCell($width, 6, $productdetail, 1, 'L', FALSE);
		$pdf->SetXY($x + $width, $y);
		$pdf->Cell(40,6, $currencycode.' ' .$productcost, 1, 0, "l");	
		$pdf->Ln(6);
	}
	if($totalpostagecost <> 0){
		$pdf->SetFont('Arial','',10);
		$y = $pdf->GetY();
		$x = $pdf->GetX();
		$width = 130;
		$pdf->MultiCell($width, 6, 'Postage and Packaging', 1, 'L', FALSE);
		$pdf->SetXY($x + $width, $y);
		$pdf->Cell(40,6, $currencycode.' ' .$totalpostagecost, 1, 0, "l");	
		$pdf->Ln(6);		
	}
	$grandtotal = $totalpostagecost + $totalcost;
	$grandtotaltax = $totalpostagecosttax + $totalcosttax;
	$grandtotaltotal = $grandtotal + $grandtotaltax;
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$width = 130;
	$pdf->SetFont('Arial','B',10);
	$pdf->MultiCell($width, 6, 'Grand Total', 1, 'L', FALSE);
	$pdf->SetXY($x + $width, $y);
	$pdf->Cell(40,6, $currencycode.' ' .$grandtotal, 1, 0, "l");	
	$pdf->Ln(6);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$width = 130;
	$pdf->MultiCell($width, 6, 'Tax Due', 1, 'L', FALSE);
	$pdf->SetXY($x + $width, $y);
	$pdf->Cell(40,6, $currencycode.' ' .$grandtotaltax, 1, 0, "l");	
	$pdf->Ln(6);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$width = 130;
	$pdf->MultiCell($width, 6, 'Grand Total Including Tax / Total Due', 1, 'L', FALSE);
	$pdf->SetXY($x + $width, $y);
	$pdf->Cell(40,6, $currencycode.' ' .$grandtotaltotal, 1, 0, "l");	
	$pdf->Ln(12);
	$pdf->SetFont('Arial','',10);
	$pdf->MultiCell(0,5,$invoiceclosingparagraph,0,1);
	if($returnssentence <> ''){
		$pdf->Ln(6);
		$pdf->SetFont('Arial','',10);
		$pdf->MultiCell(0,5,$returnssentence,0,1);
	}
}

//uncomment this, if you want to see an example in the browser for testing. However, comment it out once you are finished otherwise the whilst loop will not run.
$pdf->Output();

?>				
