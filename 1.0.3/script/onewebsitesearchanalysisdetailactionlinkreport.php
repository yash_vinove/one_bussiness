<?php


//LANGUAGE COLLECTION SECTION
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (211,212,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}

//update status as in progress
$updatestatus = mysql_query("update $database.websitesearchanalysistask set websitesearchtaskstatusid = '2', emailskipped = 0, 
completedby = 0, completeddate = '0000-00-00' 
where websitesearchanalysisid = '$rowid' and websitesearchtaskid = '$websitesearchtaskid'");

//check latest status of the records
//get TrafficHistory
class TrafficHistory {
    protected static $ServiceHost      = 'awis.amazonaws.com';
    public function TrafficHistory($accessKeyId, $secretAccessKey, $site, $offset) {
        $this->accessKeyId = $accessKeyId;
        $this->secretAccessKey = $secretAccessKey;
        $this->site = $site;
        $this->offset = $offset;
    }
    public function getTrafficHistory() {
        $queryParams = $this->buildQueryParams();
        $sig = $this->generateSignature($queryParams);
        $url = 'https://awis.amazonaws.com/?' . $queryParams . 
            '&Signature=' . $sig;
        $ret = self::makeRequest($url);
        //echo "\nResults for " . $this->site .":\n\n";
        $site = $this->site;
        self::parseResponse($ret,$site);
    }
	protected static function getTimestamp() {
        return gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time()); 
    }
    protected function buildQueryParams() {
    		$date = date ("Y-m-d");
    		$date = date ("Ymd", strtotime("-20 days", strtotime($date)));
    		$params = array(
            'Action'            => 'SitesLinkingIn',
            'ResponseGroup'     => 'SitesLinkingIn',
            'AWSAccessKeyId'    => $this->accessKeyId,
            'Timestamp'         => self::getTimestamp(),
            'Count'             => '20',
            'Start'             => $this->offset,
            'SignatureVersion'  => '2',
            'SignatureMethod'   => 'HmacSHA256',
            'Url'               => $this->site
        );
        ksort($params);
        $keyvalue = array();
        foreach($params as $k => $v) {
            $keyvalue[] = $k . '=' . rawurlencode($v);
        }
        return implode('&',$keyvalue);
    }
    protected static function makeRequest($url) {
        //echo "$url\n\n";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 6);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
  		  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
        if (strpos($result, 'Fatal error') !== false) {
    			echo 'text';
			}
    }
    asnc public static function parseResponse($response,$site) {
    	global $offset;
    	try {
        $xml = new SimpleXMLElement($response,null,false,
                                    'http://awis.amazonaws.com/doc/2005-07-11');
        if($xml->count() && $xml->Response->SitesLinkingInResult->Alexa->SitesLinkingIn->Site->count()) {
            $info = $xml->Response->SitesLinkingInResult->Alexa->SitesLinkingIn;
            $nice_array = array(
            );
            $numrec = $xml->Response->SitesLinkingInResult->Alexa->SitesLinkingIn->Site->count();
            //echo $numrec;
            if($numrec < 20){
					$offset = 10000;            
            }
            	for ($i=0; $i<$numrec; $i++){
            		array_push($nice_array, $info->Site[$i]->Title);
            		array_push($nice_array, $info->Site[$i]->Url);     	
            	}
            	
            //echo "<br/><br/>";
	        //echo "<table><thead><td>Title</td><td>Url</td></thead>";
	        $z = 1;
	        global $database;
	        global $rowid;
	        $date = date ("Y-m-d");
	        $site1 = "https://".$site;
	        $site2 = "http://".$site; 
	        $shorturl = '';     
	        $longurl = '';     
	        foreach($nice_array as $k => $v) {
	        		if($z == 1){
	            		$shorturl = $v;
	            	}
	            	if($z == 2){
	            		$longurl = $v;
	        			$z = $z +1;
	        		} 
	        		if($z == 3){
	            		//check link is already in library
						$checklink = mysql_query("select * from $database.websitesearchlink 
						where websitesearchanalysisid = '$rowid' and competitorurl = '' and linkurl = '$shorturl'");        			
	        			if(mysql_num_rows($checklink) == 0){
	        				//add link, if not in library
	        				if($longurl <> ""){
	        					$shorturl1 = $shorturl;
	        					$shorturl2 = "http://www.".$shorturl;
	        					$shorturl3 = "https://www.".$shorturl;
	        					$shorturl4 = "http://".$shorturl;
	        					$shorturl5 = "https://".$shorturl;
								$getlibraryurl = mysql_query("select * from $database.websitesearchlinklibrary
	        					where linkurl = '$shorturl1' or linkurl = '$shorturl2' or linkurl = '$shorturl3' or linkurl = '$shorturl4' or linkurl = '$shorturl5'");
		        				$getlibraryurlrow = mysql_fetch_array($getlibraryurl);
		        				$websitesearchlinklibraryid = $getlibraryurlrow['websitesearchlinklibraryid'];
		        				$query = mysql_query("insert into $database.websitesearchlink (websitesearchlinkname, websitesearchanalysisid, linkurl, 
		       				websitesearchlinklibraryid, websitesearchlinkstatusid, datecreated) values ('$longurl', '$rowid', '$shorturl', 
		       				'$websitesearchlinklibraryid', '1', '$date')");
	       				}
	        			}
	        			else {
	        				$checklinkrow = mysql_fetch_array($checklink);
							$websitesearchlinkstatusid = $checklinkrow['websitesearchlinkstatusid'];	        					
							$websitesearchlinkid = $checklinkrow['websitesearchlinkid'];	 
							//echo $websitesearchlinkid;       					
	        				if($websitesearchlinkstatusid == '2' || $websitesearchlinkstatusid == '3'){
	        					$updaterecord = mysql_query("update $database.websitesearchlink set websitesearchlinkstatusid = '4' 
	        					where websitesearchlinkid = '$websitesearchlinkid'");
	        				}	        			
	        			}
	        			$z = 0;
	        		}
	        		$z = $z+1;
	        }
        
        }
         } catch (Exception $e) {
			    echo 'Caught exception: ',  $e->getMessage(), "<br/>";    
			}
        
        //echo "</table>";
        //echo "<br/><br/>";
    }
    protected function generateSignature($url) {
        $sign = "GET\n" . strtolower(self::$ServiceHost) . "\n/\n". $url;
        //echo "String to sign: \n" . $sign . "\n";
        $sig = base64_encode(hash_hmac('sha256', $sign, $this->secretAccessKey, true));
        //echo "\nSignature: " . $sig ."\n";
        return rawurlencode($sig);
    }
}

$accessKeyId = "AKIAJXEU5GQLLAUGN25Q";
$secretAccessKey = "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";
//$site = "http://www.yahoo.com";
//$site = "http://stevedunnuk.com";
//$site = "http://www.uluru.com";
$getwebsite = mysql_query("select * from $database.websitesearchanalysis 
inner join $database.website on website.websiteid = websitesearchanalysis.onewebsiteid
where websitesearchanalysisid = '$rowid'");
$getwebsiterow = mysql_fetch_array($getwebsite);
$site = $getwebsiterow['websiteurl'];
$slash = substr($site, -1);
if($slash == "/"){
	$site = substr(trim($site), 0, -1);
}
$site = str_replace('https://', '', $site);	
$site = str_replace('http://', '', $site);	
$site = str_replace('www.', '', $site);	

$offset = 0;
while($offset <= 500){
	$traffichistory = new TrafficHistory($accessKeyId, $secretAccessKey, $site, $offset);
	$traffichistory->getTrafficHistory();
	$offset = $offset + 20;
}







//generate report
$includeurl = "fpdf181/fpdf.php";
require($includeurl);

class PDF extends FPDF
{
	// Page header
	function Header()
	{
	    // Arial bold 15
	    $this->SetFont('Arial','B',13);
	}	
	// Page footer
	function Footer()
	{
	    // Position at 1.5 cm from bottom
	    $this->SetY(-15);
	    // Arial italic 8
	    $this->SetFont('Arial','I',8);
	    // Page number
	    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	}
}

//date created
$date = date("d-M-Y");
$date2 = date("Y-m-d");

$getrecord = mysql_query("select * from $database.websitesearchanalysis 
inner join $database.website on website.websiteid = websitesearchanalysis.onewebsiteid
where websitesearchanalysisid = '$rowid'");
$getrecordrow = mysql_fetch_array($getrecord);
$websitesearchanalysisid = $getrecordrow['websitesearchanalysisid'];
$websitename = $getrecordrow['websitename'];
$websiteurl = $getrecordrow['websiteurl'];

//Instantiation of inherited class
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','B',13);
$pdf->Cell(75);
$pdf->Cell(40,10,$langval241.$websitename,0,0,'C');
$pdf->Ln(15);
$pdf->SetFont('Arial','',10);
$pdf->MultiCell(0,5,$langval242,0,1);
$pdf->Ln(5);
$pdf->SetFont('Arial','',10);
$pdf->MultiCell(0,5,$langval211.$date,0,1);
$pdf->MultiCell(0,5,$langval212.$websiteurl,0,1);
$pdf->Ln(6);

//SUMMARY
$pdf->SetFont('Arial','U',12);
$pdf->Cell(0,10,$langval243,0,1);
$pdf->SetFont('Arial','',10);
$pdf->MultiCell(0,5,$langval244,0,1);
$pdf->Ln(6);
$y = $pdf->GetY();
$x = $pdf->GetX();
$pdf->SetFont('Arial','B',10);
$pdf->Cell(120, 6, $langval245, 1, 'L', FALSE);
$y = $pdf->GetY();
$x = $pdf->GetX();
$pdf->SetXY($x, $y);
$pdf->Cell(40,6, $langval246, 1, 0, "l");	
$pdf->Ln(6);
$checklink2 = mysql_query("select * from $database.websitesearchlink 
where websitesearchanalysisid = '$rowid' and competitorurl = '' and websitesearchlinkstatusid = '1'
order by websitesearchlinkid asc");   
$numlinks5 = mysql_num_rows($checklink2); 
$pdf->SetFont('Arial','',10);
$y = $pdf->GetY();
$x = $pdf->GetX();
$pdf->SetXY($x, $y);
$pdf->Cell(120, 6, $langval247, 1, 'L', FALSE);
$y = $pdf->GetY();
$x = $pdf->GetX();
$pdf->SetXY($x, $y);
$pdf->Cell(40,6, $numlinks5, 1, 0, "l");	
$pdf->Ln(6);  			       			
$checklink2 = mysql_query("select competitorurl, count(websitesearchlinkid) as 'numlinkcomp' from $database.websitesearchlink 
where websitesearchanalysisid = '$rowid' and competitorurl <> '' and websitesearchlinkstatusid = '1'
group by competitorurl order by numlinkcomp desc");   
while($checklink2row = mysql_fetch_array($checklink2)){
	$numlinkcomp = $checklink2row['numlinkcomp'];
	$competitorurl = $checklink2row['competitorurl'];
	$pdf->SetFont('Arial','',10);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->SetXY($x, $y);
	$pdf->Cell(120, 6, $competitorurl, 1, 'L', FALSE);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->SetXY($x, $y);
	$pdf->Cell(40,6, $numlinkcomp, 1, 0, "l");	
	$pdf->Ln(6);
}
$pdf->Ln(6); 


//NEWLY SUBMITTED LINKS
$pdf->SetFont('Arial','U',12);
$pdf->Cell(0,10,$langval248,0,1);
$pdf->SetFont('Arial','',10);
$pdf->MultiCell(0,5,$langval249,0,1);
$pdf->Ln(6);
$pdf->SetFont('Arial','B',10);
$y = $pdf->GetY();
$x = $pdf->GetX();
$pdf->SetXY($x, $y);
$pdf->Cell(60, 6, $langval250, 1, 'L', FALSE);
$y = $pdf->GetY();
$x = $pdf->GetX();
$pdf->SetXY($x, $y);
$pdf->Cell(40,6, $langval251, 1, 0, "l");	
$pdf->Ln(6);  			       					       			
$checklink2 = mysql_query("select websitesearchlinklibrarytypename, count(websitesearchlinkid) as 'countoflinks' from $database.websitesearchlink 
inner join $masterdatabase.websitesearchlinklibrarytype on websitesearchlinklibrarytype.websitesearchlinklibrarytypeid = websitesearchlink.websitesearchlinklibrarytypeid
where websitesearchanalysisid = '$rowid' and competitorurl = '' and websitesearchlinkstatusid in (3,4)
group by websitesearchlink.websitesearchlinklibrarytypeid desc");   
while($checklink2row = mysql_fetch_array($checklink2)){
	$websitesearchlinklibrarytypename = $checklink2row['websitesearchlinklibrarytypename'];
	$countoflinks = $checklink2row['countoflinks'];
	$pdf->SetFont('Arial','',10);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->SetXY($x, $y);
	$pdf->Cell(60, 6, $websitesearchlinklibrarytypename, 1, 'L', FALSE);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$pdf->SetXY($x, $y);
	$pdf->Cell(40,6, $countoflinks, 1, 0, "l");	
	$pdf->Ln(6);
}
$pdf->Ln(6);
$pdf->SetFont('Arial','',10);
$pdf->MultiCell(0,5,$langval252,0,1);
$pdf->Ln(6);
$checklink2 = mysql_query("select * from $database.websitesearchlink 
inner join $masterdatabase.websitesearchlinklibrarytype on websitesearchlinklibrarytype.websitesearchlinklibrarytypeid = websitesearchlink.websitesearchlinklibrarytypeid
inner join $masterdatabase.websitesearchlinkstatus on websitesearchlinkstatus.websitesearchlinkstatusid = websitesearchlink.websitesearchlinkstatusid
where websitesearchanalysisid = '$rowid' and competitorurl = '' and websitesearchlink.websitesearchlinkstatusid in (3,4)
order by websitesearchlinklibrarytypename asc");   
$savedtype = ''; 			       			
while($checklinkrow2 = mysql_fetch_array($checklink2)){
	$type = $checklinkrow2['websitesearchlinklibrarytypename'];
	if($savedtype <> $type){
		$savedtype = $type;
		$pdf->SetFont('Arial','B',10);
		$pdf->MultiCell(0,10,$type,0,1);	
		$l = 1;
	}		
	$status = $checklinkrow2['websitesearchlinkstatusname'];
	$linkurl = $checklinkrow2['linkurl'];
	$string = $l.") ".$status." - ".$linkurl;
	$l = $l + 1;
	$pdf->SetFont('Arial','',10);
	$pdf->MultiCell(0,5,$string,0,1);
}
$pdf->Ln(6);

//YOUR WEBSITE LINKS
$pdf->SetFont('Arial','U',12);
$pdf->MultiCell(0,10,$langval253,0,1);
$pdf->SetFont('Arial','',10);
$pdf->MultiCell(0,5,$langval254,0,1);
$pdf->Ln(6);
$checklink2 = mysql_query("select * from $database.websitesearchlink 
where websitesearchanalysisid = '$rowid' and competitorurl = '' and websitesearchlinkstatusid = '1'
order by websitesearchlinkid asc");   
$l = 1;     			       			
while($checklinkrow2 = mysql_fetch_array($checklink2)){
	$url1 = $checklinkrow2['websitesearchlinkname'];
	$url2 = $checklinkrow2['linkurl'];
	if($l == 1){
		$websitesearchlinkid = $checklinkrow2['websitesearchlinkid'];
	}
	$string = $l.") ".$url2." - ".$url1;
	$l = $l + 1;
	$pdf->MultiCell(0,5,$string,0,1);
}
$pdf->Ln(6);
	
//COMPETITORS LINKS
$pdf->SetFont('Arial','U',12);
$pdf->MultiCell(0,10,$langval255,0,1);
$pdf->SetFont('Arial','',10);
$pdf->MultiCell(0,5,$langval256,0,1);
$checklink2 = mysql_query("select * from $database.websitesearchlink 
where websitesearchanalysisid = '$rowid' and competitorurl <> '' and websitesearchlinkstatusid = '1'
order by competitorurl asc, websitesearchlinkid asc");   
$l = 1;    
$savedcompetitorurl = ''; 			       			
while($checklinkrow2 = mysql_fetch_array($checklink2)){
	$competitorurl = $checklinkrow2['competitorurl'];
	if($savedcompetitorurl <> $competitorurl){
		$savedcompetitorurl = $competitorurl;
		$pdf->SetFont('Arial','B',10);
		$pdf->MultiCell(0,10,$competitorurl,0,1);	
		$l = 1;
	}		
	$url1 = $checklinkrow2['websitesearchlinkname'];
	$url2 = $checklinkrow2['linkurl'];
	$string = $l.") ".$url2." - ".$url1;
	$l = $l + 1;
	$pdf->SetFont('Arial','',10);
	$pdf->MultiCell(0,5,$string,0,1);
}


//uncomment this, if you want to see an example in the browser for testing. However, comment it out once you are finished otherwise the whilst loop will not run.
//$pdf->Output();
	


//check if onewebsite storage folder exists, if not, then create
$filedir = '../documents/'. $database."/sf/websitesearchlink/";
if(file_exists($filedir)){
}
else {
	$file_directory = "../documents/".$database."/sf/websitesearchlink/";
	mkdir($file_directory);
}

$filename="../documents/".$database."/sf/websitesearchlink/".$websitesearchlinkid." - Website Search Submitted Links Report.pdf";
$pdf->Output($filename,'F');


//create document record in structurefunctiondocument
$recordname = $websitesearchlinkid." - Website Search Submitted Links Report.pdf";
$docurl = "../documents/".$database."/sf/websitesearchlink/".$recordname;
$insertdocrecord = mysql_query("insert into $database.structurefunctiondocument (structurefunctiondocumentname, datecreated, 
documenturl, doctypename, structurefunctionid, rowid) values ('$recordname', '$date2', '$docurl', 
'Final Links Report', '235', '$websitesearchlinkid')");
	



?>