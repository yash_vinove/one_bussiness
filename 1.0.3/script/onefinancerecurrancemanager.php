<?php session_start(); ?>
<?php include('../config.php'); ?>
<?php include('../sessionconfig.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	
 	<title>Configure Recurrance</title>

 	<!-- Bootstrap -->
 	<link href="bootstrap-3.3.1/dist/css/bootstrap.css" rel="stylesheet">

 	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
 	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">   
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
	<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script type="text/javascript">
    function redirectMe (sel) {
        var url = sel[sel.selectedIndex].value;
        window.location = url;
    }
    </script>
    <link rel='stylesheet' type='text/css' href='../style.php' />
    <SCRIPT type="text/javascript">
	function SwitchHiddenDiv(){
	switch (document.getElementById("recurtype").options[document.getElementById("recurtype").selectedIndex].value){
	    case 'None':
		document.getElementById('Weekly').style.display="none";
		document.getElementById('Monthly').style.display="none";
	    document.getElementById('RecurranceRange').style.display="none";
	    break;
	    case 'Weekly':
		document.getElementById('Weekly').style.display="block";
		document.getElementById('Monthly').style.display="none";
	    document.getElementById('RecurranceRange').style.display="block";
	    break;
	    case 'Monthly':
		document.getElementById('Weekly').style.display="none";
		document.getElementById('Monthly').style.display="block";
		document.getElementById('RecurranceRange').style.display="block";
	    break;
	    default:
		alert("Unknown type");
	  }
	}
	</SCRIPT>
</head>
  
<body class='body'>
<?php include_once ('../headerthree.php');	?>
	
<?php
$submit = isset($_POST['submit']) ? $_POST['submit'] : '';
$rowid = isset($_GET['rowid']) ? strip_tags($_GET['rowid']) : '';
$error = "";

//LANGUAGE COLLECTION SECTION
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (5)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}
	
?>
	 
<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding:10px;">
	<!--search results header bar for desktop-->
	<div class='bodyheader'>
		<h3>Configure Recurrance</h3>
	</div>
<?php 
if(!$submit) {
	//get recurrance id 
	$gettransaction = mysql_query("select * from financialtransaction where financialtransactionid = '$rowid'");
	$gettransactionrow = mysql_fetch_array($gettransaction);
	$recurfinancialtransactionid = $gettransactionrow['recurfinancialtransactionid'];
	
	//recurrance details
  	$checkrecurrance = mysql_query("select * from financialtransactionrecur 
  	where financialtransactionrecurid = '$recurfinancialtransactionid'");
   	$recurrancedetail = mysql_fetch_array($checkrecurrance);
   	$recurtype = $recurrancedetail['recurtype'];

    //week recurrance details
    $weeknumberofweeks = $recurrancedetail['weeknumberofweeks'];
    $weeksunday = $recurrancedetail['weeksunday'];
    $weekmonday = $recurrancedetail['weekmonday'];
    $weektuesday = $recurrancedetail['weektuesday'];
    $weekwednesday = $recurrancedetail['weekwednesday'];
    $weekthursday = $recurrancedetail['weekthursday'];
    $weekfriday = $recurrancedetail['weekfriday'];
    $weeksaturday = $recurrancedetail['weeksaturday'];
    //month recurrance details
    $monthdaynumber = $recurrancedetail['monthdaynumber'];
    $monthfrequencynumber = $recurrancedetail['monthfrequencynumber'];
    $monthoptions = $recurrancedetail['monthoptions'];
    $monthweeknumber = $recurrancedetail['monthweeknumber'];
    $monthweekday = $recurrancedetail['monthweekday'];
    $monthfrequencynumber2 = $recurrancedetail['monthfrequencynumber2'];
    //recur pattern
    $recurpatternoptions = $recurrancedetail['recurpatternoptions'];
    $recurpatternnumberofoccurences = $recurrancedetail['recurpatternnumberofoccurences'];;
    $recurpatternenddate = $recurrancedetail['recurpatternenddate'];

    //split the renewaldate into month, day, year
    if (isset($recurpatternenddate)) {
    $parts1 = explode('-', $recurpatternenddate);
    $rendyear = $parts1[0];
    $rendmonth = $parts1[1];
    $rendday = $parts1[2];
    }
    else {
      $rendyear = 0;
      $rendmonth = 0;
      $rendday = 0;
    }

    $recurpatterndateenddate = $rendday;
    $recurpatternmonthenddate = $rendmonth;
    $recurpatternyearenddate = $rendyear;
}
else {
  	$recurtype = isset($_POST['recurtype']) ? $_POST['recurtype'] : '';
  	//week recurrance details
  	$weeknumberofweeks = isset($_POST['weeknumberofweeks']) ? $_POST['weeknumberofweeks'] : '';
  	$weeksunday = isset($_POST['weeksunday']) ? $_POST['weeksunday'] : '';
  	$weekmonday = isset($_POST['weekmonday']) ? $_POST['weekmonday'] : '';
  	$weektuesday = isset($_POST['weektuesday']) ? $_POST['weektuesday'] : '';
  	$weekwednesday = isset($_POST['weekwednesday']) ? $_POST['weekwednesday'] : '';
  	$weekthursday = isset($_POST['weekthursday']) ? $_POST['weekthursday'] : '';
  	$weekfriday = isset($_POST['weekfriday']) ? $_POST['weekfriday'] : '';
  	$weeksaturday = isset($_POST['weeksaturday']) ? $_POST['weeksaturday'] : '';
  	//month recurrance details
  	$monthdaynumber = isset($_POST['monthdaynumber']) ? $_POST['monthdaynumber'] : '';
  	$monthfrequencynumber = isset($_POST['monthfrequencynumber']) ? $_POST['monthfrequencynumber'] : '';
  	$monthoptions = isset($_POST['monthoptions']) ? $_POST['monthoptions'] : '';
  	$monthweeknumber = isset($_POST['monthweeknumber']) ? $_POST['monthweeknumber'] : '';
  	$monthweekday = isset($_POST['monthweekday']) ? $_POST['monthweekday'] : '';
  	$monthfrequencynumber2 = isset($_POST['monthfrequencynumber2']) ? $_POST['monthfrequencynumber2'] : '';
  	//recur pattern
  	$recurpatternoptions = isset($_POST['recurpatternoptions']) ? $_POST['recurpatternoptions'] : '';
  	$recurpatternnumberofoccurences = isset($_POST['recurpatternnumberofoccurences']) ? $_POST['recurpatternnumberofoccurences'] : '';
  	$recurpatterndateenddate = isset($_POST['recurpatterndateenddate']) ? $_POST['recurpatterndateenddate'] : '';
  	$recurpatternmonthenddate = isset($_POST['recurpatternmonthenddate']) ? $_POST['recurpatternmonthenddate'] : '';
  	$recurpatternyearenddate = isset($_POST['recurpatternyearenddate']) ? $_POST['recurpatternyearenddate'] : '';
  	$recurpatternenddate = $recurpatternyearenddate.'-'.$recurpatternmonthenddate.'-'.$recurpatterndateenddate;

  	//check if recurpatternenddate is a valid date
  	$convertedrecurpatternenddate=str_replace('/','-',$recurpatternenddate);
  	if(preg_match("/^((((19|20)(([02468][048])|([13579][26]))-02-29))|((20[0-9][0-9])|(19[0-9][0-9]))-((((0[1-9])|
  	(1[0-2]))-((0[1-9])|(1\d)|(2[0-8])))|((((0[13578])|(1[02]))-31)|(((0[1,3-9])|
  	(1[0-2]))-(29|30)))))$/",$convertedrecurpatternenddate)===1) {
      $checkrecurpatternenddateisvalid = "1";
  	}
  	else {
      $checkrecurpatternenddateisvalid = "0";
  	}
}

//Recieve the post data back from the form below and update database using contents of all of the above variables.
if($submit) {
  	//set the values for the days of week tickboxes
  	if (isset($_POST['weeksunday'])) {
    	$weeksunday = '1';
  	}
  	else {
    	$weeksunday = '0';
  	}
  	if (isset($_POST['weekmonday'])) {
    	$weekmonday = '1';
  	}
  	else {
    	$weekmonday = '0';
  	}
  	if (isset($_POST['weektuesday'])) {
    	$weektuesday = '1';
  	}
  	else {
    	$weektuesday = '0';
  	}
  	if (isset($_POST['weekwednesday'])) {
    	$weekwednesday = '1';
  	}
  	else {
    	$weekwednesday = '0';
  	}
  	if (isset($_POST['weekthursday'])) {
    	$weekthursday = '1';
  	}
  	else {
    	$weekthursday = '0';
  	}
  	if (isset($_POST['weekfriday'])) {
    	$weekfriday = '1';
  	}
  	else {
    	$weekfriday = '0';
  	}
  	if (isset($_POST['weeksaturday'])) {
    	$weeksaturday = '1';
  	}
  	else {
    	$weeksaturday = '0';
  	}
  	if ($recurpatternnumberofoccurences=="") {
    	$recurpatternnumberofoccurences = '0';
  	}
  	if ($monthdaynumber=="") {
    	$monthdaynumber = '0';
  	}

  	//carry out validation algorithm against the options for recurring tasks - output is pass or fail
  	$recurrencevalidationcheck = "";
  	$recurrencerangeofrecurrence = "";
  	if ($recurtype == "None") {
 		$recurrencevalidationcheck = "Pass None";
  	}
  	else {
 		if ($recurtype == "Weekly") {
			if ($weeknumberofweeks >= 1) {
				if ($weeksunday == 1 || $weekmonday == 1 || $weektuesday == 1 || $weekwednesday == 1 || $weekthursday == 1 || $weekfriday == 1 || $weeksaturday == 1) {
       			$recurrencevalidationcheck = "Pass Weekly";
       			$recurrencerangeofrecurrence = 1;
     			}
     			else {
       			$error = "Your recurrance details are incorrect - please tick at least one day of the week";
     			}
   			}
   			else {
     			$error = "Your recurrance details are incorrect - please set a number of weeks for weekly recurrance";
   			}
 		}
 		if ($recurtype == "Monthly") {
   			if ($monthoptions == 1) {
     			if ($monthdaynumber >= 1 && $monthfrequencynumber >=1) {
       			$recurrencevalidationcheck = "Pass Monthly";
       			$recurrencerangeofrecurrence = 1;
       			$monthfrequencynumber2 = 0;
     			}
     			else {
       			$error = "Your recurrance details are incorrect - please select a day of the week and a month frequency";
     			}
   			}
   			else {
     			if ($monthoptions == 2) {
       			if ($monthweeknumber <> "Select" && $monthweekday <> "Select" && $monthfrequencynumber2 >=1) {
         				$recurrencevalidationcheck = "Pass Monthly";
         				$recurrencerangeofrecurrence = 1;
         				$monthfrequencynumber = 0;
       			}
       			else {
         				$error = "Your recurrance details are incorrect - please select a week frequency, a day of the week and a month frequency";
       			}
     			}
     			else {
       			$error = "Your recurrance details are incorrect - please select one of the options from the monthly recurrance pattern";
     			}
   			}
 		}
  	}

  	//carry out validation algorithm against the range of recurrance - output is pass or fail
  	$recurrencerangeofrecurrence2 = "";
  	if ($recurrencerangeofrecurrence == 1) {
 		if ($recurpatternoptions == 1) {
   			if ($recurpatternnumberofoccurences >= 1) {
     			$recurrencerangeofrecurrence2 = "Pass RecurPattern";
   			}
   			else {
     			$error = "Your range of recurrance details are incorrect - please enter a number of occurences";
   			}
 		}
 		else {
   			if ($recurpatternoptions == 2) {
     			if (($checkrecurpatternenddateisvalid == "1")&&($recurpatternyearenddate != '' || $recurpatternmonthenddate != '' || $recurpatterndateenddate != '')&&($recurpatternyearenddate != '' && $recurpatternmonthenddate != '' && $recurpatterndateenddate != '')) {
       			$recurrencerangeofrecurrence2 = "Pass RecurPattern";
     			}
     			else {
       			$error = "Your range of recurrance details are incorrect - please enter a date to end the recurrance";
     			}
   			}
   			else {
     			$error = "Your range of recurrance details are incorrect - please select one of the options from the range of recurrance";
   			}
 		}
  	}

	//check if recurpatternenddate is a valid date
	if (date('Y-m-d', strtotime($recurpatternenddate)) == $recurpatternenddate) {
		$checkrecurpatternenddateisvalid = "1";
	} 
	else {
		$checkrecurpatternenddateisvalid = "0";
	}

  	if ($recurrencerangeofrecurrence2 = "Pass RecurPattern" && ($recurrencevalidationcheck == "Pass Weekly" || $recurrencevalidationcheck == "Pass Monthly")) {
		//get recurrance id 
		$gettransaction = mysql_query("select * from financialtransaction where financialtransactionid = '$rowid'");
		$gettransactionrow = mysql_fetch_array($gettransaction);
		$recurfinancialtransactionid = $gettransactionrow['recurfinancialtransactionid'];
		
		//delete existing recurrance record
		if($recurfinancialtransactionid > 0){
			$deleterecurrance = mysql_query("delete from financialtransactionrecur 
			where financialtransactionrecurid = '$recurfinancialtransactionid'");
		}
		
		//make some alterations to recurrence data variables so that they update correctly in database
		if ($recurpatternnumberofoccurences == "") {
		  $recurpatternnumberofoccurences = 0;
		}
		if ($recurpatternenddate == "--") {
		  $recurpatternenddate = "0000-00-00";
		}
		if ($monthfrequencynumber2 == "") {
		  $monthfrequencynumber2 = 0;
		}
		if ($monthfrequencynumber == "") {
		  $monthfrequencynumber = 0;
		}
		if ($monthdaynumber == "") {
		  $monthdaynumber = 0;
		}
		if ($monthweeknumber == "Select") {
		  $monthweeknumber = "";
		}
		if ($monthweekday == "Select") {
		  $monthweekday = "";
		}
			
		//add new recurrance record
		$addrecurrancerecord = mysql_query("insert into financialtransactionrecur
      	(recurtype, weeknumberofweeks, weeksunday, weekmonday, weektuesday, weekwednesday,
      	weekthursday, weekfriday, weeksaturday, monthoptions, monthdaynumber, monthfrequencynumber,
      	monthweeknumber, monthweekday, monthfrequencynumber2, recurpatternoptions,
      	recurpatternnumberofoccurences, recurpatternenddate)
      	values ('$recurtype', '$weeknumberofweeks', '$weeksunday', '$weekmonday',
      	'$weektuesday', '$weekwednesday', '$weekthursday', 
      	'$weekfriday', '$weeksaturday', '$monthoptions', '$monthdaynumber', '$monthfrequencynumber',
      	'$monthweeknumber', '$monthweekday', '$monthfrequencynumber2', '$recurpatternoptions',
      	'$recurpatternnumberofoccurences', '$recurpatternenddate')");
      	
      	$recordid = mysql_insert_id();
      	
      	$updatefinancialtransaction = mysql_query("update financialtransaction set recurfinancialtransactionid = '$recordid' 
      	where financialtransactionid = '$rowid'");	
	} 
	if ($recurtype == "None") {
		//get recurrance id 
		$gettransaction = mysql_query("select * from financialtransaction where financialtransactionid = '$rowid'");
		$gettransactionrow = mysql_fetch_array($gettransaction);
		$recurfinancialtransactionid = $gettransactionrow['recurfinancialtransactionid'];
		
		//delete the recurrance
		$deleterecurrance = mysql_query("delete from financialtransactionrecur 
		where financialtransactionrecurid = '$recurfinancialtransactionid'");
		
		//zero out all records that have the recurrance
		$updatefinancialtransaction = mysql_query("update financialtransaction set recurfinancialtransactionid = '0'
		where recurfinancialtransactionid = '$recurfinancialtransactionid'");
	}
		
	//redirect user back to search page after update successful
   	$url = '../pagegrid.php?pagetype=financialtransaction';
   	echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">';
}
	?>
			
	<div class='bodycontent'>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<form>
			<?php
			if($error<>"") {
				echo "<br/><p class='background-warning'>".$error."</p>";
			} 
			?>
		</form>
		<?php
		echo "<p>Setup/manage your recurrance pattern below.<br/><br/></p>";
		?>
		</div>
				
	<form action='onefinancerecurrancemanager.php?rowid=<?php echo $rowid ?>' method="post" onsubmit="return submitForm();">
		<div class="col-xs-12 col-md-9 col-sm-10 col-lg-8">
		   	<div class="form-group">
				<label for="Recurrance Type" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Recurrance Type</label>
				<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
					<select class='form-control' name='recurtype' id='recurtype' onfocus="SwitchHiddenDiv();" onchange='SwitchHiddenDiv();'>
			      	<?php
			     	if ($recurtype == "None") {
			      		echo "<option value='None' selected='true'>None</option>";
			     	} 
			     	else {
			      		echo "<option value='None'>None</option>";
		        	}
		       	if ($recurtype == "Weekly") {
		        		echo "<option value='Weekly' selected='true'>Weekly</option>";
		        	} 
		        	else {
		        		echo "<option value='Weekly'>Weekly</option>";
		        	}
		       	if ($recurtype == "Monthly") {
		       		echo "<option value='Monthly' selected='true'>Monthly</option>";
		        	} 
		        	else {
		        		echo "<option value='Monthly'>Monthly</option>";
		        	}
		        	?>
		        	</select>
		   		</div>
		   		<br/>
		   		<br/>
		 	</div>
		
			<div class="form-group" id='Weekly' style="display:none">
				<label for="Weekly Setup" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Weekly Setup</label>
				<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
				 <label style='font-weight: normal;'>Recur every: </label>
		          <input type='text' name='weeknumberofweeks' size='10' value='<?php echo $weeknumberofweeks ?>'>
		          <label style='font-weight: normal;'>week(s) on: </label>
		          <div class="checkboxes">
		            <label style='font-weight: normal;'><input type='checkbox' name='weeksunday' <?php if ($weeksunday=='1') { ?> checked="checked" <?php ;} ?>> Sunday</label>
		            <label style='font-weight: normal;'><input type='checkbox' name='weekmonday' <?php if ($weekmonday=='1') { ?> checked="checked" <?php ;} ?>> Monday</label>
		            <label style='font-weight: normal;'><input type='checkbox' name='weektuesday' <?php if ($weektuesday=='1') { ?> checked="checked" <?php ;} ?>> Tuesday</label>
		            <label style='font-weight: normal;'><input type='checkbox' name='weekwednesday' <?php if ($weekwednesday=='1') { ?> checked="checked" <?php ;} ?>> Wednesday</label>
		            <label style='font-weight: normal;'><input type='checkbox' name='weekthursday' <?php if ($weekthursday=='1') { ?> checked="checked" <?php ;} ?>> Thursday</label>
		            <label style='font-weight: normal;'><input type='checkbox' name='weekfriday' <?php if ($weekfriday=='1') { ?> checked="checked" <?php ;} ?>> Friday</label>
		            <label style='font-weight: normal;'><input type='checkbox' name='weeksaturday' <?php if ($weeksaturday=='1') { ?> checked="checked" <?php ;} ?>> Saturday</label>
		          </div>
		       </div>
		    </div>
		
			
		    <div class="form-group" id='Monthly' style="display:none">
				<label for="Recurrance Type" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Monthly Setup</label>
			   	 	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
						<?php
			        	if ($monthoptions == 1) {
			        		echo "<input name='monthoptions' type='radio' value='1' checked />";
			        	}
			        	else {
			        		echo "<input name='monthoptions' type='radio' value='1' />";
			        	}
			        	?>
		        		<label style="font-weight:normal;">Day</label>
						<select name="monthdaynumber">
			          	<option value="">Day</option>
			          	<?php for ($i = 1; $i <= 31; $i++) :
			            	if ($monthdaynumber==$i) {
			              	echo "<option value=$monthdaynumber selected='true'>$monthdaynumber</option>";
			           	}
			            	else { ?>
			              	<option value="<?php echo $i; ?>"><?php if ($i == 0) { echo "0"; } else { echo $i; } ?></option>
			            	<?php }
			          	endfor; 
			          	?>
		         		</select>
		         		<label style="font-weight:normal;">of every</label>
			   			<input type='textrecurrence' name='monthfrequencynumber' size="3" value='<?php echo $monthfrequencynumber ?>'>
		        		<label style="font-weight:normal;">months</label>
			   		</div>
			   		<label class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label"></label>
		   		 	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
						OR <br/>
		        		<?php
		        		if ($monthoptions == 2) {
		        			echo "<input name='monthoptions' type='radio' value='2' checked />";
		        		}
		        		else {
		        			echo "<input name='monthoptions' type='radio' value='2' />";
		        		}
		        		?>
		        		<label style="font-weight:normal;">The</label>
		        		<select name='monthweeknumber' id='monthweeknumber'>
			        		<?php 
			        		if ($monthweeknumber == "" or $monthweeknumber == "Select") { 
			        			echo "<option value='Select' selected='true'>Select</option>";
			        		} 
			        		else { 
			          		echo "<option value='Select'>Select</option>";
			        		}
			        		if ($monthweeknumber == "First") { 
			          		echo "<option value='First' selected='true'>First</option>";
			        		} 
			        		else {
			          		echo "<option value='First'>First</option>";
			        		}
			        		if ($monthweeknumber == "Second") { 
			          		echo "<option value='Second' selected='true'>Second</option>";
			        		} 
			        		else {
			          		echo "<option value='Second'>Second</option>";
			        		}
			        		if ($monthweeknumber == "Third") { 
			          		echo "<option value='Third' selected='true'>Third</option>";
			        		} 
			        		else {
			          		echo "<option value='Third'>Third</option>";
			        		}
			        		if ($monthweeknumber == "Fourth") { 
			          		echo "<option value='Fourth' selected='true'>Fourth</option>";
			        		} 
			        		else {
			          		echo "<option value='Fourth'>Fourth</option>";
			        		}
			        		if ($monthweeknumber == "Last") { 
			          		echo "<option value='Last' selected='true'>Last</option>";
			        		} 
			        		else {
			          		echo "<option value='Last'>Last</option>";
			        		}
			        		?>
			        		</select>
				        	<select name='monthweekday' id='monthweekday'>
				        	<?php if ($monthweekday == "" or $monthweekday == "Select") { ?>
				         	<option value='Select' selected='true'>Select</option>
				        	<?php } else { ?>
				         	<option value='Select'>Select</option>
				        	<?php } ?>
				        	<?php if ($monthweekday == "Day") { ?>
				         	<option value='Day' selected='true'>Day</option>
				        	<?php } else { ?>
				         	<option value='Day'>Day</option>
				        	<?php } ?>
				        	<?php if ($monthweekday == "Weekday") { ?>
				         	<option value='Weekday' selected='true'>Weekday</option>
				        	<?php } else { ?>
				         	<option value='Weekday'>Weekday</option>
				        	<?php } ?>
				        	<?php if ($monthweekday == "Weekend Day") { ?>
				        	<option value='Weekend Day' selected='true'>Weekend Day</option>
				        	<?php } else { ?>
				        	<option value='Weekend Day'>Weekend Day</option>
				        	<?php } ?>
				        	<?php if ($monthweekday == "Sunday") { ?>
				         	<option value='Sunday' selected='true'>Sunday</option>
				        	<?php } else { ?>
				         	<option value='Sunday'>Sunday</option>
				        	<?php } ?>
				        	<?php if ($monthweekday == "Monday") { ?>
				         	<option value='Monday' selected='true'>Monday</option>
				        	<?php } else { ?>
				         	<option value='Monday'>Monday</option>
				        	<?php } ?>
				        	<?php if ($monthweekday == "Tuesday") { ?>
				         	<option value='Tuesday' selected='true'>Tuesday</option>
				        	<?php } else { ?>
				         	<option value='Tuesday'>Tuesday</option>
				        	<?php } ?>
				        	<?php if ($monthweekday == "Wednesday") { ?>
				         	<option value='Wednesday' selected='true'>Wednesday</option>
				        	<?php } else { ?>
				         	<option value='Wednesday'>Wednesday</option>
				        	<?php } ?>
				        	<?php if ($monthweekday == "Thursday") { ?>
				         	<option value='Thursday' selected='true'>Thursday</option>
				        	<?php } else { ?>
				         	<option value='Thursday'>Thursday</option>
				        	<?php } ?>
				        	<?php if ($monthweekday == "Friday") { ?>
				         	<option value='Friday' selected='true'>Friday</option>
				        	<?php } else { ?>
				         	<option value='Friday'>Friday</option>
				        	<?php } ?>
				        	<?php if ($monthweekday == "Saturday") { ?>
				         	<option value='Saturday' selected='true'>Saturday</option>
				        	<?php } else { ?>
				         	<option value='Saturday'>Saturday</option>
				        	<?php } ?>
				     	</select>
				      	of every
				      	<input type='textrecurrence' name='monthfrequencynumber2' size="3" value='<?php echo $monthfrequencynumber2 ?>'>
				      	months
			    </div>
		    </div>
		     
		
		
			<div class="form-group" id='RecurranceRange' style="display:none">
				<label for="Recurrance Type" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Range of Recurrance</label>
				<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
					<?php
			     	if ($recurpatternoptions == 1) {
			    		echo "<input name='recurpatternoptions' type='radio' value='1' checked />";
			     	}
			      	else {
			        	echo "<input name='recurpatternoptions' type='radio' value='1' />";
			      	}
		        	?>
		        	<label style="font-weight:normal;">End after</label>
		        	<input type='textrecurrence' size='10' name='recurpatternnumberofoccurences' value='<?php echo $recurpatternnumberofoccurences ?>'>
		        	occurences  <br/>OR <br/>
		        	<div class="form-group">
		        		<?php
			        	if ($recurpatternoptions == 2) {
			        		echo "<input name='recurpatternoptions' type='radio' value='2' checked />";
			        	}
			        	else {
			        		echo "<input name='recurpatternoptions' type='radio' value='2' />";
			        	}
			        	?>
			        	<label style="font-weight:normal;">End By:</label>
		        		<select name="recurpatterndateenddate">
		          	<option value="">Day</option>
		          	<?php for ($i = 1; $i <= 31; $i++) :
		             if ($recurpatterndateenddate==$i) {
		              	echo "<option value=$recurpatterndateenddate selected='true'>$recurpatterndateenddate</option>";
		            	}
		            	else { ?>
		               	<option value="<?php echo ($i < 10) ? '0'.$i : $i; ?>"><?php echo $i; ?></option>
		            	<?php }
		          	?>
		          	<?php endfor; ?>
		          	</select>
		         		<select name="recurpatternmonthenddate">
		          	<option value="">Month</option>
		          	<?php for ($i = 1; $i <= 12; $i++) :
		          	if ($recurpatternmonthenddate==$i) {
		               	echo "<option value=$recurpatternmonthenddate selected='true'>$recurpatternmonthenddate</option>";
		           	}
		            	else { ?>
		              	<option value="<?php echo ($i < 10) ? '0'.$i : $i; ?>"><?php echo $i; ?></option>
		            	<?php }
		          	?>
		          	<?php endfor; ?>
		          	</select>
		        		<select name="recurpatternyearenddate">
		          	<option value="">Year</option>
		          	<?php for ($i = 2016; $i < date('Y')+100; $i++) :
		          	if ($recurpatternyearenddate==$i) {
		              	echo "<option value=$recurpatternyearenddate selected='true'>$recurpatternyearenddate</option>";
		           	}
		            	else { ?>
		              	<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
		            	<?php }
		          	?>
		          	<?php endfor; ?>
		          	</select>
					</div>
		     	</div>
		  	</div>
		</div>
	   <div class="form-group">
	   		<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
	    		<div style="text-align:center;">
	    			<br/><br/>
		      		<button type='submit' name='submit' value='Submit' class="button-primary"><?php echo $langval5 ?></button>																									
		    	</div>
	    	</div>
	  	</div>
	  	<script language="JavaScript" type="text/javascript">
	    <!--
	    function submitForm() {
	    //make sure hidden and iframe values are in sync before submitting form
	    //updateRTE('rte1'); //use this when syncing only 1 rich text editor ("rtel" is name of editor)
	    updateRTEs(); //uncomment and call this line instead if there are multiple rich text editors inside the form
	    alert("Submitted value: "+document.myform.rte1.value) //alert submitted value
	    return true; //Set to false to disable form submission, for easy debugging.
	    }
	
	    //Usage: initRTE(imagesPath, includesPath, cssFile)
	    initRTE("images/", "", "");
	    //-->
	  	</script>
	</form>
	
	<br/><br/><br/>
	<br/><br/><br/><br/><br/><br/><br/><br/><br/>
	<br/><br/><br/><br/><br/><br/><br/><br/><br/>
	<br/><br/><br/><br/><br/><br/><br/><br/><br/>
</div>
</div>	
<?php include_once ('../footer.php'); ?>
</body>
