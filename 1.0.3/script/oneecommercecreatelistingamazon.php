<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$date = date('Y-m-d');
include('../1.0.3/phpAmazonMWS/includes/classes.php');

$notoinsert = 1000;			


echo "<br/><br/><b>CHECK IF ANY EXISTING BATCHES HAVE BEEN PROCESSED AND PROCESS RESULTS - PRICE CHECK</b>";
//check if there are any files already processing
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$checkexistingfiles = mysql_query("select ecommerceprocessamazonfilename, ecommercesiteconfig.ecommercesiteconfigid, 
apikey1, apikey2, apikey3, apikey4, ecommerceprocessamazonfileid
 from $database.ecommerceprocessamazonfile
 inner join $database.ecommercesiteconfig on ecommercesiteconfig.ecommercesiteconfigid = ecommerceprocessamazonfile.ecommercesiteconfigid
where status = 'Update Pricing' and filetype = '_POST_PRODUCT_PRICING_DATA_' and ecommerceprocessamazonfilename <> ''");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get list of amazon config records: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
if(mysql_num_rows($checkexistingfiles) >= 1){
	//finish processing existing file
	$response = "";
	while($existingfile = mysql_fetch_array($checkexistingfiles)){
		$ecommerceprocessamazonfileid = $existingfile['ecommerceprocessamazonfileid'];
		$ecommercesiteconfigid = $existingfile['ecommercesiteconfigid'];
		$feedsubid = $existingfile['ecommerceprocessamazonfilename'];
		$apikey1 = $existingfile['apikey1'];
		$apikey2 = $existingfile['apikey2'];
		$apikey3 = $existingfile['apikey3'];
		$apikey4 = $existingfile['apikey4'];
		
		$errorupdate = "update ecommercelistinglocation 
		set integrationerrorretrynumber = 1, ecommercelistinglocationstatusid = 7, integrationerrormessage = case integrationproductid ";
		$errorupdatewhere = "";
		
		echo "<br/><br/>Get the status of the feed: ".$ecommercesiteconfigid." - ".$feedsubid;
		getFeedResult($feedsubid);	
		if (strpos(json_encode($response), 'Feed Submission Result is not ready for Feed ') !== false) {
			//list not uploaded yet
    		echo '<br/><br/>No Results yet';
		}
		else {
			//process the result
			$body = $response['body'];
			$xml = new SimpleXMLElement($body);
			$items =  $xml->xpath('/AmazonEnvelope/Message/ProcessingReport/Result');
			//echo json_encode($items);
			echo "<br/><br/>";
			foreach($items as $result){
				$sku = $result->AdditionalInfo->SKU;
				$resultdescription = $result->ResultDescription;
				$result = $result->ResultCode;
				echo "<br/>".$sku." - ".$result." - ".$resultdescription;		
				if($result == "Error" && $sku <> ""){
					$resultdescription = str_replace('"', " ", $resultdescription);
					$resultdescription = str_replace("'", " ", $resultdescription);
					$errorupdate = $errorupdate.' when "'.$sku.'" then "PRICING UPLOAD - '.$resultdescription.'" ';
					$errorupdatewhere = $errorupdatewhere.'"'.$sku.'",';
				}
			}
			
			if($errorupdatewhere <> ""){
				$errorupdatewhere = rtrim($errorupdatewhere, ",");
				$errorupdate = $errorupdate."else '' end where integrationproductid in (".$errorupdatewhere.") 
				and ecommercelistinglocationstatusid in (16)";
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				echo "<br/><br/>".$errorupdate;
				$errorupdate = mysql_query($errorupdate);
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Update all the errors into ecommercelistinglocation: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			}
			
			//mark the ecommerceprocessamazonfile processing as complete
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updatefile = mysql_query("update ecommerceprocessamazonfile 
			set status = 'Processed' where ecommerceprocessamazonfileid = '$ecommerceprocessamazonfileid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Update ecommerceprocessamazonfile as completed: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			
			//mark all the non-erroring ecommercelistinglocation records as complete
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updateecommercelistinglocation = mysql_query("update ecommercelistinglocation 
			set ecommercelistinglocationstatusid = 15 
			where integrationproductid <> '' and integrationerrormessage = '' and ecommercesiteconfigid = '$ecommercesiteconfigid'
			and ecommercelistinglocationstatusid = 16");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Update all successful records as listed: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			
		}	
	}
}
else {
	echo "<br/><br/><b>PROCESS ADDING PRICE INFORMATION</b>";
	//update price information for products that have been added to Amazon
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getsiteconfig = mysql_query("select * from $database.ecommercesiteconfig
	where ecommercesiteid = 1 
	order by rand()
	limit 1");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get list of amazon config records: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($siteconfig = mysql_fetch_array($getsiteconfig)){
		$ecommercesiteconfigid = $siteconfig['ecommercesiteconfigid'];
		$apikey1 = $siteconfig['apikey1'];
		$apikey2 = $siteconfig['apikey2'];
		$apikey3 = $siteconfig['apikey3'];
		$apikey4 = $siteconfig['apikey4'];
		
		//create products that have listing locations
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$getlistinglocations = mysql_query("select ecommercelistinglocationid, integrationproductid, listingprice, currencycode
		from $database.ecommercelistinglocation
		inner join $database.ecommercesiteconfig on ecommercelistinglocation.ecommercesiteconfigid = ecommercesiteconfig.ecommercesiteconfigid
		inner join $database.ecommercelisting on ecommercelisting.ecommercelistingid = ecommercelistinglocation.ecommercelistingid
		inner join $database.productstockitem on ecommercelisting.productstockitemid = productstockitem.productstockitemid
		inner join $database.currency on currency.currencyid = productstockitem.currencyid
		where ecommercelistinglocationstatusid in (14) and ecommercelistinglocation.ecommercesiteconfigid = $ecommercesiteconfigid
		and ecommercelistinglocation.productid > 1004493
		and listingprice > 0
		limit $notoinsert");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Get list of ecommercelistinglocation records that require pricing submitted: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		if(mysql_num_rows($getlistinglocations) >= 1){
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$createamazonfile = mysql_query("insert into ecommerceprocessamazonfile (status, filetype, ecommercesiteconfigid,
			disabled, datecreated, masteronly) values ('Update Pricing', 
			'_POST_PRODUCT_PRICING_DATA_', '$ecommercesiteconfigid', '0', '$date', '0')");
			$amazonfileid = mysql_insert_id();
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Insert a record into ecommerceprocessamazonfile: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			
			$feedprice = '<?xml version="1.0" encoding="iso-8859-1"?>
			<AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			    xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
			  <Header>
			    <DocumentVersion>1.01</DocumentVersion>
			    <MerchantIdentifier>'.$amazonfileid.'</MerchantIdentifier>
			  </Header>
			  <MessageType>Product</MessageType>';
			$i = 1;  
			
			
			$ecommercelistinglocationids = "";
			while($row92=mysql_fetch_array($getlistinglocations)){
				$ecommercelistinglocationid = $row92['ecommercelistinglocationid'];
				$integrationproductid = $row92['integrationproductid'];			
				$currencycode = $row92['currencycode'];			
				$listingprice = $row92['listingprice'];			
				$feedprice = $feedprice.'
				<Message>
				<MessageID>'.$i.'</MessageID>
				<Price>
					<SKU>'.$integrationproductid.'</SKU>
					<StandardPrice currency="'.$currencycode.'">'.$listingprice.'</StandardPrice>
				</Price>
			</Message>';
			  	
		  		$ecommercelistinglocationids = $ecommercelistinglocationids.$ecommercelistinglocationid.',';
			  	$i = $i + 1;
			}
			$feedprice = $feedprice.'</AmazonEnvelope>';
			//echo "<br/><br/>".nl2br(htmlentities($feedprice));
			echo "<br/><br/>Submit the pricing feed: ";
			sendPricingFeed($feedprice);
			echo "<br/>final feed sub id: ".$feedsubid;
			
			
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updateamazonfileid = mysql_query("update ecommerceprocessamazonfile set ecommerceprocessamazonfilename = '$feedsubid'
			where ecommerceprocessamazonfileid = '$amazonfileid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Insert the amazon id of the file being processed: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			
			$ecommercelistinglocationids = rtrim($ecommercelistinglocationids, ",");
			$updateecommercelistinglocation = "update ecommercelistinglocation set ecommercelistinglocationstatusid = 16 
			where ecommercelistinglocationid in (".$ecommercelistinglocationids.")";
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			echo "<br/><br/>".$updateecommercelistinglocation;	
			$updateamazonfileid = mysql_query($updateecommercelistinglocation);
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Insert the amazon id of the file being processed: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		}
	}		
}



echo "<br/><br/><b>CHECK IF ANY EXISTING BATCHES HAVE BEEN PROCESSED AND PROCESS RESULTS - INVENTORY CHECK</b>";
//check if there are any files already processing
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$checkexistingfiles = mysql_query("select ecommerceprocessamazonfilename, ecommercesiteconfig.ecommercesiteconfigid, 
apikey1, apikey2, apikey3, apikey4, ecommerceprocessamazonfileid
 from $database.ecommerceprocessamazonfile
 inner join $database.ecommercesiteconfig on ecommercesiteconfig.ecommercesiteconfigid = ecommerceprocessamazonfile.ecommercesiteconfigid
where status = 'Update Inventory' and filetype = '_POST_INVENTORY_AVAILABILITY_DATA_' and ecommerceprocessamazonfilename <> ''");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get list of amazon config records: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
if(mysql_num_rows($checkexistingfiles) >= 1){
	//finish processing existing file
	$response = "";
	while($existingfile = mysql_fetch_array($checkexistingfiles)){
		$ecommerceprocessamazonfileid = $existingfile['ecommerceprocessamazonfileid'];
		$ecommercesiteconfigid = $existingfile['ecommercesiteconfigid'];
		$feedsubid = $existingfile['ecommerceprocessamazonfilename'];
		$apikey1 = $existingfile['apikey1'];
		$apikey2 = $existingfile['apikey2'];
		$apikey3 = $existingfile['apikey3'];
		$apikey4 = $existingfile['apikey4'];
		
		$errorupdate = "update ecommercelistinglocation 
		set integrationerrorretrynumber = 1, ecommercelistinglocationstatusid = 7,
		integrationerrormessage = case integrationproductid ";
		$errorupdatewhere = "";
		
		echo "<br/><br/>Get the status of the feed: ".$ecommercesiteconfigid." - ".$feedsubid;
		getFeedResult($feedsubid);	
		if (strpos(json_encode($response), 'Feed Submission Result is not ready for Feed ') !== false) {
			//list not uploaded yet
    		echo '<br/><br/>No Results yet';
		}
		else {
			//process the result
			$body = $response['body'];
			$xml = new SimpleXMLElement($body);
			$items =  $xml->xpath('/AmazonEnvelope/Message/ProcessingReport/Result');
			//echo json_encode($items);
			echo "<br/><br/>";
			foreach($items as $result){
				$sku = $result->AdditionalInfo->SKU;
				$resultdescription = $result->ResultDescription;
				$result = $result->ResultCode;
				echo "<br/>".$sku." - ".$result." - ".$resultdescription;		
				if($result == "Error" && $sku <> ""){
					$resultdescription = str_replace('"', " ", $resultdescription);
					$resultdescription = str_replace("'", " ", $resultdescription);
					$errorupdate = $errorupdate.' when "'.$sku.'" then "INVENTORY UPLOAD - '.$resultdescription.'" ';
					$errorupdatewhere = $errorupdatewhere.'"'.$sku.'",';
				}
			}
			
			if($errorupdatewhere <> ""){
				$errorupdatewhere = rtrim($errorupdatewhere, ",");
				$errorupdate = $errorupdate."else '' end where integrationproductid in (".$errorupdatewhere.") 
				and ecommercelistinglocationstatusid in (17)";
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				echo "<br/><br/>".$errorupdate;
				$errorupdate = mysql_query($errorupdate);
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Update all the errors into ecommercelistinglocation: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			}
			
			//mark the ecommerceprocessamazonfile processing as complete
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updatefile = mysql_query("update ecommerceprocessamazonfile 
			set status = 'Processed' where ecommerceprocessamazonfileid = '$ecommerceprocessamazonfileid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Update ecommerceprocessamazonfile as completed: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			
			//mark all the non-erroring ecommercelistinglocation records as complete
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updateecommercelistinglocation = mysql_query("update ecommercelistinglocation 
			set ecommercelistinglocationstatusid = 2, integrationstatusname = 'Listed' 
			where integrationproductid <> '' and integrationerrormessage = '' and ecommercesiteconfigid = '$ecommercesiteconfigid'
			and ecommercelistinglocationstatusid = 17");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Update all successful records as listed: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;			
		}	
	}
}
else {
	echo "<br/><br/><b>PROCESS ADDING INVENTORY INFORMATION</b>";
	//update inventory information for products that have been added to Amazon
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getsiteconfig = mysql_query("select * from $database.ecommercesiteconfig
	where ecommercesiteid = 1 
	order by rand()
	limit 1");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get list of amazon config records: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($siteconfig = mysql_fetch_array($getsiteconfig)){
		$ecommercesiteconfigid = $siteconfig['ecommercesiteconfigid'];
		$apikey1 = $siteconfig['apikey1'];
		$apikey2 = $siteconfig['apikey2'];
		$apikey3 = $siteconfig['apikey3'];
		$apikey4 = $siteconfig['apikey4'];
		$productindividual = $siteconfig['productindividual'];
		//create products that have listing locations
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$getlistinglocations = mysql_query("select ecommercelistinglocationid, integrationproductid, productstockitem.productid, 
		mindeliverydays, maxdeliverydays, defaultmindeliverydays, defaultmaxdeliverydays
		from $database.ecommercelistinglocation
		inner join $database.ecommercesiteconfig on ecommercelistinglocation.ecommercesiteconfigid = ecommercesiteconfig.ecommercesiteconfigid
		inner join $database.ecommercelisting on ecommercelisting.ecommercelistingid = ecommercelistinglocation.ecommercelistingid
		inner join $database.productstockitem on ecommercelisting.productstockitemid = productstockitem.productstockitemid
		where ecommercelistinglocationstatusid in (15) and ecommercelistinglocation.ecommercesiteconfigid = $ecommercesiteconfigid
		and ecommercelistinglocation.productid > 1004493
		limit $notoinsert");
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Get list of ecommercelistinglocation records that require inventory submitted: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;



		if(mysql_num_rows($getlistinglocations) >= 1){
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$createamazonfile = mysql_query("insert into ecommerceprocessamazonfile (status, filetype, ecommercesiteconfigid,
			disabled, datecreated, masteronly) values ('Update Inventory', 
			'_POST_INVENTORY_AVAILABILITY_DATA_', '$ecommercesiteconfigid', '0', '$date', '0')");
			$amazonfileid = mysql_insert_id();
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Insert a record into ecommerceprocessamazonfile: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			
			//find out how many active products there are for each productid	
			$productcountlist = array();		
			if($productindividual == 0){
				$productidlist = "";
				$ecommercelistinglocationidlist = "";
				while($row22=mysql_fetch_array($getlistinglocations)){	
					$productid = $row22['productid'];
					$ecommercelistinglocationid = $row22['ecommercelistinglocationid'];
					$productidlist = $productidlist.$productid.",";
					$ecommercelistinglocationidlist = $ecommercelistinglocationidlist.$ecommercelistinglocationid.",";
				}	
				$productidlist = rtrim($productidlist, ",");
				$ecommercelistinglocationidlist = rtrim($ecommercelistinglocationidlist, ",");
				echo "<br/><br/>".$productidlist;
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$getproductcounts = mysql_query("select productid, count(productid) as 'numproducts' from productstockitem 
				where productid in ($productidlist) and productstockitemstatusid = 1 and showasnotstock = 0 
				group by productid");
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Get product count for each product: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
				
				while($row33 = mysql_fetch_array($getproductcounts)){
					$productid = $row33['productid'];
					$numproducts = $row33['numproducts'];
					array_push($productcountlist, array("productid"=>$productid, "numproducts"=>$numproducts));				
				}
				
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$getlistinglocations = mysql_query("select ecommercelistinglocationid, integrationproductid, productstockitem.productid, 
				mindeliverydays, maxdeliverydays, defaultmindeliverydays, defaultmaxdeliverydays
				from $database.ecommercelistinglocation
				inner join $database.ecommercesiteconfig on ecommercelistinglocation.ecommercesiteconfigid = ecommercesiteconfig.ecommercesiteconfigid
				inner join $database.ecommercelisting on ecommercelisting.ecommercelistingid = ecommercelistinglocation.ecommercelistingid
				inner join $database.productstockitem on ecommercelisting.productstockitemid = productstockitem.productstockitemid
				where ecommercelistinglocationid in ($ecommercelistinglocationidlist)");
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Get list of ecommercelistinglocation records that require inventory submitted: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
				
				echo "<br/><br/>".json_encode($productcountlist);
			
			}
			
			$feedinventory = '<?xml version="1.0" encoding="iso-8859-1"?>
			<AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			    xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
			  <Header>
			    <DocumentVersion>1.01</DocumentVersion>
			    <MerchantIdentifier>'.$amazonfileid.'</MerchantIdentifier>
			  </Header>
			  <MessageType>Product</MessageType>';
			$i = 1;  
			$ecommercelistinglocationids = "";
			while($row92=mysql_fetch_array($getlistinglocations)){
				$ecommercelistinglocationid = $row92['ecommercelistinglocationid'];
				$integrationproductid = $row92['integrationproductid'];	
				$productid = $row92['productid']; 
				$mindeliverydays = $row92['mindeliverydays'];	
				$maxdeliverydays = $row92['maxdeliverydays'];	
				$defaultmindeliverydays = $row92['defaultmindeliverydays'];	
				$defaultmaxdeliverydays = $row92['defaultmaxdeliverydays'];	
				$fulfillmentlatency = 0;
				if($maxdeliverydays >= 1){
					$fulfillmentlatency = $maxdeliverydays;				
				}
				else {
					$fulfillmentlatency = $defaultmaxdeliverydays;				
				}
				echo "<br/>".$ecommercelistinglocationid." - ".$productid." - ".$fulfillmentlatency;
				$numproducts = 1;
				if($productindividual == 0){
					foreach($productcountlist as $prod){
						if($prod['productid'] == $productid){
							$numproducts = $prod['numproducts'];
							break;
						}
					}				
				}	
				else {
					$numproducts = 1;				
				}	
				
				$feedinventory = $feedinventory.'
				<Message>
					<MessageID>'.$i.'</MessageID>
					<OperationType>Update</OperationType>
					<Inventory>
						<SKU>'.$integrationproductid.'</SKU>
						<Quantity>'.$numproducts.'</Quantity>';
						if($fulfillmentlatency > 0){
							$feedinventory = $feedinventory.'<FulfillmentLatency>'.$fulfillmentlatency.'</FulfillmentLatency>';
						}
					$feedinventory = $feedinventory.'
					</Inventory>
				</Message>';
				
				$ecommercelistinglocationids = $ecommercelistinglocationids.$ecommercelistinglocationid.',';
				$i = $i + 1;
				
			}
			
			$feedinventory = $feedinventory.'</AmazonEnvelope>';
			echo "<br/><br/>".nl2br(htmlentities($feedinventory));
			echo "<br/><br/>Submit the inventory feed: ";
			sendInventoryFeed($feedinventory);
			echo "<br/>final feed sub id: ".$feedsubid;
			
			
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updateamazonfileid = mysql_query("update ecommerceprocessamazonfile set ecommerceprocessamazonfilename = '$feedsubid'
			where ecommerceprocessamazonfileid = '$amazonfileid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Insert the amazon id of the file being processed: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			
			$ecommercelistinglocationids = rtrim($ecommercelistinglocationids, ",");
			$updateecommercelistinglocation = "update ecommercelistinglocation set ecommercelistinglocationstatusid = 17 
			where ecommercelistinglocationid in (".$ecommercelistinglocationids.")";
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			echo "<br/><br/>".$updateecommercelistinglocation;	
			$updateamazonfileid = mysql_query($updateecommercelistinglocation);
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Update the ecommercelistinglocation records with the correct status: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		}
	}
}	


echo "<br/><br/><b>CHECK IF ANY EXISTING BATCHES HAVE BEEN PROCESSED AND PROCESS RESULTS - PRODUCT IMPORT</b>";
//check if there are any files already processing
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$checkexistingfiles = mysql_query("select ecommerceprocessamazonfilename, ecommercesiteconfig.ecommercesiteconfigid, 
apikey1, apikey2, apikey3, apikey4, ecommerceprocessamazonfileid
from $database.ecommerceprocessamazonfile
inner join $database.ecommercesiteconfig on ecommercesiteconfig.ecommercesiteconfigid = ecommerceprocessamazonfile.ecommercesiteconfigid
where status = 'Add New Listings' and filetype = '_POST_PRODUCT_DATA_' and ecommerceprocessamazonfilename <> ''");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get list of amazon config records: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
if(mysql_num_rows($checkexistingfiles) >= 1){
	//finish processing existing file
	$response = "";
	while($existingfile = mysql_fetch_array($checkexistingfiles)){
		$ecommerceprocessamazonfileid = $existingfile['ecommerceprocessamazonfileid'];
		$ecommercesiteconfigid = $existingfile['ecommercesiteconfigid'];
		$feedsubid = $existingfile['ecommerceprocessamazonfilename'];
		$apikey1 = $existingfile['apikey1'];
		$apikey2 = $existingfile['apikey2'];
		$apikey3 = $existingfile['apikey3'];
		$apikey4 = $existingfile['apikey4'];
		
		$errorupdate = "update ecommercelistinglocation 
		set integrationerrorretrynumber = 1, ecommercelistinglocationstatusid = 7,
		integrationerrormessage = case integrationproductid ";
		$errorupdatewhere = "";
		
		echo "<br/><br/>Get the status of the feed: ".$ecommercesiteconfigid." - ".$feedsubid;
		getFeedResult($feedsubid);	
		if (strpos(json_encode($response), 'Feed Submission Result is not ready for Feed ') !== false) {
			//list not uploaded yet
    		echo '<br/><br/>No Results yet';
		}
		else {
			//process the result
			$body = $response['body'];
			$xml = new SimpleXMLElement($body);
			$items =  $xml->xpath('/AmazonEnvelope/Message/ProcessingReport/Result');
			echo json_encode($items);
			echo "<br/><br/>";
			foreach($items as $result){
				$sku = $result->AdditionalInfo->SKU;
				$resultdescription = $result->ResultDescription;
				$result = $result->ResultCode;
				echo "<br/><br/><br/>".$sku." - ".$result." - ".$resultdescription;		
				$resultdescription = str_replace('"', "'", $resultdescription);
				if($result == "Error" && $sku <> ""){
					if($resultdescription <> "There is a restriction in effect at the item level (because of Item-Level gatings on an offer)"){
						$errorupdate = $errorupdate.' when "'.$sku.'" then "PRODUCT UPLOAD - '.$resultdescription.'" ';
						$errorupdatewhere = $errorupdatewhere.'"'.$sku.'",';		
					}			
				}
			}
			
			if($errorupdatewhere <> ""){
				$errorupdatewhere = rtrim($errorupdatewhere, ",");
				$errorupdate = $errorupdate."else '' end where integrationproductid in (".$errorupdatewhere.") 
				and ecommercelistinglocationstatusid in (1,7)";
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				echo "<br/><br/>errorupdate: ".$errorupdate;
				$errorupdate = mysql_query($errorupdate);
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Update all the errors into ecommercelistinglocation: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			}
			
			//mark the ecommerceprocesamazonfile processing as complete
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updatefile = mysql_query("update ecommerceprocessamazonfile 
			set status = 'Processed' where ecommerceprocessamazonfileid = '$ecommerceprocessamazonfileid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Update ecommerceprocessamazonfile as completed: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			
			//mark all the non-erroring ecommercelistinglocation records as complete
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updateecommercelistinglocation = mysql_query("update ecommercelistinglocation 
			set ecommercelistinglocationstatusid = 14 
			where integrationproductid <> '' and integrationerrormessage = '' and ecommercesiteconfigid = '$ecommercesiteconfigid'
			and ecommercelistinglocationstatusid in (1,7)");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Update all successful records as listed: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			
		}	
	}
}
else {
	echo "<br/><br/><b>PROCESS NEW AMAZON PRODUCT LISTINGS</b>";
	//create products that have listing locations
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getsiteconfig = mysql_query("select * from $database.ecommercesiteconfig
	where ecommercesiteid = 1 
	order by rand()
	limit 1");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get list of amazon config records: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	while($siteconfig = mysql_fetch_array($getsiteconfig)){
		$ecommercesiteconfigid = $siteconfig['ecommercesiteconfigid'];
		$apikey1 = $siteconfig['apikey1'];
		$apikey2 = $siteconfig['apikey2'];
		$apikey3 = $siteconfig['apikey3'];
		$apikey4 = $siteconfig['apikey4'];
		
		//create products that have listing locations
		$nosqlqueries = $nosqlqueries + 1;
		$sqlstarttime = microtime(true);
		$getlistinglocations = "select ecommercelistinglocationid, amazonproductxml, masterproductid, ecommercelistinglocation.productid
		from $database.ecommercelistinglocation
		inner join product on product.productid = ecommercelistinglocation.productid		
		inner join $database.ecommercesiteconfig on ecommercelistinglocation.ecommercesiteconfigid = ecommercesiteconfig.ecommercesiteconfigid
		where ecommercelistinglocationstatusid in (1) and integrationerrorretrynumber < 3 and listingprice > 0
		and amazonproductxml <> '' and ecommercelistinglocation.ecommercesiteconfigid = $ecommercesiteconfigid 
		and ecommercelistinglocation.productid > 1004493
		limit $notoinsert";
		echo "<br/>".$getlistinglocations;
		$getlistinglocations = mysql_query($getlistinglocations);
		$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
		$differencemilliseconds = microtime(true) - $sqlstarttime;
		echo "<br/>Get list of ecommercelistinglocation records that require processing: ";
		echo "<br/>differencemilliseconds: ".$differencemilliseconds;
		echo "<br/>sqlqueriestime: ".$sqlqueriestime;

		// Check for exclusion --
		$sql123 = "select productid, ecommercesiteid, donotupdateproductdata, donotupdateproductimagedata
		from ecommerceproductupdateexclusion where ecommercesiteid = 1 ";
		$sql123 = mysql_query($sql123);

		$exclusionarr = array();

		while($row123 = mysql_fetch_array($sql123))
		{
			array_push($exclusionarr, array("productid" => $row123['productid'],
			"ecommercesiteid" => $row123['ecommercesiteid'], "donotupdateproductdata" => $row123['donotupdateproductdata'],
			"donotupdateproductimagedata" => $row123['donotupdateproductimagedata']));
		}

		if(mysql_num_rows($getlistinglocations) >= 1){
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$createamazonfile = mysql_query("insert into ecommerceprocessamazonfile (status, filetype, ecommercesiteconfigid,
			disabled, datecreated, masteronly) values ('Add New Listings', 
			'_POST_PRODUCT_DATA_', '$ecommercesiteconfigid', '0', '$date', '0')");
			$amazonfileid = mysql_insert_id();
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Insert a record into ecommerceprocessamazonfile: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
		
			$feed = '<?xml version="1.0" encoding="iso-8859-1"?>
			<AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			    xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
			  <Header>
			    <DocumentVersion>1.01</DocumentVersion>
			    <MerchantIdentifier>'.$amazonfileid.'</MerchantIdentifier>
			  </Header>
			  <MessageType>Product</MessageType>
			  <PurgeAndReplace>false</PurgeAndReplace>';
			$i = 1;  
			
			$updateintegrationproductids = "update ecommercelistinglocation 
			set integrationerrormessage = '', integrationerrorretrynumber = '', ecommercelistinglocationstatusid = 1, 
			integrationproductid = case ecommercelistinglocationid ";
			$updateintegrationproductidswhere = "";
			while($row92=mysql_fetch_array($getlistinglocations)){
				$ecommercelistinglocationid = $row92['ecommercelistinglocationid'];
				$prodid = $row92['productid'];
				$amazonproductxml = $row92['amazonproductxml'];			
				$masterproductid = $row92['masterproductid'];
				
				/* Code - By Rahul */
				// Check if array has product having exclusion
				$key = array_search($prodid, array_column($exclusionarr, 'productid'));
				$flag = true;
				
				if($key)
				{
					if($exclusionarr[$key]['donotupdateproductdata'] == 1)
					{
						// This product data will not be added to xml for any update.
						$flag = false;
					}
				}
				/* Code - By Rahul */
				// Check for exclusion 
				if($flag)
				{
				//check recommendedbrowsenode is not within the xml, if the product has been previously added to Amazon
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				$previousadded = mysql_query("select * from ecommercelistinglocation 
				where productid = $prodid and ecommercelistinglocationstatusid in (2,3,4,5,6,8,12,13,14,15,16,17,18)
				and ecommercesiteconfigid = $ecommercesiteconfigid");
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Check if this product has been added to Amazon previously: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;		
				$numpreviousadded = mysql_num_rows($previousadded);
				echo "<br/>numpreviousadded: ".$numpreviousadded;
				if($numpreviousadded >= 1){
					//this product has previously been added, and so need to remove the recommendedbrowsenode from XML
					$arr = explode("RecommendedBrowseNode", $amazonproductxml);
					$first = $arr[0];	
					$first = substr($first, 0, -1);
					//echo "<br/><br/>first: ".nl2br(htmlentities($first));			
					if(isset($arr[1])){
						$second = $arr[1];	
						$third = $arr[2];	
						$third = substr($third, 1);
						//echo "<br/><br/>second: ".nl2br(htmlentities($second));			
						//echo "<br/><br/>third: ".nl2br(htmlentities($third));	
						$amazonproductxml = $first.$third;
						//echo "<br/><br/>amazonproductxml: ".nl2br(htmlentities($amazonproductxml));	
					}		
				}
				
				
				$feed = $feed.'<Message>
			    <MessageID>'.$i.'</MessageID>
			    '.$amazonproductxml.'
			  </Message>
			  ';

			  $i = $i + 1;
				  
			}

			$start = '<SKU>';
			$end = '</SKU>';
			$string = ' ' . $amazonproductxml;
			$ini = strpos($string, $start);
			if ($ini == 0) return '';
			$ini += strlen($start);
			$len = strpos($string, $end, $ini) - $ini;
			$sku = substr($string, $ini, $len);
			echo "<br/>ecommercelistinglocationid: ".$ecommercelistinglocationid;
			echo " - sku: ".$sku;			

			$updateintegrationproductids = $updateintegrationproductids.' when '.$ecommercelistinglocationid.' then "'.$sku.'" ';
			$updateintegrationproductidswhere = $updateintegrationproductidswhere.$ecommercelistinglocationid.',';
			
			}
			$feed = $feed.'</AmazonEnvelope>';
			echo "<br/><br/>".nl2br(htmlentities($feed));
			echo "<br/><br/>Process the insert product feed: ";
			
			sendProductFeed($feed);
			echo "<br/>final feed sub id: ".$feedsubid;
			
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$updateamazonfileid = mysql_query("update ecommerceprocessamazonfile set ecommerceprocessamazonfilename = '$feedsubid'
			where ecommerceprocessamazonfileid = '$amazonfileid'");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Insert the amazon id of the file being processed: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			
			if($updateintegrationproductidswhere <> ""){
				$updateintegrationproductidswhere = rtrim($updateintegrationproductidswhere, ",");
				$updateintegrationproductids = $updateintegrationproductids."else '' end 
				where ecommercelistinglocationid in (".$updateintegrationproductidswhere.") 
				and ecommercelistinglocationstatusid in (1,7)";
				$nosqlqueries = $nosqlqueries + 1;
				$sqlstarttime = microtime(true);
				echo "<br/><br/>".$updateintegrationproductids;
				$updateintegrationproductids = mysql_query($updateintegrationproductids);
				$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
				$differencemilliseconds = microtime(true) - $sqlstarttime;
				echo "<br/>Update all the errors into ecommercelistinglocation: ";
				echo "<br/>differencemilliseconds: ".$differencemilliseconds;
				echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			}
			
			
		}
	}
}


//UPLOAD RELATIONSHIPS FOR SUCCESSFUL PRODUCTS

echo "<br/><br/><b>UPLOAD RELATIONSHIPS FOR SUCCESSFUL PRODUCTS</b>";
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$getsiteconfig = mysql_query("select * from $database.ecommercesiteconfig
where ecommercesiteid = 1");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get list of amazon config records: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
while($siteconfig = mysql_fetch_array($getsiteconfig)){
	$ecommercesiteconfigid = $siteconfig['ecommercesiteconfigid'];
	$apikey1 = $siteconfig['apikey1'];
	$apikey2 = $siteconfig['apikey2'];
	$apikey3 = $siteconfig['apikey3'];
	$apikey4 = $siteconfig['apikey4'];
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getrelationships = mysql_query("select masterproductid
	from ecommercelistinglocation 
	inner join product on product.productid = ecommercelistinglocation.productid
	where ecommercesiteconfigid = $ecommercesiteconfigid and ecommercelistinglocationstatusid in (14) 
	and masterproductid <> 0  and ecommercelistinglocation.productid > 1004493
	group by masterproductid");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get list of records that need relationship added/updated: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$randno = rand(1000000,2000000);
	$feed = '<?xml version="1.0" encoding="iso-8859-1"?>
	<AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	    xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
	  <Header>
	    <DocumentVersion>1.01</DocumentVersion>
	    <MerchantIdentifier>'.$randno.'</MerchantIdentifier>
	  </Header>
	  <MessageType>Relationship</MessageType>';
	$i = 1;
	
	if(mysql_num_rows($getrelationships) >= 1){			
		while($row19 = mysql_fetch_array($getrelationships)){
			$masterproductid = $row19['masterproductid'];
			echo "<br/>masterproductid: ".$masterproductid;
			
			$feed = $feed.'<Message>
			    <MessageID>'.$i.'</MessageID>
			    <OperationType>Update</OperationType>
			  ';
		
			//get masterproduct record
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$getmasterproduct = mysql_query("select * from product 
			where productid = $masterproductid");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Get master productid: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			$getmasterproductrow = mysql_fetch_array($getmasterproduct);
			$productuniqueid = $getmasterproductrow['productuniqueid'];
			echo "<br/>productuniqueid: ".$productuniqueid;
			
			$feed = $feed.'<Relationship>
			<ParentSKU>'.$productuniqueid.'</ParentSKU>
			';
		
			//get child records
			$nosqlqueries = $nosqlqueries + 1;
			$sqlstarttime = microtime(true);
			$getchildrecords = mysql_query("select * from product 
			where masterproductid = $masterproductid and disabled = 0");
			$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
			$differencemilliseconds = microtime(true) - $sqlstarttime;
			echo "<br/>Get child records: ";
			echo "<br/>differencemilliseconds: ".$differencemilliseconds;
			echo "<br/>sqlqueriestime: ".$sqlqueriestime;
			while($row20 = mysql_fetch_array($getchildrecords)){
				$childproductid = $row20['productid'];
				$childproductuniqueid = $row20['productuniqueid'];
				echo "<br/>childproductid: ".$childproductid." - ".$childproductuniqueid;
				
				$feed = $feed.'<Relation>
				<SKU>'.$childproductuniqueid.'</SKU>
				<Type>Variation</Type>
				</Relation>
				';
			}
			
			$feed = $feed.'</Relationship>
			</Message>
			';
			
			$i = $i + 1;
		}
		
		$feed = $feed.'</AmazonEnvelope>';
		echo "<br/><br/>".nl2br(htmlentities($feed));
		echo "<br/><br/>Process the insert relationship feed: ";
		
		sendRelationshipFeed($feed);
		echo "<br/>final feed sub id: ".$feedsubid;
	}

}


//test the feed result for Relationship feed - insert the feedsubid to see result - comment out after testing
/*
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$getsiteconfig = mysql_query("select * from $database.ecommercesiteconfig
where ecommercesiteid = 1");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get list of amazon config records: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
while($siteconfig = mysql_fetch_array($getsiteconfig)){
	$ecommercesiteconfigid = $siteconfig['ecommercesiteconfigid'];
	$apikey1 = $siteconfig['apikey1'];
	$apikey2 = $siteconfig['apikey2'];
	$apikey3 = $siteconfig['apikey3'];
	$apikey4 = $siteconfig['apikey4'];
	echo "<br/><br/>Get the status of the feed: ".$ecommercesiteconfigid;
	getFeedResult(477696018356);	
	if (strpos(json_encode($response), 'Feed Submission Result is not ready for Feed ') !== false) {
		//list not uploaded yet
	 	echo '<br/><br/>No Results yet';
	}
	else {
		//process the result
		$body = $response['body'];
		$xml = new SimpleXMLElement($body);
		$items =  $xml->xpath('/AmazonEnvelope/Message/ProcessingReport/Result');
		echo json_encode($items);
		echo "<br/><br/>";
		foreach($items as $result){
			$sku = $result->AdditionalInfo->SKU;
			$resultdescription = $result->ResultDescription;
			$result = $result->ResultCode;
			echo "<br/><br/><br/>".$sku." - ".$result." - ".$resultdescription;		
		}
	}
}
*/

//UPLOAD IMAGES FOR SUCCESSFUL PRODUCTS

echo "<br/><br/><b>UPLOAD IMAGES FOR SUCCESSFUL PRODUCTS</b>";

// Check for exclusion --
$sql123 = "select productid, ecommercesiteid, donotupdateproductdata, donotupdateproductimagedata
from ecommerceproductupdateexclusion where ecommercesiteid = 1 ";
$sql123 = mysql_query($sql123);

$exclusionarr = array();

while($row123 = mysql_fetch_array($sql123))
{
	array_push($exclusionarr, array("productid" => $row123['productid'],
	"ecommercesiteid" => $row123['ecommercesiteid'], "donotupdateproductdata" => $row123['donotupdateproductdata'],
	"donotupdateproductimagedata" => $row123['donotupdateproductimagedata']));
}

$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$getsiteconfig = mysql_query("select * from $database.ecommercesiteconfig
where ecommercesiteid = 1");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get list of amazon config records: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
while($siteconfig = mysql_fetch_array($getsiteconfig)){
	$ecommercesiteconfigid = $siteconfig['ecommercesiteconfigid'];
	$apikey1 = $siteconfig['apikey1'];
	$apikey2 = $siteconfig['apikey2'];
	$apikey3 = $siteconfig['apikey3'];
	$apikey4 = $siteconfig['apikey4'];
	
	$nosqlqueries = $nosqlqueries + 1;
	$sqlstarttime = microtime(true);
	$getimages = mysql_query("select product.productid, productuniqueid, documenturl
	from ecommercelistinglocation 
	inner join product on product.productid = ecommercelistinglocation.productid
	inner join structurefunctiondocument on structurefunctiondocument.rowid = product.productid
	where ecommercesiteconfigid = $ecommercesiteconfigid and ecommercelistinglocationstatusid in (14) 
	and doctypename = 'Primary Image' and structurefunctionid = 74 
	and ecommercelistinglocation.productid > 1004493");
	$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
	$differencemilliseconds = microtime(true) - $sqlstarttime;
	echo "<br/>Get list of records that need images added/updated: ";
	echo "<br/>differencemilliseconds: ".$differencemilliseconds;
	echo "<br/>sqlqueriestime: ".$sqlqueriestime;
	
	$randno = rand(2000000,3000000);
	$feed = '<?xml version="1.0" encoding="iso-8859-1"?>
	<AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	    xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
	  <Header>
	    <DocumentVersion>1.01</DocumentVersion>
	    <MerchantIdentifier>'.$randno.'</MerchantIdentifier>
	  </Header>
	  <MessageType>ProductImage</MessageType>';
	$i = 1;  
	$mainurl = $_SERVER['SERVER_NAME'];
	if(mysql_num_rows($getimages) >= 1){			
		while($row21 = mysql_fetch_array($getimages)){
			$productid = $row21['productid'];
			$productuniqueid = $row21['productuniqueid'];
			$documenturl = $row21['documenturl'];
			echo "<br/>productid: ".$productid;

			/* Code start */
			// Check if array has product having exclusion
			$keyval = array_search($productid, array_column($exclusionarr, 'productid'));
			$flagImage = true;
			
			if($keyval)
			{
				if($exclusionarr[$keyval]['donotupdateproductimagedata'] == 1)
				{
					// This product image data will not be added to xml for any update.
					$flagImage = false;
				}
			}
			/* Code end */
			// Check for exclusion 
			
			if($documenturl <> '' && $flagImage){
				$feed = $feed.'<Message>
			   	<MessageID>'.$i.'</MessageID>
			  	<OperationType>Update</OperationType>
				<ProductImage>
				<SKU>'.$productuniqueid.'</SKU>
				<ImageType>Main</ImageType>
				<ImageLocation>http://'.$mainurl.'/onebusiness/documents/'.$documenturl.'</ImageLocation>
				</ProductImage>
				</Message>
				';
				
				$i = $i + 1;
			}
		}
		
		$feed = $feed.'</AmazonEnvelope>';
		echo "<br/><br/>".nl2br(htmlentities($feed));
		echo "<br/><br/>Process the insert image feed: ";
		
		sendImageFeed($feed);
		echo "<br/>final feed sub id: ".$feedsubid;
	}
}


//test the feed result for Image feed - insert the feedsubid to see result - comment out after testing
/*
$nosqlqueries = $nosqlqueries + 1;
$sqlstarttime = microtime(true);
$getsiteconfig = mysql_query("select * from $database.ecommercesiteconfig
where ecommercesiteid = 1");
$sqlqueriestime = $sqlqueriestime + (microtime(true) - $sqlstarttime);
$differencemilliseconds = microtime(true) - $sqlstarttime;
echo "<br/>Get list of amazon config records: ";
echo "<br/>differencemilliseconds: ".$differencemilliseconds;
echo "<br/>sqlqueriestime: ".$sqlqueriestime;
while($siteconfig = mysql_fetch_array($getsiteconfig)){
	$ecommercesiteconfigid = $siteconfig['ecommercesiteconfigid'];
	$apikey1 = $siteconfig['apikey1'];
	$apikey2 = $siteconfig['apikey2'];
	$apikey3 = $siteconfig['apikey3'];
	$apikey4 = $siteconfig['apikey4'];
	
	echo "<br/><br/>Get the status of the feed: ".$ecommercesiteconfigid;
	getFeedResult(475606018345);	
	if (strpos(json_encode($response), 'Feed Submission Result is not ready for Feed ') !== false) {
		//list not uploaded yet
	 	echo '<br/><br/>No Results yet';
	}
	else {
		//process the result
		$body = $response['body'];
		$xml = new SimpleXMLElement($body);
		$items =  $xml->xpath('/AmazonEnvelope/Message/ProcessingReport/Result');
		echo json_encode($items);
		echo "<br/><br/>";
		foreach($items as $result){
			$sku = $result->AdditionalInfo->SKU;
			$resultdescription = $result->ResultDescription;
			$result = $result->ResultCode;
			echo "<br/><br/><br/>".$sku." - ".$result." - ".$resultdescription;		
		}
	}
}
*/

echo "<br/><br/><br/>";


function sendProductFeed($feed) {
    try {
        $amz=new AmazonFeed(); //if there is only one store in config, it can be omitted
        $amz->setFeedType("_POST_PRODUCT_DATA_"); //feed types listed in documentation
        $amz->setFeedContent($feed); //can be either XML or CSV data; a file upload method is available as well
        $amz->submitFeed(); //this is what actually sends the request
        return $amz->getResponse();
    } catch (Exception $ex) {
        echo '<br/>There was a problem with the Amazon library. Error: '.$ex->getMessage();
    }
}

function sendRelationshipFeed($feed) {
    try {
        $amz=new AmazonFeed(); //if there is only one store in config, it can be omitted
        $amz->setFeedType("_POST_PRODUCT_RELATIONSHIP_DATA_"); //feed types listed in documentation
        $amz->setFeedContent($feed); //can be either XML or CSV data; a file upload method is available as well
        $amz->submitFeed(); //this is what actually sends the request
        return $amz->getResponse();
    } catch (Exception $ex) {
        echo '<br/>There was a problem with the Amazon library. Error: '.$ex->getMessage();
    }
}

function sendImageFeed($feed) {
    try {
        $amz=new AmazonFeed(); //if there is only one store in config, it can be omitted
        $amz->setFeedType("_POST_PRODUCT_IMAGE_DATA_"); //feed types listed in documentation
        $amz->setFeedContent($feed); //can be either XML or CSV data; a file upload method is available as well
        $amz->submitFeed(); //this is what actually sends the request
        return $amz->getResponse();
    } catch (Exception $ex) {
        echo '<br/>There was a problem with the Amazon library. Error: '.$ex->getMessage();
    }
}

function getFeedResult($feedId) {
    try {
        $amz=new AmazonFeedResult("Default", $feedId); //feed ID can be quickly set by passing it to the constructor
        $amz->setFeedId($feedId); //otherwise, it must be set this way
        $amz->fetchFeedResult();
        return $amz->getRawFeed();
    } catch (Exception $ex) {
        echo '<br/>There was a problem with the Amazon library. Error: '.$ex->getMessage();
    }
}

function sendPricingFeed($feed) {
    try {
        $amz=new AmazonFeed(); //if there is only one store in config, it can be omitted
        $amz->setFeedType("_POST_PRODUCT_PRICING_DATA_"); //feed types listed in documentation
        $amz->setFeedContent($feed); //can be either XML or CSV data; a file upload method is available as well
        $amz->submitFeed(); //this is what actually sends the request
        return $amz->getResponse();
    } catch (Exception $ex) {
        echo '<br/>There was a problem with the Amazon library. Error: '.$ex->getMessage();
    }
}

function sendInventoryFeed($feed) {
    try {
        $amz=new AmazonFeed(); //if there is only one store in config, it can be omitted
        $amz->setFeedType("_POST_INVENTORY_AVAILABILITY_DATA_"); //feed types listed in documentation
        $amz->setFeedContent($feed); //can be either XML or CSV data; a file upload method is available as well
        $amz->submitFeed(); //this is what actually sends the request
        return $amz->getResponse();
    } catch (Exception $ex) {
        echo '<br/>There was a problem with the Amazon library. Error: '.$ex->getMessage();
    }
}

?>