<?php session_start(); ?>
<?php include('../config.php'); ?>
<?php include('../sessionconfig.php'); ?>
<?php

require('../fpdf181/fpdf.php');

	class PDF extends FPDF
	{
		
	// Page header
	function Header()
	{
	    // Arial bold 15
	    $this->SetFont('Arial','B',13);
	}
	
	
	// Page footer
	function Footer()
	{
	    // Position at 1.5 cm from bottom
	    $this->SetY(-15);
	    // Arial italic 8
	    $this->SetFont('Arial','I',8);
	    // Page number
	    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	}
	
	}



//LANGUAGE COLLECTION SECTION
$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
and languagerecordid in (796,797,798,799,800,801,802)");
while($langrow = mysql_fetch_array($lang)){
	$langid = $langrow['languagerecordid'];
	${"langval$langid"} = $langrow['languagerecordtextname'];
}

$rowid = isset($_GET['rowid']) ? strip_tags($_GET['rowid']) : '';

$querybuilder = mysql_query("select payrollpaymentid, payrollpaymentname, employeename, paiddate, 
payrollpayment.businessunitid, currencycode, amount from payrollpayment 
inner join employee on employee.employeeid = payrollpayment.employeeid
inner join currency on currency.currencyid = payrollpayment.currencyid
where payrollpaymentid = $rowid");
while($rowpayment = mysql_fetch_array($querybuilder)){
	//get needed variables
	$payrollpaymentid = $rowpayment['payrollpaymentid'];
	$payrollpaymentname = $rowpayment['payrollpaymentname'];
	$employeename = $rowpayment['employeename'];
	$businessunitid = $rowpayment['businessunitid'];
	$paiddate = $rowpayment['paiddate'];
	$totalcurrencycode = $rowpayment['currencycode'];
	$totalamount = $rowpayment['amount'];
	
	//get business settings 
	$getbusiness = mysql_query("select * from payrollconfiguration 
	where businessunitid = $businessunitid");
	if(mysql_num_rows($getbusiness) >= 1){
		$getbusinessrow = mysql_fetch_array($getbusiness);
		$paymentstatementbusinessname = $getbusinessrow['paymentstatementbusinessname'];		$paymentstatementintrosentence = $getbusinessrow['paymentstatementintrosentence'];		$paymentstatementclosingsentence = $getbusinessrow['paymentstatementclosingsentence'];
	}
	else {
		$getbusiness2 = mysql_query("select * from businessunit 
		where businessunitid = $businessunitid");
		$getbusinessrow2 = mysql_fetch_array($getbusiness2);
		$paymentstatementbusinessname = $getbusinessrow2['businessunitname'];		$paymentstatementintrosentence = "";		$paymentstatementclosingsentence = "";
	}
	
	$date = date("d-M-Y");
	$paiddate = date("d-M-Y", strtotime($paiddate));
	
	// Instantiation of inherited class
	$pdf = new PDF();
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->SetFont('Arial','B',13);
	$pdf->Cell(75);
   	$pdf->Cell(40,10,$paymentstatementbusinessname,0,0,'C');
   	$pdf->Ln(15);
	$pdf->Cell(75);
   	$pdf->Cell(40,10,$langval796.' - '.$payrollpaymentid." - ".$employeename,0,0,'C');
   	$pdf->Ln(8);
	$pdf->Cell(75);
	$pdf->SetFont('Arial','',10);
   	$pdf->Cell(40,10,$paiddate,0,0,'C');
   	$pdf->Ln(15);
	$pdf->SetFont('Arial','',10);
	//$pdf->MultiCell(0,5,'Invoice Number: '.$accountbillinginvoiceid,0,1);
	//$pdf->MultiCell(0,5,'Invoice Date: '.$invoicedate,0,1);
	//$pdf->MultiCell(0,5,'Business: '.$tenantname,0,1);
	//$pdf->MultiCell(0,5,'For the attention of: '.$tenantperson,0,1);
	$pdf->Ln(5);
	$pdf->SetFont('Arial','',10);
	$pdf->MultiCell(0,5,$paymentstatementintrosentence,0,1);
	
	
	//iterate through the paymentitem positive values
	$querybuilder2 = mysql_query("select payrollpaymentitemname, currencycode, amountpaid from payrollpaymentitem 
	inner join currency on currency.currencyid = payrollpaymentitem.currencyid
	where payrollpaymentid = $payrollpaymentid and payrollpaymentitem.disabled = 0 and amountpaid > 0");
	if(mysql_num_rows($querybuilder2) >= 1){
		$pdf->SetFont('Arial','B',10);
		$pdf->Ln(5);
		$y = $pdf->GetY();
		$x = $pdf->GetX();
		$width = 120;
		$pdf->MultiCell($width, 6, $langval797, 1, 'L', FALSE);
		$pdf->SetXY($x + $width, $y);
		$pdf->Cell(20,6, $langval798, 1, 0, "l");	
		$q = $x + $width + 20;
		$pdf->SetXY($q, $y);
		$pdf->Cell(40,6, $langval799, 1, 0, "l");	
		$pdf->Ln(6);
		while($rowpaymentitem = mysql_fetch_array($querybuilder2)){
			//get needed variables
			$payrollpaymentitemname = $rowpaymentitem['payrollpaymentitemname'];
			$currencycode = $rowpaymentitem['currencycode'];
			$amountpaid = $rowpaymentitem['amountpaid'];
			$pdf->SetFont('Arial','',10);
			$y = $pdf->GetY();
			$x = $pdf->GetX();
			$width = 120;
			$pdf->MultiCell($width, 6, $payrollpaymentitemname, 1, 'L', FALSE);
			$pdf->SetXY($x + $width, $y);
			$pdf->Cell(20,6, $currencycode, 1, 0, "l");	
			$q = $x + $width + 20;
			$pdf->SetXY($q, $y);
			$pdf->Cell(40,6, number_format($amountpaid, 2), 1, 0, "l");	
			$pdf->Ln(6);
		}
	}
	
	//iterate through the paymentitem positive values
	$querybuilder3 = mysql_query("select payrollpaymentitemname, currencycode, amountdeducted from payrollpaymentitem 
	inner join currency on currency.currencyid = payrollpaymentitem.currencyid
	where payrollpaymentid = $payrollpaymentid and payrollpaymentitem.disabled = 0 and amountdeducted > 0");
	if(mysql_num_rows($querybuilder3) >= 1){
		$pdf->SetFont('Arial','B',10);
		$pdf->Ln(5);
		$y = $pdf->GetY();
		$x = $pdf->GetX();
		$width = 120;
		$pdf->MultiCell($width, 6, $langval800, 1, 'L', FALSE);
		$pdf->SetXY($x + $width, $y);
		$pdf->Cell(20,6, $langval798, 1, 0, "l");	
		$q = $x + $width + 20;
		$pdf->SetXY($q, $y);
		$pdf->Cell(40,6, $langval801, 1, 0, "l");	
		$pdf->Ln(6);
		while($rowpaymentitem = mysql_fetch_array($querybuilder3)){
			//get needed variables
			$payrollpaymentitemname = $rowpaymentitem['payrollpaymentitemname'];
			$currencycode = $rowpaymentitem['currencycode'];
			$amountpaid = $rowpaymentitem['amountdeducted'];
			$pdf->SetFont('Arial','',10);
			$y = $pdf->GetY();
			$x = $pdf->GetX();
			$width = 120;
			$pdf->MultiCell($width, 6, $payrollpaymentitemname, 1, 'L', FALSE);
			$pdf->SetXY($x + $width, $y);
			$pdf->Cell(20,6, $currencycode, 1, 0, "l");	
			$q = $x + $width + 20;
			$pdf->SetXY($q, $y);
			$pdf->Cell(40,6, number_format($amountpaid, 2), 1, 0, "l");	
			$pdf->Ln(6);
		}
	}
	
	//show total paid
	$pdf->SetFont('Arial','B',10);
	$pdf->Ln(6);
	$y = $pdf->GetY();
	$x = $pdf->GetX();
	$width = 120;
	$pdf->MultiCell($width, 6, $langval802, 1, 'L', FALSE);
	$pdf->SetXY($x + $width, $y);
	$pdf->Cell(20,6, $totalcurrencycode, 1, 0, "l");	
	$q = $x + $width + 20;
	$pdf->SetXY($q, $y);
	$pdf->Cell(40,6, number_format($totalamount, 2), 1, 0, "l");	
	$pdf->Ln(6);
	
	$pdf->Ln(6);
	$pdf->SetFont('Arial','',10);
	$pdf->MultiCell(0,5,$paymentstatementclosingsentence,0,1);
	
	//uncomment this, if you want to see an example in the browser for testing. However, comment it out once you are finished otherwise the whilst loop will not run.
	$pdf->Output();
	//$filename="../../documents/".$database."/Tristom Labs Ltd Invoice - ".$accountbillinginvoiceid.".pdf";
	//$pdf->Output($filename,'F');
	
	$updateinvoiceasgenerated = mysql_query("update $masterdatabase.accountbillinginvoice set accountbillinginvoicestatusid = '1'
	where accountbillinginvoiceid = '$rowid'");
	
}

//redirect back to the page
$url = '../View.php?viewid=49';
echo '<META HTTP-EQUIV=Refresh CONTENT="0; URL='.$url.'">';   				


?>