<?php session_start(); ?>
<?php include('config.php'); ?>
<?php include('sessionconfig.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<title>OneBusiness</title>
	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">   
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
	<script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<link rel='stylesheet' type='text/css' href='style.php' />
</head>
  
<body class='body'>
	<?php include_once ('headerthree.php');	?>
	<?php
	//LANGUAGE COLLECTION SECTION
	$lang = mysql_query("select * from $masterdatabase.languagerecordtext where languageid = $_SESSION[languageid]
	and languagerecordid in (39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,83,84,85,86,87)");
	while($langrow = mysql_fetch_array($lang)){
		$langid = $langrow['languagerecordid'];
		${"langval$langid"} = $langrow['languagerecordtextname'];
	}
	
	$tenantid = $_SESSION["tenantid"];
	$gettenant = mysql_query("select * from $masterdatabase.tenant 
	inner join $masterdatabase.accountbillingcurrency on accountbillingcurrency.accountbillingcurrencyid = tenant.accountbillingcurrencyid
	where tenantid = '$tenantid'");
	$gettenantrow = mysql_fetch_array($gettenant);
	$accountbillingcurrencyid = $gettenantrow['accountbillingcurrencyid'];
	$tenantid = $gettenantrow['tenantid'];
	$currencycode = $gettenantrow['currencycode'];
	$accountbillingdiscountpercentage = $gettenantrow['accountbillingdiscountpercentage'];
   					   
   	//check if the tenant has any SMS twilio costs this month
   	$usagedate = date("Y-m-01");
	$getsmscost = mysql_query("select * from $masterdatabase.accountbillingtwiliodailyusage
	where tenantid = $tenantid and usagedate = '$usagedate'");
	if(mysql_num_rows($getsmscost)>= 1){
		$smsbill = 1;	
	}
	else {
		$smsbill = 0;	
	}
	
	//check if the tenant has any Email Mailchimp costs this month
	$lastmonthstart = date("Y-m-d");
	$lastmonthstart = strtotime(date("Y-m-1", strtotime($lastmonthstart)) . "-1 months");
	$lastmonthstart = date("Y-m-d",$lastmonthstart);
	$lastmonthend= strtotime(date("Y-m-d", strtotime($lastmonthstart)) . "+1 months");
	$lastmonthend = date("Y-m-d",$lastmonthend);
	$countsendout = 0;
	
	$getbusinesses = "select * from $database.business where business.disabled = 0";
	//echo $getbusinesses;
	$getbusinesses = mysql_query($getbusinesses);
	while($row23 = mysql_fetch_array($getbusinesses)){
		$tempdatabase = $row23['databasename'];
		//check if onecustomerconfig table exists in the database
		$checktable = mysql_query("SELECT table_name
		FROM information_schema.tables
		WHERE table_schema = '$tempdatabase'
		AND table_name = 'campaignsendout'");
		$numrows = mysql_num_rows($checktable);
		if($numrows >= 1){
			//check if Mailchimp API keys are inserted
			$getaccountdetails = mysql_query("select * from $tempdatabase.onecustomerconfig where onecustomerconfigid = '1'");
			$getaccountdetailrow = mysql_fetch_array($getaccountdetails);
			$mailchimpapikey = $getaccountdetailrow['mailchimpapikey'];
			
			if($mailchimpapikey <> ""){
				$countsends = mysql_query("select * from $tempdatabase.campaignsendout 
				where datesent >= '$lastmonthstart' and datesent < '$lastmonthend'");
				$countsendout = mysql_num_rows($countsends) + $countsendout;
				$mailchimpemailsent = $countsendout;		
				//echo $tempdatabase." - ".$mailchimpemailsent."<br/>";
			}	
		}
	}
	
	//current costs
	$upto5000 = 0.04;
	$upto25000 = 0.03;
	$upto200000 = 0.015;
	$upto2000000 = 0.008;
	$upto5000000 = 0.005;
	$upto25000000 = 0.002;

	
	//calculate the total cost for the business
	$mailchimptotalcost = 0;
	if($countsendout <= 5000){
		$mailchimptotalcost = $countsendout*$upto5000;		
	}
	if($countsendout > 5000 && $countsendout <= 25000){
		$mailchimptotalcost = (5000*$upto5000)+(($countsendout-5000)*$upto25000);		
	}
	if($countsendout > 25000 && $countsendout <= 200000){
		$mailchimptotalcost = (5000*$upto5000)+(20000*$upto25000)+(($countsendout-25000)*$upto200000);		
	}
	if($countsendout > 200000 && $countsendout <= 2000000){
		$mailchimptotalcost = (5000*$upto5000)+(20000*$upto25000)+(175000*$upto200000)+(($countsendout-200000)*$upto2000000);		
	}
	if($countsendout > 2000000 && $countsendout <= 5000000){
		$mailchimptotalcost = (5000*$upto5000)+(20000*$upto25000)+(175000*$upto200000)+(1800000*$upto2000000)+(($countsendout-2000000)*$upto5000000);		
	}
	if($countsendout > 5000000 && ($countsendout <= 25000000 || $countsendout >= 25000000)){
		$mailchimptotalcost = (5000*$upto5000)+(20000*$upto25000)+(175000*$upto200000)+(1800000*$upto2000000)+(3000000*$upto5000000)+(($countsendout-5000000)*$upto25000000);		
	}
	
		
   ?>
   
	<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding:10px;">
		<!--search results header bar for desktop-->
		<div class='bodyheader'>
			<h3><?php echo $langval39 ?></h3>
		</div>	
		
		<div class='bodycontent'>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			
			</div>
				
				<p><?php echo $langval40 ?> <a href="view.php?viewid=2"><?php echo $langval41 ?></a></p><br/>
				
				<h2><?php echo $langval42 ?></h2>
				<?php
				echo "<br/>";
				$date = date("d-M-Y");
				$dateoneyear = date("Y-m-d", strtotime($date .'-12 months'));

				$getinvoice = mysql_query("select accountbillinginvoicename, invoicedate, accountbillinginvoiceid, 
				accountbillinginvoicestatusname, accountbillinginvoice.accountbillinginvoicestatusid from $masterdatabase.accountbillinginvoice 
				inner join $masterdatabase.accountbillinginvoicestatus on accountbillinginvoicestatus.accountbillinginvoicestatusid = 
				accountbillinginvoice.accountbillinginvoicestatusid
				where tenantid = '$tenantid' and invoicedate >= '$dateoneyear' 
				and (accountbillinginvoice.accountbillinginvoicestatusid = '1' or accountbillinginvoice.accountbillinginvoicestatusid = '2' 
				or accountbillinginvoice.accountbillinginvoicestatusid = '3' or accountbillinginvoice.accountbillinginvoicestatusid = '4')
				order by invoicedate desc");
				
				echo "<table class='table table-bordered'>";
				echo "<thead><tr><td><b>".$langval43."</b></td><td><b>".$langval44."</b></td><td><b>".$langval45."</b></td></tr></thead>";
				while($rowinvoice = mysql_fetch_array($getinvoice)){
					$invoicename = $rowinvoice['accountbillinginvoicename']	;			
					$invoicedate = $rowinvoice['invoicedate']	;			
					$accountbiilinginvoiceid = $rowinvoice['accountbillinginvoiceid'];			
					$accountbillinginvoicestatusname = $rowinvoice['accountbillinginvoicestatusname'];			
					$accountbillinginvoicestatusid = $rowinvoice['accountbillinginvoicestatusid'];			
					$invoicedate = date("d-M-Y", strtotime($invoicedate .'-0 months'));
					$documentpath = "script/accountbillinginvoicedownloadbusiness.php?rowid=".$accountbiilinginvoiceid;		
					echo "<tr><td>".$invoicedate."</td><td><a href='".$documentpath."'>".$invoicename."</a></td><td>".$accountbillinginvoicestatusname."</td></tr>";
				}
				echo "</table>";
				
				?>
				
				<br/><br/><h2><?php echo $langval46 ?></h2><br/>
				<p><?php echo $langval47 ?></b>
				<br/>			
				<?php
				//get businesses within the tenant
				$querybuilder = "select businessid, businessname from ".$database.".business where business.disabled='0' and business.businessid<>'1'";
				//echo $querybuilder;
				$getbusiness = mysql_query($querybuilder);
				
				$totaladminusers = 0;
				$totalnormalusers = 0;
				$totalsize = 0;
				
				echo "<br/><table class='table table-bordered'>";
				echo "<thead><tr><td><b>".$langval48."</b></td><td><b>".$langval49."</b></td><td><b>".$langval50."</b></td><td><b>".$langval51."</b></td>";
				if($smsbill == 1){				
					echo "<td><b>".$langval83."</b></td>";
				}
				if($mailchimptotalcost > 0){				
					echo "<td><b>".$langval87."</b></td>";
				}
				
				echo "</tr></thead>";
				while($businessrow=mysql_fetch_array($getbusiness)){
					echo "<tr>";
					//generate databasename for business
					$businessid = $businessrow['businessid'];
					$businessname = $businessrow['businessname'];
					$businessdatabase = "onebusinesstenant".$tenantid."business".$businessid;
					//echo "<br/><br/>businessdatabase - ".$businessdatabase."<br/>";
					echo "<td>".$businessname."</td>";
					//calculate how many admin users there are
					$querybuilder = "select count(user.userid) as adminusers from ".$businessdatabase.".user inner join ".$businessdatabase.".userrole on userrole.userid = user.userid where userrole.permissionroleid = '1' and user.disabled = '0'";
					$getadminusers = mysql_query($querybuilder);
					$adminusersrow = mysql_fetch_array($getadminusers);
					$adminusers = $adminusersrow['adminusers'];
					echo "<td>".$adminusers."</td>";
					
					//calculate how many normal users there are
					$querybuilder = "select count(user.userid) as normalusers from ".$businessdatabase.".user where user.disabled = '0'";
					$getnormalusers = mysql_query($querybuilder);
					$normalusersrow = mysql_fetch_array($getnormalusers);
					$normalusers = $normalusersrow['normalusers'] - $adminusers;
					echo "<td>".$normalusers."</td>";
					
					//calculate the size of the database and file repository
					mysql_select_db($businessdatabase);  
					$q = mysql_query("SHOW TABLE STATUS");  
					$size = 0;  
					while($row = mysql_fetch_array($q)) {  
					    $size += $row["Data_length"] + $row["Index_length"];  
					}
					$databasesize = number_format($size/(1024*1024),2);
					//echo "databasesize - ".$databasesize."<br/>";
					
					$file_directory = '../documents/'.$businessdatabase;
					$output = exec('du -sk ' . $file_directory);
					$repositorysize = trim(str_replace($file_directory, '', $output)) * 1024 / 1000000;
					//echo "repositorysize - ".$repositorysize."<br/>";
					
					$databasesize = $repositorysize+$databasesize;
					echo "<td>".$databasesize." MB</td>";
					
					
					//calculate the total cost of twilio SMS
					if($smsbill==1){						
						$getsmscost = mysql_query("select finaltotalcost, currencycode from $masterdatabase.accountbillingtwiliodailyusage
						inner join $masterdatabase.accountbillingcurrency on accountbillingcurrency.accountbillingcurrencyid = accountbillingtwiliodailyusage.accountbillingcurrencyid
						where businessname = '$businessname' and usagedate = '$usagedate'");
						$getsmscostrow = mysql_fetch_array($getsmscost);
						$smscost = $getsmscostrow['finaltotalcost'];					
						$currencycode = $getsmscostrow['currencycode'];					
						echo "<td>".$currencycode." ".$smscost."</td>";
					}
					
					//display the Mailchimp Email cost
					$checktable = mysql_query("SELECT table_name
					FROM information_schema.tables
					WHERE table_schema = '$businessdatabase'
					AND table_name = 'campaignsendout'");
					$numrows = mysql_num_rows($checktable);
					if($numrows >= 1){
						//check if Mailchimp API keys are inserted
						$getaccountdetails = mysql_query("select * from $businessdatabase.onecustomerconfig where onecustomerconfigid = '1'");
						$getaccountdetailrow = mysql_fetch_array($getaccountdetails);
						$mailchimpapikey = $getaccountdetailrow['mailchimpapikey'];
						
						if($mailchimpapikey <> ""){
							$countsends = mysql_query("select * from $tempdatabase.campaignsendout 
							where datesent >= '$lastmonthstart' and datesent < '$lastmonthend'");
							$businesssendout = mysql_num_rows($countsends);
							//echo $tempdatabase." - ".$mailchimpemailsent."<br/>";
							echo "<td>".$businesssendout."</td>";
						}	
					}
					echo "</tr>";
					
					//aggregate the values
					$totaladminusers = $adminusers + $totaladminusers;
					$totalnormalusers = $normalusers + $totalnormalusers;
					$totalsize = $databasesize + $totalsize;
					
				}
				echo "</table>";
				
				//echo $totaladminusers."<br/>";
				//echo $totalnormalusers."<br/>";
				//echo $totalsize."<br/>";
				
				//calculate the cost of the admin users
				$querybuilder = "select accountbillingadminuserrateid, rate from $masterdatabase.accountbillingadminuserrate where disabled='0' and 
				accountbillingcurrencyid='$accountbillingcurrencyid' and lowvalue <= '$totaladminusers' and highvalue >= '$totaladminusers'";
				//echo $querybuilder;
				$getrate = mysql_query($querybuilder);
				$rowrate = mysql_fetch_array($getrate);
				$rateadminusers = $rowrate['rate'];
				if($rateadminusers <=0){
					$rateadminusers	= 0;
				}
				if($accountbillingdiscountpercentage <= 0){
					$accountbillingdiscountpercentage = 0;
				}
				else {
					$rateadminusers = round(($rateadminusers*((100-$accountbillingdiscountpercentage)/100)), 2);
				}
				//echo "rateadminusers - ".$rateadminusers."<br/>";
				
				//calculate the cost of the normal users
				$querybuilder = "select accountbillingnormaluserrateid, rate from $masterdatabase.accountbillingnormaluserrate where disabled='0' and 
				accountbillingcurrencyid='$accountbillingcurrencyid' and lowvalue <= '$totalnormalusers' and highvalue >= '$totalnormalusers'";
				//echo $querybuilder;
				$getrate = mysql_query($querybuilder);
				$rowrate = mysql_fetch_array($getrate);
				$ratenormalusers = $rowrate['rate'];
				if($ratenormalusers <=0){
					$ratenormalusers	= 0;
				}
				if($accountbillingdiscountpercentage <= 0){
					$accountbillingdiscountpercentage = 0;
				}
				else {
					$ratenormalusers = round(($ratenormalusers*((100-$accountbillingdiscountpercentage)/100)), 2);
				}
				//echo "ratenormalusers - ".$ratenormalusers."<br/>";
				
				
				//calculate the cost of the database and file repository
				$querybuilder = "select accountbillingdatabasesizerateid, rate from $masterdatabase.accountbillingdatabasesizerate where disabled='0' and 
				accountbillingcurrencyid='$accountbillingcurrencyid' and lowvalue <= '$totalsize' and highvalue >= '$totalsize'";
				//echo $querybuilder;
				$getrate = mysql_query($querybuilder);
				$rowrate = mysql_fetch_array($getrate);
				$ratedatabasesize = $rowrate['rate'];
				if($ratedatabasesize <=0){
					$ratedatabasesize	= 0;
				}
				if($accountbillingdiscountpercentage <= 0){
					$accountbillingdiscountpercentage = 0;
				}
				else {
					$ratedatabasesize = round(($ratedatabasesize*((100-$accountbillingdiscountpercentage)/100)), 2);
				}
				//echo "ratedatabasesize - ".$ratedatabasesize."<br/>";
					
				//calculate the total cost of SMS Twilio
				$getsmscost = mysql_query("select sum(finaltotalcost) as totalsmscost  from $masterdatabase.accountbillingtwiliodailyusage
				inner join $masterdatabase.accountbillingcurrency on accountbillingcurrency.accountbillingcurrencyid = accountbillingtwiliodailyusage.accountbillingcurrencyid
				where tenantid = $tenantid and usagedate = '$usagedate'");
				$getsmscostrow = mysql_fetch_array($getsmscost);
				$totalsmscost = $getsmscostrow['totalsmscost'];
				
				//display the Mailchimp Email cost
				if($mailchimptotalcost>0){						
					$getcurrency = mysql_query("select * from $masterdatabase.tenant 
					inner join $masterdatabase.accountbillingcurrency on accountbillingcurrency.accountbillingcurrencyid = tenant.accountbillingcurrencyid
					where tenantid = '$tenantid'");
					$getcurrencyrow = mysql_fetch_array($getcurrency);
					$currencycode = $getcurrencyrow['currencycode'];					
					echo "<td>".$currencycode." ".$mailchimptotalcost."</td>";
				}				
				
				//get summary			
				$totalvalue = $rateadminusers + $ratenormalusers + $ratedatabasesize + $totalsmscost + $mailchimptotalcost;
				
				?>
				<br/><br/><h2><?php echo $langval63 ?></h2>
				<?php
				echo "<br/>";
				echo "<p>".$langval52."</b>";
				echo "<br/>";		
				echo "<br/><table class='table table-bordered'>";
				echo "<thead><tr><td><b>".$langval53."</b></td><td><b>".$langval54."</b></td><td><b>".$langval55."</b></td></tr></thead>";
				echo "<tr><td>".$langval56."</td><td>".$currencycode." ".$rateadminusers."</td><td>".$langval57."</td></tr>";
				echo "<tr><td>".$langval58."</td><td>".$currencycode." ".$ratenormalusers."</td><td>".$langval59."</td></tr>";
				echo "<tr><td>".$langval60."</td><td>".$currencycode." ".$ratedatabasesize."</td><td>".$langval61."</td></tr>";
				if($smsbill==1){				
					echo "<tr><td>".$langval85."</td><td>".$currencycode." ".$totalsmscost."</td><td>".$langval84."</td></tr>";
				}				
				if($mailchimptotalcost>0){				
					echo "<tr><td>".$langval86."</td><td>".$currencycode." ".$mailchimptotalcost."</td><td>".$langval84."</td></tr>";
				}				
				echo "<tr><td><b>".$langval62."</b></td><td><b>".$currencycode." ".$totalvalue."</b></td></tr>";
				echo "</tr></table>";
				?>
				
			<br/><br/><br/>				
			
			</div>		
		<br/><br/><br/><br/><br/><br/>
		</div>	
		
	</div>	
	<?php include_once ('footer.php'); ?>
</body>
